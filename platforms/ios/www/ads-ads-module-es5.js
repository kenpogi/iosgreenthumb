function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["ads-ads-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/ads/ads.page.html":
  /*!*************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/ads/ads.page.html ***!
    \*************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppAdsAdsPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n<ion-content>\n    <ion-slides [options]=\"slideOptsOne\" pager=\"true\" style=\" --bullet-background: white; --bullet-background-active: yellow;\">\n        <ion-slide *ngFor=\"let image of ImageArray\">\n          <img src=\"{{image.image}}\" class=\"img-responsive\" style=\"display: block; width: 100%; height: 100vh; object-fit:fill;\">\n         \n        </ion-slide>\n    </ion-slides>\n    <div class=\"my-overlay\" text-center>\n        <ion-icon name=\"close-circle\" color=\"light\" style=\"zoom: 2.0;\" (click)=\"goChoose()\"></ion-icon>\n    </div>\n</ion-content>\n";
    /***/
  },

  /***/
  "./src/app/ads/ads.module.ts":
  /*!***********************************!*\
    !*** ./src/app/ads/ads.module.ts ***!
    \***********************************/

  /*! exports provided: AdsPageModule */

  /***/
  function srcAppAdsAdsModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AdsPageModule", function () {
      return AdsPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _ads_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./ads.page */
    "./src/app/ads/ads.page.ts");

    var routes = [{
      path: '',
      component: _ads_page__WEBPACK_IMPORTED_MODULE_6__["AdsPage"]
    }];

    var AdsPageModule = function AdsPageModule() {
      _classCallCheck(this, AdsPageModule);
    };

    AdsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      declarations: [_ads_page__WEBPACK_IMPORTED_MODULE_6__["AdsPage"]]
    })], AdsPageModule);
    /***/
  },

  /***/
  "./src/app/ads/ads.page.scss":
  /*!***********************************!*\
    !*** ./src/app/ads/ads.page.scss ***!
    \***********************************/

  /*! exports provided: default */

  /***/
  function srcAppAdsAdsPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".justify {\n  text-align: justify;\n}\n\n.img-responsive {\n  width: 100%;\n  height: auto;\n}\n\n.img-loading {\n  width: 50%;\n  height: auto;\n}\n\n.btn-default {\n  --background: #1dc1e6;\n}\n\n.my-overlay {\n  position: fixed;\n  width: 12%;\n  text-shadow: 3px 3px 5px black;\n  z-index: 20;\n  top: 2%;\n  right: 3%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZW5kcmljay9EZXNrdG9wL1RlbG1vIFNvbHV0aW9ucy9Gb3JQcm9kdWN0aW9uL21heTEwMjAyMS9sYXN0IHNldHVwL2dyZWVudGh1bWJhcHAgY29weS9zcmMvYXBwL2Fkcy9hZHMucGFnZS5zY3NzIiwic3JjL2FwcC9hZHMvYWRzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLG1CQUFBO0FDQ0o7O0FEQ0E7RUFDSSxXQUFBO0VBQ0EsWUFBQTtBQ0VKOztBREFBO0VBQ0ksVUFBQTtFQUNBLFlBQUE7QUNHSjs7QUREQTtFQUNJLHFCQUFBO0FDSUo7O0FERkE7RUFDSSxlQUFBO0VBQ0EsVUFBQTtFQUNBLDhCQUFBO0VBQ0EsV0FBQTtFQUNBLE9BQUE7RUFDQSxTQUFBO0FDS0oiLCJmaWxlIjoic3JjL2FwcC9hZHMvYWRzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5qdXN0aWZ5e1xuICAgIHRleHQtYWxpZ246IGp1c3RpZnk7XG59XG4uaW1nLXJlc3BvbnNpdmV7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiBhdXRvO1xufVxuLmltZy1sb2FkaW5ne1xuICAgIHdpZHRoOiA1MCU7XG4gICAgaGVpZ2h0OiBhdXRvO1xufVxuLmJ0bi1kZWZhdWx0e1xuICAgIC0tYmFja2dyb3VuZDogIzFkYzFlNjtcbn1cbi5teS1vdmVybGF5IHtcbiAgICBwb3NpdGlvbjogZml4ZWQ7XG4gICAgd2lkdGg6IDEyJTtcbiAgICB0ZXh0LXNoYWRvdzogM3B4IDNweCA1cHggYmxhY2s7XG4gICAgei1pbmRleDogMjA7XG4gICAgdG9wOiAyJTtcbiAgICByaWdodDogMyU7XG4gIH0iLCIuanVzdGlmeSB7XG4gIHRleHQtYWxpZ246IGp1c3RpZnk7XG59XG5cbi5pbWctcmVzcG9uc2l2ZSB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IGF1dG87XG59XG5cbi5pbWctbG9hZGluZyB7XG4gIHdpZHRoOiA1MCU7XG4gIGhlaWdodDogYXV0bztcbn1cblxuLmJ0bi1kZWZhdWx0IHtcbiAgLS1iYWNrZ3JvdW5kOiAjMWRjMWU2O1xufVxuXG4ubXktb3ZlcmxheSB7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgd2lkdGg6IDEyJTtcbiAgdGV4dC1zaGFkb3c6IDNweCAzcHggNXB4IGJsYWNrO1xuICB6LWluZGV4OiAyMDtcbiAgdG9wOiAyJTtcbiAgcmlnaHQ6IDMlO1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/ads/ads.page.ts":
  /*!*********************************!*\
    !*** ./src/app/ads/ads.page.ts ***!
    \*********************************/

  /*! exports provided: AdsPage */

  /***/
  function srcAppAdsAdsPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AdsPage", function () {
      return AdsPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");

    var AdsPage = /*#__PURE__*/function () {
      function AdsPage(navCtrl, router) {
        _classCallCheck(this, AdsPage);

        this.navCtrl = navCtrl;
        this.router = router;
        this.ImageArray = [];
        this.slideOptsOne = {
          initialSlide: 0,
          slidesPerView: 1,
          autoplay: true
        };
        this.ImageArray = [{
          'image': 'assets/ads/Brixy_Ad_Panel_1.jpg'
        }, {
          'image': 'assets/ads/Brixy_Ad_Panel_2.jpg'
        }, {
          'image': 'assets/ads/Brixy_Ad_Panel_3.jpg'
        }];
      }

      _createClass(AdsPage, [{
        key: "goChoose",
        value: function goChoose() {
          this.router.navigate(['termsconditionslogin']);
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return AdsPage;
    }();

    AdsPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
      }];
    };

    AdsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-ads',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./ads.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/ads/ads.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./ads.page.scss */
      "./src/app/ads/ads.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])], AdsPage);
    /***/
  }
}]);
//# sourceMappingURL=ads-ads-module-es5.js.map