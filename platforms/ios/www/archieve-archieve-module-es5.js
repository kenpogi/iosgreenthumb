function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["archieve-archieve-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/archieve/archieve.page.html":
  /*!***********************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/archieve/archieve.page.html ***!
    \***********************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppArchieveArchievePageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header mode=\"ios\" no-border style=\"padding-top: 3%;\">\n  <ion-toolbar mode=\"ios\">\n      <ion-buttons slot=\"secondary\" (click)=\"goBack()\">\n        <ion-button mode=\"ios\" style=\"font-size: 15px;\">\n            Back\n        </ion-button>\n      </ion-buttons>\n\n      <ion-title color=\"secondary\">Saved items </ion-title>\n    </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-row *ngFor=\"let item of itemList\">\n    <ion-col>\n      <ion-card mode=\"ios\" class=\"for_items\" >\n        <ion-card-content no-padding>\n          <ion-row>\n            <ion-col text-left size=\"5\">\n              <ion-item lines=\"none\" class=\"for_itemms\" (click)=\"goProductview(item.id)\">\n                <ion-avatar slot=\"start\">\n                  <div *ngIf=\"!item?.profile_photo\">\n                    <img src=\"assets/greenthumb-images/userpic.png\">\n                  </div>\n                  <div *ngIf=\"item?.profile_photo\">\n                    <img [src] = \"item.profile_photo\" >\n                  </div> \n                </ion-avatar>\n                <ion-label class=\"for_name\">username{{item.username}}</ion-label>\n              </ion-item>\n              <img (click)=\"goProductview(item.id)\" class=\"for_itemmimg\" [src] = \"item.item_cover_photo\">\n            </ion-col>\n            <ion-col text-left size=\"7\">\n              <ion-row (click)=\"goProductview(item.id)\">\n                <ion-col no-padding text-left>\n                  <ion-label (click)=\"goProductview(item.id)\" color=\"dark\"><h2 style=\"padding-bottom: 2.5%;\" class=\"for_itemmlocation\">\n                    <div class=\"for_unitqty\">{{item.quantity}} \n                      <label *ngIf=\"item.stocks_id=='1'\">\n                        <label *ngIf=\"item.quantity>1\">pieces</label>\n                        <label *ngIf=\"item.quantity==1\">piece</label>\n                      </label>\n                      <label *ngIf=\"item.stocks_id=='2'\">\n                        <label *ngIf=\"item.quantity>1\">pounds</label>\n                        <label *ngIf=\"item.quantity==1\">pound</label>\n                      </label>\n                      <label *ngIf=\"item.stocks_id=='3'\">\n                        <label *ngIf=\"item.quantity>1\">ounces</label>\n                        <label *ngIf=\"item.quantity==1\">ounce</label>\n                      </label>\n                      <label *ngIf=\"item.stocks_id=='4'\">\n                        {{item.others_stock}}\n                      </label>\n                    </div>\n                  </h2></ion-label>\n                  <ion-label color=\"secondary\"><h1 class=\"for_pprice\">${{item.price}}</h1></ion-label>\n                </ion-col>\n                <ion-col no-padding text-right>\n                  <div class=\"for_category\">{{item.category}}</div>\n                  <div style=\"margin-top: 5px;\">\n                      <ionic4-star-rating #rating\n                          activeIcon = \"ios-star\"\n                          defaultIcon = \"ios-star-outline\"\n                          activeColor = \"#ffce00\" \n                          defaultColor = \"#ffce00\"\n                          readonly=\"false\"\n                          rating=\"{{item.user_rating}}\"\n                          fontSize = \"15px\">\n                        </ionic4-star-rating>\n                  </div>\n                </ion-col>\n              </ion-row>\n              <ion-row style=\"margin-top: -2px;\" (click)=\"goProductview(item.id)\">\n                <ion-col no-padding text-left>\n                  <ion-label color=\"dark\">\n                    <h2 class=\"for_itemmname\">{{item.title}}</h2></ion-label>\n                </ion-col>\n              </ion-row>\n              <ion-label ><h2 class=\"for_itemmlocation\">\n                <ion-icon class=\"for_pin\" mode=\"ios\" color=\"primary\" name=\"pin\"></ion-icon>&nbsp; {{item.location}}\n              </h2></ion-label>\n              <ion-row>\n                <ion-col no-padding text-left size=\"9\" class=\"for_borderitem\" (click)=\"goProductview(item.id)\">\n                  <ion-row>\n                    <ion-col text-left no-padding>\n                      <ion-label color=\"secondary\"><h2 class=\"for_itemmbtn\">\n                          &nbsp;{{item.date}}\n                        </h2></ion-label>\n                    </ion-col>\n                    <ion-col text-left no-padding>\n                      <ion-label color=\"secondary\"><h2 class=\"for_itemmbtn\">\n                          &nbsp;{{item.time}}\n                        </h2></ion-label>\n                    </ion-col>\n                  </ion-row>\n                </ion-col>\n                <ion-col no-padding text-center size=\"3\" (click)=\"saveThis(item.id)\">\n                  <ion-icon style=\"zoom: 2;margin-top: -5px;\" name=\"heart\" mode=\"ios\"\n                  id=\"class{{item.id}}\" [class]=\"(item.save_status == '1') ? 'myHeartSave' : 'myHeartUnSave'\"></ion-icon>\n                </ion-col>\n              </ion-row>\n            </ion-col>\n          </ion-row>\n        </ion-card-content>\n      </ion-card>\n    </ion-col>\n  </ion-row>\n  <!-- <ion-row justify-content-center align-items-center style='height: 80%'>\n    <ion-col text-center>\n      <ion-label color=\"secondary\"><h2 class=\"message\">No items archived</h2></ion-label>\n      <p style=\"margin-top: 0;\">When you archive an item, you'll see it here</p>\n    </ion-col>\n  </ion-row> -->\n</ion-content>\n";
    /***/
  },

  /***/
  "./src/app/archieve/archieve-routing.module.ts":
  /*!*****************************************************!*\
    !*** ./src/app/archieve/archieve-routing.module.ts ***!
    \*****************************************************/

  /*! exports provided: ArchievePageRoutingModule */

  /***/
  function srcAppArchieveArchieveRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ArchievePageRoutingModule", function () {
      return ArchievePageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _archieve_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./archieve.page */
    "./src/app/archieve/archieve.page.ts");

    var routes = [{
      path: '',
      component: _archieve_page__WEBPACK_IMPORTED_MODULE_3__["ArchievePage"]
    }];

    var ArchievePageRoutingModule = function ArchievePageRoutingModule() {
      _classCallCheck(this, ArchievePageRoutingModule);
    };

    ArchievePageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], ArchievePageRoutingModule);
    /***/
  },

  /***/
  "./src/app/archieve/archieve.module.ts":
  /*!*********************************************!*\
    !*** ./src/app/archieve/archieve.module.ts ***!
    \*********************************************/

  /*! exports provided: ArchievePageModule */

  /***/
  function srcAppArchieveArchieveModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ArchievePageModule", function () {
      return ArchievePageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _archieve_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./archieve-routing.module */
    "./src/app/archieve/archieve-routing.module.ts");
    /* harmony import */


    var ionic4_star_rating__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ionic4-star-rating */
    "./node_modules/ionic4-star-rating/dist/index.js");
    /* harmony import */


    var _archieve_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./archieve.page */
    "./src/app/archieve/archieve.page.ts");

    var ArchievePageModule = function ArchievePageModule() {
      _classCallCheck(this, ArchievePageModule);
    };

    ArchievePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], ionic4_star_rating__WEBPACK_IMPORTED_MODULE_6__["StarRatingModule"], _archieve_routing_module__WEBPACK_IMPORTED_MODULE_5__["ArchievePageRoutingModule"]],
      declarations: [_archieve_page__WEBPACK_IMPORTED_MODULE_7__["ArchievePage"]]
    })], ArchievePageModule);
    /***/
  },

  /***/
  "./src/app/archieve/archieve.page.scss":
  /*!*********************************************!*\
    !*** ./src/app/archieve/archieve.page.scss ***!
    \*********************************************/

  /*! exports provided: default */

  /***/
  function srcAppArchieveArchievePageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".for_itemmessage {\n  display: block;\n  font-weight: normal;\n  color: black;\n  font-size: 14px;\n  margin-top: 3px;\n}\n\n.message {\n  font-size: 18px;\n  font-weight: bold;\n}\n\n.btnheader {\n  margin-top: 0.5%;\n}\n\n.card_header {\n  padding-bottom: 0;\n  padding-top: 7px;\n}\n\n.card_title {\n  font-size: 15px;\n  text-transform: uppercase;\n}\n\n.card_content {\n  font-size: 13px;\n  padding-top: 8px;\n  padding-bottom: 0px;\n  line-height: 1.1;\n  color: black;\n}\n\n.card_txt {\n  padding-top: 1.2%;\n  color: #679733;\n  font-size: 12px;\n}\n\n.card_hide {\n  color: #679733;\n  font-size: 13px;\n}\n\n.gotoAnnounce {\n  text-align: right;\n}\n\n.promocard {\n  margin-top: 0;\n  background: #e2f0cb;\n  box-shadow: none;\n  border-radius: 0;\n}\n\n.promoheader {\n  padding-top: 10px;\n  padding-bottom: 0px;\n  padding-right: 13px;\n  padding-left: 17px;\n}\n\n.promo_smalltxt {\n  text-transform: uppercase;\n  color: #679733;\n  font-size: 13px;\n  font-weight: bold;\n}\n\n.promo_largetxt {\n  text-transform: uppercase;\n  color: black;\n  font-size: 27px;\n  font-weight: bolder;\n  margin-top: -4.2%;\n}\n\n.btnshop {\n  font-size: 11px;\n  margin-top: 10%;\n  --border-radius: 3px;\n}\n\nion-select {\n  font-size: 13px;\n}\n\n.for_sortfeed {\n  padding-left: 0;\n  padding-top: 0;\n  margin-top: -5%;\n  border-bottom: 1px solid #e2f0cb;\n  padding-bottom: 0;\n  margin-bottom: 3%;\n}\n\n.for_items {\n  margin-top: 0%;\n  box-shadow: 0 1px 10px #e2f0cb;\n  border-radius: 10px;\n  border: 1px solid #e2f0cb;\n  margin-bottom: 0;\n}\n\n.for_chip {\n  --background: transparent;\n  margin: 0;\n  padding-right: 0;\n  padding-left: 15px;\n}\n\n.for_itemimg {\n  padding-left: 5px;\n  margin: auto;\n  width: 90%;\n  height: 70%;\n}\n\n.for_price {\n  font-size: 20px;\n  font-weight: bolder;\n  padding-top: 5px;\n}\n\n.for_itemname {\n  font-size: 15px;\n  font-weight: bolder;\n}\n\n.for_itemcateg {\n  text-transform: uppercase;\n  font-size: 12px;\n  font-weight: lighter;\n}\n\n.for_itemlocation {\n  font-size: 13px;\n  font-weight: lighter;\n}\n\n.for_pin {\n  font-size: 15px;\n  margin-bottom: -1%;\n  margin-left: -1%;\n}\n\n.for_itembtn {\n  font-size: 11px;\n}\n\n.for_itembtnicon {\n  font-size: 20px;\n  margin-bottom: -7%;\n  margin-right: 2%;\n}\n\n.my-custom-class .alert-wrapper {\n  background: #e5e5e5;\n}\n\n.for_itemms {\n  --padding-start: 0;\n  margin-top: -8px;\n  --inner-padding-end: 0;\n  --background: transparent;\n}\n\n.for_itemms ion-avatar {\n  width: 100% !important;\n  height: 100% !important;\n  max-width: 25px !important;\n  max-height: 25px !important;\n  margin-right: 5px;\n  margin-top: 0;\n}\n\n.for_itemms ion-avatar img {\n  border: 1px solid rgba(0, 0, 0, 0.12);\n  width: 100% !important;\n  height: 100% !important;\n  max-width: 25px !important;\n  max-height: 25px !important;\n  margin-right: 5px;\n  margin-top: 0;\n  border-radius: 50%;\n}\n\n.for_name {\n  font-size: 12px;\n  text-transform: capitalize;\n  margin-top: 0;\n  color: black;\n}\n\n.for_itemmimg {\n  margin: auto;\n  width: 90%;\n  margin-top: -10px;\n  padding-bottom: 5px;\n}\n\n.for_pprice {\n  font-size: 23px;\n  font-weight: bolder;\n}\n\n.for_category {\n  background: #76c961;\n  padding: 3px;\n  border-top-right-radius: 10px;\n  margin-top: -5px;\n  margin-right: -5px;\n  border-bottom-left-radius: 10px;\n  text-transform: capitalize;\n  font-size: 12px;\n  font-weight: lighter;\n  color: white;\n  text-align: center;\n}\n\n.for_itemmname {\n  font-size: 15px;\n  font-weight: bolder;\n}\n\n.for_itemmlocation {\n  font-size: 12.5px;\n  font-weight: lighter;\n  color: #989aa2;\n  padding-right: 5px;\n  padding-bottom: 2.5%;\n}\n\n.for_borderitem {\n  border-top: 1px solid #e2f0cb;\n  padding-top: 2.5%;\n}\n\n.for_itemmbtn {\n  font-size: 11px;\n  color: #679733;\n}\n\n.for_unitqty {\n  font-size: 11px;\n  text-align: left;\n  color: #989aa2;\n}\n\n.myHeartUnSave {\n  color: #d3dbc9;\n}\n\n.myHeartSave {\n  color: red;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZW5kcmljay9EZXNrdG9wL1RlbG1vIFNvbHV0aW9ucy9Gb3JQcm9kdWN0aW9uL21heTEwMjAyMS9sYXN0IHNldHVwL2dyZWVudGh1bWJhcHAgY29weS9zcmMvYXBwL2FyY2hpZXZlL2FyY2hpZXZlLnBhZ2Uuc2NzcyIsInNyYy9hcHAvYXJjaGlldmUvYXJjaGlldmUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksY0FBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxlQUFBO0FDQ0o7O0FEQ0E7RUFDSSxlQUFBO0VBQ0EsaUJBQUE7QUNFSjs7QURDQTtFQUNJLGdCQUFBO0FDRUo7O0FEQUE7RUFDSSxpQkFBQTtFQUNBLGdCQUFBO0FDR0o7O0FEREE7RUFDSSxlQUFBO0VBQ0EseUJBQUE7QUNJSjs7QURGQTtFQUNJLGVBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxZQUFBO0FDS0o7O0FESEE7RUFDSSxpQkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0FDTUo7O0FESkE7RUFDSSxjQUFBO0VBQ0EsZUFBQTtBQ09KOztBRExBO0VBQ0ksaUJBQUE7QUNRSjs7QUROQTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7QUNTSjs7QURQQTtFQUNJLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0FDVUo7O0FEUkE7RUFDSSx5QkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7QUNXSjs7QURUQTtFQUNJLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0FDWUo7O0FEVkE7RUFDSSxlQUFBO0VBQ0EsZUFBQTtFQUNBLG9CQUFBO0FDYUo7O0FEWEE7RUFDSSxlQUFBO0FDY0o7O0FEWkE7RUFDSSxlQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7RUFDQSxnQ0FBQTtFQUNBLGlCQUFBO0VBQ0EsaUJBQUE7QUNlSjs7QURiQTtFQUNJLGNBQUE7RUFDQSw4QkFBQTtFQUNBLG1CQUFBO0VBQ0EseUJBQUE7RUFDQSxnQkFBQTtBQ2dCSjs7QURkQTtFQUNJLHlCQUFBO0VBQ0EsU0FBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7QUNpQko7O0FEZkE7RUFDSSxpQkFBQTtFQUNBLFlBQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtBQ2tCSjs7QURoQkE7RUFDSSxlQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtBQ21CSjs7QURqQkE7RUFDSSxlQUFBO0VBQ0EsbUJBQUE7QUNvQko7O0FEbEJBO0VBQ0kseUJBQUE7RUFDQSxlQUFBO0VBQ0Esb0JBQUE7QUNxQko7O0FEbkJBO0VBQ0ksZUFBQTtFQUNBLG9CQUFBO0FDc0JKOztBRHBCQTtFQUNJLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0FDdUJKOztBRHJCQTtFQUNJLGVBQUE7QUN3Qko7O0FEdEJBO0VBQ0ksZUFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7QUN5Qko7O0FEdkJBO0VBQ0ksbUJBQUE7QUMwQko7O0FEckJBO0VBQ0ksa0JBQUE7RUFDQSxnQkFBQTtFQUNBLHNCQUFBO0VBQ0EseUJBQUE7QUN3Qko7O0FEdkJJO0VBQ0ksc0JBQUE7RUFDQSx1QkFBQTtFQUNBLDBCQUFBO0VBQ0EsMkJBQUE7RUFDQSxpQkFBQTtFQUFrQixhQUFBO0FDMEIxQjs7QUR6QlE7RUFDSSxxQ0FBQTtFQUNBLHNCQUFBO0VBQ0EsdUJBQUE7RUFDQSwwQkFBQTtFQUNBLDJCQUFBO0VBQ0EsaUJBQUE7RUFBa0IsYUFBQTtFQUNsQixrQkFBQTtBQzRCWjs7QUR2QkE7RUFDSSxlQUFBO0VBQ0EsMEJBQUE7RUFDQSxhQUFBO0VBQ0EsWUFBQTtBQzBCSjs7QUR4QkE7RUFDSSxZQUFBO0VBQ0EsVUFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7QUMyQko7O0FEekJBO0VBQ0ksZUFBQTtFQUNBLG1CQUFBO0FDNEJKOztBRDFCQTtFQUNJLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLDZCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLCtCQUFBO0VBQ0EsMEJBQUE7RUFDQSxlQUFBO0VBQ0Esb0JBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7QUM2Qko7O0FEM0JBO0VBQ0ksZUFBQTtFQUNBLG1CQUFBO0FDOEJKOztBRDVCQTtFQUNJLGlCQUFBO0VBQ0Esb0JBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7RUFDQSxvQkFBQTtBQytCSjs7QUQ3QkE7RUFDSSw2QkFBQTtFQUNBLGlCQUFBO0FDZ0NKOztBRDlCQTtFQUNJLGVBQUE7RUFDQSxjQUFBO0FDaUNKOztBRC9CQTtFQUNJLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7QUNrQ0o7O0FEL0JBO0VBQ0ksY0FBQTtBQ2tDSjs7QURoQ0E7RUFDSSxVQUFBO0FDbUNKIiwiZmlsZSI6InNyYy9hcHAvYXJjaGlldmUvYXJjaGlldmUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmZvcl9pdGVtbWVzc2FnZXtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBmb250LXdlaWdodDogbm9ybWFsO1xuICAgIGNvbG9yOiBibGFjaztcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgbWFyZ2luLXRvcDogM3B4O1xufVxuLm1lc3NhZ2V7XG4gICAgZm9udC1zaXplOiAxOHB4O1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuXG4uYnRuaGVhZGVye1xuICAgIG1hcmdpbi10b3A6IDAuNSU7XG59XG4uY2FyZF9oZWFkZXJ7XG4gICAgcGFkZGluZy1ib3R0b206IDA7XG4gICAgcGFkZGluZy10b3A6IDdweDtcbn1cbi5jYXJkX3RpdGxle1xuICAgIGZvbnQtc2l6ZTogMTVweDtcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xufVxuLmNhcmRfY29udGVudHtcbiAgICBmb250LXNpemU6IDEzcHg7XG4gICAgcGFkZGluZy10b3A6IDhweDtcbiAgICBwYWRkaW5nLWJvdHRvbTogMHB4O1xuICAgIGxpbmUtaGVpZ2h0OiAxLjE7XG4gICAgY29sb3I6IGJsYWNrO1xufVxuLmNhcmRfdHh0e1xuICAgIHBhZGRpbmctdG9wOiAxLjIlO1xuICAgIGNvbG9yOiAjNjc5NzMzO1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbn1cbi5jYXJkX2hpZGV7XG4gICAgY29sb3I6ICM2Nzk3MzM7XG4gICAgZm9udC1zaXplOiAxM3B4O1xufVxuLmdvdG9Bbm5vdW5jZXtcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcbn1cbi5wcm9tb2NhcmR7XG4gICAgbWFyZ2luLXRvcDogMDtcbiAgICBiYWNrZ3JvdW5kOiAjZTJmMGNiO1xuICAgIGJveC1zaGFkb3c6IG5vbmU7XG4gICAgYm9yZGVyLXJhZGl1czogMDtcbn1cbi5wcm9tb2hlYWRlcntcbiAgICBwYWRkaW5nLXRvcDogMTBweDtcbiAgICBwYWRkaW5nLWJvdHRvbTogMHB4O1xuICAgIHBhZGRpbmctcmlnaHQ6IDEzcHg7XG4gICAgcGFkZGluZy1sZWZ0OiAxN3B4O1xufVxuLnByb21vX3NtYWxsdHh0e1xuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgY29sb3I6ICM2Nzk3MzM7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLnByb21vX2xhcmdldHh0e1xuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgY29sb3I6IGJsYWNrO1xuICAgIGZvbnQtc2l6ZTogMjdweDtcbiAgICBmb250LXdlaWdodDogYm9sZGVyO1xuICAgIG1hcmdpbi10b3A6IC00LjIlO1xufVxuLmJ0bnNob3B7XG4gICAgZm9udC1zaXplOiAxMXB4O1xuICAgIG1hcmdpbi10b3A6IDEwJTtcbiAgICAtLWJvcmRlci1yYWRpdXM6IDNweDtcbn1cbmlvbi1zZWxlY3R7XG4gICAgZm9udC1zaXplOiAxM3B4O1xufVxuLmZvcl9zb3J0ZmVlZHtcbiAgICBwYWRkaW5nLWxlZnQ6IDA7XG4gICAgcGFkZGluZy10b3A6IDA7XG4gICAgbWFyZ2luLXRvcDogLTUlO1xuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZTJmMGNiO1xuICAgIHBhZGRpbmctYm90dG9tOiAwO1xuICAgIG1hcmdpbi1ib3R0b206IDMlO1xufVxuLmZvcl9pdGVtc3tcbiAgICBtYXJnaW4tdG9wOiAwJTtcbiAgICBib3gtc2hhZG93OiAwIDFweCAxMHB4ICNlMmYwY2I7XG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjZTJmMGNiO1xuICAgIG1hcmdpbi1ib3R0b206IDA7XG59XG4uZm9yX2NoaXB7XG4gICAgLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgICBtYXJnaW46IDA7XG4gICAgcGFkZGluZy1yaWdodDogMDtcbiAgICBwYWRkaW5nLWxlZnQ6IDE1cHg7XG59XG4uZm9yX2l0ZW1pbWd7XG4gICAgcGFkZGluZy1sZWZ0OiA1cHg7XG4gICAgbWFyZ2luOiBhdXRvO1xuICAgIHdpZHRoOiA5MCU7XG4gICAgaGVpZ2h0OiA3MCU7XG59XG4uZm9yX3ByaWNle1xuICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICBmb250LXdlaWdodDogYm9sZGVyO1xuICAgIHBhZGRpbmctdG9wOiA1cHg7XG59XG4uZm9yX2l0ZW1uYW1le1xuICAgIGZvbnQtc2l6ZTogMTVweDtcbiAgICBmb250LXdlaWdodDogYm9sZGVyO1xufVxuLmZvcl9pdGVtY2F0ZWd7XG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgICBmb250LXNpemU6IDEycHg7XG4gICAgZm9udC13ZWlnaHQ6IGxpZ2h0ZXI7XG59XG4uZm9yX2l0ZW1sb2NhdGlvbntcbiAgICBmb250LXNpemU6IDEzcHg7XG4gICAgZm9udC13ZWlnaHQ6IGxpZ2h0ZXI7XG59XG4uZm9yX3BpbntcbiAgICBmb250LXNpemU6IDE1cHg7XG4gICAgbWFyZ2luLWJvdHRvbTogLTElO1xuICAgIG1hcmdpbi1sZWZ0OiAtMSU7XG59XG4uZm9yX2l0ZW1idG57XG4gICAgZm9udC1zaXplOiAxMXB4Oztcbn1cbi5mb3JfaXRlbWJ0bmljb257XG4gICAgZm9udC1zaXplOiAyMHB4O1xuICAgIG1hcmdpbi1ib3R0b206IC03JTtcbiAgICBtYXJnaW4tcmlnaHQ6IDIlO1xufVxuLm15LWN1c3RvbS1jbGFzcyAuYWxlcnQtd3JhcHBlciB7XG4gICAgYmFja2dyb3VuZDogI2U1ZTVlNTtcbiAgfVxuXG5cblxuLmZvcl9pdGVtbXN7XG4gICAgLS1wYWRkaW5nLXN0YXJ0OiAwO1xuICAgIG1hcmdpbi10b3A6IC04cHg7XG4gICAgLS1pbm5lci1wYWRkaW5nLWVuZDogMDtcbiAgICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICAgIGlvbi1hdmF0YXIgIHsgICAgIFxuICAgICAgICB3aWR0aDogMTAwJSAhaW1wb3J0YW50O1xuICAgICAgICBoZWlnaHQ6IDEwMCUgIWltcG9ydGFudDtcbiAgICAgICAgbWF4LXdpZHRoOiAyNXB4ICFpbXBvcnRhbnQ7XG4gICAgICAgIG1heC1oZWlnaHQ6IDI1cHggIWltcG9ydGFudDtcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiA1cHg7bWFyZ2luLXRvcDogMDtcbiAgICAgICAgaW1ne1xuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgcmdiYSgwLCAwLCAwLCAwLjEyKTtcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XG4gICAgICAgICAgICBoZWlnaHQ6IDEwMCUgIWltcG9ydGFudDtcbiAgICAgICAgICAgIG1heC13aWR0aDogMjVweCAhaW1wb3J0YW50O1xuICAgICAgICAgICAgbWF4LWhlaWdodDogMjVweCAhaW1wb3J0YW50O1xuICAgICAgICAgICAgbWFyZ2luLXJpZ2h0OiA1cHg7bWFyZ2luLXRvcDogMDtcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICAgICAgfVxuICAgIH1cbiAgfVxuICBcbi5mb3JfbmFtZXtcbiAgICBmb250LXNpemU6IDEycHg7XG4gICAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG4gICAgbWFyZ2luLXRvcDogMDtcbiAgICBjb2xvcjogYmxhY2s7XG59XG4uZm9yX2l0ZW1taW1ne1xuICAgIG1hcmdpbjogYXV0bztcbiAgICB3aWR0aDogOTAlO1xuICAgIG1hcmdpbi10b3A6IC0xMHB4O1xuICAgIHBhZGRpbmctYm90dG9tOiA1cHg7XG59XG4uZm9yX3BwcmljZXtcbiAgICBmb250LXNpemU6IDIzcHg7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbn1cbi5mb3JfY2F0ZWdvcnl7XG4gICAgYmFja2dyb3VuZDogIzc2Yzk2MTtcbiAgICBwYWRkaW5nOiAzcHg7XG4gICAgYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDEwcHg7XG4gICAgbWFyZ2luLXRvcDogLTVweDtcbiAgICBtYXJnaW4tcmlnaHQ6IC01cHg7XG4gICAgYm9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czogMTBweDtcbiAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbiAgICBmb250LXNpemU6IDEycHg7XG4gICAgZm9udC13ZWlnaHQ6IGxpZ2h0ZXI7XG4gICAgY29sb3I6IHdoaXRlO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5mb3JfaXRlbW1uYW1le1xuICAgIGZvbnQtc2l6ZTogMTVweDtcbiAgICBmb250LXdlaWdodDogYm9sZGVyO1xufVxuLmZvcl9pdGVtbWxvY2F0aW9ue1xuICAgIGZvbnQtc2l6ZTogMTIuNXB4O1xuICAgIGZvbnQtd2VpZ2h0OiBsaWdodGVyO1xuICAgIGNvbG9yOiAjOTg5YWEyO1xuICAgIHBhZGRpbmctcmlnaHQ6IDVweDtcbiAgICBwYWRkaW5nLWJvdHRvbTogMi41JTtcbn1cbi5mb3JfYm9yZGVyaXRlbXtcbiAgICBib3JkZXItdG9wOiAxcHggc29saWQgI2UyZjBjYjtcbiAgICBwYWRkaW5nLXRvcDogMi41JTtcbn1cbi5mb3JfaXRlbW1idG57XG4gICAgZm9udC1zaXplOiAxMXB4O1xuICAgIGNvbG9yOiAjNjc5NzMzO1xufVxuLmZvcl91bml0cXR5e1xuICAgIGZvbnQtc2l6ZTogMTFweDtcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuICAgIGNvbG9yOiAjOTg5YWEyO1xufVxuXG4ubXlIZWFydFVuU2F2ZXtcbiAgICBjb2xvcjogI2QzZGJjOTtcbn1cbi5teUhlYXJ0U2F2ZXtcbiAgICBjb2xvcjogcmVkO1xufVxuIiwiLmZvcl9pdGVtbWVzc2FnZSB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBmb250LXdlaWdodDogbm9ybWFsO1xuICBjb2xvcjogYmxhY2s7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgbWFyZ2luLXRvcDogM3B4O1xufVxuXG4ubWVzc2FnZSB7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG5cbi5idG5oZWFkZXIge1xuICBtYXJnaW4tdG9wOiAwLjUlO1xufVxuXG4uY2FyZF9oZWFkZXIge1xuICBwYWRkaW5nLWJvdHRvbTogMDtcbiAgcGFkZGluZy10b3A6IDdweDtcbn1cblxuLmNhcmRfdGl0bGUge1xuICBmb250LXNpemU6IDE1cHg7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG59XG5cbi5jYXJkX2NvbnRlbnQge1xuICBmb250LXNpemU6IDEzcHg7XG4gIHBhZGRpbmctdG9wOiA4cHg7XG4gIHBhZGRpbmctYm90dG9tOiAwcHg7XG4gIGxpbmUtaGVpZ2h0OiAxLjE7XG4gIGNvbG9yOiBibGFjaztcbn1cblxuLmNhcmRfdHh0IHtcbiAgcGFkZGluZy10b3A6IDEuMiU7XG4gIGNvbG9yOiAjNjc5NzMzO1xuICBmb250LXNpemU6IDEycHg7XG59XG5cbi5jYXJkX2hpZGUge1xuICBjb2xvcjogIzY3OTczMztcbiAgZm9udC1zaXplOiAxM3B4O1xufVxuXG4uZ290b0Fubm91bmNlIHtcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XG59XG5cbi5wcm9tb2NhcmQge1xuICBtYXJnaW4tdG9wOiAwO1xuICBiYWNrZ3JvdW5kOiAjZTJmMGNiO1xuICBib3gtc2hhZG93OiBub25lO1xuICBib3JkZXItcmFkaXVzOiAwO1xufVxuXG4ucHJvbW9oZWFkZXIge1xuICBwYWRkaW5nLXRvcDogMTBweDtcbiAgcGFkZGluZy1ib3R0b206IDBweDtcbiAgcGFkZGluZy1yaWdodDogMTNweDtcbiAgcGFkZGluZy1sZWZ0OiAxN3B4O1xufVxuXG4ucHJvbW9fc21hbGx0eHQge1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBjb2xvcjogIzY3OTczMztcbiAgZm9udC1zaXplOiAxM3B4O1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cblxuLnByb21vX2xhcmdldHh0IHtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgY29sb3I6IGJsYWNrO1xuICBmb250LXNpemU6IDI3cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkZXI7XG4gIG1hcmdpbi10b3A6IC00LjIlO1xufVxuXG4uYnRuc2hvcCB7XG4gIGZvbnQtc2l6ZTogMTFweDtcbiAgbWFyZ2luLXRvcDogMTAlO1xuICAtLWJvcmRlci1yYWRpdXM6IDNweDtcbn1cblxuaW9uLXNlbGVjdCB7XG4gIGZvbnQtc2l6ZTogMTNweDtcbn1cblxuLmZvcl9zb3J0ZmVlZCB7XG4gIHBhZGRpbmctbGVmdDogMDtcbiAgcGFkZGluZy10b3A6IDA7XG4gIG1hcmdpbi10b3A6IC01JTtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNlMmYwY2I7XG4gIHBhZGRpbmctYm90dG9tOiAwO1xuICBtYXJnaW4tYm90dG9tOiAzJTtcbn1cblxuLmZvcl9pdGVtcyB7XG4gIG1hcmdpbi10b3A6IDAlO1xuICBib3gtc2hhZG93OiAwIDFweCAxMHB4ICNlMmYwY2I7XG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNlMmYwY2I7XG4gIG1hcmdpbi1ib3R0b206IDA7XG59XG5cbi5mb3JfY2hpcCB7XG4gIC0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG4gIG1hcmdpbjogMDtcbiAgcGFkZGluZy1yaWdodDogMDtcbiAgcGFkZGluZy1sZWZ0OiAxNXB4O1xufVxuXG4uZm9yX2l0ZW1pbWcge1xuICBwYWRkaW5nLWxlZnQ6IDVweDtcbiAgbWFyZ2luOiBhdXRvO1xuICB3aWR0aDogOTAlO1xuICBoZWlnaHQ6IDcwJTtcbn1cblxuLmZvcl9wcmljZSB7XG4gIGZvbnQtc2l6ZTogMjBweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbiAgcGFkZGluZy10b3A6IDVweDtcbn1cblxuLmZvcl9pdGVtbmFtZSB7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbn1cblxuLmZvcl9pdGVtY2F0ZWcge1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBmb250LXNpemU6IDEycHg7XG4gIGZvbnQtd2VpZ2h0OiBsaWdodGVyO1xufVxuXG4uZm9yX2l0ZW1sb2NhdGlvbiB7XG4gIGZvbnQtc2l6ZTogMTNweDtcbiAgZm9udC13ZWlnaHQ6IGxpZ2h0ZXI7XG59XG5cbi5mb3JfcGluIHtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBtYXJnaW4tYm90dG9tOiAtMSU7XG4gIG1hcmdpbi1sZWZ0OiAtMSU7XG59XG5cbi5mb3JfaXRlbWJ0biB7XG4gIGZvbnQtc2l6ZTogMTFweDtcbn1cblxuLmZvcl9pdGVtYnRuaWNvbiB7XG4gIGZvbnQtc2l6ZTogMjBweDtcbiAgbWFyZ2luLWJvdHRvbTogLTclO1xuICBtYXJnaW4tcmlnaHQ6IDIlO1xufVxuXG4ubXktY3VzdG9tLWNsYXNzIC5hbGVydC13cmFwcGVyIHtcbiAgYmFja2dyb3VuZDogI2U1ZTVlNTtcbn1cblxuLmZvcl9pdGVtbXMge1xuICAtLXBhZGRpbmctc3RhcnQ6IDA7XG4gIG1hcmdpbi10b3A6IC04cHg7XG4gIC0taW5uZXItcGFkZGluZy1lbmQ6IDA7XG4gIC0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG59XG4uZm9yX2l0ZW1tcyBpb24tYXZhdGFyIHtcbiAgd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcbiAgaGVpZ2h0OiAxMDAlICFpbXBvcnRhbnQ7XG4gIG1heC13aWR0aDogMjVweCAhaW1wb3J0YW50O1xuICBtYXgtaGVpZ2h0OiAyNXB4ICFpbXBvcnRhbnQ7XG4gIG1hcmdpbi1yaWdodDogNXB4O1xuICBtYXJnaW4tdG9wOiAwO1xufVxuLmZvcl9pdGVtbXMgaW9uLWF2YXRhciBpbWcge1xuICBib3JkZXI6IDFweCBzb2xpZCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xuICB3aWR0aDogMTAwJSAhaW1wb3J0YW50O1xuICBoZWlnaHQ6IDEwMCUgIWltcG9ydGFudDtcbiAgbWF4LXdpZHRoOiAyNXB4ICFpbXBvcnRhbnQ7XG4gIG1heC1oZWlnaHQ6IDI1cHggIWltcG9ydGFudDtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XG4gIG1hcmdpbi10b3A6IDA7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbn1cblxuLmZvcl9uYW1lIHtcbiAgZm9udC1zaXplOiAxMnB4O1xuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbiAgbWFyZ2luLXRvcDogMDtcbiAgY29sb3I6IGJsYWNrO1xufVxuXG4uZm9yX2l0ZW1taW1nIHtcbiAgbWFyZ2luOiBhdXRvO1xuICB3aWR0aDogOTAlO1xuICBtYXJnaW4tdG9wOiAtMTBweDtcbiAgcGFkZGluZy1ib3R0b206IDVweDtcbn1cblxuLmZvcl9wcHJpY2Uge1xuICBmb250LXNpemU6IDIzcHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkZXI7XG59XG5cbi5mb3JfY2F0ZWdvcnkge1xuICBiYWNrZ3JvdW5kOiAjNzZjOTYxO1xuICBwYWRkaW5nOiAzcHg7XG4gIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiAxMHB4O1xuICBtYXJnaW4tdG9wOiAtNXB4O1xuICBtYXJnaW4tcmlnaHQ6IC01cHg7XG4gIGJvcmRlci1ib3R0b20tbGVmdC1yYWRpdXM6IDEwcHg7XG4gIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xuICBmb250LXNpemU6IDEycHg7XG4gIGZvbnQtd2VpZ2h0OiBsaWdodGVyO1xuICBjb2xvcjogd2hpdGU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmZvcl9pdGVtbW5hbWUge1xuICBmb250LXNpemU6IDE1cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkZXI7XG59XG5cbi5mb3JfaXRlbW1sb2NhdGlvbiB7XG4gIGZvbnQtc2l6ZTogMTIuNXB4O1xuICBmb250LXdlaWdodDogbGlnaHRlcjtcbiAgY29sb3I6ICM5ODlhYTI7XG4gIHBhZGRpbmctcmlnaHQ6IDVweDtcbiAgcGFkZGluZy1ib3R0b206IDIuNSU7XG59XG5cbi5mb3JfYm9yZGVyaXRlbSB7XG4gIGJvcmRlci10b3A6IDFweCBzb2xpZCAjZTJmMGNiO1xuICBwYWRkaW5nLXRvcDogMi41JTtcbn1cblxuLmZvcl9pdGVtbWJ0biB7XG4gIGZvbnQtc2l6ZTogMTFweDtcbiAgY29sb3I6ICM2Nzk3MzM7XG59XG5cbi5mb3JfdW5pdHF0eSB7XG4gIGZvbnQtc2l6ZTogMTFweDtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgY29sb3I6ICM5ODlhYTI7XG59XG5cbi5teUhlYXJ0VW5TYXZlIHtcbiAgY29sb3I6ICNkM2RiYzk7XG59XG5cbi5teUhlYXJ0U2F2ZSB7XG4gIGNvbG9yOiByZWQ7XG59Il19 */";
    /***/
  },

  /***/
  "./src/app/archieve/archieve.page.ts":
  /*!*******************************************!*\
    !*** ./src/app/archieve/archieve.page.ts ***!
    \*******************************************/

  /*! exports provided: ArchievePage */

  /***/
  function srcAppArchieveArchievePageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ArchievePage", function () {
      return ArchievePage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _providers_credential_provider__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../providers/credential-provider */
    "./src/providers/credential-provider.ts");
    /* harmony import */


    var _shared_model_item_model__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../shared/model/item.model */
    "./src/app/shared/model/item.model.ts");

    var ArchievePage = /*#__PURE__*/function () {
      function ArchievePage(router, postPvdr, storage, navCtrl) {
        _classCallCheck(this, ArchievePage);

        this.router = router;
        this.postPvdr = postPvdr;
        this.storage = storage;
        this.navCtrl = navCtrl;
        this.itemList = [];
        this.login_user_id = "";
      }

      _createClass(ArchievePage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.plotData();
        }
      }, {
        key: "plotData",
        value: function plotData() {
          var _this = this;

          this.storage.get('greenthumb_user_id').then(function (user_id) {
            _this.login_user_id = user_id;
            var body = {
              action: 'getArchivedItems',
              user_id: user_id
            };
            console.log(JSON.stringify(body));

            _this.postPvdr.postData(body, 'post_item.php').subscribe(function (data) {
              return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                var items, key;
                return regeneratorRuntime.wrap(function _callee$(_context) {
                  while (1) {
                    switch (_context.prev = _context.next) {
                      case 0:
                        if (data.success) {
                          items = []; //var pictureProfile: string = '';

                          for (key in data.result) {
                            // pictureProfile = (data.result[key].profile_photo == '') ? '' :
                            // this.postPvdr.myServer()+"/brixy-live/images/"+data.result[key].profile_photo;
                            items.push(new _shared_model_item_model__WEBPACK_IMPORTED_MODULE_6__["Item"](data.result[key].item_id, data.result[key].user_id, data.result[key].username, data.result[key].profile_photo == '' ? '' : this.postPvdr.myServer() + "/greenthumb/images/" + data.result[key].profile_photo, data.result[key].price, data.result[key].title, data.result[key].stocks_id, data.result[key].others_stock, parseInt(data.result[key].quantity), data.result[key].category, data.result[key].location, data.result[key].saveStatus, data.result[key].status, this.postPvdr.myServer() + "/greenthumb/images/posted_items/images/" + data.result[key].item_photo, data.result[key].user_rating, data.result[key].date, data.result[key].time));
                          }

                          this.itemList = items;
                        }

                      case 1:
                      case "end":
                        return _context.stop();
                    }
                  }
                }, _callee, this);
              }));
            });
          });
        }
      }, {
        key: "goProductview",
        value: function goProductview(item_id) {
          // this.router.navigate(['productview']);
          var navigationExtras = {
            queryParams: {
              item_id: item_id
            }
          };
          this.navCtrl.navigateForward(['productview'], navigationExtras);
        }
      }, {
        key: "followThis",
        value: function followThis(user_id) {
          var body = {
            action: 'follow',
            user_id: user_id,
            followed_by: this.login_user_id
          };
          console.log("follow:" + JSON.stringify(body));
          this.postPvdr.postData(body, 'followers.php').subscribe(function (data) {
            console.log(data);

            if (data.success) {// uncomment this when you add system notifications
              // this.followClass = "myHeartFollow";
              // this.status = !this.status; 
              // if(this.status){
              //   let body2 = {
              //     action : 'addNotification',
              //     user_id : this.user_id,
              //     followed_by: user_id,
              //     notification_message : "has followed you",
              //     status : this.status
              //   };
              //   this.postPvdr.postData(body2, 'system_notification.php').subscribe(data => {
              //   })
              // }
            }
          });
        }
      }, {
        key: "goBack",
        value: function goBack() {
          window.history.back();
        }
      }, {
        key: "saveThis",
        value: function saveThis(item_id) {
          console.log("gi saved lamang pud");
          var body = {
            action: 'saveItem',
            item_id: item_id,
            user_id: this.login_user_id
          };
          this.postPvdr.postData(body, 'save_item.php').subscribe(function (data) {
            if (data.success) {
              if (data.saveStatus == '1') {
                console.log('1 ko');
                var element = document.getElementById("class" + item_id);
                element.classList.remove("myHeartUnSave");
                element.classList.add("myHeartSave");
                console.log("classList:" + element.classList);
              } else {
                console.log("0 ko");
                var element = document.getElementById("class" + item_id);
                element.classList.remove("myHeartSave");
                element.classList.add("myHeartUnSave");
              }
            }
          });
        }
      }]);

      return ArchievePage;
    }();

    ArchievePage.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }, {
        type: _providers_credential_provider__WEBPACK_IMPORTED_MODULE_5__["PostProvider"]
      }, {
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"]
      }];
    };

    ArchievePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-archieve',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./archieve.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/archieve/archieve.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./archieve.page.scss */
      "./src/app/archieve/archieve.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _providers_credential_provider__WEBPACK_IMPORTED_MODULE_5__["PostProvider"], _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"]])], ArchievePage);
    /***/
  }
}]);
//# sourceMappingURL=archieve-archieve-module-es5.js.map