function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["categories-categories-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/categories/categories.page.html":
  /*!***************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/categories/categories.page.html ***!
    \***************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppCategoriesCategoriesPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header mode=\"ios\" no-border>\n  <ion-toolbar mode=\"ios\">\n      <ion-buttons slot=\"secondary\" (click)=\"goBack()\">\n        <ion-button mode=\"ios\" style=\"font-size: 15px;\">\n            Back\n        </ion-button>\n      </ion-buttons>\n\n      <ion-title color=\"secondary\">Categories</ion-title>\n    </ion-toolbar>\n</ion-header>\n<ion-content>\n\n\n\n  <ion-grid class=\"grid-categories\">\n    <ion-row *ngFor=\"let category of categoryList; let i = index\" style=\"padding: 5px;\" >\n      <ion-col (click)=\"selected(category.id, category.category)\" *ngIf=\"i % 2 == 0\">\n        <ion-card mode=\"ios\" class=\"for_card\">\n          <div *ngIf=\"!category.category_photo\">\n            <img src=\"assets/greenthumb-images/greenthumblogo.png\">\n          </div>\n          <div *ngIf=\"category.category_photo\">\n            <img [src] = \"category.category_photo\">\n          </div>\n          <ion-card-content class=\"for_cardcontent\">\n            {{category.category}}\n          </ion-card-content>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n  <ion-grid class=\"grid-categories\">\n    <ion-row *ngFor=\"let category of categoryList; let i = index\" style=\"padding: 5px;padding-top: 0;\">\n      <ion-col (click)=\"selected(category.id, category.category)\" *ngIf=\"i % 2 != 0\">\n        <ion-card mode=\"ios\" class=\"for_card\">\n          <div *ngIf=\"!category.category_photo\">\n            <img src=\"assets/greenthumb-images/greenthumblogo.png\">\n          </div>\n          <div *ngIf=\"category.category_photo\">\n            <img [src] = \"category.category_photo\">\n          </div>\n          <ion-card-content class=\"for_cardcontent\">\n            {{category.category}}\n          </ion-card-content>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n</ion-content>\n";
    /***/
  },

  /***/
  "./src/app/categories/categories-routing.module.ts":
  /*!*********************************************************!*\
    !*** ./src/app/categories/categories-routing.module.ts ***!
    \*********************************************************/

  /*! exports provided: CategoriesPageRoutingModule */

  /***/
  function srcAppCategoriesCategoriesRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CategoriesPageRoutingModule", function () {
      return CategoriesPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _categories_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./categories.page */
    "./src/app/categories/categories.page.ts");

    var routes = [{
      path: '',
      component: _categories_page__WEBPACK_IMPORTED_MODULE_3__["CategoriesPage"]
    }];

    var CategoriesPageRoutingModule = function CategoriesPageRoutingModule() {
      _classCallCheck(this, CategoriesPageRoutingModule);
    };

    CategoriesPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], CategoriesPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/categories/categories.module.ts":
  /*!*************************************************!*\
    !*** ./src/app/categories/categories.module.ts ***!
    \*************************************************/

  /*! exports provided: CategoriesPageModule */

  /***/
  function srcAppCategoriesCategoriesModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CategoriesPageModule", function () {
      return CategoriesPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _categories_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./categories-routing.module */
    "./src/app/categories/categories-routing.module.ts");
    /* harmony import */


    var _categories_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./categories.page */
    "./src/app/categories/categories.page.ts");

    var CategoriesPageModule = function CategoriesPageModule() {
      _classCallCheck(this, CategoriesPageModule);
    };

    CategoriesPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _categories_routing_module__WEBPACK_IMPORTED_MODULE_5__["CategoriesPageRoutingModule"]],
      declarations: [_categories_page__WEBPACK_IMPORTED_MODULE_6__["CategoriesPage"]]
    })], CategoriesPageModule);
    /***/
  },

  /***/
  "./src/app/categories/categories.page.scss":
  /*!*************************************************!*\
    !*** ./src/app/categories/categories.page.scss ***!
    \*************************************************/

  /*! exports provided: default */

  /***/
  function srcAppCategoriesCategoriesPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".for_card {\n  margin: 0;\n  border-radius: 5px;\n}\n\n.div_card {\n  background: #e2f0cb;\n}\n\n.for_divicon {\n  zoom: 8;\n}\n\n.for_cardcontent {\n  font-size: 14px;\n  color: #679733;\n  padding: 7px;\n  text-align: center;\n  background: #e2f0cb38;\n  border-top: 1px solid #e2f0cb;\n}\n\n.grid-categories {\n  width: 50%;\n  float: left;\n  padding: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZW5kcmljay9EZXNrdG9wL1RlbG1vIFNvbHV0aW9ucy9Gb3JQcm9kdWN0aW9uL21heTEwMjAyMS9sYXN0IHNldHVwL2dyZWVudGh1bWJhcHAgY29weS9zcmMvYXBwL2NhdGVnb3JpZXMvY2F0ZWdvcmllcy5wYWdlLnNjc3MiLCJzcmMvYXBwL2NhdGVnb3JpZXMvY2F0ZWdvcmllcy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxTQUFBO0VBQ0Esa0JBQUE7QUNDSjs7QURDQTtFQUNJLG1CQUFBO0FDRUo7O0FEQUE7RUFDSSxPQUFBO0FDR0o7O0FEREE7RUFDSSxlQUFBO0VBQ0EsY0FBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLHFCQUFBO0VBQ0EsNkJBQUE7QUNJSjs7QURGQTtFQUNJLFVBQUE7RUFDQSxXQUFBO0VBQ0EsVUFBQTtBQ0tKIiwiZmlsZSI6InNyYy9hcHAvY2F0ZWdvcmllcy9jYXRlZ29yaWVzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5mb3JfY2FyZHtcbiAgICBtYXJnaW46MDtcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XG59XG4uZGl2X2NhcmR7XG4gICAgYmFja2dyb3VuZDogI2UyZjBjYjtcbn1cbi5mb3JfZGl2aWNvbntcbiAgICB6b29tOiA4O1xufVxuLmZvcl9jYXJkY29udGVudHtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgY29sb3I6ICM2Nzk3MzM7XG4gICAgcGFkZGluZzogN3B4O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBiYWNrZ3JvdW5kOiAjZTJmMGNiMzg7XG4gICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICNlMmYwY2I7XG59XG4uZ3JpZC1jYXRlZ29yaWVzIHtcbiAgICB3aWR0aDogNTAlO1xuICAgIGZsb2F0OiBsZWZ0O1xuICAgIHBhZGRpbmc6IDA7XG59IiwiLmZvcl9jYXJkIHtcbiAgbWFyZ2luOiAwO1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG59XG5cbi5kaXZfY2FyZCB7XG4gIGJhY2tncm91bmQ6ICNlMmYwY2I7XG59XG5cbi5mb3JfZGl2aWNvbiB7XG4gIHpvb206IDg7XG59XG5cbi5mb3JfY2FyZGNvbnRlbnQge1xuICBmb250LXNpemU6IDE0cHg7XG4gIGNvbG9yOiAjNjc5NzMzO1xuICBwYWRkaW5nOiA3cHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgYmFja2dyb3VuZDogI2UyZjBjYjM4O1xuICBib3JkZXItdG9wOiAxcHggc29saWQgI2UyZjBjYjtcbn1cblxuLmdyaWQtY2F0ZWdvcmllcyB7XG4gIHdpZHRoOiA1MCU7XG4gIGZsb2F0OiBsZWZ0O1xuICBwYWRkaW5nOiAwO1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/categories/categories.page.ts":
  /*!***********************************************!*\
    !*** ./src/app/categories/categories.page.ts ***!
    \***********************************************/

  /*! exports provided: CategoriesPage */

  /***/
  function srcAppCategoriesCategoriesPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CategoriesPage", function () {
      return CategoriesPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var _providers_credential_provider__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../providers/credential-provider */
    "./src/providers/credential-provider.ts");
    /* harmony import */


    var _categoriessub_categoriessub_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../categoriessub/categoriessub.page */
    "./src/app/categoriessub/categoriessub.page.ts");
    /* harmony import */


    var _shared_model_category_model__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../shared/model/category.model */
    "./src/app/shared/model/category.model.ts");

    var CategoriesPage = /*#__PURE__*/function () {
      function CategoriesPage(router, modalCtrl, navCtrl, postPvdr, storage) {
        _classCallCheck(this, CategoriesPage);

        this.router = router;
        this.modalCtrl = modalCtrl;
        this.navCtrl = navCtrl;
        this.postPvdr = postPvdr;
        this.storage = storage;
        this.segment = 0;
        this.categoryList = [];
      }

      _createClass(CategoriesPage, [{
        key: "segmentChanged",
        value: function segmentChanged() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    _context.next = 2;
                    return this.slider.slideTo(this.segment);

                  case 2:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        }
      }, {
        key: "slideChanged",
        value: function slideChanged() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
            return regeneratorRuntime.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    _context2.next = 2;
                    return this.slider.getActiveIndex();

                  case 2:
                    this.segment = _context2.sent;

                  case 3:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this);
          }));
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "ionViewWillEnter",
        value: function ionViewWillEnter() {
          this.plotData();
        }
      }, {
        key: "plotData",
        value: function plotData() {
          var _this = this;

          this.storage.get('greenthumb_user_id').then(function (user_id) {
            _this.login_user_id = user_id;
            var body321 = {
              action: 'getCategoriesMain',
              user_id: user_id
            }; //console.log("storyalang:"+JSON.stringify(body321));

            _this.postPvdr.postData(body321, 'category.php').subscribe(function (data) {
              if (data.success) {
                var categoryList = [];

                for (var key in data.result) {
                  categoryList.push(new _shared_model_category_model__WEBPACK_IMPORTED_MODULE_7__["CategoryMain"](data.result[key].id, data.result[key].category_main, data.result[key].category_main_photo == '' ? '' : _this.postPvdr.myServer() + "/greenthumb/images/categories/main/" + data.result[key].category_main_photo));
                }

                _this.categoryList = categoryList;
              }
            });
          });
        }
      }, {
        key: "goBack",
        value: function goBack() {
          //this.router.navigate(['describeitem']);
          window.history.back();
        }
      }, {
        key: "selected",
        value: function selected(x, val) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
            var _this2 = this;

            var data, modal;
            return regeneratorRuntime.wrap(function _callee3$(_context3) {
              while (1) {
                switch (_context3.prev = _context3.next) {
                  case 0:
                    data = {
                      id: x,
                      val: val
                    };
                    _context3.next = 3;
                    return this.modalCtrl.create({
                      component: _categoriessub_categoriessub_page__WEBPACK_IMPORTED_MODULE_6__["CategoriessubPage"],
                      cssClass: 'categories',
                      id: 'modal21',
                      componentProps: {
                        data: data
                      }
                    });

                  case 3:
                    modal = _context3.sent;
                    modal.onDidDismiss().then(function (datafrom) {
                      // console.log("hamon2");
                      // console.log("iba:"+JSON.stringify(datafrom));  
                      // console.log("val:"+datafrom['data'].value);
                      //console.log("iba2dfd:"+JSON.stringify(data2)); 
                      //this.navCtrl.navigateForward(['tabs/tab1'], true, data2);
                      var navigationExtras = {
                        queryParams: {
                          category_id: datafrom['data'].id
                        }
                      };

                      _this2.navCtrl.navigateForward(['tabs/tab1'], navigationExtras);
                    });
                    _context3.next = 7;
                    return modal.present();

                  case 7:
                    return _context3.abrupt("return", _context3.sent);

                  case 8:
                  case "end":
                    return _context3.stop();
                }
              }
            }, _callee3, this);
          }));
        }
      }]);

      return CategoriesPage;
    }();

    CategoriesPage.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"]
      }, {
        type: _providers_credential_provider__WEBPACK_IMPORTED_MODULE_5__["PostProvider"]
      }, {
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"]
      }];
    };

    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('slides', {
      "static": true
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonSlides"])], CategoriesPage.prototype, "slider", void 0);
    CategoriesPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-categories',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./categories.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/categories/categories.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./categories.page.scss */
      "./src/app/categories/categories.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"], _providers_credential_provider__WEBPACK_IMPORTED_MODULE_5__["PostProvider"], _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"]])], CategoriesPage);
    /***/
  }
}]);
//# sourceMappingURL=categories-categories-module-es5.js.map