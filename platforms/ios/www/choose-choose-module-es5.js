function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["choose-choose-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/choose/choose.page.html":
  /*!*******************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/choose/choose.page.html ***!
    \*******************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppChooseChoosePageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-content class=\"choose-bg\" padding text-center fullscreen style=\"--overflow: hidden;--padding-bottom: 10px;--padding-top: 10px;--padding-start: 10px;--padding-end: 10px;\">\n  <ion-card class=\"for_card1\" style=\"border-radius: 0;margin: 0;box-shadow: none;--background: none;background-image: url(assets/greenthumb-images/greenthumblogo.png);background-position: center center;background-repeat: no-repeat;background-size: cover;\">\n  </ion-card>\n  <div class=\"for_card2\" style=\"display: flex; align-items: center; justify-content: center;margin-left: -2px;\n  margin-right: -2px;\">\n    <ion-card style=\"margin: 0;box-shadow: none;width: 100%;\">\n      <ion-list class=\"line-inputemail\">\n        <ion-row>\n          <ion-col>\n            <ion-item>\n              <ion-input mode=\"ios\" class=\"item_input\" type=\"email\" [(ngModel)]=\"login_email\" placeholder=\"Email Address\"></ion-input>\n              <ion-icon mode=\"ios\" class=\"item_icon\" name=\"mail\" slot=\"end\" align-self-end></ion-icon>\n            </ion-item>\n          </ion-col>\n        </ion-row>\n      </ion-list>\n      <ion-list class=\"line-inputpass\">\n        <ion-row>\n          <ion-col>\n            <ion-item>\n              <ion-input mode=\"ios\" class=\"item_input\" type=\"password\" [(ngModel)]=\"login_pass\" placeholder=\"Password\"></ion-input>\n              <ion-icon mode=\"ios\" class=\"item_icon\" name=\"lock\" slot=\"end\" align-self-end></ion-icon>\n            </ion-item>\n          </ion-col>\n        </ion-row>\n      </ion-list>\n      <p class=\"forgot-pass\" (click)=\"forgotpass()\" style=\"text-align: right;margin: 0;padding: 0;\">\n        Forgot Password?\n      </p><br>\n    <ion-button expand=\"block\" mode=\"ios\" (click)=\"bypassAuth()\" style=\"margin-bottom: 7%;margin-top: 4%;font-size: 14px;\">\n        SIGN UP/SIGN IN\n    </ion-button> \n    <ion-text mode=\"ios\" color=\"medium\" text-center>\n        <h6 mode=\"ios\" style=\"margin-bottom: 2.5%;margin-top: -15px;\">OR</h6>\n    </ion-text> \n    <ion-button expand=\"block\" mode=\"ios\" (click)=\"bypassAuth()\" style=\"margin-bottom: 3%;font-size: 14px;\">\n        <ion-icon slot=\"start\" name=\"logo-google\" mode=\"ios\"></ion-icon>   &nbsp;\n        SIGN UP WITH GOOGLE\n    </ion-button> \n    <ion-button expand=\"block\" mode=\"ios\" (click)=\"bypassAuth()\" style=\"margin-bottom: 3%;font-size: 14px;\">\n        <ion-icon slot=\"start\" name=\"logo-facebook\" mode=\"ios\"></ion-icon> &nbsp;\n        SIGN UP WITH FACEBOOK\n    </ion-button> \n    <ion-button expand=\"block\" mode=\"ios\" (click)=\"bypassAuth()\" style=\"margin-bottom: 3%;font-size: 14px;\">\n        <ion-icon slot=\"start\" name=\"mail\" mode=\"ios\"></ion-icon> &nbsp;\n        SIGN UP WITH SMS\n    </ion-button> \n    </ion-card>\n  </div>\n  \n  \n  <!-- <div>\n    <img src=\"assets/greenthumb-images/greenthumblogo.png\">\n    <ion-list class=\"line-inputemail\">\n        <ion-row>\n          <ion-col>\n            <ion-item>\n              <ion-input mode=\"ios\" class=\"item_input\" type=\"email\" [(ngModel)]=\"login_email\" placeholder=\"Email Address\"></ion-input>\n              <ion-icon mode=\"ios\" class=\"item_icon\" name=\"mail\" slot=\"end\" align-self-end></ion-icon>\n            </ion-item>\n          </ion-col>\n        </ion-row>\n      </ion-list>\n      <ion-list class=\"line-inputpass\">\n        <ion-row>\n          <ion-col>\n            <ion-item>\n              <ion-input mode=\"ios\" class=\"item_input\" type=\"password\" [(ngModel)]=\"login_pass\" placeholder=\"Password\"></ion-input>\n              <ion-icon mode=\"ios\" class=\"item_icon\" name=\"lock\" slot=\"end\" align-self-end></ion-icon>\n            </ion-item>\n          </ion-col>\n        </ion-row>\n      </ion-list>\n    <ion-text text-right color=\"medium\" class=\"forgot-pass\" (click)=\"forgotpass()\">Forgot Password?</ion-text> <br>\n    <ion-button expand=\"block\" mode=\"ios\" (click)=\"signUp()\" style=\"margin-bottom: 7%;margin-top: 4%;\">\n        SIGN UP/SIGN IN\n    </ion-button> \n    <ion-text mode=\"ios\" color=\"medium\" text-center>\n        <h6 mode=\"ios\" style=\"margin-bottom: 2.5%;margin-top: -15px;\">OR</h6>\n    </ion-text> \n    <ion-button expand=\"block\" mode=\"ios\" (click)=\"google()\" style=\"margin-bottom: 3%;\">\n        <ion-icon slot=\"start\" name=\"logo-google\" mode=\"ios\"></ion-icon>   &nbsp;\n        SIGN UP WITH GOOGLE\n    </ion-button> \n    <ion-button expand=\"block\" mode=\"ios\" (click)=\"facebook()\" style=\"margin-bottom: 3%;\">\n        <ion-icon slot=\"start\" name=\"logo-facebook\" mode=\"ios\"></ion-icon> &nbsp;\n        SIGN UP WITH FACEBOOK\n    </ion-button> \n    <ion-button expand=\"block\" mode=\"ios\" (click)=\"mobile()\" style=\"margin-bottom: 3%;\">\n        <ion-icon slot=\"start\" name=\"mail\" mode=\"ios\"></ion-icon> &nbsp;\n        SIGN UP WITH SMS\n    </ion-button> \n  </div> -->\n</ion-content>\n";
    /***/
  },

  /***/
  "./src/app/choose/choose.module.ts":
  /*!*****************************************!*\
    !*** ./src/app/choose/choose.module.ts ***!
    \*****************************************/

  /*! exports provided: ChoosePageModule */

  /***/
  function srcAppChooseChooseModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ChoosePageModule", function () {
      return ChoosePageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _choose_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./choose.page */
    "./src/app/choose/choose.page.ts");
    /* harmony import */


    var firebaseui_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! firebaseui-angular */
    "./node_modules/firebaseui-angular/fesm2015/firebaseui-angular.js");

    var routes = [{
      path: '',
      component: _choose_page__WEBPACK_IMPORTED_MODULE_6__["ChoosePage"]
    }];

    var ChoosePageModule = function ChoosePageModule() {
      _classCallCheck(this, ChoosePageModule);
    };

    ChoosePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], firebaseui_angular__WEBPACK_IMPORTED_MODULE_7__["FirebaseUIModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      declarations: [_choose_page__WEBPACK_IMPORTED_MODULE_6__["ChoosePage"]]
    })], ChoosePageModule);
    /***/
  },

  /***/
  "./src/app/choose/choose.page.scss":
  /*!*****************************************!*\
    !*** ./src/app/choose/choose.page.scss ***!
    \*****************************************/

  /*! exports provided: default */

  /***/
  function srcAppChooseChoosePageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".for_div {\n  position: fixed;\n  width: 100%;\n  z-index: 20;\n  bottom: 0%;\n  left: 0;\n  background: white;\n  padding: 15px;\n  padding-top: 0;\n}\n\n.for_card1 {\n  height: 50%;\n}\n\n.for_card2 {\n  height: 50%;\n}\n\n/* Smartphones (portrait and landscape) ----------- */\n\n@media only screen and (min-device-width: 320px) and (max-device-width: 480px) {\n  .for_card1 {\n    height: 38%;\n  }\n\n  .for_card2 {\n    height: 62%;\n  }\n}\n\n/* Smartphones (landscape) ----------- */\n\n@media only screen and (min-width: 321px) {\n  .for_card1 {\n    height: 38%;\n  }\n\n  .for_card2 {\n    height: 62%;\n  }\n}\n\n/* Smartphones (portrait) ----------- */\n\n@media only screen and (max-width: 320px) {\n  .for_card1 {\n    height: 38%;\n  }\n\n  .for_card2 {\n    height: 62%;\n  }\n}\n\n/* iPads (portrait and landscape) ----------- */\n\n@media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {\n  .for_card1 {\n    height: 38%;\n  }\n\n  .for_card2 {\n    height: 62%;\n  }\n}\n\n/* iPads (landscape) ----------- */\n\n@media only screen and (min-device-width: 768px) and (max-device-width: 1024px) and (orientation: landscape) {\n  .for_card1 {\n    height: 38%;\n  }\n\n  .for_card2 {\n    height: 62%;\n  }\n}\n\n/* iPads (portrait) ----------- */\n\n@media only screen and (min-device-width: 768px) and (max-device-width: 1024px) and (orientation: portrait) {\n  .for_card1 {\n    height: 57%;\n  }\n\n  .for_card2 {\n    height: 43%;\n  }\n}\n\n/* iPhone 5 (portrait &amp; landscape)----------- */\n\n@media only screen and (min-device-width: 320px) and (max-device-width: 568px) {\n  .for_card1 {\n    height: 38%;\n  }\n\n  .for_card2 {\n    height: 62%;\n  }\n}\n\n/* iPhone 5 (landscape)----------- */\n\n@media only screen and (min-device-width: 320px) and (max-device-width: 568px) and (orientation: landscape) {\n  .for_card1 {\n    height: 38%;\n  }\n\n  .for_card2 {\n    height: 62%;\n  }\n}\n\n/* iPhone 5 (portrait)----------- */\n\n@media only screen and (min-device-width: 320px) and (max-device-width: 568px) and (orientation: portrait) {\n  .for_card1 {\n    height: 41%;\n  }\n\n  .for_card2 {\n    height: 57%;\n  }\n}\n\n@media only screen and (device-width: 411px) and (device-height: 731px) and (orientation: portrait) {\n  .for_card1 {\n    height: 48%;\n  }\n\n  .for_card2 {\n    height: 51%;\n  }\n}\n\n@media only screen and (min-device-width: 414px) and (min-device-height: 736px) and (orientation: portrait) {\n  .for_card1 {\n    height: 48%;\n  }\n\n  .for_card2 {\n    height: 51%;\n  }\n}\n\n@media only screen and (device-width: 411px) and (device-height: 823px) and (orientation: portrait) {\n  .for_card1 {\n    height: 52%;\n  }\n\n  .for_card2 {\n    height: 47%;\n  }\n}\n\n@media only screen and (device-width: 375px) and (device-height: 812px) and (orientation: portrait) {\n  .for_card1 {\n    height: 52%;\n  }\n\n  .for_card2 {\n    height: 47%;\n  }\n}\n\n.line-inputemail {\n  margin-bottom: 0 !important;\n  padding-bottom: 0;\n}\n\n.line-inputemail ion-item {\n  --border-color: transparent!important;\n  --highlight-height: 0;\n  border: 1px solid #dedede;\n  border-radius: 4px;\n  height: 40px;\n}\n\n.line-inputpass {\n  margin-bottom: 0 !important;\n  padding-top: 0;\n}\n\n.line-inputpass ion-item {\n  --border-color: transparent!important;\n  --highlight-height: 0;\n  border: 1px solid #dedede;\n  border-radius: 4px;\n  height: 40px;\n}\n\n.item_input {\n  --padding-top: 0;\n  color: #424242 !important;\n  --padding-bottom: 10px;\n  font-size: 14px;\n}\n\n.item_icon {\n  margin-top: 0%;\n  margin-bottom: 5%;\n}\n\n.forgot-pass {\n  color: #989aa2;\n  font-size: 13px;\n  position: fixed;\n  right: 5%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZW5kcmljay9EZXNrdG9wL1RlbG1vIFNvbHV0aW9ucy9Gb3JQcm9kdWN0aW9uL21heTEwMjAyMS9sYXN0IHNldHVwL2dyZWVudGh1bWJhcHAgY29weS9zcmMvYXBwL2Nob29zZS9jaG9vc2UucGFnZS5zY3NzIiwic3JjL2FwcC9jaG9vc2UvY2hvb3NlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGVBQUE7RUFDQSxXQUFBO0VBQ0EsV0FBQTtFQUNBLFVBQUE7RUFDQSxPQUFBO0VBQ0EsaUJBQUE7RUFDQSxhQUFBO0VBQ0EsY0FBQTtBQ0NGOztBRENBO0VBQ0UsV0FBQTtBQ0VGOztBRERBO0VBQ0UsV0FBQTtBQ0lGOztBREZBLHFEQUFBOztBQUNBO0VBR0U7SUFBWSxXQUFBO0VDSVo7O0VESEE7SUFBWSxXQUFBO0VDT1o7QUFDRjs7QURMQSx3Q0FBQTs7QUFDQTtFQUVFO0lBQVksV0FBQTtFQ09aOztFRE5BO0lBQVksV0FBQTtFQ1VaO0FBQ0Y7O0FEUkEsdUNBQUE7O0FBQ0E7RUFFRTtJQUFZLFdBQUE7RUNVWjs7RURUQTtJQUFZLFdBQUE7RUNhWjtBQUNGOztBRFhBLCtDQUFBOztBQUNBO0VBR0U7SUFBWSxXQUFBO0VDWVo7O0VEWEE7SUFBWSxXQUFBO0VDZVo7QUFDRjs7QURiQSxrQ0FBQTs7QUFDQTtFQUlFO0lBQVksV0FBQTtFQ2FaOztFRFpBO0lBQVksV0FBQTtFQ2dCWjtBQUNGOztBRGRBLGlDQUFBOztBQUNBO0VBSUU7SUFBWSxXQUFBO0VDY1o7O0VEYkE7SUFBWSxXQUFBO0VDaUJaO0FBQ0Y7O0FEZkEsbURBQUE7O0FBQ0E7RUFHRTtJQUFZLFdBQUE7RUNnQlo7O0VEZkE7SUFBWSxXQUFBO0VDbUJaO0FBQ0Y7O0FEakJBLG9DQUFBOztBQUNBO0VBSUU7SUFBWSxXQUFBO0VDaUJaOztFRGhCQTtJQUFZLFdBQUE7RUNvQlo7QUFDRjs7QURsQkEsbUNBQUE7O0FBQ0E7RUFJRTtJQUFZLFdBQUE7RUNrQlo7O0VEakJBO0lBQVksV0FBQTtFQ3FCWjtBQUNGOztBRHBCQTtFQUlFO0lBQVksV0FBQTtFQ29CWjs7RURuQkE7SUFBWSxXQUFBO0VDdUJaO0FBQ0Y7O0FEdEJBO0VBSUU7SUFBWSxXQUFBO0VDc0JaOztFRHJCQTtJQUFZLFdBQUE7RUN5Qlo7QUFDRjs7QUR4QkE7RUFJRTtJQUFZLFdBQUE7RUN3Qlo7O0VEdkJBO0lBQVksV0FBQTtFQzJCWjtBQUNGOztBRDFCQTtFQUlFO0lBQVksV0FBQTtFQzBCWjs7RUR6QkE7SUFBWSxXQUFBO0VDNkJaO0FBQ0Y7O0FEMUJBO0VBQ0ksMkJBQUE7RUFDQSxpQkFBQTtBQzRCSjs7QUQzQkk7RUFDSSxxQ0FBQTtFQUNBLHFCQUFBO0VBQ0EseUJBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7QUM2QlI7O0FEMUJBO0VBQ0UsMkJBQUE7RUFDQSxjQUFBO0FDNkJGOztBRDVCRTtFQUNJLHFDQUFBO0VBQ0EscUJBQUE7RUFDQSx5QkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtBQzhCTjs7QUQzQkE7RUFDRSxnQkFBQTtFQUNBLHlCQUFBO0VBQ0Esc0JBQUE7RUFDQSxlQUFBO0FDOEJGOztBRDVCQTtFQUNFLGNBQUE7RUFDQSxpQkFBQTtBQytCRjs7QUQ3QkE7RUFDSSxjQUFBO0VBQ0EsZUFBQTtFQUNBLGVBQUE7RUFDQSxTQUFBO0FDZ0NKIiwiZmlsZSI6InNyYy9hcHAvY2hvb3NlL2Nob29zZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZm9yX2RpdntcbiAgcG9zaXRpb246IGZpeGVkO1xuICB3aWR0aDogMTAwJTtcbiAgei1pbmRleDogMjA7XG4gIGJvdHRvbTogMCU7XG4gIGxlZnQ6IDA7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBwYWRkaW5nOiAxNXB4O1xuICBwYWRkaW5nLXRvcDogMDtcbn1cbi5mb3JfY2FyZDF7XG4gIGhlaWdodDogNTAlO31cbi5mb3JfY2FyZDJ7XG4gIGhlaWdodDogNTAlO31cblxuLyogU21hcnRwaG9uZXMgKHBvcnRyYWl0IGFuZCBsYW5kc2NhcGUpIC0tLS0tLS0tLS0tICovXG5AbWVkaWEgb25seSBzY3JlZW5cbmFuZCAobWluLWRldmljZS13aWR0aCA6IDMyMHB4KVxuYW5kIChtYXgtZGV2aWNlLXdpZHRoIDogNDgwcHgpIHtcbiAgLmZvcl9jYXJkMXsgaGVpZ2h0OiAzOCU7IH1cbiAgLmZvcl9jYXJkMnsgaGVpZ2h0OiA2MiU7IH1cbn1cbiBcbi8qIFNtYXJ0cGhvbmVzIChsYW5kc2NhcGUpIC0tLS0tLS0tLS0tICovXG5AbWVkaWEgb25seSBzY3JlZW5cbmFuZCAobWluLXdpZHRoIDogMzIxcHgpIHtcbiAgLmZvcl9jYXJkMXsgaGVpZ2h0OiAzOCU7IH1cbiAgLmZvcl9jYXJkMnsgaGVpZ2h0OiA2MiU7IH1cbn1cbiBcbi8qIFNtYXJ0cGhvbmVzIChwb3J0cmFpdCkgLS0tLS0tLS0tLS0gKi9cbkBtZWRpYSBvbmx5IHNjcmVlblxuYW5kIChtYXgtd2lkdGggOiAzMjBweCkge1xuICAuZm9yX2NhcmQxeyBoZWlnaHQ6IDM4JTsgfVxuICAuZm9yX2NhcmQyeyBoZWlnaHQ6IDYyJTsgfVxufVxuIFxuLyogaVBhZHMgKHBvcnRyYWl0IGFuZCBsYW5kc2NhcGUpIC0tLS0tLS0tLS0tICovXG5AbWVkaWEgb25seSBzY3JlZW5cbmFuZCAobWluLWRldmljZS13aWR0aCA6IDc2OHB4KVxuYW5kIChtYXgtZGV2aWNlLXdpZHRoIDogMTAyNHB4KSB7XG4gIC5mb3JfY2FyZDF7IGhlaWdodDogMzglOyB9XG4gIC5mb3JfY2FyZDJ7IGhlaWdodDogNjIlOyB9XG59XG4gXG4vKiBpUGFkcyAobGFuZHNjYXBlKSAtLS0tLS0tLS0tLSAqL1xuQG1lZGlhIG9ubHkgc2NyZWVuXG5hbmQgKG1pbi1kZXZpY2Utd2lkdGggOiA3NjhweClcbmFuZCAobWF4LWRldmljZS13aWR0aCA6IDEwMjRweClcbmFuZCAob3JpZW50YXRpb24gOiBsYW5kc2NhcGUpIHtcbiAgLmZvcl9jYXJkMXsgaGVpZ2h0OiAzOCU7IH1cbiAgLmZvcl9jYXJkMnsgaGVpZ2h0OiA2MiU7IH1cbn1cbiBcbi8qIGlQYWRzIChwb3J0cmFpdCkgLS0tLS0tLS0tLS0gKi9cbkBtZWRpYSBvbmx5IHNjcmVlblxuYW5kIChtaW4tZGV2aWNlLXdpZHRoIDogNzY4cHgpXG5hbmQgKG1heC1kZXZpY2Utd2lkdGggOiAxMDI0cHgpXG5hbmQgKG9yaWVudGF0aW9uIDogcG9ydHJhaXQpIHtcbiAgLmZvcl9jYXJkMXsgaGVpZ2h0OiA1NyU7IH1cbiAgLmZvcl9jYXJkMnsgaGVpZ2h0OiA0MyU7IH1cbn1cbiBcbi8qIGlQaG9uZSA1IChwb3J0cmFpdCAmYW1wOyBsYW5kc2NhcGUpLS0tLS0tLS0tLS0gKi9cbkBtZWRpYSBvbmx5IHNjcmVlblxuYW5kIChtaW4tZGV2aWNlLXdpZHRoIDogMzIwcHgpXG5hbmQgKG1heC1kZXZpY2Utd2lkdGggOiA1NjhweCkge1xuICAuZm9yX2NhcmQxeyBoZWlnaHQ6IDM4JTsgfVxuICAuZm9yX2NhcmQyeyBoZWlnaHQ6IDYyJTsgfVxufVxuIFxuLyogaVBob25lIDUgKGxhbmRzY2FwZSktLS0tLS0tLS0tLSAqL1xuQG1lZGlhIG9ubHkgc2NyZWVuXG5hbmQgKG1pbi1kZXZpY2Utd2lkdGggOiAzMjBweClcbmFuZCAobWF4LWRldmljZS13aWR0aCA6IDU2OHB4KVxuYW5kIChvcmllbnRhdGlvbiA6IGxhbmRzY2FwZSkge1xuICAuZm9yX2NhcmQxeyBoZWlnaHQ6IDM4JTsgfVxuICAuZm9yX2NhcmQyeyBoZWlnaHQ6IDYyJTsgfVxufVxuIFxuLyogaVBob25lIDUgKHBvcnRyYWl0KS0tLS0tLS0tLS0tICovXG5AbWVkaWEgb25seSBzY3JlZW5cbmFuZCAobWluLWRldmljZS13aWR0aCA6IDMyMHB4KVxuYW5kIChtYXgtZGV2aWNlLXdpZHRoIDogNTY4cHgpXG5hbmQgKG9yaWVudGF0aW9uIDogcG9ydHJhaXQpIHtcbiAgLmZvcl9jYXJkMXsgaGVpZ2h0OiA0MSU7IH1cbiAgLmZvcl9jYXJkMnsgaGVpZ2h0OiA1NyU7IH1cbn1cbkBtZWRpYSBvbmx5IHNjcmVlblxuYW5kIChkZXZpY2Utd2lkdGggOiA0MTFweClcbmFuZCAoZGV2aWNlLWhlaWdodCA6IDczMXB4KVxuYW5kIChvcmllbnRhdGlvbiA6IHBvcnRyYWl0KSB7XG4gIC5mb3JfY2FyZDF7IGhlaWdodDogNDglOyB9XG4gIC5mb3JfY2FyZDJ7IGhlaWdodDogNTElOyB9XG59XG5AbWVkaWEgb25seSBzY3JlZW5cbmFuZCAobWluLWRldmljZS13aWR0aCA6IDQxNHB4KVxuYW5kIChtaW4tZGV2aWNlLWhlaWdodCA6IDczNnB4KVxuYW5kIChvcmllbnRhdGlvbiA6IHBvcnRyYWl0KSB7XG4gIC5mb3JfY2FyZDF7IGhlaWdodDogNDglOyB9XG4gIC5mb3JfY2FyZDJ7IGhlaWdodDogNTElOyB9XG59XG5AbWVkaWEgb25seSBzY3JlZW5cbmFuZCAoZGV2aWNlLXdpZHRoIDogNDExcHgpXG5hbmQgKGRldmljZS1oZWlnaHQgOiA4MjNweClcbmFuZCAob3JpZW50YXRpb24gOiBwb3J0cmFpdCkge1xuICAuZm9yX2NhcmQxeyBoZWlnaHQ6IDUyJTsgfVxuICAuZm9yX2NhcmQyeyBoZWlnaHQ6IDQ3JTsgfVxufVxuQG1lZGlhIG9ubHkgc2NyZWVuXG5hbmQgKGRldmljZS13aWR0aCA6IDM3NXB4KVxuYW5kIChkZXZpY2UtaGVpZ2h0IDogODEycHgpXG5hbmQgKG9yaWVudGF0aW9uIDogcG9ydHJhaXQpIHtcbiAgLmZvcl9jYXJkMXsgaGVpZ2h0OiA1MiU7IH1cbiAgLmZvcl9jYXJkMnsgaGVpZ2h0OiA0NyU7IH1cbn1cblxuICBcbi5saW5lLWlucHV0ZW1haWwge1xuICAgIG1hcmdpbi1ib3R0b206IDAhaW1wb3J0YW50O1xuICAgIHBhZGRpbmctYm90dG9tOiAwO1xuICAgIGlvbi1pdGVtIHtcbiAgICAgICAgLS1ib3JkZXItY29sb3I6IHRyYW5zcGFyZW50IWltcG9ydGFudDtcbiAgICAgICAgLS1oaWdobGlnaHQtaGVpZ2h0OiAwO1xuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjZGVkZWRlO1xuICAgICAgICBib3JkZXItcmFkaXVzOiA0cHg7XG4gICAgICAgIGhlaWdodDogNDBweDtcbiAgICB9XG59XG4ubGluZS1pbnB1dHBhc3Mge1xuICBtYXJnaW4tYm90dG9tOiAwIWltcG9ydGFudDtcbiAgcGFkZGluZy10b3A6IDA7XG4gIGlvbi1pdGVtIHtcbiAgICAgIC0tYm9yZGVyLWNvbG9yOiB0cmFuc3BhcmVudCFpbXBvcnRhbnQ7XG4gICAgICAtLWhpZ2hsaWdodC1oZWlnaHQ6IDA7XG4gICAgICBib3JkZXI6IDFweCBzb2xpZCAjZGVkZWRlO1xuICAgICAgYm9yZGVyLXJhZGl1czogNHB4O1xuICAgICAgaGVpZ2h0OiA0MHB4O1xuICB9XG59XG4uaXRlbV9pbnB1dHtcbiAgLS1wYWRkaW5nLXRvcDogMDtcbiAgY29sb3I6ICM0MjQyNDIgIWltcG9ydGFudDtcbiAgLS1wYWRkaW5nLWJvdHRvbTogMTBweDtcbiAgZm9udC1zaXplOiAxNHB4O1xufVxuLml0ZW1faWNvbntcbiAgbWFyZ2luLXRvcDogMCU7XG4gIG1hcmdpbi1ib3R0b206IDUlO1xufVxuLmZvcmdvdC1wYXNze1xuICAgIGNvbG9yOiAjOTg5YWEyO1xuICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgICBwb3NpdGlvbjogZml4ZWQ7XG4gICAgcmlnaHQ6IDUlO1xufVxuLy8gLmZvcl9kaXZ7XG4vLyAgIHBvc2l0aW9uOiBmaXhlZDtcbi8vICAgd2lkdGg6IDEwMCU7XG4vLyAgIGhlaWdodDogNDE1cHg7XG4vLyAgIHotaW5kZXg6IDIwO1xuLy8gICBib3R0b206IDAlO1xuLy8gICBsZWZ0OiAwO1xuLy8gICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbi8vICAgcGFkZGluZzogMTVweDtcbi8vICAgcGFkZGluZy10b3A6IDA7XG4vLyB9IiwiLmZvcl9kaXYge1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIHdpZHRoOiAxMDAlO1xuICB6LWluZGV4OiAyMDtcbiAgYm90dG9tOiAwJTtcbiAgbGVmdDogMDtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIHBhZGRpbmc6IDE1cHg7XG4gIHBhZGRpbmctdG9wOiAwO1xufVxuXG4uZm9yX2NhcmQxIHtcbiAgaGVpZ2h0OiA1MCU7XG59XG5cbi5mb3JfY2FyZDIge1xuICBoZWlnaHQ6IDUwJTtcbn1cblxuLyogU21hcnRwaG9uZXMgKHBvcnRyYWl0IGFuZCBsYW5kc2NhcGUpIC0tLS0tLS0tLS0tICovXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4tZGV2aWNlLXdpZHRoOiAzMjBweCkgYW5kIChtYXgtZGV2aWNlLXdpZHRoOiA0ODBweCkge1xuICAuZm9yX2NhcmQxIHtcbiAgICBoZWlnaHQ6IDM4JTtcbiAgfVxuXG4gIC5mb3JfY2FyZDIge1xuICAgIGhlaWdodDogNjIlO1xuICB9XG59XG4vKiBTbWFydHBob25lcyAobGFuZHNjYXBlKSAtLS0tLS0tLS0tLSAqL1xuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoOiAzMjFweCkge1xuICAuZm9yX2NhcmQxIHtcbiAgICBoZWlnaHQ6IDM4JTtcbiAgfVxuXG4gIC5mb3JfY2FyZDIge1xuICAgIGhlaWdodDogNjIlO1xuICB9XG59XG4vKiBTbWFydHBob25lcyAocG9ydHJhaXQpIC0tLS0tLS0tLS0tICovXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDMyMHB4KSB7XG4gIC5mb3JfY2FyZDEge1xuICAgIGhlaWdodDogMzglO1xuICB9XG5cbiAgLmZvcl9jYXJkMiB7XG4gICAgaGVpZ2h0OiA2MiU7XG4gIH1cbn1cbi8qIGlQYWRzIChwb3J0cmFpdCBhbmQgbGFuZHNjYXBlKSAtLS0tLS0tLS0tLSAqL1xuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLWRldmljZS13aWR0aDogNzY4cHgpIGFuZCAobWF4LWRldmljZS13aWR0aDogMTAyNHB4KSB7XG4gIC5mb3JfY2FyZDEge1xuICAgIGhlaWdodDogMzglO1xuICB9XG5cbiAgLmZvcl9jYXJkMiB7XG4gICAgaGVpZ2h0OiA2MiU7XG4gIH1cbn1cbi8qIGlQYWRzIChsYW5kc2NhcGUpIC0tLS0tLS0tLS0tICovXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4tZGV2aWNlLXdpZHRoOiA3NjhweCkgYW5kIChtYXgtZGV2aWNlLXdpZHRoOiAxMDI0cHgpIGFuZCAob3JpZW50YXRpb246IGxhbmRzY2FwZSkge1xuICAuZm9yX2NhcmQxIHtcbiAgICBoZWlnaHQ6IDM4JTtcbiAgfVxuXG4gIC5mb3JfY2FyZDIge1xuICAgIGhlaWdodDogNjIlO1xuICB9XG59XG4vKiBpUGFkcyAocG9ydHJhaXQpIC0tLS0tLS0tLS0tICovXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4tZGV2aWNlLXdpZHRoOiA3NjhweCkgYW5kIChtYXgtZGV2aWNlLXdpZHRoOiAxMDI0cHgpIGFuZCAob3JpZW50YXRpb246IHBvcnRyYWl0KSB7XG4gIC5mb3JfY2FyZDEge1xuICAgIGhlaWdodDogNTclO1xuICB9XG5cbiAgLmZvcl9jYXJkMiB7XG4gICAgaGVpZ2h0OiA0MyU7XG4gIH1cbn1cbi8qIGlQaG9uZSA1IChwb3J0cmFpdCAmYW1wOyBsYW5kc2NhcGUpLS0tLS0tLS0tLS0gKi9cbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi1kZXZpY2Utd2lkdGg6IDMyMHB4KSBhbmQgKG1heC1kZXZpY2Utd2lkdGg6IDU2OHB4KSB7XG4gIC5mb3JfY2FyZDEge1xuICAgIGhlaWdodDogMzglO1xuICB9XG5cbiAgLmZvcl9jYXJkMiB7XG4gICAgaGVpZ2h0OiA2MiU7XG4gIH1cbn1cbi8qIGlQaG9uZSA1IChsYW5kc2NhcGUpLS0tLS0tLS0tLS0gKi9cbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi1kZXZpY2Utd2lkdGg6IDMyMHB4KSBhbmQgKG1heC1kZXZpY2Utd2lkdGg6IDU2OHB4KSBhbmQgKG9yaWVudGF0aW9uOiBsYW5kc2NhcGUpIHtcbiAgLmZvcl9jYXJkMSB7XG4gICAgaGVpZ2h0OiAzOCU7XG4gIH1cblxuICAuZm9yX2NhcmQyIHtcbiAgICBoZWlnaHQ6IDYyJTtcbiAgfVxufVxuLyogaVBob25lIDUgKHBvcnRyYWl0KS0tLS0tLS0tLS0tICovXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4tZGV2aWNlLXdpZHRoOiAzMjBweCkgYW5kIChtYXgtZGV2aWNlLXdpZHRoOiA1NjhweCkgYW5kIChvcmllbnRhdGlvbjogcG9ydHJhaXQpIHtcbiAgLmZvcl9jYXJkMSB7XG4gICAgaGVpZ2h0OiA0MSU7XG4gIH1cblxuICAuZm9yX2NhcmQyIHtcbiAgICBoZWlnaHQ6IDU3JTtcbiAgfVxufVxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAoZGV2aWNlLXdpZHRoOiA0MTFweCkgYW5kIChkZXZpY2UtaGVpZ2h0OiA3MzFweCkgYW5kIChvcmllbnRhdGlvbjogcG9ydHJhaXQpIHtcbiAgLmZvcl9jYXJkMSB7XG4gICAgaGVpZ2h0OiA0OCU7XG4gIH1cblxuICAuZm9yX2NhcmQyIHtcbiAgICBoZWlnaHQ6IDUxJTtcbiAgfVxufVxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLWRldmljZS13aWR0aDogNDE0cHgpIGFuZCAobWluLWRldmljZS1oZWlnaHQ6IDczNnB4KSBhbmQgKG9yaWVudGF0aW9uOiBwb3J0cmFpdCkge1xuICAuZm9yX2NhcmQxIHtcbiAgICBoZWlnaHQ6IDQ4JTtcbiAgfVxuXG4gIC5mb3JfY2FyZDIge1xuICAgIGhlaWdodDogNTElO1xuICB9XG59XG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChkZXZpY2Utd2lkdGg6IDQxMXB4KSBhbmQgKGRldmljZS1oZWlnaHQ6IDgyM3B4KSBhbmQgKG9yaWVudGF0aW9uOiBwb3J0cmFpdCkge1xuICAuZm9yX2NhcmQxIHtcbiAgICBoZWlnaHQ6IDUyJTtcbiAgfVxuXG4gIC5mb3JfY2FyZDIge1xuICAgIGhlaWdodDogNDclO1xuICB9XG59XG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChkZXZpY2Utd2lkdGg6IDM3NXB4KSBhbmQgKGRldmljZS1oZWlnaHQ6IDgxMnB4KSBhbmQgKG9yaWVudGF0aW9uOiBwb3J0cmFpdCkge1xuICAuZm9yX2NhcmQxIHtcbiAgICBoZWlnaHQ6IDUyJTtcbiAgfVxuXG4gIC5mb3JfY2FyZDIge1xuICAgIGhlaWdodDogNDclO1xuICB9XG59XG4ubGluZS1pbnB1dGVtYWlsIHtcbiAgbWFyZ2luLWJvdHRvbTogMCAhaW1wb3J0YW50O1xuICBwYWRkaW5nLWJvdHRvbTogMDtcbn1cbi5saW5lLWlucHV0ZW1haWwgaW9uLWl0ZW0ge1xuICAtLWJvcmRlci1jb2xvcjogdHJhbnNwYXJlbnQhaW1wb3J0YW50O1xuICAtLWhpZ2hsaWdodC1oZWlnaHQ6IDA7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNkZWRlZGU7XG4gIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgaGVpZ2h0OiA0MHB4O1xufVxuXG4ubGluZS1pbnB1dHBhc3Mge1xuICBtYXJnaW4tYm90dG9tOiAwICFpbXBvcnRhbnQ7XG4gIHBhZGRpbmctdG9wOiAwO1xufVxuLmxpbmUtaW5wdXRwYXNzIGlvbi1pdGVtIHtcbiAgLS1ib3JkZXItY29sb3I6IHRyYW5zcGFyZW50IWltcG9ydGFudDtcbiAgLS1oaWdobGlnaHQtaGVpZ2h0OiAwO1xuICBib3JkZXI6IDFweCBzb2xpZCAjZGVkZWRlO1xuICBib3JkZXItcmFkaXVzOiA0cHg7XG4gIGhlaWdodDogNDBweDtcbn1cblxuLml0ZW1faW5wdXQge1xuICAtLXBhZGRpbmctdG9wOiAwO1xuICBjb2xvcjogIzQyNDI0MiAhaW1wb3J0YW50O1xuICAtLXBhZGRpbmctYm90dG9tOiAxMHB4O1xuICBmb250LXNpemU6IDE0cHg7XG59XG5cbi5pdGVtX2ljb24ge1xuICBtYXJnaW4tdG9wOiAwJTtcbiAgbWFyZ2luLWJvdHRvbTogNSU7XG59XG5cbi5mb3Jnb3QtcGFzcyB7XG4gIGNvbG9yOiAjOTg5YWEyO1xuICBmb250LXNpemU6IDEzcHg7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgcmlnaHQ6IDUlO1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/choose/choose.page.ts":
  /*!***************************************!*\
    !*** ./src/app/choose/choose.page.ts ***!
    \***************************************/

  /*! exports provided: ChoosePage */

  /***/
  function srcAppChooseChoosePageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ChoosePage", function () {
      return ChoosePage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/fire/auth */
    "./node_modules/@angular/fire/auth/es2015/index.js");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _providers_credential_provider__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../../providers/credential-provider */
    "./src/providers/credential-provider.ts");
    /* harmony import */


    var _userpolicy_userpolicy_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../userpolicy/userpolicy.page */
    "./src/app/userpolicy/userpolicy.page.ts");
    /* harmony import */


    var _suspendmodal_suspendmodal_page__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ../suspendmodal/suspendmodal.page */
    "./src/app/suspendmodal/suspendmodal.page.ts");

    var ChoosePage = /*#__PURE__*/function () {
      function ChoosePage(router, alertCtrl, loadingCtrl, toastController, afAuth, modalController, postPvdr, storage) {
        _classCallCheck(this, ChoosePage);

        this.router = router;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.toastController = toastController;
        this.afAuth = afAuth;
        this.modalController = modalController;
        this.postPvdr = postPvdr;
        this.storage = storage;
        this.user = {};
        this.ui = null;
      }

      _createClass(ChoosePage, [{
        key: "loading",
        value: function loading() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    _context.next = 2;
                    return this.loadingCtrl.create({
                      message: '<img src="assets/greenthumb-images/greenthumblogo.png" style="height: 25px" height="auto" alt="loading...">',
                      translucent: true,
                      showBackdrop: false,
                      spinner: null,
                      duration: 2000
                    });

                  case 2:
                    this.loader = _context.sent;
                    this.loader.present();

                  case 4:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        }
      }, {
        key: "presentToastFailed",
        value: function presentToastFailed() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
            var toast;
            return regeneratorRuntime.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    _context2.next = 2;
                    return this.toastController.create({
                      message: 'Login Failed.',
                      duration: 3000
                    });

                  case 2:
                    toast = _context2.sent;
                    toast.present();

                  case 4:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this);
          }));
        }
      }, {
        key: "prepareToast",
        value: function prepareToast() {
          var _this = this;

          if (this.userData.first_name === null || this.userData.first_name === undefined || this.userData.first_name === 'null') {
            //this.presentToastPreGoogle();
            // let element: HTMLElement = document.getElementsByClassName('firebaseui-idp-google')[0] as HTMLElement;
            // element.click();
            this.loader.dismiss();
          } else {
            this.storage.set('email', this.userData.email);
            this.storage.set('login_used', "google");
            this.storage.set('fname', this.userData.first_name);
            var body = {
              action: 'getUserId',
              email: this.userData.email,
              login_type: 2
            };
            this.postPvdr.postData(body, 'credentials-api.php').subscribe(function (data) {
              if (data.success) {
                _this.storage.set("greenthumb_user_id", data.user_id);

                _this.login_user_id = data.user_id;
                console.log("user_id: google used:" + data.user_id);

                _this.loader.dismiss();

                _this.presentToast();

                _this.validateNotNow();
              }
            });
          }
        }
      }, {
        key: "presentToast",
        value: function presentToast() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
            var toast;
            return regeneratorRuntime.wrap(function _callee3$(_context3) {
              while (1) {
                switch (_context3.prev = _context3.next) {
                  case 0:
                    _context3.next = 2;
                    return this.toastController.create({
                      message: 'Welcome to Greenthumb, ' + this.userData.first_name + '!',
                      duration: 3000
                    });

                  case 2:
                    toast = _context3.sent;
                    toast.present();

                  case 4:
                  case "end":
                    return _context3.stop();
                }
              }
            }, _callee3, this);
          }));
        } // async presentToastPreGoogle() {
        //   const toast = await this.toastController.create({
        //     message: 'You can now login using Google. Please tap again the button.',
        //     duration: 5000
        //   });
        //   toast.present();
        // }

      }, {
        key: "presentAlertnoFbEmail",
        value: function presentAlertnoFbEmail() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
            var alert;
            return regeneratorRuntime.wrap(function _callee4$(_context4) {
              while (1) {
                switch (_context4.prev = _context4.next) {
                  case 0:
                    _context4.next = 2;
                    return this.alertCtrl.create({
                      message: 'An error occurred during the sign-up registration of your Facebook account. Please verify your email in Facebook, or login through Google or SMS to register.',
                      subHeader: 'Verify email in Facebook.',
                      buttons: ['OK']
                    });

                  case 2:
                    alert = _context4.sent;
                    _context4.next = 5;
                    return alert.present();

                  case 5:
                  case "end":
                    return _context4.stop();
                }
              }
            }, _callee4, this);
          }));
        }
      }, {
        key: "bypassAuth",
        value: function bypassAuth() {
          this.router.navigate(["/tabs"]);
        }
      }, {
        key: "validateNotNow",
        value: function validateNotNow() {
          var _this2 = this;

          var body = {
            action: 'getNotNow',
            user_id: this.login_user_id
          };
          this.postPvdr.postData(body, 'user.php').subscribe(function (data) {
            if (data.success) {
              if (parseInt(data.suspended) == 3) {
                _this2.suspendModal();
              } else {
                _this2.router.navigate(["/tabs"]);
              }
            }
          });
        }
      }, {
        key: "suspendModal",
        value: function suspendModal() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
            var modal;
            return regeneratorRuntime.wrap(function _callee5$(_context5) {
              while (1) {
                switch (_context5.prev = _context5.next) {
                  case 0:
                    _context5.next = 2;
                    return this.modalController.create({
                      component: _suspendmodal_suspendmodal_page__WEBPACK_IMPORTED_MODULE_8__["SuspendmodalPage"],
                      cssClass: ''
                    });

                  case 2:
                    modal = _context5.sent;
                    _context5.next = 5;
                    return modal.present();

                  case 5:
                    return _context5.abrupt("return", _context5.sent);

                  case 6:
                  case "end":
                    return _context5.stop();
                }
              }
            }, _callee5, this);
          }));
        }
      }, {
        key: "goToUserPolicy",
        value: function goToUserPolicy() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee6() {
            var modal;
            return regeneratorRuntime.wrap(function _callee6$(_context6) {
              while (1) {
                switch (_context6.prev = _context6.next) {
                  case 0:
                    _context6.next = 2;
                    return this.modalController.create({
                      component: _userpolicy_userpolicy_page__WEBPACK_IMPORTED_MODULE_7__["UserpolicyPage"],
                      cssClass: ''
                    });

                  case 2:
                    modal = _context6.sent;
                    _context6.next = 5;
                    return modal.present();

                  case 5:
                    return _context6.abrupt("return", _context6.sent);

                  case 6:
                  case "end":
                    return _context6.stop();
                }
              }
            }, _callee6, this);
          }));
        }
      }, {
        key: "forgotpass",
        value: function forgotpass() {
          this.router.navigate(['forgotpassword']);
        }
      }, {
        key: "mobile",
        value: function mobile() {
          this.router.navigate(['/mobilee']);
        } // emailLogin(){
        //   this.router.navigate(['/email-login']);
        // }

      }, {
        key: "signUp",
        value: function signUp() {
          var _this3 = this;

          console.log("email:" + this.login_email); // console.log("password:"+this.password);

          var body = {
            action: 'emailLogin',
            email: this.login_email
          };
          console.log("bodybodykoko:" + JSON.stringify(body));
          this.postPvdr.postData(body, 'credentials-api.php').subscribe(function (data) {
            if (data.success) {
              _this3.storage.set("greenthumb_user_id", data.user_id); // console.log("user_id:"+data.user_id);
              // console.log("email_code:"+data.email_code);


              var body2 = {
                action: 'send-email-code',
                sentTo: _this3.login_email,
                sentToCode: data.email_code
              };
              console.log("bodyisda:" + JSON.stringify(body2));

              _this3.postPvdr.sendCode(body2).subscribe(function (data2) {
                //console.log("status:"+data.email_code);
                _this3.router.navigate(['/email-code', data.email_code]);
              });
            }
          });
        } // toGuest(){
        //   this.router.navigate(['tabs']);
        // }

      }, {
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this4 = this;

          // this.afAuth.auth.signOut();
          this.storage.get("greenthumb_user_id").then(function (user_id) {
            console.log("user_id in choose:" + user_id);

            if (user_id != null) {
              _this4.presentToast();

              _this4.router.navigate(["/tabs"]);
            }
          });
        }
      }]);

      return ChoosePage;
    }();

    ChoosePage.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["LoadingController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ToastController"]
      }, {
        type: _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__["AngularFireAuth"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ModalController"]
      }, {
        type: _providers_credential_provider__WEBPACK_IMPORTED_MODULE_6__["PostProvider"]
      }, {
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"]
      }];
    };

    ChoosePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-choose',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./choose.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/choose/choose.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./choose.page.scss */
      "./src/app/choose/choose.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["LoadingController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ToastController"], _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__["AngularFireAuth"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ModalController"], _providers_credential_provider__WEBPACK_IMPORTED_MODULE_6__["PostProvider"], _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"]])], ChoosePage);
    /***/
  }
}]);
//# sourceMappingURL=choose-choose-module-es5.js.map