(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["describeitem-describeitem-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/describeitem/describeitem.page.html":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/describeitem/describeitem.page.html ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header mode=\"ios\" no-border>\n  <ion-toolbar mode=\"ios\">\n      <ion-buttons slot=\"secondary\" (click)=\"goBack()\">\n        <ion-button mode=\"ios\" style=\"font-size: 15px;\">\n            Back\n        </ion-button>\n      </ion-buttons>\n\n      <ion-title color=\"secondary\">Describe your item</ion-title>\n\n      <ion-buttons slot=\"primary\" (click)=\"goHelp()\">\n        <ion-button mode=\"ios\" style=\"font-size: 15px;\">\n            Help\n        </ion-button>\n      </ion-buttons>\n    </ion-toolbar>\n</ion-header>\n<ion-content padding>\n    <ion-list class=\"line-inputcateg\">\n      <ion-row>\n        <ion-col no-padding>\n          <ion-row (click)=\"goCategories()\">\n            <ion-col text-left no-padding>\n              <ion-label class=\"item_label\" color=\"secondary\">Select Category</ion-label>\n            </ion-col>\n            <ion-col text-right no-padding>\n              <ion-label class=\"item_label2\">Categories <ion-icon class=\"item_icon\" name=\"arrow-forward\" mode=\"ios\"></ion-icon></ion-label>\n            </ion-col>\n          </ion-row>\n          <ion-item>\n            <ion-input mode=\"ios\" type=\"text\" readonly [(ngModel)]=\"category_name\" class=\"item_input\" placeholder=\"Enter tag here\" size=\"small\"></ion-input>\n          </ion-item>\n\n          <ion-item>\n            <ion-input mode=\"ios\" type=\"text\" readonly [(ngModel)]=\"custom_category\" class=\"item_input\" placeholder=\"Enter custom category\" size=\"small\"></ion-input>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n    </ion-list>\n    <ion-list class=\"line-input\">\n      <ion-label class=\"item_label\" color=\"secondary\">STOCKS AVAILABLE</ion-label>\n      <ion-radio-group mode=\"ios\" value=\"{{stocks_id}}\" name=\"stocks_id\">\n         <ion-item style=\"margin-top: -1%;--background:transparent\" (click)=\"changeStock(1)\">\n          <ion-radio style=\"zoom: 0.8;\" value=\"1\" ></ion-radio>&nbsp;&nbsp;&nbsp;&nbsp;\n          <ion-label style=\"font-size: 13px;\">per piece</ion-label>\n         </ion-item>\n         <ion-item (click)=\"changeStock(2)\">\n          <ion-radio style=\"zoom: 0.8;\" value=\"2\" ></ion-radio>&nbsp;&nbsp;&nbsp;&nbsp;\n          <ion-label style=\"font-size: 13px;\">per pound</ion-label>\n         </ion-item>\n         <ion-item (click)=\"changeStock(3)\">\n          <ion-radio style=\"zoom: 0.8;\" value=\"3\" ></ion-radio>&nbsp;&nbsp;&nbsp;&nbsp;\n          <ion-label style=\"font-size: 13px;\">per ounce</ion-label>\n         </ion-item>\n         <!-- <ion-item>\n          <ion-radio style=\"zoom: 0.8;\" value=\"4\" ></ion-radio>&nbsp;&nbsp;&nbsp;&nbsp;\n          <ion-label style=\"font-size: 13px;\">unit of number</ion-label>\n         </ion-item> -->\n          <ion-row style=\"--background: transparent;margin-top: -3%;\">\n            <ion-col no-padding text-left size=\"4\">\n              <ion-item (click)=\"changeStock(4)\" id=\"item_others\">\n                <ion-radio style=\"zoom: 0.8;\" value=\"5\" ></ion-radio>&nbsp;&nbsp;&nbsp;&nbsp;\n                <ion-label style=\"font-size: 13px;\">Others</ion-label>\n              </ion-item>\n            </ion-col>\n            <ion-col no-padding text-left size=\"8\">\n              <ion-list class=\"line-inputcateg\" style=\"margin-top: -5%;\">\n                <ion-item style=\"--padding-start: 15px;\">\n                  <ion-input mode=\"ios\" type=\"text\" class=\"item_input\" [(ngModel)]=\"otherStock\" placeholder=\"Enter unit\" size=\"small\"></ion-input>\n                 </ion-item>\n              </ion-list>\n            </ion-col>\n          </ion-row>\n      </ion-radio-group>\n    </ion-list>\n    <ion-list class=\"line-inputcateg\" style=\"margin-top: -5%;\">\n      <ion-label class=\"item_label\" color=\"secondary\">QUANTITY</ion-label>\n      <ion-item>\n        <ion-input mode=\"ios\" type=\"number\" [(ngModel)]=\"quantity\" class=\"item_input\" placeholder=\"Enter quantity\" size=\"small\"></ion-input>\n       </ion-item>\n    </ion-list>\n    <ion-list class=\"line-inputarea\">\n      <ion-label class=\"item_label\"  color=\"secondary\">DESCRIPTION</ion-label>\n      <ion-item>\n        <ion-textarea mode=\"ios\" [(ngModel)]=\"description\" class=\"item_area\"></ion-textarea>\n       </ion-item>\n    </ion-list>\n  \n</ion-content>\n<div class=\"postitem-overlay\" padding>\n  <ion-button expand=\"block\" (click)=\"goSetprice()\" mode=\"ios\" fill=\"outline\" color=\"primary\">\n    NEXT\n  </ion-button>\n  <div class=\"postitem_container\">\n    <ul class=\"postitem_progressbar\">\n      <li class=\"whenfocus active\"><span>Photo</span></li>\n      <li class=\"whenfocus\"><span>Details</span></li>\n      <li><span>Price</span></li>\n      <li><span>Delivery</span></li>\n      <li><span>Post</span></li>\n    </ul>\n  </div>\n</div>");

/***/ }),

/***/ "./src/app/describeitem/describeitem-routing.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/describeitem/describeitem-routing.module.ts ***!
  \*************************************************************/
/*! exports provided: DescribeitemPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DescribeitemPageRoutingModule", function() { return DescribeitemPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _describeitem_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./describeitem.page */ "./src/app/describeitem/describeitem.page.ts");




const routes = [
    {
        path: '',
        component: _describeitem_page__WEBPACK_IMPORTED_MODULE_3__["DescribeitemPage"]
    }
];
let DescribeitemPageRoutingModule = class DescribeitemPageRoutingModule {
};
DescribeitemPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], DescribeitemPageRoutingModule);



/***/ }),

/***/ "./src/app/describeitem/describeitem.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/describeitem/describeitem.module.ts ***!
  \*****************************************************/
/*! exports provided: DescribeitemPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DescribeitemPageModule", function() { return DescribeitemPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _describeitem_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./describeitem-routing.module */ "./src/app/describeitem/describeitem-routing.module.ts");
/* harmony import */ var _describeitem_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./describeitem.page */ "./src/app/describeitem/describeitem.page.ts");







let DescribeitemPageModule = class DescribeitemPageModule {
};
DescribeitemPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _describeitem_routing_module__WEBPACK_IMPORTED_MODULE_5__["DescribeitemPageRoutingModule"]
        ],
        declarations: [_describeitem_page__WEBPACK_IMPORTED_MODULE_6__["DescribeitemPage"]]
    })
], DescribeitemPageModule);



/***/ }),

/***/ "./src/app/describeitem/describeitem.page.scss":
/*!*****************************************************!*\
  !*** ./src/app/describeitem/describeitem.page.scss ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".line-input {\n  margin-bottom: 0 !important;\n  background: transparent;\n}\n.line-input ion-item {\n  --border-color: transparent!important;\n  --highlight-height: 0;\n  margin-top: -5%;\n  --background:transparent;\n  --padding-start: 0;\n}\n.line-inputcateg {\n  margin-bottom: 0 !important;\n  background: transparent;\n}\n.line-inputcateg ion-item {\n  --border-color: transparent!important;\n  --highlight-height: 0;\n  border: 1px solid #e2e2e2;\n  border-radius: 4px;\n  height: 35px;\n  margin-top: 2%;\n  --background: #ffffffb5;\n}\n.line-inputarea {\n  margin-bottom: 0 !important;\n  background: transparent;\n}\n.line-inputarea ion-item {\n  --border-color: transparent!important;\n  --highlight-height: 0;\n  border: 1px solid #e2e2e2;\n  border-radius: 4px;\n  height: 80px;\n  margin-top: 2%;\n  --background: #ffffffb5;\n}\n.line-inputothers {\n  margin-bottom: 0 !important;\n  background: transparent;\n  margin-top: -2%;\n}\n.line-inputothers ion-item {\n  --border-color: transparent!important;\n  --highlight-height: 0;\n  border: 1px solid #e2e2e2;\n  border-radius: 4px;\n  height: 30px;\n  --background: #ffffffb5;\n  --padding-start: 15px;\n}\n.item-radio-checked {\n  --background: transparent;\n}\n.item_input {\n  font-size: 13px;\n  margin-top: -15px;\n  color: #424242 !important;\n}\n.item_area {\n  font-size: 13px;\n  color: #424242 !important;\n}\n.item_select {\n  font-size: 14px;\n  --padding-top: 1%;\n  color: #424242 !important;\n}\n.item_label {\n  font-weight: bold;\n  font-size: 14px;\n}\n.item_label2 {\n  font-size: 13px;\n}\n.item_icon {\n  margin-bottom: -1%;\n  font-size: 11px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZW5kcmljay9EZXNrdG9wL1RlbG1vIFNvbHV0aW9ucy9Gb3JQcm9kdWN0aW9uL21heTEwMjAyMS9sYXN0IHNldHVwL2dyZWVudGh1bWJhcHAgY29weS9zcmMvYXBwL2Rlc2NyaWJlaXRlbS9kZXNjcmliZWl0ZW0ucGFnZS5zY3NzIiwic3JjL2FwcC9kZXNjcmliZWl0ZW0vZGVzY3JpYmVpdGVtLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQTtFQUNFLDJCQUFBO0VBQ0EsdUJBQUE7QUNBRjtBRENFO0VBQ0kscUNBQUE7RUFDQSxxQkFBQTtFQUNBLGVBQUE7RUFDRCx3QkFBQTtFQUNDLGtCQUFBO0FDQ047QURFQTtFQUNJLDJCQUFBO0VBQ0EsdUJBQUE7QUNDSjtBREFJO0VBQ0kscUNBQUE7RUFDQSxxQkFBQTtFQUNBLHlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsY0FBQTtFQUNBLHVCQUFBO0FDRVI7QURDRTtFQUNFLDJCQUFBO0VBQ0EsdUJBQUE7QUNFSjtBRERJO0VBQ0kscUNBQUE7RUFDQSxxQkFBQTtFQUNBLHlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsY0FBQTtFQUNBLHVCQUFBO0FDR1I7QURBRTtFQUNFLDJCQUFBO0VBQ0EsdUJBQUE7RUFDQSxlQUFBO0FDR0o7QURGSTtFQUNJLHFDQUFBO0VBQ0EscUJBQUE7RUFDQSx5QkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLHVCQUFBO0VBQ0EscUJBQUE7QUNJUjtBRERFO0VBQ0kseUJBQUE7QUNJTjtBREZBO0VBQ0UsZUFBQTtFQUNBLGlCQUFBO0VBQ0EseUJBQUE7QUNLRjtBREhBO0VBQ0ksZUFBQTtFQUNBLHlCQUFBO0FDTUo7QURKQTtFQUNJLGVBQUE7RUFDQSxpQkFBQTtFQUNBLHlCQUFBO0FDT0o7QURMQTtFQUNFLGlCQUFBO0VBQ0EsZUFBQTtBQ1FGO0FETkE7RUFDSSxlQUFBO0FDU0o7QURQQTtFQUNJLGtCQUFBO0VBQ0EsZUFBQTtBQ1VKIiwiZmlsZSI6InNyYy9hcHAvZGVzY3JpYmVpdGVtL2Rlc2NyaWJlaXRlbS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcbi5saW5lLWlucHV0IHtcbiAgbWFyZ2luLWJvdHRvbTogMCFpbXBvcnRhbnQ7XG4gIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICBpb24taXRlbSB7XG4gICAgICAtLWJvcmRlci1jb2xvcjogdHJhbnNwYXJlbnQhaW1wb3J0YW50O1xuICAgICAgLS1oaWdobGlnaHQtaGVpZ2h0OiAwO1xuICAgICAgbWFyZ2luLXRvcDogLTUlO1xuICAgICAtLWJhY2tncm91bmQ6dHJhbnNwYXJlbnQ7XG4gICAgICAtLXBhZGRpbmctc3RhcnQ6IDA7XG4gIH1cbn1cbi5saW5lLWlucHV0Y2F0ZWcge1xuICAgIG1hcmdpbi1ib3R0b206IDAhaW1wb3J0YW50O1xuICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICAgIGlvbi1pdGVtIHtcbiAgICAgICAgLS1ib3JkZXItY29sb3I6IHRyYW5zcGFyZW50IWltcG9ydGFudDtcbiAgICAgICAgLS1oaWdobGlnaHQtaGVpZ2h0OiAwO1xuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjZTJlMmUyO1xuICAgICAgICBib3JkZXItcmFkaXVzOiA0cHg7XG4gICAgICAgIGhlaWdodDogMzVweDtcbiAgICAgICAgbWFyZ2luLXRvcDogMiU7XG4gICAgICAgIC0tYmFja2dyb3VuZDogI2ZmZmZmZmI1O1xuICAgIH1cbiAgfVxuICAubGluZS1pbnB1dGFyZWEge1xuICAgIG1hcmdpbi1ib3R0b206IDAhaW1wb3J0YW50O1xuICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICAgIGlvbi1pdGVtIHtcbiAgICAgICAgLS1ib3JkZXItY29sb3I6IHRyYW5zcGFyZW50IWltcG9ydGFudDtcbiAgICAgICAgLS1oaWdobGlnaHQtaGVpZ2h0OiAwO1xuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjZTJlMmUyO1xuICAgICAgICBib3JkZXItcmFkaXVzOiA0cHg7XG4gICAgICAgIGhlaWdodDogODBweDtcbiAgICAgICAgbWFyZ2luLXRvcDogMiU7XG4gICAgICAgIC0tYmFja2dyb3VuZDogI2ZmZmZmZmI1O1xuICAgIH1cbiAgfVxuICAubGluZS1pbnB1dG90aGVycyB7XG4gICAgbWFyZ2luLWJvdHRvbTogMCFpbXBvcnRhbnQ7XG4gICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG4gICAgbWFyZ2luLXRvcDogLTIlO1xuICAgIGlvbi1pdGVtIHtcbiAgICAgICAgLS1ib3JkZXItY29sb3I6IHRyYW5zcGFyZW50IWltcG9ydGFudDtcbiAgICAgICAgLS1oaWdobGlnaHQtaGVpZ2h0OiAwO1xuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjZTJlMmUyO1xuICAgICAgICBib3JkZXItcmFkaXVzOiA0cHg7XG4gICAgICAgIGhlaWdodDogMzBweDtcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiAjZmZmZmZmYjU7XG4gICAgICAgIC0tcGFkZGluZy1zdGFydDogMTVweDtcbiAgICB9XG4gIH1cbiAgLml0ZW0tcmFkaW8tY2hlY2tlZHtcbiAgICAgIC0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG4gIH1cbi5pdGVtX2lucHV0e1xuICBmb250LXNpemU6IDEzcHg7XG4gIG1hcmdpbi10b3A6IC0xNXB4O1xuICBjb2xvcjogIzQyNDI0MiFpbXBvcnRhbnQ7XG59XG4uaXRlbV9hcmVhe1xuICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgICBjb2xvcjogIzQyNDI0MiFpbXBvcnRhbnQ7XG4gIH1cbi5pdGVtX3NlbGVjdHtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgLS1wYWRkaW5nLXRvcDogMSU7XG4gICAgY29sb3I6ICM0MjQyNDIhaW1wb3J0YW50O1xufVxuLml0ZW1fbGFiZWx7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBmb250LXNpemU6IDE0cHg7XG59XG4uaXRlbV9sYWJlbDJ7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICB9XG4uaXRlbV9pY29ue1xuICAgIG1hcmdpbi1ib3R0b206IC0xJTtcbiAgICBmb250LXNpemU6IDExcHg7XG59IiwiLmxpbmUtaW5wdXQge1xuICBtYXJnaW4tYm90dG9tOiAwICFpbXBvcnRhbnQ7XG4gIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xufVxuLmxpbmUtaW5wdXQgaW9uLWl0ZW0ge1xuICAtLWJvcmRlci1jb2xvcjogdHJhbnNwYXJlbnQhaW1wb3J0YW50O1xuICAtLWhpZ2hsaWdodC1oZWlnaHQ6IDA7XG4gIG1hcmdpbi10b3A6IC01JTtcbiAgLS1iYWNrZ3JvdW5kOnRyYW5zcGFyZW50O1xuICAtLXBhZGRpbmctc3RhcnQ6IDA7XG59XG5cbi5saW5lLWlucHV0Y2F0ZWcge1xuICBtYXJnaW4tYm90dG9tOiAwICFpbXBvcnRhbnQ7XG4gIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xufVxuLmxpbmUtaW5wdXRjYXRlZyBpb24taXRlbSB7XG4gIC0tYm9yZGVyLWNvbG9yOiB0cmFuc3BhcmVudCFpbXBvcnRhbnQ7XG4gIC0taGlnaGxpZ2h0LWhlaWdodDogMDtcbiAgYm9yZGVyOiAxcHggc29saWQgI2UyZTJlMjtcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xuICBoZWlnaHQ6IDM1cHg7XG4gIG1hcmdpbi10b3A6IDIlO1xuICAtLWJhY2tncm91bmQ6ICNmZmZmZmZiNTtcbn1cblxuLmxpbmUtaW5wdXRhcmVhIHtcbiAgbWFyZ2luLWJvdHRvbTogMCAhaW1wb3J0YW50O1xuICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbn1cbi5saW5lLWlucHV0YXJlYSBpb24taXRlbSB7XG4gIC0tYm9yZGVyLWNvbG9yOiB0cmFuc3BhcmVudCFpbXBvcnRhbnQ7XG4gIC0taGlnaGxpZ2h0LWhlaWdodDogMDtcbiAgYm9yZGVyOiAxcHggc29saWQgI2UyZTJlMjtcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xuICBoZWlnaHQ6IDgwcHg7XG4gIG1hcmdpbi10b3A6IDIlO1xuICAtLWJhY2tncm91bmQ6ICNmZmZmZmZiNTtcbn1cblxuLmxpbmUtaW5wdXRvdGhlcnMge1xuICBtYXJnaW4tYm90dG9tOiAwICFpbXBvcnRhbnQ7XG4gIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICBtYXJnaW4tdG9wOiAtMiU7XG59XG4ubGluZS1pbnB1dG90aGVycyBpb24taXRlbSB7XG4gIC0tYm9yZGVyLWNvbG9yOiB0cmFuc3BhcmVudCFpbXBvcnRhbnQ7XG4gIC0taGlnaGxpZ2h0LWhlaWdodDogMDtcbiAgYm9yZGVyOiAxcHggc29saWQgI2UyZTJlMjtcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xuICBoZWlnaHQ6IDMwcHg7XG4gIC0tYmFja2dyb3VuZDogI2ZmZmZmZmI1O1xuICAtLXBhZGRpbmctc3RhcnQ6IDE1cHg7XG59XG5cbi5pdGVtLXJhZGlvLWNoZWNrZWQge1xuICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xufVxuXG4uaXRlbV9pbnB1dCB7XG4gIGZvbnQtc2l6ZTogMTNweDtcbiAgbWFyZ2luLXRvcDogLTE1cHg7XG4gIGNvbG9yOiAjNDI0MjQyICFpbXBvcnRhbnQ7XG59XG5cbi5pdGVtX2FyZWEge1xuICBmb250LXNpemU6IDEzcHg7XG4gIGNvbG9yOiAjNDI0MjQyICFpbXBvcnRhbnQ7XG59XG5cbi5pdGVtX3NlbGVjdCB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgLS1wYWRkaW5nLXRvcDogMSU7XG4gIGNvbG9yOiAjNDI0MjQyICFpbXBvcnRhbnQ7XG59XG5cbi5pdGVtX2xhYmVsIHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGZvbnQtc2l6ZTogMTRweDtcbn1cblxuLml0ZW1fbGFiZWwyIHtcbiAgZm9udC1zaXplOiAxM3B4O1xufVxuXG4uaXRlbV9pY29uIHtcbiAgbWFyZ2luLWJvdHRvbTogLTElO1xuICBmb250LXNpemU6IDExcHg7XG59Il19 */");

/***/ }),

/***/ "./src/app/describeitem/describeitem.page.ts":
/*!***************************************************!*\
  !*** ./src/app/describeitem/describeitem.page.ts ***!
  \***************************************************/
/*! exports provided: DescribeitemPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DescribeitemPage", function() { return DescribeitemPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _selectcategories_selectcategories_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../selectcategories/selectcategories.page */ "./src/app/selectcategories/selectcategories.page.ts");






let DescribeitemPage = class DescribeitemPage {
    constructor(router, navCtrl, toastController, modalController, storage) {
        this.router = router;
        this.navCtrl = navCtrl;
        this.toastController = toastController;
        this.modalController = modalController;
        this.storage = storage;
        this.category_id = 1;
        this.stocks_id = 1;
        this.stocks_name = "";
        this.description = "";
        this.category_name = "";
        this.otherStock = "";
        this.custom_category = "";
    }
    ngOnInit() {
        this.stocks_id = 1;
        // this.storage.get("greenthumb_itempost_pic").then((pictures)=>{
        //   for (var key in pictures) {
        //     console.log("filenames:"+pictures[key].picture_filename)
        //   }
        // });
    }
    changeStock(id) {
        this.stocks_id = id;
    }
    goCategories() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            //this.router.navigate(['categories']);
            const modal = yield this.modalController.create({
                component: _selectcategories_selectcategories_page__WEBPACK_IMPORTED_MODULE_5__["SelectcategoriesPage"],
                cssClass: 'categories',
                id: 'myModal',
            });
            modal.onDidDismiss()
                .then((data) => {
                console.log("data:" + JSON.stringify(data));
                this.category_id = data['data'].id; // Here's your selected category!
                this.category_name = data['data'].value;
                console.log("category_name:" + this.category_name);
            });
            return yield modal.present();
        });
    }
    goHelp() {
        //window.location.href="http://greenthumbtrade.com/help";
        this.navCtrl.navigateForward((['tabs/help']), { animated: false, });
    }
    goBack() {
        window.history.back();
        //this.navCtrl.navigateForward((['tab3']), { animated: false, });
    }
    goSetprice() {
        if (this.stocks_id == 1) {
            this.stocks_name = "per piece";
        }
        else if (this.stocks_id == 2) {
            this.stocks_name = "per pound";
        }
        else if (this.stocks_id == 3) {
            this.stocks_name = "per ounce";
        }
        else {
            this.stocks_name = this.otherStock;
        }
        let data = {
            category_id: this.category_id,
            stocks_id: this.stocks_id,
            stocks_name: this.stocks_name,
            quantity: this.quantity,
            description: this.description,
            otherStock: this.otherStock,
            category_name: this.category_name
        };
        this.storage.set("greenthumb_description", data);
        // this.storage.get("description").then((val) => {
        //   console.log("category_id", val['category_id']);
        //   console.log("stocks_id", val['stocks_id']);
        //   console.log("quantity", val['quantity']);
        //   console.log("description", val['description']);
        // })
        if (this.category_name == "") {
            this.presentToast("Please Select Category");
        }
        else if (this.quantity == null) {
            this.presentToast("Please Input Quantity");
        }
        else if (this.description == "") {
            this.presentToast("Please Input Description of the Item");
        }
        else {
            console.log("stocks_id:" + this.stocks_id);
            this.navCtrl.navigateForward((['setprice']), { animated: false, });
        }
    }
    presentToast(x) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: x,
                duration: 3000
            });
            toast.present();
        });
    }
};
DescribeitemPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"] }
];
DescribeitemPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-describeitem',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./describeitem.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/describeitem/describeitem.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./describeitem.page.scss */ "./src/app/describeitem/describeitem.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"],
        _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"]])
], DescribeitemPage);



/***/ })

}]);
//# sourceMappingURL=describeitem-describeitem-module-es2015.js.map