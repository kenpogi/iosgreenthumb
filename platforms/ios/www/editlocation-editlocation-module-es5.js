function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["editlocation-editlocation-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/editlocation/editlocation.page.html":
  /*!*******************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/editlocation/editlocation.page.html ***!
    \*******************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppEditlocationEditlocationPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-title>editlocation</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n</ion-content>\n";
    /***/
  },

  /***/
  "./src/app/editlocation/editlocation-routing.module.ts":
  /*!*************************************************************!*\
    !*** ./src/app/editlocation/editlocation-routing.module.ts ***!
    \*************************************************************/

  /*! exports provided: EditlocationPageRoutingModule */

  /***/
  function srcAppEditlocationEditlocationRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "EditlocationPageRoutingModule", function () {
      return EditlocationPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _editlocation_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./editlocation.page */
    "./src/app/editlocation/editlocation.page.ts");

    var routes = [{
      path: '',
      component: _editlocation_page__WEBPACK_IMPORTED_MODULE_3__["EditlocationPage"]
    }];

    var EditlocationPageRoutingModule = function EditlocationPageRoutingModule() {
      _classCallCheck(this, EditlocationPageRoutingModule);
    };

    EditlocationPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], EditlocationPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/editlocation/editlocation.module.ts":
  /*!*****************************************************!*\
    !*** ./src/app/editlocation/editlocation.module.ts ***!
    \*****************************************************/

  /*! exports provided: EditlocationPageModule */

  /***/
  function srcAppEditlocationEditlocationModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "EditlocationPageModule", function () {
      return EditlocationPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _editlocation_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./editlocation-routing.module */
    "./src/app/editlocation/editlocation-routing.module.ts");
    /* harmony import */


    var _editlocation_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./editlocation.page */
    "./src/app/editlocation/editlocation.page.ts");

    var EditlocationPageModule = function EditlocationPageModule() {
      _classCallCheck(this, EditlocationPageModule);
    };

    EditlocationPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _editlocation_routing_module__WEBPACK_IMPORTED_MODULE_5__["EditlocationPageRoutingModule"]],
      declarations: [_editlocation_page__WEBPACK_IMPORTED_MODULE_6__["EditlocationPage"]]
    })], EditlocationPageModule);
    /***/
  },

  /***/
  "./src/app/editlocation/editlocation.page.scss":
  /*!*****************************************************!*\
    !*** ./src/app/editlocation/editlocation.page.scss ***!
    \*****************************************************/

  /*! exports provided: default */

  /***/
  function srcAppEditlocationEditlocationPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2VkaXRsb2NhdGlvbi9lZGl0bG9jYXRpb24ucGFnZS5zY3NzIn0= */";
    /***/
  },

  /***/
  "./src/app/editlocation/editlocation.page.ts":
  /*!***************************************************!*\
    !*** ./src/app/editlocation/editlocation.page.ts ***!
    \***************************************************/

  /*! exports provided: EditlocationPage */

  /***/
  function srcAppEditlocationEditlocationPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "EditlocationPage", function () {
      return EditlocationPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var EditlocationPage = /*#__PURE__*/function () {
      function EditlocationPage() {
        _classCallCheck(this, EditlocationPage);
      }

      _createClass(EditlocationPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return EditlocationPage;
    }();

    EditlocationPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-editlocation',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./editlocation.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/editlocation/editlocation.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./editlocation.page.scss */
      "./src/app/editlocation/editlocation.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])], EditlocationPage);
    /***/
  }
}]);
//# sourceMappingURL=editlocation-editlocation-module-es5.js.map