function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["editprofile-editprofile-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/editprofile/editprofile.page.html":
  /*!*****************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/editprofile/editprofile.page.html ***!
    \*****************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppEditprofileEditprofilePageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<!-- <ion-header translucent mode=\"ios\">\n  \n</ion-header> -->\n\n<ion-content mode=\"ios\">\n  \n  <div text-center *ngIf=\"!picture\" class=\"profile_card\">\n    <div class=\"wrapper\">\n      <ion-toolbar mode=\"ios\" style=\"--background: transparent;padding-top: 3%;\">\n        <ion-title style=\"font-size: 14px;\" color=\"light\" mode=\"ios\">EDIT PROFILE</ion-title>\n      </ion-toolbar>\n      <br>\n      <ion-avatar style=\"margin: auto;\">\n        <img src=\"assets/icon/profilee.png\">\n      </ion-avatar>\n      <ion-icon mode=\"ios\" (click)=\"upload_photo()\" class=\"upload_photo\" name=\"camera\"></ion-icon>\n      <br><br>\n    </div>\n  </div>\n\n  <ion-card style=\"color: white;\" *ngIf=\"picture\" text-center [style.backgroundImage]=\"'url('+ complete_pic +')'\">\n    <div class=\"wrapper\">\n      <ion-toolbar mode=\"ios\" style=\"--background: transparent;padding-top: 3%;\">\n        <ion-title style=\"font-size: 14px;\" color=\"light\" mode=\"ios\">EDIT PROFILE</ion-title>\n      </ion-toolbar>\n      <br>\n      <ion-avatar style=\"margin: auto;\">\n        <img [src] = \"complete_pic\">\n      </ion-avatar>\n      <ion-icon mode=\"ios\" (click)=\"upload_photo()\" class=\"upload_photo\" name=\"camera\"></ion-icon>\n      <br><br>\n    </div>\n  </ion-card>\n\n  <ion-list class=\"line-input\" padding>\n    <ion-row>\n      <ion-col>\n        <ion-label class=\"item_label\">Email</ion-label>\n        <ion-item>\n          <ion-input mode=\"ios\" type=\"email\" class=\"item_input\" size=\"small\" maxlength=\"50\" minlength=\"7\" [(ngModel)]=\"inpt_email\" value=\"{{ email }}\"></ion-input>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n  </ion-list>\n  <ion-list class=\"line-input\" padding style=\"padding-top: 0;\">\n    <ion-row>\n      <ion-col style=\"padding-right: 5%\">\n        <ion-label class=\"item_label\">First Name</ion-label>\n        <ion-item style=\" margin-top: 9%;\">\n          <ion-input mode=\"ios\" type=\"text\" class=\"item_input\" size=\"small\" maxlength=\"50\" minlength=\"2\" [(ngModel)]=\"inpt_fname\" value=\"{{ fname }}\"></ion-input>\n        </ion-item>\n      </ion-col>\n      <ion-col style=\"padding-left: 5%\">\n        <ion-label class=\"item_label\">Last Name</ion-label>\n        <ion-item style=\" margin-top: 9%;\"> \n          <ion-input mode=\"ios\" type=\"text\" class=\"item_input\" size=\"small\" maxlength=\"50\" minlength=\"2\" [(ngModel)]=\"inpt_lname\" value=\"{{ lname }}\"></ion-input>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n  </ion-list>\n  <ion-list class=\"line-input\" padding style=\"padding-top: 0;\">\n    <ion-row>\n      <ion-col>\n        <ion-label class=\"item_label\">Nickname</ion-label>\n        <ion-item>\n          <ion-input mode=\"ios\" type=\"text\" class=\"item_input\" size=\"small\" maxlength=\"50\" minlength=\"2\" [(ngModel)]=\"inpt_nickname\"  value=\"{{ nickname }}\"></ion-input>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n  </ion-list>\n  <ion-list class=\"line-input\" padding style=\"padding-top: 0;\">\n    <ion-row>\n      <ion-col>\n        <ion-label class=\"item_label\">Gender</ion-label>\n        <ion-item>\n          <ion-label  class=\"item_label\" style=\"margin-top: 0;\">Select One</ion-label>\n          <ion-select interface=\"popover\"  class=\"item_select\" mode=\"ios\" value=\"{{ gender }}\" [(ngModel)]=\"inpt_gender\">\n            <ion-select-option value=\"f\">Female</ion-select-option>\n            <ion-select-option value=\"m\">Male</ion-select-option>\n          </ion-select>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n  </ion-list>\n  <ion-list class=\"line-input\" padding style=\"padding-top: 0;\">\n    <ion-row>\n      <ion-col>\n        <ion-label class=\"item_label\">Birthdate</ion-label>\n        <ion-item>\n          <ion-input mode=\"ios\" type=\"date\" class=\"item_input\" size=\"small\" displayFormat=\"MM/DD/YYYY\" [(ngModel)]=\"inpt_birthdate\" value=\"{{ birthdate }}\" min=\"1945-01-01\" max=\"2012-12-31\"></ion-input>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n  </ion-list>\n  <ion-list class=\"line-input\" padding mode=\"ios\">\n    <ion-row>\n      <ion-col style=\"padding-right: 5%\">\n        <ion-label class=\"item_label\">Town/City</ion-label>\n        <ion-item style=\" margin-top: 9%;\">\n          <ion-input mode=\"ios\" type=\"text\" class=\"item_input\" size=\"small\" maxlength=\"50\" minlength=\"2\" [(ngModel)]=\"inpt_city\"  value=\"{{ city }}\"></ion-input>\n        </ion-item>\n      </ion-col>\n      <ion-col style=\"padding-left: 5%\">\n        <ion-label class=\"item_label\">Zip Code</ion-label>\n        <ion-item style=\" margin-top: 9%;\">\n          <ion-input mode=\"ios\" type=\"number\" class=\"item_input\" size=\"small\" maxlength=\"7\" minlength=\"2\" [(ngModel)]=\"inpt_zip_code\"  value=\"{{ zip_code }}\"></ion-input>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n  </ion-list>\n  <ion-list class=\"line-input\" padding style=\"padding-top: 0;\">\n    <ion-row>\n      <ion-col>\n        <ion-label class=\"item_label\">Country</ion-label>\n        <ion-item>\n          <ion-input mode=\"ios\" type=\"text\" class=\"item_input\" size=\"small\" maxlength=\"50\" minlength=\"2\" [(ngModel)]=\"inpt_country\" value=\"{{ country }}\"></ion-input>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n  </ion-list>\n  <ion-list class=\"line-input\" padding style=\"padding-top: 0;\">\n    <ion-row>\n      <ion-col>\n        <ion-label class=\"item_label\">Contact Number</ion-label>\n        <ion-item>\n          <ion-input mode=\"ios\" type=\"number\" class=\"item_input\" size=\"small\" maxlength=\"12\" minlength=\"11\" [(ngModel)]=\"inpt_mobile_num\" value=\"{{ mobile_num }}\"></ion-input>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n  </ion-list>\n  <ion-list padding>\n    <ion-row>\n      <ion-col>\n        <ion-button color=\"light\" expand=\"block\" mode=\"ios\" (click)=\"goBack()\">CANCEL</ion-button>\n      </ion-col>\n      <ion-col>\n        <ion-button expand=\"block\" mode=\"ios\" (click)=\"save_profile()\">SAVE</ion-button>\n      </ion-col>\n    </ion-row>\n  </ion-list>\n\n  \n  \n  \n \n \n<!-- <ion-button text-right size=\"small\" mode=\"ios\" color=\"light\" (click)=\"upload_photo()\"><ion-icon mode=\"ios\" name=\"image\"></ion-icon>\n  &nbsp;Update Profile Picture</ion-button><br>\n<div *ngIf=\"!picture\">\n<img src=\"assets/icon/profilee.png\" style=\"width: 25%;\">\n</div>\n<div *ngIf=\"picture\">\n<img [src] = \"complete_pic\" style=\"width: 25%;\">\n</div> -->\n<!-- <ion-list mode=\"ios\">\n  <ion-item>\n    <ion-grid>\n      <ion-row>\n        <ion-col size=\"6\">\n          <ion-label position=\"floating\" style=\"color: #cecece;\">First Name</ion-label>\n          <ion-input type=\"text\" mode=\"ios\" maxlength=\"50\" minlength=\"2\" [(ngModel)]=\"inpt_fname\" value=\"{{ fname }}\"></ion-input>\n        </ion-col>\n        <ion-col  size=\"6\">\n          <ion-label position=\"floating\" style=\"color: #cecece;\">Last Name</ion-label>\n          <ion-input type=\"text\" mode=\"ios\" maxlength=\"50\" minlength=\"2\" [(ngModel)]=\"inpt_lname\" value=\"{{ lname }}\"></ion-input>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n    <ion-icon mode=\"ios\" name=\"person\" slot=\"start\"></ion-icon>\n  </ion-item>\n  <ion-item>\n    <ion-label position=\"floating\" style=\"color: #cecece;\">Nickname</ion-label>\n    <ion-input type=\"text\" mode=\"ios\" maxlength=\"50\" minlength=\"2\" [(ngModel)]=\"inpt_nickname\"  value=\"{{ nickname }}\"></ion-input>\n    <ion-icon mode=\"ios\" name=\"person\" slot=\"start\"></ion-icon>\n  </ion-item>\n  <ion-item>\n    <ion-label position=\"floating\" style=\"color: #cecece;\">Birthdate</ion-label>\n    <ion-datetime displayFormat=\"MM/DD/YYYY\" [(ngModel)]=\"inpt_birthdate\" value=\"{{ birthdate }}\" min=\"1945-01-01\" max=\"2012-12-31\"></ion-datetime>\n    <ion-icon mode=\"ios\" name=\"ios-calendar\" slot=\"start\"></ion-icon>\n    \n  </ion-item>\n\n  <ion-item>\n    <ion-label position=\"floating\" style=\"color: #cecece;\">Gender</ion-label>\n    <ion-select interface=\"popover\" mode=\"ios\" value=\"{{ gender }}\" [(ngModel)]=\"inpt_gender\">\n      <ion-select-option value=\"Male\">Male</ion-select-option>\n      <ion-select-option value=\"Female\">Female</ion-select-option>\n    </ion-select>\n    <ion-icon mode=\"ios\" name=\"man\" slot=\"start\"></ion-icon>\n  </ion-item>\n  <ion-item>        \n    <ion-grid>\n      <ion-row>\n        <ion-col size=\"6\">\n          <ion-label position=\"floating\" style=\"color: #cecece;\">Town/City</ion-label>\n          <ion-input type=\"text\" mode=\"ios\" maxlength=\"50\" minlength=\"2\" [(ngModel)]=\"inpt_city\"  value=\"{{ city }}\"></ion-input>\n        </ion-col>\n        <ion-col  size=\"6\">\n          <ion-label position=\"floating\" style=\"color: #cecece;\">Zip Code</ion-label>\n          <ion-input type=\"number\" mode=\"ios\" maxlength=\"7\" minlength=\"2\" [(ngModel)]=\"inpt_zip_code\"  value=\"{{ zip_code }}\"></ion-input>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n    <ion-icon mode=\"ios\" name=\"pin\" slot=\"start\"></ion-icon>\n  </ion-item>       \n  <ion-item>\n    <ion-label position=\"floating\" style=\"color: #cecece;\">Country</ion-label>\n    <ion-input type=\"text\" mode=\"ios\" maxlength=\"50\" minlength=\"2\" [(ngModel)]=\"inpt_country\" value=\"{{ country }}\"></ion-input>\n    <ion-icon mode=\"ios\" name=\"pin\" slot=\"start\"></ion-icon>\n  </ion-item>\n  <ion-item>\n    <ion-label position=\"floating\" style=\"color: #cecece;\">Email</ion-label>\n    <ion-input type=\"email\" mode=\"ios\" maxlength=\"50\" minlength=\"7\" [(ngModel)]=\"inpt_email\" value=\"{{ email }}\"></ion-input>\n    <ion-icon mode=\"ios\" name=\"mail\" slot=\"start\"></ion-icon>\n  </ion-item>\n  <ion-item>\n    <ion-label position=\"floating\" style=\"color: #cecece;\">Contact Number</ion-label>\n    <ion-input type=\"number\" mode=\"ios\" maxlength=\"12\" minlength=\"11\" [(ngModel)]=\"inpt_mobile_num\" value=\"{{ mobile_num }}\"></ion-input>\n    <ion-icon mode=\"ios\" name=\"call\" slot=\"start\"></ion-icon>\n  </ion-item>\n    <br><br>\n  <ion-button expand=\"block\" mode=\"ios\" (click)=\"save_profile()\" color=\"secondary\">SAVE</ion-button>\n</ion-list> -->\n</ion-content>\n";
    /***/
  },

  /***/
  "./src/app/editprofile/editprofile.module.ts":
  /*!***************************************************!*\
    !*** ./src/app/editprofile/editprofile.module.ts ***!
    \***************************************************/

  /*! exports provided: EditprofilePageModule */

  /***/
  function srcAppEditprofileEditprofileModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "EditprofilePageModule", function () {
      return EditprofilePageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _editprofile_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./editprofile.page */
    "./src/app/editprofile/editprofile.page.ts");

    var routes = [{
      path: '',
      component: _editprofile_page__WEBPACK_IMPORTED_MODULE_6__["EditprofilePage"]
    }];

    var EditprofilePageModule = function EditprofilePageModule() {
      _classCallCheck(this, EditprofilePageModule);
    };

    EditprofilePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      declarations: [_editprofile_page__WEBPACK_IMPORTED_MODULE_6__["EditprofilePage"]]
    })], EditprofilePageModule);
    /***/
  },

  /***/
  "./src/app/editprofile/editprofile.page.scss":
  /*!***************************************************!*\
    !*** ./src/app/editprofile/editprofile.page.scss ***!
    \***************************************************/

  /*! exports provided: default */

  /***/
  function srcAppEditprofileEditprofilePageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".upload_photo {\n  background: white;\n  color: black;\n  padding: 5px;\n  border-radius: 50%;\n  font-size: 20px;\n  position: absolute;\n  top: 65%;\n  left: 57%;\n}\n\n.profile_card {\n  color: white;\n  --background: black;\n  background-image: url(\"/assets/icon/background1.jpg\");\n  background-position: center center;\n  background-repeat: no-repeat;\n  background-size: cover;\n}\n\n.wrapper {\n  background-color: #4242428c;\n  -webkit-backdrop-filter: blur(10px);\n          backdrop-filter: blur(10px);\n}\n\nion-avatar {\n  width: 100% !important;\n  height: 100% !important;\n  max-width: 120px !important;\n  max-height: 120px !important;\n}\n\n.line-inputname {\n  background: transparent !important;\n}\n\n.line-inputname ion-item {\n  --border-color: transparent!important;\n  --highlight-height: 0;\n  border-bottom: 1px solid #ffffff;\n  --background: transparent !important;\n}\n\n.line-inputname ion-item ion-input {\n  font-size: 14px;\n  color: white !important;\n}\n\n.line-input {\n  margin-bottom: 0 !important;\n}\n\n.line-input ion-item {\n  --border-color: transparent!important;\n  --highlight-height: 0;\n  border: 1px solid #dedede;\n  border-radius: 4px;\n  height: 40px;\n  margin-top: 4%;\n}\n\n.item_input {\n  font-size: 14px;\n  --padding-top: 0;\n  color: #424242 !important;\n}\n\n.item_select {\n  font-size: 14px;\n  --padding-top: 1%;\n  color: #424242 !important;\n}\n\n.item_label {\n  color: #b3aeae !important;\n  font-weight: 300;\n  font-size: 13px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZW5kcmljay9EZXNrdG9wL1RlbG1vIFNvbHV0aW9ucy9Gb3JQcm9kdWN0aW9uL21heTEwMjAyMS9sYXN0IHNldHVwL2dyZWVudGh1bWJhcHAgY29weS9zcmMvYXBwL2VkaXRwcm9maWxlL2VkaXRwcm9maWxlLnBhZ2Uuc2NzcyIsInNyYy9hcHAvZWRpdHByb2ZpbGUvZWRpdHByb2ZpbGUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksaUJBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLFNBQUE7QUNDSjs7QURDQTtFQUNJLFlBQUE7RUFDQSxtQkFBQTtFQUNBLHFEQUFBO0VBQ0Esa0NBQUE7RUFDQSw0QkFBQTtFQUNBLHNCQUFBO0FDRUo7O0FEQUE7RUFFSSwyQkFBQTtFQUNBLG1DQUFBO1VBQUEsMkJBQUE7QUNFSjs7QURBQTtFQUNJLHNCQUFBO0VBQ0EsdUJBQUE7RUFDQSwyQkFBQTtFQUNBLDRCQUFBO0FDR0o7O0FEREE7RUFDSSxrQ0FBQTtBQ0lKOztBRERJO0VBQ0kscUNBQUE7RUFDQSxxQkFBQTtFQUNBLGdDQUFBO0VBQ0Esb0NBQUE7QUNHUjs7QURDUTtFQUNJLGVBQUE7RUFFQSx1QkFBQTtBQ0FaOztBREtBO0VBQ0ksMkJBQUE7QUNGSjs7QURHSTtFQUNJLHFDQUFBO0VBQ0EscUJBQUE7RUFDQSx5QkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLGNBQUE7QUNEUjs7QURjQTtFQUNJLGVBQUE7RUFDQSxnQkFBQTtFQUNBLHlCQUFBO0FDWEo7O0FEYUE7RUFDSSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSx5QkFBQTtBQ1ZKOztBRFlBO0VBQ0kseUJBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7QUNUSiIsImZpbGUiOiJzcmMvYXBwL2VkaXRwcm9maWxlL2VkaXRwcm9maWxlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi51cGxvYWRfcGhvdG97XG4gICAgYmFja2dyb3VuZDogd2hpdGU7XG4gICAgY29sb3I6IGJsYWNrO1xuICAgIHBhZGRpbmc6IDVweDtcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgZm9udC1zaXplOiAyMHB4O1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDY1JTtcbiAgICBsZWZ0OiA1NyU7XG59XG4ucHJvZmlsZV9jYXJke1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICAtLWJhY2tncm91bmQ6IGJsYWNrO1xuICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybCgnL2Fzc2V0cy9pY29uL2JhY2tncm91bmQxLmpwZycpO1xuICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlciBjZW50ZXI7XG4gICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xufVxuLndyYXBwZXJ7XG4gICAgLy8gYmFja2dyb3VuZC1jb2xvcjogcmdiYSgyNTUsIDI1NSwgMjU1LCAuMTUpO1xuICAgIGJhY2tncm91bmQtY29sb3I6ICM0MjQyNDI4YztcbiAgICBiYWNrZHJvcC1maWx0ZXI6IGJsdXIoMTBweCk7XG59XG5pb24tYXZhdGFyICB7ICAgICBcbiAgICB3aWR0aDoxMDAlICFpbXBvcnRhbnQ7ICBcbiAgICBoZWlnaHQgOiAxMDAlICFpbXBvcnRhbnQ7ICBcbiAgICBtYXgtd2lkdGg6IDEyMHB4ICFpbXBvcnRhbnQ7ICAvL2FueSBzaXplXG4gICAgbWF4LWhlaWdodDogMTIwcHggIWltcG9ydGFudDsgLy9hbnkgc2l6ZSBcbiAgICB9XG4ubGluZS1pbnB1dG5hbWUge1xuICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50ICFpbXBvcnRhbnQ7XG4gICAgLy8gbWFyZ2luOiA1JSAhaW1wb3J0YW50O1xuICAgIC8vIG1hcmdpbi10b3A6IDAgIWltcG9ydGFudDtcbiAgICBpb24taXRlbSB7XG4gICAgICAgIC0tYm9yZGVyLWNvbG9yOiB0cmFuc3BhcmVudCFpbXBvcnRhbnQ7XG4gICAgICAgIC0taGlnaGxpZ2h0LWhlaWdodDogMDtcbiAgICAgICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNmZmZmZmY7XG4gICAgICAgIC0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQgIWltcG9ydGFudDtcbiAgICAgICAgLy8gYm9yZGVyLXJhZGl1czogNHB4O1xuICAgICAgICAvLyBoZWlnaHQ6IDQwcHg7XG4gICAgICAgIC8vIG1hcmdpbi10b3A6IDQlO1xuICAgICAgICBpb24taW5wdXR7XG4gICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgICAgICAvLyAtLXBhZGRpbmctdG9wOiAwO1xuICAgICAgICAgICAgY29sb3I6IHdoaXRlIWltcG9ydGFudDtcbiAgICAgICAgICAgIC8vIC0tcGFkZGluZy1ib3R0b206IDA7XG4gICAgICAgIH1cbiAgICB9XG59XG4ubGluZS1pbnB1dCB7XG4gICAgbWFyZ2luLWJvdHRvbTogMCFpbXBvcnRhbnQ7XG4gICAgaW9uLWl0ZW0ge1xuICAgICAgICAtLWJvcmRlci1jb2xvcjogdHJhbnNwYXJlbnQhaW1wb3J0YW50O1xuICAgICAgICAtLWhpZ2hsaWdodC1oZWlnaHQ6IDA7XG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNkZWRlZGU7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgICAgICAgaGVpZ2h0OiA0MHB4O1xuICAgICAgICBtYXJnaW4tdG9wOiA0JTtcblxuICAgICAgICAvLyAmLml0ZW0taGFzLWZvY3VzIHtcbiAgICAgICAgLy8gICAgIGJvcmRlcjogMXB4IHNvbGlkIHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbiAgICAgICAgLy8gfVxuXG4gICAgICAgIC8vIGlvbi1sYWJlbCB7XG4gICAgICAgIC8vICAgICBjb2xvcjogIzgwN2U3ZSFpbXBvcnRhbnQ7XG4gICAgICAgIC8vICAgICBmb250LXdlaWdodDogMzAwO1xuICAgICAgICAvLyAgICAgZm9udC1zaXplOiAxcmVtIWltcG9ydGFudDtcbiAgICAgICAgLy8gfVxuICAgIH1cbn1cbi5pdGVtX2lucHV0e1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAtLXBhZGRpbmctdG9wOiAwO1xuICAgIGNvbG9yOiAjNDI0MjQyIWltcG9ydGFudDtcbn1cbi5pdGVtX3NlbGVjdHtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgLS1wYWRkaW5nLXRvcDogMSU7XG4gICAgY29sb3I6ICM0MjQyNDIhaW1wb3J0YW50O1xufVxuLml0ZW1fbGFiZWx7XG4gICAgY29sb3I6ICNiM2FlYWUgIWltcG9ydGFudDtcbiAgICBmb250LXdlaWdodDogMzAwO1xuICAgIGZvbnQtc2l6ZTogMTNweDtcbn0iLCIudXBsb2FkX3Bob3RvIHtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIGNvbG9yOiBibGFjaztcbiAgcGFkZGluZzogNXB4O1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG4gIGZvbnQtc2l6ZTogMjBweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDY1JTtcbiAgbGVmdDogNTclO1xufVxuXG4ucHJvZmlsZV9jYXJkIHtcbiAgY29sb3I6IHdoaXRlO1xuICAtLWJhY2tncm91bmQ6IGJsYWNrO1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCIvYXNzZXRzL2ljb24vYmFja2dyb3VuZDEuanBnXCIpO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXIgY2VudGVyO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xufVxuXG4ud3JhcHBlciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICM0MjQyNDI4YztcbiAgYmFja2Ryb3AtZmlsdGVyOiBibHVyKDEwcHgpO1xufVxuXG5pb24tYXZhdGFyIHtcbiAgd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcbiAgaGVpZ2h0OiAxMDAlICFpbXBvcnRhbnQ7XG4gIG1heC13aWR0aDogMTIwcHggIWltcG9ydGFudDtcbiAgbWF4LWhlaWdodDogMTIwcHggIWltcG9ydGFudDtcbn1cblxuLmxpbmUtaW5wdXRuYW1lIHtcbiAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQgIWltcG9ydGFudDtcbn1cbi5saW5lLWlucHV0bmFtZSBpb24taXRlbSB7XG4gIC0tYm9yZGVyLWNvbG9yOiB0cmFuc3BhcmVudCFpbXBvcnRhbnQ7XG4gIC0taGlnaGxpZ2h0LWhlaWdodDogMDtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNmZmZmZmY7XG4gIC0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQgIWltcG9ydGFudDtcbn1cbi5saW5lLWlucHV0bmFtZSBpb24taXRlbSBpb24taW5wdXQge1xuICBmb250LXNpemU6IDE0cHg7XG4gIGNvbG9yOiB3aGl0ZSAhaW1wb3J0YW50O1xufVxuXG4ubGluZS1pbnB1dCB7XG4gIG1hcmdpbi1ib3R0b206IDAgIWltcG9ydGFudDtcbn1cbi5saW5lLWlucHV0IGlvbi1pdGVtIHtcbiAgLS1ib3JkZXItY29sb3I6IHRyYW5zcGFyZW50IWltcG9ydGFudDtcbiAgLS1oaWdobGlnaHQtaGVpZ2h0OiAwO1xuICBib3JkZXI6IDFweCBzb2xpZCAjZGVkZWRlO1xuICBib3JkZXItcmFkaXVzOiA0cHg7XG4gIGhlaWdodDogNDBweDtcbiAgbWFyZ2luLXRvcDogNCU7XG59XG5cbi5pdGVtX2lucHV0IHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICAtLXBhZGRpbmctdG9wOiAwO1xuICBjb2xvcjogIzQyNDI0MiAhaW1wb3J0YW50O1xufVxuXG4uaXRlbV9zZWxlY3Qge1xuICBmb250LXNpemU6IDE0cHg7XG4gIC0tcGFkZGluZy10b3A6IDElO1xuICBjb2xvcjogIzQyNDI0MiAhaW1wb3J0YW50O1xufVxuXG4uaXRlbV9sYWJlbCB7XG4gIGNvbG9yOiAjYjNhZWFlICFpbXBvcnRhbnQ7XG4gIGZvbnQtd2VpZ2h0OiAzMDA7XG4gIGZvbnQtc2l6ZTogMTNweDtcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/editprofile/editprofile.page.ts":
  /*!*************************************************!*\
    !*** ./src/app/editprofile/editprofile.page.ts ***!
    \*************************************************/

  /*! exports provided: EditprofilePage */

  /***/
  function srcAppEditprofileEditprofilePageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "EditprofilePage", function () {
      return EditprofilePage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _providers_credential_provider__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../providers/credential-provider */
    "./src/providers/credential-provider.ts");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");

    var EditprofilePage = /*#__PURE__*/function () {
      function EditprofilePage(router, postPvdr, storage, toastController) {
        _classCallCheck(this, EditprofilePage);

        this.router = router;
        this.postPvdr = postPvdr;
        this.storage = storage;
        this.toastController = toastController;
      }

      _createClass(EditprofilePage, [{
        key: "presentToast",
        value: function presentToast() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
            var toast;
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    _context.next = 2;
                    return this.toastController.create({
                      message: 'Your profile has been saved.',
                      duration: 3000
                    });

                  case 2:
                    toast = _context.sent;
                    toast.present();

                  case 4:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        }
      }, {
        key: "presentToastLack",
        value: function presentToastLack() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
            var toast;
            return regeneratorRuntime.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    _context2.next = 2;
                    return this.toastController.create({
                      message: 'Please fill in all fields.',
                      duration: 3000
                    });

                  case 2:
                    toast = _context2.sent;
                    toast.present();

                  case 4:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this);
          }));
        }
      }, {
        key: "presentToastWarning",
        value: function presentToastWarning() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
            var toast;
            return regeneratorRuntime.wrap(function _callee3$(_context3) {
              while (1) {
                switch (_context3.prev = _context3.next) {
                  case 0:
                    _context3.next = 2;
                    return this.toastController.create({
                      message: "A problem occured. Please try to login again.",
                      duration: 3000
                    });

                  case 2:
                    toast = _context3.sent;
                    toast.present();

                  case 4:
                  case "end":
                    return _context3.stop();
                }
              }
            }, _callee3, this);
          }));
        }
      }, {
        key: "goBack",
        value: function goBack() {
          this.router.navigate(['myaccount']);
        }
      }, {
        key: "upload_photo",
        value: function upload_photo() {
          this.router.navigate(['uploadphoto']);
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {
          this.get_profile();
        }
      }, {
        key: "get_profile",
        value: function get_profile() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
            var _this = this;

            return regeneratorRuntime.wrap(function _callee5$(_context5) {
              while (1) {
                switch (_context5.prev = _context5.next) {
                  case 0:
                    this.storage.get("user_id").then(function (user_id) {
                      return new Promise(function (resolve) {
                        var body = {
                          action: 'get_profile',
                          user_id: user_id
                        };

                        _this.postPvdr.postData(body, 'credentials-api.php').subscribe(function (data) {
                          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
                            return regeneratorRuntime.wrap(function _callee4$(_context4) {
                              while (1) {
                                switch (_context4.prev = _context4.next) {
                                  case 0:
                                    if (data.result.fname) {
                                      this.fname = data.result.fname;
                                      this.lname = data.result.lname;
                                    } else {
                                      this.fname = "";
                                      this.lname = "";
                                    }

                                    console.log("data.result.fname:" + data.result.fname); //console.log("myServer:"+this.postPvdr.myServer());

                                    this.picture = data.result.photo;
                                    this.complete_pic = this.postPvdr.myServer() + "/brixy-live/images/" + this.picture;
                                    this.nickname = data.result.nickname;
                                    this.birthdate = data.result.birthdate;
                                    this.gender = data.result.gender;
                                    this.city = data.result.city;
                                    this.zip_code = data.result.zip_code;
                                    this.country = data.result.country;
                                    this.email = data.result.email;
                                    this.mobile_num = data.result.mobile_num;
                                    resolve(true);

                                  case 13:
                                  case "end":
                                    return _context4.stop();
                                }
                              }
                            }, _callee4, this);
                          }));
                        });
                      });
                    }); // this.storage.get('login_used').then((login) => {
                    //   this.login_used = new String(login);
                    //   console.log("Login: "+this.login_used);
                    //   if(login === "facebook" || login === "google"){
                    //     this.storage.get('email').then((email) => {
                    //       if(email !== null){
                    //         return new Promise(resolve => {
                    //           let login_type = login==="facebook" ? 1 : 2;
                    //           let body = {
                    //             action : 'get_profile',
                    //             login_used : login_type,
                    //             email : email
                    //           };
                    //           this.postPvdr.postData(body, 'credentials-api.php').subscribe(async data => {
                    //             if(data.result.fname){
                    //               this.fname = data.result.fname;
                    //               this.lname = data.result.lname;
                    //             }else{
                    //               this.fname = "";
                    //               this.lname = "";
                    //             }
                    //               this.picture = data.result.photo;
                    //               this.complete_pic = "http://192.168.1.2/brixy-live/images/"+this.picture;
                    //               this.nickname = data.result.nickname;
                    //               this.birthdate = data.result.birthdate;
                    //               this.gender = data.result.gender;
                    //               this.city = data.result.city;
                    //               this.zip_code = data.result.zip_code;
                    //               this.country = data.result.country;
                    //               this.email = data.result.email;
                    //               this.mobile_num = data.result.mobile_num;
                    //             resolve(true);
                    //           });
                    //         });
                    //       } else {
                    //         this.fname = "Anonymous";
                    //         this.lname = "User";
                    //         this.city = "Unknown";
                    //         this.country = "Address";
                    //       }
                    //     });
                    //   } else {
                    //     this.storage.get('mobile_num').then((mobile_num) => {
                    //       if(this.login_used == "mobile"){
                    //         return new Promise(resolve => {
                    //           let body = {
                    //             action : 'get_profile_mobile',
                    //             mobile_num : mobile_num
                    //           };
                    //           this.postPvdr.postData(body, 'credentials-api.php').subscribe(async data => {
                    //             if(data.result.fname !== 'null'){
                    //               this.fname = data.result.fname;
                    //               this.lname = data.result.lname;
                    //             }else{
                    //               this.fname = "";
                    //               this.lname = "";
                    //             }
                    //               this.picture = data.result.photo;
                    //               this.complete_pic = "http://192.168.1.2/brixy-live/images/"+this.picture;
                    //               this.nickname = data.result.nickname;
                    //               this.birthdate = data.result.birthdate;
                    //               this.gender = data.result.gender;
                    //               this.city = data.result.city;
                    //               this.zip_code = data.result.zip_code;
                    //               this.country = data.result.country;
                    //               this.email = data.result.email;
                    //               this.mobile_num = data.result.mobile_num;
                    //             resolve(true);
                    //           });
                    //         });
                    //       } else {
                    //         this.fname = "Anonymous";
                    //         this.lname = "User";
                    //         this.city = "Unknown";
                    //         this.country = "Address";
                    //       }
                    //     });
                    //   }
                    // });

                  case 1:
                  case "end":
                    return _context5.stop();
                }
              }
            }, _callee5, this);
          }));
        }
      }, {
        key: "save_profile",
        value: function save_profile() {
          var _this2 = this;

          this.storage.get("user_id").then(function (user_id) {
            if (_this2.inpt_fname !== undefined && _this2.inpt_lname !== undefined && _this2.inpt_nickname !== undefined && _this2.inpt_birthdate !== undefined && _this2.inpt_gender !== undefined && _this2.inpt_city !== undefined && _this2.inpt_zip_code !== undefined && _this2.inpt_country !== undefined && _this2.inpt_email !== undefined && _this2.inpt_mobile_num !== undefined) {
              var body = {
                action: 'save_profile',
                user_id: user_id,
                fname: _this2.inpt_fname,
                lname: _this2.inpt_lname,
                nickname: _this2.inpt_nickname,
                birthdate: _this2.inpt_birthdate,
                gender: _this2.inpt_gender,
                city: _this2.inpt_city,
                zip_code: _this2.inpt_zip_code,
                country: _this2.inpt_country,
                email: _this2.inpt_email,
                mobile_num: _this2.inpt_mobile_num
              }; //console.log("bodyko:"+JSON.stringify(body));

              _this2.postPvdr.postData(body, 'credentials-api.php').subscribe(function (data) {
                if (data.success) {
                  _this2.presentToast();

                  _this2.router.navigate(['myaccount']);
                }
              });
            } else {
              _this2.presentToastLack();
            }
          }); // this.storage.get('login_used').then((login) => {
          //   if(login == "mobile"){
          //     if(this.inpt_fname !== undefined && this.inpt_lname !== undefined && this.inpt_nickname !== undefined && this.inpt_birthdate !== undefined && this.inpt_gender !== undefined && this.inpt_city !== undefined && this.inpt_zip_code !== undefined && this.inpt_country !== undefined && this.inpt_email !== undefined && this.inpt_mobile_num !== undefined){
          //       let body = {
          //         action : 'save_profile_mobile',
          //         fname : this.inpt_fname,
          //         lname : this.inpt_lname,
          //         nickname : this.inpt_nickname,
          //         birthdate : this.inpt_birthdate,
          //         gender : this.inpt_gender,
          //         city : this.inpt_city,
          //         zip_code : this.inpt_zip_code,
          //         country : this.inpt_country,
          //         email : this.inpt_email,
          //         mobile_num :this.inpt_mobile_num
          //       };
          //       this.postPvdr.postData(body, 'credentials-api.php').subscribe(data => {
          //         if(data.success){
          //           this.presentToast();
          //         }
          //     });
          //   } else{
          //     this.presentToastLack();
          //   }
          //   } else {
          //     //this.presentToastWarning();
          //     if(this.inpt_fname !== undefined && this.inpt_lname !== undefined && this.inpt_nickname !== undefined && this.inpt_birthdate !== undefined && this.inpt_gender !== undefined && this.inpt_city !== undefined && this.inpt_zip_code !== undefined && this.inpt_country !== undefined && this.inpt_email !== undefined && this.inpt_mobile_num !== undefined){
          //       let login_type = login==="facebook" ? 1 : 2;
          //       let body = {
          //         action : 'save_profile',
          //         login_used : login_type,
          //         fname : this.inpt_fname,
          //         lname : this.inpt_lname,
          //         nickname : this.inpt_nickname,
          //         birthdate: this.inpt_birthdate,
          //         gender : this.inpt_gender,
          //         city : this.inpt_city,
          //         zip_code : this.inpt_zip_code,
          //         country : this.inpt_country,
          //         email : this.inpt_email,
          //         mobile_num :this.inpt_mobile_num
          //       };
          //       //console.log("bodyko:"+JSON.stringify(body));
          //       this.postPvdr.postData(body, 'credentials-api.php').subscribe(data => {
          //         if(data.success){
          //           this.presentToast();
          //           this.router.navigate(['myaccount']);
          //         }
          //       });
          //     } else {
          //       this.presentToastLack();
          //     }
          //   }
          // });
        }
      }, {
        key: "calculateAge",
        value: function calculateAge() {
          var bdate = document.getElementById("studentBdate").innerHTML;
          var b = bdate.split("-");
          var birthMonth = parseInt(b[1]);
          var birthDay = parseInt(b[2]);
          var birthYear = parseInt(b[0]); // let months   = ['', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
          // document.getElementById("studentBdate").innerHTML = months[parseInt(birthMonth)] + ' ' + parseInt(birthDay) + ', ' + birthYear;

          var todayDate = new Date();
          var todayYear = todayDate.getFullYear();
          var todayMonth = todayDate.getMonth();
          var todayDay = todayDate.getDate();
          var age = todayYear - birthYear;

          if (todayMonth < birthMonth - 1) {
            age--;
          }

          if (birthMonth - 1 == todayMonth && todayDay < birthDay) {
            age--;
          }

          console.log("Calculated Age:" + age); //document.getElementById("studentAge").innerHTML = age;
        }
      }]);

      return EditprofilePage;
    }();

    EditprofilePage.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }, {
        type: _providers_credential_provider__WEBPACK_IMPORTED_MODULE_3__["PostProvider"]
      }, {
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ToastController"]
      }];
    };

    EditprofilePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-editprofile',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./editprofile.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/editprofile/editprofile.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./editprofile.page.scss */
      "./src/app/editprofile/editprofile.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _providers_credential_provider__WEBPACK_IMPORTED_MODULE_3__["PostProvider"], _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ToastController"]])], EditprofilePage);
    /***/
  }
}]);
//# sourceMappingURL=editprofile-editprofile-module-es5.js.map