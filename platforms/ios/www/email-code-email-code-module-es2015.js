(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["email-code-email-code-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/email-code/email-code.page.html":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/email-code/email-code.page.html ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-title>Email Code</ion-title>\n    <!-- <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"home\"></ion-back-button>\n    </ion-buttons> -->\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"ion-padding\" style=\"text-align: center;\">\n  <!--  <img src=\"assets/icon/profilee.png\" style=\"width: 35%;padding-top: 15%;padding-bottom: 15%;\">-->\n  <ion-list mode=\"ios\">\n     \n  <ion-item>\n\n    <ion-grid>\n      <ion-row>\n        <ion-col size=\"12\">\n          <ion-label position=\"floating\" style=\"color: #cecece;\">Code</ion-label>\n          <!-- <ion-input type=\"number\" value=\"{{inpt_mobile_num}}\" placeholder=\"e.g. 9123456789\" [(ngModel)]=\"inpt_mobile\"></ion-input> -->\n          <!-- <ion-input type=\"text\"  placeholder=\"e.g. 9123456789\" [(ngModel)]=\"inpt_mobile\"></ion-input> -->\n          <ion-input type=\"email\" [(ngModel)]=\"email_code\"></ion-input>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-item>\n  <br>\n  <ion-button expand=\"full\" (click)=\"confirm()\">Confirm</ion-button>\n</ion-list>\n</ion-content>\n<ion-footer>\n<p style=\"text-align: center;font-size: 12px;\"><b>Developed by: Telmo Solutions</b></p>\n</ion-footer>\n");

/***/ }),

/***/ "./src/app/email-code/email-code-routing.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/email-code/email-code-routing.module.ts ***!
  \*********************************************************/
/*! exports provided: EmailCodePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmailCodePageRoutingModule", function() { return EmailCodePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _email_code_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./email-code.page */ "./src/app/email-code/email-code.page.ts");




const routes = [
    {
        path: '',
        component: _email_code_page__WEBPACK_IMPORTED_MODULE_3__["EmailCodePage"]
    }
];
let EmailCodePageRoutingModule = class EmailCodePageRoutingModule {
};
EmailCodePageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], EmailCodePageRoutingModule);



/***/ }),

/***/ "./src/app/email-code/email-code.module.ts":
/*!*************************************************!*\
  !*** ./src/app/email-code/email-code.module.ts ***!
  \*************************************************/
/*! exports provided: EmailCodePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmailCodePageModule", function() { return EmailCodePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _email_code_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./email-code-routing.module */ "./src/app/email-code/email-code-routing.module.ts");
/* harmony import */ var _email_code_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./email-code.page */ "./src/app/email-code/email-code.page.ts");







let EmailCodePageModule = class EmailCodePageModule {
};
EmailCodePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _email_code_routing_module__WEBPACK_IMPORTED_MODULE_5__["EmailCodePageRoutingModule"]
        ],
        declarations: [_email_code_page__WEBPACK_IMPORTED_MODULE_6__["EmailCodePage"]]
    })
], EmailCodePageModule);



/***/ }),

/***/ "./src/app/email-code/email-code.page.scss":
/*!*************************************************!*\
  !*** ./src/app/email-code/email-code.page.scss ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2VtYWlsLWNvZGUvZW1haWwtY29kZS5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/email-code/email-code.page.ts":
/*!***********************************************!*\
  !*** ./src/app/email-code/email-code.page.ts ***!
  \***********************************************/
/*! exports provided: EmailCodePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmailCodePage", function() { return EmailCodePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");




let EmailCodePage = class EmailCodePage {
    constructor(router, activatedRoute) {
        this.router = router;
        this.activatedRoute = activatedRoute;
    }
    ngOnInit() {
        this.server_code = this.activatedRoute.snapshot.paramMap.get("email_code");
        console.log("email_code:" + this.server_code);
    }
    confirm() {
        if (this.email_code == this.server_code) {
            console.log("alright pareho kamo");
            this.router.navigate(["/tabs"]);
        }
        else {
            console.log("mag-iba man ang code friend");
        }
    }
};
EmailCodePage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] }
];
EmailCodePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-email-code',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./email-code.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/email-code/email-code.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./email-code.page.scss */ "./src/app/email-code/email-code.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
        _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
], EmailCodePage);



/***/ })

}]);
//# sourceMappingURL=email-code-email-code-module-es2015.js.map