(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["email-login-email-login-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/email-login/email-login.page.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/email-login/email-login.page.html ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-title>Email Sign Up</ion-title>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"home\"></ion-back-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"ion-padding\" style=\"text-align: center;\">\n  <img src=\"assets/icon/profilee.png\" style=\"width: 35%;padding-top: 15%;padding-bottom: 15%;\">\n  <ion-list mode=\"ios\">\n     \n  <ion-item>\n\n    <ion-grid>\n      <ion-row>\n        <ion-col size=\"12\">\n          <ion-label position=\"floating\" style=\"color: #cecece;\">Email</ion-label>\n          <!-- <ion-input type=\"number\" value=\"{{inpt_mobile_num}}\" placeholder=\"e.g. 9123456789\" [(ngModel)]=\"inpt_mobile\"></ion-input> -->\n          <!-- <ion-input type=\"text\"  placeholder=\"e.g. 9123456789\" [(ngModel)]=\"inpt_mobile\"></ion-input> -->\n          <ion-input type=\"email\" [(ngModel)]=\"email\" ></ion-input>\n        </ion-col>\n      </ion-row>\n      <!-- <ion-row>\n        <ion-col size=\"12\">\n          <ion-label position=\"floating\" style=\"color: #cecece;\">Password</ion-label>\n          <ion-input type=\"password\" [(ngModel)]=\"password\" ></ion-input>\n        </ion-col>\n      </ion-row> -->\n    </ion-grid>\n  </ion-item>\n  <br>\n  <ion-button expand=\"full\" (click)=\"signUp()\">Sign Up</ion-button>\n</ion-list>\n</ion-content>\n<ion-footer>\n<p style=\"text-align: center;font-size: 12px;\"><b>Developed by: Telmo Solutions</b></p>\n</ion-footer>\n");

/***/ }),

/***/ "./src/app/email-login/email-login-routing.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/email-login/email-login-routing.module.ts ***!
  \***********************************************************/
/*! exports provided: EmailLoginPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmailLoginPageRoutingModule", function() { return EmailLoginPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _email_login_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./email-login.page */ "./src/app/email-login/email-login.page.ts");




const routes = [
    {
        path: '',
        component: _email_login_page__WEBPACK_IMPORTED_MODULE_3__["EmailLoginPage"]
    }
];
let EmailLoginPageRoutingModule = class EmailLoginPageRoutingModule {
};
EmailLoginPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], EmailLoginPageRoutingModule);



/***/ }),

/***/ "./src/app/email-login/email-login.module.ts":
/*!***************************************************!*\
  !*** ./src/app/email-login/email-login.module.ts ***!
  \***************************************************/
/*! exports provided: EmailLoginPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmailLoginPageModule", function() { return EmailLoginPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _email_login_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./email-login-routing.module */ "./src/app/email-login/email-login-routing.module.ts");
/* harmony import */ var _email_login_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./email-login.page */ "./src/app/email-login/email-login.page.ts");







let EmailLoginPageModule = class EmailLoginPageModule {
};
EmailLoginPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _email_login_routing_module__WEBPACK_IMPORTED_MODULE_5__["EmailLoginPageRoutingModule"]
        ],
        declarations: [_email_login_page__WEBPACK_IMPORTED_MODULE_6__["EmailLoginPage"]]
    })
], EmailLoginPageModule);



/***/ }),

/***/ "./src/app/email-login/email-login.page.scss":
/*!***************************************************!*\
  !*** ./src/app/email-login/email-login.page.scss ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2VtYWlsLWxvZ2luL2VtYWlsLWxvZ2luLnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/email-login/email-login.page.ts":
/*!*************************************************!*\
  !*** ./src/app/email-login/email-login.page.ts ***!
  \*************************************************/
/*! exports provided: EmailLoginPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmailLoginPage", function() { return EmailLoginPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _providers_credential_provider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../providers/credential-provider */ "./src/providers/credential-provider.ts");





let EmailLoginPage = class EmailLoginPage {
    constructor(postPvdr, router, storage) {
        this.postPvdr = postPvdr;
        this.router = router;
        this.storage = storage;
    }
    ngOnInit() {
    }
    signUp() {
        console.log("email:" + this.email);
        // console.log("password:"+this.password);
        let body = {
            action: 'emailLogin',
            email: this.email,
        };
        console.log("bodybody:" + JSON.stringify(body));
        this.postPvdr.postData(body, 'credentials-api.php').subscribe(data => {
            if (data.success) {
                this.storage.set("user_id", data.user_id);
                // console.log("user_id:"+data.user_id);
                // console.log("email_code:"+data.email_code);
                let body2 = {
                    action: 'send-email-code',
                    sentTo: this.email,
                    sentToCode: data.email_code
                };
                console.log("bodyisda:" + JSON.stringify(body2));
                this.postPvdr.sendCode(body2).subscribe(data2 => {
                    //console.log("status:"+data.email_code);
                    this.router.navigate(['/email-code', data.email_code]);
                });
            }
        });
    }
};
EmailLoginPage.ctorParameters = () => [
    { type: _providers_credential_provider__WEBPACK_IMPORTED_MODULE_4__["PostProvider"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"] }
];
EmailLoginPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-email-login',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./email-login.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/email-login/email-login.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./email-login.page.scss */ "./src/app/email-login/email-login.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_providers_credential_provider__WEBPACK_IMPORTED_MODULE_4__["PostProvider"],
        _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
        _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"]])
], EmailLoginPage);



/***/ })

}]);
//# sourceMappingURL=email-login-email-login-module-es2015.js.map