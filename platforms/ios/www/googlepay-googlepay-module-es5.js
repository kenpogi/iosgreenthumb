function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["googlepay-googlepay-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/googlepay/googlepay.page.html":
  /*!*************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/googlepay/googlepay.page.html ***!
    \*************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppGooglepayGooglepayPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header mode=\"ios\" no-border>\n  <ion-toolbar mode=\"ios\">\n      <ion-buttons slot=\"secondary\" (click)=\"goBack()\">\n        <ion-button mode=\"ios\" style=\"font-size: 15px;\">\n            Back\n        </ion-button>\n      </ion-buttons>\n      <ion-title color=\"secondary\">Google Pay</ion-title>\n    </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n</ion-content>\n";
    /***/
  },

  /***/
  "./src/app/googlepay/googlepay-routing.module.ts":
  /*!*******************************************************!*\
    !*** ./src/app/googlepay/googlepay-routing.module.ts ***!
    \*******************************************************/

  /*! exports provided: GooglepayPageRoutingModule */

  /***/
  function srcAppGooglepayGooglepayRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "GooglepayPageRoutingModule", function () {
      return GooglepayPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _googlepay_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./googlepay.page */
    "./src/app/googlepay/googlepay.page.ts");

    var routes = [{
      path: '',
      component: _googlepay_page__WEBPACK_IMPORTED_MODULE_3__["GooglepayPage"]
    }];

    var GooglepayPageRoutingModule = function GooglepayPageRoutingModule() {
      _classCallCheck(this, GooglepayPageRoutingModule);
    };

    GooglepayPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], GooglepayPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/googlepay/googlepay.module.ts":
  /*!***********************************************!*\
    !*** ./src/app/googlepay/googlepay.module.ts ***!
    \***********************************************/

  /*! exports provided: GooglepayPageModule */

  /***/
  function srcAppGooglepayGooglepayModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "GooglepayPageModule", function () {
      return GooglepayPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _googlepay_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./googlepay-routing.module */
    "./src/app/googlepay/googlepay-routing.module.ts");
    /* harmony import */


    var _googlepay_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./googlepay.page */
    "./src/app/googlepay/googlepay.page.ts");

    var GooglepayPageModule = function GooglepayPageModule() {
      _classCallCheck(this, GooglepayPageModule);
    };

    GooglepayPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _googlepay_routing_module__WEBPACK_IMPORTED_MODULE_5__["GooglepayPageRoutingModule"]],
      declarations: [_googlepay_page__WEBPACK_IMPORTED_MODULE_6__["GooglepayPage"]]
    })], GooglepayPageModule);
    /***/
  },

  /***/
  "./src/app/googlepay/googlepay.page.scss":
  /*!***********************************************!*\
    !*** ./src/app/googlepay/googlepay.page.scss ***!
    \***********************************************/

  /*! exports provided: default */

  /***/
  function srcAppGooglepayGooglepayPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2dvb2dsZXBheS9nb29nbGVwYXkucGFnZS5zY3NzIn0= */";
    /***/
  },

  /***/
  "./src/app/googlepay/googlepay.page.ts":
  /*!*********************************************!*\
    !*** ./src/app/googlepay/googlepay.page.ts ***!
    \*********************************************/

  /*! exports provided: GooglepayPage */

  /***/
  function srcAppGooglepayGooglepayPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "GooglepayPage", function () {
      return GooglepayPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");

    var GooglepayPage = /*#__PURE__*/function () {
      function GooglepayPage(router) {
        _classCallCheck(this, GooglepayPage);

        this.router = router;
      }

      _createClass(GooglepayPage, [{
        key: "goBack",
        value: function goBack() {
          // this.router.navigate(['help']);
          window.history.back();
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return GooglepayPage;
    }();

    GooglepayPage.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }];
    };

    GooglepayPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-googlepay',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./googlepay.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/googlepay/googlepay.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./googlepay.page.scss */
      "./src/app/googlepay/googlepay.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])], GooglepayPage);
    /***/
  }
}]);
//# sourceMappingURL=googlepay-googlepay-module-es5.js.map