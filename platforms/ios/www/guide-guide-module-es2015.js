(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["guide-guide-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/guide/guide.page.html":
/*!*****************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/guide/guide.page.html ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n<ion-content mode=\"ios\">\n  <br>\n    <ion-row text-center>\n      <ion-col>\n          <h3 style=\"line-height: 0px;font-weight: bolder;\">Getting Started</h3>\n          <p>Here is a guide to get you started</p>\n          <ion-button mode=\"ios\" shape=\"round\" class=\"btn-default\" (click)=\"toChoose()\">Continue</ion-button>\n      </ion-col>\n    </ion-row>\n        <ion-slides pager=\"true\">\n          <ion-slide>\n            <ion-grid>\n              <ion-row>\n                <ion-col size=\"4\" >\n                  <b style=\"font-size: 110px;color:#7ddcf2;font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;\">1</b>\n                  <h6 >Guide 1 Title</h6>\n                </ion-col>\n                <ion-col size=\"7\" >\n                    <img src=\"assets/icon/imgsample.jpg\" >  \n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col>\n                  <p class=\"justify\" padding style=\"padding-top: 0;font-size: 14px\">\n                      Lorem ipsum dolor sit amet, consete adipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliqu yam erat, sed. idunt ut labore et doloremagnaliqu yam erat, sed.\n                      <br><br>\n                      Lorem ipsum dolor sit amet, consete adipscing elitr, dolor sit amet, consete nonumy eirmod tempor.\n                  </p>\n                </ion-col>\n              </ion-row>\n            </ion-grid>\n          </ion-slide>\n          <ion-slide>\n              <ion-grid>\n                <ion-row>\n                  <ion-col size=\"4\" >\n                    <b style=\"font-size: 110px;color:#7ddcf2;font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;\">2</b>\n                    <h6 >Guide 2 Title</h6>\n                  </ion-col>\n                  <ion-col size=\"7\" >\n                      <img src=\"assets/icon/imgsample.jpg\" >  \n                  </ion-col>\n                </ion-row>\n                <ion-row>\n                  <ion-col>\n                    <p class=\"justify\" padding style=\"padding-top: 0;font-size: 14px\">\n                        Lorem ipsum dolor sit amet, consete adipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliqu yam erat, sed. idunt ut labore et doloremagnaliqu yam erat, sed.\n                        <br><br>\n                        Lorem ipsum dolor sit amet, consete adipscing elitr, dolor sit amet, consete nonumy eirmod tempor.\n                    </p>\n                  </ion-col>\n                </ion-row>\n              </ion-grid>\n            </ion-slide>\n            <ion-slide>\n                <ion-grid>\n                  <ion-row>\n                    <ion-col size=\"4\" >\n                      <b style=\"font-size: 110px;color:#7ddcf2;font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;\">3</b>\n                      <h6 >Guide 3 Title</h6>\n                    </ion-col>\n                    <ion-col size=\"7\" >\n                        <img src=\"assets/icon/imgsample.jpg\" >  \n                    </ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col>\n                      <p class=\"justify\" padding style=\"padding-top: 0;font-size: 14px\">\n                          Lorem ipsum dolor sit amet, consete adipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliqu yam erat, sed. idunt ut labore et doloremagnaliqu yam erat, sed.\n                          <br><br>\n                          Lorem ipsum dolor sit amet, consete adipscing elitr, dolor sit amet, consete nonumy eirmod tempor.\n                      </p>\n                    </ion-col>\n                  </ion-row>\n                </ion-grid>\n              </ion-slide>\n              <ion-slide>\n                  <ion-grid>\n                    <ion-row>\n                      <ion-col size=\"4\" >\n                        <b style=\"font-size: 110px;color:#7ddcf2;font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;\">4</b>\n                        <h6 >Guide 4 Title</h6>\n                      </ion-col>\n                      <ion-col size=\"7\" >\n                          <img src=\"assets/icon/imgsample.jpg\" >  \n                      </ion-col>\n                    </ion-row>\n                    <ion-row>\n                      <ion-col>\n                        <p class=\"justify\" padding style=\"padding-top: 0;font-size: 14px\">\n                            Lorem ipsum dolor sit amet, consete adipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliqu yam erat, sed. idunt ut labore et doloremagnaliqu yam erat, sed.\n                            <br><br>\n                            Lorem ipsum dolor sit amet, consete adipscing elitr, dolor sit amet, consete nonumy eirmod tempor.\n                        </p>\n                      </ion-col>\n                    </ion-row>\n                  </ion-grid>\n                </ion-slide>\n        </ion-slides>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/guide/guide.module.ts":
/*!***************************************!*\
  !*** ./src/app/guide/guide.module.ts ***!
  \***************************************/
/*! exports provided: GuidePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GuidePageModule", function() { return GuidePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _guide_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./guide.page */ "./src/app/guide/guide.page.ts");







const routes = [
    {
        path: '',
        component: _guide_page__WEBPACK_IMPORTED_MODULE_6__["GuidePage"]
    }
];
let GuidePageModule = class GuidePageModule {
};
GuidePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_guide_page__WEBPACK_IMPORTED_MODULE_6__["GuidePage"]]
    })
], GuidePageModule);



/***/ }),

/***/ "./src/app/guide/guide.page.scss":
/*!***************************************!*\
  !*** ./src/app/guide/guide.page.scss ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".justify {\n  text-align: justify;\n}\n\n.img-responsive {\n  width: 100%;\n  height: auto;\n}\n\n.img-loading {\n  width: 50%;\n  height: auto;\n}\n\n.btn-default {\n  --background: #1dc1e6;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZW5kcmljay9EZXNrdG9wL1RlbG1vIFNvbHV0aW9ucy9Gb3JQcm9kdWN0aW9uL21heTEwMjAyMS9sYXN0IHNldHVwL2dyZWVudGh1bWJhcHAgY29weS9zcmMvYXBwL2d1aWRlL2d1aWRlLnBhZ2Uuc2NzcyIsInNyYy9hcHAvZ3VpZGUvZ3VpZGUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksbUJBQUE7QUNDSjs7QURDQTtFQUNJLFdBQUE7RUFDQSxZQUFBO0FDRUo7O0FEQUE7RUFDSSxVQUFBO0VBQ0EsWUFBQTtBQ0dKOztBRERBO0VBQ0kscUJBQUE7QUNJSiIsImZpbGUiOiJzcmMvYXBwL2d1aWRlL2d1aWRlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5qdXN0aWZ5e1xuICAgIHRleHQtYWxpZ246IGp1c3RpZnk7XG59XG4uaW1nLXJlc3BvbnNpdmV7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiBhdXRvO1xufVxuLmltZy1sb2FkaW5ne1xuICAgIHdpZHRoOiA1MCU7XG4gICAgaGVpZ2h0OiBhdXRvO1xufVxuLmJ0bi1kZWZhdWx0e1xuICAgIC0tYmFja2dyb3VuZDogIzFkYzFlNjtcbn0iLCIuanVzdGlmeSB7XG4gIHRleHQtYWxpZ246IGp1c3RpZnk7XG59XG5cbi5pbWctcmVzcG9uc2l2ZSB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IGF1dG87XG59XG5cbi5pbWctbG9hZGluZyB7XG4gIHdpZHRoOiA1MCU7XG4gIGhlaWdodDogYXV0bztcbn1cblxuLmJ0bi1kZWZhdWx0IHtcbiAgLS1iYWNrZ3JvdW5kOiAjMWRjMWU2O1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/guide/guide.page.ts":
/*!*************************************!*\
  !*** ./src/app/guide/guide.page.ts ***!
  \*************************************/
/*! exports provided: GuidePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GuidePage", function() { return GuidePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");



let GuidePage = class GuidePage {
    constructor(router) {
        this.router = router;
    }
    toChoose() {
        this.router.navigate(['choose']);
    }
    ngOnInit() {
    }
};
GuidePage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
GuidePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-guide',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./guide.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/guide/guide.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./guide.page.scss */ "./src/app/guide/guide.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
], GuidePage);



/***/ })

}]);
//# sourceMappingURL=guide-guide-module-es2015.js.map