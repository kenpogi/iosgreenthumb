(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["livestream-livestream-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/livestream/livestream.page.html":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/livestream/livestream.page.html ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n<ion-content>\n  <div class=\"my-overlay\" text-center>\n    <ion-row >\n        <ion-col text-left size=\"8\">\n          <ion-list mode=\"ios\" style=\"background-color: transparent;\">\n            <ion-item mode=\"ios\" lines=\"none\" class=\"item_profile\">\n              <ion-thumbnail slot=\"start\" mode=\"ios\" (click)=\"openliveprofile()\">\n                  <div *ngIf=\"!user_photo\">\n                    <img src=\"assets/icon/brixylogo.png\">\n                  </div>\n                  <div *ngIf=\"user_photo\">\n                   <img [src] = \"user_photo\">\n                  </div> \n              </ion-thumbnail>\n              <ion-label mode=\"ios\" color=\"light\">\n                <h3 mode=\"ios\" style=\"font-family: system-ui;\">{{fname+\" \"+lname}}</h3>\n                <h6 class=\"lvlnum\" mode=\"ios\" style=\"font-size: 12px;\"> \n                  <img src=\"assets/icon/medal.png\" class=\"badge_knight3\">&nbsp;{{user_level}}&nbsp;&nbsp;\n                  <img src=\"assets/icon/follow.png\" class=\"badge_knight3\">&nbsp;{{followers}}&nbsp;&nbsp;\n                  <img src=\"assets/icon/viewer.png\" class=\"badge_knight3\">&nbsp;{{following}}</h6>\n              </ion-label>\n            </ion-item>\n          </ion-list>\n        </ion-col>\n        <ion-col text-left size=\"4\">\n          <div class=\"thumnails\">\n            <div class=\"list-thumbnail\">\n              <div class=\"img-thumb\">\n                <div style=\"display: inline-block;\" *ngFor=\"let view of viewersPic;\">\n                  <img [src] = \"view?.viewerPic\">\n                </div>\n              </div>\n            </div>\n          </div>\n        </ion-col>\n      </ion-row>\n      <ion-row padding style=\"margin-top: -10%;\">\n        <ion-col text-left>\n          <ion-row>\n            <ion-col style=\"padding-left: 0;\">\n              <img src=\"assets/icon/report.png\" class=\"brixy-icon\" style=\"width: 23% !important;\">\n            </ion-col>\n          </ion-row>\n          <!-- <ion-row>\n            <ion-col style=\"padding-left: 0;margin-top: 8%;\">\n              <img (click)=\"follow()\" [ngClass]=\"status ? 'myHeartFollow' : 'myHeartUnfollow'\">\n            </ion-col>\n          </ion-row> -->\n          <ion-row>\n            <ion-col style=\"padding-left: 0;margin-top: 8%;\">\n              <img src=\"assets/icon/kick.png\" class=\"brixy-icon\" >\n            </ion-col>\n          </ion-row>\n          <ion-row>\n            <ion-col style=\"padding-left: 0;margin-top: 8%;\">\n              <img src=\"assets/icon/close.png\" (click)=\"goHome()\" class=\"brixy-icon\" >\n            </ion-col>\n          </ion-row>\n        </ion-col>\n        <ion-col text-right style=\"visibility: hidden;\">\n          <ion-icon (click)=\"goHome()\" class=\"close_icon\" name=\"close\" mode=\"ios\" mode=\"ios\"></ion-icon>\n        </ion-col>\n      </ion-row>\n  </div>\n  <video id=\"localVideo\" autoplay playsinline [muted]=\"'muted'\" style=\"height: 95%\"></video>\n  <!-- <img src=\"assets/icon/girl.gif\" style=\"height: 95%;\"> -->\n\n  <div class=\"my-overlay\" style=\"bottom:0%;\" text-left>\n    <div class=\"overlay_message\">\n      <div class=\"wrapper\">\n        <ion-row>\n          <ion-col>\n            <p *ngFor=\"let message of messages\">\n              <ion-label color=\"warning\"><b>{{message.user_id}}</b></ion-label>&nbsp;\n              <ion-text color=\"light\">\n              {{message.msg}}</ion-text>\n            </p> \n            <p style=\"visibility: hidden;margin-bottom: -12%;\">\n                <ion-label color=\"warning\"><b>userid544</b></ion-label>&nbsp;\n                <ion-text color=\"light\">fdfdsffdf dffdfd sffd fdffdfds ffdfdff dfdsffd fd ffd fdsffdfdffdfdsffdfdf\n                </ion-text>\n            </p>\n          </ion-col>\n        </ion-row>\n      </div>\n    </div>\n\n    <ion-toolbar color=\"light\">\n      <ion-row align-items-center>\n        <ion-col size=\"10\">\n          <ion-textarea color=\"dark\" placeholder=\"Write message\" class=\"message-input\" rows=\"1\" [(ngModel)]=\"message\"></ion-textarea>\n        </ion-col>\n        <ion-col size=\"2\">\n          <ion-button mode=\"ios\" expand=\"block\" fill=\"clear\" [disabled]=\"message === ''\" class=\"msg-btn\"\n            (click)=\"sendMessage()\">\n            <ion-icon name=\"ios-send\" slot=\"icon-only\" mode=\"ios\" color=\"primary\"></ion-icon>\n          </ion-button>\n        </ion-col>\n      </ion-row>\n    </ion-toolbar>\n</div>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/livestream/livestream.module.ts":
/*!*************************************************!*\
  !*** ./src/app/livestream/livestream.module.ts ***!
  \*************************************************/
/*! exports provided: LivestreamPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LivestreamPageModule", function() { return LivestreamPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _livestream_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./livestream.page */ "./src/app/livestream/livestream.page.ts");







const routes = [
    {
        path: '',
        component: _livestream_page__WEBPACK_IMPORTED_MODULE_6__["LivestreamPage"]
    }
];
let LivestreamPageModule = class LivestreamPageModule {
};
LivestreamPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_livestream_page__WEBPACK_IMPORTED_MODULE_6__["LivestreamPage"]]
    })
], LivestreamPageModule);



/***/ }),

/***/ "./src/app/livestream/livestream.page.scss":
/*!*************************************************!*\
  !*** ./src/app/livestream/livestream.page.scss ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".my-overlay {\n  position: fixed;\n  width: 100%;\n  z-index: 20;\n}\n\n.overlay_message {\n  height: 300px;\n  display: flex;\n  flex-direction: column-reverse;\n  align-items: flex-end;\n  overflow-y: auto;\n  padding-left: 55px;\n  padding-right: 2px;\n  z-index: 99;\n}\n\n.overlay_message p {\n  padding: 8px;\n  background: rgba(0, 0, 0, 0.4);\n  border-radius: 50px;\n}\n\n.item_profile {\n  --background: rgba(0, 0, 0, 0.3);\n  border: 1px solid #1dc1e6;\n  border-radius: 30px;\n}\n\n.close_icon {\n  zoom: 3;\n  color: white;\n  position: fixed;\n  right: 2%;\n  top: 8.5%;\n}\n\n.thumnails {\n  overflow-x: scroll;\n  overflow-y: hidden;\n  margin-top: 5%;\n}\n\n.thumnails .list-thumbnail {\n  height: 100%;\n  white-space: nowrap;\n}\n\n.thumnails .list-thumbnail .img-thumb img {\n  display: inline-block;\n  border-radius: 50%;\n  padding: 3px;\n  width: 50px;\n  height: 50px;\n  margin: 0 2px 0 0;\n  line-height: 60px;\n}\n\n::-webkit-scrollbar {\n  display: none;\n}\n\n.myHeartFollow {\n  width: 23% !important;\n  height: auto;\n  content: url(\"/assets/icon/followed.png\");\n}\n\n.myHeartUnfollow {\n  width: 23% !important;\n  height: auto;\n  content: url(\"/assets/icon/follow.png\");\n}\n\n.lvlnum {\n  margin: 0;\n  margin-top: 4%;\n  font-weight: bolder;\n  font-family: Verdana, Geneva, Tahoma, sans-serif;\n}\n\nion-item {\n  --padding-start: 0% !important;\n}\n\n.brixy-icon {\n  width: 23% !important;\n  height: auto;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZW5kcmljay9EZXNrdG9wL1RlbG1vIFNvbHV0aW9ucy9Gb3JQcm9kdWN0aW9uL21heTEwMjAyMS9sYXN0IHNldHVwL2dyZWVudGh1bWJhcHAgY29weS9zcmMvYXBwL2xpdmVzdHJlYW0vbGl2ZXN0cmVhbS5wYWdlLnNjc3MiLCJzcmMvYXBwL2xpdmVzdHJlYW0vbGl2ZXN0cmVhbS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxlQUFBO0VBQ0EsV0FBQTtFQUNBLFdBQUE7QUNDRjs7QURDQTtFQUNFLGFBQUE7RUFDQSxhQUFBO0VBQ0EsOEJBQUE7RUFDQSxxQkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDRSxrQkFBQTtFQUNGLFdBQUE7QUNFRjs7QURERTtFQUNFLFlBQUE7RUFDQSw4QkFBQTtFQUNBLG1CQUFBO0FDR0o7O0FEQUE7RUFDQSxnQ0FBQTtFQUNBLHlCQUFBO0VBQ0EsbUJBQUE7QUNHQTs7QUREQTtFQUNBLE9BQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLFNBQUE7RUFDQSxTQUFBO0FDSUE7O0FERkE7RUFDRSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsY0FBQTtBQ0tGOztBREpFO0VBQ0UsWUFBQTtFQUNBLG1CQUFBO0FDTUo7O0FETEk7RUFDRSxxQkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxpQkFBQTtBQ09OOztBREhBO0VBQ0UsYUFBQTtBQ01GOztBREpBO0VBQ0UscUJBQUE7RUFDQSxZQUFBO0VBQ0EseUNBQUE7QUNPRjs7QURMQTtFQUNFLHFCQUFBO0VBQ0EsWUFBQTtFQUNBLHVDQUFBO0FDUUY7O0FETkE7RUFDRSxTQUFBO0VBQ0EsY0FBQTtFQUNBLG1CQUFBO0VBQ0EsZ0RBQUE7QUNTRjs7QURQQTtFQUNFLDhCQUFBO0FDVUY7O0FEUkE7RUFDRSxxQkFBQTtFQUNBLFlBQUE7QUNXRiIsImZpbGUiOiJzcmMvYXBwL2xpdmVzdHJlYW0vbGl2ZXN0cmVhbS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubXktb3ZlcmxheSB7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgd2lkdGg6IDEwMCU7XG4gIHotaW5kZXg6IDIwO1xufVxuLm92ZXJsYXlfbWVzc2FnZSB7XG4gIGhlaWdodDogMzAwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW4tcmV2ZXJzZTsgXG4gIGFsaWduLWl0ZW1zOiBmbGV4LWVuZDtcbiAgb3ZlcmZsb3cteTogYXV0bztcbiAgcGFkZGluZy1sZWZ0OiA1NXB4O1xuICAgIHBhZGRpbmctcmlnaHQ6IDJweDtcbiAgei1pbmRleDogOTk7XG4gIHB7XG4gICAgcGFkZGluZzogOHB4O1xuICAgIGJhY2tncm91bmQ6IHJnYmEoMCwgMCwgMCwgMC40KTtcbiAgICBib3JkZXItcmFkaXVzOiA1MHB4O1xuICB9XG59XG4uaXRlbV9wcm9maWxle1xuLS1iYWNrZ3JvdW5kOiByZ2JhKDAsIDAsIDAsIDAuMyk7XG5ib3JkZXI6IDFweCBzb2xpZCMxZGMxZTY7XG5ib3JkZXItcmFkaXVzOiAzMHB4O1xufVxuLmNsb3NlX2ljb257XG56b29tOiAzLjA7XG5jb2xvcjogd2hpdGU7XG5wb3NpdGlvbjogZml4ZWQ7XG5yaWdodDogMiU7XG50b3A6IDguNSU7XG59XG4udGh1bW5haWxze1xuICBvdmVyZmxvdy14OiBzY3JvbGw7XG4gIG92ZXJmbG93LXk6IGhpZGRlbjtcbiAgbWFyZ2luLXRvcDogNSU7XG4gIC5saXN0LXRodW1ibmFpbHtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgICAuaW1nLXRodW1iIGltZ3tcbiAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICAgIHBhZGRpbmc6IDNweDtcbiAgICAgIHdpZHRoOiA1MHB4O1xuICAgICAgaGVpZ2h0OiA1MHB4O1xuICAgICAgbWFyZ2luOjAgMnB4IDAgMDsgXG4gICAgICBsaW5lLWhlaWdodDogNjBweDtcbiAgICB9XG4gIH1cbn1cbjo6LXdlYmtpdC1zY3JvbGxiYXIgeyBcbiAgZGlzcGxheTogbm9uZTsgXG59XG4ubXlIZWFydEZvbGxvd3tcbiAgd2lkdGg6IDIzJSAhaW1wb3J0YW50O1xuICBoZWlnaHQ6IGF1dG87XG4gIGNvbnRlbnQ6dXJsKFwiL2Fzc2V0cy9pY29uL2ZvbGxvd2VkLnBuZ1wiKTtcbn1cbi5teUhlYXJ0VW5mb2xsb3d7XG4gIHdpZHRoOiAyMyUgIWltcG9ydGFudDtcbiAgaGVpZ2h0OiBhdXRvO1xuICBjb250ZW50OnVybChcIi9hc3NldHMvaWNvbi9mb2xsb3cucG5nXCIpO1xufVxuLmx2bG51bXtcbiAgbWFyZ2luOiAwO1xuICBtYXJnaW4tdG9wOiA0JTtcbiAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbiAgZm9udC1mYW1pbHk6IFZlcmRhbmEsIEdlbmV2YSwgVGFob21hLCBzYW5zLXNlcmlmO1xufVxuaW9uLWl0ZW0ge1xuICAtLXBhZGRpbmctc3RhcnQ6IDAlICFpbXBvcnRhbnQ7XG59XG4uYnJpeHktaWNvbntcbiAgd2lkdGg6IDIzJSAhaW1wb3J0YW50O1xuICBoZWlnaHQ6IGF1dG87XG59IiwiLm15LW92ZXJsYXkge1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIHdpZHRoOiAxMDAlO1xuICB6LWluZGV4OiAyMDtcbn1cblxuLm92ZXJsYXlfbWVzc2FnZSB7XG4gIGhlaWdodDogMzAwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW4tcmV2ZXJzZTtcbiAgYWxpZ24taXRlbXM6IGZsZXgtZW5kO1xuICBvdmVyZmxvdy15OiBhdXRvO1xuICBwYWRkaW5nLWxlZnQ6IDU1cHg7XG4gIHBhZGRpbmctcmlnaHQ6IDJweDtcbiAgei1pbmRleDogOTk7XG59XG4ub3ZlcmxheV9tZXNzYWdlIHAge1xuICBwYWRkaW5nOiA4cHg7XG4gIGJhY2tncm91bmQ6IHJnYmEoMCwgMCwgMCwgMC40KTtcbiAgYm9yZGVyLXJhZGl1czogNTBweDtcbn1cblxuLml0ZW1fcHJvZmlsZSB7XG4gIC0tYmFja2dyb3VuZDogcmdiYSgwLCAwLCAwLCAwLjMpO1xuICBib3JkZXI6IDFweCBzb2xpZCAjMWRjMWU2O1xuICBib3JkZXItcmFkaXVzOiAzMHB4O1xufVxuXG4uY2xvc2VfaWNvbiB7XG4gIHpvb206IDM7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgcG9zaXRpb246IGZpeGVkO1xuICByaWdodDogMiU7XG4gIHRvcDogOC41JTtcbn1cblxuLnRodW1uYWlscyB7XG4gIG92ZXJmbG93LXg6IHNjcm9sbDtcbiAgb3ZlcmZsb3cteTogaGlkZGVuO1xuICBtYXJnaW4tdG9wOiA1JTtcbn1cbi50aHVtbmFpbHMgLmxpc3QtdGh1bWJuYWlsIHtcbiAgaGVpZ2h0OiAxMDAlO1xuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xufVxuLnRodW1uYWlscyAubGlzdC10aHVtYm5haWwgLmltZy10aHVtYiBpbWcge1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgcGFkZGluZzogM3B4O1xuICB3aWR0aDogNTBweDtcbiAgaGVpZ2h0OiA1MHB4O1xuICBtYXJnaW46IDAgMnB4IDAgMDtcbiAgbGluZS1oZWlnaHQ6IDYwcHg7XG59XG5cbjo6LXdlYmtpdC1zY3JvbGxiYXIge1xuICBkaXNwbGF5OiBub25lO1xufVxuXG4ubXlIZWFydEZvbGxvdyB7XG4gIHdpZHRoOiAyMyUgIWltcG9ydGFudDtcbiAgaGVpZ2h0OiBhdXRvO1xuICBjb250ZW50OiB1cmwoXCIvYXNzZXRzL2ljb24vZm9sbG93ZWQucG5nXCIpO1xufVxuXG4ubXlIZWFydFVuZm9sbG93IHtcbiAgd2lkdGg6IDIzJSAhaW1wb3J0YW50O1xuICBoZWlnaHQ6IGF1dG87XG4gIGNvbnRlbnQ6IHVybChcIi9hc3NldHMvaWNvbi9mb2xsb3cucG5nXCIpO1xufVxuXG4ubHZsbnVtIHtcbiAgbWFyZ2luOiAwO1xuICBtYXJnaW4tdG9wOiA0JTtcbiAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbiAgZm9udC1mYW1pbHk6IFZlcmRhbmEsIEdlbmV2YSwgVGFob21hLCBzYW5zLXNlcmlmO1xufVxuXG5pb24taXRlbSB7XG4gIC0tcGFkZGluZy1zdGFydDogMCUgIWltcG9ydGFudDtcbn1cblxuLmJyaXh5LWljb24ge1xuICB3aWR0aDogMjMlICFpbXBvcnRhbnQ7XG4gIGhlaWdodDogYXV0bztcbn0iXX0= */");

/***/ }),

/***/ "./src/app/livestream/livestream.page.ts":
/*!***********************************************!*\
  !*** ./src/app/livestream/livestream.page.ts ***!
  \***********************************************/
/*! exports provided: LivestreamPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LivestreamPage", function() { return LivestreamPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/android-permissions/ngx */ "./node_modules/@ionic-native/android-permissions/ngx/index.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var ngx_socket_io__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-socket-io */ "./node_modules/ngx-socket-io/fesm2015/ngx-socket-io.js");
/* harmony import */ var _liveprofile_liveprofile_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../liveprofile/liveprofile.page */ "./src/app/liveprofile/liveprofile.page.ts");
/* harmony import */ var _providers_credential_provider__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../providers/credential-provider */ "./src/providers/credential-provider.ts");









let LivestreamPage = class LivestreamPage {
    constructor(platform, androidPermissions, postPvdr, storage, socket, router, modalController) {
        //console.log("PLATFORM: "+this.platform.platforms());
        this.platform = platform;
        this.androidPermissions = androidPermissions;
        this.postPvdr = postPvdr;
        this.storage = storage;
        this.socket = socket;
        this.router = router;
        this.modalController = modalController;
        this.message = '';
        this.messages = [];
        this.viewersPic = [];
        this.webRTCAdaptor = null;
        //webRTCAdaptor: any;
        this.streamId = '';
        if (this.platform.is('cordova')) {
            this.platform.ready().then(() => {
                this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.CAMERA).then(result => console.log('Has permission? ', result.hasPermission), err => this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.CAMERA, this.androidPermissions.PERMISSION.RECORD_AUDIO, this.androidPermissions.PERMISSION.MODIFY_AUDIO_SETTINGS]));
                this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.MODIFY_AUDIO_SETTINGS).then(result => console.log('Has audio permission? ', result.hasPermission), err => this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.CAMERA, this.androidPermissions.PERMISSION.RECORD_AUDIO, this.androidPermissions.PERMISSION.MODIFY_AUDIO_SETTINGS]));
                this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.CAMERA, this.androidPermissions.PERMISSION.RECORD_AUDIO, this.androidPermissions.PERMISSION.MODIFY_AUDIO_SETTINGS]);
            });
        }
        else {
            alert("Cannot play Live streaming at the moment. Please contact Brixy Live at brixylive.com.");
        }
    }
    ngAfterViewInit() {
        this.start();
        this.plotDetails();
    }
    plotDetails() {
        this.storage.get("user_id").then((user_id) => {
            let body = {
                action: 'getUser_liveData',
                user_id: user_id
            };
            this.postPvdr.postData(body, 'credentials-api.php').subscribe((data) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                if (data.success) {
                    for (const key in data.result) {
                        this.user_id = data.result[key].user_id;
                        this.fname = data.result[key].fname;
                        this.lname = data.result[key].lname;
                        this.city = data.result[key].city;
                        this.user_photo = (data.result[key].profile_photo == '') ? '' :
                            this.postPvdr.myServer() + "/brixy-live/images/" + data.result[key].profile_photo;
                        this.user_level = data.result[key].user_level;
                        this.followers = data.result[key].followers;
                        this.following = data.result[key].following;
                    }
                }
            }));
        }); // end of user_id
    }
    goTab5() {
        this.router.navigate(['tabs/tab5']);
    }
    ionViewWillLeave() {
        this.goHome();
    }
    goHome() {
        this.storage.get("user_id").then((user_id) => {
            let body2 = {
                action: 'updateLive',
                user_id: user_id
            };
            this.postPvdr.postData(body2, 'credentials-api.php').subscribe((data) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                if (data.success) {
                    //this.webRTCAdaptor.stop(this.streamId);
                    this.router.navigate(['tabs']);
                    this.webRTCAdaptor.stop(this.streamId);
                }
            }));
        });
    }
    openliveprofile() {
        this.storage.get("user_id").then((user_id) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _liveprofile_liveprofile_page__WEBPACK_IMPORTED_MODULE_7__["LiveprofilePage"],
                cssClass: 'liveprofilemodalstyle',
                componentProps: {
                    liveStreamProfileId: user_id,
                }
            });
            return yield modal.present();
        }));
    }
    startPublishing(streamK) {
        this.webRTCAdaptor.publish(streamK);
    }
    start() {
        var pc_config = {
            'iceServers': [{
                    'urls': 'stun:stun.l.google.com:19302'
                }]
        };
        var sdpConstraints = {
            OfferToReceiveAudio: false,
            OfferToReceiveVideo: false
        };
        var mediaConstraints = {
            video: true,
            audio: true
        };
        // var appName = location.pathname.substring(0, location.pathname.lastIndexOf("/")+1);
        var websocketURL = "ws://livebrixylive.com:5080/WebRTCAppEE/websocket";
        var appName = location.pathname.substring(0, location.pathname.lastIndexOf("/") + 1);
        var path = location.hostname + ":" + location.port + appName + "websocket";
        var websocketURL123 = "ws://" + path;
        console.log("location:" + location.protocol);
        console.log("path:" + path);
        if (location.protocol.startsWith("https")) {
            websocketURL123 = "wss://" + path;
        }
        this.storage.get("broadcast_topic").then((broadcast_topic) => {
            console.log("broadcast:" + broadcast_topic);
            this.storage.get("broadcast_type").then((broadcast_type) => {
                this.storage.get("user_id").then((user_id) => {
                    //var regExpr = /[^a-zA-Z0-9 ]/g;
                    // var streamName = user_id;
                    this.login_user_id = user_id;
                    let dateTime = new Date();
                    let body = {
                        action: "publisher",
                        user_id: user_id,
                        livestream_type: broadcast_type,
                        date_live: dateTime,
                        time_started: dateTime,
                        broadcast_topic: broadcast_topic
                    };
                    console.log("publisher:" + JSON.stringify(body));
                    this.postPvdr.postData(body, 'credentials-api.php').subscribe(data => {
                        if (data.success) {
                            console.log("stream_id:" + data.result);
                            console.log("this.user_id:" + user_id);
                            this.streamId = data.result;
                            var webRTCAdaptor = new WebRTCAdaptor({
                                websocket_url: websocketURL,
                                mediaConstraints: mediaConstraints,
                                peerconnection_config: pc_config,
                                sdp_constraints: sdpConstraints,
                                localVideoId: "localVideo",
                                // isPlayMode: true,
                                callback: function (info) {
                                    if (info == "initialized") {
                                        console.log("initialized1:" + data.result);
                                        //webRTCAdaptor.publish(streamName);
                                        console.log("after initialized");
                                        webRTCAdaptor.publish(data.result);
                                        //this.startPublishing(data.result);
                                        console.log("after published");
                                    }
                                    console.log("info content:" + info);
                                },
                                callbackError: function (error) {
                                    //some of the possible errors, NotFoundError, SecurityError,PermissionDeniedError
                                    console.log("error callback: " + error);
                                    alert("There was a problem occured while initializing the stream. The required version of your device may not be compatible for streaming or please allow the permissions (Camera and Audio).");
                                }
                            });
                            // code for chat
                            let body2 = {
                                action: 'getuserdata',
                                user_id: this.login_user_id
                            };
                            this.postPvdr.postData(body2, 'credentials-api.php').subscribe((data) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                                if (data.success) {
                                    if (data.result.nickname == "") {
                                        if (data.result.login_type_id == "3") {
                                            this.login_nickname = data.result.mobile_num;
                                        }
                                        else if (data.result.login_type_id == "4") {
                                            this.login_nickname = data.result.email;
                                        }
                                        else {
                                            this.login_nickname = data.result.fname;
                                        }
                                    }
                                    else {
                                        this.login_nickname = data.result.nickname;
                                    }
                                    this.login_profile_photo = data.result.photo;
                                    this.socket.connect();
                                    // Join chatroom
                                    console.log("user_id:" + this.user_id + "room:" + this.streamId);
                                    this.socket.emit('joinRoom', {
                                        user_id: this.login_nickname,
                                        room: this.streamId,
                                        profile_picture: this.login_profile_photo
                                    });
                                    this.socket.fromEvent('message').subscribe(message => {
                                        this.messages.push(message);
                                        console.log("human og send" + JSON.stringify(message));
                                    });
                                    this.socket.fromEvent('joinRoomMessage').subscribe(message => {
                                        console.log("message from server:" + JSON.stringify(message));
                                        this.messages.push(message);
                                        this.addPictureViewer(message['profile_picture']);
                                    });
                                    this.socket.fromEvent('leaveRoom').subscribe(message => {
                                        console.log("message from server leave:" + JSON.stringify(message));
                                        this.messages.push(message);
                                    });
                                } // end of success
                            }));
                        }
                    });
                }); // end of storage user_id
            }); // end of storage broadcast_type
        }); // end of storage broadcast_topic
    }
    addPictureViewer(joinPic) {
        console.log("picture of those who join:" + joinPic);
        var viewPic = joinPic == '' ? "assets/icon/brixylogo.png" :
            this.postPvdr.myServer() + "/brixy-live/images/" + joinPic;
        let body = {
            viewerPic: viewPic
        };
        this.viewersPic.push(body);
        console.log("vi:" + JSON.stringify(this.viewersPic));
    }
    sendMessage() {
        this.socket.emit('send-message', { user_id: this.login_nickname + " : ", room: this.streamId, text: this.message });
        let body = {
            action: 'insertConvoInGC',
            user_id: this.login_user_id,
            stream_id: this.streamId,
            message: this.message
        };
        this.postPvdr.postData(body, 'messages.php').subscribe((data) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            if (data.success) {
            }
        }));
        this.message = '';
    }
};
LivestreamPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"] },
    { type: _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_4__["AndroidPermissions"] },
    { type: _providers_credential_provider__WEBPACK_IMPORTED_MODULE_8__["PostProvider"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"] },
    { type: ngx_socket_io__WEBPACK_IMPORTED_MODULE_6__["Socket"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"] }
];
LivestreamPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-livestream',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./livestream.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/livestream/livestream.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./livestream.page.scss */ "./src/app/livestream/livestream.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"],
        _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_4__["AndroidPermissions"],
        _providers_credential_provider__WEBPACK_IMPORTED_MODULE_8__["PostProvider"],
        _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"],
        ngx_socket_io__WEBPACK_IMPORTED_MODULE_6__["Socket"],
        _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"]])
], LivestreamPage);



/***/ })

}]);
//# sourceMappingURL=livestream-livestream-module-es2015.js.map