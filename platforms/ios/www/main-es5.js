function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"], {
  /***/
  "./$$_lazy_route_resource lazy recursive":
  /*!******************************************************!*\
    !*** ./$$_lazy_route_resource lazy namespace object ***!
    \******************************************************/

  /*! no static exports found */

  /***/
  function $$_lazy_route_resourceLazyRecursive(module, exports, __webpack_require__) {
    var map = {
      "../categories/categories.module": ["./src/app/categories/categories.module.ts", "categories-categories-module"],
      "../communityforum/communityforum.module": ["./src/app/communityforum/communityforum.module.ts", "communityforum-communityforum-module"],
      "../help/help.module": ["./src/app/help/help.module.ts", "help-help-module"],
      "../itemposted/itemposted.module": ["./src/app/itemposted/itemposted.module.ts", "common", "itemposted-itemposted-module"],
      "../myaccount/myaccount.module": ["./src/app/myaccount/myaccount.module.ts", "myaccount-myaccount-module"],
      "../offers/offers.module": ["./src/app/offers/offers.module.ts", "common", "offers-offers-module"],
      "../sellersifollow/sellersifollow.module": ["./src/app/sellersifollow/sellersifollow.module.ts", "common", "sellersifollow-sellersifollow-module"],
      "../tab1/tab1.module": ["./src/app/tab1/tab1.module.ts", "common", "tab1-tab1-module"],
      "../tab3/tab3.module": ["./src/app/tab3/tab3.module.ts", "common", "tab3-tab3-module"],
      "../tab5/tab5.module": ["./src/app/tab5/tab5.module.ts", "common", "tab5-tab5-module"],
      "../userpolicy/userpolicy.module": ["./src/app/userpolicy/userpolicy.module.ts"],
      "../viewprofile/viewprofile.module": ["./src/app/viewprofile/viewprofile.module.ts", "common", "viewprofile-viewprofile-module"],
      "./ads/ads.module": ["./src/app/ads/ads.module.ts", "ads-ads-module"],
      "./allgifts/allgifts.module": ["./src/app/allgifts/allgifts.module.ts"],
      "./brixyloads/brixyloads.module": ["./src/app/brixyloads/brixyloads.module.ts", "brixyloads-brixyloads-module"],
      "./brixyvideo/brixyvideo.module": ["./src/app/brixyvideo/brixyvideo.module.ts", "brixyvideo-brixyvideo-module"],
      "./choose/choose.module": ["./src/app/choose/choose.module.ts", "choose-choose-module"],
      "./editprofile/editprofile.module": ["./src/app/editprofile/editprofile.module.ts", "editprofile-editprofile-module"],
      "./email-code/email-code.module": ["./src/app/email-code/email-code.module.ts", "email-code-email-code-module"],
      "./email-login/email-login.module": ["./src/app/email-login/email-login.module.ts", "email-login-email-login-module"],
      "./guide/guide.module": ["./src/app/guide/guide.module.ts", "guide-guide-module"],
      "./historygiftpreview/historygiftpreview.module": ["./src/app/historygiftpreview/historygiftpreview.module.ts"],
      "./historypreview/historypreview.module": ["./src/app/historypreview/historypreview.module.ts"],
      "./liveprofile/liveprofile.module": ["./src/app/liveprofile/liveprofile.module.ts"],
      "./livestream/livestream.module": ["./src/app/livestream/livestream.module.ts", "livestream-livestream-module"],
      "./message/message.module": ["./src/app/message/message.module.ts", "common", "message-message-module"],
      "./mobilee/mobilee.module": ["./src/app/mobilee/mobilee.module.ts", "mobilee-mobilee-module"],
      "./myaccount/myaccount.module": ["./src/app/myaccount/myaccount.module.ts", "myaccount-myaccount-module"],
      "./mybroadcast/mybroadcast.module": ["./src/app/mybroadcast/mybroadcast.module.ts", "mybroadcast-mybroadcast-module"],
      "./paymentrequest/paymentrequest.module": ["./src/app/paymentrequest/paymentrequest.module.ts", "paymentrequest-paymentrequest-module"],
      "./privacy/privacy.module": ["./src/app/privacy/privacy.module.ts", "privacy-privacy-module"],
      "./readytogolive/readytogolive.module": ["./src/app/readytogolive/readytogolive.module.ts", "readytogolive-readytogolive-module"],
      "./register/register.module": ["./src/app/register/register.module.ts"],
      "./sendgiftbronzemodal/sendgiftbronzemodal.module": ["./src/app/sendgiftbronzemodal/sendgiftbronzemodal.module.ts"],
      "./sendgiftgoldmodal/sendgiftgoldmodal.module": ["./src/app/sendgiftgoldmodal/sendgiftgoldmodal.module.ts"],
      "./sendgiftpopularmodal/sendgiftpopularmodal.module": ["./src/app/sendgiftpopularmodal/sendgiftpopularmodal.module.ts"],
      "./sendgiftsilvermodal/sendgiftsilvermodal.module": ["./src/app/sendgiftsilvermodal/sendgiftsilvermodal.module.ts"],
      "./settings/settings.module": ["./src/app/settings/settings.module.ts", "settings-settings-module"],
      "./tab3/tab3.module": ["./src/app/tab3/tab3.module.ts", "common", "tab3-tab3-module"],
      "./tab4/tab4.module": ["./src/app/tab4/tab4.module.ts", "tab4-tab4-module"],
      "./tab5/tab5.module": ["./src/app/tab5/tab5.module.ts", "common", "tab5-tab5-module"],
      "./tab6/tab6.module": ["./src/app/tab6/tab6.module.ts", "common", "tab6-tab6-module"],
      "./tabs/tabs.module": ["./src/app/tabs/tabs.module.ts", "tabs-tabs-module"],
      "./terms/terms.module": ["./src/app/terms/terms.module.ts", "terms-terms-module"],
      "./termsconditions/termsconditions.module": ["./src/app/termsconditions/termsconditions.module.ts", "termsconditions-termsconditions-module"],
      "./termsconditionslogin/termsconditionslogin.module": ["./src/app/termsconditionslogin/termsconditionslogin.module.ts", "termsconditionslogin-termsconditionslogin-module"],
      "./uploadphoto/uploadphoto.module": ["./src/app/uploadphoto/uploadphoto.module.ts", "uploadphoto-uploadphoto-module"],
      "./viewlive/viewlive.module": ["./src/app/viewlive/viewlive.module.ts", "viewlive-viewlive-module"],
      "./wallet/wallet.module": ["./src/app/wallet/wallet.module.ts", "wallet-wallet-module"]
    };

    function webpackAsyncContext(req) {
      if (!__webpack_require__.o(map, req)) {
        return Promise.resolve().then(function () {
          var e = new Error("Cannot find module '" + req + "'");
          e.code = 'MODULE_NOT_FOUND';
          throw e;
        });
      }

      var ids = map[req],
          id = ids[0];
      return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function () {
        return __webpack_require__(id);
      });
    }

    webpackAsyncContext.keys = function webpackAsyncContextKeys() {
      return Object.keys(map);
    };

    webpackAsyncContext.id = "./$$_lazy_route_resource lazy recursive";
    module.exports = webpackAsyncContext;
    /***/
  },

  /***/
  "./node_modules/@ionic-super-tabs/core/dist/esm lazy recursive ^\\.\\/.*\\.entry\\.js$ include: \\.entry\\.js$ exclude: \\.system\\.entry\\.js$":
  /*!****************************************************************************************************************************************************!*\
    !*** ./node_modules/@ionic-super-tabs/core/dist/esm lazy ^\.\/.*\.entry\.js$ include: \.entry\.js$ exclude: \.system\.entry\.js$ namespace object ***!
    \****************************************************************************************************************************************************/

  /*! no static exports found */

  /***/
  function node_modulesIonicSuperTabsCoreDistEsmLazyRecursiveEntryJs$IncludeEntryJs$ExcludeSystemEntryJs$(module, exports, __webpack_require__) {
    var map = {
      "./super-tab-button_2.entry.js": ["./node_modules/@ionic-super-tabs/core/dist/esm/super-tab-button_2.entry.js", "common", 0],
      "./super-tab-indicator.entry.js": ["./node_modules/@ionic-super-tabs/core/dist/esm/super-tab-indicator.entry.js", 1],
      "./super-tab_3.entry.js": ["./node_modules/@ionic-super-tabs/core/dist/esm/super-tab_3.entry.js", "common", 2]
    };

    function webpackAsyncContext(req) {
      if (!__webpack_require__.o(map, req)) {
        return Promise.resolve().then(function () {
          var e = new Error("Cannot find module '" + req + "'");
          e.code = 'MODULE_NOT_FOUND';
          throw e;
        });
      }

      var ids = map[req],
          id = ids[0];
      return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function () {
        return __webpack_require__(id);
      });
    }

    webpackAsyncContext.keys = function webpackAsyncContextKeys() {
      return Object.keys(map);
    };

    webpackAsyncContext.id = "./node_modules/@ionic-super-tabs/core/dist/esm lazy recursive ^\\.\\/.*\\.entry\\.js$ include: \\.entry\\.js$ exclude: \\.system\\.entry\\.js$";
    module.exports = webpackAsyncContext;
    /***/
  },

  /***/
  "./node_modules/@ionic/core/dist/esm lazy recursive ^\\.\\/.*\\.entry\\.js$ include: \\.entry\\.js$ exclude: \\.system\\.entry\\.js$":
  /*!*****************************************************************************************************************************************!*\
    !*** ./node_modules/@ionic/core/dist/esm lazy ^\.\/.*\.entry\.js$ include: \.entry\.js$ exclude: \.system\.entry\.js$ namespace object ***!
    \*****************************************************************************************************************************************/

  /*! no static exports found */

  /***/
  function node_modulesIonicCoreDistEsmLazyRecursiveEntryJs$IncludeEntryJs$ExcludeSystemEntryJs$(module, exports, __webpack_require__) {
    var map = {
      "./ion-action-sheet-controller_8.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-action-sheet-controller_8.entry.js", "common", 3],
      "./ion-action-sheet-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-action-sheet-ios.entry.js", "common", 4],
      "./ion-action-sheet-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-action-sheet-md.entry.js", "common", 5],
      "./ion-alert-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-alert-ios.entry.js", "common", 6],
      "./ion-alert-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-alert-md.entry.js", "common", 7],
      "./ion-app_8-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-app_8-ios.entry.js", "common", 8],
      "./ion-app_8-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-app_8-md.entry.js", "common", 9],
      "./ion-avatar_3-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-avatar_3-ios.entry.js", "common", 10],
      "./ion-avatar_3-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-avatar_3-md.entry.js", "common", 11],
      "./ion-back-button-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-back-button-ios.entry.js", "common", 12],
      "./ion-back-button-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-back-button-md.entry.js", "common", 13],
      "./ion-backdrop-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-backdrop-ios.entry.js", 14],
      "./ion-backdrop-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-backdrop-md.entry.js", 15],
      "./ion-button_2-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-button_2-ios.entry.js", "common", 16],
      "./ion-button_2-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-button_2-md.entry.js", "common", 17],
      "./ion-card_5-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-card_5-ios.entry.js", "common", 18],
      "./ion-card_5-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-card_5-md.entry.js", "common", 19],
      "./ion-checkbox-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-checkbox-ios.entry.js", "common", 20],
      "./ion-checkbox-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-checkbox-md.entry.js", "common", 21],
      "./ion-chip-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-chip-ios.entry.js", "common", 22],
      "./ion-chip-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-chip-md.entry.js", "common", 23],
      "./ion-col_3.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-col_3.entry.js", 24],
      "./ion-datetime_3-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-datetime_3-ios.entry.js", "common", 25],
      "./ion-datetime_3-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-datetime_3-md.entry.js", "common", 26],
      "./ion-fab_3-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-fab_3-ios.entry.js", "common", 27],
      "./ion-fab_3-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-fab_3-md.entry.js", "common", 28],
      "./ion-img.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-img.entry.js", 29],
      "./ion-infinite-scroll_2-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-infinite-scroll_2-ios.entry.js", "common", 30],
      "./ion-infinite-scroll_2-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-infinite-scroll_2-md.entry.js", "common", 31],
      "./ion-input-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-input-ios.entry.js", "common", 32],
      "./ion-input-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-input-md.entry.js", "common", 33],
      "./ion-item-option_3-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-item-option_3-ios.entry.js", "common", 34],
      "./ion-item-option_3-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-item-option_3-md.entry.js", "common", 35],
      "./ion-item_8-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-item_8-ios.entry.js", "common", 36],
      "./ion-item_8-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-item_8-md.entry.js", "common", 37],
      "./ion-loading-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-loading-ios.entry.js", "common", 38],
      "./ion-loading-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-loading-md.entry.js", "common", 39],
      "./ion-menu_4-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-menu_4-ios.entry.js", "common", 40],
      "./ion-menu_4-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-menu_4-md.entry.js", "common", 41],
      "./ion-modal-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-modal-ios.entry.js", "common", 42],
      "./ion-modal-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-modal-md.entry.js", "common", 43],
      "./ion-nav_5.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-nav_5.entry.js", "common", 44],
      "./ion-popover-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-popover-ios.entry.js", "common", 45],
      "./ion-popover-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-popover-md.entry.js", "common", 46],
      "./ion-progress-bar-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-progress-bar-ios.entry.js", "common", 47],
      "./ion-progress-bar-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-progress-bar-md.entry.js", "common", 48],
      "./ion-radio_2-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-radio_2-ios.entry.js", "common", 49],
      "./ion-radio_2-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-radio_2-md.entry.js", "common", 50],
      "./ion-range-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-range-ios.entry.js", "common", 51],
      "./ion-range-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-range-md.entry.js", "common", 52],
      "./ion-refresher_2-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-refresher_2-ios.entry.js", "common", 53],
      "./ion-refresher_2-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-refresher_2-md.entry.js", "common", 54],
      "./ion-reorder_2-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-reorder_2-ios.entry.js", "common", 55],
      "./ion-reorder_2-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-reorder_2-md.entry.js", "common", 56],
      "./ion-ripple-effect.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-ripple-effect.entry.js", 57],
      "./ion-route_4.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-route_4.entry.js", "common", 58],
      "./ion-searchbar-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-searchbar-ios.entry.js", "common", 59],
      "./ion-searchbar-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-searchbar-md.entry.js", "common", 60],
      "./ion-segment_2-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-segment_2-ios.entry.js", "common", 61],
      "./ion-segment_2-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-segment_2-md.entry.js", "common", 62],
      "./ion-select_3-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-select_3-ios.entry.js", "common", 63],
      "./ion-select_3-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-select_3-md.entry.js", "common", 64],
      "./ion-slide_2-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-slide_2-ios.entry.js", 65],
      "./ion-slide_2-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-slide_2-md.entry.js", 66],
      "./ion-spinner.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-spinner.entry.js", "common", 67],
      "./ion-split-pane-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-split-pane-ios.entry.js", 68],
      "./ion-split-pane-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-split-pane-md.entry.js", 69],
      "./ion-tab-bar_2-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-tab-bar_2-ios.entry.js", "common", 70],
      "./ion-tab-bar_2-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-tab-bar_2-md.entry.js", "common", 71],
      "./ion-tab_2.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-tab_2.entry.js", "common", 72],
      "./ion-text.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-text.entry.js", "common", 73],
      "./ion-textarea-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-textarea-ios.entry.js", "common", 74],
      "./ion-textarea-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-textarea-md.entry.js", "common", 75],
      "./ion-toast-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-toast-ios.entry.js", "common", 76],
      "./ion-toast-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-toast-md.entry.js", "common", 77],
      "./ion-toggle-ios.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-toggle-ios.entry.js", "common", 78],
      "./ion-toggle-md.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-toggle-md.entry.js", "common", 79],
      "./ion-virtual-scroll.entry.js": ["./node_modules/@ionic/core/dist/esm/ion-virtual-scroll.entry.js", 80]
    };

    function webpackAsyncContext(req) {
      if (!__webpack_require__.o(map, req)) {
        return Promise.resolve().then(function () {
          var e = new Error("Cannot find module '" + req + "'");
          e.code = 'MODULE_NOT_FOUND';
          throw e;
        });
      }

      var ids = map[req],
          id = ids[0];
      return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function () {
        return __webpack_require__(id);
      });
    }

    webpackAsyncContext.keys = function webpackAsyncContextKeys() {
      return Object.keys(map);
    };

    webpackAsyncContext.id = "./node_modules/@ionic/core/dist/esm lazy recursive ^\\.\\/.*\\.entry\\.js$ include: \\.entry\\.js$ exclude: \\.system\\.entry\\.js$";
    module.exports = webpackAsyncContext;
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/allgifts/allgifts.page.html":
  /*!***********************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/allgifts/allgifts.page.html ***!
    \***********************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppAllgiftsAllgiftsPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n  <ion-header mode=\"ios\">\n    <ion-toolbar mode=\"ios\">\n      <ion-buttons slot=\"primary\" >\n        <ion-button>\n          <ion-icon slot=\"icon-only\" color=\"primary\" name=\"gift\" mode=\"ios\"></ion-icon>\n        </ion-button>\n      </ion-buttons>\n\n        <ion-title>Send Gifts</ion-title>\n\n        <ion-buttons slot=\"secondary\" (click)=\"closemodal()\">\n          <ion-button>\n              <ion-icon name=\"close\" slot=\"icon-only\" mode=\"ios\"></ion-icon>\n          </ion-button>\n        </ion-buttons>\n      </ion-toolbar>\n    <ion-segment style=\"padding: 5px\" color=\"primary\" mode=\"ios\" (ionChange)=\"segmentChanged()\" [(ngModel)]=\"segment\">\n          <ion-segment-button layout=\"icon-start\" mode=\"ios\" value=\"0\">\n              <ion-label>Bronze</ion-label>\n          </ion-segment-button>\n          <ion-segment-button layout=\"icon-start\" mode=\"ios\" value=\"1\">\n              <ion-label>Silver</ion-label>\n          </ion-segment-button>\n          <ion-segment-button layout=\"icon-start\" mode=\"ios\" value=\"2\">\n              <ion-label>Gold</ion-label>\n          </ion-segment-button>\n          <ion-segment-button layout=\"icon-start\" mode=\"ios\" value=\"3\">\n              <ion-label>Popular</ion-label>\n          </ion-segment-button>\n      </ion-segment>\n    </ion-header>\n<ion-content mode=\"ios\" no-padding>\n  <ion-slides #slides (ionSlideDidChange)=\"slideChanged()\" >\n          <ion-slide>\n              <!-- Bronze -->\n              <ion-list  mode=\"ios\" style=\"width: 100%;\" padding>\n                <ion-item  mode=\"ios\" *ngFor=\"let w of bronzeList;let i=index\" (click)=\"openSilver(w)\">\n                     <ion-avatar  mode=\"ios\" slot=\"start\" >\n                          <img [src]=\"w.image\" >  \n                     </ion-avatar>\n                      <ion-label  mode=\"ios\" no-padding>\n                        <h4  mode=\"ios\" >{{w.name}}</h4>\n                      </ion-label>\n                      <div mode=\"ios\" slot=\"end\" class=\"lvlnum\">{{w.price}}</div>\n                      <div style=\"display: inline-block;\">\n                        <img class=\"lvlicon\" src=\"assets/icon/dollar.png\"></div>  \n                </ion-item>\n            </ion-list>\n          </ion-slide>\n          <ion-slide>\n              <!-- Silver -->\n              <ion-list  mode=\"ios\" style=\"width: 100%;\" padding>\n                  <ion-item  mode=\"ios\" *ngFor=\"let x of silverList;let i=index\" (click)=\"openSilver(x)\">\n                      <ion-avatar  mode=\"ios\" slot=\"start\">\n                          <img [src]=\"x.image\" >  \n                      </ion-avatar>\n                        <ion-label mode=\"ios\"  no-padding>\n                          <h4  mode=\"ios\" >{{x.name}}</h4>\n                        </ion-label>\n                        <div mode=\"ios\" slot=\"end\" class=\"lvlnum\">{{x.price}}</div>\n                      <div style=\"display: inline-block;\">\n                        <img class=\"lvlicon\" src=\"assets/icon/dollar.png\"></div> \n                  </ion-item>\n              </ion-list>\n          </ion-slide>\n          <ion-slide>\n              <!-- Gold -->\n              <ion-list  mode=\"ios\" style=\"width: 100%;\" padding>\n                  <!-- (click)=\"openGold(y)\" -->\n                  <ion-item  mode=\"ios\" *ngFor=\"let y of goldList;let i=index\" (click)=\"openSilver(y)\">\n                       <ion-avatar  mode=\"ios\" slot=\"start\">\n                          <img [src]=\"y.image\">  \n                       </ion-avatar>\n                        <ion-label  mode=\"ios\"  no-padding>\n                          <h4  mode=\"ios\" >{{y.name}}</h4>\n                        </ion-label>\n                        <div mode=\"ios\" slot=\"end\" class=\"lvlnum\">{{y.price}}</div>\n                      <div style=\"display: inline-block;\">\n                        <img class=\"lvlicon\" src=\"assets/icon/dollar.png\"></div> \n                  </ion-item>\n              </ion-list>\n          </ion-slide>\n          <ion-slide>\n              <!-- Popular -->\n              <ion-list  mode=\"ios\" style=\"width: 100%;\" padding>\n                  <!-- (click)=\"openPopular(z)\" -->\n                  <ion-item  mode=\"ios\" *ngFor=\"let z of popularList;let i=index\" (click)=\"openSilver(z)\" >\n                       <ion-avatar  mode=\"ios\" slot=\"start\">\n                          <img [src]=\"z.image\">  \n                       </ion-avatar>\n                        <ion-label  mode=\"ios\" no-padding >\n                          <h4  mode=\"ios\">{{z.name}}</h4>\n                        </ion-label>\n                        <div mode=\"ios\" slot=\"end\" class=\"lvlnum\">{{z.price}}</div>\n                      <div style=\"display: inline-block;\">\n                        <img class=\"lvlicon\" src=\"assets/icon/dollar.png\"></div> \n                  </ion-item>\n              </ion-list>\n          </ion-slide>\n  </ion-slides>\n</ion-content>  \n<ion-footer text-center mode=\"ios\" style=\"--ion-background-color: transparent\">\n      <ion-toolbar mode=\"ios\" style=\"--background: transparent\">\n        <ion-buttons slot=\"secondary\">\n          <ion-button>\n            <img class=\"lvlicon2\" src=\"assets/icon/dollar.png\">\n            <ion-text class=\"lvlnum2\">100</ion-text>\n          </ion-button>\n        </ion-buttons>\n        <ion-buttons slot=\"primary\">\n          <ion-button>\n            <img class=\"lvlicon2\" src=\"assets/icon/star.png\">\n            <ion-text class=\"lvlnum2\">10</ion-text>\n          </ion-button>\n        </ion-buttons>\n      </ion-toolbar>\n    </ion-footer>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html":
  /*!**************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
    \**************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppAppComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-app>\n  <ion-router-outlet></ion-router-outlet>\n</ion-app>\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/categoriessub/categoriessub.page.html":
  /*!*********************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/categoriessub/categoriessub.page.html ***!
    \*********************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppCategoriessubCategoriessubPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header mode=\"ios\" no-border>\n  <ion-toolbar mode=\"ios\">\n      <ion-buttons slot=\"secondary\" (click)=\"goBack()\">\n        <ion-button mode=\"ios\" style=\"font-size: 15px;\">\n            Back\n        </ion-button>\n      </ion-buttons>\n\n      <ion-title color=\"secondary\">Choose Category</ion-title>\n    </ion-toolbar>\n</ion-header>\n<ion-content>\n\n\n\n  <ion-grid class=\"grid-categories\">\n    <ion-row *ngFor=\"let category of categoryList; let i = index\" style=\"padding: 5px;\" >\n      <ion-col (click)=\"selected(category.id, category.category)\" *ngIf=\"i % 2 == 0\">\n        <ion-card mode=\"ios\" class=\"for_card\">\n          <div *ngIf=\"!category.category_photo\">\n            <img src=\"assets/greenthumb-images/greenthumblogo.png\">\n          </div>\n          <div *ngIf=\"category.category_photo\">\n            <img [src] = \"category.category_photo\">\n          </div>\n          <ion-card-content class=\"for_cardcontent\">\n            {{category.category}}\n          </ion-card-content>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n  <ion-grid class=\"grid-categories\">\n    <ion-row *ngFor=\"let category of categoryList; let i = index\" style=\"padding: 5px;\">\n      <ion-col (click)=\"selected(category.id, category.category)\" *ngIf=\"i % 2 != 0\">\n        <ion-card mode=\"ios\" class=\"for_card\">\n          <div *ngIf=\"!category.category_photo\">\n            <img src=\"assets/greenthumb-images/greenthumblogo.png\">\n          </div>\n          <div *ngIf=\"category.category_photo\">\n            <img [src] = \"category.category_photo\">\n          </div>\n          <ion-card-content class=\"for_cardcontent\">\n            {{category.category}}\n          </ion-card-content>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n</ion-content>\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/giftbronze/giftbronze.component.html":
  /*!********************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/giftbronze/giftbronze.component.html ***!
    \********************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppGiftbronzeGiftbronzeComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n<ion-segment padding color=\"primary\" mode=\"ios\" style=\"padding-bottom: 0;\">\n    <ion-segment-button layout=\"icon-start\" mode=\"ios\" value=\"0\" checked>\n        <ion-label>Bronze</ion-label>\n    </ion-segment-button>\n</ion-segment>\n<ion-content scrollable=\"true\" mode=\"ios\">\n        <ion-list  mode=\"ios\">\n            <ion-item  mode=\"ios\" *ngFor=\"let x of bronzeGiftList;let i=index\" (click)=\"openBronze(x)\">\n                 <ion-avatar  mode=\"ios\" slot=\"start\" style=\"border-radius: 0;width:20% !important;  \n                 height : 35% !important;  \n                 max-width: 20px !important;  \n                 max-height: 35px !important;\">\n                        <img [src]=\"x.image\" style=\"border-radius: 0;\">  \n                 </ion-avatar>\n                  <ion-label  mode=\"ios\"  no-padding>\n                    <h4  mode=\"ios\"  style=\"font-size: 11px;\">{{x.name}}</h4>\n                  </ion-label>\n                  <p  mode=\"ios\" slot=\"end\" style=\"font-size: 13px;\">{{x.price}}\n                     <ion-icon name=\"logo-bitcoin\" color=\"warning\"></ion-icon></p>\n            </ion-item>\n        </ion-list>\n</ion-content>\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/giftgold/giftgold.component.html":
  /*!****************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/giftgold/giftgold.component.html ***!
    \****************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppGiftgoldGiftgoldComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n<ion-segment padding color=\"primary\" mode=\"ios\" style=\"padding-bottom: 0;\">\n    <ion-segment-button layout=\"icon-start\" mode=\"ios\" value=\"0\" checked>\n        <ion-label>Gold</ion-label>\n    </ion-segment-button>\n</ion-segment>\n<ion-content scrollable=\"true\" mode=\"ios\">\n        <ion-list  mode=\"ios\">\n                <!-- (click)=\"openGold(x)\" -->\n            <ion-item  mode=\"ios\" *ngFor=\"let x of goldGiftList;let i=index\" >\n                 <ion-avatar  mode=\"ios\" slot=\"start\" style=\"border-radius: 0;width:20% !important;  \n                 height : 35% !important;  \n                 max-width: 20px !important;  \n                 max-height: 35px !important;\">\n                        <img [src]=\"x.image\" style=\"border-radius: 0;\">  \n                 </ion-avatar>\n                  <ion-label  mode=\"ios\"  no-padding>\n                    <h4  mode=\"ios\"  style=\"font-size: 11px;\">{{x.name}}</h4>\n                  </ion-label>\n                  <p  mode=\"ios\" slot=\"end\" style=\"font-size: 13px;\">{{x.price}}\n                     <ion-icon name=\"logo-bitcoin\" color=\"warning\"></ion-icon></p>\n            </ion-item>\n        </ion-list>\n</ion-content>\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/giftpopular/giftpopular.component.html":
  /*!**********************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/giftpopular/giftpopular.component.html ***!
    \**********************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppGiftpopularGiftpopularComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n<ion-segment padding color=\"primary\" mode=\"ios\" style=\"padding-bottom: 0;\">\n        <ion-segment-button layout=\"icon-start\" mode=\"ios\" value=\"0\" checked>\n            <ion-label>Popular</ion-label>\n        </ion-segment-button>\n    </ion-segment>\n    <ion-content scrollable=\"true\" mode=\"ios\">\n            <ion-list  mode=\"ios\">\n                    <!-- (click)=\"openPopular(x)\" -->\n                <ion-item  mode=\"ios\" *ngFor=\"let x of popularGiftList;let i=index\" >\n                     <ion-avatar  mode=\"ios\" slot=\"start\" style=\"border-radius: 0;width:20% !important;  \n                     height : 35% !important;  \n                     max-width: 20px !important;  \n                     max-height: 35px !important;\">\n                            <img [src]=\"x.image\" style=\"border-radius: 0;\">  \n                     </ion-avatar>\n                      <ion-label  mode=\"ios\"  no-padding>\n                        <h4  mode=\"ios\"  style=\"font-size: 11px;\">{{x.name}}</h4>\n                      </ion-label>\n                      <p  mode=\"ios\" slot=\"end\" style=\"font-size: 13px;\">{{x.price}}\n                         <ion-icon name=\"logo-bitcoin\" color=\"warning\"></ion-icon></p>\n                </ion-item>\n            </ion-list>\n    </ion-content>\n    ";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/giftsilver/giftsilver.component.html":
  /*!********************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/giftsilver/giftsilver.component.html ***!
    \********************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppGiftsilverGiftsilverComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n<ion-segment padding color=\"primary\" mode=\"ios\" style=\"padding-bottom: 0;\">\n    <ion-segment-button layout=\"icon-start\" mode=\"ios\" value=\"0\" checked>\n        <ion-label>Silver</ion-label>\n    </ion-segment-button>\n</ion-segment>\n<ion-content scrollable=\"true\" mode=\"ios\">\n        <ion-list  mode=\"ios\">\n            <ion-item  mode=\"ios\" *ngFor=\"let x of silverGiftList;let i=index\" (click)=\"openSilver(x)\" >\n                 <ion-avatar  mode=\"ios\" slot=\"start\" style=\"border-radius: 0;width:20% !important;  \n                 height : 35% !important;  \n                 max-width: 20px !important;  \n                 max-height: 35px !important;\">\n                        <img [src]=\"x.image\" style=\"border-radius: 0;\">  \n                 </ion-avatar>\n                  <ion-label  mode=\"ios\" no-padding>\n                    <h4  mode=\"ios\"  style=\"font-size: 11px;\">{{x.name}}</h4>\n                  </ion-label>\n                  <p  mode=\"ios\" slot=\"end\" style=\"font-size: 13px;\">{{x.price}}\n                     <ion-icon name=\"logo-bitcoin\" color=\"warning\"></ion-icon></p>\n            </ion-item>\n        </ion-list>\n</ion-content>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/historygiftpreview/historygiftpreview.page.html":
  /*!*******************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/historygiftpreview/historygiftpreview.page.html ***!
    \*******************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppHistorygiftpreviewHistorygiftpreviewPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header mode=\"ios\">\n    <ion-toolbar mode=\"ios\" no-padding>\n        <ion-item slot=\"start\" style=\"font-size: 11px;\" lines=\"none\">\n            <ion-label mode=\"ios\"><b>GEMS:</b>&nbsp;&nbsp;</ion-label>\n            <ion-icon name=\"star\" color=\"warning\" mode=\"ios\"></ion-icon>\n            <ion-text mode=\"ios\">10</ion-text>\n        </ion-item>\n        <ion-item slot=\"end\" style=\"font-size: 11px;\" lines=\"none\">\n            <ion-label mode=\"ios\"><b>COINS:</b>&nbsp;&nbsp;</ion-label>\n            <ion-icon name=\"logo-bitcoin\" color=\"warning\" mode=\"ios\"></ion-icon>\n            <ion-text mode=\"ios\">100</ion-text>\n        </ion-item>\n    </ion-toolbar>\n  </ion-header>\n<ion-content mode=\"ios\" text-center style=\"padding-bottom: 0;\">\n    <p style=\"font-size: 13px;\" mode=\"ios\" no-padding><b>{{gift.dateandtime}}</b></p>\n   <ion-img [src]=\"gift.image\" style=\"width: 60%;margin: 0 auto\" mode=\"ios\"></ion-img>\n   <p mode=\"ios\" style=\"line-height: 0;\">{{gift.text}}</p> \n   \n  </ion-content>\n  <ion-footer>\n      <ion-row style=\"padding-bottom: 8px;\" padding>\n          <ion-col text-left>\n              <ion-row>\n                  <ion-col text-left no-padding><ion-label style=\"font-size:11px\"><b>Remaining Cashout</b></ion-label></ion-col>\n              </ion-row>\n              <ion-row>\n                  <ion-col text-left no-padding>\n                          <ion-icon name=\"logo-bitcoin\" color=\"warning\" mode=\"ios\"></ion-icon>\n                          <ion-note style=\"font-size:16px\" color=\"dark\">10,000</ion-note>\n                  </ion-col>\n              </ion-row>\n          </ion-col>\n          <ion-col text-right>\n              <ion-row>\n                  <ion-col text-right no-padding><ion-label style=\"font-size:11px\"><b>Total Cashout</b></ion-label></ion-col>\n              </ion-row>\n              <ion-row>\n                  <ion-col text-right no-padding>\n                          <ion-icon name=\"logo-bitcoin\" color=\"warning\" mode=\"ios\"></ion-icon>\n                          <ion-note style=\"font-size:16px\" color=\"dark\">50,000</ion-note>\n                  </ion-col>\n              </ion-row>\n          </ion-col>\n      </ion-row>\n  </ion-footer>\n  ";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/historypreview/historypreview.page.html":
  /*!***********************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/historypreview/historypreview.page.html ***!
    \***********************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppHistorypreviewHistorypreviewPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header mode=\"ios\">\n    <ion-toolbar mode=\"ios\" no-padding>\n        <ion-item slot=\"start\" style=\"font-size: 11px;\" lines=\"none\">\n            <ion-label mode=\"ios\"><b>GEMS:</b>&nbsp;&nbsp;</ion-label>\n            <ion-icon name=\"star\" color=\"warning\" mode=\"ios\"></ion-icon>\n            <ion-text mode=\"ios\">10</ion-text>\n        </ion-item>\n        <ion-item slot=\"end\" style=\"font-size: 11px;\" lines=\"none\">\n            <ion-label mode=\"ios\"><b>COINS:</b>&nbsp;&nbsp;</ion-label>\n            <ion-icon name=\"logo-bitcoin\" color=\"warning\" mode=\"ios\"></ion-icon>\n            <ion-text mode=\"ios\">100</ion-text>\n        </ion-item>\n    </ion-toolbar>\n  </ion-header>\n<ion-content mode=\"ios\" text-center style=\"padding-bottom: 0;\">\n    <p style=\"font-size: 13px;\" mode=\"ios\" no-padding><b>{{transaction.dateandtime}}</b></p>\n   <ion-img [src]=\"transaction.image\" style=\"width: 60%;margin: 0 auto\" mode=\"ios\"></ion-img>\n   <p mode=\"ios\" style=\"line-height: 0;\">{{transaction.text}}</p> \n   \n  </ion-content>\n  <ion-footer>\n      <ion-row style=\"padding-bottom: 8px;\" padding>\n          <ion-col text-left>\n              <ion-row>\n                  <ion-col text-left no-padding><ion-label style=\"font-size:11px\"><b>Remaining Cashout</b></ion-label></ion-col>\n              </ion-row>\n              <ion-row>\n                  <ion-col text-left no-padding>\n                          <ion-icon name=\"logo-bitcoin\" color=\"warning\" mode=\"ios\"></ion-icon>\n                          <ion-note style=\"font-size:16px\" color=\"dark\">10,000</ion-note>\n                  </ion-col>\n              </ion-row>\n          </ion-col>\n          <ion-col text-right>\n              <ion-row>\n                  <ion-col text-right no-padding><ion-label style=\"font-size:11px\"><b>Total Cashout</b></ion-label></ion-col>\n              </ion-row>\n              <ion-row>\n                  <ion-col text-right no-padding>\n                          <ion-icon name=\"logo-bitcoin\" color=\"warning\" mode=\"ios\"></ion-icon>\n                          <ion-note style=\"font-size:16px\" color=\"dark\">50,000</ion-note>\n                  </ion-col>\n              </ion-row>\n          </ion-col>\n      </ion-row>\n  </ion-footer>\n  ";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/liveprofile/liveprofile.page.html":
  /*!*****************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/liveprofile/liveprofile.page.html ***!
    \*****************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppLiveprofileLiveprofilePageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header mode=\"ios\" no-border>\n  <ion-toolbar mode=\"ios\">\n      <ion-buttons slot=\"secondary\" (click)=\"reportUser()\">\n        <ion-button  mode=\"ios\">\n          <img src=\"assets/icon/report.png\" style=\"width: 32px;\" mode=\"ios\">&nbsp;Report\n        </ion-button>\n      </ion-buttons>\n      <!-- <ion-buttons slot=\"secondary\" mode=\"ios\">\n        <ion-button  mode=\"ios\">\n            <ion-icon name=\"heart-dislike\" color=\"warning\"  mode=\"ios\"></ion-icon>\n        </ion-button>\n      </ion-buttons> -->\n\n      <ion-buttons slot=\"primary\" (click)=\"goBack()\" mode=\"ios\">\n        <ion-button mode=\"ios\">\n          <ion-icon color=\"primary\" name=\"close-circle\" size=\"large\" mode=\"ios\"></ion-icon>\n        </ion-button>\n      </ion-buttons>\n\n    </ion-toolbar>\n</ion-header>\n<ion-content mode=\"ios\">\n  <ion-card mode=\"ios\" style=\"margin: 0;\"> \n    \n      <div *ngIf=\"!user_photo\">\n        <img src=\"assets/icon/brixylogo.png\" style=\"width:90%;margin: auto;height: 405px;\">\n      </div>\n      <div *ngIf=\"user_photo\">\n      <img [src] = \"user_photo\" style=\"width:90%;margin: auto;height: 405px;\">\n      </div> \n    \n    <ion-card-header text-left style=\"padding-top:10%;padding-bottom: 1%;\" mode=\"ios\">\n      <ion-card-subtitle>\n        <ion-row>\n          <ion-col no-padding> <b>User ID: </b>{{user_locator_id}}</ion-col>\n          <ion-col no-padding style=\"margin-top: -3%;\" text-right> \n            <button id=\"btn_custom\" (click)=\"follow()\" [ngClass]=\"status ? 'myHeartFollow' : 'myHeartUnfollow'\">\n              <span></span>\n            </button>\n            \n              \n              <!-- <ion-icon name=\"heart\" (click)=\"follow()\" [ngClass]=\"status ? 'myHeartFollow' : 'myHeartUnfollow'\" mode=\"ios\"></ion-icon> -->\n          </ion-col>\n        </ion-row>\n      </ion-card-subtitle>\n      <ion-card-title >\n       <ion-row>\n        <ion-col no-padding> <b style=\"font-size: 22px;\">{{fname+\" \"+lname}}</b></ion-col>\n        <ion-col no-padding style=\"margin-top: 2%;\" text-right> \n          <button id=\"btn_custom\" (click)=\"message()\" style=\"background:  #1dc1e6;color: white\">\n            <span>Message</span>\n          </button>\n            <!-- <ion-icon name=\"heart\" (click)=\"follow()\" [ngClass]=\"status ? 'myHeartFollow' : 'myHeartUnfollow'\" mode=\"ios\"></ion-icon> -->\n        </ion-col>\n      </ion-row>\n      </ion-card-title>\n      <ion-card-subtitle style=\"margin-top: -2%;\" >From {{city+\", \"+country}}</ion-card-subtitle>\n    </ion-card-header>\n    <ion-card-content mode=\"ios\" no-padding>\n      <ion-grid>\n        <ion-row style=\"padding-bottom: 1%;\">\n          <ion-col>\n            <ion-button mode=\"ios\" expand=\"block\"  shape=\"round\" style=\"font-size: 12px;--background: #656d78\">\n              Followers:&nbsp;&nbsp;<div style=\"margin-top: 0;\" class=\"lvlnum\">{{followers}}</div></ion-button>\n          </ion-col>\n          <ion-col>\n            <ion-button mode=\"ios\" expand=\"block\" shape=\"round\" style=\"font-size: 12px;--background: #656d78\">\n              Following:&nbsp;&nbsp;<div style=\"margin-top: 0;\" class=\"lvlnum\">{{following}}</div></ion-button>\n          </ion-col>\n        </ion-row> \n        <ion-row>\n          <ion-col>\n            <ion-segment-button>\n              <ion-text class=\"lvltext\" mode=\"ios\">XP</ion-text>\n              <img class=\"lvlicon\" src=\"assets/icon/star.png\">\n              <ion-label class=\"lvlnum\" color=\"dark\">{{user_experience | number:'1.0':'en-US'}}</ion-label>\n            </ion-segment-button>\n          </ion-col>\n          <ion-col>\n            <ion-segment-button>\n              <ion-text class=\"lvltext\" mode=\"ios\">Gold Bars</ion-text>\n              <img class=\"lvlicon\" src=\"assets/icon/gold.png\">\n              <ion-label class=\"lvlnum\" color=\"dark\">{{gold_bars | number:'1.0':'en-US'}}</ion-label>\n            </ion-segment-button>\n          </ion-col>\n          <ion-col>\n            <ion-segment-button>\n              <ion-text class=\"lvltext\" mode=\"ios\">Coins</ion-text>\n              <img class=\"lvlicon\" src=\"assets/icon/imgcoin.png\">\n              <ion-label class=\"lvlnum\" color=\"dark\">{{user_coins | number:'1.0':'en-US'}}</ion-label>\n            </ion-segment-button>\n          </ion-col>\n        </ion-row>\n          <ion-row>\n            <ion-col style=\"background: #f1f1f4;\" size=\"5.5\"><br>\n              <ion-segment-button>\n                <ion-text class=\"lvltext\" mode=\"ios\">Level</ion-text>\n                <img class=\"lvlicon2\" src=\"assets/icon/medal.png\">\n                <ion-label class=\"lvlnum\" style=\"margin-top: 2%;\" color=\"dark\">{{user_level}}</ion-label>\n                <ion-text class=\"lvlname\" >{{badge_name}}</ion-text>\n              </ion-segment-button><br>\n            </ion-col>\n            <ion-col size=\"0.5\">\n            </ion-col>\n            <ion-col text-center class=\"lvlcol2\" size=\"6\">\n            </ion-col>\n          </ion-row>\n          <ion-row>\n            <ion-col>\n              <div style=\"padding: 10%;padding-top: 0;padding-bottom: 0;\">\n                <div class=\"progress-outer\">\n                  <div class=\"progress-inner\" [style.width]=\" ((user_experience/bracket_to) * 100) + '%'\">\n                    {{user_experience}} / {{bracket_to}}\n                  </div>\n                </div>\n              </div>\n            </ion-col>\n          </ion-row>\n      </ion-grid>\n      \n    </ion-card-content>\n  </ion-card>\n  <!-- <ion-card mode=\"ios\" style=\"color: white;\" text-center>\n    <br><br>\n    <ion-card-header mode=\"ios\" no-padding>\n      <div *ngIf=\"!user_photo\">\n        <img src=\"assets/icon/brixylogo.png\" style=\"width:100px;height:100px;display: block;margin: auto\">\n      </div>\n      <div *ngIf=\"user_photo\">\n       <img [src] = \"user_photo\" style=\"width:100px;height:100px;display: block;margin: auto\">\n      </div> \n      <ion-card-title no-padding style=\"color: white;\">{{fname+\" \"+lname}}<br>\n        <ion-label style=\"font-size: 13px;\">User ID: {{user_locator_id}}</ion-label> <br>\n        <ion-label style=\"font-size: 13px;\">{{city+\", \"+country}}</ion-label> \n          <ion-grid no-padding>\n            <ion-row>\n                <ion-col>\n                    <ion-label style=\"font-size: 15px;\">Level</ion-label>\n                </ion-col>\n                <ion-col>\n                  <ion-label style=\"font-size: 15px;\">XP</ion-label>\n              </ion-col>\n                <ion-col>\n                    <ion-label style=\"font-size: 15px;\">Gold bars</ion-label>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                  <ion-col>\n                      <img src=\"assets/icon/medal.png\" style=\"width: 30px !important; height: 30px; margin: auto;\">\n                      <ion-label style=\"font-size: 15px;\">{{user_level}}</ion-label><br/>\n                      <ion-label style=\"font-size: 15px;\">{{badge_name}}</ion-label>\n                  </ion-col>\n                  <ion-col>\n                    <img src=\"assets/icon/gold.png\" style=\"width: 30px !important; height: 30px; margin: auto;\">\n                    <ion-label style=\"font-size: 15px;\">{{user_experience | number:'1.0':'en-US'}}</ion-label>\n                </ion-col>\n                  <ion-col>\n                      <img src=\"assets/icon/gold.png\" style=\"width: 30px !important; height: 30px; margin: auto;\">\n                      <ion-label style=\"font-size: 15px;\">{{gold_bars | number:'1.0':'en-US'}}</ion-label>\n                  </ion-col>\n                </ion-row>\n              <ion-row style=\"text-align: center;padding: 0%;\">\n                <ion-col>\n                  <div>\n                      <ion-button mode=\"ios\" shape=\"round\" color=\"light\" style=\"font-size: 12px;\">Followers: {{followers}}</ion-button>\n                  </div>\n                </ion-col>\n                <ion-col>\n                    <div>\n                        <ion-button mode=\"ios\" shape=\"round\" color=\"light\" style=\"font-size: 12px;\">Following: {{following}}</ion-button>\n                    </div>\n                  </ion-col>\n              </ion-row>\n              <ion-row>\n\n                <div style=\"width:100px;height:100px;display: block;margin: auto\">\n\n                  <ion-icon name=\"heart\" (click)=\"follow()\" [ngClass]=\"status ? 'myHeartFollow' : 'myHeartUnfollow'\" mode=\"ios\"></ion-icon>\n              \n                </div>\n              </ion-row>\n            </ion-grid>\n      </ion-card-title>\n    </ion-card-header>\n  </ion-card> -->\n  \n</ion-content>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/payreqconfirmation/payreqconfirmation.page.html":
  /*!*******************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/payreqconfirmation/payreqconfirmation.page.html ***!
    \*******************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPayreqconfirmationPayreqconfirmationPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header mode=\"ios\">\n  <ion-toolbar mode=\"ios\" no-padding>\n  </ion-toolbar>\n</ion-header>\n<ion-content mode=\"ios\" text-center style=\"padding-bottom: 0;\">\n   <p style=\"font-size: 20px;\" mode=\"ios\" no-padding><b>Gold Bars {{paychart.gold_bars}}</b></p> \n <p mode=\"ios\" style=\"line-height: 0;\"> <ion-icon name=\"logo-bitcoin\" color=\"warning\" mode=\"ios\"></ion-icon>USD {{paychart.usd_amount}}</p> \n <p>Request Cash out ? </p>\n</ion-content>\n<ion-footer mode=\"ios\">\n    <ion-toolbar mode=\"ios\" style=\"padding-left: 10px;padding-right: 10px\">\n        <ion-button slot=\"start\" mode=\"ios\" color=\"danger\" size=\"small\" (click)=\"closeModal()\">\n          Cancel\n        </ion-button>\n         <ion-button slot=\"end\" (click)=\"confirmPaymentRequest()\" mode=\"ios\" color=\"secondary\" size=\"small\">\n          Proceed\n        </ion-button>\n      </ion-toolbar>\n</ion-footer>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/privacypolicy/privacypolicy.page.html":
  /*!*********************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/privacypolicy/privacypolicy.page.html ***!
    \*********************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPrivacypolicyPrivacypolicyPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header mode=\"ios\" no-border>\n  <ion-toolbar mode=\"ios\">\n      <ion-buttons slot=\"secondary\" (click)=\"goBack()\">\n        <ion-button mode=\"ios\" style=\"font-size: 15px;\">\n            Back\n        </ion-button>\n      </ion-buttons>\n\n      <ion-title color=\"secondary\">Privacy Policy</ion-title>\n\n      <!-- <ion-buttons slot=\"primary\" (click)=\"goHelp()\">\n        <ion-button mode=\"ios\" style=\"font-size: 15px;\">\n            Help\n        </ion-button>\n      </ion-buttons> -->\n    </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <iframe src=\"https://www.greenthumbtrade.com/privacy-policy\" style=\"height: 80vh; width: 100%;\"></iframe>\n</ion-content>\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/purchasecoins/purchasecoins.page.html":
  /*!*********************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/purchasecoins/purchasecoins.page.html ***!
    \*********************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPurchasecoinsPurchasecoinsPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n<ion-content mode=\"ios\" text-center style=\"padding-bottom: 0;\">\n  <br>\n  <ion-row style=\"padding-bottom: 5px;\">\n    <ion-col style=\"color: black;margin-top: 1.2%;\" mode=\"ios\" text-center>\n        <img class=\"lvlicon2\" src=\"assets/icon/imgcoin.png\">\n        <ion-text class=\"lvlnum\">{{coin.coins}}</ion-text>\n    </ion-col>\n  </ion-row>\n  <h5 style=\"font-weight: bold;padding-bottom: 3%;\">Purchase coins? </h5>\n <p mode=\"ios\" style=\"line-height: 0;\">TOTAL</p> \n <ion-row style=\"padding-bottom: 5px;margin-top: -2%;\">\n  <ion-col style=\"color: black;\" mode=\"ios\" text-center>\n      <img class=\"lvlicon3\" src=\"assets/icon/dollar.png\">\n      <ion-text class=\"lvlnum2\">{{coin.amount}}</ion-text>\n  </ion-col>\n</ion-row>\n\n<ion-row padding style=\"padding-bottom: 0;\">\n  <!-- <ion-col>\n    <ion-button mode=\"ios\" color=\"light\" expand=\"block\" (click)=\"closeModal()\">\n      Cancel\n    </ion-button>\n  </ion-col> -->\n  <ion-col>\n    <ion-button  (click)=\"purchaseCoins()\" mode=\"ios\" expand=\"block\">\n      Purchase\n    </ion-button>\n  </ion-col>\n</ion-row>\n</ion-content>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/purchaseproduct/purchaseproduct.page.html":
  /*!*************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/purchaseproduct/purchaseproduct.page.html ***!
    \*************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPurchaseproductPurchaseproductPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header mode=\"ios\" no-border>\n  <ion-toolbar mode=\"ios\">\n      <ion-buttons slot=\"secondary\" (click)=\"closeModal()\">\n        <ion-button mode=\"ios\" style=\"font-size: 15px;\" (click)=\"closeModal()\">\n            Back\n        </ion-button>\n      </ion-buttons>\n\n      <ion-title color=\"secondary\">Enter Quantity</ion-title>\n    </ion-toolbar>\n</ion-header>\n<ion-content padding class=\"for_post\">\n\n  <ion-row justify-content-center align-items-center style='height: 90%'>\n    <ion-col>\n      <ion-list class=\"line-input\">\n        <ion-item>\n          <ion-input mode=\"ios\" [(ngModel)]=\"purchase_quantity\" type=\"number\" class=\"item_input\" placeholder=\"0\"></ion-input>\n         </ion-item>\n      </ion-list>\n    </ion-col>\n  </ion-row>\n</ion-content>\n<div id=\"overlay\" padding>\n  <ion-button expand=\"block\" mode=\"ios\" (click)=\"purchaseProduct()\">\n    PURCHASE\n  </ion-button>\n</div>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/rate/rate.page.html":
  /*!***************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/rate/rate.page.html ***!
    \***************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppRateRatePageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n<ion-content text-center>\n  <h4 style=\"font-weight: bold;\">Tell us what you think</h4>\n  <p class=\"for_txt\">Lorem ipsum dolor sit amet,f consectetur adipiscing elit</p>\n  <ionic4-star-rating #rating\n      activeIcon = \"ios-star\"\n      defaultIcon = \"ios-star-outline\"\n      activeColor = \"#ffce00\" \n      defaultColor = \"#ffce00\"\n      readonly=\"false\"\n      rating=\"0\"\n      fontSize = \"40px\"\n      (ratingChanged)=\"logRatingChange($event)\">\n  </ionic4-star-rating>\n  <ion-button class=\"for_button\" mode=\"ios\" (click)=\"submitRate()\" expand=\"block\">SUBMIT</ion-button>\n</ion-content>\n\n<ion-footer mode=\"ios\">\n  <ion-toolbar mode=\"ios\" (click)=\"close()\">\n    <ion-title color=\"secondary\"> Not Now</ion-title>\n</ion-toolbar>\n</ion-footer>\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/register/register.page.html":
  /*!***********************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/register/register.page.html ***!
    \***********************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppRegisterRegisterPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header mode=\"ios\" no-border>\n  <ion-toolbar mode=\"ios\">\n    <ion-buttons slot=\"secondary\" (click)=\"cancel()\">\n      <ion-button color=\"secondary\" mode=\"ios\" style=\"font-size: 15px;\">\n          Not Now\n      </ion-button>\n    </ion-buttons>\n\n    <ion-buttons slot=\"primary\" >\n        <ion-button>\n            <img src=\"assets/greenthumb-images/greenthumblogo.png\" style=\"width: 27px;border-radius: 50%;\" mode=\"ios\">\n        </ion-button>\n      </ion-buttons>\n</ion-toolbar>\n</ion-header>\n<ion-content mode=\"ios\" padding style=\"--padding-top: 0;\">\n  <p text-center style=\"padding: 0;margin-bottom: 30px;\">Please fill-up the essential details below to acknowledge our\n  <ion-note color=\"secondary\" style=\"font-weight: bold;\" (click)=\"goToUserPolicy();\">User Policy.</ion-note></p>\n\n  <ion-item style=\"margin-top: 5px;\" mode=\"ios\" id=\"for_eachitem\">\n    <ion-label color=\"secondary\" position=\"floating\" mode=\"ios\">First Name</ion-label>\n    <ion-input placeholder=\"Enter here\" type=\"text\" mode=\"ios\" [(ngModel)]=\"inpt_fname\" maxlength=\"50\" minlength=\"2\" \n      value=\"{{ fname }}\"></ion-input>\n    <ion-icon style=\"align-self: flex-end;\" name=\"person\" slot=\"end\" mode=\"ios\" color=\"medium\"></ion-icon>\n  </ion-item>\n  <ion-item style=\"margin-top: 5px;\" mode=\"ios\" id=\"for_eachitem\">\n    <ion-label color=\"secondary\" position=\"floating\" mode=\"ios\">Last Name</ion-label>\n    <ion-input placeholder=\"Enter here\" type=\"text\" mode=\"ios\" [(ngModel)]=\"inpt_lname\" maxlength=\"50\" minlength=\"2\"\n      value=\"{{ lname }}\"></ion-input>\n    <ion-icon style=\"align-self: flex-end;\" name=\"person\" slot=\"end\" mode=\"ios\" color=\"medium\"></ion-icon>\n  </ion-item>\n  <ion-item style=\"margin-top: 5px;\" mode=\"ios\" id=\"for_eachitem\">\n    <ion-label color=\"secondary\" position=\"floating\" mode=\"ios\">Birthdate</ion-label>\n    <ion-datetime placeholder=\"Enter here\" mode=\"ios\" value=\"{{ bdate }}\" displayFormat=\"MM/DD/YYYY\" [(ngModel)]=\"inpt_birthdate\" min=\"1945-01-01\" max=\"2012-12-31\"></ion-datetime>\n    <ion-icon style=\"align-self: flex-end;\" name=\"calendar\" slot=\"end\" mode=\"ios\" color=\"medium\"></ion-icon>\n  </ion-item>\n  <ion-item style=\"margin-top: 5px;\" mode=\"ios\" id=\"for_eachitem\">\n    <ion-label color=\"secondary\" position=\"floating\" mode=\"ios\">Town/City</ion-label>\n    <ion-input placeholder=\"Enter here\" type=\"text\" mode=\"ios\" value=\"{{ city }}\" [(ngModel)]=\"inpt_city\" maxlength=\"50\" minlength=\"2\"></ion-input>\n    <ion-icon style=\"align-self: flex-end;\" name=\"pin\" slot=\"end\" mode=\"ios\" color=\"medium\"></ion-icon>\n  </ion-item>\n  <ion-item style=\"margin-top: 5px;\" mode=\"ios\" id=\"for_eachitem\">\n    <ion-label color=\"secondary\" position=\"floating\" mode=\"ios\">Country</ion-label>\n    <ion-input placeholder=\"Enter here\" type=\"text\" mode=\"ios\" value=\"{{ country }}\" [(ngModel)]=\"inpt_country\" maxlength=\"50\" minlength=\"2\"></ion-input>\n    <ion-icon style=\"align-self: flex-end;\" name=\"home\" slot=\"end\" mode=\"ios\" color=\"medium\"></ion-icon>\n  </ion-item>\n  <ion-item style=\"margin-top: 5px;\" mode=\"ios\" id=\"for_eachitem\">\n    <ion-label color=\"secondary\" position=\"floating\" mode=\"ios\">Email</ion-label>\n    <ion-input placeholder=\"Enter here\" type=\"email\" [(ngModel)]=\"inpt_email\" value=\"{{ email }}\" maxlength=\"50\" minlength=\"7\"></ion-input>\n    <ion-icon style=\"align-self: flex-end;\" name=\"mail\" slot=\"end\" mode=\"ios\" color=\"medium\"></ion-icon>\n  </ion-item>\n  <ion-item style=\"margin-top: 5px;\" mode=\"ios\" id=\"for_eachitem\">\n    <ion-label color=\"secondary\" position=\"floating\" mode=\"ios\">Contact Number</ion-label>\n    <ion-input placeholder=\"Enter here\" type=\"text\" [(ngModel)]=\"inpt_mobile_num\" value=\"{{ mobile_num }}\" maxlength=\"12\" minlength=\"11\"></ion-input>\n    <ion-icon style=\"align-self: flex-end;\" name=\"call\" slot=\"end\" mode=\"ios\" color=\"medium\"></ion-icon>\n  </ion-item>\n  <br><br>\n  <ion-button expand=\"block\" mode=\"ios\" (click)=\"register()\">UPDATE</ion-button>\n\n</ion-content>\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/report/report.page.html":
  /*!*******************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/report/report.page.html ***!
    \*******************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppReportReportPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header mode=\"ios\" no-border>\n  <ion-toolbar mode=\"ios\">\n      <ion-buttons slot=\"secondary\" (click)=\"goBack()\">\n        <ion-button mode=\"ios\" style=\"font-size: 15px;\">\n          Back\n      </ion-button>\n      </ion-buttons>\n\n      <ion-title color=\"secondary\">Report Item</ion-title>\n\n      <ion-buttons slot=\"primary\" >\n        <ion-button>\n            <img src=\"assets/greenthumb-images/greenthumblogo.png\" style=\"width: 32px;border-radius: 50%;\" mode=\"ios\">\n        </ion-button>\n      </ion-buttons>\n    </ion-toolbar>\n</ion-header>\n\n<ion-content mode=\"ios\">\n  <p text-center style=\"padding-bottom: 15px;\">\n    You have reported <b style=\"color: #679733;font-weight: bold;\">{{reportedName}}</b>, \n    please choose the reason below why you have chosen this option.\n  </p>\n\n  <ion-radio-group allow-empty-selection [(ngModel)]=\"theReason\" name=\"myReason\" mode=\"ios\">\n  <ion-item *ngFor=\"let x of reasonList;\" mode=\"ios\" lines=\"none\" class=\"for_itemspace\">\n    \n        <ion-label style=\"font-size: 13px\" mode=\"ios\">\n          {{x.reason}}</ion-label>\n        <ion-radio slot=\"start\" value=\"{{x.reason}}\"></ion-radio>\n  </ion-item>\n</ion-radio-group>\n\n\n<ion-radio-group allow-empty-selection name=\"others\" mode=\"ios\">\n    <ion-item (click)=\"othersClick()\" mode=\"ios\" lines=\"none\" class=\"for_itemspace\">\n      <ion-label style=\"font-size: 13px\" mode=\"ios\">\n          Others /  Additional</ion-label>\n        <ion-radio slot=\"start\" ></ion-radio>\n    </ion-item>\n  </ion-radio-group>\n\n  <ion-list class=\"line-input\" padding>\n    <ion-row>\n      <ion-col>\n        <ion-label class=\"item_label\">Reason</ion-label>\n        <ion-item>\n          <ion-textarea [(ngModel)]=\"otherReason\" mode=\"ios\" class=\"item_input\" size=\"small\"></ion-textarea>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n  </ion-list>\n\n<ion-row padding>\n  <ion-col>\n    <ion-button expand=\"block\" (click)=\"goBack()\" mode=\"ios\" color=\"light\">Cancel</ion-button>\n  </ion-col>\n  <ion-col>\n    <ion-button expand=\"block\" (click)=\"reportThisUser()\" mode=\"ios\">Report</ion-button>\n  </ion-col>\n</ion-row>\n\n</ion-content>\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/reportuser/reportuser.page.html":
  /*!***************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/reportuser/reportuser.page.html ***!
    \***************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppReportuserReportuserPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header mode=\"ios\" no-border>\n  <ion-toolbar mode=\"ios\">\n      <ion-buttons slot=\"secondary\" (click)=\"goBack()\">\n        <ion-button mode=\"ios\" style=\"font-size: 15px;\">\n          Back\n      </ion-button>\n      </ion-buttons>\n\n      <ion-title color=\"secondary\">Report User</ion-title>\n\n      <ion-buttons slot=\"primary\" >\n        <ion-button>\n            <img src=\"assets/greenthumb-images/greenthumblogo.png\" style=\"width: 32px;border-radius: 50%;\" mode=\"ios\">\n        </ion-button>\n      </ion-buttons>\n    </ion-toolbar>\n</ion-header>\n\n<ion-content mode=\"ios\">\n  <p text-center style=\"padding-bottom: 15px;\">\n    You have reported <b style=\"color: #679733;font-weight: bold;\">{{reportedName}}</b>, \n    please choose the reason below why you have chosen this option.\n  </p>\n  <ion-radio-group allow-empty-selection [(ngModel)]=\"theReason\" name=\"myReason\" mode=\"ios\">\n  <ion-item *ngFor=\"let x of reasonList;\" mode=\"ios\" lines=\"none\" class=\"for_itemspace\">\n    \n        <ion-label style=\"font-size: 13px\" mode=\"ios\">\n          {{x.reason}}</ion-label>\n        <ion-radio slot=\"start\" value=\"{{x.reason}}\"></ion-radio>\n  </ion-item>\n</ion-radio-group>\n\n\n<ion-radio-group allow-empty-selection name=\"others\" mode=\"ios\">\n    <ion-item (click)=\"othersClick()\" mode=\"ios\" lines=\"none\" class=\"for_itemspace\">\n      <ion-label style=\"font-size: 13px\" mode=\"ios\">\n          Others /  Additional</ion-label>\n        <ion-radio slot=\"start\" ></ion-radio>\n    </ion-item>\n  </ion-radio-group>\n\n  <ion-list class=\"line-input\" padding>\n    <ion-row>\n      <ion-col>\n        <ion-label class=\"item_label\">Reason</ion-label>\n        <ion-item>\n          <ion-textarea [(ngModel)]=\"otherReason\" mode=\"ios\" class=\"item_input\" size=\"small\"></ion-textarea>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n  </ion-list>\n\n<ion-row padding>\n  <ion-col>\n    <ion-button expand=\"block\" (click)=\"goBack()\" mode=\"ios\" color=\"light\">Cancel</ion-button>\n  </ion-col>\n  <ion-col>\n    <ion-button expand=\"block\" (click)=\"reportThisUser()\" mode=\"ios\">Report</ion-button>\n  </ion-col>\n</ion-row>\n\n</ion-content>\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/selectcategories/selectcategories.page.html":
  /*!***************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/selectcategories/selectcategories.page.html ***!
    \***************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppSelectcategoriesSelectcategoriesPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header mode=\"ios\" no-border>\n  <ion-toolbar mode=\"ios\">\n      <ion-buttons slot=\"secondary\" (click)=\"goBack()\">\n        <ion-button mode=\"ios\" style=\"font-size: 15px;\">\n            Back\n        </ion-button>\n      </ion-buttons>\n\n      <ion-title color=\"secondary\">Categories</ion-title>\n    </ion-toolbar>\n</ion-header>\n<ion-content>\n\n\n  <ion-grid class=\"grid-categories\">\n    <ion-row *ngFor=\"let category of categoryList; let i = index\" style=\"padding: 5px;\" >\n      <ion-col (click)=\"selected(category.id, category.category)\" *ngIf=\"i % 2 == 0\">\n        <ion-card mode=\"ios\" class=\"for_card\" text-center>\n          <div *ngIf=\"!category.category_photo\">\n            <img src=\"assets/greenthumb-images/greenthumblogo.png\">\n          </div>\n          <div *ngIf=\"category.category_photo\">\n            <img [src] = \"category.category_photo\">\n          </div>\n          <ion-card-content class=\"for_cardcontent\">\n            {{category.category}}\n          </ion-card-content>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n  <ion-grid class=\"grid-categories\">\n    <ion-row *ngFor=\"let category of categoryList; let i = index\" style=\"padding: 5px;\">\n      <ion-col (click)=\"selected(category.id, category.category)\" *ngIf=\"i % 2 != 0\">\n        <ion-card mode=\"ios\" class=\"for_card\" text-center>\n          <div *ngIf=\"!category.category_photo\">\n            <img src=\"assets/greenthumb-images/greenthumblogo.png\">\n          </div>\n          <div *ngIf=\"category.category_photo\">\n            <img [src] = \"category.category_photo\">\n          </div>\n          <ion-card-content class=\"for_cardcontent\">\n            {{category.category}}\n          </ion-card-content>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n\n<!-- <ion-segment style=\"padding: 5px\" color=\"primary\" mode=\"ios\" (ionChange)=\"segmentChanged()\" [(ngModel)]=\"segment\">\n  <ion-segment-button style=\"padding: 3px\" mode=\"ios\" value=\"0\">\n      <ion-label>Vegetables</ion-label>\n  </ion-segment-button>  \n  <ion-segment-button style=\"padding: 3px\" mode=\"ios\" value=\"1\">\n      <ion-label>Fruits</ion-label>\n  </ion-segment-button>\n  <ion-segment-button style=\"padding: 3px\" mode=\"ios\" value=\"2\">\n    <ion-label>Produces</ion-label>\n</ion-segment-button>\n</ion-segment>\n\n  <ion-slides #slides (ionSlideDidChange)=\"slideChanged()\" mode=\"ios\">\n    \n        <ion-slide>\n          <ion-grid>\n            <ion-row style=\"padding: 5px;\">\n              <ion-col size=\"6\" (click)=\"selected(2,'Apple')\">\n                <ion-card mode=\"ios\" class=\"for_card\">\n                  <div class=\"div_card\">\n                    <ion-icon color=\"dark\" class=\"for_divicon\" name=\"search\" mode=\"ios\"></ion-icon>\n                  </div>\n                  <ion-card-content class=\"for_cardcontent\">\n                    Apple\n                  </ion-card-content>\n                </ion-card>\n              </ion-col>\n              <ion-col size=\"6\" (click)=\"selected(3,'Orange')\">\n                <ion-card mode=\"ios\" class=\"for_card\">\n                  <div class=\"div_card\">\n                    <ion-icon color=\"dark\" class=\"for_divicon\" name=\"pin\" mode=\"ios\"></ion-icon>\n                  </div>\n                  <ion-card-content class=\"for_cardcontent\">\n                    Orange\n                  </ion-card-content>\n                </ion-card>\n              </ion-col>\n            </ion-row>\n            <ion-row style=\"padding: 5px;\">\n              <ion-col size=\"6\" (click)=\"selected(4,'Grapes')\">\n                <ion-card mode=\"ios\" class=\"for_card\">\n                  <div class=\"div_card\">\n                    <ion-icon color=\"dark\" class=\"for_divicon\" name=\"people\" mode=\"ios\"></ion-icon>\n                  </div>\n                  <ion-card-content class=\"for_cardcontent\">\n                    Grapes\n                  </ion-card-content>\n                </ion-card>\n              </ion-col>\n              <ion-col size=\"6\" (click)=\"selected(5,'Bananas')\">\n                <ion-card mode=\"ios\" class=\"for_card\">\n                  <div class=\"div_card\">\n                    <ion-icon color=\"dark\" class=\"for_divicon\" name=\"search\" mode=\"ios\"></ion-icon>\n                  </div>\n                  <ion-card-content class=\"for_cardcontent\">\n                    Bananas\n                  </ion-card-content>\n                </ion-card>\n              </ion-col>\n            </ion-row>\n          </ion-grid>\n        </ion-slide>\n       \n        <ion-slide>\n          <ion-grid>\n            <ion-row style=\"padding: 5px;\">\n              <ion-col>\n                <p>empty</p>\n              </ion-col>\n            </ion-row>\n          </ion-grid>\n        </ion-slide>\n\n        <ion-slide>\n          <ion-grid>\n            <ion-row style=\"padding: 5px;\">\n              <ion-col>\n                <p>empty</p>\n              </ion-col>\n            </ion-row>\n          </ion-grid>\n        </ion-slide>\n\n    </ion-slides> -->\n</ion-content>\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/selectcategoriessub/selectcategoriessub.page.html":
  /*!*********************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/selectcategoriessub/selectcategoriessub.page.html ***!
    \*********************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppSelectcategoriessubSelectcategoriessubPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header mode=\"ios\" no-border>\n  <ion-toolbar mode=\"ios\">\n      <ion-buttons slot=\"secondary\" (click)=\"goBack()\">\n        <ion-button mode=\"ios\" style=\"font-size: 15px;\">\n            Back\n        </ion-button>\n      </ion-buttons>\n\n      <ion-title color=\"secondary\">Choose Category</ion-title>\n    </ion-toolbar>\n</ion-header>\n<ion-content>\n\n\n\n  <ion-grid class=\"grid-categories\">\n    <ion-row *ngFor=\"let category of categoryList; let i = index\" style=\"padding: 5px;\" >\n      <ion-col (click)=\"selected(category.id, category.category)\" *ngIf=\"i % 2 == 0\">\n        <ion-card mode=\"ios\" class=\"for_card\" text-center>\n          <div *ngIf=\"!category.category_photo\">\n            <img src=\"assets/greenthumb-images/greenthumblogo.png\">\n          </div>\n          <div *ngIf=\"category.category_photo\">\n            <img [src] = \"category.category_photo\">\n          </div>\n          <ion-card-content class=\"for_cardcontent\">\n            {{category.category}}\n          </ion-card-content>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n  <ion-grid class=\"grid-categories\">\n    <ion-row *ngFor=\"let category of categoryList; let i = index\" style=\"padding: 5px;\">\n      <ion-col (click)=\"selected(category.id, category.category)\" *ngIf=\"i % 2 != 0\">\n        <ion-card mode=\"ios\" class=\"for_card\" text-center>\n          <div *ngIf=\"!category.category_photo\">\n            <img src=\"assets/greenthumb-images/greenthumblogo.png\">\n          </div>\n          <div *ngIf=\"category.category_photo\">\n            <img [src] = \"category.category_photo\">\n          </div>\n          <ion-card-content class=\"for_cardcontent\">\n            {{category.category}}\n          </ion-card-content>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n</ion-content>\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/sendgiftbronzemodal/sendgiftbronzemodal.page.html":
  /*!*********************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/sendgiftbronzemodal/sendgiftbronzemodal.page.html ***!
    \*********************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppSendgiftbronzemodalSendgiftbronzemodalPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header mode=\"ios\">\n    <ion-toolbar mode=\"ios\" no-padding>\n        <ion-item slot=\"start\" style=\"font-size: 11px;\" lines=\"none\">\n            <ion-label mode=\"ios\"><b>GEMS:</b>&nbsp;&nbsp;</ion-label>\n            <ion-icon name=\"star\" color=\"warning\" mode=\"ios\"></ion-icon>\n            <ion-text mode=\"ios\">10</ion-text>\n        </ion-item>\n        <ion-item slot=\"end\" style=\"font-size: 11px;\" lines=\"none\">\n            <ion-label mode=\"ios\"><b>COINS:</b>&nbsp;&nbsp;</ion-label>\n            <ion-icon name=\"logo-bitcoin\" color=\"warning\" mode=\"ios\"></ion-icon>\n            <ion-text mode=\"ios\">100</ion-text>\n        </ion-item>\n    </ion-toolbar>\n  </ion-header>\n<ion-content mode=\"ios\" text-center style=\"padding-bottom: 0;\">\n    <p style=\"font-size: 13px;\" mode=\"ios\" no-padding><b>{{bronze.name}}</b></p>\n   <ion-img [src]=\"bronze.imagegif\" style=\"width: 80%;margin: 0 auto\" mode=\"ios\"></ion-img>\n   <p mode=\"ios\" style=\"line-height: 0;\"> <ion-icon name=\"logo-bitcoin\" color=\"warning\" mode=\"ios\"></ion-icon> {{bronze.price}}</p> \n  </ion-content>\n  <ion-footer mode=\"ios\">\n      <ion-toolbar mode=\"ios\" style=\"padding-left: 10px;padding-right: 10px\">\n          <ion-button slot=\"start\" mode=\"ios\" color=\"danger\" size=\"small\" (click)=\"closeModal()\">\n            Cancel\n          </ion-button>\n           <ion-button slot=\"end\" mode=\"ios\" (click)=\"sendGift()\" color=\"secondary\" size=\"small\">\n            Send gift\n          </ion-button>\n        </ion-toolbar>\n  </ion-footer>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/sendgiftgoldmodal/sendgiftgoldmodal.page.html":
  /*!*****************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/sendgiftgoldmodal/sendgiftgoldmodal.page.html ***!
    \*****************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppSendgiftgoldmodalSendgiftgoldmodalPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header mode=\"ios\">\n  <ion-toolbar mode=\"ios\" no-padding>\n      <ion-item slot=\"start\" style=\"font-size: 11px;\" lines=\"none\">\n          <ion-label mode=\"ios\"><b>GEMS:</b>&nbsp;&nbsp;</ion-label>\n          <ion-icon name=\"star\" color=\"warning\" mode=\"ios\"></ion-icon>\n          <ion-text mode=\"ios\">10</ion-text>\n      </ion-item>\n      <ion-item slot=\"end\" style=\"font-size: 11px;\" lines=\"none\">\n          <ion-label mode=\"ios\"><b>COINS:</b>&nbsp;&nbsp;</ion-label>\n          <ion-icon name=\"logo-bitcoin\" color=\"warning\" mode=\"ios\"></ion-icon>\n          <ion-text mode=\"ios\">100</ion-text>\n      </ion-item>\n  </ion-toolbar>\n</ion-header>\n<ion-content mode=\"ios\" text-center style=\"padding-bottom: 0;\">\n  <p style=\"font-size: 13px;\" mode=\"ios\" no-padding><b>{{gold.name}}</b></p>\n <ion-img [src]=\"gold.imagegif\" style=\"width: 40%;margin: 0 auto\" mode=\"ios\"></ion-img>\n <p mode=\"ios\" style=\"line-height: 0;\"> <ion-icon name=\"logo-bitcoin\" color=\"warning\" mode=\"ios\"></ion-icon> {{gold.price}}</p> \n</ion-content>\n<ion-footer mode=\"ios\">\n    <ion-toolbar mode=\"ios\" style=\"padding-left: 10px;padding-right: 10px\">\n        <ion-button slot=\"start\" mode=\"ios\" color=\"danger\" size=\"small\" (click)=\"closeModal()\">\n          Cancel\n        </ion-button>\n         <ion-button slot=\"end\" mode=\"ios\" color=\"secondary\" size=\"small\">\n          Send gift\n        </ion-button>\n      </ion-toolbar>\n</ion-footer>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/sendgiftpopularmodal/sendgiftpopularmodal.page.html":
  /*!***********************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/sendgiftpopularmodal/sendgiftpopularmodal.page.html ***!
    \***********************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppSendgiftpopularmodalSendgiftpopularmodalPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header mode=\"ios\">\n  <ion-toolbar mode=\"ios\" no-padding>\n      <ion-item slot=\"start\" style=\"font-size: 11px;\" lines=\"none\">\n          <ion-label mode=\"ios\"><b>GEMS:</b>&nbsp;&nbsp;</ion-label>\n          <ion-icon name=\"star\" color=\"warning\" mode=\"ios\"></ion-icon>\n          <ion-text mode=\"ios\">10</ion-text>\n      </ion-item>\n      <ion-item slot=\"end\" style=\"font-size: 11px;\" lines=\"none\">\n          <ion-label mode=\"ios\"><b>COINS:</b>&nbsp;&nbsp;</ion-label>\n          <ion-icon name=\"logo-bitcoin\" color=\"warning\" mode=\"ios\"></ion-icon>\n          <ion-text mode=\"ios\">100</ion-text>\n      </ion-item>\n  </ion-toolbar>\n</ion-header>\n<ion-content mode=\"ios\" text-center style=\"padding-bottom: 0;\">\n  <p style=\"font-size: 13px;\" mode=\"ios\" no-padding><b>{{popular.name}}</b></p>\n <ion-img [src]=\"popular.imagegif\" style=\"width: 40%;margin: 0 auto\" mode=\"ios\"></ion-img>\n <p mode=\"ios\" style=\"line-height: 0;\"> <ion-icon name=\"logo-bitcoin\" color=\"warning\" mode=\"ios\"></ion-icon> {{popular.price}}</p> \n</ion-content>\n<ion-footer mode=\"ios\">\n    <ion-toolbar mode=\"ios\" style=\"padding-left: 10px;padding-right: 10px\">\n        <ion-button slot=\"start\" mode=\"ios\" color=\"danger\" size=\"small\" (click)=\"closeModal()\">\n          Cancel\n        </ion-button>\n         <ion-button slot=\"end\" mode=\"ios\" color=\"secondary\" size=\"small\">\n          Send gift\n        </ion-button>\n      </ion-toolbar>\n</ion-footer>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/sendgiftsilvermodal/sendgiftsilvermodal.page.html":
  /*!*********************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/sendgiftsilvermodal/sendgiftsilvermodal.page.html ***!
    \*********************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppSendgiftsilvermodalSendgiftsilvermodalPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<!-- <ion-header mode=\"ios\">\n  <ion-toolbar mode=\"ios\" no-padding>\n      <ion-item slot=\"start\" style=\"font-size: 11px;\" lines=\"none\">\n          <ion-label mode=\"ios\"><b>XP:</b>&nbsp;&nbsp;</ion-label>\n          <ion-icon name=\"star\" color=\"warning\" mode=\"ios\"></ion-icon>\n          <ion-text mode=\"ios\">{{user_experience | number:'1.0':'en-US'}}</ion-text>\n      </ion-item>\n      <ion-item slot=\"end\" style=\"font-size: 11px;\" lines=\"none\">\n          <ion-label mode=\"ios\"><b>COINS:</b>&nbsp;&nbsp;</ion-label>\n          <ion-icon name=\"logo-bitcoin\" color=\"warning\" mode=\"ios\"></ion-icon>\n          <ion-text mode=\"ios\">{{user_coins | number:'1.0':'en-US'}}</ion-text>\n      </ion-item>\n  </ion-toolbar>\n</ion-header> -->\n<ion-content mode=\"ios\" text-center style=\"padding-bottom: 0;\">\n  <ion-row style=\"padding: 10px;\">\n    <ion-col style=\"color: black\" mode=\"ios\" text-left>\n      <ion-label class=\"lvltext\"><b>COINS:</b>&nbsp;&nbsp;</ion-label><br>\n      <img class=\"lvlicon2\" src=\"assets/icon/dollar.png\">\n      <ion-text class=\"lvlnum\">{{user_coins | number:'1.0':'en-US'}}</ion-text>\n    </ion-col>\n    <ion-col style=\"color: black;\" mode=\"ios\" text-right>\n      <ion-label class=\"lvltext\"><b>XP:</b>&nbsp;&nbsp;</ion-label> <br>\n      <img class=\"lvlicon\" src=\"assets/icon/star.png\">\n      <ion-text class=\"lvlnum\">{{user_experience | number:'1.0':'en-US'}}</ion-text>\n    </ion-col>\n</ion-row>\n  <p mode=\"ios\" no-padding><b>{{silver.name}}</b></p>\n <ion-img [src]=\"silver.imagegif\" style=\"width: 80%;margin: 0 auto\" mode=\"ios\"></ion-img>\n <ion-row style=\"padding: 10px;\">\n    <ion-col style=\"color: black\" mode=\"ios\" text-center>\n      <img class=\"lvlicon\" src=\"assets/icon/dollar.png\">\n      <ion-text class=\"lvlnum\">{{silver.price}}</ion-text>\n    </ion-col>\n  </ion-row>\n <!-- <p mode=\"ios\" style=\"line-height: 0;\"> \n  <ion-icon name=\"logo-bitcoin\" color=\"warning\" mode=\"ios\"></ion-icon> {{silver.price}}</p>  -->\n</ion-content>\n<ion-footer mode=\"ios\">\n    <ion-toolbar mode=\"ios\" style=\"padding-left: 10px;padding-right: 10px\">\n        <ion-button slot=\"start\" mode=\"ios\" color=\"light\" size=\"small\" (click)=\"closeModal()\">\n          Cancel\n        </ion-button>\n          <ion-item>\n            <ion-select value=\"1\" interface=\"popover\" [(ngModel)]=\"giftQuantity\">\n              <ion-select-option value=\"1\">1</ion-select-option>\n              <ion-select-option value=\"10\">10</ion-select-option>\n              <ion-select-option value=\"50\">50</ion-select-option>\n              <ion-select-option value=\"100\">100</ion-select-option>\n            </ion-select>\n          </ion-item>\n         <ion-button slot=\"end\" mode=\"ios\" size=\"small\" (click)=\"sendGift()\">\n          Send gift\n        </ion-button>\n      </ion-toolbar>\n</ion-footer>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/suspendmodal/suspendmodal.page.html":
  /*!*******************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/suspendmodal/suspendmodal.page.html ***!
    \*******************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppSuspendmodalSuspendmodalPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n<ion-content padding text-center>\n  <ion-row justify-content-center align-items-center style='height: 100%'>\n    <ion-col>\n      <img style=\"width: 40%;\" src=\"assets/greenthumb-images/notice_emoticon.png\">\n      <ion-label color=\"secondary\"><h2 class=\"message\">Greenthumb Notice</h2></ion-label>\n      <p style=\"padding: 0 20px;\">\n        Greetings! Your account has been temporarily suspended due to not acknowledging and filling up your details in our modal. \n      </p>  \n      <p style=\"padding: 0 20px;\">To reactivate your account, please email <b>admin@greenthumbtrade.com</b>.\n        And for more details, please go to our User Policy Page.\n      </p>\n      <p style=\"padding: 0 20px;\">\n        <ion-note style=\"font-weight: bold;\" color=\"secondary\">Thank you!</ion-note>\n      </p>\n      <ion-button style=\"margin-top: 20px;\" mode=\"ios\" expand=\"block\" (click)=\"goToUserPolicy();\">VISIT USER POLICY</ion-button>\n      <ion-button style=\"margin-top: 10px;\" mode=\"ios\" expand=\"block\" fill=\"outline\" (click)=\"goBack()\">GO BACK</ion-button>\n    </ion-col>\n  </ion-row>\n\n\n</ion-content>\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/userpolicy/userpolicy.page.html":
  /*!***************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/userpolicy/userpolicy.page.html ***!
    \***************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppUserpolicyUserpolicyPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header mode=\"ios\" no-border>\n  <ion-toolbar mode=\"ios\">\n      <ion-buttons slot=\"secondary\" (click)=\"goBack()\">\n        <ion-button mode=\"ios\" style=\"font-size: 15px;\">\n            Back\n        </ion-button>\n      </ion-buttons>\n\n      <ion-title color=\"secondary\">User Policy</ion-title>\n\n      <!-- <ion-buttons slot=\"primary\" (click)=\"goHelp()\">\n        <ion-button mode=\"ios\" style=\"font-size: 15px;\">\n            Help\n        </ion-button>\n      </ion-buttons> -->\n    </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <iframe src=\"https://www.greenthumbtrade.com/user-policy\" style=\"height: 100%; width: 100%;\"></iframe>\n</ion-content>\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/vippackage/vippackage.page.html":
  /*!***************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/vippackage/vippackage.page.html ***!
    \***************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppVippackageVippackagePageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header mode=\"ios\" no-border>\n  <ion-toolbar mode=\"ios\">\n    <ion-buttons slot=\"secondary\" (click)=\"goBack()\">\n      <ion-button mode=\"ios\">\n          <ion-icon name=\"arrow-back\"  mode=\"ios\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n\n      <ion-title>VIP Package</ion-title>\n\n      <ion-buttons slot=\"primary\" mode=\"ios\">\n          <ion-button mode=\"ios\">\n              <img src=\"assets/icon/brixylogo.png\" style=\"width: 32px;\" mode=\"ios\">\n          </ion-button>\n        </ion-buttons>\n    </ion-toolbar>\n</ion-header>\n\n<ion-content mode=\"ios\" >\n\n  <ion-card mode=\"ios\" no-padding style=\"box-shadow: none;\"> \n    <ion-card-header text-left mode=\"ios\" no-padding>\n      <ion-card-subtitle style=\"padding-bottom: 0.5%;\"><b>Go Premium</b></ion-card-subtitle>\n      <ion-card-title style=\"font-weight: bolder;font-size: 21px;\">Choose a Brixy Live Package</ion-card-title>\n    </ion-card-header><br>\n    <ion-card-content mode=\"ios\" no-padding>\n      <ion-row>\n        <ion-col no-padding>\n          <!-- for captain -->\n          <ion-list *ngIf=\"vippackage=='3'\" mode=\"ios\">\n            <ion-item mode=\"ios\" lines=\"none\" style=\"font-size: 13px;\">\n              <ion-icon class=\"item_icon\" name=\"checkmark\" color=\"primary\" slot=\"start\" mode=\"ios\"></ion-icon>\n              <ion-label class=\"item_package\" mode=\"ios\">Captain</ion-label>\n            </ion-item>\n            <ion-item mode=\"ios\" lines=\"none\" class=\"item_sec\">\n              <ion-icon class=\"item_icon\" name=\"checkmark\" color=\"primary\" slot=\"start\" mode=\"ios\"></ion-icon>\n              <ion-label class=\"item_label\" mode=\"ios\">special avatar</ion-label>\n            </ion-item>\n            <ion-item mode=\"ios\" lines=\"none\" class=\"item_sec\">\n              <ion-icon class=\"item_icon\" name=\"checkmark\" color=\"primary\" slot=\"start\" mode=\"ios\"></ion-icon>\n              <ion-label class=\"item_label\" mode=\"ios\">special profile card frame</ion-label>\n            </ion-item>\n            <ion-item mode=\"ios\" lines=\"none\" class=\"item_sec\">\n              <ion-icon class=\"item_icon\" name=\"checkmark\" color=\"primary\" slot=\"start\" mode=\"ios\"></ion-icon>\n              <ion-label class=\"item_label\" mode=\"ios\">distinctive chat bubble</ion-label>\n            </ion-item>\n            <ion-item mode=\"ios\" lines=\"none\" class=\"item_sec\">\n              <ion-icon class=\"item_icon\" name=\"checkmark\" color=\"primary\" slot=\"start\" mode=\"ios\"></ion-icon>\n              <ion-label class=\"item_label\" mode=\"ios\">special entrance effect</ion-label>\n            </ion-item>\n            <ion-item mode=\"ios\" lines=\"none\" class=\"item_sec\">\n              <ion-icon class=\"item_icon\" name=\"checkmark\" color=\"primary\" slot=\"start\" mode=\"ios\"></ion-icon>\n              <ion-label class=\"item_label\" mode=\"ios\">10% discount on private livestream</ion-label>\n            </ion-item>\n          </ion-list>\n\n           <!-- for general -->\n           <ion-list *ngIf=\"vippackage=='2'\" mode=\"ios\">\n            <ion-item mode=\"ios\" lines=\"none\" style=\"font-size: 13px;\">\n              <ion-icon class=\"item_icon\" name=\"checkmark\" color=\"primary\" slot=\"start\" mode=\"ios\"></ion-icon>\n              <ion-label class=\"item_package\" mode=\"ios\">General</ion-label>\n            </ion-item>\n            <ion-item mode=\"ios\" lines=\"none\" class=\"item_sec\">\n              <ion-icon class=\"item_icon\" name=\"checkmark\" color=\"primary\" slot=\"start\" mode=\"ios\"></ion-icon>\n              <ion-label class=\"item_label\" mode=\"ios\">special avatar</ion-label>\n            </ion-item>\n            <ion-item mode=\"ios\" lines=\"none\" class=\"item_sec\">\n              <ion-icon class=\"item_icon\" name=\"checkmark\" color=\"primary\" slot=\"start\" mode=\"ios\"></ion-icon>\n              <ion-label class=\"item_label\" mode=\"ios\">special profile card frame</ion-label>\n            </ion-item>\n            <ion-item mode=\"ios\" lines=\"none\" class=\"item_sec\">\n              <ion-icon class=\"item_icon\" name=\"checkmark\" color=\"primary\" slot=\"start\" mode=\"ios\"></ion-icon>\n              <ion-label class=\"item_label\" mode=\"ios\">distinctive chat bubble</ion-label>\n            </ion-item>\n            <ion-item mode=\"ios\" lines=\"none\" class=\"item_sec\">\n              <ion-icon class=\"item_icon\" name=\"checkmark\" color=\"primary\" slot=\"start\" mode=\"ios\"></ion-icon>\n              <ion-label class=\"item_label\" mode=\"ios\">special entrance effect</ion-label>\n            </ion-item>\n            <ion-item mode=\"ios\" lines=\"none\" class=\"item_sec\">\n              <ion-icon class=\"item_icon\" name=\"checkmark\" color=\"primary\" slot=\"start\" mode=\"ios\"></ion-icon>\n              <ion-label class=\"item_label\" mode=\"ios\">25% discount on private livestream</ion-label>\n            </ion-item>\n          </ion-list>\n\n           <!-- for captain -->\n           <ion-list *ngIf=\"vippackage=='1'\" mode=\"ios\">\n            <ion-item mode=\"ios\" lines=\"none\" style=\"font-size: 13px;\">\n              <ion-icon class=\"item_icon\" name=\"checkmark\" color=\"primary\" slot=\"start\" mode=\"ios\"></ion-icon>\n              <ion-label class=\"item_package\" mode=\"ios\">Commander</ion-label>\n            </ion-item>\n            <ion-item mode=\"ios\" lines=\"none\" class=\"item_sec\">\n              <ion-icon class=\"item_icon\" name=\"checkmark\" color=\"primary\" slot=\"start\" mode=\"ios\"></ion-icon>\n              <ion-label class=\"item_label\" mode=\"ios\">special avatar</ion-label>\n            </ion-item>\n            <ion-item mode=\"ios\" lines=\"none\" class=\"item_sec\">\n              <ion-icon class=\"item_icon\" name=\"checkmark\" color=\"primary\" slot=\"start\" mode=\"ios\"></ion-icon>\n              <ion-label class=\"item_label\" mode=\"ios\">special profile card frame</ion-label>\n            </ion-item>\n            <ion-item mode=\"ios\" lines=\"none\" class=\"item_sec\">\n              <ion-icon class=\"item_icon\" name=\"checkmark\" color=\"primary\" slot=\"start\" mode=\"ios\"></ion-icon>\n              <ion-label class=\"item_label\" mode=\"ios\">distinctive chat bubble</ion-label>\n            </ion-item>\n            <ion-item mode=\"ios\" lines=\"none\" class=\"item_sec\">\n              <ion-icon class=\"item_icon\" name=\"checkmark\" color=\"primary\" slot=\"start\" mode=\"ios\"></ion-icon>\n              <ion-label class=\"item_label\" mode=\"ios\">special entrance effect</ion-label>\n            </ion-item>\n            <ion-item mode=\"ios\" lines=\"none\" class=\"item_sec\">\n              <ion-icon class=\"item_icon\" name=\"checkmark\" color=\"primary\" slot=\"start\" mode=\"ios\"></ion-icon>\n              <ion-label class=\"item_label\" mode=\"ios\">50% discount on private livestream</ion-label>\n            </ion-item>\n          </ion-list>\n        </ion-col>\n        <ion-col no-padding class=\"bk_treasure\"></ion-col>\n      </ion-row>\n    </ion-card-content>\n  </ion-card>\n\n    <ion-radio-group [(ngModel)]=\"vippackage\" >\n      <ion-row padding style=\"padding-top: 0;padding-bottom: 0;\">\n        <ion-col style=\"margin-right: 10px;padding: 0;\" class=\"ion-align-self-start\">\n          <ion-item lines=\"none\" id=\"radio_uncheck\">\n            <div padding style=\"width: 100%;\">\n              <ion-radio class=\"item_radio\" [(disabled)]=\"withPackage\" value=\"3\"></ion-radio>\n              <br><br>\n              <img class=\"lvlicon\" src=\"assets/icon/imgcoin.png\"><br>\n              <ion-text class=\"lvlnum\" style=\"font-weight: bolder;\">{{300 | number:'1.0':'en-US'}}</ion-text>\n              <h3 class=\"prename\"><b>Captain</b></h3>\n            </div>\n          </ion-item>\n        </ion-col>\n        <ion-col style=\"margin-right: 10px;padding: 0;\" class=\"ion-align-self-start\">\n          <ion-item lines=\"none\" id=\"radio_uncheck\">\n            <div padding style=\"width: 100%;\">\n              <ion-radio class=\"item_radio\" [(disabled)]=\"withPackage\" value=\"2\"></ion-radio>\n              <br><br>\n              <img class=\"lvlicon\" src=\"assets/icon/imgcoin.png\"><br>\n                <ion-text class=\"lvlnum\" style=\"font-weight: bolder;\">{{500 | number:'1.0':'en-US'}}</ion-text>\n                <h3 class=\"prename\"><b>General</b></h3>\n            </div>\n          </ion-item>\n        </ion-col>\n        <ion-col style=\"padding: 0;\" class=\"ion-align-self-start\">\n          <ion-item lines=\"none\" id=\"radio_uncheck\">\n            <div padding style=\"width: 100%;\">\n              <ion-radio class=\"item_radio\" [(disabled)]=\"withPackage\" value=\"1\"></ion-radio>\n              <br><br>\n              <img class=\"lvlicon\" src=\"assets/icon/imgcoin.png\"><br>\n              <ion-text class=\"lvlnum\" style=\"font-weight: bolder;\">{{ 1000 | number:'1.0':'en-US'}}</ion-text>\n              <h3 class=\"prename\"><b>Commander</b></h3>\n            </div>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n      \n  </ion-radio-group>\n\n      <ion-row padding  text-center style=\"padding-bottom: 0;\">\n          <ion-col>\n            <p style=\"color: #737373;font-size: 14px;\">You are currently subscribed as a Captain/General/Commander</p>\n              <ion-button expand=\"block\" mode=\"ios\" (click)=\"subscribe()\">SUBSCRIBE</ion-button>\n          </ion-col>\n      </ion-row>\n   \n      <ion-row padding text-center style=\"padding-top: 0;\" *ngIf=\"withPackage\">\n        <ion-col size=\"1\"></ion-col>\n        <ion-col size=\"10\" text-center>\n          <ion-item lines=\"none\" text-center>\n            <ion-label text-center style=\"font-size: 14px;\"><b> Monthly Subscription Auto Renew</b></ion-label>\n            <ion-checkbox text-center (click)=\"addValue($event)\" [(ngModel)]=\"monthlySubscription\" slot=\"start\" mode=\"ios\" style=\"font-size: 10px;margin-right: 0px;\" color=\"primary\"></ion-checkbox>\n          </ion-item>\n          <p text-center style=\"color: #737373;font-size: 13px;padding: 0px;\">\n            The VIP Package is on a fixed 30 day subscription. Unchecking the Auto Renew automatically cancels your\n            subscription on the succeeding month.\n          </p>\n        </ion-col>\n        <ion-col size=\"1\"></ion-col>\n      </ion-row>\n\n      <!-- <ion-row padding text-center style=\"padding-top: 0;\">\n        <ion-col>\n          <h5 style=\"font-weight: bold;\">Lorem Title here</h5>\n          <p text-center style=\"color: #737373;font-size: 13px;padding-left: 20px;padding-right: 20px;\">\n            The VIP Package is on a fixed 30 day subscription. Unchecking the Auto Renew automatically cancels your\n            subscription on the succeeding month.\n          </p>\n        </ion-col>\n    </ion-row> -->\n  \n</ion-content>\n";
    /***/
  },

  /***/
  "./node_modules/tslib/tslib.es6.js":
  /*!*****************************************!*\
    !*** ./node_modules/tslib/tslib.es6.js ***!
    \*****************************************/

  /*! exports provided: __extends, __assign, __rest, __decorate, __param, __metadata, __awaiter, __generator, __createBinding, __exportStar, __values, __read, __spread, __spreadArrays, __await, __asyncGenerator, __asyncDelegator, __asyncValues, __makeTemplateObject, __importStar, __importDefault, __classPrivateFieldGet, __classPrivateFieldSet */

  /***/
  function node_modulesTslibTslibEs6Js(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__extends", function () {
      return __extends;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__assign", function () {
      return _assign;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__rest", function () {
      return __rest;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__decorate", function () {
      return __decorate;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__param", function () {
      return __param;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__metadata", function () {
      return __metadata;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__awaiter", function () {
      return __awaiter;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__generator", function () {
      return __generator;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__createBinding", function () {
      return __createBinding;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__exportStar", function () {
      return __exportStar;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__values", function () {
      return __values;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__read", function () {
      return __read;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__spread", function () {
      return __spread;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__spreadArrays", function () {
      return __spreadArrays;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__await", function () {
      return __await;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__asyncGenerator", function () {
      return __asyncGenerator;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__asyncDelegator", function () {
      return __asyncDelegator;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__asyncValues", function () {
      return __asyncValues;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__makeTemplateObject", function () {
      return __makeTemplateObject;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__importStar", function () {
      return __importStar;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__importDefault", function () {
      return __importDefault;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__classPrivateFieldGet", function () {
      return __classPrivateFieldGet;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__classPrivateFieldSet", function () {
      return __classPrivateFieldSet;
    });
    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation.
    
    Permission to use, copy, modify, and/or distribute this software for any
    purpose with or without fee is hereby granted.
    
    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
    REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
    AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
    INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
    LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
    OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
    PERFORMANCE OF THIS SOFTWARE.
    ***************************************************************************** */

    /* global Reflect, Promise */


    var _extendStatics = function extendStatics(d, b) {
      _extendStatics = Object.setPrototypeOf || {
        __proto__: []
      } instanceof Array && function (d, b) {
        d.__proto__ = b;
      } || function (d, b) {
        for (var p in b) {
          if (b.hasOwnProperty(p)) d[p] = b[p];
        }
      };

      return _extendStatics(d, b);
    };

    function __extends(d, b) {
      _extendStatics(d, b);

      function __() {
        this.constructor = d;
      }

      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }

    var _assign = function __assign() {
      _assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
          s = arguments[i];

          for (var p in s) {
            if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
          }
        }

        return t;
      };

      return _assign.apply(this, arguments);
    };

    function __rest(s, e) {
      var t = {};

      for (var p in s) {
        if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
      }

      if (s != null && typeof Object.getOwnPropertySymbols === "function") for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
        if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i])) t[p[i]] = s[p[i]];
      }
      return t;
    }

    function __decorate(decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    }

    function __param(paramIndex, decorator) {
      return function (target, key) {
        decorator(target, key, paramIndex);
      };
    }

    function __metadata(metadataKey, metadataValue) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
    }

    function __awaiter(thisArg, _arguments, P, generator) {
      function adopt(value) {
        return value instanceof P ? value : new P(function (resolve) {
          resolve(value);
        });
      }

      return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) {
          try {
            step(generator.next(value));
          } catch (e) {
            reject(e);
          }
        }

        function rejected(value) {
          try {
            step(generator["throw"](value));
          } catch (e) {
            reject(e);
          }
        }

        function step(result) {
          result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
        }

        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    }

    function __generator(thisArg, body) {
      var _ = {
        label: 0,
        sent: function sent() {
          if (t[0] & 1) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      },
          f,
          y,
          t,
          g;
      return g = {
        next: verb(0),
        "throw": verb(1),
        "return": verb(2)
      }, typeof Symbol === "function" && (g[Symbol.iterator] = function () {
        return this;
      }), g;

      function verb(n) {
        return function (v) {
          return step([n, v]);
        };
      }

      function step(op) {
        if (f) throw new TypeError("Generator is already executing.");

        while (_) {
          try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];

            switch (op[0]) {
              case 0:
              case 1:
                t = op;
                break;

              case 4:
                _.label++;
                return {
                  value: op[1],
                  done: false
                };

              case 5:
                _.label++;
                y = op[1];
                op = [0];
                continue;

              case 7:
                op = _.ops.pop();

                _.trys.pop();

                continue;

              default:
                if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) {
                  _ = 0;
                  continue;
                }

                if (op[0] === 3 && (!t || op[1] > t[0] && op[1] < t[3])) {
                  _.label = op[1];
                  break;
                }

                if (op[0] === 6 && _.label < t[1]) {
                  _.label = t[1];
                  t = op;
                  break;
                }

                if (t && _.label < t[2]) {
                  _.label = t[2];

                  _.ops.push(op);

                  break;
                }

                if (t[2]) _.ops.pop();

                _.trys.pop();

                continue;
            }

            op = body.call(thisArg, _);
          } catch (e) {
            op = [6, e];
            y = 0;
          } finally {
            f = t = 0;
          }
        }

        if (op[0] & 5) throw op[1];
        return {
          value: op[0] ? op[1] : void 0,
          done: true
        };
      }
    }

    function __createBinding(o, m, k, k2) {
      if (k2 === undefined) k2 = k;
      o[k2] = m[k];
    }

    function __exportStar(m, exports) {
      for (var p in m) {
        if (p !== "default" && !exports.hasOwnProperty(p)) exports[p] = m[p];
      }
    }

    function __values(o) {
      var s = typeof Symbol === "function" && Symbol.iterator,
          m = s && o[s],
          i = 0;
      if (m) return m.call(o);
      if (o && typeof o.length === "number") return {
        next: function next() {
          if (o && i >= o.length) o = void 0;
          return {
            value: o && o[i++],
            done: !o
          };
        }
      };
      throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
    }

    function __read(o, n) {
      var m = typeof Symbol === "function" && o[Symbol.iterator];
      if (!m) return o;
      var i = m.call(o),
          r,
          ar = [],
          e;

      try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) {
          ar.push(r.value);
        }
      } catch (error) {
        e = {
          error: error
        };
      } finally {
        try {
          if (r && !r.done && (m = i["return"])) m.call(i);
        } finally {
          if (e) throw e.error;
        }
      }

      return ar;
    }

    function __spread() {
      for (var ar = [], i = 0; i < arguments.length; i++) {
        ar = ar.concat(__read(arguments[i]));
      }

      return ar;
    }

    function __spreadArrays() {
      for (var s = 0, i = 0, il = arguments.length; i < il; i++) {
        s += arguments[i].length;
      }

      for (var r = Array(s), k = 0, i = 0; i < il; i++) {
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++) {
          r[k] = a[j];
        }
      }

      return r;
    }

    ;

    function __await(v) {
      return this instanceof __await ? (this.v = v, this) : new __await(v);
    }

    function __asyncGenerator(thisArg, _arguments, generator) {
      if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
      var g = generator.apply(thisArg, _arguments || []),
          i,
          q = [];
      return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () {
        return this;
      }, i;

      function verb(n) {
        if (g[n]) i[n] = function (v) {
          return new Promise(function (a, b) {
            q.push([n, v, a, b]) > 1 || resume(n, v);
          });
        };
      }

      function resume(n, v) {
        try {
          step(g[n](v));
        } catch (e) {
          settle(q[0][3], e);
        }
      }

      function step(r) {
        r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r);
      }

      function fulfill(value) {
        resume("next", value);
      }

      function reject(value) {
        resume("throw", value);
      }

      function settle(f, v) {
        if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]);
      }
    }

    function __asyncDelegator(o) {
      var i, p;
      return i = {}, verb("next"), verb("throw", function (e) {
        throw e;
      }), verb("return"), i[Symbol.iterator] = function () {
        return this;
      }, i;

      function verb(n, f) {
        i[n] = o[n] ? function (v) {
          return (p = !p) ? {
            value: __await(o[n](v)),
            done: n === "return"
          } : f ? f(v) : v;
        } : f;
      }
    }

    function __asyncValues(o) {
      if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
      var m = o[Symbol.asyncIterator],
          i;
      return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () {
        return this;
      }, i);

      function verb(n) {
        i[n] = o[n] && function (v) {
          return new Promise(function (resolve, reject) {
            v = o[n](v), settle(resolve, reject, v.done, v.value);
          });
        };
      }

      function settle(resolve, reject, d, v) {
        Promise.resolve(v).then(function (v) {
          resolve({
            value: v,
            done: d
          });
        }, reject);
      }
    }

    function __makeTemplateObject(cooked, raw) {
      if (Object.defineProperty) {
        Object.defineProperty(cooked, "raw", {
          value: raw
        });
      } else {
        cooked.raw = raw;
      }

      return cooked;
    }

    ;

    function __importStar(mod) {
      if (mod && mod.__esModule) return mod;
      var result = {};
      if (mod != null) for (var k in mod) {
        if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
      }
      result["default"] = mod;
      return result;
    }

    function __importDefault(mod) {
      return mod && mod.__esModule ? mod : {
        "default": mod
      };
    }

    function __classPrivateFieldGet(receiver, privateMap) {
      if (!privateMap.has(receiver)) {
        throw new TypeError("attempted to get private field on non-instance");
      }

      return privateMap.get(receiver);
    }

    function __classPrivateFieldSet(receiver, privateMap, value) {
      if (!privateMap.has(receiver)) {
        throw new TypeError("attempted to set private field on non-instance");
      }

      privateMap.set(receiver, value);
      return value;
    }
    /***/

  },

  /***/
  "./src/app/allgifts/allgifts.module.ts":
  /*!*********************************************!*\
    !*** ./src/app/allgifts/allgifts.module.ts ***!
    \*********************************************/

  /*! exports provided: AllgiftsPageModule */

  /***/
  function srcAppAllgiftsAllgiftsModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AllgiftsPageModule", function () {
      return AllgiftsPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _allgifts_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./allgifts.page */
    "./src/app/allgifts/allgifts.page.ts");

    var routes = [{
      path: '',
      component: _allgifts_page__WEBPACK_IMPORTED_MODULE_6__["AllgiftsPage"]
    }];

    var AllgiftsPageModule = function AllgiftsPageModule() {
      _classCallCheck(this, AllgiftsPageModule);
    };

    AllgiftsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      declarations: [_allgifts_page__WEBPACK_IMPORTED_MODULE_6__["AllgiftsPage"]]
    })], AllgiftsPageModule);
    /***/
  },

  /***/
  "./src/app/allgifts/allgifts.page.scss":
  /*!*********************************************!*\
    !*** ./src/app/allgifts/allgifts.page.scss ***!
    \*********************************************/

  /*! exports provided: default */

  /***/
  function srcAppAllgiftsAllgiftsPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "ion-item {\n  --padding-start: 0% !important;\n}\n\n.lvlnum {\n  font-size: 11px;\n  font-weight: bolder;\n  font-family: Verdana, Geneva, Tahoma, sans-serif;\n  display: inline-block;\n}\n\n.lvlnum2 {\n  margin: 0;\n  margin-top: 4%;\n  font-weight: bolder;\n  font-family: Verdana, Geneva, Tahoma, sans-serif;\n}\n\n.lvlicon {\n  width: 20px !important;\n  height: 20px;\n  margin: auto;\n  margin-bottom: -2%;\n  margin-right: 3%;\n}\n\n.lvlicon2 {\n  width: 25px !important;\n  height: 25px;\n  margin: auto;\n  margin-right: 3%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZW5kcmljay9EZXNrdG9wL1RlbG1vIFNvbHV0aW9ucy9Gb3JQcm9kdWN0aW9uL21heTEwMjAyMS9sYXN0IHNldHVwL2dyZWVudGh1bWJhcHAgY29weS9zcmMvYXBwL2FsbGdpZnRzL2FsbGdpZnRzLnBhZ2Uuc2NzcyIsInNyYy9hcHAvYWxsZ2lmdHMvYWxsZ2lmdHMucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO0VBQ0ksOEJBQUE7QUNBSjs7QURFRTtFQUNFLGVBQUE7RUFDQSxtQkFBQTtFQUNBLGdEQUFBO0VBQ0EscUJBQUE7QUNDSjs7QURDRTtFQUNFLFNBQUE7RUFDQSxjQUFBO0VBQ0EsbUJBQUE7RUFDQSxnREFBQTtBQ0VKOztBRENFO0VBQ0Usc0JBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7QUNFSjs7QURBRTtFQUNFLHNCQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFFQSxnQkFBQTtBQ0VKIiwiZmlsZSI6InNyYy9hcHAvYWxsZ2lmdHMvYWxsZ2lmdHMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXG5pb24taXRlbSB7XG4gICAgLS1wYWRkaW5nLXN0YXJ0OiAwJSAhaW1wb3J0YW50O1xuICB9XG4gIC5sdmxudW17XG4gICAgZm9udC1zaXplOiAxMXB4O1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkZXI7XG4gICAgZm9udC1mYW1pbHk6IFZlcmRhbmEsIEdlbmV2YSwgVGFob21hLCBzYW5zLXNlcmlmO1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgfVxuICAubHZsbnVtMntcbiAgICBtYXJnaW46IDA7XG4gICAgbWFyZ2luLXRvcDogNCU7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbiAgICBmb250LWZhbWlseTogVmVyZGFuYSwgR2VuZXZhLCBUYWhvbWEsIHNhbnMtc2VyaWY7XG4gIH1cblxuICAubHZsaWNvbntcbiAgICB3aWR0aDogMjBweCAhaW1wb3J0YW50OyBcbiAgICBoZWlnaHQ6IDIwcHg7IFxuICAgIG1hcmdpbjogYXV0bztcbiAgICBtYXJnaW4tYm90dG9tOiAtMiU7XG4gICAgbWFyZ2luLXJpZ2h0OiAzJTtcbiAgfVxuICAubHZsaWNvbjJ7XG4gICAgd2lkdGg6IDI1cHggIWltcG9ydGFudDsgXG4gICAgaGVpZ2h0OiAyNXB4OyBcbiAgICBtYXJnaW46IGF1dG87XG4gICAgLy8gbWFyZ2luLWJvdHRvbTogLTIlO1xuICAgIG1hcmdpbi1yaWdodDogMyU7XG4gIH0iLCJpb24taXRlbSB7XG4gIC0tcGFkZGluZy1zdGFydDogMCUgIWltcG9ydGFudDtcbn1cblxuLmx2bG51bSB7XG4gIGZvbnQtc2l6ZTogMTFweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbiAgZm9udC1mYW1pbHk6IFZlcmRhbmEsIEdlbmV2YSwgVGFob21hLCBzYW5zLXNlcmlmO1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG59XG5cbi5sdmxudW0yIHtcbiAgbWFyZ2luOiAwO1xuICBtYXJnaW4tdG9wOiA0JTtcbiAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbiAgZm9udC1mYW1pbHk6IFZlcmRhbmEsIEdlbmV2YSwgVGFob21hLCBzYW5zLXNlcmlmO1xufVxuXG4ubHZsaWNvbiB7XG4gIHdpZHRoOiAyMHB4ICFpbXBvcnRhbnQ7XG4gIGhlaWdodDogMjBweDtcbiAgbWFyZ2luOiBhdXRvO1xuICBtYXJnaW4tYm90dG9tOiAtMiU7XG4gIG1hcmdpbi1yaWdodDogMyU7XG59XG5cbi5sdmxpY29uMiB7XG4gIHdpZHRoOiAyNXB4ICFpbXBvcnRhbnQ7XG4gIGhlaWdodDogMjVweDtcbiAgbWFyZ2luOiBhdXRvO1xuICBtYXJnaW4tcmlnaHQ6IDMlO1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/allgifts/allgifts.page.ts":
  /*!*******************************************!*\
    !*** ./src/app/allgifts/allgifts.page.ts ***!
    \*******************************************/

  /*! exports provided: AllgiftsPage */

  /***/
  function srcAppAllgiftsAllgiftsPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AllgiftsPage", function () {
      return AllgiftsPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _sendgiftbronzemodal_sendgiftbronzemodal_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../sendgiftbronzemodal/sendgiftbronzemodal.page */
    "./src/app/sendgiftbronzemodal/sendgiftbronzemodal.page.ts");
    /* harmony import */


    var _sendgiftsilvermodal_sendgiftsilvermodal_page__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../sendgiftsilvermodal/sendgiftsilvermodal.page */
    "./src/app/sendgiftsilvermodal/sendgiftsilvermodal.page.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _shared_model_gifts_model__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../shared/model/gifts.model */
    "./src/app/shared/model/gifts.model.ts");
    /* harmony import */


    var src_providers_credential_provider__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/providers/credential-provider */
    "./src/providers/credential-provider.ts");

    var AllgiftsPage = /*#__PURE__*/function () {
      function AllgiftsPage(modalController, router, navParams, postPvdr) {
        _classCallCheck(this, AllgiftsPage);

        this.modalController = modalController;
        this.router = router;
        this.postPvdr = postPvdr;
        this.player = null;
        this.segment = 0;
        this.bronzeList = []; //  {
        //    //not yet
        //   name:'Brixy Butterfly Basic Bronze',
        //  image:'assets/gif/rainbow.gif',
        //  imagegif: 'assets/gif/rainbow.gif',
        //  imageaudio: 'assets/gifaudio/lips.mp3',
        //  imageaudio2: '',
        //  price:"5000"
        //  }
        //  async openBronze(bronze) {
        //       const modal = await this.modalController.create({
        //         component: SendgiftbronzemodalPage,
        //         cssClass: 'sendgiftmodal',
        //         componentProps: { bronze: bronze, 'live_user_id' : this.live_user_id}
        //       });
        //   if (this.player != null) {
        //     this.player.stop();
        //     this.player.unload();
        //     this.player = null;
        // }
        //       this.player = new Howl({
        //         src: [bronze.imageaudio],
        //         onend: function() {
        //             this.player = new Howl({
        //                   src: [bronze.imageaudio2]
        //                 });
        //                 this.player.play();
        //               }
        //       });
        //       this.player.play();
        //       Howler.volume(0.5);
        //    
        //       return await modal.present();
        //       }

        this.silverList = [];
        this.goldList = [];
        this.popularList = [];
        this.live_user_id = navParams.get('live_user_id');
      }

      _createClass(AllgiftsPage, [{
        key: "segmentChanged",
        value: function segmentChanged() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    _context.next = 2;
                    return this.slider.slideTo(this.segment);

                  case 2:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        }
      }, {
        key: "slideChanged",
        value: function slideChanged() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
            return regeneratorRuntime.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    _context2.next = 2;
                    return this.slider.getActiveIndex();

                  case 2:
                    this.segment = _context2.sent;

                  case 3:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this);
          }));
        }
      }, {
        key: "sendgift",
        value: function sendgift() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
            var modal;
            return regeneratorRuntime.wrap(function _callee3$(_context3) {
              while (1) {
                switch (_context3.prev = _context3.next) {
                  case 0:
                    _context3.next = 2;
                    return this.modalController.create({
                      component: _sendgiftbronzemodal_sendgiftbronzemodal_page__WEBPACK_IMPORTED_MODULE_3__["SendgiftbronzemodalPage"],
                      cssClass: 'sendgiftmodal'
                    });

                  case 2:
                    modal = _context3.sent;
                    _context3.next = 5;
                    return modal.present();

                  case 5:
                    return _context3.abrupt("return", _context3.sent);

                  case 6:
                  case "end":
                    return _context3.stop();
                }
              }
            }, _callee3, this);
          }));
        }
      }, {
        key: "viewlive",
        value: function viewlive() {
          this.router.navigate(['viewlive']);
        } // silver page is the one used for all

      }, {
        key: "openSilver",
        value: function openSilver(silver) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
            var modal;
            return regeneratorRuntime.wrap(function _callee4$(_context4) {
              while (1) {
                switch (_context4.prev = _context4.next) {
                  case 0:
                    _context4.next = 2;
                    return this.modalController.create({
                      component: _sendgiftsilvermodal_sendgiftsilvermodal_page__WEBPACK_IMPORTED_MODULE_4__["SendgiftsilvermodalPage"],
                      cssClass: 'sendgiftmodal',
                      componentProps: {
                        silver: silver,
                        'live_user_id': this.live_user_id
                      }
                    });

                  case 2:
                    modal = _context4.sent;
                    _context4.next = 5;
                    return modal.present();

                  case 5:
                    return _context4.abrupt("return", _context4.sent);

                  case 6:
                  case "end":
                    return _context4.stop();
                }
              }
            }, _callee4, this);
          }));
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {
          this.plotData();
        }
      }, {
        key: "plotData",
        value: function plotData() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
            var _this = this;

            var body, body2, body3, body4;
            return regeneratorRuntime.wrap(function _callee5$(_context5) {
              while (1) {
                switch (_context5.prev = _context5.next) {
                  case 0:
                    // for bronze
                    body = {
                      action: 'showGifts',
                      type: 1
                    };
                    this.postPvdr.postData(body, 'brixy-store.php').subscribe(function (data) {
                      console.log(data);

                      if (data.success) {
                        for (var key in data.result) {
                          _this.bronzeList.push(new _shared_model_gifts_model__WEBPACK_IMPORTED_MODULE_6__["giftModel"](data.result[key].id, data.result[key].gifts, data.result[key].image_gif, data.result[key].image_gif, data.result[key].audio, '', data.result[key].gold_bar)); //console.log(data.result[key].user_id);

                        }
                      }
                    }); // for silver

                    body2 = {
                      action: 'showGifts',
                      type: 2
                    };
                    this.postPvdr.postData(body2, 'brixy-store.php').subscribe(function (data) {
                      ;

                      if (data.success) {
                        for (var key in data.result) {
                          _this.silverList.push(new _shared_model_gifts_model__WEBPACK_IMPORTED_MODULE_6__["giftModel"](data.result[key].id, data.result[key].gifts, data.result[key].image_gif, data.result[key].image_gif, data.result[key].audio, '', data.result[key].gold_bar));
                        }
                      }
                    }); // for gold

                    body3 = {
                      action: 'showGifts',
                      type: 3
                    };
                    this.postPvdr.postData(body3, 'brixy-store.php').subscribe(function (data) {
                      if (data.success) {
                        for (var key in data.result) {
                          _this.goldList.push(new _shared_model_gifts_model__WEBPACK_IMPORTED_MODULE_6__["giftModel"](data.result[key].id, data.result[key].gifts, data.result[key].image_gif, data.result[key].image_gif, data.result[key].audio, '', data.result[key].gold_bar));
                        }
                      }
                    }); //for popular gift

                    body4 = {
                      action: 'showGifts',
                      type: 4
                    };
                    this.postPvdr.postData(body4, 'brixy-store.php').subscribe(function (data) {
                      if (data.success) {
                        for (var key in data.result) {
                          _this.popularList.push(new _shared_model_gifts_model__WEBPACK_IMPORTED_MODULE_6__["giftModel"](data.result[key].id, data.result[key].gifts, data.result[key].image_gif, data.result[key].image_gif, data.result[key].audio, '', data.result[key].gold_bar));
                        }
                      }
                    });

                  case 8:
                  case "end":
                    return _context5.stop();
                }
              }
            }, _callee5, this);
          }));
        } // end of plot data

      }, {
        key: "closemodal",
        value: function closemodal() {
          this.modalController.dismiss();
        }
      }]);

      return AllgiftsPage;
    }();

    AllgiftsPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavParams"]
      }, {
        type: src_providers_credential_provider__WEBPACK_IMPORTED_MODULE_7__["PostProvider"]
      }];
    };

    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('slides', {
      "static": true
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonSlides"])], AllgiftsPage.prototype, "slider", void 0);
    AllgiftsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-allgifts',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./allgifts.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/allgifts/allgifts.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./allgifts.page.scss */
      "./src/app/allgifts/allgifts.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavParams"], src_providers_credential_provider__WEBPACK_IMPORTED_MODULE_7__["PostProvider"]])], AllgiftsPage);
    /***/
  },

  /***/
  "./src/app/app-routing.module.ts":
  /*!***************************************!*\
    !*** ./src/app/app-routing.module.ts ***!
    \***************************************/

  /*! exports provided: AppRoutingModule */

  /***/
  function srcAppAppRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function () {
      return AppRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");

    var routes = [{
      path: '',
      redirectTo: 'home',
      pathMatch: 'full'
    }, {
      path: 'home',
      loadChildren: function loadChildren() {
        return __webpack_require__.e(
        /*! import() | home-home-module */
        "home-home-module").then(__webpack_require__.bind(null,
        /*! ./home/home.module */
        "./src/app/home/home.module.ts")).then(function (m) {
          return m.HomePageModule;
        });
      }
    }, {
      path: 'ads',
      loadChildren: './ads/ads.module#AdsPageModule'
    }, {
      path: 'guide',
      loadChildren: './guide/guide.module#GuidePageModule'
    }, {
      path: 'choose',
      loadChildren: './choose/choose.module#ChoosePageModule'
    }, {
      path: 'mobilee',
      loadChildren: './mobilee/mobilee.module#MobileePageModule'
    }, {
      path: 'email-login',
      loadChildren: './email-login/email-login.module#EmailLoginPageModule'
    }, {
      path: 'email-code/:email_code',
      loadChildren: './email-code/email-code.module#EmailCodePageModule'
    }, {
      path: 'tabs',
      loadChildren: './tabs/tabs.module#TabsPageModule'
    }, {
      path: 'brixyloads',
      loadChildren: './brixyloads/brixyloads.module#BrixyloadsPageModule'
    }, {
      path: 'tab3',
      loadChildren: './tab3/tab3.module#Tab3PageModule'
    }, {
      path: 'termsconditions',
      loadChildren: './termsconditions/termsconditions.module#TermsconditionsPageModule'
    }, {
      path: 'tab4',
      loadChildren: './tab4/tab4.module#Tab4PageModule'
    }, {
      path: 'tab5',
      loadChildren: './tab5/tab5.module#Tab5PageModule'
    }, {
      path: 'tab6',
      loadChildren: './tab6/tab6.module#Tab6PageModule'
    }, {
      path: 'myaccount',
      loadChildren: './myaccount/myaccount.module#MyaccountPageModule'
    }, {
      path: 'settings',
      loadChildren: './settings/settings.module#SettingsPageModule'
    }, {
      path: 'editprofile',
      loadChildren: './editprofile/editprofile.module#EditprofilePageModule'
    }, {
      path: 'message',
      loadChildren: './message/message.module#MessagePageModule'
    }, {
      path: 'message/:id',
      loadChildren: './message/message.module#MessagePageModule'
    }, {
      path: 'viewlive',
      loadChildren: './viewlive/viewlive.module#ViewlivePageModule'
    }, {
      path: 'viewlive/:id',
      loadChildren: './viewlive/viewlive.module#ViewlivePageModule'
    }, {
      path: 'allgifts',
      loadChildren: './allgifts/allgifts.module#AllgiftsPageModule'
    }, {
      path: 'brixyvideo',
      loadChildren: './brixyvideo/brixyvideo.module#BrixyvideoPageModule'
    }, {
      path: 'mybroadcast',
      loadChildren: './mybroadcast/mybroadcast.module#MybroadcastPageModule'
    }, {
      path: 'sendgiftbronzemodal',
      loadChildren: './sendgiftbronzemodal/sendgiftbronzemodal.module#SendgiftbronzemodalPageModule'
    }, {
      path: 'sendgiftsilvermodal',
      loadChildren: './sendgiftsilvermodal/sendgiftsilvermodal.module#SendgiftsilvermodalPageModule'
    }, {
      path: 'sendgiftgoldmodal',
      loadChildren: './sendgiftgoldmodal/sendgiftgoldmodal.module#SendgiftgoldmodalPageModule'
    }, {
      path: 'sendgiftpopularmodal',
      loadChildren: './sendgiftpopularmodal/sendgiftpopularmodal.module#SendgiftpopularmodalPageModule'
    }, {
      path: 'terms',
      loadChildren: './terms/terms.module#TermsPageModule'
    }, {
      path: 'privacy',
      loadChildren: './privacy/privacy.module#PrivacyPageModule'
    }, {
      path: 'readytogolive',
      loadChildren: './readytogolive/readytogolive.module#ReadytogolivePageModule'
    }, {
      path: 'wallet',
      loadChildren: './wallet/wallet.module#WalletPageModule'
    }, {
      path: 'paymentrequest',
      loadChildren: './paymentrequest/paymentrequest.module#PaymentrequestPageModule'
    }, {
      path: 'historypreview',
      loadChildren: './historypreview/historypreview.module#HistorypreviewPageModule'
    }, {
      path: 'historygiftpreview',
      loadChildren: './historygiftpreview/historygiftpreview.module#HistorygiftpreviewPageModule'
    }, {
      path: 'termsconditionslogin',
      loadChildren: './termsconditionslogin/termsconditionslogin.module#TermsconditionsloginPageModule'
    }, {
      path: 'liveprofile',
      loadChildren: './liveprofile/liveprofile.module#LiveprofilePageModule'
    }, {
      path: 'register',
      loadChildren: './register/register.module#RegisterPageModule'
    }, {
      path: 'uploadphoto',
      loadChildren: './uploadphoto/uploadphoto.module#UploadphotoPageModule'
    }, {
      path: 'livestream',
      loadChildren: './livestream/livestream.module#LivestreamPageModule'
    }, {
      path: 'email-login',
      loadChildren: function loadChildren() {
        return __webpack_require__.e(
        /*! import() | email-login-email-login-module */
        "email-login-email-login-module").then(__webpack_require__.bind(null,
        /*! ./email-login/email-login.module */
        "./src/app/email-login/email-login.module.ts")).then(function (m) {
          return m.EmailLoginPageModule;
        });
      }
    }, {
      path: 'email-code',
      loadChildren: function loadChildren() {
        return __webpack_require__.e(
        /*! import() | email-code-email-code-module */
        "email-code-email-code-module").then(__webpack_require__.bind(null,
        /*! ./email-code/email-code.module */
        "./src/app/email-code/email-code.module.ts")).then(function (m) {
          return m.EmailCodePageModule;
        });
      }
    }, {
      path: 'purchasecoins',
      loadChildren: function loadChildren() {
        return Promise.resolve().then(__webpack_require__.bind(null,
        /*! ./purchasecoins/purchasecoins.module */
        "./src/app/purchasecoins/purchasecoins.module.ts")).then(function (m) {
          return m.PurchasecoinsPageModule;
        });
      }
    }, {
      path: 'search',
      loadChildren: function loadChildren() {
        return Promise.all(
        /*! import() | search-search-module */
        [__webpack_require__.e("common"), __webpack_require__.e("search-search-module")]).then(__webpack_require__.bind(null,
        /*! ./search/search.module */
        "./src/app/search/search.module.ts")).then(function (m) {
          return m.SearchPageModule;
        });
      }
    }, {
      path: 'forgotpassword',
      loadChildren: function loadChildren() {
        return __webpack_require__.e(
        /*! import() | forgotpassword-forgotpassword-module */
        "forgotpassword-forgotpassword-module").then(__webpack_require__.bind(null,
        /*! ./forgotpassword/forgotpassword.module */
        "./src/app/forgotpassword/forgotpassword.module.ts")).then(function (m) {
          return m.ForgotpasswordPageModule;
        });
      }
    }, {
      path: 'paymentrequest',
      loadChildren: function loadChildren() {
        return __webpack_require__.e(
        /*! import() | paymentrequest-paymentrequest-module */
        "paymentrequest-paymentrequest-module").then(__webpack_require__.bind(null,
        /*! ./paymentrequest/paymentrequest.module */
        "./src/app/paymentrequest/paymentrequest.module.ts")).then(function (m) {
          return m.PaymentrequestPageModule;
        });
      }
    }, {
      path: 'reportuser',
      loadChildren: function loadChildren() {
        return Promise.resolve().then(__webpack_require__.bind(null,
        /*! ./reportuser/reportuser.module */
        "./src/app/reportuser/reportuser.module.ts")).then(function (m) {
          return m.ReportuserPageModule;
        });
      }
    }, {
      path: 'vippackage',
      loadChildren: function loadChildren() {
        return Promise.resolve().then(__webpack_require__.bind(null,
        /*! ./vippackage/vippackage.module */
        "./src/app/vippackage/vippackage.module.ts")).then(function (m) {
          return m.VippackagePageModule;
        });
      }
    }, {
      path: 'vipackage',
      loadChildren: function loadChildren() {
        return __webpack_require__.e(
        /*! import() | vipackage-vipackage-module */
        "vipackage-vipackage-module").then(__webpack_require__.bind(null,
        /*! ./vipackage/vipackage.module */
        "./src/app/vipackage/vipackage.module.ts")).then(function (m) {
          return m.VipackagePageModule;
        });
      }
    }, {
      path: 'payoneerid',
      loadChildren: function loadChildren() {
        return __webpack_require__.e(
        /*! import() | payoneerid-payoneerid-module */
        "payoneerid-payoneerid-module").then(__webpack_require__.bind(null,
        /*! ./payoneerid/payoneerid.module */
        "./src/app/payoneerid/payoneerid.module.ts")).then(function (m) {
          return m.PayoneeridPageModule;
        });
      }
    }, {
      path: 'paymentsettings',
      loadChildren: function loadChildren() {
        return __webpack_require__.e(
        /*! import() | paymentsettings-paymentsettings-module */
        "paymentsettings-paymentsettings-module").then(__webpack_require__.bind(null,
        /*! ./paymentsettings/paymentsettings.module */
        "./src/app/paymentsettings/paymentsettings.module.ts")).then(function (m) {
          return m.PaymentsettingsPageModule;
        });
      }
    }, {
      path: 'systemnotif',
      loadChildren: function loadChildren() {
        return Promise.all(
        /*! import() | systemnotif-systemnotif-module */
        [__webpack_require__.e("common"), __webpack_require__.e("systemnotif-systemnotif-module")]).then(__webpack_require__.bind(null,
        /*! ./systemnotif/systemnotif.module */
        "./src/app/systemnotif/systemnotif.module.ts")).then(function (m) {
          return m.SystemnotifPageModule;
        });
      }
    }, {
      path: 'help',
      loadChildren: function loadChildren() {
        return __webpack_require__.e(
        /*! import() | help-help-module */
        "help-help-module").then(__webpack_require__.bind(null,
        /*! ./help/help.module */
        "./src/app/help/help.module.ts")).then(function (m) {
          return m.HelpPageModule;
        });
      }
    }, {
      path: 'describeitem',
      loadChildren: function loadChildren() {
        return __webpack_require__.e(
        /*! import() | describeitem-describeitem-module */
        "describeitem-describeitem-module").then(__webpack_require__.bind(null,
        /*! ./describeitem/describeitem.module */
        "./src/app/describeitem/describeitem.module.ts")).then(function (m) {
          return m.DescribeitemPageModule;
        });
      }
    }, {
      path: 'categories',
      loadChildren: function loadChildren() {
        return __webpack_require__.e(
        /*! import() | categories-categories-module */
        "categories-categories-module").then(__webpack_require__.bind(null,
        /*! ./categories/categories.module */
        "./src/app/categories/categories.module.ts")).then(function (m) {
          return m.CategoriesPageModule;
        });
      }
    }, {
      path: 'setprice',
      loadChildren: function loadChildren() {
        return __webpack_require__.e(
        /*! import() | setprice-setprice-module */
        "setprice-setprice-module").then(__webpack_require__.bind(null,
        /*! ./setprice/setprice.module */
        "./src/app/setprice/setprice.module.ts")).then(function (m) {
          return m.SetpricePageModule;
        });
      }
    }, {
      path: 'selectlocation',
      loadChildren: function loadChildren() {
        return __webpack_require__.e(
        /*! import() | selectlocation-selectlocation-module */
        "selectlocation-selectlocation-module").then(__webpack_require__.bind(null,
        /*! ./selectlocation/selectlocation.module */
        "./src/app/selectlocation/selectlocation.module.ts")).then(function (m) {
          return m.SelectlocationPageModule;
        });
      }
    }, {
      path: 'shareitem',
      loadChildren: function loadChildren() {
        return Promise.all(
        /*! import() | shareitem-shareitem-module */
        [__webpack_require__.e("common"), __webpack_require__.e("shareitem-shareitem-module")]).then(__webpack_require__.bind(null,
        /*! ./shareitem/shareitem.module */
        "./src/app/shareitem/shareitem.module.ts")).then(function (m) {
          return m.ShareitemPageModule;
        });
      }
    }, {
      path: 'itemposted',
      loadChildren: function loadChildren() {
        return Promise.all(
        /*! import() | itemposted-itemposted-module */
        [__webpack_require__.e("common"), __webpack_require__.e("itemposted-itemposted-module")]).then(__webpack_require__.bind(null,
        /*! ./itemposted/itemposted.module */
        "./src/app/itemposted/itemposted.module.ts")).then(function (m) {
          return m.ItempostedPageModule;
        });
      }
    }, {
      path: 'invite',
      loadChildren: function loadChildren() {
        return __webpack_require__.e(
        /*! import() | invite-invite-module */
        "invite-invite-module").then(__webpack_require__.bind(null,
        /*! ./invite/invite.module */
        "./src/app/invite/invite.module.ts")).then(function (m) {
          return m.InvitePageModule;
        });
      }
    }, {
      path: 'viewprofile',
      loadChildren: function loadChildren() {
        return Promise.all(
        /*! import() | viewprofile-viewprofile-module */
        [__webpack_require__.e("common"), __webpack_require__.e("viewprofile-viewprofile-module")]).then(__webpack_require__.bind(null,
        /*! ./viewprofile/viewprofile.module */
        "./src/app/viewprofile/viewprofile.module.ts")).then(function (m) {
          return m.ViewprofilePageModule;
        });
      }
    }, {
      path: 'selectcategories',
      loadChildren: function loadChildren() {
        return Promise.resolve().then(__webpack_require__.bind(null,
        /*! ./selectcategories/selectcategories.module */
        "./src/app/selectcategories/selectcategories.module.ts")).then(function (m) {
          return m.SelectcategoriesPageModule;
        });
      }
    }, {
      path: 'productview',
      loadChildren: function loadChildren() {
        return Promise.all(
        /*! import() | productview-productview-module */
        [__webpack_require__.e("common"), __webpack_require__.e("productview-productview-module")]).then(__webpack_require__.bind(null,
        /*! ./productview/productview.module */
        "./src/app/productview/productview.module.ts")).then(function (m) {
          return m.ProductviewPageModule;
        });
      }
    }, {
      path: 'report',
      loadChildren: function loadChildren() {
        return Promise.resolve().then(__webpack_require__.bind(null,
        /*! ./report/report.module */
        "./src/app/report/report.module.ts")).then(function (m) {
          return m.ReportPageModule;
        });
      }
    }, {
      path: 'viewitemsellerprofile',
      loadChildren: function loadChildren() {
        return Promise.all(
        /*! import() | viewitemsellerprofile-viewitemsellerprofile-module */
        [__webpack_require__.e("common"), __webpack_require__.e("viewitemsellerprofile-viewitemsellerprofile-module")]).then(__webpack_require__.bind(null,
        /*! ./viewitemsellerprofile/viewitemsellerprofile.module */
        "./src/app/viewitemsellerprofile/viewitemsellerprofile.module.ts")).then(function (m) {
          return m.ViewitemsellerprofilePageModule;
        });
      }
    }, {
      path: 'accountsettings',
      loadChildren: function loadChildren() {
        return __webpack_require__.e(
        /*! import() | accountsettings-accountsettings-module */
        "accountsettings-accountsettings-module").then(__webpack_require__.bind(null,
        /*! ./accountsettings/accountsettings.module */
        "./src/app/accountsettings/accountsettings.module.ts")).then(function (m) {
          return m.AccountsettingsPageModule;
        });
      }
    }, {
      path: 'offers',
      loadChildren: function loadChildren() {
        return Promise.all(
        /*! import() | offers-offers-module */
        [__webpack_require__.e("common"), __webpack_require__.e("offers-offers-module")]).then(__webpack_require__.bind(null,
        /*! ./offers/offers.module */
        "./src/app/offers/offers.module.ts")).then(function (m) {
          return m.OffersPageModule;
        });
      }
    }, {
      path: 'sellfaster',
      loadChildren: function loadChildren() {
        return Promise.all(
        /*! import() | sellfaster-sellfaster-module */
        [__webpack_require__.e("common"), __webpack_require__.e("sellfaster-sellfaster-module")]).then(__webpack_require__.bind(null,
        /*! ./sellfaster/sellfaster.module */
        "./src/app/sellfaster/sellfaster.module.ts")).then(function (m) {
          return m.SellfasterPageModule;
        });
      }
    }, {
      path: 'sellingproductview',
      loadChildren: function loadChildren() {
        return __webpack_require__.e(
        /*! import() | sellingproductview-sellingproductview-module */
        "sellingproductview-sellingproductview-module").then(__webpack_require__.bind(null,
        /*! ./sellingproductview/sellingproductview.module */
        "./src/app/sellingproductview/sellingproductview.module.ts")).then(function (m) {
          return m.SellingproductviewPageModule;
        });
      }
    }, {
      path: 'edititem',
      loadChildren: function loadChildren() {
        return Promise.all(
        /*! import() | edititem-edititem-module */
        [__webpack_require__.e("common"), __webpack_require__.e("edititem-edititem-module")]).then(__webpack_require__.bind(null,
        /*! ./edititem/edititem.module */
        "./src/app/edititem/edititem.module.ts")).then(function (m) {
          return m.EdititemPageModule;
        });
      }
    }, {
      path: 'selectcategoriessub',
      loadChildren: function loadChildren() {
        return Promise.resolve().then(__webpack_require__.bind(null,
        /*! ./selectcategoriessub/selectcategoriessub.module */
        "./src/app/selectcategoriessub/selectcategoriessub.module.ts")).then(function (m) {
          return m.SelectcategoriessubPageModule;
        });
      }
    }, {
      path: 'categoriessub',
      loadChildren: function loadChildren() {
        return Promise.resolve().then(__webpack_require__.bind(null,
        /*! ./categoriessub/categoriessub.module */
        "./src/app/categoriessub/categoriessub.module.ts")).then(function (m) {
          return m.CategoriessubPageModule;
        });
      }
    }, {
      path: 'location',
      loadChildren: function loadChildren() {
        return __webpack_require__.e(
        /*! import() | location-location-module */
        "location-location-module").then(__webpack_require__.bind(null,
        /*! ./location/location.module */
        "./src/app/location/location.module.ts")).then(function (m) {
          return m.LocationPageModule;
        });
      }
    }, {
      path: 'editlocation',
      loadChildren: function loadChildren() {
        return __webpack_require__.e(
        /*! import() | editlocation-editlocation-module */
        "editlocation-editlocation-module").then(__webpack_require__.bind(null,
        /*! ./editlocation/editlocation.module */
        "./src/app/editlocation/editlocation.module.ts")).then(function (m) {
          return m.EditlocationPageModule;
        });
      }
    }, {
      path: 'archieve',
      loadChildren: function loadChildren() {
        return Promise.all(
        /*! import() | archieve-archieve-module */
        [__webpack_require__.e("common"), __webpack_require__.e("archieve-archieve-module")]).then(__webpack_require__.bind(null,
        /*! ./archieve/archieve.module */
        "./src/app/archieve/archieve.module.ts")).then(function (m) {
          return m.ArchievePageModule;
        });
      }
    }, {
      path: 'sellersifollow',
      loadChildren: function loadChildren() {
        return Promise.all(
        /*! import() | sellersifollow-sellersifollow-module */
        [__webpack_require__.e("common"), __webpack_require__.e("sellersifollow-sellersifollow-module")]).then(__webpack_require__.bind(null,
        /*! ./sellersifollow/sellersifollow.module */
        "./src/app/sellersifollow/sellersifollow.module.ts")).then(function (m) {
          return m.SellersifollowPageModule;
        });
      }
    }, {
      path: 'promoteplus',
      loadChildren: function loadChildren() {
        return __webpack_require__.e(
        /*! import() | promoteplus-promoteplus-module */
        "promoteplus-promoteplus-module").then(__webpack_require__.bind(null,
        /*! ./promoteplus/promoteplus.module */
        "./src/app/promoteplus/promoteplus.module.ts")).then(function (m) {
          return m.PromoteplusPageModule;
        });
      }
    }, {
      path: 'userpolicy',
      loadChildren: function loadChildren() {
        return Promise.resolve().then(__webpack_require__.bind(null,
        /*! ./userpolicy/userpolicy.module */
        "./src/app/userpolicy/userpolicy.module.ts")).then(function (m) {
          return m.UserpolicyPageModule;
        });
      }
    }, {
      path: 'privacypolicy',
      loadChildren: function loadChildren() {
        return Promise.resolve().then(__webpack_require__.bind(null,
        /*! ./privacypolicy/privacypolicy.module */
        "./src/app/privacypolicy/privacypolicy.module.ts")).then(function (m) {
          return m.PrivacypolicyPageModule;
        });
      }
    }, {
      path: 'communityforum',
      loadChildren: function loadChildren() {
        return __webpack_require__.e(
        /*! import() | communityforum-communityforum-module */
        "communityforum-communityforum-module").then(__webpack_require__.bind(null,
        /*! ./communityforum/communityforum.module */
        "./src/app/communityforum/communityforum.module.ts")).then(function (m) {
          return m.CommunityforumPageModule;
        });
      }
    }, {
      path: 'suspendmodal',
      loadChildren: function loadChildren() {
        return Promise.resolve().then(__webpack_require__.bind(null,
        /*! ./suspendmodal/suspendmodal.module */
        "./src/app/suspendmodal/suspendmodal.module.ts")).then(function (m) {
          return m.SuspendmodalPageModule;
        });
      }
    }, {
      path: 'applepay',
      loadChildren: function loadChildren() {
        return __webpack_require__.e(
        /*! import() | applepay-applepay-module */
        "applepay-applepay-module").then(__webpack_require__.bind(null,
        /*! ./applepay/applepay.module */
        "./src/app/applepay/applepay.module.ts")).then(function (m) {
          return m.ApplepayPageModule;
        });
      }
    }, {
      path: 'googlepay',
      loadChildren: function loadChildren() {
        return __webpack_require__.e(
        /*! import() | googlepay-googlepay-module */
        "googlepay-googlepay-module").then(__webpack_require__.bind(null,
        /*! ./googlepay/googlepay.module */
        "./src/app/googlepay/googlepay.module.ts")).then(function (m) {
          return m.GooglepayPageModule;
        });
      }
    }, {
      path: 'logoutscreen',
      loadChildren: function loadChildren() {
        return __webpack_require__.e(
        /*! import() | logoutscreen-logoutscreen-module */
        "logoutscreen-logoutscreen-module").then(__webpack_require__.bind(null,
        /*! ./logoutscreen/logoutscreen.module */
        "./src/app/logoutscreen/logoutscreen.module.ts")).then(function (m) {
          return m.LogoutscreenPageModule;
        });
      }
    }, {
      path: 'webprofile',
      loadChildren: function loadChildren() {
        return __webpack_require__.e(
        /*! import() | webprofile-webprofile-module */
        "webprofile-webprofile-module").then(__webpack_require__.bind(null,
        /*! ./webprofile/webprofile.module */
        "./src/app/webprofile/webprofile.module.ts")).then(function (m) {
          return m.WebprofilePageModule;
        });
      }
    }, {
      path: 'webproduct',
      loadChildren: function loadChildren() {
        return __webpack_require__.e(
        /*! import() | webproduct-webproduct-module */
        "webproduct-webproduct-module").then(__webpack_require__.bind(null,
        /*! ./webproduct/webproduct.module */
        "./src/app/webproduct/webproduct.module.ts")).then(function (m) {
          return m.WebproductPageModule;
        });
      }
    }, {
      path: 'rate',
      loadChildren: function loadChildren() {
        return Promise.resolve().then(__webpack_require__.bind(null,
        /*! ./rate/rate.module */
        "./src/app/rate/rate.module.ts")).then(function (m) {
          return m.RatePageModule;
        });
      }
    }, {
      path: 'purchaseproduct',
      loadChildren: function loadChildren() {
        return Promise.resolve().then(__webpack_require__.bind(null,
        /*! ./purchaseproduct/purchaseproduct.module */
        "./src/app/purchaseproduct/purchaseproduct.module.ts")).then(function (m) {
          return m.PurchaseproductPageModule;
        });
      }
    }, {
      path: 'subscribe',
      loadChildren: function loadChildren() {
        return __webpack_require__.e(
        /*! import() | subscribe-subscribe-module */
        "subscribe-subscribe-module").then(__webpack_require__.bind(null,
        /*! ./subscribe/subscribe.module */
        "./src/app/subscribe/subscribe.module.ts")).then(function (m) {
          return m.SubscribePageModule;
        });
      }
    }];

    var AppRoutingModule = function AppRoutingModule() {
      _classCallCheck(this, AppRoutingModule);
    };

    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes, {
        preloadingStrategy: _angular_router__WEBPACK_IMPORTED_MODULE_2__["PreloadAllModules"]
      })],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], AppRoutingModule);
    /***/
  },

  /***/
  "./src/app/app.component.scss":
  /*!************************************!*\
    !*** ./src/app/app.component.scss ***!
    \************************************/

  /*! exports provided: default */

  /***/
  function srcAppAppComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJ9 */";
    /***/
  },

  /***/
  "./src/app/app.component.ts":
  /*!**********************************!*\
    !*** ./src/app/app.component.ts ***!
    \**********************************/

  /*! exports provided: AppComponent */

  /***/
  function srcAppAppComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppComponent", function () {
      return AppComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic-native/splash-screen/ngx */
    "./node_modules/@ionic-native/splash-screen/ngx/index.js");
    /* harmony import */


    var _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic-native/status-bar/ngx */
    "./node_modules/@ionic-native/status-bar/ngx/index.js");

    var AppComponent = /*#__PURE__*/function () {
      function AppComponent(platform, splashScreen, statusBar) {
        _classCallCheck(this, AppComponent);

        this.platform = platform;
        this.splashScreen = splashScreen;
        this.statusBar = statusBar;
        this.initializeApp();
      }

      _createClass(AppComponent, [{
        key: "initializeApp",
        value: function initializeApp() {
          var _this2 = this;

          this.platform.ready().then(function () {
            _this2.splashScreen.hide();

            _this2.platform.backButton.subscribeWithPriority(9999, function () {
              document.addEventListener('backbutton', function (event) {
                event.preventDefault();
                event.stopPropagation();
              }, false);
            }); //this.statusBar.styleDefault();

          });
        }
      }]);

      return AppComponent;
    }();

    AppComponent.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"]
      }, {
        type: _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_3__["SplashScreen"]
      }, {
        type: _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_4__["StatusBar"]
      }];
    };

    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-root',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./app.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./app.component.scss */
      "./src/app/app.component.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"], _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_3__["SplashScreen"], _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_4__["StatusBar"]])], AppComponent);
    /***/
  },

  /***/
  "./src/app/app.module.ts":
  /*!*******************************!*\
    !*** ./src/app/app.module.ts ***!
    \*******************************/

  /*! exports provided: AppModule */

  /***/
  function srcAppAppModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppModule", function () {
      return AppModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/platform-browser */
    "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic-native/splash-screen/ngx */
    "./node_modules/@ionic-native/splash-screen/ngx/index.js");
    /* harmony import */


    var _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @ionic-native/status-bar/ngx */
    "./node_modules/@ionic-native/status-bar/ngx/index.js");
    /* harmony import */


    var _app_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./app.component */
    "./src/app/app.component.ts");
    /* harmony import */


    var _app_routing_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ./app-routing.module */
    "./src/app/app-routing.module.ts");
    /* harmony import */


    var _ionic_native_video_player_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! @ionic-native/video-player/ngx */
    "./node_modules/@ionic-native/video-player/ngx/index.js");
    /* harmony import */


    var _ionic_native_youtube_video_player_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! @ionic-native/youtube-video-player/ngx */
    "./node_modules/@ionic-native/youtube-video-player/ngx/index.js");
    /* harmony import */


    var _giftbronze_giftbronze_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! ./giftbronze/giftbronze.component */
    "./src/app/giftbronze/giftbronze.component.ts");
    /* harmony import */


    var _giftgold_giftgold_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! ./giftgold/giftgold.component */
    "./src/app/giftgold/giftgold.component.ts");
    /* harmony import */


    var _giftsilver_giftsilver_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! ./giftsilver/giftsilver.component */
    "./src/app/giftsilver/giftsilver.component.ts");
    /* harmony import */


    var _giftpopular_giftpopular_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
    /*! ./giftpopular/giftpopular.component */
    "./src/app/giftpopular/giftpopular.component.ts");
    /* harmony import */


    var _sendgiftbronzemodal_sendgiftbronzemodal_module__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(
    /*! ./sendgiftbronzemodal/sendgiftbronzemodal.module */
    "./src/app/sendgiftbronzemodal/sendgiftbronzemodal.module.ts");
    /* harmony import */


    var _sendgiftsilvermodal_sendgiftsilvermodal_module__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(
    /*! ./sendgiftsilvermodal/sendgiftsilvermodal.module */
    "./src/app/sendgiftsilvermodal/sendgiftsilvermodal.module.ts");
    /* harmony import */


    var _sendgiftgoldmodal_sendgiftgoldmodal_module__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(
    /*! ./sendgiftgoldmodal/sendgiftgoldmodal.module */
    "./src/app/sendgiftgoldmodal/sendgiftgoldmodal.module.ts");
    /* harmony import */


    var _sendgiftpopularmodal_sendgiftpopularmodal_module__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(
    /*! ./sendgiftpopularmodal/sendgiftpopularmodal.module */
    "./src/app/sendgiftpopularmodal/sendgiftpopularmodal.module.ts");
    /* harmony import */


    var _historypreview_historypreview_module__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(
    /*! ./historypreview/historypreview.module */
    "./src/app/historypreview/historypreview.module.ts");
    /* harmony import */


    var _historygiftpreview_historygiftpreview_module__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(
    /*! ./historygiftpreview/historygiftpreview.module */
    "./src/app/historygiftpreview/historygiftpreview.module.ts");
    /* harmony import */


    var _allgifts_allgifts_module__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(
    /*! ./allgifts/allgifts.module */
    "./src/app/allgifts/allgifts.module.ts");
    /* harmony import */


    var _liveprofile_liveprofile_module__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(
    /*! ./liveprofile/liveprofile.module */
    "./src/app/liveprofile/liveprofile.module.ts");
    /* harmony import */


    var _rate_rate_module__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(
    /*! ./rate/rate.module */
    "./src/app/rate/rate.module.ts");
    /* harmony import */


    var _reportuser_reportuser_module__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(
    /*! ./reportuser/reportuser.module */
    "./src/app/reportuser/reportuser.module.ts");
    /* harmony import */


    var _vippackage_vippackage_module__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(
    /*! ./vippackage/vippackage.module */
    "./src/app/vippackage/vippackage.module.ts");
    /* harmony import */


    var _register_register_module__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(
    /*! ./register/register.module */
    "./src/app/register/register.module.ts");
    /* harmony import */


    var _purchasecoins_purchasecoins_module__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(
    /*! ./purchasecoins/purchasecoins.module */
    "./src/app/purchasecoins/purchasecoins.module.ts");
    /* harmony import */


    var _selectcategories_selectcategories_module__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(
    /*! ./selectcategories/selectcategories.module */
    "./src/app/selectcategories/selectcategories.module.ts");
    /* harmony import */


    var _selectcategoriessub_selectcategoriessub_module__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(
    /*! ./selectcategoriessub/selectcategoriessub.module */
    "./src/app/selectcategoriessub/selectcategoriessub.module.ts");
    /* harmony import */


    var _categoriessub_categoriessub_module__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(
    /*! ./categoriessub/categoriessub.module */
    "./src/app/categoriessub/categoriessub.module.ts");
    /* harmony import */


    var _report_report_module__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(
    /*! ./report/report.module */
    "./src/app/report/report.module.ts");
    /* harmony import */


    var _userpolicy_userpolicy_module__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(
    /*! ./userpolicy/userpolicy.module */
    "./src/app/userpolicy/userpolicy.module.ts");
    /* harmony import */


    var _privacypolicy_privacypolicy_module__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(
    /*! ./privacypolicy/privacypolicy.module */
    "./src/app/privacypolicy/privacypolicy.module.ts");
    /* harmony import */


    var _suspendmodal_suspendmodal_module__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(
    /*! ./suspendmodal/suspendmodal.module */
    "./src/app/suspendmodal/suspendmodal.module.ts");
    /* harmony import */


    var _purchaseproduct_purchaseproduct_module__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(
    /*! ./purchaseproduct/purchaseproduct.module */
    "./src/app/purchaseproduct/purchaseproduct.module.ts");
    /* harmony import */


    var _payreqconfirmation_payreqconfirmation_module__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(
    /*! ./payreqconfirmation/payreqconfirmation.module */
    "./src/app/payreqconfirmation/payreqconfirmation.module.ts");
    /* harmony import */


    var firebaseui_angular__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(
    /*! firebaseui-angular */
    "./node_modules/firebaseui-angular/fesm2015/firebaseui-angular.js");
    /* harmony import */


    var _ionic_native_local_notifications_ngx__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(
    /*! @ionic-native/local-notifications/ngx */
    "./node_modules/@ionic-native/local-notifications/ngx/index.js");
    /* harmony import */


    var _ionic_native_vibration_ngx__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(
    /*! @ionic-native/vibration/ngx */
    "./node_modules/@ionic-native/vibration/ngx/index.js");
    /* harmony import */


    var _angular_fire__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(
    /*! @angular/fire */
    "./node_modules/@angular/fire/es2015/index.js");
    /* harmony import */


    var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_41__ = __webpack_require__(
    /*! @angular/fire/auth */
    "./node_modules/@angular/fire/auth/es2015/index.js");
    /* harmony import */


    var _environments_environment__WEBPACK_IMPORTED_MODULE_42__ = __webpack_require__(
    /*! ../environments/environment */
    "./src/environments/environment.ts");
    /* harmony import */


    var _providers_credential_provider__WEBPACK_IMPORTED_MODULE_43__ = __webpack_require__(
    /*! ../providers/credential-provider */
    "./src/providers/credential-provider.ts");
    /* harmony import */


    var _providers_chat_messages_provider__WEBPACK_IMPORTED_MODULE_44__ = __webpack_require__(
    /*! ../providers/chat-messages.provider */
    "./src/providers/chat-messages.provider.ts");
    /* harmony import */


    var _providers_sms_provider__WEBPACK_IMPORTED_MODULE_45__ = __webpack_require__(
    /*! ../providers/sms-provider */
    "./src/providers/sms-provider.ts");
    /* harmony import */


    var _angular_http__WEBPACK_IMPORTED_MODULE_46__ = __webpack_require__(
    /*! @angular/http */
    "./node_modules/@angular/http/fesm2015/http.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_47__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_48__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_49__ = __webpack_require__(
    /*! @ionic-native/android-permissions/ngx */
    "./node_modules/@ionic-native/android-permissions/ngx/index.js");
    /* harmony import */


    var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_50__ = __webpack_require__(
    /*! @ionic-native/camera/ngx */
    "./node_modules/@ionic-native/camera/ngx/index.js");
    /* harmony import */


    var _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_51__ = __webpack_require__(
    /*! @ionic-native/file-transfer/ngx */
    "./node_modules/@ionic-native/file-transfer/ngx/index.js");
    /* harmony import */


    var _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_52__ = __webpack_require__(
    /*! @ionic-native/file/ngx */
    "./node_modules/@ionic-native/file/ngx/index.js");
    /* harmony import */


    var _ionic_native_crop_ngx__WEBPACK_IMPORTED_MODULE_53__ = __webpack_require__(
    /*! @ionic-native/crop/ngx */
    "./node_modules/@ionic-native/crop/ngx/index.js");
    /* harmony import */


    var _ionic_super_tabs_angular__WEBPACK_IMPORTED_MODULE_54__ = __webpack_require__(
    /*! @ionic-super-tabs/angular */
    "./node_modules/@ionic-super-tabs/angular/fesm2015/ionic-super-tabs-angular.js");
    /* harmony import */


    var _ionic_native_app_minimize_ngx__WEBPACK_IMPORTED_MODULE_55__ = __webpack_require__(
    /*! @ionic-native/app-minimize/ngx */
    "./node_modules/@ionic-native/app-minimize/ngx/index.js");
    /* harmony import */


    var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_56__ = __webpack_require__(
    /*! @ionic-native/geolocation/ngx */
    "./node_modules/@ionic-native/geolocation/ngx/index.js");
    /* harmony import */


    var _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_57__ = __webpack_require__(
    /*! @ionic-native/native-geocoder/ngx */
    "./node_modules/@ionic-native/native-geocoder/ngx/index.js");
    /* harmony import */


    var ngx_socket_io__WEBPACK_IMPORTED_MODULE_58__ = __webpack_require__(
    /*! ngx-socket-io */
    "./node_modules/ngx-socket-io/fesm2015/ngx-socket-io.js");

    var config = {
      url: 'http://178.128.65.70:3001',
      options: {}
    }; //const config: SocketIoConfig = { url: 'http://192.168.1.9:3001', options: {} };

    var firebaseUiAuthConfig = {
      signInSuccessUrl: '/tabs',
      siteName: 'Greenthumb Trade',
      signInFlow: 'popup',
      signInOptions: [firebaseui_angular__WEBPACK_IMPORTED_MODULE_37__["firebaseui"].auth.AnonymousAuthProvider.PROVIDER_ID],
      tosUrl: '/termsconditions',
      privacyPolicyUrl: '/termsconditions',
      credentialHelper: firebaseui_angular__WEBPACK_IMPORTED_MODULE_37__["firebaseui"].auth.CredentialHelper.NONE
    };

    var AppModule = function AppModule() {
      _classCallCheck(this, AppModule);
    };

    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      declarations: [_app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"], _giftbronze_giftbronze_component__WEBPACK_IMPORTED_MODULE_11__["GiftbronzeComponent"], _giftgold_giftgold_component__WEBPACK_IMPORTED_MODULE_12__["GiftgoldComponent"], _giftsilver_giftsilver_component__WEBPACK_IMPORTED_MODULE_13__["GiftsilverComponent"], _giftpopular_giftpopular_component__WEBPACK_IMPORTED_MODULE_14__["GiftpopularComponent"]],
      entryComponents: [_giftbronze_giftbronze_component__WEBPACK_IMPORTED_MODULE_11__["GiftbronzeComponent"], _giftgold_giftgold_component__WEBPACK_IMPORTED_MODULE_12__["GiftgoldComponent"], _giftsilver_giftsilver_component__WEBPACK_IMPORTED_MODULE_13__["GiftsilverComponent"], _giftpopular_giftpopular_component__WEBPACK_IMPORTED_MODULE_14__["GiftpopularComponent"]],
      imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["BrowserModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"].forRoot(), _app_routing_module__WEBPACK_IMPORTED_MODULE_8__["AppRoutingModule"], _angular_http__WEBPACK_IMPORTED_MODULE_46__["HttpModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_47__["HttpClientModule"], _ionic_storage__WEBPACK_IMPORTED_MODULE_48__["IonicStorageModule"].forRoot(), _sendgiftbronzemodal_sendgiftbronzemodal_module__WEBPACK_IMPORTED_MODULE_15__["SendgiftbronzemodalPageModule"], _sendgiftsilvermodal_sendgiftsilvermodal_module__WEBPACK_IMPORTED_MODULE_16__["SendgiftsilvermodalPageModule"], _sendgiftgoldmodal_sendgiftgoldmodal_module__WEBPACK_IMPORTED_MODULE_17__["SendgiftgoldmodalPageModule"], _sendgiftpopularmodal_sendgiftpopularmodal_module__WEBPACK_IMPORTED_MODULE_18__["SendgiftpopularmodalPageModule"], _purchaseproduct_purchaseproduct_module__WEBPACK_IMPORTED_MODULE_35__["PurchaseproductPageModule"], _purchasecoins_purchasecoins_module__WEBPACK_IMPORTED_MODULE_27__["PurchasecoinsPageModule"], _payreqconfirmation_payreqconfirmation_module__WEBPACK_IMPORTED_MODULE_36__["PayreqconfirmationPageModule"], _historypreview_historypreview_module__WEBPACK_IMPORTED_MODULE_19__["HistorypreviewPageModule"], _historygiftpreview_historygiftpreview_module__WEBPACK_IMPORTED_MODULE_20__["HistorygiftpreviewPageModule"], _allgifts_allgifts_module__WEBPACK_IMPORTED_MODULE_21__["AllgiftsPageModule"], _register_register_module__WEBPACK_IMPORTED_MODULE_26__["RegisterPageModule"], _vippackage_vippackage_module__WEBPACK_IMPORTED_MODULE_25__["VippackagePageModule"], _liveprofile_liveprofile_module__WEBPACK_IMPORTED_MODULE_22__["LiveprofilePageModule"], _rate_rate_module__WEBPACK_IMPORTED_MODULE_23__["RatePageModule"], _reportuser_reportuser_module__WEBPACK_IMPORTED_MODULE_24__["ReportuserPageModule"], _selectcategories_selectcategories_module__WEBPACK_IMPORTED_MODULE_28__["SelectcategoriesPageModule"], _selectcategoriessub_selectcategoriessub_module__WEBPACK_IMPORTED_MODULE_29__["SelectcategoriessubPageModule"], _categoriessub_categoriessub_module__WEBPACK_IMPORTED_MODULE_30__["CategoriessubPageModule"], _report_report_module__WEBPACK_IMPORTED_MODULE_31__["ReportPageModule"], _userpolicy_userpolicy_module__WEBPACK_IMPORTED_MODULE_32__["UserpolicyPageModule"], _privacypolicy_privacypolicy_module__WEBPACK_IMPORTED_MODULE_33__["PrivacypolicyPageModule"], _suspendmodal_suspendmodal_module__WEBPACK_IMPORTED_MODULE_34__["SuspendmodalPageModule"], _ionic_super_tabs_angular__WEBPACK_IMPORTED_MODULE_54__["SuperTabsModule"].forRoot(), _angular_fire__WEBPACK_IMPORTED_MODULE_40__["AngularFireModule"].initializeApp(_environments_environment__WEBPACK_IMPORTED_MODULE_42__["environment"].firebase), _angular_fire_auth__WEBPACK_IMPORTED_MODULE_41__["AngularFireAuthModule"], firebaseui_angular__WEBPACK_IMPORTED_MODULE_37__["FirebaseUIModule"].forRoot(firebaseUiAuthConfig), ngx_socket_io__WEBPACK_IMPORTED_MODULE_58__["SocketIoModule"].forRoot(config)],
      providers: [_ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_49__["AndroidPermissions"], _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_50__["Camera"], _ionic_native_crop_ngx__WEBPACK_IMPORTED_MODULE_53__["Crop"], _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_6__["StatusBar"], _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_5__["SplashScreen"], _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_51__["FileTransfer"], _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_52__["File"], _providers_credential_provider__WEBPACK_IMPORTED_MODULE_43__["PostProvider"], _providers_chat_messages_provider__WEBPACK_IMPORTED_MODULE_44__["ChatMessages"], _providers_sms_provider__WEBPACK_IMPORTED_MODULE_45__["SmsProvider"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"], _ionic_native_video_player_ngx__WEBPACK_IMPORTED_MODULE_9__["VideoPlayer"], _ionic_native_app_minimize_ngx__WEBPACK_IMPORTED_MODULE_55__["AppMinimize"], _ionic_native_local_notifications_ngx__WEBPACK_IMPORTED_MODULE_38__["LocalNotifications"], _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_56__["Geolocation"], _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_57__["NativeGeocoder"], _ionic_native_vibration_ngx__WEBPACK_IMPORTED_MODULE_39__["Vibration"], _ionic_native_youtube_video_player_ngx__WEBPACK_IMPORTED_MODULE_10__["YoutubeVideoPlayer"], {
        provide: _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouteReuseStrategy"],
        useClass: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicRouteStrategy"]
      }],
      bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"]]
    })], AppModule);
    /***/
  },

  /***/
  "./src/app/categoriessub/categoriessub-routing.module.ts":
  /*!***************************************************************!*\
    !*** ./src/app/categoriessub/categoriessub-routing.module.ts ***!
    \***************************************************************/

  /*! exports provided: CategoriessubPageRoutingModule */

  /***/
  function srcAppCategoriessubCategoriessubRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CategoriessubPageRoutingModule", function () {
      return CategoriessubPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _categoriessub_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./categoriessub.page */
    "./src/app/categoriessub/categoriessub.page.ts");

    var routes = [{
      path: '',
      component: _categoriessub_page__WEBPACK_IMPORTED_MODULE_3__["CategoriessubPage"]
    }];

    var CategoriessubPageRoutingModule = function CategoriessubPageRoutingModule() {
      _classCallCheck(this, CategoriessubPageRoutingModule);
    };

    CategoriessubPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], CategoriessubPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/categoriessub/categoriessub.module.ts":
  /*!*******************************************************!*\
    !*** ./src/app/categoriessub/categoriessub.module.ts ***!
    \*******************************************************/

  /*! exports provided: CategoriessubPageModule */

  /***/
  function srcAppCategoriessubCategoriessubModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CategoriessubPageModule", function () {
      return CategoriessubPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _categoriessub_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./categoriessub-routing.module */
    "./src/app/categoriessub/categoriessub-routing.module.ts");
    /* harmony import */


    var _categoriessub_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./categoriessub.page */
    "./src/app/categoriessub/categoriessub.page.ts");

    var CategoriessubPageModule = function CategoriessubPageModule() {
      _classCallCheck(this, CategoriessubPageModule);
    };

    CategoriessubPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _categoriessub_routing_module__WEBPACK_IMPORTED_MODULE_5__["CategoriessubPageRoutingModule"]],
      declarations: [_categoriessub_page__WEBPACK_IMPORTED_MODULE_6__["CategoriessubPage"]]
    })], CategoriessubPageModule);
    /***/
  },

  /***/
  "./src/app/categoriessub/categoriessub.page.scss":
  /*!*******************************************************!*\
    !*** ./src/app/categoriessub/categoriessub.page.scss ***!
    \*******************************************************/

  /*! exports provided: default */

  /***/
  function srcAppCategoriessubCategoriessubPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".for_card {\n  margin: 0;\n  box-shadow: 1px 1px 3px #e2f0cb;\n  border-radius: 5px;\n}\n\n.div_card {\n  background: #e2f0cb;\n}\n\n.for_divicon {\n  zoom: 8;\n}\n\n.for_cardcontent {\n  font-size: 14px;\n  color: #679733;\n  padding: 7px;\n  text-align: center;\n  background: #e2f0cb38;\n  border-top: 1px solid #e2f0cb;\n}\n\n.grid-categories {\n  width: 50%;\n  float: left;\n  padding: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZW5kcmljay9EZXNrdG9wL1RlbG1vIFNvbHV0aW9ucy9Gb3JQcm9kdWN0aW9uL21heTEwMjAyMS9sYXN0IHNldHVwL2dyZWVudGh1bWJhcHAgY29weS9zcmMvYXBwL2NhdGVnb3JpZXNzdWIvY2F0ZWdvcmllc3N1Yi5wYWdlLnNjc3MiLCJzcmMvYXBwL2NhdGVnb3JpZXNzdWIvY2F0ZWdvcmllc3N1Yi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxTQUFBO0VBQ0EsK0JBQUE7RUFDQSxrQkFBQTtBQ0NKOztBRENBO0VBQ0ksbUJBQUE7QUNFSjs7QURBQTtFQUNJLE9BQUE7QUNHSjs7QUREQTtFQUNJLGVBQUE7RUFDQSxjQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EscUJBQUE7RUFDQSw2QkFBQTtBQ0lKOztBREZBO0VBQ0ksVUFBQTtFQUNBLFdBQUE7RUFDQSxVQUFBO0FDS0oiLCJmaWxlIjoic3JjL2FwcC9jYXRlZ29yaWVzc3ViL2NhdGVnb3JpZXNzdWIucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmZvcl9jYXJke1xuICAgIG1hcmdpbjowO1xuICAgIGJveC1zaGFkb3c6IDFweCAxcHggM3B4ICNlMmYwY2I7XG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xufVxuLmRpdl9jYXJke1xuICAgIGJhY2tncm91bmQ6ICNlMmYwY2I7XG59XG4uZm9yX2Rpdmljb257XG4gICAgem9vbTogODtcbn1cbi5mb3JfY2FyZGNvbnRlbnR7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgIGNvbG9yOiAjNjc5NzMzO1xuICAgIHBhZGRpbmc6IDdweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgYmFja2dyb3VuZDogI2UyZjBjYjM4O1xuICAgIGJvcmRlci10b3A6IDFweCBzb2xpZCAjZTJmMGNiO1xufVxuLmdyaWQtY2F0ZWdvcmllcyB7XG4gICAgd2lkdGg6IDUwJTtcbiAgICBmbG9hdDogbGVmdDtcbiAgICBwYWRkaW5nOiAwO1xufSIsIi5mb3JfY2FyZCB7XG4gIG1hcmdpbjogMDtcbiAgYm94LXNoYWRvdzogMXB4IDFweCAzcHggI2UyZjBjYjtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xufVxuXG4uZGl2X2NhcmQge1xuICBiYWNrZ3JvdW5kOiAjZTJmMGNiO1xufVxuXG4uZm9yX2Rpdmljb24ge1xuICB6b29tOiA4O1xufVxuXG4uZm9yX2NhcmRjb250ZW50IHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBjb2xvcjogIzY3OTczMztcbiAgcGFkZGluZzogN3B4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGJhY2tncm91bmQ6ICNlMmYwY2IzODtcbiAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICNlMmYwY2I7XG59XG5cbi5ncmlkLWNhdGVnb3JpZXMge1xuICB3aWR0aDogNTAlO1xuICBmbG9hdDogbGVmdDtcbiAgcGFkZGluZzogMDtcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/categoriessub/categoriessub.page.ts":
  /*!*****************************************************!*\
    !*** ./src/app/categoriessub/categoriessub.page.ts ***!
    \*****************************************************/

  /*! exports provided: CategoriessubPage */

  /***/
  function srcAppCategoriessubCategoriessubPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CategoriessubPage", function () {
      return CategoriessubPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _shared_model_category_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../shared/model/category.model */
    "./src/app/shared/model/category.model.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var _providers_credential_provider__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../providers/credential-provider */
    "./src/providers/credential-provider.ts");

    var CategoriessubPage = /*#__PURE__*/function () {
      function CategoriessubPage(navParams, storage, modalCtrl, postPvdr) {
        _classCallCheck(this, CategoriessubPage);

        this.storage = storage;
        this.modalCtrl = modalCtrl;
        this.postPvdr = postPvdr;
        this.categoryList = [];
        this.data = navParams.get('data');
        this.cat_main_id = this.data['id'];
      }

      _createClass(CategoriessubPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.plotData();
        }
      }, {
        key: "selected",
        value: function selected(x, val) {
          var data = {
            id: x
          };
          console.log("dataklaro:" + JSON.stringify(data));
          this.modalCtrl.dismiss(data, null, 'modal21');
        }
      }, {
        key: "plotData",
        value: function plotData() {
          var _this3 = this;

          this.storage.get('greenthumb_user_id').then(function (user_id) {
            _this3.login_user_id = user_id;
            var body321 = {
              action: 'getCategoriesSub',
              cat_main_id: _this3.cat_main_id
            };

            _this3.postPvdr.postData(body321, 'category.php').subscribe(function (data) {
              if (data.success) {
                var categoryList = [];

                for (var key in data.result) {
                  categoryList.push(new _shared_model_category_model__WEBPACK_IMPORTED_MODULE_2__["CategorySub"](data.result[key].category_id, data.result[key].main_category_id, data.result[key].category, data.result[key].category_photo == '' ? '' : _this3.postPvdr.myServer() + "/greenthumb/images/categories/sub/" + data.result[key].category_photo));
                  console.log("pictures of sub:" + data.result[key].category_photo);
                }

                _this3.categoryList = categoryList;
              }
            });
          });
        }
      }, {
        key: "goBack",
        value: function goBack() {
          //this.router.navigate(['describeitem']);
          //window.history.back();
          this.modalCtrl.dismiss();
        }
      }]);

      return CategoriessubPage;
    }();

    CategoriessubPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavParams"]
      }, {
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"]
      }, {
        type: _providers_credential_provider__WEBPACK_IMPORTED_MODULE_5__["PostProvider"]
      }];
    };

    CategoriessubPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-categoriessub',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./categoriessub.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/categoriessub/categoriessub.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./categoriessub.page.scss */
      "./src/app/categoriessub/categoriessub.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavParams"], _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"], _providers_credential_provider__WEBPACK_IMPORTED_MODULE_5__["PostProvider"]])], CategoriessubPage);
    /***/
  },

  /***/
  "./src/app/giftbronze/giftbronze.component.scss":
  /*!******************************************************!*\
    !*** ./src/app/giftbronze/giftbronze.component.scss ***!
    \******************************************************/

  /*! exports provided: default */

  /***/
  function srcAppGiftbronzeGiftbronzeComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2dpZnRicm9uemUvZ2lmdGJyb256ZS5jb21wb25lbnQuc2NzcyJ9 */";
    /***/
  },

  /***/
  "./src/app/giftbronze/giftbronze.component.ts":
  /*!****************************************************!*\
    !*** ./src/app/giftbronze/giftbronze.component.ts ***!
    \****************************************************/

  /*! exports provided: GiftbronzeComponent */

  /***/
  function srcAppGiftbronzeGiftbronzeComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "GiftbronzeComponent", function () {
      return GiftbronzeComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _sendgiftbronzemodal_sendgiftbronzemodal_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../sendgiftbronzemodal/sendgiftbronzemodal.page */
    "./src/app/sendgiftbronzemodal/sendgiftbronzemodal.page.ts");
    /* harmony import */


    var howler__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! howler */
    "./node_modules/howler/dist/howler.js");
    /* harmony import */


    var howler__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(howler__WEBPACK_IMPORTED_MODULE_4__);
    /* harmony import */


    var _shared_model_gifts_model__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../shared/model/gifts.model */
    "./src/app/shared/model/gifts.model.ts");
    /* harmony import */


    var _providers_credential_provider__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../../providers/credential-provider */
    "./src/providers/credential-provider.ts");

    var GiftbronzeComponent = /*#__PURE__*/function () {
      function GiftbronzeComponent(modalController, postPvdr) {
        _classCallCheck(this, GiftbronzeComponent);

        this.modalController = modalController;
        this.postPvdr = postPvdr;
        this.player = null;
        this.bronzeList = [{
          name: 'High-Five ',
          image: 'assets/gif/highfive.gif',
          imagegif: 'assets/gif/highfive.gif',
          imageaudio: 'assets/gifaudio/highfive.mp3',
          imageaudio2: '',
          price: "10"
        }, {
          name: 'Thumbs-up',
          image: 'assets/gif/thumbsup.gif',
          imagegif: 'assets/gif/thumbsup.gif',
          imageaudio: 'assets/gifaudio/thumbsup.mp3',
          imageaudio2: '',
          price: "10"
        }, {
          name: 'Chocolate Bar (With bite)',
          image: 'assets/gif/choco.gif',
          imagegif: 'assets/gif/choco.gif',
          imageaudio: 'assets/gifaudio/choco.wav',
          imageaudio2: '',
          price: "30"
        }, {
          name: 'Big Lips Kiss',
          image: 'assets/gif/lips.gif',
          imagegif: 'assets/gif/lips.gif',
          imageaudio: 'assets/gifaudio/lips.mp3',
          imageaudio2: '',
          price: "50"
        }, {
          name: 'Puppy Dog',
          image: 'assets/gif/puppy.gif',
          imagegif: 'assets/gif/puppy.gif',
          imageaudio: 'assets/gifaudio/puppy.wav',
          imageaudio2: '',
          price: "50"
        }, //   {
        //     // notyet
        //   name:'Fireworks',
        //  image:'assets/gif/thumbsup.gif',
        //  imagegif: 'assets/gif/thumbsup.gif',
        //  imageaudio: 'assets/gifaudio/lips.mp3',
        //  imageaudio2: '',
        //  price:"100"
        //   },
        {
          name: 'Panda Bear Stuff Toy',
          image: 'assets/gif/panda_gif.gif',
          imagegif: 'assets/gif/panda_gif.gif',
          imageaudio: 'assets/gifaudio/panda.wav',
          imageaudio2: '',
          price: "250"
        }, {
          name: 'Boquet of Roses',
          image: 'assets/gif/boquet.gif',
          imagegif: 'assets/gif/boquet.gif',
          imageaudio: 'assets/gifaudio/boquet.wav',
          imageaudio2: '',
          price: "500"
        }, {
          name: 'Rainbow',
          image: 'assets/gif/rainbow.gif',
          imagegif: 'assets/gif/rainbow.gif',
          imageaudio: 'assets/gifaudio/rainbow.mp3',
          imageaudio2: '',
          price: "1000"
        }];
      }

      _createClass(GiftbronzeComponent, [{
        key: "openBronze",
        value: function openBronze(bronze) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee6() {
            var modal;
            return regeneratorRuntime.wrap(function _callee6$(_context6) {
              while (1) {
                switch (_context6.prev = _context6.next) {
                  case 0:
                    _context6.next = 2;
                    return this.modalController.create({
                      component: _sendgiftbronzemodal_sendgiftbronzemodal_page__WEBPACK_IMPORTED_MODULE_3__["SendgiftbronzemodalPage"],
                      cssClass: 'sendgiftmodal',
                      componentProps: {
                        bronze: bronze
                      }
                    });

                  case 2:
                    modal = _context6.sent;

                    if (this.player != null) {
                      this.player.stop();
                      this.player.unload();
                      this.player = null;
                    }

                    this.player = new howler__WEBPACK_IMPORTED_MODULE_4__["Howl"]({
                      src: [bronze.imageaudio],
                      onend: function onend() {
                        this.player = new howler__WEBPACK_IMPORTED_MODULE_4__["Howl"]({
                          src: [bronze.imageaudio2]
                        });
                        this.player.play();
                      }
                    });
                    this.player.play();
                    howler__WEBPACK_IMPORTED_MODULE_4__["Howler"].volume(0.5);
                    _context6.next = 9;
                    return modal.present();

                  case 9:
                    return _context6.abrupt("return", _context6.sent);

                  case 10:
                  case "end":
                    return _context6.stop();
                }
              }
            }, _callee6, this);
          }));
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {
          this.plotData();
        } // ionViewDidEnter(){
        //   //this.plotData();
        // }

      }, {
        key: "plotData",
        value: function plotData() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee7() {
            var _this4 = this;

            var body;
            return regeneratorRuntime.wrap(function _callee7$(_context7) {
              while (1) {
                switch (_context7.prev = _context7.next) {
                  case 0:
                    body = {
                      action: 'showGifts',
                      type: 1
                    };
                    this.postPvdr.postData(body, 'brixy-store.php').subscribe(function (data) {
                      console.log(data);

                      if (data.success) {
                        var gifts = [];

                        for (var key in data.result) {
                          gifts.push(new _shared_model_gifts_model__WEBPACK_IMPORTED_MODULE_5__["giftModel"](data.result[key].id, data.result[key].gifts, data.result[key].image_gif, data.result[key].image_gif, data.result[key].audio, '', data.result[key].gold_bar)); //console.log(data.result[key].user_id);
                        }

                        _this4.bronzeGiftList = gifts;
                      }
                    });

                  case 2:
                  case "end":
                    return _context7.stop();
                }
              }
            }, _callee7, this);
          }));
        }
      }]);

      return GiftbronzeComponent;
    }();

    GiftbronzeComponent.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
      }, {
        type: _providers_credential_provider__WEBPACK_IMPORTED_MODULE_6__["PostProvider"]
      }];
    };

    GiftbronzeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-giftbronze',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./giftbronze.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/giftbronze/giftbronze.component.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./giftbronze.component.scss */
      "./src/app/giftbronze/giftbronze.component.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"], _providers_credential_provider__WEBPACK_IMPORTED_MODULE_6__["PostProvider"]])], GiftbronzeComponent);
    /***/
  },

  /***/
  "./src/app/giftgold/giftgold.component.scss":
  /*!**************************************************!*\
    !*** ./src/app/giftgold/giftgold.component.scss ***!
    \**************************************************/

  /*! exports provided: default */

  /***/
  function srcAppGiftgoldGiftgoldComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2dpZnRnb2xkL2dpZnRnb2xkLmNvbXBvbmVudC5zY3NzIn0= */";
    /***/
  },

  /***/
  "./src/app/giftgold/giftgold.component.ts":
  /*!************************************************!*\
    !*** ./src/app/giftgold/giftgold.component.ts ***!
    \************************************************/

  /*! exports provided: GiftgoldComponent */

  /***/
  function srcAppGiftgoldGiftgoldComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "GiftgoldComponent", function () {
      return GiftgoldComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _sendgiftgoldmodal_sendgiftgoldmodal_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../sendgiftgoldmodal/sendgiftgoldmodal.page */
    "./src/app/sendgiftgoldmodal/sendgiftgoldmodal.page.ts");
    /* harmony import */


    var howler__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! howler */
    "./node_modules/howler/dist/howler.js");
    /* harmony import */


    var howler__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(howler__WEBPACK_IMPORTED_MODULE_4__);
    /* harmony import */


    var _providers_credential_provider__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../providers/credential-provider */
    "./src/providers/credential-provider.ts");
    /* harmony import */


    var _shared_model_gifts_model__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../shared/model/gifts.model */
    "./src/app/shared/model/gifts.model.ts");

    var GiftgoldComponent = /*#__PURE__*/function () {
      function GiftgoldComponent(modalController, postPvdr) {
        _classCallCheck(this, GiftgoldComponent);

        this.modalController = modalController;
        this.postPvdr = postPvdr;
        this.player = null;
        this.goldList = [{
          name: 'Invited to dance ball',
          image: 'assets/gif/lips.gif',
          imagegif: 'assets/gif/lips.gif',
          imageaudio: 'assets/gifaudio/lips.mp3',
          price: "2700"
        }, {
          name: 'Golden Microphone',
          image: 'assets/gif/lips.gif',
          imagegif: 'assets/gif/lips.gif',
          imageaudio: 'assets/gifaudio/lips.mp3',
          price: "4999"
        }, {
          name: 'Fine Dining Dinner Date',
          image: 'assets/gif/lips.gif',
          imagegif: 'assets/gif/lips.gif',
          imageaudio: 'assets/gifaudio/lips.mp3',
          price: "5500"
        }, {
          name: 'Black Limosuine',
          image: 'assets/gif/lips.gif',
          imagegif: 'assets/gif/lips.gif',
          imageaudio: 'assets/gifaudio/lips.mp3',
          price: "14000"
        }, {
          name: 'White Castle',
          image: 'assets/gif/lips.gif',
          imagegif: 'assets/gif/lips.gif',
          imageaudio: 'assets/gifaudio/lips.mp3',
          price: "19999"
        }, {
          name: 'Private Jet',
          image: 'assets/gif/lips.gif',
          imagegif: 'assets/gif/lips.gif',
          imageaudio: 'assets/gifaudio/lips.mp3',
          price: "28000"
        }, {
          name: 'Cruiseliner Ship',
          image: 'assets/gif/lips.gif',
          imagegif: 'assets/gif/lips.gif',
          imageaudio: 'assets/gifaudio/lips.mp3',
          price: "38000"
        }, {
          name: 'Brixy Golden Butterfly',
          image: 'assets/gif/lips.gif',
          imagegif: 'assets/gif/lips.gif',
          imageaudio: 'assets/gifaudio/lips.mp3',
          price: "570000"
        }, {
          name: 'Grand Mansion',
          image: 'assets/gif/lips.gif',
          imagegif: 'assets/gif/lips.gif',
          imageaudio: 'assets/gifaudio/lips.mp3',
          price: "4999"
        }, {
          name: 'Galaxy Milky Way',
          image: 'assets/gif/lips.gif',
          imagegif: 'assets/gif/lips.gif',
          imageaudio: 'assets/gifaudio/lips.mp3',
          price: "6000"
        }, {
          name: 'Space Ship UFO',
          image: 'assets/gif/lips.gif',
          imagegif: 'assets/gif/lips.gif',
          imageaudio: 'assets/gifaudio/lips.mp3',
          price: "12000"
        }, {
          name: 'Sportscar Lamborghini',
          image: 'assets/gif/lips.gif',
          imagegif: 'assets/gif/lips.gif',
          imageaudio: 'assets/gifaudio/lips.mp3',
          price: "15000"
        }, {
          name: 'Planet Earth World Peace',
          image: 'assets/gif/lips.gif',
          imagegif: 'assets/gif/lips.gif',
          imageaudio: 'assets/gifaudio/lips.mp3',
          price: "56000"
        }, {
          name: 'Exclusive Resort Vacation',
          image: 'assets/gif/lips.gif',
          imagegif: 'assets/gif/lips.gif',
          imageaudio: 'assets/gifaudio/lips.mp3',
          price: "114000"
        }, {
          name: 'Golden Castle',
          image: 'assets/gif/lips.gif',
          imagegif: 'assets/gif/lips.gif',
          imageaudio: 'assets/gifaudio/lips.mp3',
          price: "285000"
        }];
      }

      _createClass(GiftgoldComponent, [{
        key: "openGold",
        value: function openGold(gold) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee8() {
            var modal;
            return regeneratorRuntime.wrap(function _callee8$(_context8) {
              while (1) {
                switch (_context8.prev = _context8.next) {
                  case 0:
                    _context8.next = 2;
                    return this.modalController.create({
                      component: _sendgiftgoldmodal_sendgiftgoldmodal_page__WEBPACK_IMPORTED_MODULE_3__["SendgiftgoldmodalPage"],
                      cssClass: 'sendgiftmodal',
                      componentProps: {
                        gold: gold
                      }
                    });

                  case 2:
                    modal = _context8.sent;

                    if (this.player != null) {
                      this.player.stop();
                      this.player.unload();
                      this.player = null;
                    }

                    this.player = new howler__WEBPACK_IMPORTED_MODULE_4__["Howl"]({
                      src: [gold.imageaudio]
                    });
                    this.player.play();
                    howler__WEBPACK_IMPORTED_MODULE_4__["Howler"].volume(0.5);
                    _context8.next = 9;
                    return modal.present();

                  case 9:
                    return _context8.abrupt("return", _context8.sent);

                  case 10:
                  case "end":
                    return _context8.stop();
                }
              }
            }, _callee8, this);
          }));
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {
          this.plotData();
        }
      }, {
        key: "plotData",
        value: function plotData() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee9() {
            var _this5 = this;

            var body;
            return regeneratorRuntime.wrap(function _callee9$(_context9) {
              while (1) {
                switch (_context9.prev = _context9.next) {
                  case 0:
                    body = {
                      action: 'showGifts',
                      type: 3
                    };
                    this.postPvdr.postData(body, 'brixy-store.php').subscribe(function (data) {
                      console.log(data);

                      if (data.success) {
                        var gifts = [];

                        for (var key in data.result) {
                          gifts.push(new _shared_model_gifts_model__WEBPACK_IMPORTED_MODULE_6__["giftModel"](data.result[key].id, data.result[key].gifts, data.result[key].image_gif, data.result[key].image_gif, data.result[key].audio, '', data.result[key].gold_bar)); //console.log(data.result[key].user_id);
                        }

                        _this5.goldGiftList = gifts;
                      }
                    });

                  case 2:
                  case "end":
                    return _context9.stop();
                }
              }
            }, _callee9, this);
          }));
        }
      }]);

      return GiftgoldComponent;
    }();

    GiftgoldComponent.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
      }, {
        type: _providers_credential_provider__WEBPACK_IMPORTED_MODULE_5__["PostProvider"]
      }];
    };

    GiftgoldComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-giftgold',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./giftgold.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/giftgold/giftgold.component.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./giftgold.component.scss */
      "./src/app/giftgold/giftgold.component.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"], _providers_credential_provider__WEBPACK_IMPORTED_MODULE_5__["PostProvider"]])], GiftgoldComponent);
    /***/
  },

  /***/
  "./src/app/giftpopular/giftpopular.component.scss":
  /*!********************************************************!*\
    !*** ./src/app/giftpopular/giftpopular.component.scss ***!
    \********************************************************/

  /*! exports provided: default */

  /***/
  function srcAppGiftpopularGiftpopularComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2dpZnRwb3B1bGFyL2dpZnRwb3B1bGFyLmNvbXBvbmVudC5zY3NzIn0= */";
    /***/
  },

  /***/
  "./src/app/giftpopular/giftpopular.component.ts":
  /*!******************************************************!*\
    !*** ./src/app/giftpopular/giftpopular.component.ts ***!
    \******************************************************/

  /*! exports provided: GiftpopularComponent */

  /***/
  function srcAppGiftpopularGiftpopularComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "GiftpopularComponent", function () {
      return GiftpopularComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _sendgiftpopularmodal_sendgiftpopularmodal_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../sendgiftpopularmodal/sendgiftpopularmodal.page */
    "./src/app/sendgiftpopularmodal/sendgiftpopularmodal.page.ts");
    /* harmony import */


    var howler__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! howler */
    "./node_modules/howler/dist/howler.js");
    /* harmony import */


    var howler__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(howler__WEBPACK_IMPORTED_MODULE_4__);
    /* harmony import */


    var _providers_credential_provider__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../providers/credential-provider */
    "./src/providers/credential-provider.ts");
    /* harmony import */


    var _shared_model_gifts_model__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../shared/model/gifts.model */
    "./src/app/shared/model/gifts.model.ts");

    var GiftpopularComponent = /*#__PURE__*/function () {
      function GiftpopularComponent(modalController, postPvdr) {
        _classCallCheck(this, GiftpopularComponent);

        this.modalController = modalController;
        this.postPvdr = postPvdr;
        this.player = null;
        this.popularList = [{
          name: 'Tsinelas',
          image: 'assets/gif/lips.gif',
          imagegif: 'assets/gif/lips.gif',
          imageaudio: 'assets/gifaudio/lips.mp3',
          price: "10"
        }, {
          name: 'Sampaguita',
          image: 'assets/gif/lips.gif',
          imagegif: 'assets/gif/lips.gif',
          imageaudio: 'assets/gifaudio/lips.mp3',
          price: "10"
        }, {
          name: 'Sagot Gulaman',
          image: 'assets/gif/lips.gif',
          imagegif: 'assets/gif/lips.gif',
          imageaudio: 'assets/gifaudio/lips.mp3',
          price: "20"
        }, {
          name: 'Street Foods (Kwek-kwek, Fishball, Isaw)',
          image: 'assets/gif/lips.gif',
          imagegif: 'assets/gif/lips.gif',
          imageaudio: 'assets/gifaudio/lips.mp3',
          price: "100"
        }, {
          name: 'Barong Tagalog ',
          image: 'assets/gif/lips.gif',
          imagegif: 'assets/gif/lips.gif',
          imageaudio: 'assets/gifaudio/lips.mp3',
          price: "100"
        }, {
          name: 'Kalabaw',
          image: 'assets/gif/lips.gif',
          imagegif: 'assets/gif/lips.gif',
          imageaudio: 'assets/gifaudio/lips.mp3',
          price: "150"
        }, {
          name: 'Lechon',
          image: 'assets/gif/lips.gif',
          imagegif: 'assets/gif/lips.gif',
          imageaudio: 'assets/gifaudio/lips.mp3',
          price: "200"
        }, {
          name: 'Kalesa',
          image: 'assets/gif/lips.gif',
          imagegif: 'assets/gif/lips.gif',
          imageaudio: 'assets/gifaudio/lips.mp3',
          price: "250"
        }, {
          name: 'Tricycle',
          image: 'assets/gif/lips.gif',
          imagegif: 'assets/gif/lips.gif',
          imageaudio: 'assets/gifaudio/lips.mp3',
          price: "300"
        }, {
          name: 'Jeepney',
          image: 'assets/gif/lips.gif',
          imagegif: 'assets/gif/lips.gif',
          imageaudio: 'assets/gifaudio/lips.mp3',
          price: "500"
        }];
      }

      _createClass(GiftpopularComponent, [{
        key: "openPopular",
        value: function openPopular(popular) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee10() {
            var modal;
            return regeneratorRuntime.wrap(function _callee10$(_context10) {
              while (1) {
                switch (_context10.prev = _context10.next) {
                  case 0:
                    _context10.next = 2;
                    return this.modalController.create({
                      component: _sendgiftpopularmodal_sendgiftpopularmodal_page__WEBPACK_IMPORTED_MODULE_3__["SendgiftpopularmodalPage"],
                      cssClass: 'sendgiftmodal',
                      componentProps: {
                        popular: popular
                      }
                    });

                  case 2:
                    modal = _context10.sent;

                    if (this.player != null) {
                      this.player.stop();
                      this.player.unload();
                      this.player = null;
                    }

                    this.player = new howler__WEBPACK_IMPORTED_MODULE_4__["Howl"]({
                      src: [popular.imageaudio]
                    });
                    this.player.play();
                    howler__WEBPACK_IMPORTED_MODULE_4__["Howler"].volume(0.5);
                    _context10.next = 9;
                    return modal.present();

                  case 9:
                    return _context10.abrupt("return", _context10.sent);

                  case 10:
                  case "end":
                    return _context10.stop();
                }
              }
            }, _callee10, this);
          }));
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {
          this.plotData();
        }
      }, {
        key: "plotData",
        value: function plotData() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee11() {
            var _this6 = this;

            var body;
            return regeneratorRuntime.wrap(function _callee11$(_context11) {
              while (1) {
                switch (_context11.prev = _context11.next) {
                  case 0:
                    body = {
                      action: 'showGifts',
                      type: 4
                    };
                    this.postPvdr.postData(body, 'brixy-store.php').subscribe(function (data) {
                      console.log(data);

                      if (data.success) {
                        var gifts = [];

                        for (var key in data.result) {
                          gifts.push(new _shared_model_gifts_model__WEBPACK_IMPORTED_MODULE_6__["giftModel"](data.result[key].id, data.result[key].gifts, data.result[key].image_gif, data.result[key].image_gif, data.result[key].audio, '', data.result[key].gold_bar)); //console.log(data.result[key].user_id);
                        }

                        _this6.popularGiftList = gifts;
                      }
                    });

                  case 2:
                  case "end":
                    return _context11.stop();
                }
              }
            }, _callee11, this);
          }));
        }
      }]);

      return GiftpopularComponent;
    }();

    GiftpopularComponent.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
      }, {
        type: _providers_credential_provider__WEBPACK_IMPORTED_MODULE_5__["PostProvider"]
      }];
    };

    GiftpopularComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-giftpopular',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./giftpopular.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/giftpopular/giftpopular.component.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./giftpopular.component.scss */
      "./src/app/giftpopular/giftpopular.component.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"], _providers_credential_provider__WEBPACK_IMPORTED_MODULE_5__["PostProvider"]])], GiftpopularComponent);
    /***/
  },

  /***/
  "./src/app/giftsilver/giftsilver.component.scss":
  /*!******************************************************!*\
    !*** ./src/app/giftsilver/giftsilver.component.scss ***!
    \******************************************************/

  /*! exports provided: default */

  /***/
  function srcAppGiftsilverGiftsilverComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2dpZnRzaWx2ZXIvZ2lmdHNpbHZlci5jb21wb25lbnQuc2NzcyJ9 */";
    /***/
  },

  /***/
  "./src/app/giftsilver/giftsilver.component.ts":
  /*!****************************************************!*\
    !*** ./src/app/giftsilver/giftsilver.component.ts ***!
    \****************************************************/

  /*! exports provided: GiftsilverComponent */

  /***/
  function srcAppGiftsilverGiftsilverComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "GiftsilverComponent", function () {
      return GiftsilverComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _sendgiftsilvermodal_sendgiftsilvermodal_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../sendgiftsilvermodal/sendgiftsilvermodal.page */
    "./src/app/sendgiftsilvermodal/sendgiftsilvermodal.page.ts");
    /* harmony import */


    var howler__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! howler */
    "./node_modules/howler/dist/howler.js");
    /* harmony import */


    var howler__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(howler__WEBPACK_IMPORTED_MODULE_4__);
    /* harmony import */


    var _providers_credential_provider__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../providers/credential-provider */
    "./src/providers/credential-provider.ts");
    /* harmony import */


    var _shared_model_gifts_model__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../shared/model/gifts.model */
    "./src/app/shared/model/gifts.model.ts");

    var GiftsilverComponent = /*#__PURE__*/function () {
      function GiftsilverComponent(modalController, postPvdr) {
        _classCallCheck(this, GiftsilverComponent);

        this.modalController = modalController;
        this.postPvdr = postPvdr;
        this.player = null;
        this.silverList = [//   {
        //   name:'Bottle of Perfume',
        //  image:'assets/gif/lips.gif',
        //  imagegif: 'assets/gif/lips.gif',
        //  imageaudio: 'assets/gifaudio/lips.mp3',
        //  price:"100"
        //   },
        {
          name: 'Hugs',
          image: 'assets/gif/hug.gif',
          imagegif: 'assets/gif/hug.gif',
          imageaudio: 'assets/gifaudio/hug.wav',
          price: "100"
        }, {
          name: 'Birthday Cake (With candles)',
          image: 'assets/gif/cak.gif',
          imagegif: 'assets/gif/cak.gif',
          imageaudio: 'assets/gifaudio/birthday.mp3',
          price: "199"
        } //   {
        //   name:'Cash Rain (Raining Cash)',
        //  image:'assets/gif/lips.gif',
        //  imagegif: 'assets/gif/lips.gif',
        //  imageaudio: 'assets/gifaudio/lips.mp3',
        //  price:"299"
        //   },
        //   {
        //   name:'Sunrise',
        //  image:'assets/gif/lips.gif',
        //  imagegif: 'assets/gif/lips.gif',
        //  imageaudio: 'assets/gifaudio/lips.mp3',
        //  price:"500"
        //   },
        //   {
        //   name:'Shooting Star',
        //  image:'assets/gif/lips.gif',
        //  imagegif: 'assets/gif/lips.gif',
        //  imageaudio: 'assets/gifaudio/lips.mp3',
        //  price:"500"
        //   },
        //   {
        //   name:'Gift Box (1) - Proposal Wedding Ring',
        //  image:'assets/gif/lips.gif',
        //  imagegif: 'assets/gif/lips.gif',
        //  imageaudio: 'assets/gifaudio/lips.mp3',
        //  price:"799"
        //   },
        //   {
        //   name:'Pink Volkswagon Beetle (Car)',
        //  image:'assets/gif/lips.gif',
        //  imagegif: 'assets/gif/lips.gif',
        //  imageaudio: 'assets/gifaudio/lips.mp3',
        //  price:"1000"
        //   },
        //   {
        //   name:'Sports Car (Mustang/Muscle Car)',
        //  image:'assets/gif/lips.gif',
        //  imagegif: 'assets/gif/lips.gif',
        //  imageaudio: 'assets/gifaudio/lips.mp3',
        //  price:"1999"
        //   },
        //  {
        //   name:'Brixy Butterfly Silver',
        //  image:'assets/gif/lips.gif',
        //  imagegif: 'assets/gif/lips.gif',
        //  imageaudio: 'assets/gifaudio/lips.mp3',
        //  price:"10000"
        //  }
        ];
      }

      _createClass(GiftsilverComponent, [{
        key: "openSilver",
        value: function openSilver(silver) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee12() {
            var modal;
            return regeneratorRuntime.wrap(function _callee12$(_context12) {
              while (1) {
                switch (_context12.prev = _context12.next) {
                  case 0:
                    _context12.next = 2;
                    return this.modalController.create({
                      component: _sendgiftsilvermodal_sendgiftsilvermodal_page__WEBPACK_IMPORTED_MODULE_3__["SendgiftsilvermodalPage"],
                      cssClass: 'sendgiftmodal',
                      componentProps: {
                        silver: silver
                      }
                    });

                  case 2:
                    modal = _context12.sent;

                    if (this.player != null) {
                      this.player.stop();
                      this.player.unload();
                      this.player = null;
                    }

                    this.player = new howler__WEBPACK_IMPORTED_MODULE_4__["Howl"]({
                      src: [silver.imageaudio]
                    });
                    this.player.play();
                    howler__WEBPACK_IMPORTED_MODULE_4__["Howler"].volume(0.5);
                    _context12.next = 9;
                    return modal.present();

                  case 9:
                    return _context12.abrupt("return", _context12.sent);

                  case 10:
                  case "end":
                    return _context12.stop();
                }
              }
            }, _callee12, this);
          }));
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {
          this.plotData();
        }
      }, {
        key: "plotData",
        value: function plotData() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee13() {
            var _this7 = this;

            var body;
            return regeneratorRuntime.wrap(function _callee13$(_context13) {
              while (1) {
                switch (_context13.prev = _context13.next) {
                  case 0:
                    body = {
                      action: 'showGifts',
                      type: 2
                    };
                    this.postPvdr.postData(body, 'brixy-store.php').subscribe(function (data) {
                      console.log(data);

                      if (data.success) {
                        var gifts = [];

                        for (var key in data.result) {
                          gifts.push(new _shared_model_gifts_model__WEBPACK_IMPORTED_MODULE_6__["giftModel"](data.result[key].id, data.result[key].gifts, data.result[key].image_gif, data.result[key].image_gif, data.result[key].audio, '', data.result[key].gold_bar)); //console.log(data.result[key].user_id);
                        }

                        _this7.silverGiftList = gifts;
                      }
                    });

                  case 2:
                  case "end":
                    return _context13.stop();
                }
              }
            }, _callee13, this);
          }));
        }
      }]);

      return GiftsilverComponent;
    }();

    GiftsilverComponent.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
      }, {
        type: _providers_credential_provider__WEBPACK_IMPORTED_MODULE_5__["PostProvider"]
      }];
    };

    GiftsilverComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-giftsilver',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./giftsilver.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/giftsilver/giftsilver.component.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./giftsilver.component.scss */
      "./src/app/giftsilver/giftsilver.component.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"], _providers_credential_provider__WEBPACK_IMPORTED_MODULE_5__["PostProvider"]])], GiftsilverComponent);
    /***/
  },

  /***/
  "./src/app/historygiftpreview/historygiftpreview.module.ts":
  /*!*****************************************************************!*\
    !*** ./src/app/historygiftpreview/historygiftpreview.module.ts ***!
    \*****************************************************************/

  /*! exports provided: HistorygiftpreviewPageModule */

  /***/
  function srcAppHistorygiftpreviewHistorygiftpreviewModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "HistorygiftpreviewPageModule", function () {
      return HistorygiftpreviewPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _historygiftpreview_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./historygiftpreview.page */
    "./src/app/historygiftpreview/historygiftpreview.page.ts");

    var routes = [{
      path: '',
      component: _historygiftpreview_page__WEBPACK_IMPORTED_MODULE_6__["HistorygiftpreviewPage"]
    }];

    var HistorygiftpreviewPageModule = function HistorygiftpreviewPageModule() {
      _classCallCheck(this, HistorygiftpreviewPageModule);
    };

    HistorygiftpreviewPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      declarations: [_historygiftpreview_page__WEBPACK_IMPORTED_MODULE_6__["HistorygiftpreviewPage"]]
    })], HistorygiftpreviewPageModule);
    /***/
  },

  /***/
  "./src/app/historygiftpreview/historygiftpreview.page.scss":
  /*!*****************************************************************!*\
    !*** ./src/app/historygiftpreview/historygiftpreview.page.scss ***!
    \*****************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppHistorygiftpreviewHistorygiftpreviewPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2hpc3RvcnlnaWZ0cHJldmlldy9oaXN0b3J5Z2lmdHByZXZpZXcucGFnZS5zY3NzIn0= */";
    /***/
  },

  /***/
  "./src/app/historygiftpreview/historygiftpreview.page.ts":
  /*!***************************************************************!*\
    !*** ./src/app/historygiftpreview/historygiftpreview.page.ts ***!
    \***************************************************************/

  /*! exports provided: HistorygiftpreviewPage */

  /***/
  function srcAppHistorygiftpreviewHistorygiftpreviewPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "HistorygiftpreviewPage", function () {
      return HistorygiftpreviewPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var howler__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! howler */
    "./node_modules/howler/dist/howler.js");
    /* harmony import */


    var howler__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(howler__WEBPACK_IMPORTED_MODULE_3__);

    var HistorygiftpreviewPage = /*#__PURE__*/function () {
      function HistorygiftpreviewPage(navParams, modalCtrl) {
        _classCallCheck(this, HistorygiftpreviewPage);

        this.modalCtrl = modalCtrl;
        this.player = null;
        this.gift = navParams.get('gift');
      }

      _createClass(HistorygiftpreviewPage, [{
        key: "closeModal",
        value: function closeModal() {
          this.modalCtrl.dismiss();
          howler__WEBPACK_IMPORTED_MODULE_3__["Howler"].volume(0.0);
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "ionViewWillLeave",
        value: function ionViewWillLeave() {
          howler__WEBPACK_IMPORTED_MODULE_3__["Howler"].volume(0.0);
        }
      }]);

      return HistorygiftpreviewPage;
    }();

    HistorygiftpreviewPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavParams"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
      }];
    };

    HistorygiftpreviewPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-historygiftpreview',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./historygiftpreview.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/historygiftpreview/historygiftpreview.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./historygiftpreview.page.scss */
      "./src/app/historygiftpreview/historygiftpreview.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavParams"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]])], HistorygiftpreviewPage);
    /***/
  },

  /***/
  "./src/app/historypreview/historypreview.module.ts":
  /*!*********************************************************!*\
    !*** ./src/app/historypreview/historypreview.module.ts ***!
    \*********************************************************/

  /*! exports provided: HistorypreviewPageModule */

  /***/
  function srcAppHistorypreviewHistorypreviewModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "HistorypreviewPageModule", function () {
      return HistorypreviewPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _historypreview_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./historypreview.page */
    "./src/app/historypreview/historypreview.page.ts");

    var routes = [{
      path: '',
      component: _historypreview_page__WEBPACK_IMPORTED_MODULE_6__["HistorypreviewPage"]
    }];

    var HistorypreviewPageModule = function HistorypreviewPageModule() {
      _classCallCheck(this, HistorypreviewPageModule);
    };

    HistorypreviewPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      declarations: [_historypreview_page__WEBPACK_IMPORTED_MODULE_6__["HistorypreviewPage"]]
    })], HistorypreviewPageModule);
    /***/
  },

  /***/
  "./src/app/historypreview/historypreview.page.scss":
  /*!*********************************************************!*\
    !*** ./src/app/historypreview/historypreview.page.scss ***!
    \*********************************************************/

  /*! exports provided: default */

  /***/
  function srcAppHistorypreviewHistorypreviewPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2hpc3RvcnlwcmV2aWV3L2hpc3RvcnlwcmV2aWV3LnBhZ2Uuc2NzcyJ9 */";
    /***/
  },

  /***/
  "./src/app/historypreview/historypreview.page.ts":
  /*!*******************************************************!*\
    !*** ./src/app/historypreview/historypreview.page.ts ***!
    \*******************************************************/

  /*! exports provided: HistorypreviewPage */

  /***/
  function srcAppHistorypreviewHistorypreviewPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "HistorypreviewPage", function () {
      return HistorypreviewPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");

    var HistorypreviewPage = /*#__PURE__*/function () {
      function HistorypreviewPage(navParams, modalCtrl) {
        _classCallCheck(this, HistorypreviewPage);

        this.modalCtrl = modalCtrl;
        this.transaction = navParams.get('transaction');
      }

      _createClass(HistorypreviewPage, [{
        key: "closeModal",
        value: function closeModal() {
          this.modalCtrl.dismiss();
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return HistorypreviewPage;
    }();

    HistorypreviewPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavParams"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
      }];
    };

    HistorypreviewPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-historypreview',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./historypreview.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/historypreview/historypreview.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./historypreview.page.scss */
      "./src/app/historypreview/historypreview.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavParams"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]])], HistorypreviewPage);
    /***/
  },

  /***/
  "./src/app/liveprofile/liveprofile.module.ts":
  /*!***************************************************!*\
    !*** ./src/app/liveprofile/liveprofile.module.ts ***!
    \***************************************************/

  /*! exports provided: LiveprofilePageModule */

  /***/
  function srcAppLiveprofileLiveprofileModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "LiveprofilePageModule", function () {
      return LiveprofilePageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _liveprofile_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./liveprofile.page */
    "./src/app/liveprofile/liveprofile.page.ts");

    var routes = [{
      path: '',
      component: _liveprofile_page__WEBPACK_IMPORTED_MODULE_6__["LiveprofilePage"]
    }];

    var LiveprofilePageModule = function LiveprofilePageModule() {
      _classCallCheck(this, LiveprofilePageModule);
    };

    LiveprofilePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      declarations: [_liveprofile_page__WEBPACK_IMPORTED_MODULE_6__["LiveprofilePage"]]
    })], LiveprofilePageModule);
    /***/
  },

  /***/
  "./src/app/liveprofile/liveprofile.page.scss":
  /*!***************************************************!*\
    !*** ./src/app/liveprofile/liveprofile.page.scss ***!
    \***************************************************/

  /*! exports provided: default */

  /***/
  function srcAppLiveprofileLiveprofilePageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".my-overlay-right {\n  position: fixed;\n  z-index: 20;\n  top: 2%;\n  right: 2%;\n}\n\n.my-overlay-left {\n  position: fixed;\n  z-index: 20;\n  top: 2%;\n  left: 2%;\n}\n\n#btn_custom {\n  border-radius: 6px;\n  padding-top: 0;\n  padding-bottom: 0;\n  height: 2.1em;\n  width: 10em;\n  font-size: 13px;\n  text-transform: uppercase;\n}\n\n.myHeartFollow {\n  color: #1dc1e6;\n  border: 1px solid #1dc1e6;\n  background: white;\n}\n\n.myHeartFollow span {\n  display: none;\n}\n\n.myHeartFollow:after {\n  content: \"follow\";\n}\n\n.myHeartUnfollow {\n  color: white;\n  border: 1px solid #1dc1e6;\n  background: #1dc1e6;\n}\n\n.myHeartUnfollow span {\n  display: none;\n}\n\n.myHeartUnfollow:after {\n  content: \"following\";\n}\n\n.lvlicon {\n  width: 30px !important;\n  height: auto;\n  margin: auto;\n}\n\n.lvlicon2 {\n  width: 60px !important;\n  height: auto;\n  margin: auto;\n}\n\n.lvltext {\n  text-transform: capitalize;\n  font-size: 12px;\n  font-family: arial;\n}\n\n.lvlcol2 {\n  --background: none;\n  background-image: url(\"/assets/icon/knight2.png\");\n  background-position: center center;\n  background-repeat: no-repeat;\n  background-size: contain;\n  border-right: 0;\n  border-bottom: 0;\n}\n\n.lvlnum {\n  margin: 0;\n  margin-top: 4%;\n  font-weight: bolder;\n  font-family: Verdana, Geneva, Tahoma, sans-serif;\n}\n\n.lvlname {\n  font-size: 14px;\n  text-transform: uppercase;\n  font-weight: bolder;\n  color: white;\n  background: #1dc1e6;\n  padding: 10px;\n  border-radius: 25px;\n  padding-top: 8px;\n  padding-bottom: 8px;\n  margin-top: 2%;\n}\n\n.progress-outer {\n  width: 96%;\n  margin: 10px 2%;\n  padding: 3px;\n  text-align: center;\n  background-color: #f4f4f4;\n  border: 1px solid #dcdcdc;\n  color: #fff;\n  border-radius: 20px;\n}\n\n.progress-inner {\n  min-width: 15%;\n  white-space: nowrap;\n  overflow: hidden;\n  padding: 5px;\n  border-radius: 20px;\n  background-color: #1dc1e6;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZW5kcmljay9EZXNrdG9wL1RlbG1vIFNvbHV0aW9ucy9Gb3JQcm9kdWN0aW9uL21heTEwMjAyMS9sYXN0IHNldHVwL2dyZWVudGh1bWJhcHAgY29weS9zcmMvYXBwL2xpdmVwcm9maWxlL2xpdmVwcm9maWxlLnBhZ2Uuc2NzcyIsInNyYy9hcHAvbGl2ZXByb2ZpbGUvbGl2ZXByb2ZpbGUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksZUFBQTtFQUVBLFdBQUE7RUFDQSxPQUFBO0VBQ0EsU0FBQTtBQ0FKOztBREVFO0VBQ0UsZUFBQTtFQUVBLFdBQUE7RUFDQSxPQUFBO0VBQ0EsUUFBQTtBQ0FKOztBREVBO0VBQ0Usa0JBQUE7RUFDRSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxhQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSx5QkFBQTtBQ0NKOztBRENFO0VBQ0UsY0FBQTtFQUNBLHlCQUFBO0VBQ0EsaUJBQUE7QUNFSjs7QURBRTtFQUNFLGFBQUE7QUNHSjs7QURERTtFQUNFLGlCQUFBO0FDSUo7O0FEREU7RUFDRSxZQUFBO0VBQ0EseUJBQUE7RUFDQSxtQkFBQTtBQ0lKOztBREZFO0VBQ0UsYUFBQTtBQ0tKOztBREhFO0VBQ0Usb0JBQUE7QUNNSjs7QURIRTtFQUNFLHNCQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7QUNNSjs7QURKRTtFQUNFLHNCQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7QUNPSjs7QURMRTtFQUNFLDBCQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0FDUUo7O0FESEU7RUFDRSxrQkFBQTtFQUNBLGlEQUFBO0VBQ0Esa0NBQUE7RUFDQSw0QkFBQTtFQUNBLHdCQUFBO0VBQ0EsZUFBQTtFQUFnQixnQkFBQTtBQ09wQjs7QURMRTtFQUNFLFNBQUE7RUFDQSxjQUFBO0VBQ0EsbUJBQUE7RUFDQSxnREFBQTtBQ1FKOztBRE5FO0VBQ0UsZUFBQTtFQUNFLHlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0VBQ0EsY0FBQTtBQ1NOOztBRExNO0VBQ0ksVUFBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSx5QkFBQTtFQUNBLHlCQUFBO0VBQ0EsV0FBQTtFQUNBLG1CQUFBO0FDUVY7O0FETE07RUFDSSxjQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLHlCQUFBO0FDUVYiLCJmaWxlIjoic3JjL2FwcC9saXZlcHJvZmlsZS9saXZlcHJvZmlsZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubXktb3ZlcmxheS1yaWdodCB7XG4gICAgcG9zaXRpb246IGZpeGVkO1xuICAgIC8vIHRleHQtc2hhZG93OiAzcHggM3B4IDVweCBibGFjaztcbiAgICB6LWluZGV4OiAyMDtcbiAgICB0b3A6IDIlO1xuICAgIHJpZ2h0OiAyJTtcbiAgfVxuICAubXktb3ZlcmxheS1sZWZ0IHtcbiAgICBwb3NpdGlvbjogZml4ZWQ7XG4gICAgLy8gdGV4dC1zaGFkb3c6IDNweCAzcHggNXB4IGJsYWNrO1xuICAgIHotaW5kZXg6IDIwO1xuICAgIHRvcDogMiU7XG4gICAgbGVmdDogMiU7XG4gIH1cbiNidG5fY3VzdG9te1xuICBib3JkZXItcmFkaXVzOiA2cHg7XG4gICAgcGFkZGluZy10b3A6IDA7XG4gICAgcGFkZGluZy1ib3R0b206IDA7XG4gICAgaGVpZ2h0OiAyLjFlbTtcbiAgICB3aWR0aDogMTBlbTtcbiAgICBmb250LXNpemU6IDEzcHg7XG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbn1cbiAgLm15SGVhcnRGb2xsb3d7XG4gICAgY29sb3I6ICMxZGMxZTY7XG4gICAgYm9yZGVyOiAxcHggc29saWQgIzFkYzFlNjtcbiAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgfVxuICAubXlIZWFydEZvbGxvdyBzcGFuIHtcbiAgICBkaXNwbGF5OiBub25lO1xuICB9XG4gIC5teUhlYXJ0Rm9sbG93OmFmdGVyIHtcbiAgICBjb250ZW50OiAnZm9sbG93JztcbiAgfVxuXG4gIC5teUhlYXJ0VW5mb2xsb3d7XG4gICAgY29sb3I6IHdoaXRlO1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICMxZGMxZTY7XG4gICAgYmFja2dyb3VuZDogIzFkYzFlNjtcbiAgfVxuICAubXlIZWFydFVuZm9sbG93IHNwYW4ge1xuICAgIGRpc3BsYXk6IG5vbmU7XG4gIH1cbiAgLm15SGVhcnRVbmZvbGxvdzphZnRlciB7XG4gICAgY29udGVudDogJ2ZvbGxvd2luZyc7XG4gIH1cblxuICAubHZsaWNvbntcbiAgICB3aWR0aDogMzBweCAhaW1wb3J0YW50OyBcbiAgICBoZWlnaHQ6IGF1dG87IFxuICAgIG1hcmdpbjogYXV0bztcbiAgfVxuICAubHZsaWNvbjJ7XG4gICAgd2lkdGg6IDYwcHggIWltcG9ydGFudDsgXG4gICAgaGVpZ2h0OiBhdXRvOyBcbiAgICBtYXJnaW46IGF1dG87XG4gIH1cbiAgLmx2bHRleHR7XG4gICAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG4gICAgZm9udC1zaXplOiAxMnB4O1xuICAgIGZvbnQtZmFtaWx5OiBhcmlhbDtcbiAgfVxuICAvLyAubHZsY29se1xuICAvLyAgIGJvcmRlcjogMXB4IHNvbGlkICNlZmYxZjg7XG4gIC8vIH1cbiAgLmx2bGNvbDJ7XG4gICAgLS1iYWNrZ3JvdW5kOiBub25lO1xuICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybCgnL2Fzc2V0cy9pY29uL2tuaWdodDIucG5nJyk7XG4gICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyIGNlbnRlcjtcbiAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcbiAgICBib3JkZXItcmlnaHQ6IDA7Ym9yZGVyLWJvdHRvbTogMDtcbiAgfVxuICAubHZsbnVte1xuICAgIG1hcmdpbjogMDtcbiAgICBtYXJnaW4tdG9wOiA0JTtcbiAgICBmb250LXdlaWdodDogYm9sZGVyO1xuICAgIGZvbnQtZmFtaWx5OiBWZXJkYW5hLCBHZW5ldmEsIFRhaG9tYSwgc2Fucy1zZXJpZjtcbiAgfVxuICAubHZsbmFtZXtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICAgICAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbiAgICAgIGNvbG9yOiB3aGl0ZTtcbiAgICAgIGJhY2tncm91bmQ6ICMxZGMxZTY7XG4gICAgICBwYWRkaW5nOiAxMHB4O1xuICAgICAgYm9yZGVyLXJhZGl1czogMjVweDtcbiAgICAgIHBhZGRpbmctdG9wOiA4cHg7XG4gICAgICBwYWRkaW5nLWJvdHRvbTogOHB4O1xuICAgICAgbWFyZ2luLXRvcDogMiU7XG4gIH1cbiAgXG4gIFxuICAgICAgLnByb2dyZXNzLW91dGVyIHtcbiAgICAgICAgICB3aWR0aDogOTYlO1xuICAgICAgICAgIG1hcmdpbjogMTBweCAyJTtcbiAgICAgICAgICBwYWRkaW5nOiAzcHg7XG4gICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmNGY0ZjQ7XG4gICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2RjZGNkYztcbiAgICAgICAgICBjb2xvcjogI2ZmZjtcbiAgICAgICAgICBib3JkZXItcmFkaXVzOiAyMHB4O1xuICAgICAgfVxuICBcbiAgICAgIC5wcm9ncmVzcy1pbm5lciB7XG4gICAgICAgICAgbWluLXdpZHRoOiAxNSU7XG4gICAgICAgICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgICAgICAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgICAgICAgIHBhZGRpbmc6IDVweDtcbiAgICAgICAgICBib3JkZXItcmFkaXVzOiAyMHB4O1xuICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICMxZGMxZTY7XG4gICAgICB9XG4gICAgICBcbiAgIiwiLm15LW92ZXJsYXktcmlnaHQge1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIHotaW5kZXg6IDIwO1xuICB0b3A6IDIlO1xuICByaWdodDogMiU7XG59XG5cbi5teS1vdmVybGF5LWxlZnQge1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIHotaW5kZXg6IDIwO1xuICB0b3A6IDIlO1xuICBsZWZ0OiAyJTtcbn1cblxuI2J0bl9jdXN0b20ge1xuICBib3JkZXItcmFkaXVzOiA2cHg7XG4gIHBhZGRpbmctdG9wOiAwO1xuICBwYWRkaW5nLWJvdHRvbTogMDtcbiAgaGVpZ2h0OiAyLjFlbTtcbiAgd2lkdGg6IDEwZW07XG4gIGZvbnQtc2l6ZTogMTNweDtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbn1cblxuLm15SGVhcnRGb2xsb3cge1xuICBjb2xvcjogIzFkYzFlNjtcbiAgYm9yZGVyOiAxcHggc29saWQgIzFkYzFlNjtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG59XG5cbi5teUhlYXJ0Rm9sbG93IHNwYW4ge1xuICBkaXNwbGF5OiBub25lO1xufVxuXG4ubXlIZWFydEZvbGxvdzphZnRlciB7XG4gIGNvbnRlbnQ6IFwiZm9sbG93XCI7XG59XG5cbi5teUhlYXJ0VW5mb2xsb3cge1xuICBjb2xvcjogd2hpdGU7XG4gIGJvcmRlcjogMXB4IHNvbGlkICMxZGMxZTY7XG4gIGJhY2tncm91bmQ6ICMxZGMxZTY7XG59XG5cbi5teUhlYXJ0VW5mb2xsb3cgc3BhbiB7XG4gIGRpc3BsYXk6IG5vbmU7XG59XG5cbi5teUhlYXJ0VW5mb2xsb3c6YWZ0ZXIge1xuICBjb250ZW50OiBcImZvbGxvd2luZ1wiO1xufVxuXG4ubHZsaWNvbiB7XG4gIHdpZHRoOiAzMHB4ICFpbXBvcnRhbnQ7XG4gIGhlaWdodDogYXV0bztcbiAgbWFyZ2luOiBhdXRvO1xufVxuXG4ubHZsaWNvbjIge1xuICB3aWR0aDogNjBweCAhaW1wb3J0YW50O1xuICBoZWlnaHQ6IGF1dG87XG4gIG1hcmdpbjogYXV0bztcbn1cblxuLmx2bHRleHQge1xuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBmb250LWZhbWlseTogYXJpYWw7XG59XG5cbi5sdmxjb2wyIHtcbiAgLS1iYWNrZ3JvdW5kOiBub25lO1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCIvYXNzZXRzL2ljb24va25pZ2h0Mi5wbmdcIik7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlciBjZW50ZXI7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcbiAgYm9yZGVyLXJpZ2h0OiAwO1xuICBib3JkZXItYm90dG9tOiAwO1xufVxuXG4ubHZsbnVtIHtcbiAgbWFyZ2luOiAwO1xuICBtYXJnaW4tdG9wOiA0JTtcbiAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbiAgZm9udC1mYW1pbHk6IFZlcmRhbmEsIEdlbmV2YSwgVGFob21hLCBzYW5zLXNlcmlmO1xufVxuXG4ubHZsbmFtZSB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbiAgY29sb3I6IHdoaXRlO1xuICBiYWNrZ3JvdW5kOiAjMWRjMWU2O1xuICBwYWRkaW5nOiAxMHB4O1xuICBib3JkZXItcmFkaXVzOiAyNXB4O1xuICBwYWRkaW5nLXRvcDogOHB4O1xuICBwYWRkaW5nLWJvdHRvbTogOHB4O1xuICBtYXJnaW4tdG9wOiAyJTtcbn1cblxuLnByb2dyZXNzLW91dGVyIHtcbiAgd2lkdGg6IDk2JTtcbiAgbWFyZ2luOiAxMHB4IDIlO1xuICBwYWRkaW5nOiAzcHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Y0ZjRmNDtcbiAgYm9yZGVyOiAxcHggc29saWQgI2RjZGNkYztcbiAgY29sb3I6ICNmZmY7XG4gIGJvcmRlci1yYWRpdXM6IDIwcHg7XG59XG5cbi5wcm9ncmVzcy1pbm5lciB7XG4gIG1pbi13aWR0aDogMTUlO1xuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICBwYWRkaW5nOiA1cHg7XG4gIGJvcmRlci1yYWRpdXM6IDIwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICMxZGMxZTY7XG59Il19 */";
    /***/
  },

  /***/
  "./src/app/liveprofile/liveprofile.page.ts":
  /*!*************************************************!*\
    !*** ./src/app/liveprofile/liveprofile.page.ts ***!
    \*************************************************/

  /*! exports provided: LiveprofilePage */

  /***/
  function srcAppLiveprofileLiveprofilePageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "LiveprofilePage", function () {
      return LiveprofilePage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var _providers_credential_provider__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../providers/credential-provider */
    "./src/providers/credential-provider.ts");
    /* harmony import */


    var _reportuser_reportuser_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../reportuser/reportuser.page */
    "./src/app/reportuser/reportuser.page.ts");

    var LiveprofilePage = /*#__PURE__*/function () {
      function LiveprofilePage(router, modalController, storage, postPvdr, navCtrl) {
        _classCallCheck(this, LiveprofilePage);

        this.router = router;
        this.modalController = modalController;
        this.storage = storage;
        this.postPvdr = postPvdr;
        this.navCtrl = navCtrl;
        this.followClass = "myHeartFollow";
        this.status = false;
      }

      _createClass(LiveprofilePage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          //console.log("boycott nba:"+this.liveStreamProfileId);
          this.plotData();
        }
      }, {
        key: "goBack",
        value: function goBack() {
          this.modalController.dismiss();
        }
      }, {
        key: "message",
        value: function message() {
          console.log("hello yellow");
          this.goBack();
          this.navCtrl.navigateForward(['/message/' + this.liveStreamProfileId]);
        }
      }, {
        key: "plotData",
        value: function plotData() {
          var _this8 = this;

          var body = {
            action: 'getUser_liveData',
            user_id: this.liveStreamProfileId
          };
          this.postPvdr.postData(body, 'credentials-api.php').subscribe(function (data) {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this8, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee14() {
              var key;
              return regeneratorRuntime.wrap(function _callee14$(_context14) {
                while (1) {
                  switch (_context14.prev = _context14.next) {
                    case 0:
                      if (data.success) {
                        for (key in data.result) {
                          this.user_id = data.result[key].user_id;
                          this.fname = data.result[key].fname;
                          this.lname = data.result[key].lname;
                          this.city = data.result[key].city;
                          this.country = data.result[key].country;
                          this.user_photo = data.result[key].profile_photo == '' ? '' : this.postPvdr.myServer() + "/brixy-live/images/" + data.result[key].profile_photo;
                          this.user_level = data.result[key].user_level;
                          this.user_locator_id = data.result[key].user_locator_id;
                          this.followers = data.result[key].followers;
                          this.following = data.result[key].following;
                          this.gold_bars = data.result[key].gold_bar;
                          this.user_experience = data.result[key].user_experience;
                          this.badge_name = data.result[key].badge_name;
                          this.user_coins = data.result[key].user_coins;
                          this.bracket_to = data.result[key].bracket_to;
                        }
                      }

                    case 1:
                    case "end":
                      return _context14.stop();
                  }
                }
              }, _callee14, this);
            }));
          });
          this.storage.get('user_id').then(function (user_id) {
            var body2 = {
              action: 'followerStatus',
              followed_by: user_id,
              user_id: _this8.liveStreamProfileId
            };

            _this8.postPvdr.postData(body2, 'followers.php').subscribe(function (data) {
              return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this8, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee15() {
                return regeneratorRuntime.wrap(function _callee15$(_context15) {
                  while (1) {
                    switch (_context15.prev = _context15.next) {
                      case 0:
                        if (data.success) {
                          if (data.result == "1") {
                            this.status = true;
                          }
                        }

                      case 1:
                      case "end":
                        return _context15.stop();
                    }
                  }
                }, _callee15, this);
              }));
            });
          });
        }
      }, {
        key: "reportUser",
        value: function reportUser() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee16() {
            var modal;
            return regeneratorRuntime.wrap(function _callee16$(_context16) {
              while (1) {
                switch (_context16.prev = _context16.next) {
                  case 0:
                    _context16.next = 2;
                    return this.modalController.create({
                      component: _reportuser_reportuser_page__WEBPACK_IMPORTED_MODULE_6__["ReportuserPage"],
                      cssClass: 'liveprofilemodalstyle',
                      componentProps: {
                        reportedId: this.liveStreamProfileId,
                        reportedName: this.fname + ' ' + this.lname
                      }
                    });

                  case 2:
                    modal = _context16.sent;
                    modal.onDidDismiss().then(function (data) {
                      //const user = data['data']; // Here's your selected user!
                      console.log("dismiss of liveprofilepage modal"); //this.plotFollowStatus();
                    });
                    _context16.next = 6;
                    return modal.present();

                  case 6:
                    return _context16.abrupt("return", _context16.sent);

                  case 7:
                  case "end":
                    return _context16.stop();
                }
              }
            }, _callee16, this);
          }));
        }
      }, {
        key: "follow",
        value: function follow() {
          var _this9 = this;

          this.storage.get('user_id').then(function (user_id) {
            console.log("user_id follow:" + user_id);
            var body = {
              action: 'follow',
              user_id: _this9.user_id,
              followed_by: user_id,
              status: _this9.status
            };

            _this9.postPvdr.postData(body, 'followers.php').subscribe(function (data) {
              console.log(data);

              if (data.success) {
                _this9.followClass = "myHeartFollow";
                _this9.status = !_this9.status;

                if (_this9.status) {
                  var body2 = {
                    action: 'addNotification',
                    user_id: _this9.user_id,
                    followed_by: user_id,
                    notification_message: "has followed you",
                    status: _this9.status
                  };

                  _this9.postPvdr.postData(body2, 'system_notification.php').subscribe(function (data) {});
                }
              }
            });
          });
        }
      }]);

      return LiveprofilePage;
    }();

    LiveprofilePage.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"]
      }, {
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"]
      }, {
        type: _providers_credential_provider__WEBPACK_IMPORTED_MODULE_5__["PostProvider"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"]
      }];
    };

    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)], LiveprofilePage.prototype, "liveStreamProfileId", void 0);
    LiveprofilePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-liveprofile',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./liveprofile.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/liveprofile/liveprofile.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./liveprofile.page.scss */
      "./src/app/liveprofile/liveprofile.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"], _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"], _providers_credential_provider__WEBPACK_IMPORTED_MODULE_5__["PostProvider"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"]])], LiveprofilePage);
    /***/
  },

  /***/
  "./src/app/payreqconfirmation/payreqconfirmation-routing.module.ts":
  /*!*************************************************************************!*\
    !*** ./src/app/payreqconfirmation/payreqconfirmation-routing.module.ts ***!
    \*************************************************************************/

  /*! exports provided: PayreqconfirmationPageRoutingModule */

  /***/
  function srcAppPayreqconfirmationPayreqconfirmationRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PayreqconfirmationPageRoutingModule", function () {
      return PayreqconfirmationPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _payreqconfirmation_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./payreqconfirmation.page */
    "./src/app/payreqconfirmation/payreqconfirmation.page.ts");

    var routes = [{
      path: '',
      component: _payreqconfirmation_page__WEBPACK_IMPORTED_MODULE_3__["PayreqconfirmationPage"]
    }];

    var PayreqconfirmationPageRoutingModule = function PayreqconfirmationPageRoutingModule() {
      _classCallCheck(this, PayreqconfirmationPageRoutingModule);
    };

    PayreqconfirmationPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], PayreqconfirmationPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/payreqconfirmation/payreqconfirmation.module.ts":
  /*!*****************************************************************!*\
    !*** ./src/app/payreqconfirmation/payreqconfirmation.module.ts ***!
    \*****************************************************************/

  /*! exports provided: PayreqconfirmationPageModule */

  /***/
  function srcAppPayreqconfirmationPayreqconfirmationModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PayreqconfirmationPageModule", function () {
      return PayreqconfirmationPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _payreqconfirmation_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./payreqconfirmation-routing.module */
    "./src/app/payreqconfirmation/payreqconfirmation-routing.module.ts");
    /* harmony import */


    var _payreqconfirmation_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./payreqconfirmation.page */
    "./src/app/payreqconfirmation/payreqconfirmation.page.ts");

    var PayreqconfirmationPageModule = function PayreqconfirmationPageModule() {
      _classCallCheck(this, PayreqconfirmationPageModule);
    };

    PayreqconfirmationPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _payreqconfirmation_routing_module__WEBPACK_IMPORTED_MODULE_5__["PayreqconfirmationPageRoutingModule"]],
      declarations: [_payreqconfirmation_page__WEBPACK_IMPORTED_MODULE_6__["PayreqconfirmationPage"]]
    })], PayreqconfirmationPageModule);
    /***/
  },

  /***/
  "./src/app/payreqconfirmation/payreqconfirmation.page.scss":
  /*!*****************************************************************!*\
    !*** ./src/app/payreqconfirmation/payreqconfirmation.page.scss ***!
    \*****************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPayreqconfirmationPayreqconfirmationPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BheXJlcWNvbmZpcm1hdGlvbi9wYXlyZXFjb25maXJtYXRpb24ucGFnZS5zY3NzIn0= */";
    /***/
  },

  /***/
  "./src/app/payreqconfirmation/payreqconfirmation.page.ts":
  /*!***************************************************************!*\
    !*** ./src/app/payreqconfirmation/payreqconfirmation.page.ts ***!
    \***************************************************************/

  /*! exports provided: PayreqconfirmationPage */

  /***/
  function srcAppPayreqconfirmationPayreqconfirmationPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PayreqconfirmationPage", function () {
      return PayreqconfirmationPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var _providers_credential_provider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../providers/credential-provider */
    "./src/providers/credential-provider.ts");

    var PayreqconfirmationPage = /*#__PURE__*/function () {
      function PayreqconfirmationPage(navParams, storage, modalCtrl, toastController, postPvdr) {
        _classCallCheck(this, PayreqconfirmationPage);

        this.storage = storage;
        this.modalCtrl = modalCtrl;
        this.toastController = toastController;
        this.postPvdr = postPvdr;
        this.paychart = navParams.get('paychart');
      }

      _createClass(PayreqconfirmationPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "closeModal",
        value: function closeModal() {
          this.modalCtrl.dismiss();
        }
      }, {
        key: "confirmPaymentRequest",
        value: function confirmPaymentRequest() {
          var _this10 = this;

          // console.log("paychart:"+this.paychart.id);
          // console.log("paychart:"+this.paychart.gold_bars);
          // console.log("paychart:"+this.paychart.usd_amount);
          this.storage.get("user_id").then(function (user_id) {
            var body2 = {
              action: 'requestPayment',
              user_id: user_id,
              gold_bars: _this10.paychart.gold_bars,
              usd_amount: _this10.paychart.usd_amount
            };
            console.log("body:" + JSON.stringify(body2));

            _this10.postPvdr.postData(body2, 'payment.php').subscribe(function (data) {
              if (data.success) {
                _this10.presentToast();

                _this10.modalCtrl.dismiss();
              }
            });
          });
        }
      }, {
        key: "presentToast",
        value: function presentToast() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee17() {
            var toast;
            return regeneratorRuntime.wrap(function _callee17$(_context17) {
              while (1) {
                switch (_context17.prev = _context17.next) {
                  case 0:
                    _context17.next = 2;
                    return this.toastController.create({
                      message: 'Cashout Requested Successfully.',
                      duration: 3000
                    });

                  case 2:
                    toast = _context17.sent;
                    toast.present();

                  case 4:
                  case "end":
                    return _context17.stop();
                }
              }
            }, _callee17, this);
          }));
        }
      }]);

      return PayreqconfirmationPage;
    }();

    PayreqconfirmationPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavParams"]
      }, {
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]
      }, {
        type: _providers_credential_provider__WEBPACK_IMPORTED_MODULE_4__["PostProvider"]
      }];
    };

    PayreqconfirmationPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-payreqconfirmation',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./payreqconfirmation.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/payreqconfirmation/payreqconfirmation.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./payreqconfirmation.page.scss */
      "./src/app/payreqconfirmation/payreqconfirmation.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavParams"], _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"], _providers_credential_provider__WEBPACK_IMPORTED_MODULE_4__["PostProvider"]])], PayreqconfirmationPage);
    /***/
  },

  /***/
  "./src/app/privacypolicy/privacypolicy-routing.module.ts":
  /*!***************************************************************!*\
    !*** ./src/app/privacypolicy/privacypolicy-routing.module.ts ***!
    \***************************************************************/

  /*! exports provided: PrivacypolicyPageRoutingModule */

  /***/
  function srcAppPrivacypolicyPrivacypolicyRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PrivacypolicyPageRoutingModule", function () {
      return PrivacypolicyPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _privacypolicy_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./privacypolicy.page */
    "./src/app/privacypolicy/privacypolicy.page.ts");

    var routes = [{
      path: '',
      component: _privacypolicy_page__WEBPACK_IMPORTED_MODULE_3__["PrivacypolicyPage"]
    }];

    var PrivacypolicyPageRoutingModule = function PrivacypolicyPageRoutingModule() {
      _classCallCheck(this, PrivacypolicyPageRoutingModule);
    };

    PrivacypolicyPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], PrivacypolicyPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/privacypolicy/privacypolicy.module.ts":
  /*!*******************************************************!*\
    !*** ./src/app/privacypolicy/privacypolicy.module.ts ***!
    \*******************************************************/

  /*! exports provided: PrivacypolicyPageModule */

  /***/
  function srcAppPrivacypolicyPrivacypolicyModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PrivacypolicyPageModule", function () {
      return PrivacypolicyPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _privacypolicy_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./privacypolicy-routing.module */
    "./src/app/privacypolicy/privacypolicy-routing.module.ts");
    /* harmony import */


    var _privacypolicy_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./privacypolicy.page */
    "./src/app/privacypolicy/privacypolicy.page.ts");

    var PrivacypolicyPageModule = function PrivacypolicyPageModule() {
      _classCallCheck(this, PrivacypolicyPageModule);
    };

    PrivacypolicyPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _privacypolicy_routing_module__WEBPACK_IMPORTED_MODULE_5__["PrivacypolicyPageRoutingModule"]],
      declarations: [_privacypolicy_page__WEBPACK_IMPORTED_MODULE_6__["PrivacypolicyPage"]]
    })], PrivacypolicyPageModule);
    /***/
  },

  /***/
  "./src/app/privacypolicy/privacypolicy.page.scss":
  /*!*******************************************************!*\
    !*** ./src/app/privacypolicy/privacypolicy.page.scss ***!
    \*******************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPrivacypolicyPrivacypolicyPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ByaXZhY3lwb2xpY3kvcHJpdmFjeXBvbGljeS5wYWdlLnNjc3MifQ== */";
    /***/
  },

  /***/
  "./src/app/privacypolicy/privacypolicy.page.ts":
  /*!*****************************************************!*\
    !*** ./src/app/privacypolicy/privacypolicy.page.ts ***!
    \*****************************************************/

  /*! exports provided: PrivacypolicyPage */

  /***/
  function srcAppPrivacypolicyPrivacypolicyPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PrivacypolicyPage", function () {
      return PrivacypolicyPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");

    var PrivacypolicyPage = /*#__PURE__*/function () {
      function PrivacypolicyPage(modalController) {
        _classCallCheck(this, PrivacypolicyPage);

        this.modalController = modalController;
      }

      _createClass(PrivacypolicyPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "goBack",
        value: function goBack() {
          this.modalController.dismiss();
        }
      }]);

      return PrivacypolicyPage;
    }();

    PrivacypolicyPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
      }];
    };

    PrivacypolicyPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-privacypolicy',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./privacypolicy.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/privacypolicy/privacypolicy.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./privacypolicy.page.scss */
      "./src/app/privacypolicy/privacypolicy.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]])], PrivacypolicyPage);
    /***/
  },

  /***/
  "./src/app/purchasecoins/purchasecoins-routing.module.ts":
  /*!***************************************************************!*\
    !*** ./src/app/purchasecoins/purchasecoins-routing.module.ts ***!
    \***************************************************************/

  /*! exports provided: PurchasecoinsPageRoutingModule */

  /***/
  function srcAppPurchasecoinsPurchasecoinsRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PurchasecoinsPageRoutingModule", function () {
      return PurchasecoinsPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _purchasecoins_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./purchasecoins.page */
    "./src/app/purchasecoins/purchasecoins.page.ts");

    var routes = [{
      path: '',
      component: _purchasecoins_page__WEBPACK_IMPORTED_MODULE_3__["PurchasecoinsPage"]
    }];

    var PurchasecoinsPageRoutingModule = function PurchasecoinsPageRoutingModule() {
      _classCallCheck(this, PurchasecoinsPageRoutingModule);
    };

    PurchasecoinsPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], PurchasecoinsPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/purchasecoins/purchasecoins.module.ts":
  /*!*******************************************************!*\
    !*** ./src/app/purchasecoins/purchasecoins.module.ts ***!
    \*******************************************************/

  /*! exports provided: PurchasecoinsPageModule */

  /***/
  function srcAppPurchasecoinsPurchasecoinsModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PurchasecoinsPageModule", function () {
      return PurchasecoinsPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _purchasecoins_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./purchasecoins-routing.module */
    "./src/app/purchasecoins/purchasecoins-routing.module.ts");
    /* harmony import */


    var _purchasecoins_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./purchasecoins.page */
    "./src/app/purchasecoins/purchasecoins.page.ts");

    var PurchasecoinsPageModule = function PurchasecoinsPageModule() {
      _classCallCheck(this, PurchasecoinsPageModule);
    };

    PurchasecoinsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _purchasecoins_routing_module__WEBPACK_IMPORTED_MODULE_5__["PurchasecoinsPageRoutingModule"]],
      declarations: [_purchasecoins_page__WEBPACK_IMPORTED_MODULE_6__["PurchasecoinsPage"]]
    })], PurchasecoinsPageModule);
    /***/
  },

  /***/
  "./src/app/purchasecoins/purchasecoins.page.scss":
  /*!*******************************************************!*\
    !*** ./src/app/purchasecoins/purchasecoins.page.scss ***!
    \*******************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPurchasecoinsPurchasecoinsPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".lvlicon2 {\n  width: 40px !important;\n  height: 40px;\n  margin: auto;\n  margin-bottom: -1%;\n  margin-right: 2%;\n}\n\n.lvltext {\n  text-transform: capitalize;\n  font-size: 12px;\n  font-family: arial;\n}\n\n.lvlnum {\n  margin: 0;\n  margin-top: 4%;\n  font-weight: bolder;\n  font-family: Verdana, Geneva, Tahoma, sans-serif;\n  font-size: 30px;\n}\n\n.lvlicon3 {\n  width: 20px !important;\n  height: 18px;\n  margin: auto;\n  margin-bottom: -1%;\n  margin-right: 1%;\n}\n\n.lvltext2 {\n  text-transform: capitalize;\n  font-size: 12px;\n  font-family: arial;\n}\n\n.lvlnum2 {\n  margin: 0;\n  margin-top: 4%;\n  font-weight: bolder;\n  font-family: Verdana, Geneva, Tahoma, sans-serif;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZW5kcmljay9EZXNrdG9wL1RlbG1vIFNvbHV0aW9ucy9Gb3JQcm9kdWN0aW9uL21heTEwMjAyMS9sYXN0IHNldHVwL2dyZWVudGh1bWJhcHAgY29weS9zcmMvYXBwL3B1cmNoYXNlY29pbnMvcHVyY2hhc2Vjb2lucy5wYWdlLnNjc3MiLCJzcmMvYXBwL3B1cmNoYXNlY29pbnMvcHVyY2hhc2Vjb2lucy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxzQkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtBQ0NKOztBRENFO0VBQ0UsMEJBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7QUNFSjs7QURBRTtFQUNFLFNBQUE7RUFDQSxjQUFBO0VBQ0EsbUJBQUE7RUFDQSxnREFBQTtFQUNBLGVBQUE7QUNHSjs7QURBRTtFQUNFLHNCQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0FDR0o7O0FEREU7RUFDRSwwQkFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtBQ0lKOztBREZFO0VBQ0UsU0FBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtFQUNBLGdEQUFBO0FDS0oiLCJmaWxlIjoic3JjL2FwcC9wdXJjaGFzZWNvaW5zL3B1cmNoYXNlY29pbnMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmx2bGljb24ye1xuICAgIHdpZHRoOiA0MHB4ICFpbXBvcnRhbnQ7XG4gICAgaGVpZ2h0OiA0MHB4O1xuICAgIG1hcmdpbjogYXV0bztcbiAgICBtYXJnaW4tYm90dG9tOiAtMSU7XG4gICAgbWFyZ2luLXJpZ2h0OiAyJTtcbiAgfVxuICAubHZsdGV4dHtcbiAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbiAgICBmb250LXNpemU6IDEycHg7XG4gICAgZm9udC1mYW1pbHk6IGFyaWFsO1xuICB9XG4gIC5sdmxudW17XG4gICAgbWFyZ2luOiAwO1xuICAgIG1hcmdpbi10b3A6IDQlO1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkZXI7XG4gICAgZm9udC1mYW1pbHk6IFZlcmRhbmEsIEdlbmV2YSwgVGFob21hLCBzYW5zLXNlcmlmO1xuICAgIGZvbnQtc2l6ZTogMzBweDtcbiAgfVxuXG4gIC5sdmxpY29uM3tcbiAgICB3aWR0aDogMjBweCAhaW1wb3J0YW50O1xuICAgIGhlaWdodDogMThweDtcbiAgICBtYXJnaW46IGF1dG87XG4gICAgbWFyZ2luLWJvdHRvbTogLTElO1xuICAgIG1hcmdpbi1yaWdodDogMSU7XG4gIH1cbiAgLmx2bHRleHQye1xuICAgIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICBmb250LWZhbWlseTogYXJpYWw7XG4gIH1cbiAgLmx2bG51bTJ7XG4gICAgbWFyZ2luOiAwO1xuICAgIG1hcmdpbi10b3A6IDQlO1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkZXI7XG4gICAgZm9udC1mYW1pbHk6IFZlcmRhbmEsIEdlbmV2YSwgVGFob21hLCBzYW5zLXNlcmlmO1xuICB9IiwiLmx2bGljb24yIHtcbiAgd2lkdGg6IDQwcHggIWltcG9ydGFudDtcbiAgaGVpZ2h0OiA0MHB4O1xuICBtYXJnaW46IGF1dG87XG4gIG1hcmdpbi1ib3R0b206IC0xJTtcbiAgbWFyZ2luLXJpZ2h0OiAyJTtcbn1cblxuLmx2bHRleHQge1xuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBmb250LWZhbWlseTogYXJpYWw7XG59XG5cbi5sdmxudW0ge1xuICBtYXJnaW46IDA7XG4gIG1hcmdpbi10b3A6IDQlO1xuICBmb250LXdlaWdodDogYm9sZGVyO1xuICBmb250LWZhbWlseTogVmVyZGFuYSwgR2VuZXZhLCBUYWhvbWEsIHNhbnMtc2VyaWY7XG4gIGZvbnQtc2l6ZTogMzBweDtcbn1cblxuLmx2bGljb24zIHtcbiAgd2lkdGg6IDIwcHggIWltcG9ydGFudDtcbiAgaGVpZ2h0OiAxOHB4O1xuICBtYXJnaW46IGF1dG87XG4gIG1hcmdpbi1ib3R0b206IC0xJTtcbiAgbWFyZ2luLXJpZ2h0OiAxJTtcbn1cblxuLmx2bHRleHQyIHtcbiAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgZm9udC1mYW1pbHk6IGFyaWFsO1xufVxuXG4ubHZsbnVtMiB7XG4gIG1hcmdpbjogMDtcbiAgbWFyZ2luLXRvcDogNCU7XG4gIGZvbnQtd2VpZ2h0OiBib2xkZXI7XG4gIGZvbnQtZmFtaWx5OiBWZXJkYW5hLCBHZW5ldmEsIFRhaG9tYSwgc2Fucy1zZXJpZjtcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/purchasecoins/purchasecoins.page.ts":
  /*!*****************************************************!*\
    !*** ./src/app/purchasecoins/purchasecoins.page.ts ***!
    \*****************************************************/

  /*! exports provided: PurchasecoinsPage */

  /***/
  function srcAppPurchasecoinsPurchasecoinsPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PurchasecoinsPage", function () {
      return PurchasecoinsPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var _providers_credential_provider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../providers/credential-provider */
    "./src/providers/credential-provider.ts");

    var PurchasecoinsPage = /*#__PURE__*/function () {
      function PurchasecoinsPage(navParams, storage, modalCtrl, toastController, postPvdr) {
        _classCallCheck(this, PurchasecoinsPage);

        this.storage = storage;
        this.modalCtrl = modalCtrl;
        this.toastController = toastController;
        this.postPvdr = postPvdr;
        this.coin = navParams.get('coin');
      }

      _createClass(PurchasecoinsPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "closeModal",
        value: function closeModal() {
          this.modalCtrl.dismiss();
        }
      }, {
        key: "presentToast",
        value: function presentToast() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee18() {
            var toast;
            return regeneratorRuntime.wrap(function _callee18$(_context18) {
              while (1) {
                switch (_context18.prev = _context18.next) {
                  case 0:
                    _context18.next = 2;
                    return this.toastController.create({
                      message: 'Purchased the coins successfully.',
                      duration: 3000
                    });

                  case 2:
                    toast = _context18.sent;
                    toast.present();

                  case 4:
                  case "end":
                    return _context18.stop();
                }
              }
            }, _callee18, this);
          }));
        }
      }, {
        key: "purchaseCoins",
        value: function purchaseCoins() {
          var _this11 = this;

          console.log("mipalit lamang pud");
          console.log("coin Id:" + this.coin.id);
          console.log("coin coins:" + this.coin.coins);
          console.log("coin amount:" + this.coin.amount);
          this.modalCtrl.dismiss();
          this.storage.get("user_id").then(function (user_id) {
            var body2 = {
              action: 'purchaseCoins',
              user_id: user_id,
              coin_id: _this11.coin.id,
              coins: _this11.coin.coins,
              amount: _this11.coin.amount
            };

            _this11.postPvdr.postData(body2, 'brixy-store.php').subscribe(function (data) {
              if (data.success) {
                _this11.presentToast();
              }
            });
          });
        }
      }]);

      return PurchasecoinsPage;
    }();

    PurchasecoinsPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavParams"]
      }, {
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]
      }, {
        type: _providers_credential_provider__WEBPACK_IMPORTED_MODULE_4__["PostProvider"]
      }];
    };

    PurchasecoinsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-purchasecoins',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./purchasecoins.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/purchasecoins/purchasecoins.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./purchasecoins.page.scss */
      "./src/app/purchasecoins/purchasecoins.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavParams"], _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"], _providers_credential_provider__WEBPACK_IMPORTED_MODULE_4__["PostProvider"]])], PurchasecoinsPage);
    /***/
  },

  /***/
  "./src/app/purchaseproduct/purchaseproduct-routing.module.ts":
  /*!*******************************************************************!*\
    !*** ./src/app/purchaseproduct/purchaseproduct-routing.module.ts ***!
    \*******************************************************************/

  /*! exports provided: PurchaseproductPageRoutingModule */

  /***/
  function srcAppPurchaseproductPurchaseproductRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PurchaseproductPageRoutingModule", function () {
      return PurchaseproductPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _purchaseproduct_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./purchaseproduct.page */
    "./src/app/purchaseproduct/purchaseproduct.page.ts");

    var routes = [{
      path: '',
      component: _purchaseproduct_page__WEBPACK_IMPORTED_MODULE_3__["PurchaseproductPage"]
    }];

    var PurchaseproductPageRoutingModule = function PurchaseproductPageRoutingModule() {
      _classCallCheck(this, PurchaseproductPageRoutingModule);
    };

    PurchaseproductPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], PurchaseproductPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/purchaseproduct/purchaseproduct.module.ts":
  /*!***********************************************************!*\
    !*** ./src/app/purchaseproduct/purchaseproduct.module.ts ***!
    \***********************************************************/

  /*! exports provided: PurchaseproductPageModule */

  /***/
  function srcAppPurchaseproductPurchaseproductModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PurchaseproductPageModule", function () {
      return PurchaseproductPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _purchaseproduct_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./purchaseproduct-routing.module */
    "./src/app/purchaseproduct/purchaseproduct-routing.module.ts");
    /* harmony import */


    var _purchaseproduct_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./purchaseproduct.page */
    "./src/app/purchaseproduct/purchaseproduct.page.ts");

    var PurchaseproductPageModule = function PurchaseproductPageModule() {
      _classCallCheck(this, PurchaseproductPageModule);
    };

    PurchaseproductPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _purchaseproduct_routing_module__WEBPACK_IMPORTED_MODULE_5__["PurchaseproductPageRoutingModule"]],
      declarations: [_purchaseproduct_page__WEBPACK_IMPORTED_MODULE_6__["PurchaseproductPage"]]
    })], PurchaseproductPageModule);
    /***/
  },

  /***/
  "./src/app/purchaseproduct/purchaseproduct.page.scss":
  /*!***********************************************************!*\
    !*** ./src/app/purchaseproduct/purchaseproduct.page.scss ***!
    \***********************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPurchaseproductPurchaseproductPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".for_post {\n  --background: none;\n  background-image: url(\"/assets/greenthumb-images/bunny1.png\");\n  background-repeat: no-repeat;\n  background-size: contain;\n}\n\n.for_post:before {\n  content: \"\";\n  position: absolute;\n  left: 0;\n  right: 0;\n  top: 0;\n  bottom: 5%;\n  background: rgba(255, 255, 255, 0.9);\n}\n\n#overlay {\n  width: 100%;\n  height: 85px;\n  background: white;\n  z-index: 20;\n  bottom: 0%;\n  left: 0;\n  border-top: 1px solid #e2e2e2;\n}\n\n.line-input {\n  margin-bottom: 0 !important;\n  background: transparent;\n}\n\n.line-input ion-item {\n  --border-color: transparent!important;\n  --highlight-height: 0;\n  border: 1px solid #e2e2e2;\n  border-radius: 4px;\n  height: 59px;\n  margin: auto;\n  --background: white;\n  width: 160px;\n  text-align: center !important;\n}\n\n.item_input {\n  font-size: 32px;\n  color: #679733 !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZW5kcmljay9EZXNrdG9wL1RlbG1vIFNvbHV0aW9ucy9Gb3JQcm9kdWN0aW9uL21heTEwMjAyMS9sYXN0IHNldHVwL2dyZWVudGh1bWJhcHAgY29weS9zcmMvYXBwL3B1cmNoYXNlcHJvZHVjdC9wdXJjaGFzZXByb2R1Y3QucGFnZS5zY3NzIiwic3JjL2FwcC9wdXJjaGFzZXByb2R1Y3QvcHVyY2hhc2Vwcm9kdWN0LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGtCQUFBO0VBQ0EsNkRBQUE7RUFDQSw0QkFBQTtFQUNBLHdCQUFBO0FDQ0o7O0FEQ0E7RUFDSSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxPQUFBO0VBQVMsUUFBQTtFQUNULE1BQUE7RUFBUSxVQUFBO0VBQ1Isb0NBQUE7QUNJSjs7QURGQTtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxXQUFBO0VBQ0EsVUFBQTtFQUNBLE9BQUE7RUFDQSw2QkFBQTtBQ0tKOztBREhFO0VBQ0UsMkJBQUE7RUFDQSx1QkFBQTtBQ01KOztBRExJO0VBQ0kscUNBQUE7RUFDQSxxQkFBQTtFQUNBLHlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLDZCQUFBO0FDT1I7O0FESkE7RUFDSSxlQUFBO0VBQ0EseUJBQUE7QUNPSiIsImZpbGUiOiJzcmMvYXBwL3B1cmNoYXNlcHJvZHVjdC9wdXJjaGFzZXByb2R1Y3QucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmZvcl9wb3N0e1xuICAgIC0tYmFja2dyb3VuZDogbm9uZTtcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoJy9hc3NldHMvZ3JlZW50aHVtYi1pbWFnZXMvYnVubnkxLnBuZycpO1xuICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gICAgYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xufVxuLmZvcl9wb3N0OmJlZm9yZSB7XG4gICAgY29udGVudDogXCJcIjtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgbGVmdDogMDsgcmlnaHQ6IDA7XG4gICAgdG9wOiAwOyBib3R0b206IDUlO1xuICAgIGJhY2tncm91bmQ6IHJnYmEoMjU1LDI1NSwyNTUsOTAlKTtcbiAgfVxuI292ZXJsYXl7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiA4NXB4O1xuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuICAgIHotaW5kZXg6IDIwO1xuICAgIGJvdHRvbTogMCU7XG4gICAgbGVmdDogMDtcbiAgICBib3JkZXItdG9wOiAxcHggc29saWQgI2UyZTJlMjtcbiAgfVxuICAubGluZS1pbnB1dCB7XG4gICAgbWFyZ2luLWJvdHRvbTogMCFpbXBvcnRhbnQ7XG4gICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG4gICAgaW9uLWl0ZW0ge1xuICAgICAgICAtLWJvcmRlci1jb2xvcjogdHJhbnNwYXJlbnQhaW1wb3J0YW50O1xuICAgICAgICAtLWhpZ2hsaWdodC1oZWlnaHQ6IDA7XG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNlMmUyZTI7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgICAgICAgaGVpZ2h0OiA1OXB4O1xuICAgICAgICBtYXJnaW46IGF1dG87XG4gICAgICAgIC0tYmFja2dyb3VuZDogd2hpdGU7XG4gICAgICAgIHdpZHRoOiAxNjBweDtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyICFpbXBvcnRhbnQ7XG4gICAgfVxuICB9XG4uaXRlbV9pbnB1dHtcbiAgICBmb250LXNpemU6IDMycHg7XG4gICAgY29sb3I6ICM2Nzk3MzMgIWltcG9ydGFudDtcbn0iLCIuZm9yX3Bvc3Qge1xuICAtLWJhY2tncm91bmQ6IG5vbmU7XG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybChcIi9hc3NldHMvZ3JlZW50aHVtYi1pbWFnZXMvYnVubnkxLnBuZ1wiKTtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xufVxuXG4uZm9yX3Bvc3Q6YmVmb3JlIHtcbiAgY29udGVudDogXCJcIjtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiAwO1xuICByaWdodDogMDtcbiAgdG9wOiAwO1xuICBib3R0b206IDUlO1xuICBiYWNrZ3JvdW5kOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuOSk7XG59XG5cbiNvdmVybGF5IHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogODVweDtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIHotaW5kZXg6IDIwO1xuICBib3R0b206IDAlO1xuICBsZWZ0OiAwO1xuICBib3JkZXItdG9wOiAxcHggc29saWQgI2UyZTJlMjtcbn1cblxuLmxpbmUtaW5wdXQge1xuICBtYXJnaW4tYm90dG9tOiAwICFpbXBvcnRhbnQ7XG4gIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xufVxuLmxpbmUtaW5wdXQgaW9uLWl0ZW0ge1xuICAtLWJvcmRlci1jb2xvcjogdHJhbnNwYXJlbnQhaW1wb3J0YW50O1xuICAtLWhpZ2hsaWdodC1oZWlnaHQ6IDA7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNlMmUyZTI7XG4gIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgaGVpZ2h0OiA1OXB4O1xuICBtYXJnaW46IGF1dG87XG4gIC0tYmFja2dyb3VuZDogd2hpdGU7XG4gIHdpZHRoOiAxNjBweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyICFpbXBvcnRhbnQ7XG59XG5cbi5pdGVtX2lucHV0IHtcbiAgZm9udC1zaXplOiAzMnB4O1xuICBjb2xvcjogIzY3OTczMyAhaW1wb3J0YW50O1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/purchaseproduct/purchaseproduct.page.ts":
  /*!*********************************************************!*\
    !*** ./src/app/purchaseproduct/purchaseproduct.page.ts ***!
    \*********************************************************/

  /*! exports provided: PurchaseproductPage */

  /***/
  function srcAppPurchaseproductPurchaseproductPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PurchaseproductPage", function () {
      return PurchaseproductPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _providers_credential_provider__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../providers/credential-provider */
    "./src/providers/credential-provider.ts");

    var PurchaseproductPage = /*#__PURE__*/function () {
      function PurchaseproductPage(navParams, postPvdr, modalCtrl, toastController, navCtrl) {
        _classCallCheck(this, PurchaseproductPage);

        this.postPvdr = postPvdr;
        this.modalCtrl = modalCtrl;
        this.toastController = toastController;
        this.navCtrl = navCtrl;
        this.item_id = "";
        this.item_user_id = "";
        this.login_user_id = "";
        this.item_name = "";
        this.item_id = navParams.get('item_id');
        this.item_user_id = navParams.get('item_user_id');
        this.login_user_id = navParams.get('login_user_id');
        this.item_name = navParams.get('item_name');
      }

      _createClass(PurchaseproductPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "closeModal",
        value: function closeModal() {
          this.modalCtrl.dismiss(this.purchase_quantity);
        }
      }, {
        key: "purchaseProduct",
        value: function purchaseProduct() {
          var _this12 = this;

          var body = {
            action: 'purchaseItem',
            item_id: this.item_id,
            user_id: this.login_user_id,
            item_user_id: this.item_user_id,
            purchase_quantity: this.purchase_quantity,
            item_name: this.item_name
          };
          this.postPvdr.postData(body, 'save_item.php').subscribe(function (data) {
            if (data.success) {
              if (data.invalid_quantity) {
                _this12.presentToast();
              } else {
                _this12.navCtrl.navigateRoot(['/message/' + _this12.item_user_id + "-separator-" + _this12.item_id + "-separator-" + '1']);

                _this12.modalCtrl.dismiss(_this12.purchase_quantity);
              }
            }
          });
        }
      }, {
        key: "presentToast",
        value: function presentToast() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee19() {
            var toast;
            return regeneratorRuntime.wrap(function _callee19$(_context19) {
              while (1) {
                switch (_context19.prev = _context19.next) {
                  case 0:
                    _context19.next = 2;
                    return this.toastController.create({
                      message: 'Quantity must be lesser than or equal to the item stocks.',
                      duration: 3000
                    });

                  case 2:
                    toast = _context19.sent;
                    toast.present();

                  case 4:
                  case "end":
                    return _context19.stop();
                }
              }
            }, _callee19, this);
          }));
        }
      }]);

      return PurchaseproductPage;
    }();

    PurchaseproductPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavParams"]
      }, {
        type: _providers_credential_provider__WEBPACK_IMPORTED_MODULE_3__["PostProvider"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]
      }];
    };

    PurchaseproductPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-purchaseproduct',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./purchaseproduct.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/purchaseproduct/purchaseproduct.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./purchaseproduct.page.scss */
      "./src/app/purchaseproduct/purchaseproduct.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavParams"], _providers_credential_provider__WEBPACK_IMPORTED_MODULE_3__["PostProvider"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]])], PurchaseproductPage);
    /***/
  },

  /***/
  "./src/app/rate/rate-routing.module.ts":
  /*!*********************************************!*\
    !*** ./src/app/rate/rate-routing.module.ts ***!
    \*********************************************/

  /*! exports provided: RatePageRoutingModule */

  /***/
  function srcAppRateRateRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "RatePageRoutingModule", function () {
      return RatePageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _rate_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./rate.page */
    "./src/app/rate/rate.page.ts");

    var routes = [{
      path: '',
      component: _rate_page__WEBPACK_IMPORTED_MODULE_3__["RatePage"]
    }];

    var RatePageRoutingModule = function RatePageRoutingModule() {
      _classCallCheck(this, RatePageRoutingModule);
    };

    RatePageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], RatePageRoutingModule);
    /***/
  },

  /***/
  "./src/app/rate/rate.module.ts":
  /*!*************************************!*\
    !*** ./src/app/rate/rate.module.ts ***!
    \*************************************/

  /*! exports provided: RatePageModule */

  /***/
  function srcAppRateRateModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "RatePageModule", function () {
      return RatePageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _rate_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./rate-routing.module */
    "./src/app/rate/rate-routing.module.ts");
    /* harmony import */


    var _rate_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./rate.page */
    "./src/app/rate/rate.page.ts");
    /* harmony import */


    var ionic4_star_rating__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ionic4-star-rating */
    "./node_modules/ionic4-star-rating/dist/index.js");

    var RatePageModule = function RatePageModule() {
      _classCallCheck(this, RatePageModule);
    };

    RatePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], ionic4_star_rating__WEBPACK_IMPORTED_MODULE_7__["StarRatingModule"], _rate_routing_module__WEBPACK_IMPORTED_MODULE_5__["RatePageRoutingModule"]],
      declarations: [_rate_page__WEBPACK_IMPORTED_MODULE_6__["RatePage"]]
    })], RatePageModule);
    /***/
  },

  /***/
  "./src/app/rate/rate.page.scss":
  /*!*************************************!*\
    !*** ./src/app/rate/rate.page.scss ***!
    \*************************************/

  /*! exports provided: default */

  /***/
  function srcAppRateRatePageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".for_txt {\n  line-height: 1.3;\n  margin-top: -5px;\n  margin-bottom: 10px;\n}\n\n.for_button {\n  margin: 20px;\n  margin-bottom: 0;\n  margin-top: 15px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZW5kcmljay9EZXNrdG9wL1RlbG1vIFNvbHV0aW9ucy9Gb3JQcm9kdWN0aW9uL21heTEwMjAyMS9sYXN0IHNldHVwL2dyZWVudGh1bWJhcHAgY29weS9zcmMvYXBwL3JhdGUvcmF0ZS5wYWdlLnNjc3MiLCJzcmMvYXBwL3JhdGUvcmF0ZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7QUNDSjs7QURDQTtFQUNJLFlBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0FDRUoiLCJmaWxlIjoic3JjL2FwcC9yYXRlL3JhdGUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmZvcl90eHR7XG4gICAgbGluZS1oZWlnaHQ6IDEuMztcbiAgICBtYXJnaW4tdG9wOiAtNXB4O1xuICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XG59XG4uZm9yX2J1dHRvbntcbiAgICBtYXJnaW46IDIwcHg7XG4gICAgbWFyZ2luLWJvdHRvbTogMDtcbiAgICBtYXJnaW4tdG9wOiAxNXB4O1xufSIsIi5mb3JfdHh0IHtcbiAgbGluZS1oZWlnaHQ6IDEuMztcbiAgbWFyZ2luLXRvcDogLTVweDtcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcbn1cblxuLmZvcl9idXR0b24ge1xuICBtYXJnaW46IDIwcHg7XG4gIG1hcmdpbi1ib3R0b206IDA7XG4gIG1hcmdpbi10b3A6IDE1cHg7XG59Il19 */";
    /***/
  },

  /***/
  "./src/app/rate/rate.page.ts":
  /*!***********************************!*\
    !*** ./src/app/rate/rate.page.ts ***!
    \***********************************/

  /*! exports provided: RatePage */

  /***/
  function srcAppRateRatePageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "RatePage", function () {
      return RatePage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _providers_credential_provider__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../providers/credential-provider */
    "./src/providers/credential-provider.ts");

    var RatePage = /*#__PURE__*/function () {
      function RatePage(modalController, postPvdr, toastController) {
        _classCallCheck(this, RatePage);

        this.modalController = modalController;
        this.postPvdr = postPvdr;
        this.toastController = toastController;
        this.myRate = "";
      }

      _createClass(RatePage, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "logRatingChange",
        value: function logRatingChange(event) {
          this.myRate = event;
        }
      }, {
        key: "submitRate",
        value: function submitRate() {
          var _this13 = this;

          console.log("rate_user_id:" + this.rate_user_id);
          console.log("login_user_id:" + this.login_user_id);
          console.log("notif_id:" + this.notif_id);

          if (this.myRate == '') {
            this.presentToast('Please select a rating');
          } else {
            var body = {
              action: 'rateUser',
              rate_user_id: this.rate_user_id,
              login_user_id: this.login_user_id,
              myRate: this.myRate,
              notif_id: this.notif_id
            };
            this.postPvdr.postData(body, 'rating.php').subscribe(function (data) {
              if (data.success) {
                _this13.modalController.dismiss(true);

                _this13.presentToast('Thank you for your rating');
              }
            });
          }
        }
      }, {
        key: "close",
        value: function close() {
          this.modalController.dismiss(false);
        }
      }, {
        key: "presentToast",
        value: function presentToast(message) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee20() {
            var toast;
            return regeneratorRuntime.wrap(function _callee20$(_context20) {
              while (1) {
                switch (_context20.prev = _context20.next) {
                  case 0:
                    _context20.next = 2;
                    return this.toastController.create({
                      message: message,
                      duration: 3000
                    });

                  case 2:
                    toast = _context20.sent;
                    toast.present();

                  case 4:
                  case "end":
                    return _context20.stop();
                }
              }
            }, _callee20, this);
          }));
        }
      }]);

      return RatePage;
    }();

    RatePage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
      }, {
        type: _providers_credential_provider__WEBPACK_IMPORTED_MODULE_3__["PostProvider"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]
      }];
    };

    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)], RatePage.prototype, "login_user_id", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)], RatePage.prototype, "rate_user_id", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)], RatePage.prototype, "notif_id", void 0);
    RatePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-rate',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./rate.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/rate/rate.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./rate.page.scss */
      "./src/app/rate/rate.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"], _providers_credential_provider__WEBPACK_IMPORTED_MODULE_3__["PostProvider"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]])], RatePage);
    /***/
  },

  /***/
  "./src/app/register/register.module.ts":
  /*!*********************************************!*\
    !*** ./src/app/register/register.module.ts ***!
    \*********************************************/

  /*! exports provided: RegisterPageModule */

  /***/
  function srcAppRegisterRegisterModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "RegisterPageModule", function () {
      return RegisterPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _register_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./register.page */
    "./src/app/register/register.page.ts");

    var routes = [{
      path: '',
      component: _register_page__WEBPACK_IMPORTED_MODULE_6__["RegisterPage"]
    }];

    var RegisterPageModule = function RegisterPageModule() {
      _classCallCheck(this, RegisterPageModule);
    };

    RegisterPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      declarations: [_register_page__WEBPACK_IMPORTED_MODULE_6__["RegisterPage"]]
    })], RegisterPageModule);
    /***/
  },

  /***/
  "./src/app/register/register.page.scss":
  /*!*********************************************!*\
    !*** ./src/app/register/register.page.scss ***!
    \*********************************************/

  /*! exports provided: default */

  /***/
  function srcAppRegisterRegisterPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "#for_eachitem {\n  font-size: 14px;\n  --inner-padding-end: 0px !important;\n  --border-color: #e2f0cb;\n  --padding-start: 0% !important;\n}\n\n.for_seticon {\n  font-size: 15px;\n  color: #9da2b3;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZW5kcmljay9EZXNrdG9wL1RlbG1vIFNvbHV0aW9ucy9Gb3JQcm9kdWN0aW9uL21heTEwMjAyMS9sYXN0IHNldHVwL2dyZWVudGh1bWJhcHAgY29weS9zcmMvYXBwL3JlZ2lzdGVyL3JlZ2lzdGVyLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcmVnaXN0ZXIvcmVnaXN0ZXIucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO0VBQ0UsZUFBQTtFQUNBLG1DQUFBO0VBQ0EsdUJBQUE7RUFDQSw4QkFBQTtBQ0FGOztBREVBO0VBQ0UsZUFBQTtFQUNBLGNBQUE7QUNDRiIsImZpbGUiOiJzcmMvYXBwL3JlZ2lzdGVyL3JlZ2lzdGVyLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIlxuI2Zvcl9lYWNoaXRlbXtcbiAgZm9udC1zaXplOiAxNHB4O1xuICAtLWlubmVyLXBhZGRpbmctZW5kOiAwcHggIWltcG9ydGFudDtcbiAgLS1ib3JkZXItY29sb3I6ICNlMmYwY2I7XG4gIC0tcGFkZGluZy1zdGFydDogMCUgIWltcG9ydGFudDtcbn1cbi5mb3Jfc2V0aWNvbntcbiAgZm9udC1zaXplOiAxNXB4O1xuICBjb2xvcjogIzlkYTJiMztcbn0iLCIjZm9yX2VhY2hpdGVtIHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICAtLWlubmVyLXBhZGRpbmctZW5kOiAwcHggIWltcG9ydGFudDtcbiAgLS1ib3JkZXItY29sb3I6ICNlMmYwY2I7XG4gIC0tcGFkZGluZy1zdGFydDogMCUgIWltcG9ydGFudDtcbn1cblxuLmZvcl9zZXRpY29uIHtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBjb2xvcjogIzlkYTJiMztcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/register/register.page.ts":
  /*!*******************************************!*\
    !*** ./src/app/register/register.page.ts ***!
    \*******************************************/

  /*! exports provided: RegisterPage */

  /***/
  function srcAppRegisterRegisterPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "RegisterPage", function () {
      return RegisterPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _providers_credential_provider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../providers/credential-provider */
    "./src/providers/credential-provider.ts");
    /* harmony import */


    var _userpolicy_userpolicy_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../userpolicy/userpolicy.page */
    "./src/app/userpolicy/userpolicy.page.ts");

    var RegisterPage = /*#__PURE__*/function () {
      function RegisterPage(storage, toastController, postPvdr, modalController) {
        _classCallCheck(this, RegisterPage);

        this.storage = storage;
        this.toastController = toastController;
        this.postPvdr = postPvdr;
        this.modalController = modalController;
      }

      _createClass(RegisterPage, [{
        key: "presentToast",
        value: function presentToast() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee21() {
            var toast;
            return regeneratorRuntime.wrap(function _callee21$(_context21) {
              while (1) {
                switch (_context21.prev = _context21.next) {
                  case 0:
                    _context21.next = 2;
                    return this.toastController.create({
                      message: 'Congratulations. Your account has been updated.',
                      duration: 3000
                    });

                  case 2:
                    toast = _context21.sent;
                    toast.present();

                  case 4:
                  case "end":
                    return _context21.stop();
                }
              }
            }, _callee21, this);
          }));
        }
      }, {
        key: "presentToastLack",
        value: function presentToastLack() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee22() {
            var toast;
            return regeneratorRuntime.wrap(function _callee22$(_context22) {
              while (1) {
                switch (_context22.prev = _context22.next) {
                  case 0:
                    _context22.next = 2;
                    return this.toastController.create({
                      message: 'Please fill in all fields.',
                      duration: 3000
                    });

                  case 2:
                    toast = _context22.sent;
                    toast.present();

                  case 4:
                  case "end":
                    return _context22.stop();
                }
              }
            }, _callee22, this);
          }));
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {
          this.pre_register();
        }
      }, {
        key: "pre_register",
        value: function pre_register() {
          var _this14 = this;

          this.storage.get("greenthumb_user_id").then(function (user_id) {
            _this14.login_user_id = user_id;
            var body = {
              action: 'getUserData',
              user_id: user_id
            };

            _this14.postPvdr.postData(body, 'user.php').subscribe(function (data) {
              return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this14, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee23() {
                return regeneratorRuntime.wrap(function _callee23$(_context23) {
                  while (1) {
                    switch (_context23.prev = _context23.next) {
                      case 0:
                        if (data.success) {
                          this.email = data.result.email;
                          this.fname = data.result.fname;
                          this.lname = data.result.lname;
                          this.bdate = data.result.bdate;
                          this.city = data.result.city;
                          this.country = data.result.country;
                          this.mobile_num = data.result.mobile_num;
                        }

                      case 1:
                      case "end":
                        return _context23.stop();
                    }
                  }
                }, _callee23, this);
              }));
            });
          }); // this.storage.get('login_used').then((val) => {
          //   this.login_used = val;
          // });
          // this.storage.get('email').then((val) => {
          //   this.email = val;
          // });
          // this.storage.get('fname').then((val) => {
          //   this.fname = val;
          // });
          // this.storage.get('mobile_num').then((val) => {
          //   this.mobile_num = val;
          // });
        }
      }, {
        key: "register",
        value: function register() {
          var _this15 = this;

          var body1 = {
            action: "register",
            fname: this.inpt_fname,
            lname: this.inpt_lname,
            // nickname : this.inpt_nickname,
            birthdate: this.inpt_birthdate.substring(0, 10),
            // gender : this.inpt_gender,
            city: this.inpt_city,
            country: this.inpt_country,
            email: this.inpt_email,
            mobile_num: this.inpt_mobile_num,
            user_id: this.login_user_id
          };
          console.log("bodybody1:" + JSON.stringify(body1));

          if (this.inpt_fname !== undefined && this.inpt_lname !== undefined && this.inpt_birthdate !== undefined && this.inpt_city !== undefined && this.inpt_country !== undefined && this.inpt_email !== undefined && this.inpt_mobile_num !== undefined) {
            var body = {
              action: "register",
              fname: this.inpt_fname,
              lname: this.inpt_lname,
              // nickname : this.inpt_nickname,
              birthdate: this.inpt_birthdate.substring(0, 10),
              // gender : this.inpt_gender,
              city: this.inpt_city,
              country: this.inpt_country,
              email: this.inpt_email,
              mobile_num: this.inpt_mobile_num,
              user_id: this.login_user_id
            };
            console.log("bodybody:" + JSON.stringify(body));
            this.postPvdr.postData(body, 'credentials-api.php').subscribe(function (data) {
              if (data.success) {
                _this15.presentToast();

                _this15.modalController.dismiss();
              }
            });
          } else {
            this.presentToastLack();
          }
        }
      }, {
        key: "goToUserPolicy",
        value: function goToUserPolicy() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee24() {
            var modal;
            return regeneratorRuntime.wrap(function _callee24$(_context24) {
              while (1) {
                switch (_context24.prev = _context24.next) {
                  case 0:
                    _context24.next = 2;
                    return this.modalController.create({
                      component: _userpolicy_userpolicy_page__WEBPACK_IMPORTED_MODULE_5__["UserpolicyPage"],
                      cssClass: ''
                    });

                  case 2:
                    modal = _context24.sent;
                    _context24.next = 5;
                    return modal.present();

                  case 5:
                    return _context24.abrupt("return", _context24.sent);

                  case 6:
                  case "end":
                    return _context24.stop();
                }
              }
            }, _callee24, this);
          }));
        }
      }, {
        key: "cancel",
        value: function cancel() {
          this.modalController.dismiss();
        }
      }]);

      return RegisterPage;
    }();

    RegisterPage.ctorParameters = function () {
      return [{
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_2__["Storage"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"]
      }, {
        type: _providers_credential_provider__WEBPACK_IMPORTED_MODULE_4__["PostProvider"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"]
      }];
    };

    RegisterPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-register',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./register.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/register/register.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./register.page.scss */
      "./src/app/register/register.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_storage__WEBPACK_IMPORTED_MODULE_2__["Storage"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"], _providers_credential_provider__WEBPACK_IMPORTED_MODULE_4__["PostProvider"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"]])], RegisterPage);
    /***/
  },

  /***/
  "./src/app/report/report-routing.module.ts":
  /*!*************************************************!*\
    !*** ./src/app/report/report-routing.module.ts ***!
    \*************************************************/

  /*! exports provided: ReportPageRoutingModule */

  /***/
  function srcAppReportReportRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ReportPageRoutingModule", function () {
      return ReportPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _report_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./report.page */
    "./src/app/report/report.page.ts");

    var routes = [{
      path: '',
      component: _report_page__WEBPACK_IMPORTED_MODULE_3__["ReportPage"]
    }];

    var ReportPageRoutingModule = function ReportPageRoutingModule() {
      _classCallCheck(this, ReportPageRoutingModule);
    };

    ReportPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], ReportPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/report/report.module.ts":
  /*!*****************************************!*\
    !*** ./src/app/report/report.module.ts ***!
    \*****************************************/

  /*! exports provided: ReportPageModule */

  /***/
  function srcAppReportReportModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ReportPageModule", function () {
      return ReportPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _report_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./report-routing.module */
    "./src/app/report/report-routing.module.ts");
    /* harmony import */


    var _report_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./report.page */
    "./src/app/report/report.page.ts");

    var ReportPageModule = function ReportPageModule() {
      _classCallCheck(this, ReportPageModule);
    };

    ReportPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _report_routing_module__WEBPACK_IMPORTED_MODULE_5__["ReportPageRoutingModule"]],
      declarations: [_report_page__WEBPACK_IMPORTED_MODULE_6__["ReportPage"]]
    })], ReportPageModule);
    /***/
  },

  /***/
  "./src/app/report/report.page.scss":
  /*!*****************************************!*\
    !*** ./src/app/report/report.page.scss ***!
    \*****************************************/

  /*! exports provided: default */

  /***/
  function srcAppReportReportPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".line-input {\n  margin-bottom: 0 !important;\n}\n.line-input ion-item {\n  --border-color: transparent!important;\n  --highlight-height: 0;\n  border: 1px solid #dedede;\n  border-radius: 4px;\n  height: 100px;\n  margin-top: 4%;\n}\n.item_input {\n  font-size: 14px;\n  color: #424242 !important;\n}\n.item_label {\n  color: #b3aeae !important;\n  font-weight: 300;\n  font-size: 13px;\n}\n.for_itemspace {\n  margin-top: -10px;\n  --background: transparent;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZW5kcmljay9EZXNrdG9wL1RlbG1vIFNvbHV0aW9ucy9Gb3JQcm9kdWN0aW9uL21heTEwMjAyMS9sYXN0IHNldHVwL2dyZWVudGh1bWJhcHAgY29weS9zcmMvYXBwL3JlcG9ydC9yZXBvcnQucGFnZS5zY3NzIiwic3JjL2FwcC9yZXBvcnQvcmVwb3J0LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLDJCQUFBO0FDQ0o7QURBSTtFQUNJLHFDQUFBO0VBQ0EscUJBQUE7RUFDQSx5QkFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLGNBQUE7QUNFUjtBRENBO0VBQ0ksZUFBQTtFQUVBLHlCQUFBO0FDQ0o7QURDQTtFQUNJLHlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0FDRUo7QURBQTtFQUNJLGlCQUFBO0VBQ0EseUJBQUE7QUNHSiIsImZpbGUiOiJzcmMvYXBwL3JlcG9ydC9yZXBvcnQucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmxpbmUtaW5wdXQge1xuICAgIG1hcmdpbi1ib3R0b206IDAhaW1wb3J0YW50O1xuICAgIGlvbi1pdGVtIHtcbiAgICAgICAgLS1ib3JkZXItY29sb3I6IHRyYW5zcGFyZW50IWltcG9ydGFudDtcbiAgICAgICAgLS1oaWdobGlnaHQtaGVpZ2h0OiAwO1xuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjZGVkZWRlO1xuICAgICAgICBib3JkZXItcmFkaXVzOiA0cHg7XG4gICAgICAgIGhlaWdodDogMTAwcHg7XG4gICAgICAgIG1hcmdpbi10b3A6IDQlO1xuICAgIH1cbn1cbi5pdGVtX2lucHV0e1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAvLyAtLXBhZGRpbmctdG9wOiAwO1xuICAgIGNvbG9yOiAjNDI0MjQyIWltcG9ydGFudDtcbn1cbi5pdGVtX2xhYmVse1xuICAgIGNvbG9yOiAjYjNhZWFlICFpbXBvcnRhbnQ7XG4gICAgZm9udC13ZWlnaHQ6IDMwMDtcbiAgICBmb250LXNpemU6IDEzcHg7XG59XG4uZm9yX2l0ZW1zcGFjZXtcbiAgICBtYXJnaW4tdG9wOiAtMTBweDtcbiAgICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xufSIsIi5saW5lLWlucHV0IHtcbiAgbWFyZ2luLWJvdHRvbTogMCAhaW1wb3J0YW50O1xufVxuLmxpbmUtaW5wdXQgaW9uLWl0ZW0ge1xuICAtLWJvcmRlci1jb2xvcjogdHJhbnNwYXJlbnQhaW1wb3J0YW50O1xuICAtLWhpZ2hsaWdodC1oZWlnaHQ6IDA7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNkZWRlZGU7XG4gIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgaGVpZ2h0OiAxMDBweDtcbiAgbWFyZ2luLXRvcDogNCU7XG59XG5cbi5pdGVtX2lucHV0IHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBjb2xvcjogIzQyNDI0MiAhaW1wb3J0YW50O1xufVxuXG4uaXRlbV9sYWJlbCB7XG4gIGNvbG9yOiAjYjNhZWFlICFpbXBvcnRhbnQ7XG4gIGZvbnQtd2VpZ2h0OiAzMDA7XG4gIGZvbnQtc2l6ZTogMTNweDtcbn1cblxuLmZvcl9pdGVtc3BhY2Uge1xuICBtYXJnaW4tdG9wOiAtMTBweDtcbiAgLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/report/report.page.ts":
  /*!***************************************!*\
    !*** ./src/app/report/report.page.ts ***!
    \***************************************/

  /*! exports provided: ReportPage */

  /***/
  function srcAppReportReportPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ReportPage", function () {
      return ReportPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _shared_model_report_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../shared/model/report.model */
    "./src/app/shared/model/report.model.ts");
    /* harmony import */


    var _providers_credential_provider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../providers/credential-provider */
    "./src/providers/credential-provider.ts");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");

    var ReportPage = /*#__PURE__*/function () {
      function ReportPage(router, postPvdr, storage, modalController, alertCtrl) {
        _classCallCheck(this, ReportPage);

        this.router = router;
        this.postPvdr = postPvdr;
        this.storage = storage;
        this.modalController = modalController;
        this.alertCtrl = alertCtrl;
        this.theReason = '';
        this.otherReason = '';
        this.others = false;
        this.reasonList = [];
      }

      _createClass(ReportPage, [{
        key: "goBack",
        value: function goBack() {
          // this.router.navigate(['help']);
          //window.history.back();
          this.modalController.dismiss();
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {
          this.plotData();
          this.theReason = "Showing Inappropriate Behavior";
        }
      }, {
        key: "plotData",
        value: function plotData() {
          var _this16 = this;

          var body = {
            action: 'reportReason'
          };
          this.postPvdr.postData(body, 'report.php').subscribe(function (data) {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this16, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee25() {
              var key;
              return regeneratorRuntime.wrap(function _callee25$(_context25) {
                while (1) {
                  switch (_context25.prev = _context25.next) {
                    case 0:
                      if (data.success) {
                        for (key in data.result) {
                          this.reasonList.push(new _shared_model_report_model__WEBPACK_IMPORTED_MODULE_3__["reportReason"](data.result[key].id, data.result[key].reason));
                        }
                      }

                    case 1:
                    case "end":
                      return _context25.stop();
                  }
                }
              }, _callee25, this);
            }));
          });
        }
      }, {
        key: "othersClick",
        value: function othersClick() {
          this.others = !this.others;
          console.log("this.others:" + this.others);

          if (!this.others) {
            this.otherReason = "";
          }
        }
      }, {
        key: "reportThisUser",
        value: function reportThisUser() {
          var _this17 = this;

          console.log("thereason:" + this.theReason);
          this.storage.get('greenthumb_user_id').then(function (user_id) {
            if (_this17.others && _this17.otherReason.length > 0) {
              _this17.theReason = _this17.theReason + "     -     " + _this17.otherReason;
            }

            var body2 = {
              action: 'reportThisItem',
              reported_item: _this17.reportedId,
              reported_by: user_id,
              report_reason: _this17.theReason
            };
            console.log("report:" + JSON.stringify(body2));

            _this17.postPvdr.postData(body2, 'report.php').subscribe(function (data) {
              return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this17, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee26() {
                return regeneratorRuntime.wrap(function _callee26$(_context26) {
                  while (1) {
                    switch (_context26.prev = _context26.next) {
                      case 0:
                        if (data.success) {
                          this.goBack();
                          this.alertCtrl.create({
                            header: 'Report Successfully Sent!',
                            message: '<b style="text-align: justify;font-weight:lighter">You have successfully reported <b style="color: #1dc1e6;font-weight: lighter;">' + this.reportedName + '</b>,' + 'it will take some time for our administrators to go through numerous user reports.' + ' Please bear with us, rest assured we will go through each one carefully and take the' + ' necessary actions to make your user experience in Greenthumb a pleasant one. We thank you' + ' for your patience. Please email screenshots of your report to Greenthumb@gmail.com</b>',
                            cssClass: 'foo',
                            buttons: [{
                              text: 'CLOSE',
                              role: 'cancel',
                              handler: function handler() {
                                console.log('Done reporting!');
                              }
                            }]
                          }).then(function (res) {
                            res.present();
                          });
                        }

                      case 1:
                      case "end":
                        return _context26.stop();
                    }
                  }
                }, _callee26, this);
              }));
            });
          });
        }
      }]);

      return ReportPage;
    }();

    ReportPage.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }, {
        type: _providers_credential_provider__WEBPACK_IMPORTED_MODULE_4__["PostProvider"]
      }, {
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ModalController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"]
      }];
    };

    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)], ReportPage.prototype, "reportedName", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)], ReportPage.prototype, "reportedId", void 0);
    ReportPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-report',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./report.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/report/report.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./report.page.scss */
      "./src/app/report/report.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _providers_credential_provider__WEBPACK_IMPORTED_MODULE_4__["PostProvider"], _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ModalController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"]])], ReportPage);
    /***/
  },

  /***/
  "./src/app/reportuser/reportuser-routing.module.ts":
  /*!*********************************************************!*\
    !*** ./src/app/reportuser/reportuser-routing.module.ts ***!
    \*********************************************************/

  /*! exports provided: ReportuserPageRoutingModule */

  /***/
  function srcAppReportuserReportuserRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ReportuserPageRoutingModule", function () {
      return ReportuserPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _reportuser_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./reportuser.page */
    "./src/app/reportuser/reportuser.page.ts");

    var routes = [{
      path: '',
      component: _reportuser_page__WEBPACK_IMPORTED_MODULE_3__["ReportuserPage"]
    }];

    var ReportuserPageRoutingModule = function ReportuserPageRoutingModule() {
      _classCallCheck(this, ReportuserPageRoutingModule);
    };

    ReportuserPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], ReportuserPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/reportuser/reportuser.module.ts":
  /*!*************************************************!*\
    !*** ./src/app/reportuser/reportuser.module.ts ***!
    \*************************************************/

  /*! exports provided: ReportuserPageModule */

  /***/
  function srcAppReportuserReportuserModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ReportuserPageModule", function () {
      return ReportuserPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _reportuser_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./reportuser-routing.module */
    "./src/app/reportuser/reportuser-routing.module.ts");
    /* harmony import */


    var _reportuser_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./reportuser.page */
    "./src/app/reportuser/reportuser.page.ts");

    var ReportuserPageModule = function ReportuserPageModule() {
      _classCallCheck(this, ReportuserPageModule);
    };

    ReportuserPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _reportuser_routing_module__WEBPACK_IMPORTED_MODULE_5__["ReportuserPageRoutingModule"]],
      declarations: [_reportuser_page__WEBPACK_IMPORTED_MODULE_6__["ReportuserPage"]]
    })], ReportuserPageModule);
    /***/
  },

  /***/
  "./src/app/reportuser/reportuser.page.scss":
  /*!*************************************************!*\
    !*** ./src/app/reportuser/reportuser.page.scss ***!
    \*************************************************/

  /*! exports provided: default */

  /***/
  function srcAppReportuserReportuserPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".line-input {\n  margin-bottom: 0 !important;\n}\n.line-input ion-item {\n  --border-color: transparent!important;\n  --highlight-height: 0;\n  border: 1px solid #dedede;\n  border-radius: 4px;\n  height: 100px;\n  margin-top: 4%;\n}\n.item_input {\n  font-size: 14px;\n  color: #424242 !important;\n}\n.item_label {\n  color: #b3aeae !important;\n  font-weight: 300;\n  font-size: 13px;\n}\n.for_itemspace {\n  margin-top: -10px;\n  --background: transparent;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZW5kcmljay9EZXNrdG9wL1RlbG1vIFNvbHV0aW9ucy9Gb3JQcm9kdWN0aW9uL21heTEwMjAyMS9sYXN0IHNldHVwL2dyZWVudGh1bWJhcHAgY29weS9zcmMvYXBwL3JlcG9ydHVzZXIvcmVwb3J0dXNlci5wYWdlLnNjc3MiLCJzcmMvYXBwL3JlcG9ydHVzZXIvcmVwb3J0dXNlci5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSwyQkFBQTtBQ0NKO0FEQUk7RUFDSSxxQ0FBQTtFQUNBLHFCQUFBO0VBQ0EseUJBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxjQUFBO0FDRVI7QURDQTtFQUNJLGVBQUE7RUFFQSx5QkFBQTtBQ0NKO0FEQ0E7RUFDSSx5QkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtBQ0VKO0FEQUE7RUFDSSxpQkFBQTtFQUNBLHlCQUFBO0FDR0oiLCJmaWxlIjoic3JjL2FwcC9yZXBvcnR1c2VyL3JlcG9ydHVzZXIucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmxpbmUtaW5wdXQge1xuICAgIG1hcmdpbi1ib3R0b206IDAhaW1wb3J0YW50O1xuICAgIGlvbi1pdGVtIHtcbiAgICAgICAgLS1ib3JkZXItY29sb3I6IHRyYW5zcGFyZW50IWltcG9ydGFudDtcbiAgICAgICAgLS1oaWdobGlnaHQtaGVpZ2h0OiAwO1xuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjZGVkZWRlO1xuICAgICAgICBib3JkZXItcmFkaXVzOiA0cHg7XG4gICAgICAgIGhlaWdodDogMTAwcHg7XG4gICAgICAgIG1hcmdpbi10b3A6IDQlO1xuICAgIH1cbn1cbi5pdGVtX2lucHV0e1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAvLyAtLXBhZGRpbmctdG9wOiAwO1xuICAgIGNvbG9yOiAjNDI0MjQyIWltcG9ydGFudDtcbn1cbi5pdGVtX2xhYmVse1xuICAgIGNvbG9yOiAjYjNhZWFlICFpbXBvcnRhbnQ7XG4gICAgZm9udC13ZWlnaHQ6IDMwMDtcbiAgICBmb250LXNpemU6IDEzcHg7XG59XG4uZm9yX2l0ZW1zcGFjZXtcbiAgICBtYXJnaW4tdG9wOiAtMTBweDtcbiAgICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xufSIsIi5saW5lLWlucHV0IHtcbiAgbWFyZ2luLWJvdHRvbTogMCAhaW1wb3J0YW50O1xufVxuLmxpbmUtaW5wdXQgaW9uLWl0ZW0ge1xuICAtLWJvcmRlci1jb2xvcjogdHJhbnNwYXJlbnQhaW1wb3J0YW50O1xuICAtLWhpZ2hsaWdodC1oZWlnaHQ6IDA7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNkZWRlZGU7XG4gIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgaGVpZ2h0OiAxMDBweDtcbiAgbWFyZ2luLXRvcDogNCU7XG59XG5cbi5pdGVtX2lucHV0IHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBjb2xvcjogIzQyNDI0MiAhaW1wb3J0YW50O1xufVxuXG4uaXRlbV9sYWJlbCB7XG4gIGNvbG9yOiAjYjNhZWFlICFpbXBvcnRhbnQ7XG4gIGZvbnQtd2VpZ2h0OiAzMDA7XG4gIGZvbnQtc2l6ZTogMTNweDtcbn1cblxuLmZvcl9pdGVtc3BhY2Uge1xuICBtYXJnaW4tdG9wOiAtMTBweDtcbiAgLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/reportuser/reportuser.page.ts":
  /*!***********************************************!*\
    !*** ./src/app/reportuser/reportuser.page.ts ***!
    \***********************************************/

  /*! exports provided: ReportuserPage */

  /***/
  function srcAppReportuserReportuserPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ReportuserPage", function () {
      return ReportuserPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var _providers_credential_provider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../providers/credential-provider */
    "./src/providers/credential-provider.ts");
    /* harmony import */


    var _shared_model_report_model__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../shared/model/report.model */
    "./src/app/shared/model/report.model.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");

    var ReportuserPage = /*#__PURE__*/function () {
      function ReportuserPage(router, storage, modalController, alertCtrl, postPvdr) {
        _classCallCheck(this, ReportuserPage);

        this.router = router;
        this.storage = storage;
        this.modalController = modalController;
        this.alertCtrl = alertCtrl;
        this.postPvdr = postPvdr;
        this.theReason = '';
        this.otherReason = '';
        this.others = false;
        this.reasonList = [];
      }

      _createClass(ReportuserPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.plotData();
          this.theReason = "Showing Inappropriate Behavior";
        }
      }, {
        key: "goBack",
        value: function goBack() {
          this.modalController.dismiss();
        }
      }, {
        key: "toHome",
        value: function toHome() {
          this.router.navigate(['tabs']);
        }
      }, {
        key: "othersClick",
        value: function othersClick() {
          this.others = !this.others;
          console.log("this.others:" + this.others);

          if (!this.others) {
            this.otherReason = "";
          }
        }
      }, {
        key: "changeReason",
        value: function changeReason(x) {
          this.theReason = x;
        }
      }, {
        key: "reportThisUser",
        value: function reportThisUser() {
          var _this18 = this;

          console.log("thereason:" + this.theReason);
          this.storage.get('greenthumb_user_id').then(function (user_id) {
            if (_this18.others && _this18.otherReason.length > 0) {
              _this18.theReason = _this18.theReason + "     -     " + _this18.otherReason;
            }

            var body2 = {
              action: 'reportThisUser',
              reported_user: _this18.reportedId,
              reported_by: user_id,
              report_reason: _this18.theReason
            };

            _this18.postPvdr.postData(body2, 'report.php').subscribe(function (data) {
              return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this18, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee27() {
                return regeneratorRuntime.wrap(function _callee27$(_context27) {
                  while (1) {
                    switch (_context27.prev = _context27.next) {
                      case 0:
                        if (data.success) {
                          this.goBack();
                          this.alertCtrl.create({
                            header: 'Report Successfully Sent!',
                            message: '<b style="text-align: justify;font-weight:lighter">You have successfully reported <b style="color: #1dc1e6;font-weight: lighter;">' + this.reportedName + '</b>,' + 'it will take some time for our administrators to go through numerous user reports.' + ' Please bear with us, rest assured we will go through each one carefully and take the' + ' necessary actions to make your user experience in Greenthumb a pleasant one. We thank you' + ' for your patience. Please email screenshots of your report to Greenthumb@gmail.com</b>',
                            cssClass: 'foo',
                            buttons: [{
                              text: 'CLOSE',
                              role: 'cancel',
                              handler: function handler() {
                                console.log('Done reporting!');
                              }
                            }]
                          }).then(function (res) {
                            res.present();
                          });
                        }

                      case 1:
                      case "end":
                        return _context27.stop();
                    }
                  }
                }, _callee27, this);
              }));
            });
          });
        }
      }, {
        key: "plotData",
        value: function plotData() {
          var _this19 = this;

          var body = {
            action: 'reportReasonUser'
          };
          this.postPvdr.postData(body, 'report.php').subscribe(function (data) {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this19, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee28() {
              var key;
              return regeneratorRuntime.wrap(function _callee28$(_context28) {
                while (1) {
                  switch (_context28.prev = _context28.next) {
                    case 0:
                      if (data.success) {
                        for (key in data.result) {
                          this.reasonList.push(new _shared_model_report_model__WEBPACK_IMPORTED_MODULE_5__["reportReason"](data.result[key].id, data.result[key].reason));
                        }
                      }

                    case 1:
                    case "end":
                      return _context28.stop();
                  }
                }
              }, _callee28, this);
            }));
          });
        }
      }]);

      return ReportuserPage;
    }();

    ReportuserPage.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }, {
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ModalController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"]
      }, {
        type: _providers_credential_provider__WEBPACK_IMPORTED_MODULE_4__["PostProvider"]
      }];
    };

    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)], ReportuserPage.prototype, "reportedName", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)], ReportuserPage.prototype, "reportedId", void 0);
    ReportuserPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-reportuser',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./reportuser.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/reportuser/reportuser.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./reportuser.page.scss */
      "./src/app/reportuser/reportuser.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ModalController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"], _providers_credential_provider__WEBPACK_IMPORTED_MODULE_4__["PostProvider"]])], ReportuserPage);
    /***/
  },

  /***/
  "./src/app/selectcategories/selectcategories-routing.module.ts":
  /*!*********************************************************************!*\
    !*** ./src/app/selectcategories/selectcategories-routing.module.ts ***!
    \*********************************************************************/

  /*! exports provided: SelectcategoriesPageRoutingModule */

  /***/
  function srcAppSelectcategoriesSelectcategoriesRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SelectcategoriesPageRoutingModule", function () {
      return SelectcategoriesPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _selectcategories_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./selectcategories.page */
    "./src/app/selectcategories/selectcategories.page.ts");

    var routes = [{
      path: '',
      component: _selectcategories_page__WEBPACK_IMPORTED_MODULE_3__["SelectcategoriesPage"]
    }];

    var SelectcategoriesPageRoutingModule = function SelectcategoriesPageRoutingModule() {
      _classCallCheck(this, SelectcategoriesPageRoutingModule);
    };

    SelectcategoriesPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], SelectcategoriesPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/selectcategories/selectcategories.module.ts":
  /*!*************************************************************!*\
    !*** ./src/app/selectcategories/selectcategories.module.ts ***!
    \*************************************************************/

  /*! exports provided: SelectcategoriesPageModule */

  /***/
  function srcAppSelectcategoriesSelectcategoriesModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SelectcategoriesPageModule", function () {
      return SelectcategoriesPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _selectcategories_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./selectcategories-routing.module */
    "./src/app/selectcategories/selectcategories-routing.module.ts");
    /* harmony import */


    var _selectcategories_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./selectcategories.page */
    "./src/app/selectcategories/selectcategories.page.ts");

    var SelectcategoriesPageModule = function SelectcategoriesPageModule() {
      _classCallCheck(this, SelectcategoriesPageModule);
    };

    SelectcategoriesPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _selectcategories_routing_module__WEBPACK_IMPORTED_MODULE_5__["SelectcategoriesPageRoutingModule"]],
      declarations: [_selectcategories_page__WEBPACK_IMPORTED_MODULE_6__["SelectcategoriesPage"]]
    })], SelectcategoriesPageModule);
    /***/
  },

  /***/
  "./src/app/selectcategories/selectcategories.page.scss":
  /*!*************************************************************!*\
    !*** ./src/app/selectcategories/selectcategories.page.scss ***!
    \*************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppSelectcategoriesSelectcategoriesPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".for_card {\n  margin: 0;\n  box-shadow: 1px 1px 3px #e2f0cb;\n  border-radius: 5px;\n}\n\n.div_card {\n  background: #e2f0cb;\n}\n\n.for_divicon {\n  zoom: 8;\n}\n\n.for_itemimg {\n  margin: auto;\n  width: 90%;\n  padding: 7px;\n  height: 110px;\n}\n\n.for_cardcontent {\n  font-size: 14px;\n  color: #679733;\n  padding: 7px;\n  text-align: center;\n  background: #e2f0cb38;\n  border-top: 1px solid #e2f0cb;\n}\n\n.grid-categories {\n  width: 50%;\n  float: left;\n  padding: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZW5kcmljay9EZXNrdG9wL1RlbG1vIFNvbHV0aW9ucy9Gb3JQcm9kdWN0aW9uL21heTEwMjAyMS9sYXN0IHNldHVwL2dyZWVudGh1bWJhcHAgY29weS9zcmMvYXBwL3NlbGVjdGNhdGVnb3JpZXMvc2VsZWN0Y2F0ZWdvcmllcy5wYWdlLnNjc3MiLCJzcmMvYXBwL3NlbGVjdGNhdGVnb3JpZXMvc2VsZWN0Y2F0ZWdvcmllcy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxTQUFBO0VBQ0EsK0JBQUE7RUFDQSxrQkFBQTtBQ0NKOztBRENBO0VBQ0ksbUJBQUE7QUNFSjs7QURBQTtFQUNJLE9BQUE7QUNHSjs7QUREQTtFQUNJLFlBQUE7RUFDQSxVQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7QUNJSjs7QURGQTtFQUNJLGVBQUE7RUFDQSxjQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EscUJBQUE7RUFDQSw2QkFBQTtBQ0tKOztBREhBO0VBQ0ksVUFBQTtFQUNBLFdBQUE7RUFDQSxVQUFBO0FDTUoiLCJmaWxlIjoic3JjL2FwcC9zZWxlY3RjYXRlZ29yaWVzL3NlbGVjdGNhdGVnb3JpZXMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmZvcl9jYXJke1xuICAgIG1hcmdpbjowO1xuICAgIGJveC1zaGFkb3c6IDFweCAxcHggM3B4ICNlMmYwY2I7XG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xufVxuLmRpdl9jYXJke1xuICAgIGJhY2tncm91bmQ6ICNlMmYwY2I7XG59XG4uZm9yX2Rpdmljb257XG4gICAgem9vbTogODtcbn1cbi5mb3JfaXRlbWltZ3tcbiAgICBtYXJnaW46IGF1dG87XG4gICAgd2lkdGg6IDkwJTtcbiAgICBwYWRkaW5nOiA3cHg7XG4gICAgaGVpZ2h0OiAxMTBweDtcbn1cbi5mb3JfY2FyZGNvbnRlbnR7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgIGNvbG9yOiAjNjc5NzMzO1xuICAgIHBhZGRpbmc6IDdweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgYmFja2dyb3VuZDogI2UyZjBjYjM4O1xuICAgIGJvcmRlci10b3A6IDFweCBzb2xpZCAjZTJmMGNiO1xufVxuLmdyaWQtY2F0ZWdvcmllcyB7XG4gICAgd2lkdGg6IDUwJTtcbiAgICBmbG9hdDogbGVmdDtcbiAgICBwYWRkaW5nOiAwO1xufSIsIi5mb3JfY2FyZCB7XG4gIG1hcmdpbjogMDtcbiAgYm94LXNoYWRvdzogMXB4IDFweCAzcHggI2UyZjBjYjtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xufVxuXG4uZGl2X2NhcmQge1xuICBiYWNrZ3JvdW5kOiAjZTJmMGNiO1xufVxuXG4uZm9yX2Rpdmljb24ge1xuICB6b29tOiA4O1xufVxuXG4uZm9yX2l0ZW1pbWcge1xuICBtYXJnaW46IGF1dG87XG4gIHdpZHRoOiA5MCU7XG4gIHBhZGRpbmc6IDdweDtcbiAgaGVpZ2h0OiAxMTBweDtcbn1cblxuLmZvcl9jYXJkY29udGVudCB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgY29sb3I6ICM2Nzk3MzM7XG4gIHBhZGRpbmc6IDdweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBiYWNrZ3JvdW5kOiAjZTJmMGNiMzg7XG4gIGJvcmRlci10b3A6IDFweCBzb2xpZCAjZTJmMGNiO1xufVxuXG4uZ3JpZC1jYXRlZ29yaWVzIHtcbiAgd2lkdGg6IDUwJTtcbiAgZmxvYXQ6IGxlZnQ7XG4gIHBhZGRpbmc6IDA7XG59Il19 */";
    /***/
  },

  /***/
  "./src/app/selectcategories/selectcategories.page.ts":
  /*!***********************************************************!*\
    !*** ./src/app/selectcategories/selectcategories.page.ts ***!
    \***********************************************************/

  /*! exports provided: SelectcategoriesPage */

  /***/
  function srcAppSelectcategoriesSelectcategoriesPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SelectcategoriesPage", function () {
      return SelectcategoriesPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var _providers_credential_provider__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../providers/credential-provider */
    "./src/providers/credential-provider.ts");
    /* harmony import */


    var _selectcategoriessub_selectcategoriessub_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../selectcategoriessub/selectcategoriessub.page */
    "./src/app/selectcategoriessub/selectcategoriessub.page.ts");
    /* harmony import */


    var _shared_model_category_model__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../shared/model/category.model */
    "./src/app/shared/model/category.model.ts");

    var SelectcategoriesPage = /*#__PURE__*/function () {
      function SelectcategoriesPage(router, modalCtrl, navCtrl, postPvdr, storage) {
        _classCallCheck(this, SelectcategoriesPage);

        this.router = router;
        this.modalCtrl = modalCtrl;
        this.navCtrl = navCtrl;
        this.postPvdr = postPvdr;
        this.storage = storage;
        this.segment = 0;
        this.categoryList = [];
      }

      _createClass(SelectcategoriesPage, [{
        key: "segmentChanged",
        value: function segmentChanged() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee29() {
            return regeneratorRuntime.wrap(function _callee29$(_context29) {
              while (1) {
                switch (_context29.prev = _context29.next) {
                  case 0:
                    _context29.next = 2;
                    return this.slider.slideTo(this.segment);

                  case 2:
                  case "end":
                    return _context29.stop();
                }
              }
            }, _callee29, this);
          }));
        }
      }, {
        key: "slideChanged",
        value: function slideChanged() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee30() {
            return regeneratorRuntime.wrap(function _callee30$(_context30) {
              while (1) {
                switch (_context30.prev = _context30.next) {
                  case 0:
                    _context30.next = 2;
                    return this.slider.getActiveIndex();

                  case 2:
                    this.segment = _context30.sent;

                  case 3:
                  case "end":
                    return _context30.stop();
                }
              }
            }, _callee30, this);
          }));
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "ionViewWillEnter",
        value: function ionViewWillEnter() {
          this.plotData();
        }
      }, {
        key: "plotData",
        value: function plotData() {
          var _this20 = this;

          this.storage.get('greenthumb_user_id').then(function (user_id) {
            _this20.login_user_id = user_id;
            var body321 = {
              action: 'getCategoriesMain',
              user_id: user_id
            }; //console.log("storyalang:"+JSON.stringify(body321));

            _this20.postPvdr.postData(body321, 'category.php').subscribe(function (data) {
              if (data.success) {
                var categoryList = [];

                for (var key in data.result) {
                  categoryList.push(new _shared_model_category_model__WEBPACK_IMPORTED_MODULE_7__["CategoryMain"](data.result[key].id, data.result[key].category_main, data.result[key].category_main_photo == '' ? '' : _this20.postPvdr.myServer() + "/greenthumb/images/categories/main/" + data.result[key].category_main_photo));
                }

                _this20.categoryList = categoryList;
              }
            });
          });
        }
      }, {
        key: "goBack",
        value: function goBack() {
          //this.router.navigate(['describeitem']);
          //window.history.back();
          console.log("hamon");
          this.modalCtrl.dismiss();
        }
      }, {
        key: "selected",
        value: function selected(x, val) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee31() {
            var _this21 = this;

            var data, modal;
            return regeneratorRuntime.wrap(function _callee31$(_context31) {
              while (1) {
                switch (_context31.prev = _context31.next) {
                  case 0:
                    data = {
                      id: x,
                      val: val
                    };
                    _context31.next = 3;
                    return this.modalCtrl.create({
                      component: _selectcategoriessub_selectcategoriessub_page__WEBPACK_IMPORTED_MODULE_6__["SelectcategoriessubPage"],
                      cssClass: 'categories',
                      id: 'modal2',
                      componentProps: {
                        data: data
                      }
                    });

                  case 3:
                    modal = _context31.sent;
                    modal.onDidDismiss().then(function (datafrom) {
                      // console.log("hamon2");
                      // console.log("iba:"+JSON.stringify(datafrom));  
                      // console.log("val:"+datafrom['data'].value);
                      var data2 = {
                        id: datafrom['data'].id,
                        value: datafrom['data'].value
                      }; //console.log("iba2dfd:"+JSON.stringify(data2)); 

                      _this21.modalCtrl.dismiss(data2, null, "myModal");
                    });
                    _context31.next = 7;
                    return modal.present();

                  case 7:
                    return _context31.abrupt("return", _context31.sent);

                  case 8:
                  case "end":
                    return _context31.stop();
                }
              }
            }, _callee31, this);
          }));
        }
      }]);

      return SelectcategoriesPage;
    }();

    SelectcategoriesPage.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"]
      }, {
        type: _providers_credential_provider__WEBPACK_IMPORTED_MODULE_5__["PostProvider"]
      }, {
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"]
      }];
    };

    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('slides', {
      "static": true
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonSlides"])], SelectcategoriesPage.prototype, "slider", void 0);
    SelectcategoriesPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-selectcategories',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./selectcategories.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/selectcategories/selectcategories.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./selectcategories.page.scss */
      "./src/app/selectcategories/selectcategories.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"], _providers_credential_provider__WEBPACK_IMPORTED_MODULE_5__["PostProvider"], _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"]])], SelectcategoriesPage);
    /***/
  },

  /***/
  "./src/app/selectcategoriessub/selectcategoriessub-routing.module.ts":
  /*!***************************************************************************!*\
    !*** ./src/app/selectcategoriessub/selectcategoriessub-routing.module.ts ***!
    \***************************************************************************/

  /*! exports provided: SelectcategoriessubPageRoutingModule */

  /***/
  function srcAppSelectcategoriessubSelectcategoriessubRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SelectcategoriessubPageRoutingModule", function () {
      return SelectcategoriessubPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _selectcategoriessub_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./selectcategoriessub.page */
    "./src/app/selectcategoriessub/selectcategoriessub.page.ts");

    var routes = [{
      path: '',
      component: _selectcategoriessub_page__WEBPACK_IMPORTED_MODULE_3__["SelectcategoriessubPage"]
    }];

    var SelectcategoriessubPageRoutingModule = function SelectcategoriessubPageRoutingModule() {
      _classCallCheck(this, SelectcategoriessubPageRoutingModule);
    };

    SelectcategoriessubPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], SelectcategoriessubPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/selectcategoriessub/selectcategoriessub.module.ts":
  /*!*******************************************************************!*\
    !*** ./src/app/selectcategoriessub/selectcategoriessub.module.ts ***!
    \*******************************************************************/

  /*! exports provided: SelectcategoriessubPageModule */

  /***/
  function srcAppSelectcategoriessubSelectcategoriessubModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SelectcategoriessubPageModule", function () {
      return SelectcategoriessubPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _selectcategoriessub_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./selectcategoriessub-routing.module */
    "./src/app/selectcategoriessub/selectcategoriessub-routing.module.ts");
    /* harmony import */


    var _selectcategoriessub_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./selectcategoriessub.page */
    "./src/app/selectcategoriessub/selectcategoriessub.page.ts");

    var SelectcategoriessubPageModule = function SelectcategoriessubPageModule() {
      _classCallCheck(this, SelectcategoriessubPageModule);
    };

    SelectcategoriessubPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _selectcategoriessub_routing_module__WEBPACK_IMPORTED_MODULE_5__["SelectcategoriessubPageRoutingModule"]],
      declarations: [_selectcategoriessub_page__WEBPACK_IMPORTED_MODULE_6__["SelectcategoriessubPage"]]
    })], SelectcategoriessubPageModule);
    /***/
  },

  /***/
  "./src/app/selectcategoriessub/selectcategoriessub.page.scss":
  /*!*******************************************************************!*\
    !*** ./src/app/selectcategoriessub/selectcategoriessub.page.scss ***!
    \*******************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppSelectcategoriessubSelectcategoriessubPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".for_card {\n  margin: 0;\n  box-shadow: 1px 1px 3px #e2f0cb;\n  border-radius: 5px;\n}\n\n.div_card {\n  background: #e2f0cb;\n}\n\n.for_itemimg {\n  margin: auto;\n  width: 90%;\n  padding: 7px;\n  height: 110px;\n}\n\n.for_divicon {\n  zoom: 8;\n}\n\n.for_cardcontent {\n  font-size: 14px;\n  color: #679733;\n  padding: 7px;\n  text-align: center;\n  background: #e2f0cb38;\n  border-top: 1px solid #e2f0cb;\n}\n\n.grid-categories {\n  width: 50%;\n  float: left;\n  padding: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZW5kcmljay9EZXNrdG9wL1RlbG1vIFNvbHV0aW9ucy9Gb3JQcm9kdWN0aW9uL21heTEwMjAyMS9sYXN0IHNldHVwL2dyZWVudGh1bWJhcHAgY29weS9zcmMvYXBwL3NlbGVjdGNhdGVnb3JpZXNzdWIvc2VsZWN0Y2F0ZWdvcmllc3N1Yi5wYWdlLnNjc3MiLCJzcmMvYXBwL3NlbGVjdGNhdGVnb3JpZXNzdWIvc2VsZWN0Y2F0ZWdvcmllc3N1Yi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxTQUFBO0VBQ0EsK0JBQUE7RUFDQSxrQkFBQTtBQ0NKOztBRENBO0VBQ0ksbUJBQUE7QUNFSjs7QURBQTtFQUNJLFlBQUE7RUFDQSxVQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7QUNHSjs7QUREQTtFQUNJLE9BQUE7QUNJSjs7QURGQTtFQUNJLGVBQUE7RUFDQSxjQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EscUJBQUE7RUFDQSw2QkFBQTtBQ0tKOztBREhBO0VBQ0ksVUFBQTtFQUNBLFdBQUE7RUFDQSxVQUFBO0FDTUoiLCJmaWxlIjoic3JjL2FwcC9zZWxlY3RjYXRlZ29yaWVzc3ViL3NlbGVjdGNhdGVnb3JpZXNzdWIucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmZvcl9jYXJke1xuICAgIG1hcmdpbjowO1xuICAgIGJveC1zaGFkb3c6IDFweCAxcHggM3B4ICNlMmYwY2I7XG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xufVxuLmRpdl9jYXJke1xuICAgIGJhY2tncm91bmQ6ICNlMmYwY2I7XG59XG4uZm9yX2l0ZW1pbWd7XG4gICAgbWFyZ2luOiBhdXRvO1xuICAgIHdpZHRoOiA5MCU7XG4gICAgcGFkZGluZzogN3B4O1xuICAgIGhlaWdodDogMTEwcHg7XG59XG4uZm9yX2Rpdmljb257XG4gICAgem9vbTogODtcbn1cbi5mb3JfY2FyZGNvbnRlbnR7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgIGNvbG9yOiAjNjc5NzMzO1xuICAgIHBhZGRpbmc6IDdweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgYmFja2dyb3VuZDogI2UyZjBjYjM4O1xuICAgIGJvcmRlci10b3A6IDFweCBzb2xpZCAjZTJmMGNiO1xufVxuLmdyaWQtY2F0ZWdvcmllcyB7XG4gICAgd2lkdGg6IDUwJTtcbiAgICBmbG9hdDogbGVmdDtcbiAgICBwYWRkaW5nOiAwO1xufSIsIi5mb3JfY2FyZCB7XG4gIG1hcmdpbjogMDtcbiAgYm94LXNoYWRvdzogMXB4IDFweCAzcHggI2UyZjBjYjtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xufVxuXG4uZGl2X2NhcmQge1xuICBiYWNrZ3JvdW5kOiAjZTJmMGNiO1xufVxuXG4uZm9yX2l0ZW1pbWcge1xuICBtYXJnaW46IGF1dG87XG4gIHdpZHRoOiA5MCU7XG4gIHBhZGRpbmc6IDdweDtcbiAgaGVpZ2h0OiAxMTBweDtcbn1cblxuLmZvcl9kaXZpY29uIHtcbiAgem9vbTogODtcbn1cblxuLmZvcl9jYXJkY29udGVudCB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgY29sb3I6ICM2Nzk3MzM7XG4gIHBhZGRpbmc6IDdweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBiYWNrZ3JvdW5kOiAjZTJmMGNiMzg7XG4gIGJvcmRlci10b3A6IDFweCBzb2xpZCAjZTJmMGNiO1xufVxuXG4uZ3JpZC1jYXRlZ29yaWVzIHtcbiAgd2lkdGg6IDUwJTtcbiAgZmxvYXQ6IGxlZnQ7XG4gIHBhZGRpbmc6IDA7XG59Il19 */";
    /***/
  },

  /***/
  "./src/app/selectcategoriessub/selectcategoriessub.page.ts":
  /*!*****************************************************************!*\
    !*** ./src/app/selectcategoriessub/selectcategoriessub.page.ts ***!
    \*****************************************************************/

  /*! exports provided: SelectcategoriessubPage */

  /***/
  function srcAppSelectcategoriessubSelectcategoriessubPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SelectcategoriessubPage", function () {
      return SelectcategoriessubPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var _providers_credential_provider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../providers/credential-provider */
    "./src/providers/credential-provider.ts");
    /* harmony import */


    var _shared_model_category_model__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../shared/model/category.model */
    "./src/app/shared/model/category.model.ts");

    var SelectcategoriessubPage = /*#__PURE__*/function () {
      function SelectcategoriessubPage(navParams, storage, modalCtrl, postPvdr) {
        _classCallCheck(this, SelectcategoriessubPage);

        this.storage = storage;
        this.modalCtrl = modalCtrl;
        this.postPvdr = postPvdr;
        this.categoryList = [];
        this.data = navParams.get('data');
        this.cat_main_id = this.data['id'];
        console.log("data in sub:" + this.data['id']);
      }

      _createClass(SelectcategoriessubPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.plotData();
        }
      }, {
        key: "selected",
        value: function selected(x, val) {
          var data = {
            id: x,
            value: val
          };
          console.log("dataklaro:" + JSON.stringify(data));
          this.modalCtrl.dismiss(data, null, 'modal2');
        }
      }, {
        key: "plotData",
        value: function plotData() {
          var _this22 = this;

          this.storage.get('greenthumb_user_id').then(function (user_id) {
            _this22.login_user_id = user_id;
            var body321 = {
              action: 'getCategoriesSub',
              cat_main_id: _this22.cat_main_id
            };
            console.log("storyalangtulong:" + JSON.stringify(body321));

            _this22.postPvdr.postData(body321, 'category.php').subscribe(function (data) {
              if (data.success) {
                var categoryList = [];

                for (var key in data.result) {
                  categoryList.push(new _shared_model_category_model__WEBPACK_IMPORTED_MODULE_5__["CategorySub"](data.result[key].category_id, data.result[key].main_category_id, data.result[key].category, data.result[key].category_photo == '' ? '' : _this22.postPvdr.myServer() + "/greenthumb/images/categories/sub/" + data.result[key].category_photo));
                }

                _this22.categoryList = categoryList;
              }
            });
          });
        }
      }, {
        key: "goBack",
        value: function goBack() {
          //this.router.navigate(['describeitem']);
          //window.history.back();
          this.modalCtrl.dismiss();
        }
      }]);

      return SelectcategoriessubPage;
    }();

    SelectcategoriessubPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavParams"]
      }, {
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
      }, {
        type: _providers_credential_provider__WEBPACK_IMPORTED_MODULE_4__["PostProvider"]
      }];
    };

    SelectcategoriessubPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-selectcategoriessub',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./selectcategoriessub.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/selectcategoriessub/selectcategoriessub.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./selectcategoriessub.page.scss */
      "./src/app/selectcategoriessub/selectcategoriessub.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavParams"], _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"], _providers_credential_provider__WEBPACK_IMPORTED_MODULE_4__["PostProvider"]])], SelectcategoriessubPage);
    /***/
  },

  /***/
  "./src/app/sendgiftbronzemodal/sendgiftbronzemodal.module.ts":
  /*!*******************************************************************!*\
    !*** ./src/app/sendgiftbronzemodal/sendgiftbronzemodal.module.ts ***!
    \*******************************************************************/

  /*! exports provided: SendgiftbronzemodalPageModule */

  /***/
  function srcAppSendgiftbronzemodalSendgiftbronzemodalModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SendgiftbronzemodalPageModule", function () {
      return SendgiftbronzemodalPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _sendgiftbronzemodal_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./sendgiftbronzemodal.page */
    "./src/app/sendgiftbronzemodal/sendgiftbronzemodal.page.ts");

    var routes = [{
      path: '',
      component: _sendgiftbronzemodal_page__WEBPACK_IMPORTED_MODULE_6__["SendgiftbronzemodalPage"]
    }];

    var SendgiftbronzemodalPageModule = function SendgiftbronzemodalPageModule() {
      _classCallCheck(this, SendgiftbronzemodalPageModule);
    };

    SendgiftbronzemodalPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      declarations: [_sendgiftbronzemodal_page__WEBPACK_IMPORTED_MODULE_6__["SendgiftbronzemodalPage"]]
    })], SendgiftbronzemodalPageModule);
    /***/
  },

  /***/
  "./src/app/sendgiftbronzemodal/sendgiftbronzemodal.page.scss":
  /*!*******************************************************************!*\
    !*** ./src/app/sendgiftbronzemodal/sendgiftbronzemodal.page.scss ***!
    \*******************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppSendgiftbronzemodalSendgiftbronzemodalPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NlbmRnaWZ0YnJvbnplbW9kYWwvc2VuZGdpZnRicm9uemVtb2RhbC5wYWdlLnNjc3MifQ== */";
    /***/
  },

  /***/
  "./src/app/sendgiftbronzemodal/sendgiftbronzemodal.page.ts":
  /*!*****************************************************************!*\
    !*** ./src/app/sendgiftbronzemodal/sendgiftbronzemodal.page.ts ***!
    \*****************************************************************/

  /*! exports provided: SendgiftbronzemodalPage */

  /***/
  function srcAppSendgiftbronzemodalSendgiftbronzemodalPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SendgiftbronzemodalPage", function () {
      return SendgiftbronzemodalPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var howler__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! howler */
    "./node_modules/howler/dist/howler.js");
    /* harmony import */


    var howler__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(howler__WEBPACK_IMPORTED_MODULE_3__);
    /* harmony import */


    var src_providers_credential_provider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! src/providers/credential-provider */
    "./src/providers/credential-provider.ts");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");

    var SendgiftbronzemodalPage = /*#__PURE__*/function () {
      function SendgiftbronzemodalPage(navParams, modalCtrl, storage, toastController, postPvdr) {
        _classCallCheck(this, SendgiftbronzemodalPage);

        this.modalCtrl = modalCtrl;
        this.storage = storage;
        this.toastController = toastController;
        this.postPvdr = postPvdr;
        this.player = null;
        this.bronze = navParams.get('bronze');
        this.live_user_id = navParams.get('live_user_id');
      }

      _createClass(SendgiftbronzemodalPage, [{
        key: "closeModal",
        value: function closeModal() {
          this.modalCtrl.dismiss();
          howler__WEBPACK_IMPORTED_MODULE_3__["Howler"].volume(0.0);
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "ionViewWillLeave",
        value: function ionViewWillLeave() {
          howler__WEBPACK_IMPORTED_MODULE_3__["Howler"].volume(0.0);
        }
      }, {
        key: "sendGift",
        value: function sendGift() {
          var _this23 = this;

          this.storage.get("user_id").then(function (user_id) {
            var body = {
              action: 'checkBalance',
              user_id: user_id
            }; //console.log("sendGifts:"+JSON.stringify(body2));

            _this23.postPvdr.postData(body, 'brixy-store.php').subscribe(function (data) {
              if (data.success) {
                if (data.result.gold_bar >= _this23.bronze.price) {
                  var body2 = {
                    action: 'sendGifts',
                    user_id: user_id,
                    live_user_id: _this23.live_user_id,
                    economy_id: _this23.bronze.id,
                    amount: _this23.bronze.price
                  }; //console.log("sendGifts:"+JSON.stringify(body2));

                  _this23.postPvdr.postData(body2, 'brixy-store.php').subscribe(function (data) {
                    if (data.success) {
                      _this23.modalCtrl.dismiss();

                      _this23.presentToast('Sent gift successfully.');
                    }
                  });
                } else {
                  _this23.presentToast('Insufficient balance of gold bars.');
                }
              } else {
                _this23.modalCtrl.dismiss();

                _this23.presentToast('Error.');
              }
            });
          });
        }
      }, {
        key: "presentToast",
        value: function presentToast(toastMessage) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee32() {
            var toast;
            return regeneratorRuntime.wrap(function _callee32$(_context32) {
              while (1) {
                switch (_context32.prev = _context32.next) {
                  case 0:
                    _context32.next = 2;
                    return this.toastController.create({
                      message: toastMessage,
                      duration: 3000
                    });

                  case 2:
                    toast = _context32.sent;
                    toast.present();

                  case 4:
                  case "end":
                    return _context32.stop();
                }
              }
            }, _callee32, this);
          }));
        }
      }]);

      return SendgiftbronzemodalPage;
    }();

    SendgiftbronzemodalPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavParams"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
      }, {
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]
      }, {
        type: src_providers_credential_provider__WEBPACK_IMPORTED_MODULE_4__["PostProvider"]
      }];
    };

    SendgiftbronzemodalPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-sendgiftbronzemodal',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./sendgiftbronzemodal.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/sendgiftbronzemodal/sendgiftbronzemodal.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./sendgiftbronzemodal.page.scss */
      "./src/app/sendgiftbronzemodal/sendgiftbronzemodal.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavParams"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"], _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"], src_providers_credential_provider__WEBPACK_IMPORTED_MODULE_4__["PostProvider"]])], SendgiftbronzemodalPage);
    /***/
  },

  /***/
  "./src/app/sendgiftgoldmodal/sendgiftgoldmodal.module.ts":
  /*!***************************************************************!*\
    !*** ./src/app/sendgiftgoldmodal/sendgiftgoldmodal.module.ts ***!
    \***************************************************************/

  /*! exports provided: SendgiftgoldmodalPageModule */

  /***/
  function srcAppSendgiftgoldmodalSendgiftgoldmodalModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SendgiftgoldmodalPageModule", function () {
      return SendgiftgoldmodalPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _sendgiftgoldmodal_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./sendgiftgoldmodal.page */
    "./src/app/sendgiftgoldmodal/sendgiftgoldmodal.page.ts");

    var routes = [{
      path: '',
      component: _sendgiftgoldmodal_page__WEBPACK_IMPORTED_MODULE_6__["SendgiftgoldmodalPage"]
    }];

    var SendgiftgoldmodalPageModule = function SendgiftgoldmodalPageModule() {
      _classCallCheck(this, SendgiftgoldmodalPageModule);
    };

    SendgiftgoldmodalPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      declarations: [_sendgiftgoldmodal_page__WEBPACK_IMPORTED_MODULE_6__["SendgiftgoldmodalPage"]]
    })], SendgiftgoldmodalPageModule);
    /***/
  },

  /***/
  "./src/app/sendgiftgoldmodal/sendgiftgoldmodal.page.scss":
  /*!***************************************************************!*\
    !*** ./src/app/sendgiftgoldmodal/sendgiftgoldmodal.page.scss ***!
    \***************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppSendgiftgoldmodalSendgiftgoldmodalPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NlbmRnaWZ0Z29sZG1vZGFsL3NlbmRnaWZ0Z29sZG1vZGFsLnBhZ2Uuc2NzcyJ9 */";
    /***/
  },

  /***/
  "./src/app/sendgiftgoldmodal/sendgiftgoldmodal.page.ts":
  /*!*************************************************************!*\
    !*** ./src/app/sendgiftgoldmodal/sendgiftgoldmodal.page.ts ***!
    \*************************************************************/

  /*! exports provided: SendgiftgoldmodalPage */

  /***/
  function srcAppSendgiftgoldmodalSendgiftgoldmodalPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SendgiftgoldmodalPage", function () {
      return SendgiftgoldmodalPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var howler__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! howler */
    "./node_modules/howler/dist/howler.js");
    /* harmony import */


    var howler__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(howler__WEBPACK_IMPORTED_MODULE_3__);

    var SendgiftgoldmodalPage = /*#__PURE__*/function () {
      function SendgiftgoldmodalPage(navParams, modalCtrl) {
        _classCallCheck(this, SendgiftgoldmodalPage);

        this.modalCtrl = modalCtrl;
        this.player = null;
        this.gold = navParams.get('gold');
      }

      _createClass(SendgiftgoldmodalPage, [{
        key: "closeModal",
        value: function closeModal() {
          this.modalCtrl.dismiss();
          howler__WEBPACK_IMPORTED_MODULE_3__["Howler"].volume(0.0);
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "ionViewWillLeave",
        value: function ionViewWillLeave() {
          howler__WEBPACK_IMPORTED_MODULE_3__["Howler"].volume(0.0);
        }
      }]);

      return SendgiftgoldmodalPage;
    }();

    SendgiftgoldmodalPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavParams"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
      }];
    };

    SendgiftgoldmodalPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-sendgiftgoldmodal',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./sendgiftgoldmodal.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/sendgiftgoldmodal/sendgiftgoldmodal.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./sendgiftgoldmodal.page.scss */
      "./src/app/sendgiftgoldmodal/sendgiftgoldmodal.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavParams"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]])], SendgiftgoldmodalPage);
    /***/
  },

  /***/
  "./src/app/sendgiftpopularmodal/sendgiftpopularmodal.module.ts":
  /*!*********************************************************************!*\
    !*** ./src/app/sendgiftpopularmodal/sendgiftpopularmodal.module.ts ***!
    \*********************************************************************/

  /*! exports provided: SendgiftpopularmodalPageModule */

  /***/
  function srcAppSendgiftpopularmodalSendgiftpopularmodalModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SendgiftpopularmodalPageModule", function () {
      return SendgiftpopularmodalPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _sendgiftpopularmodal_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./sendgiftpopularmodal.page */
    "./src/app/sendgiftpopularmodal/sendgiftpopularmodal.page.ts");

    var routes = [{
      path: '',
      component: _sendgiftpopularmodal_page__WEBPACK_IMPORTED_MODULE_6__["SendgiftpopularmodalPage"]
    }];

    var SendgiftpopularmodalPageModule = function SendgiftpopularmodalPageModule() {
      _classCallCheck(this, SendgiftpopularmodalPageModule);
    };

    SendgiftpopularmodalPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      declarations: [_sendgiftpopularmodal_page__WEBPACK_IMPORTED_MODULE_6__["SendgiftpopularmodalPage"]]
    })], SendgiftpopularmodalPageModule);
    /***/
  },

  /***/
  "./src/app/sendgiftpopularmodal/sendgiftpopularmodal.page.scss":
  /*!*********************************************************************!*\
    !*** ./src/app/sendgiftpopularmodal/sendgiftpopularmodal.page.scss ***!
    \*********************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppSendgiftpopularmodalSendgiftpopularmodalPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NlbmRnaWZ0cG9wdWxhcm1vZGFsL3NlbmRnaWZ0cG9wdWxhcm1vZGFsLnBhZ2Uuc2NzcyJ9 */";
    /***/
  },

  /***/
  "./src/app/sendgiftpopularmodal/sendgiftpopularmodal.page.ts":
  /*!*******************************************************************!*\
    !*** ./src/app/sendgiftpopularmodal/sendgiftpopularmodal.page.ts ***!
    \*******************************************************************/

  /*! exports provided: SendgiftpopularmodalPage */

  /***/
  function srcAppSendgiftpopularmodalSendgiftpopularmodalPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SendgiftpopularmodalPage", function () {
      return SendgiftpopularmodalPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var howler__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! howler */
    "./node_modules/howler/dist/howler.js");
    /* harmony import */


    var howler__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(howler__WEBPACK_IMPORTED_MODULE_3__);

    var SendgiftpopularmodalPage = /*#__PURE__*/function () {
      function SendgiftpopularmodalPage(navParams, modalCtrl) {
        _classCallCheck(this, SendgiftpopularmodalPage);

        this.modalCtrl = modalCtrl;
        this.player = null;
        this.popular = navParams.get('popular');
      }

      _createClass(SendgiftpopularmodalPage, [{
        key: "closeModal",
        value: function closeModal() {
          this.modalCtrl.dismiss();
          howler__WEBPACK_IMPORTED_MODULE_3__["Howler"].volume(0.0);
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "ionViewWillLeave",
        value: function ionViewWillLeave() {
          howler__WEBPACK_IMPORTED_MODULE_3__["Howler"].volume(0.0);
        }
      }]);

      return SendgiftpopularmodalPage;
    }();

    SendgiftpopularmodalPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavParams"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
      }];
    };

    SendgiftpopularmodalPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-sendgiftpopularmodal',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./sendgiftpopularmodal.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/sendgiftpopularmodal/sendgiftpopularmodal.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./sendgiftpopularmodal.page.scss */
      "./src/app/sendgiftpopularmodal/sendgiftpopularmodal.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavParams"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]])], SendgiftpopularmodalPage);
    /***/
  },

  /***/
  "./src/app/sendgiftsilvermodal/sendgiftsilvermodal.module.ts":
  /*!*******************************************************************!*\
    !*** ./src/app/sendgiftsilvermodal/sendgiftsilvermodal.module.ts ***!
    \*******************************************************************/

  /*! exports provided: SendgiftsilvermodalPageModule */

  /***/
  function srcAppSendgiftsilvermodalSendgiftsilvermodalModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SendgiftsilvermodalPageModule", function () {
      return SendgiftsilvermodalPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _sendgiftsilvermodal_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./sendgiftsilvermodal.page */
    "./src/app/sendgiftsilvermodal/sendgiftsilvermodal.page.ts");

    var routes = [{
      path: '',
      component: _sendgiftsilvermodal_page__WEBPACK_IMPORTED_MODULE_6__["SendgiftsilvermodalPage"]
    }];

    var SendgiftsilvermodalPageModule = function SendgiftsilvermodalPageModule() {
      _classCallCheck(this, SendgiftsilvermodalPageModule);
    };

    SendgiftsilvermodalPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      declarations: [_sendgiftsilvermodal_page__WEBPACK_IMPORTED_MODULE_6__["SendgiftsilvermodalPage"]]
    })], SendgiftsilvermodalPageModule);
    /***/
  },

  /***/
  "./src/app/sendgiftsilvermodal/sendgiftsilvermodal.page.scss":
  /*!*******************************************************************!*\
    !*** ./src/app/sendgiftsilvermodal/sendgiftsilvermodal.page.scss ***!
    \*******************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppSendgiftsilvermodalSendgiftsilvermodalPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".lvlicon {\n  width: 25px !important;\n  height: 25px;\n  margin: auto;\n  margin-bottom: -2%;\n  margin-right: 3%;\n}\n\n.lvlicon2 {\n  width: 20px !important;\n  height: 20px;\n  margin: auto;\n  margin-bottom: -2%;\n  margin-right: 3%;\n}\n\n.lvltext {\n  text-transform: capitalize;\n  font-size: 12px;\n  font-family: arial;\n}\n\n.lvlnum {\n  margin: 0;\n  margin-top: 4%;\n  font-weight: bolder;\n  font-family: Verdana, Geneva, Tahoma, sans-serif;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZW5kcmljay9EZXNrdG9wL1RlbG1vIFNvbHV0aW9ucy9Gb3JQcm9kdWN0aW9uL21heTEwMjAyMS9sYXN0IHNldHVwL2dyZWVudGh1bWJhcHAgY29weS9zcmMvYXBwL3NlbmRnaWZ0c2lsdmVybW9kYWwvc2VuZGdpZnRzaWx2ZXJtb2RhbC5wYWdlLnNjc3MiLCJzcmMvYXBwL3NlbmRnaWZ0c2lsdmVybW9kYWwvc2VuZGdpZnRzaWx2ZXJtb2RhbC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxzQkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtBQ0NKOztBRENFO0VBQ0Usc0JBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7QUNFSjs7QURBRTtFQUNFLDBCQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0FDR0o7O0FEREU7RUFDRSxTQUFBO0VBQ0EsY0FBQTtFQUNBLG1CQUFBO0VBQ0EsZ0RBQUE7QUNJSiIsImZpbGUiOiJzcmMvYXBwL3NlbmRnaWZ0c2lsdmVybW9kYWwvc2VuZGdpZnRzaWx2ZXJtb2RhbC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubHZsaWNvbntcbiAgICB3aWR0aDogMjVweCAhaW1wb3J0YW50OyBcbiAgICBoZWlnaHQ6IDI1cHg7IFxuICAgIG1hcmdpbjogYXV0bztcbiAgICBtYXJnaW4tYm90dG9tOiAtMiU7XG4gICAgbWFyZ2luLXJpZ2h0OiAzJTtcbiAgfVxuICAubHZsaWNvbjJ7XG4gICAgd2lkdGg6IDIwcHggIWltcG9ydGFudDtcbiAgICBoZWlnaHQ6IDIwcHg7XG4gICAgbWFyZ2luOiBhdXRvO1xuICAgIG1hcmdpbi1ib3R0b206IC0yJTtcbiAgICBtYXJnaW4tcmlnaHQ6IDMlO1xuICB9XG4gIC5sdmx0ZXh0e1xuICAgIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICBmb250LWZhbWlseTogYXJpYWw7XG4gIH1cbiAgLmx2bG51bXtcbiAgICBtYXJnaW46IDA7XG4gICAgbWFyZ2luLXRvcDogNCU7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbiAgICBmb250LWZhbWlseTogVmVyZGFuYSwgR2VuZXZhLCBUYWhvbWEsIHNhbnMtc2VyaWY7XG4gIH0iLCIubHZsaWNvbiB7XG4gIHdpZHRoOiAyNXB4ICFpbXBvcnRhbnQ7XG4gIGhlaWdodDogMjVweDtcbiAgbWFyZ2luOiBhdXRvO1xuICBtYXJnaW4tYm90dG9tOiAtMiU7XG4gIG1hcmdpbi1yaWdodDogMyU7XG59XG5cbi5sdmxpY29uMiB7XG4gIHdpZHRoOiAyMHB4ICFpbXBvcnRhbnQ7XG4gIGhlaWdodDogMjBweDtcbiAgbWFyZ2luOiBhdXRvO1xuICBtYXJnaW4tYm90dG9tOiAtMiU7XG4gIG1hcmdpbi1yaWdodDogMyU7XG59XG5cbi5sdmx0ZXh0IHtcbiAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgZm9udC1mYW1pbHk6IGFyaWFsO1xufVxuXG4ubHZsbnVtIHtcbiAgbWFyZ2luOiAwO1xuICBtYXJnaW4tdG9wOiA0JTtcbiAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbiAgZm9udC1mYW1pbHk6IFZlcmRhbmEsIEdlbmV2YSwgVGFob21hLCBzYW5zLXNlcmlmO1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/sendgiftsilvermodal/sendgiftsilvermodal.page.ts":
  /*!*****************************************************************!*\
    !*** ./src/app/sendgiftsilvermodal/sendgiftsilvermodal.page.ts ***!
    \*****************************************************************/

  /*! exports provided: SendgiftsilvermodalPage */

  /***/
  function srcAppSendgiftsilvermodalSendgiftsilvermodalPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SendgiftsilvermodalPage", function () {
      return SendgiftsilvermodalPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var howler__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! howler */
    "./node_modules/howler/dist/howler.js");
    /* harmony import */


    var howler__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(howler__WEBPACK_IMPORTED_MODULE_3__);
    /* harmony import */


    var src_providers_credential_provider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! src/providers/credential-provider */
    "./src/providers/credential-provider.ts");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");

    var SendgiftsilvermodalPage = /*#__PURE__*/function () {
      function SendgiftsilvermodalPage(navParams, modalCtrl, storage, toastController, postPvdr) {
        _classCallCheck(this, SendgiftsilvermodalPage);

        this.modalCtrl = modalCtrl;
        this.storage = storage;
        this.toastController = toastController;
        this.postPvdr = postPvdr;
        this.player = null;
        this.giftQuantity = '1';
        this.user_experience = 0;
        this.user_coins = 0;
        this.silver = navParams.get('silver');
        this.live_user_id = navParams.get('live_user_id');
      }

      _createClass(SendgiftsilvermodalPage, [{
        key: "closeModal",
        value: function closeModal() {
          this.modalCtrl.dismiss();
          howler__WEBPACK_IMPORTED_MODULE_3__["Howler"].volume(0.0);
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {
          this.showCoinsandGold();
        }
      }, {
        key: "showCoinsandGold",
        value: function showCoinsandGold() {
          var _this24 = this;

          this.storage.get("user_id").then(function (user_id) {
            var body = {
              action: 'getGold_bar',
              user_id: user_id
            };

            _this24.postPvdr.postData(body, 'brixy-store.php').subscribe(function (data) {
              if (data.success) {
                for (var key in data.result) {
                  _this24.user_experience = data.result[key].user_experience;
                  _this24.user_coins = data.result[key].user_coins;
                }
              }
            });
          });
        }
      }, {
        key: "ionViewWillLeave",
        value: function ionViewWillLeave() {
          howler__WEBPACK_IMPORTED_MODULE_3__["Howler"].volume(0.0);
        }
      }, {
        key: "sendGift",
        value: function sendGift() {
          var _this25 = this;

          this.storage.get("user_id").then(function (user_id) {
            var body = {
              action: 'checkBalance',
              user_id: user_id
            };

            _this25.postPvdr.postData(body, 'brixy-store.php').subscribe(function (data) {
              if (data.success) {
                if (parseInt(data.user_coins) >= parseInt(_this25.silver.price) * parseInt(_this25.giftQuantity)) {
                  var body2 = {
                    action: 'sendGifts',
                    user_id: user_id,
                    giftQuantity: parseInt(_this25.giftQuantity),
                    live_user_id: _this25.live_user_id,
                    economy_id: _this25.silver.id,
                    amount: _this25.silver.price
                  }; //console.log("sendGifts:"+JSON.stringify(body2));

                  _this25.postPvdr.postData(body2, 'brixy-store.php').subscribe(function (data) {
                    if (data.success) {
                      _this25.modalCtrl.dismiss();

                      _this25.presentToast('Sent gift successfully.');
                    }
                  });
                } else {
                  _this25.presentToast('Insufficient balance of coins.');
                }
              } else {
                _this25.modalCtrl.dismiss();

                _this25.presentToast('Error.');
              }
            });
          });
        }
      }, {
        key: "presentToast",
        value: function presentToast(toastMessage) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee33() {
            var toast;
            return regeneratorRuntime.wrap(function _callee33$(_context33) {
              while (1) {
                switch (_context33.prev = _context33.next) {
                  case 0:
                    _context33.next = 2;
                    return this.toastController.create({
                      message: toastMessage,
                      duration: 3000
                    });

                  case 2:
                    toast = _context33.sent;
                    toast.present();

                  case 4:
                  case "end":
                    return _context33.stop();
                }
              }
            }, _callee33, this);
          }));
        }
      }]);

      return SendgiftsilvermodalPage;
    }();

    SendgiftsilvermodalPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavParams"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
      }, {
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]
      }, {
        type: src_providers_credential_provider__WEBPACK_IMPORTED_MODULE_4__["PostProvider"]
      }];
    };

    SendgiftsilvermodalPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-sendgiftsilvermodal',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./sendgiftsilvermodal.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/sendgiftsilvermodal/sendgiftsilvermodal.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./sendgiftsilvermodal.page.scss */
      "./src/app/sendgiftsilvermodal/sendgiftsilvermodal.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavParams"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"], _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"], src_providers_credential_provider__WEBPACK_IMPORTED_MODULE_4__["PostProvider"]])], SendgiftsilvermodalPage);
    /***/
  },

  /***/
  "./src/app/shared/model/category.model.ts":
  /*!************************************************!*\
    !*** ./src/app/shared/model/category.model.ts ***!
    \************************************************/

  /*! exports provided: CategoryMain, CategorySub */

  /***/
  function srcAppSharedModelCategoryModelTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CategoryMain", function () {
      return CategoryMain;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CategorySub", function () {
      return CategorySub;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");

    var CategoryMain = function CategoryMain(id, category, category_photo) {
      _classCallCheck(this, CategoryMain);

      this.id = id;
      this.category = category;
      this.category_photo = category_photo;
    };

    var CategorySub = function CategorySub(id, cat_main_id, category, category_photo) {
      _classCallCheck(this, CategorySub);

      this.id = id;
      this.cat_main_id = cat_main_id;
      this.category = category;
      this.category_photo = category_photo;
    };
    /***/

  },

  /***/
  "./src/app/shared/model/gifts.model.ts":
  /*!*********************************************!*\
    !*** ./src/app/shared/model/gifts.model.ts ***!
    \*********************************************/

  /*! exports provided: giftModel, giftTransactionModel */

  /***/
  function srcAppSharedModelGiftsModelTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "giftModel", function () {
      return giftModel;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "giftTransactionModel", function () {
      return giftTransactionModel;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");

    var giftModel = function giftModel(id, name, image, imagegif, imageaudio, imageaudio2, price) {
      _classCallCheck(this, giftModel);

      this.id = id;
      this.name = name;
      this.image = image;
      this.imagegif = imagegif;
      this.imageaudio = imageaudio;
      this.imageaudio2 = imageaudio2;
      this.price = price;
    };

    var giftTransactionModel = function giftTransactionModel(image, dateandtime, imageaudio, imageaudio2, text) {
      _classCallCheck(this, giftTransactionModel);

      this.image = image;
      this.dateandtime = dateandtime;
      this.imageaudio = imageaudio;
      this.imageaudio2 = imageaudio2;
      this.text = text;
    };
    /***/

  },

  /***/
  "./src/app/shared/model/report.model.ts":
  /*!**********************************************!*\
    !*** ./src/app/shared/model/report.model.ts ***!
    \**********************************************/

  /*! exports provided: reportReason */

  /***/
  function srcAppSharedModelReportModelTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "reportReason", function () {
      return reportReason;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");

    var reportReason = function reportReason(id, reason) {
      _classCallCheck(this, reportReason);

      this.id = id;
      this.reason = reason;
    };
    /***/

  },

  /***/
  "./src/app/suspendmodal/suspendmodal-routing.module.ts":
  /*!*************************************************************!*\
    !*** ./src/app/suspendmodal/suspendmodal-routing.module.ts ***!
    \*************************************************************/

  /*! exports provided: SuspendmodalPageRoutingModule */

  /***/
  function srcAppSuspendmodalSuspendmodalRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SuspendmodalPageRoutingModule", function () {
      return SuspendmodalPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _suspendmodal_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./suspendmodal.page */
    "./src/app/suspendmodal/suspendmodal.page.ts");

    var routes = [{
      path: '',
      component: _suspendmodal_page__WEBPACK_IMPORTED_MODULE_3__["SuspendmodalPage"]
    }];

    var SuspendmodalPageRoutingModule = function SuspendmodalPageRoutingModule() {
      _classCallCheck(this, SuspendmodalPageRoutingModule);
    };

    SuspendmodalPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], SuspendmodalPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/suspendmodal/suspendmodal.module.ts":
  /*!*****************************************************!*\
    !*** ./src/app/suspendmodal/suspendmodal.module.ts ***!
    \*****************************************************/

  /*! exports provided: SuspendmodalPageModule */

  /***/
  function srcAppSuspendmodalSuspendmodalModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SuspendmodalPageModule", function () {
      return SuspendmodalPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _suspendmodal_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./suspendmodal-routing.module */
    "./src/app/suspendmodal/suspendmodal-routing.module.ts");
    /* harmony import */


    var _suspendmodal_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./suspendmodal.page */
    "./src/app/suspendmodal/suspendmodal.page.ts");

    var SuspendmodalPageModule = function SuspendmodalPageModule() {
      _classCallCheck(this, SuspendmodalPageModule);
    };

    SuspendmodalPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _suspendmodal_routing_module__WEBPACK_IMPORTED_MODULE_5__["SuspendmodalPageRoutingModule"]],
      declarations: [_suspendmodal_page__WEBPACK_IMPORTED_MODULE_6__["SuspendmodalPage"]]
    })], SuspendmodalPageModule);
    /***/
  },

  /***/
  "./src/app/suspendmodal/suspendmodal.page.scss":
  /*!*****************************************************!*\
    !*** ./src/app/suspendmodal/suspendmodal.page.scss ***!
    \*****************************************************/

  /*! exports provided: default */

  /***/
  function srcAppSuspendmodalSuspendmodalPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".message {\n  font-size: 20px;\n  font-weight: bold;\n  margin-top: 20px;\n}\n\np {\n  font-size: 13px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZW5kcmljay9EZXNrdG9wL1RlbG1vIFNvbHV0aW9ucy9Gb3JQcm9kdWN0aW9uL21heTEwMjAyMS9sYXN0IHNldHVwL2dyZWVudGh1bWJhcHAgY29weS9zcmMvYXBwL3N1c3BlbmRtb2RhbC9zdXNwZW5kbW9kYWwucGFnZS5zY3NzIiwic3JjL2FwcC9zdXNwZW5kbW9kYWwvc3VzcGVuZG1vZGFsLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0FDQ0o7O0FEQ0E7RUFDSSxlQUFBO0FDRUoiLCJmaWxlIjoic3JjL2FwcC9zdXNwZW5kbW9kYWwvc3VzcGVuZG1vZGFsLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5tZXNzYWdle1xuICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICBtYXJnaW4tdG9wOiAyMHB4O1xufVxucHtcbiAgICBmb250LXNpemU6IDEzcHg7XG59IiwiLm1lc3NhZ2Uge1xuICBmb250LXNpemU6IDIwcHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBtYXJnaW4tdG9wOiAyMHB4O1xufVxuXG5wIHtcbiAgZm9udC1zaXplOiAxM3B4O1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/suspendmodal/suspendmodal.page.ts":
  /*!***************************************************!*\
    !*** ./src/app/suspendmodal/suspendmodal.page.ts ***!
    \***************************************************/

  /*! exports provided: SuspendmodalPage */

  /***/
  function srcAppSuspendmodalSuspendmodalPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SuspendmodalPage", function () {
      return SuspendmodalPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");

    var SuspendmodalPage = /*#__PURE__*/function () {
      function SuspendmodalPage(modalController, router) {
        _classCallCheck(this, SuspendmodalPage);

        this.modalController = modalController;
        this.router = router;
      }

      _createClass(SuspendmodalPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "goBack",
        value: function goBack() {
          this.modalController.dismiss();
        }
      }, {
        key: "goToUserPolicy",
        value: function goToUserPolicy() {
          this.router.navigate(['tabs/userpolicy']);
        }
      }]);

      return SuspendmodalPage;
    }();

    SuspendmodalPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }];
    };

    SuspendmodalPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-suspendmodal',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./suspendmodal.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/suspendmodal/suspendmodal.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./suspendmodal.page.scss */
      "./src/app/suspendmodal/suspendmodal.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])], SuspendmodalPage);
    /***/
  },

  /***/
  "./src/app/userpolicy/userpolicy-routing.module.ts":
  /*!*********************************************************!*\
    !*** ./src/app/userpolicy/userpolicy-routing.module.ts ***!
    \*********************************************************/

  /*! exports provided: UserpolicyPageRoutingModule */

  /***/
  function srcAppUserpolicyUserpolicyRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "UserpolicyPageRoutingModule", function () {
      return UserpolicyPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _userpolicy_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./userpolicy.page */
    "./src/app/userpolicy/userpolicy.page.ts");

    var routes = [{
      path: '',
      component: _userpolicy_page__WEBPACK_IMPORTED_MODULE_3__["UserpolicyPage"]
    }];

    var UserpolicyPageRoutingModule = function UserpolicyPageRoutingModule() {
      _classCallCheck(this, UserpolicyPageRoutingModule);
    };

    UserpolicyPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], UserpolicyPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/userpolicy/userpolicy.module.ts":
  /*!*************************************************!*\
    !*** ./src/app/userpolicy/userpolicy.module.ts ***!
    \*************************************************/

  /*! exports provided: UserpolicyPageModule */

  /***/
  function srcAppUserpolicyUserpolicyModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "UserpolicyPageModule", function () {
      return UserpolicyPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _userpolicy_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./userpolicy-routing.module */
    "./src/app/userpolicy/userpolicy-routing.module.ts");
    /* harmony import */


    var _userpolicy_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./userpolicy.page */
    "./src/app/userpolicy/userpolicy.page.ts");

    var UserpolicyPageModule = function UserpolicyPageModule() {
      _classCallCheck(this, UserpolicyPageModule);
    };

    UserpolicyPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _userpolicy_routing_module__WEBPACK_IMPORTED_MODULE_5__["UserpolicyPageRoutingModule"]],
      declarations: [_userpolicy_page__WEBPACK_IMPORTED_MODULE_6__["UserpolicyPage"]]
    })], UserpolicyPageModule);
    /***/
  },

  /***/
  "./src/app/userpolicy/userpolicy.page.scss":
  /*!*************************************************!*\
    !*** ./src/app/userpolicy/userpolicy.page.scss ***!
    \*************************************************/

  /*! exports provided: default */

  /***/
  function srcAppUserpolicyUserpolicyPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VzZXJwb2xpY3kvdXNlcnBvbGljeS5wYWdlLnNjc3MifQ== */";
    /***/
  },

  /***/
  "./src/app/userpolicy/userpolicy.page.ts":
  /*!***********************************************!*\
    !*** ./src/app/userpolicy/userpolicy.page.ts ***!
    \***********************************************/

  /*! exports provided: UserpolicyPage */

  /***/
  function srcAppUserpolicyUserpolicyPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "UserpolicyPage", function () {
      return UserpolicyPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");

    var UserpolicyPage = /*#__PURE__*/function () {
      function UserpolicyPage(modalController) {
        _classCallCheck(this, UserpolicyPage);

        this.modalController = modalController;
      }

      _createClass(UserpolicyPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {} // goBack(){
        //   this.modalController.dismiss();
        // }

      }, {
        key: "goBack",
        value: function goBack() {
          window.history.back();
        }
      }]);

      return UserpolicyPage;
    }();

    UserpolicyPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
      }];
    };

    UserpolicyPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-userpolicy',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./userpolicy.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/userpolicy/userpolicy.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./userpolicy.page.scss */
      "./src/app/userpolicy/userpolicy.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]])], UserpolicyPage);
    /***/
  },

  /***/
  "./src/app/vippackage/vippackage-routing.module.ts":
  /*!*********************************************************!*\
    !*** ./src/app/vippackage/vippackage-routing.module.ts ***!
    \*********************************************************/

  /*! exports provided: VippackagePageRoutingModule */

  /***/
  function srcAppVippackageVippackageRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "VippackagePageRoutingModule", function () {
      return VippackagePageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _vippackage_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./vippackage.page */
    "./src/app/vippackage/vippackage.page.ts");

    var routes = [{
      path: '',
      component: _vippackage_page__WEBPACK_IMPORTED_MODULE_3__["VippackagePage"]
    }];

    var VippackagePageRoutingModule = function VippackagePageRoutingModule() {
      _classCallCheck(this, VippackagePageRoutingModule);
    };

    VippackagePageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], VippackagePageRoutingModule);
    /***/
  },

  /***/
  "./src/app/vippackage/vippackage.module.ts":
  /*!*************************************************!*\
    !*** ./src/app/vippackage/vippackage.module.ts ***!
    \*************************************************/

  /*! exports provided: VippackagePageModule */

  /***/
  function srcAppVippackageVippackageModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "VippackagePageModule", function () {
      return VippackagePageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _vippackage_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./vippackage-routing.module */
    "./src/app/vippackage/vippackage-routing.module.ts");
    /* harmony import */


    var _vippackage_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./vippackage.page */
    "./src/app/vippackage/vippackage.page.ts");

    var VippackagePageModule = function VippackagePageModule() {
      _classCallCheck(this, VippackagePageModule);
    };

    VippackagePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _vippackage_routing_module__WEBPACK_IMPORTED_MODULE_5__["VippackagePageRoutingModule"]],
      declarations: [_vippackage_page__WEBPACK_IMPORTED_MODULE_6__["VippackagePage"]]
    })], VippackagePageModule);
    /***/
  },

  /***/
  "./src/app/vippackage/vippackage.page.scss":
  /*!*************************************************!*\
    !*** ./src/app/vippackage/vippackage.page.scss ***!
    \*************************************************/

  /*! exports provided: default */

  /***/
  function srcAppVippackageVippackagePageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".lvlicon {\n  width: 20px !important;\n  height: 20px;\n  margin: auto;\n  margin-bottom: -2%;\n  margin-right: 3%;\n}\n\n.lvlnum {\n  margin: 0;\n  margin-top: 4%;\n  font-family: Verdana, Geneva, Tahoma, sans-serif;\n  font-size: 18px;\n}\n\nion-item {\n  --padding-start: 0% !important;\n  --inner-padding-end: 0;\n}\n\n#radio_uncheck {\n  --background: white;\n  width: 100%;\n  margin: auto;\n  --background: var(--ion-item-background,transparent);\n  border-radius: 8px;\n  transform: translateZ(0);\n  transition: transform 0.5s cubic-bezier(0.12, 0.72, 0.29, 1);\n  font-size: 14px;\n  box-shadow: 0 4px 16px rgba(0, 0, 0, 0.12);\n  color: black;\n}\n\n.item-radio-checked {\n  --background: linear-gradient(to top left, #1dc1e6 0%, #1dc1e696 95%) !important;\n  width: 100%;\n  margin: auto;\n  --background: var(--ion-item-background,transparent);\n  border-radius: 8px;\n  transform: translateZ(0);\n  transition: transform 0.5s cubic-bezier(0.12, 0.72, 0.29, 1);\n  font-size: 14px;\n  box-shadow: 0 4px 16px rgba(0, 0, 0, 0.12);\n  color: white !important;\n}\n\n.item_icon {\n  margin-bottom: 0;\n  margin-right: 5px;\n}\n\n.item_package {\n  margin-bottom: 0;\n  font-weight: bolder;\n  text-transform: uppercase;\n  font-size: 16px;\n}\n\n.item_label {\n  margin-bottom: 0;\n  text-transform: capitalize;\n}\n\n.item_sec {\n  font-size: 13px;\n  margin-top: -8%;\n  --background: transparent;\n}\n\n.bk_treasure {\n  --background: none;\n  background-image: url(\"/assets/icon/treasure.png\");\n  background-position: center center;\n  background-repeat: no-repeat;\n  background-size: cover;\n}\n\n.item-block {\n  --background:linear-gradient(to top left, #1dc1e6 0%, #1dc1e696 95%) !important;\n  width: 100%;\n  margin: auto;\n  --background: var(--ion-item-background,transparent);\n  border-radius: 8px;\n  transform: translateZ(0);\n  transition: transform 0.5s cubic-bezier(0.12, 0.72, 0.29, 1);\n  font-size: 14px;\n  box-shadow: 0 4px 16px rgba(0, 0, 0, 0.12);\n  color: green;\n}\n\n.prename {\n  margin-top: 8%;\n  font-size: 12px;\n  font-weight: lighter !important;\n  text-transform: uppercase;\n}\n\n.pretext {\n  font-size: 12px;\n  margin-top: 1%;\n  font-weight: lighter !important;\n}\n\n.item_radio {\n  float: right;\n  margin-top: -5%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZW5kcmljay9EZXNrdG9wL1RlbG1vIFNvbHV0aW9ucy9Gb3JQcm9kdWN0aW9uL21heTEwMjAyMS9sYXN0IHNldHVwL2dyZWVudGh1bWJhcHAgY29weS9zcmMvYXBwL3ZpcHBhY2thZ2UvdmlwcGFja2FnZS5wYWdlLnNjc3MiLCJzcmMvYXBwL3ZpcHBhY2thZ2UvdmlwcGFja2FnZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxzQkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtBQ0NGOztBREVFO0VBQ0UsU0FBQTtFQUNBLGNBQUE7RUFDQSxnREFBQTtFQUVBLGVBQUE7QUNBSjs7QURHRTtFQUNFLDhCQUFBO0VBQ0Esc0JBQUE7QUNBSjs7QURHRTtFQUNFLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFBaUIsb0RBQUE7RUFDakIsa0JBQUE7RUFFQSx3QkFBQTtFQUdBLDREQUFBO0VBRUEsZUFBQTtFQUVBLDBDQUFBO0VBQ0EsWUFBQTtBQ0NKOztBRENFO0VBQ0UsZ0ZBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUFpQixvREFBQTtFQUNqQixrQkFBQTtFQUVBLHdCQUFBO0VBR0EsNERBQUE7RUFFQSxlQUFBO0VBRUEsMENBQUE7RUFDQSx1QkFBQTtBQ0dKOztBREFBO0VBQ0UsZ0JBQUE7RUFDQSxpQkFBQTtBQ0dGOztBRERBO0VBQ0UsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLHlCQUFBO0VBQ0EsZUFBQTtBQ0lGOztBREZBO0VBQ0UsZ0JBQUE7RUFDQSwwQkFBQTtBQ0tGOztBREhBO0VBQ0UsZUFBQTtFQUNBLGVBQUE7RUFDQSx5QkFBQTtBQ01GOztBREpBO0VBQ0Usa0JBQUE7RUFDRSxrREFBQTtFQUNBLGtDQUFBO0VBQ0EsNEJBQUE7RUFDQSxzQkFBQTtBQ09KOztBRExBO0VBQ0UsK0VBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUFpQixvREFBQTtFQUNqQixrQkFBQTtFQUVBLHdCQUFBO0VBR0EsNERBQUE7RUFFQSxlQUFBO0VBRUEsMENBQUE7RUFDQSxZQUFBO0FDU0Y7O0FEUEE7RUFDRSxjQUFBO0VBQ0EsZUFBQTtFQUNBLCtCQUFBO0VBQ0EseUJBQUE7QUNVRjs7QURSQTtFQUNFLGVBQUE7RUFDQSxjQUFBO0VBQ0EsK0JBQUE7QUNXRjs7QURUQTtFQUNFLFlBQUE7RUFDQSxlQUFBO0FDWUYiLCJmaWxlIjoic3JjL2FwcC92aXBwYWNrYWdlL3ZpcHBhY2thZ2UucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmx2bGljb257XG4gIHdpZHRoOiAyMHB4ICFpbXBvcnRhbnQ7XG4gIGhlaWdodDogMjBweDtcbiAgbWFyZ2luOiBhdXRvO1xuICBtYXJnaW4tYm90dG9tOiAtMiU7XG4gIG1hcmdpbi1yaWdodDogMyU7XG4gIC8vIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbn0gIFxuICAubHZsbnVte1xuICAgIG1hcmdpbjogMDtcbiAgICBtYXJnaW4tdG9wOiA0JTtcbiAgICBmb250LWZhbWlseTogVmVyZGFuYSwgR2VuZXZhLCBUYWhvbWEsIHNhbnMtc2VyaWY7XG4gICAgLy8gZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICBcbiAgfVxuICBpb24taXRlbSB7XG4gICAgLS1wYWRkaW5nLXN0YXJ0OiAwJSAhaW1wb3J0YW50O1xuICAgIC0taW5uZXItcGFkZGluZy1lbmQ6IDA7XG4gICAgXG4gIH1cbiAgI3JhZGlvX3VuY2hlY2t7XG4gICAgLS1iYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBtYXJnaW46IGF1dG87ICAgIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWl0ZW0tYmFja2dyb3VuZCx0cmFuc3BhcmVudCk7XG4gICAgYm9yZGVyLXJhZGl1czogOHB4O1xuICAgIC13ZWJraXQtdHJhbnNmb3JtOiB0cmFuc2xhdGVaKDApO1xuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWigwKTtcbiAgICAtd2Via2l0LXRyYW5zaXRpb246IC13ZWJraXQtdHJhbnNmb3JtIC41cyBjdWJpYy1iZXppZXIoLjEyLC43MiwuMjksMSk7XG4gICAgdHJhbnNpdGlvbjogLXdlYmtpdC10cmFuc2Zvcm0gLjVzIGN1YmljLWJlemllciguMTIsLjcyLC4yOSwxKTtcbiAgICB0cmFuc2l0aW9uOiB0cmFuc2Zvcm0gLjVzIGN1YmljLWJlemllciguMTIsLjcyLC4yOSwxKTtcbiAgICB0cmFuc2l0aW9uOiB0cmFuc2Zvcm0gLjVzIGN1YmljLWJlemllciguMTIsLjcyLC4yOSwxKSwtd2Via2l0LXRyYW5zZm9ybSAuNXMgY3ViaWMtYmV6aWVyKC4xMiwuNzIsLjI5LDEpO1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAtd2Via2l0LWJveC1zaGFkb3c6IDAgNHB4IDE2cHggcmdiYSgwLDAsMCwuMTIpO1xuICAgIGJveC1zaGFkb3c6IDAgNHB4IDE2cHggcmdiYSgwLDAsMCwuMTIpO1xuICAgIGNvbG9yOiBibGFjaztcbiAgfVxuICAuaXRlbS1yYWRpby1jaGVja2Vke1xuICAgIC0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KHRvIHRvcCBsZWZ0LCAjMWRjMWU2IDAlLCAjMWRjMWU2OTYgOTUlKSAhaW1wb3J0YW50O1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIG1hcmdpbjogYXV0bzsgICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24taXRlbS1iYWNrZ3JvdW5kLHRyYW5zcGFyZW50KTtcbiAgICBib3JkZXItcmFkaXVzOiA4cHg7XG4gICAgLXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZVooMCk7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVaKDApO1xuICAgIC13ZWJraXQtdHJhbnNpdGlvbjogLXdlYmtpdC10cmFuc2Zvcm0gLjVzIGN1YmljLWJlemllciguMTIsLjcyLC4yOSwxKTtcbiAgICB0cmFuc2l0aW9uOiAtd2Via2l0LXRyYW5zZm9ybSAuNXMgY3ViaWMtYmV6aWVyKC4xMiwuNzIsLjI5LDEpO1xuICAgIHRyYW5zaXRpb246IHRyYW5zZm9ybSAuNXMgY3ViaWMtYmV6aWVyKC4xMiwuNzIsLjI5LDEpO1xuICAgIHRyYW5zaXRpb246IHRyYW5zZm9ybSAuNXMgY3ViaWMtYmV6aWVyKC4xMiwuNzIsLjI5LDEpLC13ZWJraXQtdHJhbnNmb3JtIC41cyBjdWJpYy1iZXppZXIoLjEyLC43MiwuMjksMSk7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgIC13ZWJraXQtYm94LXNoYWRvdzogMCA0cHggMTZweCByZ2JhKDAsMCwwLC4xMik7XG4gICAgYm94LXNoYWRvdzogMCA0cHggMTZweCByZ2JhKDAsMCwwLC4xMik7XG4gICAgY29sb3I6IHdoaXRlICFpbXBvcnRhbnQ7XG4gIH1cblxuLml0ZW1faWNvbntcbiAgbWFyZ2luLWJvdHRvbTogMDtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XG59XG4uaXRlbV9wYWNrYWdle1xuICBtYXJnaW4tYm90dG9tOiAwO1xuICBmb250LXdlaWdodDogYm9sZGVyO1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBmb250LXNpemU6IDE2cHg7XG59XG4uaXRlbV9sYWJlbHtcbiAgbWFyZ2luLWJvdHRvbTogMDtcbiAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG59XG4uaXRlbV9zZWN7XG4gIGZvbnQtc2l6ZTogMTNweDtcbiAgbWFyZ2luLXRvcDogLTglO1xuICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xufVxuLmJrX3RyZWFzdXJle1xuICAtLWJhY2tncm91bmQ6IG5vbmU7XG4gICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKCcvYXNzZXRzL2ljb24vdHJlYXN1cmUucG5nJyk7XG4gICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyIGNlbnRlcjtcbiAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG59XG4uaXRlbS1ibG9ja3tcbiAgLS1iYWNrZ3JvdW5kOmxpbmVhci1ncmFkaWVudCh0byB0b3AgbGVmdCwgIzFkYzFlNiAwJSwgIzFkYzFlNjk2IDk1JSkgIWltcG9ydGFudDtcbiAgd2lkdGg6IDEwMCU7XG4gIG1hcmdpbjogYXV0bzsgICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24taXRlbS1iYWNrZ3JvdW5kLHRyYW5zcGFyZW50KTtcbiAgYm9yZGVyLXJhZGl1czogOHB4O1xuICAtd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlWigwKTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGVaKDApO1xuICAtd2Via2l0LXRyYW5zaXRpb246IC13ZWJraXQtdHJhbnNmb3JtIC41cyBjdWJpYy1iZXppZXIoLjEyLC43MiwuMjksMSk7XG4gIHRyYW5zaXRpb246IC13ZWJraXQtdHJhbnNmb3JtIC41cyBjdWJpYy1iZXppZXIoLjEyLC43MiwuMjksMSk7XG4gIHRyYW5zaXRpb246IHRyYW5zZm9ybSAuNXMgY3ViaWMtYmV6aWVyKC4xMiwuNzIsLjI5LDEpO1xuICB0cmFuc2l0aW9uOiB0cmFuc2Zvcm0gLjVzIGN1YmljLWJlemllciguMTIsLjcyLC4yOSwxKSwtd2Via2l0LXRyYW5zZm9ybSAuNXMgY3ViaWMtYmV6aWVyKC4xMiwuNzIsLjI5LDEpO1xuICBmb250LXNpemU6IDE0cHg7XG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCA0cHggMTZweCByZ2JhKDAsMCwwLC4xMik7XG4gIGJveC1zaGFkb3c6IDAgNHB4IDE2cHggcmdiYSgwLDAsMCwuMTIpO1xuICBjb2xvcjogZ3JlZW47XG59XG4ucHJlbmFtZXtcbiAgbWFyZ2luLXRvcDogOCU7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgZm9udC13ZWlnaHQ6IGxpZ2h0ZXIgIWltcG9ydGFudDtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbn1cbi5wcmV0ZXh0e1xuICBmb250LXNpemU6IDEycHg7XG4gIG1hcmdpbi10b3A6IDElO1xuICBmb250LXdlaWdodDogbGlnaHRlciAhaW1wb3J0YW50O1xufVxuLml0ZW1fcmFkaW97XG4gIGZsb2F0OiByaWdodDtcbiAgbWFyZ2luLXRvcDogLTUlO1xufVxuIiwiLmx2bGljb24ge1xuICB3aWR0aDogMjBweCAhaW1wb3J0YW50O1xuICBoZWlnaHQ6IDIwcHg7XG4gIG1hcmdpbjogYXV0bztcbiAgbWFyZ2luLWJvdHRvbTogLTIlO1xuICBtYXJnaW4tcmlnaHQ6IDMlO1xufVxuXG4ubHZsbnVtIHtcbiAgbWFyZ2luOiAwO1xuICBtYXJnaW4tdG9wOiA0JTtcbiAgZm9udC1mYW1pbHk6IFZlcmRhbmEsIEdlbmV2YSwgVGFob21hLCBzYW5zLXNlcmlmO1xuICBmb250LXNpemU6IDE4cHg7XG59XG5cbmlvbi1pdGVtIHtcbiAgLS1wYWRkaW5nLXN0YXJ0OiAwJSAhaW1wb3J0YW50O1xuICAtLWlubmVyLXBhZGRpbmctZW5kOiAwO1xufVxuXG4jcmFkaW9fdW5jaGVjayB7XG4gIC0tYmFja2dyb3VuZDogd2hpdGU7XG4gIHdpZHRoOiAxMDAlO1xuICBtYXJnaW46IGF1dG87XG4gIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWl0ZW0tYmFja2dyb3VuZCx0cmFuc3BhcmVudCk7XG4gIGJvcmRlci1yYWRpdXM6IDhweDtcbiAgLXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZVooMCk7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlWigwKTtcbiAgLXdlYmtpdC10cmFuc2l0aW9uOiAtd2Via2l0LXRyYW5zZm9ybSAwLjVzIGN1YmljLWJlemllcigwLjEyLCAwLjcyLCAwLjI5LCAxKTtcbiAgdHJhbnNpdGlvbjogLXdlYmtpdC10cmFuc2Zvcm0gMC41cyBjdWJpYy1iZXppZXIoMC4xMiwgMC43MiwgMC4yOSwgMSk7XG4gIHRyYW5zaXRpb246IHRyYW5zZm9ybSAwLjVzIGN1YmljLWJlemllcigwLjEyLCAwLjcyLCAwLjI5LCAxKTtcbiAgdHJhbnNpdGlvbjogdHJhbnNmb3JtIDAuNXMgY3ViaWMtYmV6aWVyKDAuMTIsIDAuNzIsIDAuMjksIDEpLCAtd2Via2l0LXRyYW5zZm9ybSAwLjVzIGN1YmljLWJlemllcigwLjEyLCAwLjcyLCAwLjI5LCAxKTtcbiAgZm9udC1zaXplOiAxNHB4O1xuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgNHB4IDE2cHggcmdiYSgwLCAwLCAwLCAwLjEyKTtcbiAgYm94LXNoYWRvdzogMCA0cHggMTZweCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xuICBjb2xvcjogYmxhY2s7XG59XG5cbi5pdGVtLXJhZGlvLWNoZWNrZWQge1xuICAtLWJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCh0byB0b3AgbGVmdCwgIzFkYzFlNiAwJSwgIzFkYzFlNjk2IDk1JSkgIWltcG9ydGFudDtcbiAgd2lkdGg6IDEwMCU7XG4gIG1hcmdpbjogYXV0bztcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24taXRlbS1iYWNrZ3JvdW5kLHRyYW5zcGFyZW50KTtcbiAgYm9yZGVyLXJhZGl1czogOHB4O1xuICAtd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlWigwKTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGVaKDApO1xuICAtd2Via2l0LXRyYW5zaXRpb246IC13ZWJraXQtdHJhbnNmb3JtIDAuNXMgY3ViaWMtYmV6aWVyKDAuMTIsIDAuNzIsIDAuMjksIDEpO1xuICB0cmFuc2l0aW9uOiAtd2Via2l0LXRyYW5zZm9ybSAwLjVzIGN1YmljLWJlemllcigwLjEyLCAwLjcyLCAwLjI5LCAxKTtcbiAgdHJhbnNpdGlvbjogdHJhbnNmb3JtIDAuNXMgY3ViaWMtYmV6aWVyKDAuMTIsIDAuNzIsIDAuMjksIDEpO1xuICB0cmFuc2l0aW9uOiB0cmFuc2Zvcm0gMC41cyBjdWJpYy1iZXppZXIoMC4xMiwgMC43MiwgMC4yOSwgMSksIC13ZWJraXQtdHJhbnNmb3JtIDAuNXMgY3ViaWMtYmV6aWVyKDAuMTIsIDAuNzIsIDAuMjksIDEpO1xuICBmb250LXNpemU6IDE0cHg7XG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCA0cHggMTZweCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xuICBib3gtc2hhZG93OiAwIDRweCAxNnB4IHJnYmEoMCwgMCwgMCwgMC4xMik7XG4gIGNvbG9yOiB3aGl0ZSAhaW1wb3J0YW50O1xufVxuXG4uaXRlbV9pY29uIHtcbiAgbWFyZ2luLWJvdHRvbTogMDtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XG59XG5cbi5pdGVtX3BhY2thZ2Uge1xuICBtYXJnaW4tYm90dG9tOiAwO1xuICBmb250LXdlaWdodDogYm9sZGVyO1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBmb250LXNpemU6IDE2cHg7XG59XG5cbi5pdGVtX2xhYmVsIHtcbiAgbWFyZ2luLWJvdHRvbTogMDtcbiAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG59XG5cbi5pdGVtX3NlYyB7XG4gIGZvbnQtc2l6ZTogMTNweDtcbiAgbWFyZ2luLXRvcDogLTglO1xuICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xufVxuXG4uYmtfdHJlYXN1cmUge1xuICAtLWJhY2tncm91bmQ6IG5vbmU7XG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybChcIi9hc3NldHMvaWNvbi90cmVhc3VyZS5wbmdcIik7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlciBjZW50ZXI7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG59XG5cbi5pdGVtLWJsb2NrIHtcbiAgLS1iYWNrZ3JvdW5kOmxpbmVhci1ncmFkaWVudCh0byB0b3AgbGVmdCwgIzFkYzFlNiAwJSwgIzFkYzFlNjk2IDk1JSkgIWltcG9ydGFudDtcbiAgd2lkdGg6IDEwMCU7XG4gIG1hcmdpbjogYXV0bztcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24taXRlbS1iYWNrZ3JvdW5kLHRyYW5zcGFyZW50KTtcbiAgYm9yZGVyLXJhZGl1czogOHB4O1xuICAtd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlWigwKTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGVaKDApO1xuICAtd2Via2l0LXRyYW5zaXRpb246IC13ZWJraXQtdHJhbnNmb3JtIDAuNXMgY3ViaWMtYmV6aWVyKDAuMTIsIDAuNzIsIDAuMjksIDEpO1xuICB0cmFuc2l0aW9uOiAtd2Via2l0LXRyYW5zZm9ybSAwLjVzIGN1YmljLWJlemllcigwLjEyLCAwLjcyLCAwLjI5LCAxKTtcbiAgdHJhbnNpdGlvbjogdHJhbnNmb3JtIDAuNXMgY3ViaWMtYmV6aWVyKDAuMTIsIDAuNzIsIDAuMjksIDEpO1xuICB0cmFuc2l0aW9uOiB0cmFuc2Zvcm0gMC41cyBjdWJpYy1iZXppZXIoMC4xMiwgMC43MiwgMC4yOSwgMSksIC13ZWJraXQtdHJhbnNmb3JtIDAuNXMgY3ViaWMtYmV6aWVyKDAuMTIsIDAuNzIsIDAuMjksIDEpO1xuICBmb250LXNpemU6IDE0cHg7XG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCA0cHggMTZweCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xuICBib3gtc2hhZG93OiAwIDRweCAxNnB4IHJnYmEoMCwgMCwgMCwgMC4xMik7XG4gIGNvbG9yOiBncmVlbjtcbn1cblxuLnByZW5hbWUge1xuICBtYXJnaW4tdG9wOiA4JTtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBmb250LXdlaWdodDogbGlnaHRlciAhaW1wb3J0YW50O1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xufVxuXG4ucHJldGV4dCB7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgbWFyZ2luLXRvcDogMSU7XG4gIGZvbnQtd2VpZ2h0OiBsaWdodGVyICFpbXBvcnRhbnQ7XG59XG5cbi5pdGVtX3JhZGlvIHtcbiAgZmxvYXQ6IHJpZ2h0O1xuICBtYXJnaW4tdG9wOiAtNSU7XG59Il19 */";
    /***/
  },

  /***/
  "./src/app/vippackage/vippackage.page.ts":
  /*!***********************************************!*\
    !*** ./src/app/vippackage/vippackage.page.ts ***!
    \***********************************************/

  /*! exports provided: VippackagePage */

  /***/
  function srcAppVippackageVippackagePageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "VippackagePage", function () {
      return VippackagePage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var _providers_credential_provider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../providers/credential-provider */
    "./src/providers/credential-provider.ts");

    var VippackagePage = /*#__PURE__*/function () {
      function VippackagePage(storage, modalCtrl, alertCtrl, toastController, postPvdr) {
        _classCallCheck(this, VippackagePage);

        this.storage = storage;
        this.modalCtrl = modalCtrl;
        this.alertCtrl = alertCtrl;
        this.toastController = toastController;
        this.postPvdr = postPvdr;
        this.vippackage = "";
        this.withPackage = false;
        this.monthlySubscription = false;
      }

      _createClass(VippackagePage, [{
        key: "goBack",
        value: function goBack() {
          window.history.back();
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {
          this.vippackage = "3";
          this.plotData();
        }
      }, {
        key: "plotData",
        value: function plotData() {
          var _this26 = this;

          this.storage.get("user_id").then(function (user_id) {
            var body2 = {
              action: 'checkSubscription',
              user_id: user_id
            };
            console.log("subscription:" + JSON.stringify(body2));

            _this26.postPvdr.postData(body2, 'subscription.php').subscribe(function (data) {
              if (data.success) {
                var provide = "";

                for (var key in data.result) {
                  //this.vippackage = data.result[key].vip_package_id;
                  console.log("provide:" + data.result[key].vip_package_id);
                  provide = data.result[key].vip_package_id;
                }

                _this26.vippackage = '' + provide;
                _this26.withPackage = true;
                console.log("vippackage:" + _this26.vippackage);
                var body21 = {
                  action: 'checkMonthlySubscription',
                  user_id: user_id
                };
                console.log("subscription:" + JSON.stringify(body21));

                _this26.postPvdr.postData(body21, 'subscription.php').subscribe(function (data) {
                  for (var _key in data.result) {
                    if (data.result[_key].vip_subscription == "1") {
                      _this26.monthlySubscription = true;
                    } else {
                      _this26.monthlySubscription = false;
                    }

                    console.log("monthlySubscritpion:" + data.result[_key].vip_subscription);
                  }

                  console.log("monthlySubscritpion:" + _this26.monthlySubscription);
                });
              }
            });
          });
        }
      }, {
        key: "subscribe",
        value: function subscribe() {
          var _this27 = this;

          if (!this.withPackage) {
            this.alertCtrl.create({
              header: 'Subscription Package',
              message: 'Are you sure you want to subscribe this package?',
              cssClass: 'food',
              buttons: [{
                text: 'CANCEL',
                role: 'cancel',
                handler: function handler() {
                  console.log('Cancel verification!');
                }
              }, {
                text: 'YES',
                handler: function handler() {
                  _this27.subscribeVerified();
                }
              }]
            }).then(function (res) {
              res.present();
            });
          } else {
            this.presentToast("You are currently subscribed to a VIP Package.");
          }
        }
      }, {
        key: "subscribeVerified",
        value: function subscribeVerified() {
          var _this28 = this;

          this.storage.get("user_id").then(function (user_id) {
            var body2 = {
              action: 'subscribePackage',
              user_id: user_id,
              vippackage: _this28.vippackage
            };
            console.log("looua:" + JSON.stringify(body2));

            _this28.postPvdr.postData(body2, 'subscription.php').subscribe(function (data) {
              if (data.success) {
                _this28.presentToast("Package Successfully Subscribed.");

                _this28.withPackage = true;
                _this28.monthlySubscription = true;
              }
            });
          });
        }
      }, {
        key: "addValue",
        value: function addValue(e) {
          e.preventDefault();
          e.stopImmediatePropagation();
          e.cancelBubble = true;
          e.stopPropagation(); // Doing other stuff here to control if checkbox should be checked or not!, or just let it be empty!

          if (this.monthlySubscription) {
            this.verifyCancel();
          } else {
            this.verifySubscription();
          }

          return false;
        }
      }, {
        key: "verifySubscription",
        value: function verifySubscription() {
          var _this29 = this;

          this.alertCtrl.create({
            header: 'Subscription Package',
            message: 'Are you sure you want to avail Monthly Subscription?',
            cssClass: 'food',
            buttons: [{
              text: 'CANCEL',
              role: 'cancel',
              handler: function handler() {
                console.log('Cancel subscription!');
                _this29.monthlySubscription = false;
              }
            }, {
              text: 'YES',
              handler: function handler() {
                _this29.monthlySubscription = true;

                _this29.availSubscription();
              }
            }]
          }).then(function (res) {
            res.present();
          });
        }
      }, {
        key: "verifyCancel",
        value: function verifyCancel() {
          var _this30 = this;

          this.alertCtrl.create({
            header: 'Subscription Package',
            message: 'Are you sure you want to cancel Monthly Subscription?',
            cssClass: 'food',
            buttons: [{
              text: 'CANCEL',
              role: 'cancel',
              handler: function handler() {
                console.log('Cancel subscription!');
                _this30.monthlySubscription = true;
              }
            }, {
              text: 'YES',
              handler: function handler() {
                _this30.monthlySubscription = false;

                _this30.cancelSubscription();
              }
            }]
          }).then(function (res) {
            res.present();
          });
        }
      }, {
        key: "availSubscription",
        value: function availSubscription() {
          var _this31 = this;

          this.storage.get("user_id").then(function (user_id) {
            var body2 = {
              action: 'monthlySubscription',
              user_id: user_id,
              vip_subscription: 1
            };

            _this31.postPvdr.postData(body2, 'subscription.php').subscribe(function (data) {
              if (data.success) {
                _this31.withPackage = true;
                _this31.monthlySubscription = false;
              }
            });
          });
        }
      }, {
        key: "cancelSubscription",
        value: function cancelSubscription() {
          var _this32 = this;

          this.storage.get("user_id").then(function (user_id) {
            var body2 = {
              action: 'monthlySubscription',
              user_id: user_id,
              vip_subscription: 0
            };
            console.log("looua:" + JSON.stringify(body2));

            _this32.postPvdr.postData(body2, 'subscription.php').subscribe(function (data) {
              if (data.success) {
                _this32.withPackage = true;
                _this32.monthlySubscription = false;
              }
            });
          });
        }
      }, {
        key: "presentToast",
        value: function presentToast(toastMessage) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee34() {
            var toast;
            return regeneratorRuntime.wrap(function _callee34$(_context34) {
              while (1) {
                switch (_context34.prev = _context34.next) {
                  case 0:
                    _context34.next = 2;
                    return this.toastController.create({
                      message: toastMessage,
                      duration: 3000
                    });

                  case 2:
                    toast = _context34.sent;
                    toast.present();

                  case 4:
                  case "end":
                    return _context34.stop();
                }
              }
            }, _callee34, this);
          }));
        }
      }]);

      return VippackagePage;
    }();

    VippackagePage.ctorParameters = function () {
      return [{
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]
      }, {
        type: _providers_credential_provider__WEBPACK_IMPORTED_MODULE_4__["PostProvider"]
      }];
    };

    VippackagePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-vippackage',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./vippackage.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/vippackage/vippackage.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./vippackage.page.scss */
      "./src/app/vippackage/vippackage.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"], _providers_credential_provider__WEBPACK_IMPORTED_MODULE_4__["PostProvider"]])], VippackagePage);
    /***/
  },

  /***/
  "./src/environments/environment.ts":
  /*!*****************************************!*\
    !*** ./src/environments/environment.ts ***!
    \*****************************************/

  /*! exports provided: environment */

  /***/
  function srcEnvironmentsEnvironmentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "environment", function () {
      return environment;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js"); // This file can be replaced during build by using the `fileReplacements` array.
    // `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
    // The list of file replacements can be found in `angular.json`.


    var environment = {
      production: false,
      firebase: {
        apiKey: "AIzaSyCAmchUJBbSkGvvl0EFUz5XPvcf9hP7aHA",
        authDomain: "brixy-live.firebaseapp.com",
        databaseURL: "https://brixy-live.firebaseio.com",
        projectId: "brixy-live",
        storageBucket: "brixy-live.appspot.com",
        messagingSenderId: "405056445813",
        appId: "1:405056445813:web:aa90800b34cc4566c8073c",
        measurementId: "G-PS9KVFRFSF"
      }
    };
    /*
     * For easier debugging in development mode, you can import the following file
     * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
     *
     * This import should be commented out in production mode because it will have a negative impact
     * on performance if an error is thrown.
     */
    // import 'zone.js/dist/zone-error';  // Included with Angular CLI.

    /***/
  },

  /***/
  "./src/main.ts":
  /*!*********************!*\
    !*** ./src/main.ts ***!
    \*********************/

  /*! no exports provided */

  /***/
  function srcMainTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/platform-browser-dynamic */
    "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
    /* harmony import */


    var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./app/app.module */
    "./src/app/app.module.ts");
    /* harmony import */


    var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./environments/environment */
    "./src/environments/environment.ts");

    if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
      Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
    }

    Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])["catch"](function (err) {
      return console.log(err);
    });
    /***/
  },

  /***/
  "./src/providers/chat-messages.provider.ts":
  /*!*************************************************!*\
    !*** ./src/providers/chat-messages.provider.ts ***!
    \*************************************************/

  /*! exports provided: ChatMessages */

  /***/
  function srcProvidersChatMessagesProviderTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ChatMessages", function () {
      return ChatMessages;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/http */
    "./node_modules/@angular/http/fesm2015/http.js");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");

    var ChatMessages = /*#__PURE__*/function () {
      //server: string = "http://165.22.242.129/brixy-live/server_api/";
      function ChatMessages(http) {
        _classCallCheck(this, ChatMessages);

        this.http = http; //server: string = "https://www.telmotestserver.website/brixy-live/server_api/"; // server address https://www.telmotestserver.website/brixy-live/server_api/ http://localhost/server_api/

        this.server = "http://192.168.1.5/brixy-live/server_api/";
      }

      _createClass(ChatMessages, [{
        key: "postData",
        value: function postData(body, file) {
          var type = "application/json; charset=UTF-8";
          var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({
            'Content-Type': type
          });
          var options = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({
            headers: headers
          });
          return this.http.post(this.server + file, JSON.stringify(body), options).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) {
            return res.json();
          })); //.pipe(map(res => console.log(res)))
        }
      }, {
        key: "getMessages",
        value: function getMessages(body, file) {
          //let apiURL = `${this.apiRoot}?term=${term}&media=music&limit=20&callback=JSONP_CALLBACK`;
          // return this.http.get(apiURL) (1)
          //     .map(res => { (2)
          //       return res.json().results.map(item => { (3)
          //         return new SearchItem( (4)
          //             item.trackName,
          //             item.artistName,
          //             item.trackViewUrl,
          //             item.artworkUrl30,
          //             item.artistId
          //         );
          //       });
          //     });
          var type = "application/json; charset=UTF-8";
          var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({
            'Content-Type': type
          });
          var options = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({
            headers: headers
          });
          return this.http.post(this.server + file, JSON.stringify(body), options).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) {
            return res.json();
          }));
        }
      }, {
        key: "sendMessage",
        value: function sendMessage(body, file) {
          var type = "application/json; charset=UTF-8";
          var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({
            'Content-Type': type
          });
          var options = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({
            headers: headers
          });
          return this.http.post(this.server + file, JSON.stringify(body), options).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) {
            return res.json();
          }));
        }
      }]);

      return ChatMessages;
    }();

    ChatMessages.ctorParameters = function () {
      return [{
        type: _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"]
      }];
    };

    ChatMessages = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"]])], ChatMessages);
    /***/
  },

  /***/
  "./src/providers/credential-provider.ts":
  /*!**********************************************!*\
    !*** ./src/providers/credential-provider.ts ***!
    \**********************************************/

  /*! exports provided: PostProvider */

  /***/
  function srcProvidersCredentialProviderTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PostProvider", function () {
      return PostProvider;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/http */
    "./node_modules/@angular/http/fesm2015/http.js");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");

    var PostProvider = /*#__PURE__*/function () {
      function PostProvider(http) {
        _classCallCheck(this, PostProvider);

        this.http = http; //server: string = "https://www.telmotestserver.website/brixy-live/server_api/"; // server address https://www.telmotestserver.website/brixy-live/server_api/ http://localhost/server_api/
        //server: string = "http://192.168.1.5/brixy-live/server_api/";
        //myServerIP: string = "165.22.242.129";

        this.ip = "greenthumbtrade.com"; //ip: string = "192.168.1.9"
        //ip: string = "localhost"

        this.myServerIP = "https://" + this.ip + "/green123thumb456"; //myServerIP: string = "http://"+this.ip+"";

        this.server = this.myServerIP + "/greenthumb/server_api/";
        this.emailServer = this.myServerIP + "/greenthumb-email/send-email.php";
      }

      _createClass(PostProvider, [{
        key: "postData",
        value: function postData(body, file) {
          var type = "application/json; charset=UTF-8";
          var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({
            'Content-Type': type
          });
          var options = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({
            headers: headers
          });
          return this.http.post(this.server + file, JSON.stringify(body), options).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) {
            return res.json();
          })); //.pipe(map(res => console.log(res)))
        }
      }, {
        key: "sendCode",
        value: function sendCode(body) {
          var type = "application/json; charset=UTF-8";
          var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({
            'Content-Type': type
          });
          var options = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({
            headers: headers
          });
          console.log("emailServer:" + this.emailServer);
          return this.http.post(this.emailServer, JSON.stringify(body), options).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) {
            return res.json();
          })); //.pipe(map(res => console.log(res)))
        }
      }, {
        key: "myServer",
        value: function myServer() {
          return this.myServerIP;
        }
      }]);

      return PostProvider;
    }();

    PostProvider.ctorParameters = function () {
      return [{
        type: _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"]
      }];
    };

    PostProvider = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"]])], PostProvider);
    /***/
  },

  /***/
  "./src/providers/sms-provider.ts":
  /*!***************************************!*\
    !*** ./src/providers/sms-provider.ts ***!
    \***************************************/

  /*! exports provided: SmsProvider */

  /***/
  function srcProvidersSmsProviderTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SmsProvider", function () {
      return SmsProvider;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/http */
    "./node_modules/@angular/http/fesm2015/http.js");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");

    var SmsProvider = /*#__PURE__*/function () {
      function SmsProvider(http) {
        _classCallCheck(this, SmsProvider);

        this.http = http;
        this.server = "https://www.greenthumbtrade.com/green123thumb456/greenthumb_sms/"; // server address
      }

      _createClass(SmsProvider, [{
        key: "smsData",
        value: function smsData(body, file) {
          var type = "application/json; charset=UTF-8";
          var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({
            'Content-Type': type
          });
          var options = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({
            headers: headers
          });
          console.log("hello samantha:" + this.server + file);
          return this.http.post(this.server + file, JSON.stringify(body), options).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) {
            return res.json();
          }));
        }
      }]);

      return SmsProvider;
    }();

    SmsProvider.ctorParameters = function () {
      return [{
        type: _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"]
      }];
    };

    SmsProvider = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"]])], SmsProvider);
    /***/
  },

  /***/
  0:
  /*!***************************!*\
    !*** multi ./src/main.ts ***!
    \***************************/

  /*! no static exports found */

  /***/
  function _(module, exports, __webpack_require__) {
    module.exports = __webpack_require__(
    /*! /Users/kendrick/Desktop/Telmo Solutions/ForProduction/may102021/last setup/greenthumbapp copy/src/main.ts */
    "./src/main.ts");
    /***/
  },

  /***/
  1:
  /*!********************!*\
    !*** ws (ignored) ***!
    \********************/

  /*! no static exports found */

  /***/
  function _(module, exports) {
    /* (ignored) */

    /***/
  }
}, [[0, "runtime", "vendor"]]]);
//# sourceMappingURL=main-es5.js.map