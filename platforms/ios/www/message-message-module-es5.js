function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["message-message-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/message/message.page.html":
  /*!*********************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/message/message.page.html ***!
    \*********************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppMessageMessagePageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header  mode=\"ios\" no-border>\n  <ion-toolbar mode=\"ios\">\n      <ion-buttons slot=\"secondary\">\n        <ion-button mode=\"ios\" style=\"font-size: 15px;\" color=\"dark\" (click)=\"goTab5()\">\n          Back\n      </ion-button>\n      </ion-buttons>\n      \n      <ion-title color=\"secondary\">{{nickname}}</ion-title>\n\n      <ion-buttons slot=\"primary\" >\n        <ion-button>\n            <img src=\"assets/greenthumb-images/greenthumblogo.png\" style=\"width: 32px;border-radius: 50%;\" mode=\"ios\">\n        </ion-button>\n      </ion-buttons>\n    </ion-toolbar>\n</ion-header>\n \n<ion-content>\n \n  <ion-grid>\n    <!-- <ion-text color=\"medium\" text-center>\n      <p>You joined the chat as {{ currentUser }}</p>\n    </ion-text> -->\n    <!-- <ion-row *ngFor=\"let messageFDB of messagesFromDB\">\n \n      <ion-col size=\"9\" *ngIf=\"messageFDB?.messaged_user_id !== user_id\" class=\"message other-message\">\n        <span>{{ messageFDB?.message }}</span>\n        <div class=\"time\" text-right><br>{{ messageFDB?.date }} {{ messageFDB?.time }}</div>\n      </ion-col>\n \n      <ion-col offset=\"3\" size=\"9\" *ngIf=\"messageFDB?.messaged_user_id === user_id\" class=\"message my-message\">\n        <span>{{ messageFDB?.message }}</span>\n        <div class=\"time\" text-right><br>{{ messageFDB?.date}} {{ messageFDB?.time }}</div>\n      </ion-col>\n \n    </ion-row> -->\n    <ion-row>\n      <ion-col style=\"padding-top: 0;\">\n        <ion-card mode=\"ios\" class=\"for_items\">\n          <ion-card-content no-padding>\n            <ion-row>\n              <ion-col text-left size=\"5\">\n                <ion-item lines=\"none\" class=\"for_itemms\">\n                  <ion-avatar slot=\"start\">\n                    <div *ngIf=\"!picture\">\n                      <img src=\"assets/greenthumb-images/userpic.png\">\n                    </div>\n                    <div *ngIf=\"picture\">\n                      <img [src] = \"complete_pic\">\n                    </div>\n                  </ion-avatar>\n                  <ion-label class=\"for_name\">{{item_seller}}</ion-label>\n                </ion-item>\n                <img  class=\"for_itemmimg\" [src] = \"item_cover_photo\">\n              </ion-col>\n              <ion-col text-left size=\"7\">\n                <ion-row >\n                  <ion-col no-padding text-left>\n                    <div class=\"for_unitqty\">{{item_quantity}} \n                      <label *ngIf=\"stocks_id=='1'\">\n                        <label *ngIf=\"item_quantity>1\">pieces</label>\n                        <label *ngIf=\"item_quantity<=1\">piece</label>\n                      </label>\n                      <label *ngIf=\"stocks_id=='2'\">\n                        <label *ngIf=\"item_quantity>1\">pounds</label>\n                        <label *ngIf=\"item_quantity<=1\">pound</label>\n                      </label>\n                      <label *ngIf=\"stocks_id=='3'\">\n                        <label *ngIf=\"item_quantity>1\">ounces</label>\n                        <label *ngIf=\"item_quantity<=1\">ounce</label>\n                      </label>\n                      <label *ngIf=\"stocks_id=='4'\">\n                        {{item_other_stock}}\n                      </label>\n                    </div>\n                    <ion-label color=\"secondary\"><h1 class=\"for_pprice\">${{item_price}}</h1></ion-label>\n                  </ion-col>\n                  <ion-col no-padding text-right>\n                    <div class=\"for_category\">{{item_category}}</div>\n                    <div style=\"margin-top: 5px;\">\n                      <ionic4-star-rating #rating\n                        activeIcon = \"ios-star\"\n                        defaultIcon = \"ios-star-outline\"\n                        activeColor = \"#ffce00\" \n                        defaultColor = \"#ffce00\"\n                        readonly=\"false\"\n                        rating=\"{{user_rating}}\"\n                        fontSize = \"15px\">\n                      </ionic4-star-rating>\n                    </div>\n                  </ion-col>\n                </ion-row>\n                <ion-row style=\"margin-top: -2px;\">\n                  <ion-col no-padding text-left>\n                    <ion-label color=\"dark\">\n                      <h2 class=\"for_itemmname\">{{item_name}}</h2></ion-label>\n                  </ion-col>\n                </ion-row>\n                <ion-label><h2 class=\"for_itemmlocation\">\n                  <ion-icon class=\"for_pin\" mode=\"ios\" color=\"primary\" name=\"pin\"></ion-icon>&nbsp;{{item_location}}\n                </h2></ion-label>\n                <ion-row>\n                  <ion-col no-padding text-left size=\"9\" class=\"for_borderitem\">\n                    <ion-row>\n                      <ion-col text-left no-padding>\n                        <ion-label color=\"secondary\"><h2 class=\"for_itemmbtn\">\n                            &nbsp;{{item_date}}\n                          </h2></ion-label>\n                      </ion-col>\n                      <ion-col text-left no-padding>\n                        <ion-label color=\"secondary\"><h2 class=\"for_itemmbtn\">\n                            &nbsp;{{item_time}}\n                          </h2></ion-label>\n                      </ion-col>\n                    </ion-row>\n                  </ion-col>\n                  <ion-col no-padding text-center size=\"3\">\n                    <ion-icon style=\"zoom: 2;margin-top: -5px;\" name=\"heart\" mode=\"ios\" class=\"myHeartUnSave\"></ion-icon>\n                  </ion-col>\n                </ion-row>\n\n                <ion-row *ngIf=\"withPurchasedPending\">\n                  <ion-col no-padding text-left>\n                    <ion-label color=\"dark\">\n                      <h2 class=\"for_itemmname\">Added to cart : {{quantity_purchased}}</h2>\n                      <h2 class=\"for_itemmname\"> $ {{total_amount}}</h2>\n                    </ion-label>\n                  </ion-col>\n                  <ion-col size=\"6\" no-padding *ngIf=\"ownProfile\">\n                    <ion-button expand=\"block\" *ngIf=\"!alreadySold\" mode=\"ios\" (click)=\"confirmMarkSold()\" >\n                      &nbsp;&nbsp;Mark Sold\n                    </ion-button>\n                    <ion-button expand=\"block\" *ngIf=\"alreadySold\" mode=\"ios\" >\n                      &nbsp;&nbsp;Sold\n                    </ion-button>\n                  </ion-col>\n                </ion-row>\n              </ion-col>\n            </ion-row>\n          </ion-card-content>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n    <div class=\"for_rowpadding\" *ngFor=\"let messageFDB of messagesFromDB\">\n      <div *ngIf=\"messageFDB?.message!=''\">\n        <div class=\"yours messages\" *ngIf=\"messageFDB?.messaged_user_id !== user_id\">\n          <ion-item lines=\"none\" class=\"for_rowitem\">\n            <ion-avatar *ngIf=\"!conv_image\" id=\"for_yourspic\" slot=\"start\">\n              <img src=\"assets/greenthumb-images/userpicdark.png\">\n            </ion-avatar>\n            <ion-avatar *ngIf=\"conv_image\" id=\"for_yourspic\" slot=\"start\">\n              <img [src]=\"conv_image\">\n            </ion-avatar>\n            <div class=\"message last\">\n              {{ messageFDB?.message }}<br>\n              <ion-label class=\"for_yourstime\">{{ messageFDB?.date }} {{ messageFDB?.time }}</ion-label>\n            </div>\n          </ion-item>\n        </div>\n        <div class=\"mine messages\" *ngIf=\"messageFDB?.messaged_user_id === user_id\">\n          <ion-item lines=\"none\" class=\"for_rowitem\">\n            <div class=\"message last\">\n              {{ messageFDB?.message }} <br>\n              <ion-label class=\"for_minetime\">{{ messageFDB?.date}} {{ messageFDB?.time }}</ion-label>\n            </div>\n          </ion-item>\n        </div>\n      </div>\n      \n    </div>\n\n    <!-- for new messages -->\n    \n    <!-- <ion-row *ngFor=\"let message of messages\">\n \n      <ion-col size=\"9\" *ngIf=\"message.user_id !== user_id\" class=\"message other-message\">\n        <span>{{ message.msg }}</span>\n        <div class=\"time\" text-right><br>{{ message.createdAt | date:'short' }}</div>\n      </ion-col>\n \n      <ion-col offset=\"3\" size=\"9\" *ngIf=\"message.user_id === user_id\" class=\"message my-message\">\n        <span>{{ message.msg }}</span>\n        <div class=\"time\" text-right><br>{{ message.createdAt | date:'short' }}</div>\n      </ion-col>\n \n    </ion-row> -->\n    <div class=\"for_rowpadding\" *ngFor=\"let message of messages\">\n      <div class=\"yours messages\" *ngIf=\"message.user_id !== user_id\">\n        <ion-item lines=\"none\" class=\"for_rowitem\">\n          <ion-avatar id=\"for_yourspic\" slot=\"start\">\n            <img src=\"assets/greenthumb-images/userpicdark.png\">\n          </ion-avatar>\n          <div class=\"message last\">\n            {{ message.msg }} <br>\n            <ion-label class=\"for_yourstime\">{{ message.createdAt | date:'short' }}</ion-label>\n          </div>\n        </ion-item>\n      </div>\n      <div class=\"mine messages\" *ngIf=\"message.user_id === user_id\">\n        <ion-item lines=\"none\" class=\"for_rowitem\">\n          <div class=\"message last\">\n            {{ message.msg }} <br>\n            <ion-label class=\"for_minetime\">{{ message.createdAt | date:'short' }}</ion-label>\n          </div>\n        </ion-item>\n      </div>\n    </div>\n    \n  </ion-grid>\n  <ion-grid style=\"text-align: center;\" *ngIf=\"loader\"> \n\n    <ion-spinner name=\"crescent\"></ion-spinner>\n</ion-grid>\n</ion-content>\n \n<ion-footer>\n  \n  <ion-toolbar color=\"light\">\n    <ion-row align-items-center>\n      <ion-col size=\"10\">\n        <ion-textarea auto-grow class=\"message-input\" rows=\"1\" [(ngModel)]=\"message\"></ion-textarea>\n      </ion-col>\n      <ion-col size=\"2\">\n        <ion-button expand=\"block\" fill=\"clear\" color=\"primary\" [disabled]=\"message === ''\" class=\"msg-btn\"\n          (click)=\"sendMessage()\">\n          <ion-icon name=\"ios-send\" slot=\"icon-only\"></ion-icon>\n        </ion-button>\n      </ion-col>\n    </ion-row>\n  </ion-toolbar>\n</ion-footer>\n\n\n\n\n\n\n\n\n<!-- <ion-header mode=\"ios\">\n  <ion-row>\n    <ion-col size=\"9\"  mode=\"ios\">\n        <ion-item lines=\"none\">\n            <ion-avatar slot=\"start\" style=\"min-width: 3rem;min-height: 3rem;\">\n                <img src=\"assets/icon/profilee.png\"  mode=\"ios\">\n              </ion-avatar>\n          <ion-label>\n            <h2>Lalisa Manuban</h2>\n            <p style=\"font-size: 15px;text-align: right;\">\n                <ion-icon name=\"eye\" style=\"font-size: 17px\" mode=\"ios\"></ion-icon>&nbsp;8k\n                    <ion-icon name=\"heart\" style=\"font-size: 17px\" mode=\"ios\"></ion-icon>&nbsp;8k\n                    <ion-icon name=\"trophy\" style=\"font-size: 17px\" mode=\"ios\"></ion-icon>&nbsp;26\n                   </p>\n          </ion-label>\n        </ion-item>\n    </ion-col>\n    <ion-col  mode=\"ios\" text-right padding style=\"padding-top: 5%;\">\n       <ion-icon name=\"heart\" style=\"color: #98989d;font-size:40px;\"  mode=\"ios\"></ion-icon>  \n    </ion-col>\n  </ion-row>\n</ion-header>\n\n<ion-content mode=\"ios\">\n  <img src=\"assets/icon/girl.gif\" style=\"height: 100%;\">\n  <div style=\"background: white;bottom: 0; position: fixed;width: 100%;height: 10%;box-shadow: 5px 2px 8px black;\" padding-bottom>\n  \n      <ion-item no-lines>\n          <ion-icon slot=\"start\" mode=\"ios\" (click)=\"goTab5()\" style=\"color: #98989d;font-size: 30px;vertical-align: middle;\" name=\"arrow-back\"></ion-icon>\n          <ion-input mode=\"ios\" placeholder=\"Write message\" style=\"border: 1xp solid black;width: 100%;\"></ion-input>\n          <ion-icon slot=\"end\" mode=\"ios\" name=\"send\" color=\"secondary\" style=\"font-size: 40px;vertical-align: middle;\" ></ion-icon>\n      </ion-item>\n      \n  </div>\n</ion-content> -->\n";
    /***/
  },

  /***/
  "./src/app/message/message.module.ts":
  /*!*******************************************!*\
    !*** ./src/app/message/message.module.ts ***!
    \*******************************************/

  /*! exports provided: MessagePageModule */

  /***/
  function srcAppMessageMessageModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "MessagePageModule", function () {
      return MessagePageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var ionic4_star_rating__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ionic4-star-rating */
    "./node_modules/ionic4-star-rating/dist/index.js");
    /* harmony import */


    var _message_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./message.page */
    "./src/app/message/message.page.ts");

    var routes = [{
      path: '',
      component: _message_page__WEBPACK_IMPORTED_MODULE_7__["MessagePage"]
    }];

    var MessagePageModule = function MessagePageModule() {
      _classCallCheck(this, MessagePageModule);
    };

    MessagePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], ionic4_star_rating__WEBPACK_IMPORTED_MODULE_6__["StarRatingModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      declarations: [_message_page__WEBPACK_IMPORTED_MODULE_7__["MessagePage"]]
    })], MessagePageModule);
    /***/
  },

  /***/
  "./src/app/message/message.page.scss":
  /*!*******************************************!*\
    !*** ./src/app/message/message.page.scss ***!
    \*******************************************/

  /*! exports provided: default */

  /***/
  function srcAppMessageMessagePageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".my-overlay {\n  position: fixed;\n  width: 98%;\n  z-index: 20;\n  color: black;\n  top: 1%;\n  left: 1%;\n  right: 1%;\n  text-align: center;\n}\n\n.for_rowpadding {\n  padding-left: 5px;\n  padding-right: 5px;\n}\n\n.for_rowitem {\n  --padding-start:0;\n  --backround:transparent;\n  --inner-padding-end: 0;\n}\n\n#for_yourspic {\n  width: 100% !important;\n  height: 100% !important;\n  max-width: 30px !important;\n  max-height: 30px !important;\n  margin-right: 5px;\n  align-self: flex-end;\n}\n\n#for_yourspic img {\n  border: 1px solid rgba(0, 0, 0, 0.12);\n  width: 100% !important;\n  height: 100% !important;\n  max-width: 30px !important;\n  max-height: 30px !important;\n  border-radius: 50%;\n}\n\n.for_minetime {\n  font-size: 11px;\n  padding: 0;\n  margin: 0;\n  text-align: right;\n  margin-top: 10px;\n  color: rgba(255, 255, 255, 0.7);\n}\n\n.for_yourstime {\n  font-size: 11px;\n  padding: 0;\n  margin: 0;\n  text-align: right;\n  margin-top: 10px;\n  color: #a5a5a5;\n}\n\n.messages {\n  margin-top: 5px;\n  display: flex;\n  flex-direction: column;\n}\n\n.message {\n  border-radius: 15px;\n  padding: 8px 15px;\n  margin-top: 5px;\n  margin-bottom: 5px;\n  font-size: 14px;\n}\n\n.yours {\n  align-items: flex-start;\n}\n\n.yours .message {\n  background-color: #eee;\n  position: relative;\n  margin-right: 60px;\n}\n\n.mine {\n  align-items: flex-end;\n}\n\n.mine .message {\n  color: white;\n  background: #76c961e8;\n  background-attachment: fixed;\n  position: relative;\n  margin-left: 60px;\n}\n\n.yours .message.last:before {\n  content: \"\";\n  position: absolute;\n  z-index: 0;\n  bottom: 0;\n  left: -7px;\n  height: 20px;\n  width: 20px;\n  background: #eee;\n  border-bottom-right-radius: 15px;\n}\n\n.yours .message.last:after {\n  content: \"\";\n  position: absolute;\n  z-index: 1;\n  bottom: 0;\n  left: -10px;\n  width: 10px;\n  height: 20px;\n  background: white;\n  border-bottom-right-radius: 10px;\n}\n\n.mine .message.last:before {\n  content: \"\";\n  position: absolute;\n  z-index: 0;\n  bottom: 0;\n  right: -8px;\n  height: 20px;\n  width: 20px;\n  background: #76c961e8;\n  background-attachment: fixed;\n  border-bottom-left-radius: 15px;\n}\n\n.mine .message.last:after {\n  content: \"\";\n  position: absolute;\n  z-index: 1;\n  bottom: 0;\n  right: -10px;\n  width: 10px;\n  height: 20px;\n  background: white;\n  border-bottom-left-radius: 10px;\n}\n\n.message-input {\n  margin-top: 0px;\n  border: 1px solid var(--ion-color-medium);\n  border-radius: 10px;\n  background: #fff;\n}\n\n.msg-btn {\n  --padding-start: 0.5em;\n  --padding-end: 0.5em;\n}\n\n.myHeartUnSave {\n  color: #d3dbc9;\n}\n\n.myHeartSave {\n  color: red;\n}\n\n.for_items {\n  box-shadow: none;\n  border-radius: 0;\n  border: 1px solid #e2f0cb6e;\n  border-bottom: 1px solid #e2f0cb6e;\n  border-top: 1px solid #e2f0cb;\n  margin: 0;\n  margin-left: -3%;\n  margin-right: -3%;\n}\n\n.for_itemms {\n  --padding-start: 0;\n  margin-top: -8px;\n  --inner-padding-end: 0;\n  --background: transparent;\n}\n\n.for_itemms ion-avatar {\n  width: 100% !important;\n  height: 100% !important;\n  max-width: 25px !important;\n  max-height: 25px !important;\n  margin-right: 5px;\n  margin-top: 0;\n}\n\n.for_itemms ion-avatar img {\n  border: 1px solid rgba(0, 0, 0, 0.12);\n  width: 100% !important;\n  height: 100% !important;\n  max-width: 25px !important;\n  max-height: 25px !important;\n  margin-right: 5px;\n  margin-top: 0;\n  border-radius: 50%;\n}\n\n.for_name {\n  font-size: 12px;\n  text-transform: capitalize;\n  margin-top: 0;\n  color: black;\n}\n\n.for_itemmimg {\n  margin: auto;\n  width: 90%;\n  margin-top: -10px;\n  padding-bottom: 5px;\n}\n\n.for_pprice {\n  font-size: 23px;\n  font-weight: bolder;\n}\n\n.for_category {\n  background: #76c961;\n  padding: 3px;\n  border-top-right-radius: 10px;\n  margin-top: -5px;\n  margin-right: -5px;\n  border-bottom-left-radius: 10px;\n  text-transform: capitalize;\n  font-size: 12px;\n  font-weight: lighter;\n  color: white;\n  text-align: center;\n}\n\n.for_itemmname {\n  font-size: 15px;\n  font-weight: bolder;\n}\n\n.for_itemmlocation {\n  font-size: 12.5px;\n  font-weight: lighter;\n  color: #989aa2;\n  padding-right: 5px;\n  padding-bottom: 2.5%;\n}\n\n.for_borderitem {\n  border-top: 1px solid #e2f0cb;\n  padding-top: 2.5%;\n}\n\n.for_itemmbtn {\n  font-size: 11px;\n  color: #679733;\n}\n\n.for_unitqty {\n  font-size: 11px;\n  text-align: left;\n  color: #989aa2;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZW5kcmljay9EZXNrdG9wL1RlbG1vIFNvbHV0aW9ucy9Gb3JQcm9kdWN0aW9uL21heTEwMjAyMS9sYXN0IHNldHVwL2dyZWVudGh1bWJhcHAgY29weS9zcmMvYXBwL21lc3NhZ2UvbWVzc2FnZS5wYWdlLnNjc3MiLCJzcmMvYXBwL21lc3NhZ2UvbWVzc2FnZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxlQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsT0FBQTtFQUNBLFFBQUE7RUFDQSxTQUFBO0VBQ0Esa0JBQUE7QUNDSjs7QURDQTtFQUNFLGlCQUFBO0VBQ0Esa0JBQUE7QUNFRjs7QURBQTtFQUNFLGlCQUFBO0VBQ0EsdUJBQUE7RUFDQSxzQkFBQTtBQ0dGOztBRERBO0VBQ0Usc0JBQUE7RUFDQSx1QkFBQTtFQUNBLDBCQUFBO0VBQ0EsMkJBQUE7RUFDQSxpQkFBQTtFQUNBLG9CQUFBO0FDSUY7O0FESEU7RUFDSSxxQ0FBQTtFQUNBLHNCQUFBO0VBQ0EsdUJBQUE7RUFDQSwwQkFBQTtFQUNBLDJCQUFBO0VBQ0Esa0JBQUE7QUNLTjs7QURGQTtFQUNFLGVBQUE7RUFDQSxVQUFBO0VBQ0EsU0FBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSwrQkFBQTtBQ0tGOztBREhBO0VBQ0UsZUFBQTtFQUNBLFVBQUE7RUFDQSxTQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7QUNNRjs7QURIQTtFQUNFLGVBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7QUNNRjs7QURIQTtFQUNFLG1CQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0FDTUY7O0FESEE7RUFDRSx1QkFBQTtBQ01GOztBREhBO0VBQ0Usc0JBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0FDTUY7O0FESEE7RUFDRSxxQkFBQTtBQ01GOztBREhBO0VBQ0UsWUFBQTtFQUNBLHFCQUFBO0VBQ0EsNEJBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0FDTUY7O0FESEE7RUFDRSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxVQUFBO0VBQ0EsU0FBQTtFQUNBLFVBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0VBQ0EsZ0NBQUE7QUNNRjs7QURKQTtFQUNFLFdBQUE7RUFDQSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxTQUFBO0VBQ0EsV0FBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQ0FBQTtBQ09GOztBREpBO0VBQ0UsV0FBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLFNBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxxQkFBQTtFQUNBLDRCQUFBO0VBQ0EsK0JBQUE7QUNPRjs7QURKQTtFQUNFLFdBQUE7RUFDQSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxTQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSwrQkFBQTtBQ09GOztBRG1CRTtFQUNFLGVBQUE7RUFDQSx5Q0FBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7QUNoQko7O0FEbUJFO0VBQ0Usc0JBQUE7RUFDQSxvQkFBQTtBQ2hCSjs7QURxQkE7RUFDSSxjQUFBO0FDbEJKOztBRG9CQTtFQUNJLFVBQUE7QUNqQko7O0FEbUJFO0VBTUUsZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLDJCQUFBO0VBQ0Esa0NBQUE7RUFDQSw2QkFBQTtFQUNBLFNBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0FDckJKOztBRHVCRTtFQUNFLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxzQkFBQTtFQUNBLHlCQUFBO0FDcEJKOztBRHFCSTtFQUNJLHNCQUFBO0VBQ0EsdUJBQUE7RUFDQSwwQkFBQTtFQUNBLDJCQUFBO0VBQ0EsaUJBQUE7RUFBa0IsYUFBQTtBQ2xCMUI7O0FEbUJRO0VBQ0kscUNBQUE7RUFDQSxzQkFBQTtFQUNBLHVCQUFBO0VBQ0EsMEJBQUE7RUFDQSwyQkFBQTtFQUNBLGlCQUFBO0VBQWtCLGFBQUE7RUFDbEIsa0JBQUE7QUNoQlo7O0FEcUJBO0VBQ0ksZUFBQTtFQUNBLDBCQUFBO0VBQ0EsYUFBQTtFQUNBLFlBQUE7QUNsQko7O0FEb0JBO0VBQ0ksWUFBQTtFQUNBLFVBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0FDakJKOztBRG1CQTtFQUNFLGVBQUE7RUFDQSxtQkFBQTtBQ2hCRjs7QURrQkE7RUFDRSxtQkFBQTtFQUNBLFlBQUE7RUFDQSw2QkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSwrQkFBQTtFQUNBLDBCQUFBO0VBQ0EsZUFBQTtFQUNBLG9CQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0FDZkY7O0FEaUJBO0VBQ0UsZUFBQTtFQUNBLG1CQUFBO0FDZEY7O0FEZ0JBO0VBQ0UsaUJBQUE7RUFDQSxvQkFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtFQUNBLG9CQUFBO0FDYkY7O0FEZUE7RUFDSSw2QkFBQTtFQUNBLGlCQUFBO0FDWko7O0FEY0E7RUFDSSxlQUFBO0VBQ0EsY0FBQTtBQ1hKOztBRGFBO0VBQ0ksZUFBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtBQ1ZKIiwiZmlsZSI6InNyYy9hcHAvbWVzc2FnZS9tZXNzYWdlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5teS1vdmVybGF5IHtcbiAgICBwb3NpdGlvbjogZml4ZWQ7XG4gICAgd2lkdGg6IDk4JTtcbiAgICB6LWluZGV4OiAyMDtcbiAgICBjb2xvcjogYmxhY2s7XG4gICAgdG9wOiAxJTtcbiAgICBsZWZ0OiAxJTtcbiAgICByaWdodDogMSU7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLmZvcl9yb3dwYWRkaW5ne1xuICBwYWRkaW5nLWxlZnQ6IDVweDtcbiAgcGFkZGluZy1yaWdodDogNXB4O1xufVxuLmZvcl9yb3dpdGVte1xuICAtLXBhZGRpbmctc3RhcnQ6MDtcbiAgLS1iYWNrcm91bmQ6dHJhbnNwYXJlbnQ7XG4gIC0taW5uZXItcGFkZGluZy1lbmQ6IDA7XG59XG4jZm9yX3lvdXJzcGljICB7ICAgICBcbiAgd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcbiAgaGVpZ2h0OiAxMDAlICFpbXBvcnRhbnQ7XG4gIG1heC13aWR0aDogMzBweCAhaW1wb3J0YW50O1xuICBtYXgtaGVpZ2h0OiAzMHB4ICFpbXBvcnRhbnQ7XG4gIG1hcmdpbi1yaWdodDogNXB4O1xuICBhbGlnbi1zZWxmOiBmbGV4LWVuZDtcbiAgaW1ne1xuICAgICAgYm9yZGVyOiAxcHggc29saWQgcmdiYSgwLCAwLCAwLCAwLjEyKTtcbiAgICAgIHdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XG4gICAgICBoZWlnaHQ6IDEwMCUgIWltcG9ydGFudDtcbiAgICAgIG1heC13aWR0aDogMzBweCAhaW1wb3J0YW50O1xuICAgICAgbWF4LWhlaWdodDogMzBweCAhaW1wb3J0YW50O1xuICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xuICB9XG59XG4uZm9yX21pbmV0aW1le1xuICBmb250LXNpemU6IDExcHg7XG4gIHBhZGRpbmc6IDA7XG4gIG1hcmdpbjogMDtcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG4gIGNvbG9yOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuNyk7XG59XG4uZm9yX3lvdXJzdGltZXtcbiAgZm9udC1zaXplOiAxMXB4O1xuICBwYWRkaW5nOiAwO1xuICBtYXJnaW46IDA7XG4gIHRleHQtYWxpZ246IHJpZ2h0O1xuICBtYXJnaW4tdG9wOiAxMHB4O1xuICBjb2xvcjogI2E1YTVhNTtcbn1cblxuLm1lc3NhZ2VzIHtcbiAgbWFyZ2luLXRvcDogNXB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xufVxuXG4ubWVzc2FnZSB7XG4gIGJvcmRlci1yYWRpdXM6IDE1cHg7XG4gIHBhZGRpbmc6IDhweCAxNXB4O1xuICBtYXJnaW4tdG9wOiA1cHg7XG4gIG1hcmdpbi1ib3R0b206IDVweDtcbiAgZm9udC1zaXplOiAxNHB4O1xufVxuXG4ueW91cnMge1xuICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcbn1cblxuLnlvdXJzIC5tZXNzYWdlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2VlZTtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBtYXJnaW4tcmlnaHQ6IDYwcHg7XG59XG5cbi5taW5lIHtcbiAgYWxpZ24taXRlbXM6IGZsZXgtZW5kO1xufVxuXG4ubWluZSAubWVzc2FnZSB7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgYmFja2dyb3VuZDogIzc2Yzk2MWU4O1xuICBiYWNrZ3JvdW5kLWF0dGFjaG1lbnQ6IGZpeGVkO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIG1hcmdpbi1sZWZ0OiA2MHB4O1xufVxuXG4ueW91cnMgLm1lc3NhZ2UubGFzdDpiZWZvcmUge1xuICBjb250ZW50OiBcIlwiO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHotaW5kZXg6IDA7XG4gIGJvdHRvbTogMDtcbiAgbGVmdDogLTdweDtcbiAgaGVpZ2h0OiAyMHB4O1xuICB3aWR0aDogMjBweDtcbiAgYmFja2dyb3VuZDogI2VlZTtcbiAgYm9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6IDE1cHg7XG59XG4ueW91cnMgLm1lc3NhZ2UubGFzdDphZnRlciB7XG4gIGNvbnRlbnQ6IFwiXCI7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgei1pbmRleDogMTtcbiAgYm90dG9tOiAwO1xuICBsZWZ0OiAtMTBweDtcbiAgd2lkdGg6IDEwcHg7XG4gIGhlaWdodDogMjBweDtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIGJvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOiAxMHB4O1xufVxuXG4ubWluZSAubWVzc2FnZS5sYXN0OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXCI7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgei1pbmRleDogMDtcbiAgYm90dG9tOiAwO1xuICByaWdodDogLThweDtcbiAgaGVpZ2h0OiAyMHB4O1xuICB3aWR0aDogMjBweDtcbiAgYmFja2dyb3VuZDogIzc2Yzk2MWU4O1xuICBiYWNrZ3JvdW5kLWF0dGFjaG1lbnQ6IGZpeGVkO1xuICBib3JkZXItYm90dG9tLWxlZnQtcmFkaXVzOiAxNXB4O1xufVxuXG4ubWluZSAubWVzc2FnZS5sYXN0OmFmdGVyIHtcbiAgY29udGVudDogXCJcIjtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB6LWluZGV4OiAxO1xuICBib3R0b206IDA7XG4gIHJpZ2h0OiAtMTBweDtcbiAgd2lkdGg6IDEwcHg7XG4gIGhlaWdodDogMjBweDtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIGJvcmRlci1ib3R0b20tbGVmdC1yYWRpdXM6IDEwcHg7XG59XG5cbi8vIC5tZXNzYWdlIHtcbi8vICAgICBwYWRkaW5nOiAxMHB4O1xuLy8gICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4vLyAgICAgbWFyZ2luLWJvdHRvbTogNHB4O1xuLy8gICAgIHdoaXRlLXNwYWNlOiBwcmUtd3JhcDtcbi8vICAgfVxuICAgXG4vLyAgIC5teS1tZXNzYWdlIHtcbi8vICAgICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItdGVydGlhcnkpO1xuLy8gICAgIGNvbG9yOiAjZmZmO1xuLy8gICB9XG4gICBcbi8vICAgLm90aGVyLW1lc3NhZ2Uge1xuLy8gICAgIGJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1zZWNvbmRhcnkpO1xuLy8gICAgIGNvbG9yOiAjZmZmO1xuLy8gICB9XG4gICBcbi8vICAgLnRpbWUge1xuLy8gICAgIGNvbG9yOiAjZGZkZmRmO1xuLy8gICAgIGZsb2F0OiByaWdodDtcbi8vICAgICBmb250LXNpemU6IHNtYWxsO1xuLy8gICB9XG4gICBcbiAgLm1lc3NhZ2UtaW5wdXQge1xuICAgIG1hcmdpbi10b3A6IDBweDtcbiAgICBib3JkZXI6IDFweCBzb2xpZCB2YXIoLS1pb24tY29sb3ItbWVkaXVtKTtcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgIGJhY2tncm91bmQ6ICNmZmY7XG4gIH1cbiAgIFxuICAubXNnLWJ0biB7XG4gICAgLS1wYWRkaW5nLXN0YXJ0OiAwLjVlbTtcbiAgICAtLXBhZGRpbmctZW5kOiAwLjVlbTtcbiAgfVxuXG5cblxuLm15SGVhcnRVblNhdmV7XG4gICAgY29sb3I6ICNkM2RiYzk7XG59XG4ubXlIZWFydFNhdmV7XG4gICAgY29sb3I6IHJlZDtcbn1cbiAgLmZvcl9pdGVtc3tcbiAgICAvLyBtYXJnaW4tdG9wOiAwJTtcbiAgICAvLyBib3gtc2hhZG93OiAwIDFweCAxMHB4ICNlMmYwY2I7XG4gICAgLy8gYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICAvL2JvcmRlcjogMXB4IHNvbGlkICNlMmYwY2I7XG4gICAgLy8gbWFyZ2luLWJvdHRvbTogMDtcbiAgICBib3gtc2hhZG93OiBub25lO1xuICAgIGJvcmRlci1yYWRpdXM6IDA7XG4gICAgYm9yZGVyOiAxcHggc29saWQgI2UyZjBjYjZlO1xuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZTJmMGNiNmU7XG4gICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICNlMmYwY2I7XG4gICAgbWFyZ2luOiAwO1xuICAgIG1hcmdpbi1sZWZ0OiAtMyU7XG4gICAgbWFyZ2luLXJpZ2h0OiAtMyU7XG59XG4gIC5mb3JfaXRlbW1ze1xuICAgIC0tcGFkZGluZy1zdGFydDogMDtcbiAgICBtYXJnaW4tdG9wOiAtOHB4O1xuICAgIC0taW5uZXItcGFkZGluZy1lbmQ6IDA7XG4gICAgLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgICBpb24tYXZhdGFyICB7ICAgICBcbiAgICAgICAgd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcbiAgICAgICAgaGVpZ2h0OiAxMDAlICFpbXBvcnRhbnQ7XG4gICAgICAgIG1heC13aWR0aDogMjVweCAhaW1wb3J0YW50O1xuICAgICAgICBtYXgtaGVpZ2h0OiAyNXB4ICFpbXBvcnRhbnQ7XG4gICAgICAgIG1hcmdpbi1yaWdodDogNXB4O21hcmdpbi10b3A6IDA7XG4gICAgICAgIGltZ3tcbiAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkIHJnYmEoMCwgMCwgMCwgMC4xMik7XG4gICAgICAgICAgICB3aWR0aDogMTAwJSAhaW1wb3J0YW50O1xuICAgICAgICAgICAgaGVpZ2h0OiAxMDAlICFpbXBvcnRhbnQ7XG4gICAgICAgICAgICBtYXgtd2lkdGg6IDI1cHggIWltcG9ydGFudDtcbiAgICAgICAgICAgIG1heC1oZWlnaHQ6IDI1cHggIWltcG9ydGFudDtcbiAgICAgICAgICAgIG1hcmdpbi1yaWdodDogNXB4O21hcmdpbi10b3A6IDA7XG4gICAgICAgICAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgICAgIH1cbiAgICB9XG4gIH1cbiAgXG4uZm9yX25hbWV7XG4gICAgZm9udC1zaXplOiAxMnB4O1xuICAgIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xuICAgIG1hcmdpbi10b3A6IDA7XG4gICAgY29sb3I6IGJsYWNrO1xufVxuLmZvcl9pdGVtbWltZ3tcbiAgICBtYXJnaW46IGF1dG87XG4gICAgd2lkdGg6IDkwJTtcbiAgICBtYXJnaW4tdG9wOiAtMTBweDtcbiAgICBwYWRkaW5nLWJvdHRvbTogNXB4O1xufVxuLmZvcl9wcHJpY2V7XG4gIGZvbnQtc2l6ZTogMjNweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbn1cbi5mb3JfY2F0ZWdvcnl7XG4gIGJhY2tncm91bmQ6ICM3NmM5NjE7XG4gIHBhZGRpbmc6IDNweDtcbiAgYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDEwcHg7XG4gIG1hcmdpbi10b3A6IC01cHg7XG4gIG1hcmdpbi1yaWdodDogLTVweDtcbiAgYm9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czogMTBweDtcbiAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgZm9udC13ZWlnaHQ6IGxpZ2h0ZXI7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLmZvcl9pdGVtbW5hbWV7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbn1cbi5mb3JfaXRlbW1sb2NhdGlvbntcbiAgZm9udC1zaXplOiAxMi41cHg7XG4gIGZvbnQtd2VpZ2h0OiBsaWdodGVyO1xuICBjb2xvcjogIzk4OWFhMjtcbiAgcGFkZGluZy1yaWdodDogNXB4O1xuICBwYWRkaW5nLWJvdHRvbTogMi41JTtcbn1cbi5mb3JfYm9yZGVyaXRlbXtcbiAgICBib3JkZXItdG9wOiAxcHggc29saWQgI2UyZjBjYjtcbiAgICBwYWRkaW5nLXRvcDogMi41JTtcbn1cbi5mb3JfaXRlbW1idG57XG4gICAgZm9udC1zaXplOiAxMXB4O1xuICAgIGNvbG9yOiAjNjc5NzMzO1xufVxuLmZvcl91bml0cXR5e1xuICAgIGZvbnQtc2l6ZTogMTFweDtcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuICAgIGNvbG9yOiAjOTg5YWEyO1xufSIsIi5teS1vdmVybGF5IHtcbiAgcG9zaXRpb246IGZpeGVkO1xuICB3aWR0aDogOTglO1xuICB6LWluZGV4OiAyMDtcbiAgY29sb3I6IGJsYWNrO1xuICB0b3A6IDElO1xuICBsZWZ0OiAxJTtcbiAgcmlnaHQ6IDElO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5mb3Jfcm93cGFkZGluZyB7XG4gIHBhZGRpbmctbGVmdDogNXB4O1xuICBwYWRkaW5nLXJpZ2h0OiA1cHg7XG59XG5cbi5mb3Jfcm93aXRlbSB7XG4gIC0tcGFkZGluZy1zdGFydDowO1xuICAtLWJhY2tyb3VuZDp0cmFuc3BhcmVudDtcbiAgLS1pbm5lci1wYWRkaW5nLWVuZDogMDtcbn1cblxuI2Zvcl95b3Vyc3BpYyB7XG4gIHdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XG4gIGhlaWdodDogMTAwJSAhaW1wb3J0YW50O1xuICBtYXgtd2lkdGg6IDMwcHggIWltcG9ydGFudDtcbiAgbWF4LWhlaWdodDogMzBweCAhaW1wb3J0YW50O1xuICBtYXJnaW4tcmlnaHQ6IDVweDtcbiAgYWxpZ24tc2VsZjogZmxleC1lbmQ7XG59XG4jZm9yX3lvdXJzcGljIGltZyB7XG4gIGJvcmRlcjogMXB4IHNvbGlkIHJnYmEoMCwgMCwgMCwgMC4xMik7XG4gIHdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XG4gIGhlaWdodDogMTAwJSAhaW1wb3J0YW50O1xuICBtYXgtd2lkdGg6IDMwcHggIWltcG9ydGFudDtcbiAgbWF4LWhlaWdodDogMzBweCAhaW1wb3J0YW50O1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG59XG5cbi5mb3JfbWluZXRpbWUge1xuICBmb250LXNpemU6IDExcHg7XG4gIHBhZGRpbmc6IDA7XG4gIG1hcmdpbjogMDtcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG4gIGNvbG9yOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuNyk7XG59XG5cbi5mb3JfeW91cnN0aW1lIHtcbiAgZm9udC1zaXplOiAxMXB4O1xuICBwYWRkaW5nOiAwO1xuICBtYXJnaW46IDA7XG4gIHRleHQtYWxpZ246IHJpZ2h0O1xuICBtYXJnaW4tdG9wOiAxMHB4O1xuICBjb2xvcjogI2E1YTVhNTtcbn1cblxuLm1lc3NhZ2VzIHtcbiAgbWFyZ2luLXRvcDogNXB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xufVxuXG4ubWVzc2FnZSB7XG4gIGJvcmRlci1yYWRpdXM6IDE1cHg7XG4gIHBhZGRpbmc6IDhweCAxNXB4O1xuICBtYXJnaW4tdG9wOiA1cHg7XG4gIG1hcmdpbi1ib3R0b206IDVweDtcbiAgZm9udC1zaXplOiAxNHB4O1xufVxuXG4ueW91cnMge1xuICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcbn1cblxuLnlvdXJzIC5tZXNzYWdlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2VlZTtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBtYXJnaW4tcmlnaHQ6IDYwcHg7XG59XG5cbi5taW5lIHtcbiAgYWxpZ24taXRlbXM6IGZsZXgtZW5kO1xufVxuXG4ubWluZSAubWVzc2FnZSB7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgYmFja2dyb3VuZDogIzc2Yzk2MWU4O1xuICBiYWNrZ3JvdW5kLWF0dGFjaG1lbnQ6IGZpeGVkO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIG1hcmdpbi1sZWZ0OiA2MHB4O1xufVxuXG4ueW91cnMgLm1lc3NhZ2UubGFzdDpiZWZvcmUge1xuICBjb250ZW50OiBcIlwiO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHotaW5kZXg6IDA7XG4gIGJvdHRvbTogMDtcbiAgbGVmdDogLTdweDtcbiAgaGVpZ2h0OiAyMHB4O1xuICB3aWR0aDogMjBweDtcbiAgYmFja2dyb3VuZDogI2VlZTtcbiAgYm9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6IDE1cHg7XG59XG5cbi55b3VycyAubWVzc2FnZS5sYXN0OmFmdGVyIHtcbiAgY29udGVudDogXCJcIjtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB6LWluZGV4OiAxO1xuICBib3R0b206IDA7XG4gIGxlZnQ6IC0xMHB4O1xuICB3aWR0aDogMTBweDtcbiAgaGVpZ2h0OiAyMHB4O1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgYm9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6IDEwcHg7XG59XG5cbi5taW5lIC5tZXNzYWdlLmxhc3Q6YmVmb3JlIHtcbiAgY29udGVudDogXCJcIjtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB6LWluZGV4OiAwO1xuICBib3R0b206IDA7XG4gIHJpZ2h0OiAtOHB4O1xuICBoZWlnaHQ6IDIwcHg7XG4gIHdpZHRoOiAyMHB4O1xuICBiYWNrZ3JvdW5kOiAjNzZjOTYxZTg7XG4gIGJhY2tncm91bmQtYXR0YWNobWVudDogZml4ZWQ7XG4gIGJvcmRlci1ib3R0b20tbGVmdC1yYWRpdXM6IDE1cHg7XG59XG5cbi5taW5lIC5tZXNzYWdlLmxhc3Q6YWZ0ZXIge1xuICBjb250ZW50OiBcIlwiO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHotaW5kZXg6IDE7XG4gIGJvdHRvbTogMDtcbiAgcmlnaHQ6IC0xMHB4O1xuICB3aWR0aDogMTBweDtcbiAgaGVpZ2h0OiAyMHB4O1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgYm9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czogMTBweDtcbn1cblxuLm1lc3NhZ2UtaW5wdXQge1xuICBtYXJnaW4tdG9wOiAwcHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkIHZhcigtLWlvbi1jb2xvci1tZWRpdW0pO1xuICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICBiYWNrZ3JvdW5kOiAjZmZmO1xufVxuXG4ubXNnLWJ0biB7XG4gIC0tcGFkZGluZy1zdGFydDogMC41ZW07XG4gIC0tcGFkZGluZy1lbmQ6IDAuNWVtO1xufVxuXG4ubXlIZWFydFVuU2F2ZSB7XG4gIGNvbG9yOiAjZDNkYmM5O1xufVxuXG4ubXlIZWFydFNhdmUge1xuICBjb2xvcjogcmVkO1xufVxuXG4uZm9yX2l0ZW1zIHtcbiAgYm94LXNoYWRvdzogbm9uZTtcbiAgYm9yZGVyLXJhZGl1czogMDtcbiAgYm9yZGVyOiAxcHggc29saWQgI2UyZjBjYjZlO1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2UyZjBjYjZlO1xuICBib3JkZXItdG9wOiAxcHggc29saWQgI2UyZjBjYjtcbiAgbWFyZ2luOiAwO1xuICBtYXJnaW4tbGVmdDogLTMlO1xuICBtYXJnaW4tcmlnaHQ6IC0zJTtcbn1cblxuLmZvcl9pdGVtbXMge1xuICAtLXBhZGRpbmctc3RhcnQ6IDA7XG4gIG1hcmdpbi10b3A6IC04cHg7XG4gIC0taW5uZXItcGFkZGluZy1lbmQ6IDA7XG4gIC0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG59XG4uZm9yX2l0ZW1tcyBpb24tYXZhdGFyIHtcbiAgd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcbiAgaGVpZ2h0OiAxMDAlICFpbXBvcnRhbnQ7XG4gIG1heC13aWR0aDogMjVweCAhaW1wb3J0YW50O1xuICBtYXgtaGVpZ2h0OiAyNXB4ICFpbXBvcnRhbnQ7XG4gIG1hcmdpbi1yaWdodDogNXB4O1xuICBtYXJnaW4tdG9wOiAwO1xufVxuLmZvcl9pdGVtbXMgaW9uLWF2YXRhciBpbWcge1xuICBib3JkZXI6IDFweCBzb2xpZCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xuICB3aWR0aDogMTAwJSAhaW1wb3J0YW50O1xuICBoZWlnaHQ6IDEwMCUgIWltcG9ydGFudDtcbiAgbWF4LXdpZHRoOiAyNXB4ICFpbXBvcnRhbnQ7XG4gIG1heC1oZWlnaHQ6IDI1cHggIWltcG9ydGFudDtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XG4gIG1hcmdpbi10b3A6IDA7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbn1cblxuLmZvcl9uYW1lIHtcbiAgZm9udC1zaXplOiAxMnB4O1xuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbiAgbWFyZ2luLXRvcDogMDtcbiAgY29sb3I6IGJsYWNrO1xufVxuXG4uZm9yX2l0ZW1taW1nIHtcbiAgbWFyZ2luOiBhdXRvO1xuICB3aWR0aDogOTAlO1xuICBtYXJnaW4tdG9wOiAtMTBweDtcbiAgcGFkZGluZy1ib3R0b206IDVweDtcbn1cblxuLmZvcl9wcHJpY2Uge1xuICBmb250LXNpemU6IDIzcHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkZXI7XG59XG5cbi5mb3JfY2F0ZWdvcnkge1xuICBiYWNrZ3JvdW5kOiAjNzZjOTYxO1xuICBwYWRkaW5nOiAzcHg7XG4gIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiAxMHB4O1xuICBtYXJnaW4tdG9wOiAtNXB4O1xuICBtYXJnaW4tcmlnaHQ6IC01cHg7XG4gIGJvcmRlci1ib3R0b20tbGVmdC1yYWRpdXM6IDEwcHg7XG4gIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xuICBmb250LXNpemU6IDEycHg7XG4gIGZvbnQtd2VpZ2h0OiBsaWdodGVyO1xuICBjb2xvcjogd2hpdGU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmZvcl9pdGVtbW5hbWUge1xuICBmb250LXNpemU6IDE1cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkZXI7XG59XG5cbi5mb3JfaXRlbW1sb2NhdGlvbiB7XG4gIGZvbnQtc2l6ZTogMTIuNXB4O1xuICBmb250LXdlaWdodDogbGlnaHRlcjtcbiAgY29sb3I6ICM5ODlhYTI7XG4gIHBhZGRpbmctcmlnaHQ6IDVweDtcbiAgcGFkZGluZy1ib3R0b206IDIuNSU7XG59XG5cbi5mb3JfYm9yZGVyaXRlbSB7XG4gIGJvcmRlci10b3A6IDFweCBzb2xpZCAjZTJmMGNiO1xuICBwYWRkaW5nLXRvcDogMi41JTtcbn1cblxuLmZvcl9pdGVtbWJ0biB7XG4gIGZvbnQtc2l6ZTogMTFweDtcbiAgY29sb3I6ICM2Nzk3MzM7XG59XG5cbi5mb3JfdW5pdHF0eSB7XG4gIGZvbnQtc2l6ZTogMTFweDtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgY29sb3I6ICM5ODlhYTI7XG59Il19 */";
    /***/
  },

  /***/
  "./src/app/message/message.page.ts":
  /*!*****************************************!*\
    !*** ./src/app/message/message.page.ts ***!
    \*****************************************/

  /*! exports provided: MessagePage */

  /***/
  function srcAppMessageMessagePageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "MessagePage", function () {
      return MessagePage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var ngx_socket_io__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ngx-socket-io */
    "./node_modules/ngx-socket-io/fesm2015/ngx-socket-io.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _providers_chat_messages_provider__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../providers/chat-messages.provider */
    "./src/providers/chat-messages.provider.ts");
    /* harmony import */


    var _providers_credential_provider__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../../providers/credential-provider */
    "./src/providers/credential-provider.ts");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var _shared_model_chat_model__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ../shared/model/chat.model */
    "./src/app/shared/model/chat.model.ts");

    var MessagePage = /*#__PURE__*/function () {
      function MessagePage(router, chatMessages, storage, alertCtrl, activatedRoute, postPvdr, socket, toastCtrl, navCtrl) {
        _classCallCheck(this, MessagePage);

        this.router = router;
        this.chatMessages = chatMessages;
        this.storage = storage;
        this.alertCtrl = alertCtrl;
        this.activatedRoute = activatedRoute;
        this.postPvdr = postPvdr;
        this.socket = socket;
        this.toastCtrl = toastCtrl;
        this.navCtrl = navCtrl;
        this.message = '';
        this.messages = [];
        this.currentUser = '';
        this.receiversId = '';
        this.session_id = '';
        this.user_id = '';
        this.item_id = '';
        this.nickname = '';
        this.conv_image = '';
        this.messagesFromDB = [];
        this.item_name = "";
        this.item_category = "";
        this.item_price = "";
        this.item_seller = "";
        this.item_user_id = "";
        this.item_saveStatus = "";
        this.item_status = "";
        this.item_description = "";
        this.item_location = "";
        this.item_date = "";
        this.item_time = "";
        this.stocks_id = "";
        this.item_cover_photo = "";
        this.loader = false;
        this.withPurchasedPending = false;
        this.ownProfile = false;
        this.alreadySold = false;
        this.fromPurchase = false;
      }

      _createClass(MessagePage, [{
        key: "goTab5",
        value: function goTab5() {
          //this.router.navigate(['tabs/tab5']);
          window.history.back(); //this.navCtrl.navigateRoot(['tabs/tab5']);
        } // sendMessage() {
        //   let body = {
        //     action : "addMessage"
        //   };
        //   this.chatMessages.sendMessage(body, 'credentials-api.php').subscribe(data => {
        //     if(data.success){
        //     }
        //   });
        // }

      }, {
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "ngOnDestroy",
        value: function ngOnDestroy() {
          this.socket.disconnect();
        }
      }, {
        key: "ionViewWillEnter",
        value: function ionViewWillEnter() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
            var paramId, res;
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    paramId = this.activatedRoute.snapshot.paramMap.get("id");
                    res = paramId.split("-separator-"); // this.livestreamId = res[0];

                    this.item_id = res[1];
                    this.receiversId = res[0];
                    this.fromPurchase = res[2] == '1' ? true : false;
                    _context.next = 7;
                    return this.plotData(this.receiversId);

                  case 7:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        }
      }, {
        key: "sendMessage",
        value: function sendMessage() {
          var _this = this;

          this.loader = true;
          this.socket.emit('send-message', {
            user_id: this.user_id,
            room: this.session_id,
            text: this.message,
            vip_packageOfUser: ""
          });
          var body = {
            action: 'insertConvo',
            user_id: this.user_id,
            receiversId: this.receiversId,
            session_id: this.session_id,
            message: this.message,
            item_id: this.item_id
          };
          console.log("sessionId in inserting new message:" + this.session_id);
          this.postPvdr.postData(body, 'messages.php').subscribe(function (data) {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      if (data.success) {}

                    case 1:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2);
            }));
          });
          this.message = '';
        }
      }, {
        key: "ionViewWillLeave",
        value: function ionViewWillLeave() {
          var _this2 = this;

          console.log("will leave message and disconnect to socket");

          if (this.session_id != '') {
            var body = {
              action: 'updateConvoStatus',
              receiversId: this.receiversId,
              session_id: this.session_id
            };
            this.postPvdr.postData(body, 'messages.php').subscribe(function (data) {
              return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
                return regeneratorRuntime.wrap(function _callee3$(_context3) {
                  while (1) {
                    switch (_context3.prev = _context3.next) {
                      case 0:
                        if (data.success) {}

                      case 1:
                      case "end":
                        return _context3.stop();
                    }
                  }
                }, _callee3);
              }));
            });
          }

          this.socket.disconnect();
        }
      }, {
        key: "plotData",
        value: function plotData(val) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee12() {
            var _this3 = this;

            var body2;
            return regeneratorRuntime.wrap(function _callee12$(_context12) {
              while (1) {
                switch (_context12.prev = _context12.next) {
                  case 0:
                    _context12.next = 2;
                    return this.storage.get("greenthumb_user_id").then(function (user_id) {
                      return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this3, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee10() {
                        var _this4 = this;

                        var body, body123, body212;
                        return regeneratorRuntime.wrap(function _callee10$(_context10) {
                          while (1) {
                            switch (_context10.prev = _context10.next) {
                              case 0:
                                this.login_user_id = user_id;
                                body = {
                                  action: 'getSessionId',
                                  user_id: user_id,
                                  receiversId: val,
                                  item_id: this.item_id
                                };
                                console.log("getSession:" + JSON.stringify(body));
                                this.postPvdr.postData(body, 'messages.php').subscribe(function (data) {
                                  return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this4, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee7() {
                                    var _this5 = this;

                                    var _body, name, body3, _body2;

                                    return regeneratorRuntime.wrap(function _callee7$(_context7) {
                                      while (1) {
                                        switch (_context7.prev = _context7.next) {
                                          case 0:
                                            if (data.success) {
                                              this.session_id = data.session_id;
                                              this.user_id = user_id;

                                              if (this.fromPurchase) {
                                                _body = {
                                                  action: 'insertInitialConvo',
                                                  user_id: this.user_id,
                                                  session_id: this.session_id,
                                                  message: this.message
                                                };
                                                this.postPvdr.postData(_body, 'messages.php').subscribe(function (data) {
                                                  return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this5, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
                                                    return regeneratorRuntime.wrap(function _callee4$(_context4) {
                                                      while (1) {
                                                        switch (_context4.prev = _context4.next) {
                                                          case 0:
                                                          case "end":
                                                            return _context4.stop();
                                                        }
                                                      }
                                                    }, _callee4);
                                                  }));
                                                });
                                              }

                                              console.log("hey123");
                                              this.messagesFromDB = [];
                                              this.socket.connect(); // Join chatroom

                                              console.log("user_id:" + this.user_id + "room:" + this.session_id);
                                              this.socket.emit('joinRoom', {
                                                user_id: this.user_id,
                                                room: this.session_id,
                                                profile_picture: '',
                                                vip_packageOfUser: ""
                                              }); //let name = `user-${new Date().getTime()}`;

                                              name = this.nickname;
                                              this.currentUser = name; //this.socket.emit('set-name', name);
                                              // this.socket.fromEvent('users-changed').subscribe(data => {
                                              //   let user = data['user'];
                                              //   if (data['event'] === 'left') {
                                              //     this.showToast('User left: ' + user);
                                              //   } else {
                                              //     this.showToast('User joined: ' + user);
                                              //   }
                                              // });

                                              this.socket.fromEvent('message').subscribe(function (message) {
                                                _this5.messages.push(message);

                                                console.log("human og send" + JSON.stringify(message));
                                                _this5.loader = false;
                                              });
                                              this.socket.fromEvent('joinRoomMessage').subscribe(function (message) {
                                                console.log("message from server:" + message);
                                              });

                                              if (data.session_chat != '0') {
                                                body3 = {
                                                  action: 'getConvo',
                                                  session_id: this.session_id
                                                };
                                                console.log("getConvo:" + JSON.stringify(body3));
                                                this.postPvdr.postData(body3, 'messages.php').subscribe(function (data) {
                                                  return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this5, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
                                                    var key;
                                                    return regeneratorRuntime.wrap(function _callee5$(_context5) {
                                                      while (1) {
                                                        switch (_context5.prev = _context5.next) {
                                                          case 0:
                                                            if (data.success) {
                                                              for (key in data.result) {
                                                                this.messagesFromDB.push(new _shared_model_chat_model__WEBPACK_IMPORTED_MODULE_8__["chat"](data.result[key].id, data.result[key].session_id, data.result[key].messaged_user_id, data.result[key].message, data.result[key].date, data.result[key].time, data.result[key].status));
                                                              }
                                                            }

                                                          case 1:
                                                          case "end":
                                                            return _context5.stop();
                                                        }
                                                      }
                                                    }, _callee5, this);
                                                  }));
                                                });
                                              }

                                              if (data.session_id != '') {
                                                _body2 = {
                                                  action: 'updateConvoStatus',
                                                  receiversId: this.receiversId,
                                                  session_id: data.session_id
                                                };
                                                this.postPvdr.postData(_body2, 'messages.php').subscribe(function (data) {
                                                  return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this5, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee6() {
                                                    return regeneratorRuntime.wrap(function _callee6$(_context6) {
                                                      while (1) {
                                                        switch (_context6.prev = _context6.next) {
                                                          case 0:
                                                            if (data.success) {}

                                                          case 1:
                                                          case "end":
                                                            return _context6.stop();
                                                        }
                                                      }
                                                    }, _callee6);
                                                  }));
                                                });
                                              }
                                            }

                                          case 1:
                                          case "end":
                                            return _context7.stop();
                                        }
                                      }
                                    }, _callee7, this);
                                  }));
                                }); // plot Data for product

                                body123 = {
                                  action: 'viewPostItemforMessage',
                                  item_id: this.item_id,
                                  user_id: this.login_user_id
                                };
                                console.log(JSON.stringify(body123));
                                this.postPvdr.postData(body123, 'post_item.php').subscribe(function (data) {
                                  return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this4, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee8() {
                                    var items, key;
                                    return regeneratorRuntime.wrap(function _callee8$(_context8) {
                                      while (1) {
                                        switch (_context8.prev = _context8.next) {
                                          case 0:
                                            if (data.success) {
                                              items = []; //var pictureProfile: string = '';

                                              for (key in data.result) {
                                                // items.push(new Item(
                                                //   data.result[key].item_id,
                                                //   data.result[key].user_id,
                                                //   data.result[key].username,
                                                //   data.result[key].price,
                                                //   data.result[key].title,
                                                //   data.result[key].category,
                                                //   data.result[key].location,
                                                //   data.result[key].saveStatus,
                                                //   data.result[key].date,
                                                //   data.result[key].time
                                                //   )
                                                // );
                                                this.item_name = data.result[key].title;
                                                this.item_category = data.result[key].category;
                                                this.item_seller = data.result[key].username;
                                                this.item_user_id = data.result[key].user_id;
                                                this.item_price = data.result[key].price;
                                                this.item_location = data.result[key].location + ' ' + data.result[key].city;
                                                this.item_saveStatus = data.result[key].saveStatus;
                                                this.item_description = data.result[key].description;
                                                this.item_quantity = parseInt(data.result[key].quantity);
                                                this.item_date = data.result[key].date;
                                                this.item_time = data.result[key].time;
                                                this.item_status = data.result[key].status;
                                                this.stocks_id = data.result[key].stocks_id;
                                                this.item_other_stock = data.result[key].others_stock;
                                                this.item_cover_photo = data.result[key].item_photo == '' ? '' : this.postPvdr.myServer() + "/greenthumb/images/posted_items/images/" + data.result[key].item_photo;
                                                this.picture = data.result[key].profile_photo;
                                                this.complete_pic = this.postPvdr.myServer() + "/greenthumb/images/" + this.picture;
                                                this.user_rating = data.result[key].user_rating;
                                              }

                                              this.ownProfile = this.login_user_id == this.item_user_id ? true : false;
                                            }

                                          case 1:
                                          case "end":
                                            return _context8.stop();
                                        }
                                      }
                                    }, _callee8, this);
                                  }));
                                }); // get purchased pending

                                body212 = {
                                  action: 'getPurchasePending',
                                  user_id: user_id,
                                  user_id_2: val,
                                  item_id: this.item_id
                                };
                                console.log('getpurchasepending:' + JSON.stringify(body212));
                                this.postPvdr.postData(body212, 'save_item.php').subscribe(function (data) {
                                  return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this4, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee9() {
                                    return regeneratorRuntime.wrap(function _callee9$(_context9) {
                                      while (1) {
                                        switch (_context9.prev = _context9.next) {
                                          case 0:
                                            if (data.success) {
                                              if (data.pending_purchase) {
                                                this.withPurchasedPending = true;
                                                this.quantity_purchased = data.quantity_purchased;
                                                this.total_amount = data.total_amount;

                                                if (data.mystatus == "3") {
                                                  this.alreadySold = true;
                                                }
                                              } else {}
                                            }

                                          case 1:
                                          case "end":
                                            return _context9.stop();
                                        }
                                      }
                                    }, _callee9, this);
                                  }));
                                });

                              case 10:
                              case "end":
                                return _context10.stop();
                            }
                          }
                        }, _callee10, this);
                      }));
                    });

                  case 2:
                    body2 = {
                      action: 'getuserdata',
                      user_id: val
                    };
                    this.postPvdr.postData(body2, 'credentials-api.php').subscribe(function (data) {
                      return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this3, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee11() {
                        return regeneratorRuntime.wrap(function _callee11$(_context11) {
                          while (1) {
                            switch (_context11.prev = _context11.next) {
                              case 0:
                                if (data.success) {
                                  if (data.result.nickname == "") {
                                    if (data.result.login_type_id == "3") {
                                      this.nickname = data.result.mobile_num;
                                    } else if (data.result.login_type_id == "4") {
                                      this.nickname = data.result.email;
                                    } else {
                                      this.nickname = data.result.fname;
                                    }
                                  } else {
                                    this.nickname = data.result.nickname;
                                  }

                                  this.conv_image = data.result.photo == '' ? '' : this.postPvdr.myServer() + "/greenthumb/images/" + data.result.photo;
                                }

                              case 1:
                              case "end":
                                return _context11.stop();
                            }
                          }
                        }, _callee11, this);
                      }));
                    });

                  case 4:
                  case "end":
                    return _context12.stop();
                }
              }
            }, _callee12, this);
          }));
        }
      }, {
        key: "confirmMarkSold",
        value: function confirmMarkSold() {
          var _this6 = this;

          this.alertCtrl.create({
            header: "Greenthumb Trade",
            message: "Are you sure to mark this as sold?",
            buttons: [{
              text: "No",
              handler: function handler() {
                console.log("Disagree clicked");
              }
            }, {
              text: "Yes",
              handler: function handler() {
                console.log("Agree clicked");

                _this6.markSold();
              }
            }]
          }).then(function (res) {
            res.present();
          });
        }
      }, {
        key: "markSold",
        value: function markSold() {
          var _this7 = this;

          console.log("gi sold lamang pud");
          var body = {
            action: 'markSoldItem',
            item_id: this.item_id,
            user_id: this.login_user_id,
            purchased_user_id: this.receiversId,
            item_status: this.item_status,
            item_name: this.item_name,
            quantity_purchased: this.quantity_purchased
          };
          console.log(JSON.stringify(body));
          this.postPvdr.postData(body, 'save_item.php').subscribe(function (data) {
            if (data.success) {
              _this7.alreadySold = true; // if(data.saveStatus == '1'){
              //   console.log('1 ko');
              //   var element = document.getElementById("class"+item_id);
              //   element.classList.remove("myHeartUnSave");
              //   element.classList.add("myHeartSave");
              //   console.log("classList:"+element.classList);
              // }
              // else{
              //   console.log("0 ko");
              //   var element = document.getElementById("class"+item_id);
              //   element.classList.remove("myHeartSave");
              //   element.classList.add("myHeartUnSave");
              // }
            }
          });
        }
      }, {
        key: "showToast",
        value: function showToast(msg) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee13() {
            var toast;
            return regeneratorRuntime.wrap(function _callee13$(_context13) {
              while (1) {
                switch (_context13.prev = _context13.next) {
                  case 0:
                    _context13.next = 2;
                    return this.toastCtrl.create({
                      message: msg,
                      position: 'top',
                      duration: 2000
                    });

                  case 2:
                    toast = _context13.sent;
                    toast.present();

                  case 4:
                  case "end":
                    return _context13.stop();
                }
              }
            }, _callee13, this);
          }));
        }
      }]);

      return MessagePage;
    }();

    MessagePage.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }, {
        type: _providers_chat_messages_provider__WEBPACK_IMPORTED_MODULE_5__["ChatMessages"]
      }, {
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_7__["Storage"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
      }, {
        type: _providers_credential_provider__WEBPACK_IMPORTED_MODULE_6__["PostProvider"]
      }, {
        type: ngx_socket_io__WEBPACK_IMPORTED_MODULE_3__["Socket"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"]
      }];
    };

    MessagePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-message',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./message.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/message/message.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./message.page.scss */
      "./src/app/message/message.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _providers_chat_messages_provider__WEBPACK_IMPORTED_MODULE_5__["ChatMessages"], _ionic_storage__WEBPACK_IMPORTED_MODULE_7__["Storage"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _providers_credential_provider__WEBPACK_IMPORTED_MODULE_6__["PostProvider"], ngx_socket_io__WEBPACK_IMPORTED_MODULE_3__["Socket"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"]])], MessagePage);
    /***/
  }
}]);
//# sourceMappingURL=message-message-module-es5.js.map