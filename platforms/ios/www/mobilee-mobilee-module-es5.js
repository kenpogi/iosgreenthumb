function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["mobilee-mobilee-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/mobilee/mobilee.page.html":
  /*!*********************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/mobilee/mobilee.page.html ***!
    \*********************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppMobileeMobileePageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header mode=\"ios\" no-border>\n    <ion-toolbar mode=\"ios\">\n      <ion-buttons slot=\"secondary\" (click)=\"goBack()\">\n        <ion-button mode=\"ios\">\n            <ion-icon name=\"arrow-back\" mode=\"ios\"></ion-icon>\n        </ion-button>\n      </ion-buttons>\n\n      <ion-title mode=\"ios\" color=\"secondary\">Signup with SMS</ion-title>\n\n    </ion-toolbar>\n  </ion-header>\n\n<ion-content>\n  <ion-avatar style=\"margin: auto;margin-top: 10%;\">\n    <img src=\"assets/greenthumb-images/greenthumblogo.png\">\n  </ion-avatar>\n  <br>\n    <ion-list class=\"line-input\" padding>\n      <ion-row>\n        <ion-col>\n          <ion-label class=\"item_label\" text-left>Country Code: USA(+1)</ion-label>\n          <!-- <ion-item>\n            <ion-label  class=\"item_label\" style=\"margin-top: 0;\">Select One</ion-label>\n            <ion-select interface=\"popover\"  class=\"item_select\" mode=\"ios\" [(ngModel)]=\"inpt_country\">\n              <ion-select-option value=\"1\" selected>USA (+1)</ion-select-option>\n            </ion-select>\n          </ion-item> -->\n        </ion-col>\n      </ion-row>\n    </ion-list>\n   \n    <ion-list class=\"line-input\" padding style=\"padding-top: 0;\">\n      <ion-row>\n        <ion-col size=\"8\">\n          <ion-label class=\"item_label\">Mobile</ion-label>\n          <ion-item>\n            <ion-input mode=\"ios\" type=\"number\"  placeholder=\"\" [(ngModel)]=\"inpt_mobile\" class=\"item_input\" size=\"small\"></ion-input>\n          </ion-item>\n        </ion-col>\n        <ion-col size=\"4\">\n          <ion-button expand=\"block\" mode=\"ios\" (click)=\"sendOTP()\" size=\"small\" style=\"margin-top: 32%;opacity: 0.8;\">\n            GET CODE\n        </ion-button> \n        </ion-col>\n      </ion-row>\n    </ion-list>\n\n    <ion-list class=\"line-input\" padding style=\"padding-top: 0;\">\n      <ion-row>\n        <ion-col>\n          <ion-label class=\"item_label\">Code</ion-label>\n          <ion-item>\n            <ion-input mode=\"ios\" placeholder=\"Enter code here\" class=\"item_input\" size=\"small\" type=\"number\" [(ngModel)]=\"inpt_code\"></ion-input>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n    </ion-list>\n\n    <ion-list padding>\n      <ion-row>\n        <ion-col>\n          <ion-button expand=\"block\" mode=\"ios\" (click)=\"login()\">SAVE</ion-button>\n        </ion-col>\n      </ion-row>\n    </ion-list>\n</ion-content>\n";
    /***/
  },

  /***/
  "./src/app/mobilee/mobilee.module.ts":
  /*!*******************************************!*\
    !*** ./src/app/mobilee/mobilee.module.ts ***!
    \*******************************************/

  /*! exports provided: MobileePageModule */

  /***/
  function srcAppMobileeMobileeModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "MobileePageModule", function () {
      return MobileePageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _mobilee_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./mobilee.page */
    "./src/app/mobilee/mobilee.page.ts");

    var routes = [{
      path: '',
      component: _mobilee_page__WEBPACK_IMPORTED_MODULE_6__["MobileePage"]
    }];

    var MobileePageModule = function MobileePageModule() {
      _classCallCheck(this, MobileePageModule);
    };

    MobileePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      declarations: [_mobilee_page__WEBPACK_IMPORTED_MODULE_6__["MobileePage"]]
    })], MobileePageModule);
    /***/
  },

  /***/
  "./src/app/mobilee/mobilee.page.scss":
  /*!*******************************************!*\
    !*** ./src/app/mobilee/mobilee.page.scss ***!
    \*******************************************/

  /*! exports provided: default */

  /***/
  function srcAppMobileeMobileePageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".line-input {\n  margin-bottom: 0 !important;\n}\n.line-input ion-item {\n  --border-color: transparent!important;\n  --highlight-height: 0;\n  border: 1px solid #dedede;\n  border-radius: 4px;\n  height: 40px;\n  margin-top: 4%;\n}\n.item_input {\n  font-size: 14px;\n  --padding-top: 0;\n  color: #424242 !important;\n}\n.item_select {\n  font-size: 14px;\n  --padding-top: 1%;\n  color: #424242 !important;\n}\n.item_label {\n  color: #b3aeae !important;\n  font-weight: 300;\n  font-size: 13px;\n}\nion-avatar {\n  width: 100% !important;\n  height: 100% !important;\n  max-width: 120px !important;\n  max-height: 120px !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZW5kcmljay9EZXNrdG9wL1RlbG1vIFNvbHV0aW9ucy9Gb3JQcm9kdWN0aW9uL21heTEwMjAyMS9sYXN0IHNldHVwL2dyZWVudGh1bWJhcHAgY29weS9zcmMvYXBwL21vYmlsZWUvbW9iaWxlZS5wYWdlLnNjc3MiLCJzcmMvYXBwL21vYmlsZWUvbW9iaWxlZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSwyQkFBQTtBQ0NKO0FEQUk7RUFDSSxxQ0FBQTtFQUNBLHFCQUFBO0VBQ0EseUJBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxjQUFBO0FDRVI7QURDQTtFQUNJLGVBQUE7RUFDQSxnQkFBQTtFQUNBLHlCQUFBO0FDRUo7QURBQTtFQUNJLGVBQUE7RUFDQSxpQkFBQTtFQUNBLHlCQUFBO0FDR0o7QUREQTtFQUNJLHlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0FDSUo7QURGQTtFQUNJLHNCQUFBO0VBQ0EsdUJBQUE7RUFDQSwyQkFBQTtFQUNBLDRCQUFBO0FDS0oiLCJmaWxlIjoic3JjL2FwcC9tb2JpbGVlL21vYmlsZWUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmxpbmUtaW5wdXQge1xuICAgIG1hcmdpbi1ib3R0b206IDAhaW1wb3J0YW50O1xuICAgIGlvbi1pdGVtIHtcbiAgICAgICAgLS1ib3JkZXItY29sb3I6IHRyYW5zcGFyZW50IWltcG9ydGFudDtcbiAgICAgICAgLS1oaWdobGlnaHQtaGVpZ2h0OiAwO1xuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjZGVkZWRlO1xuICAgICAgICBib3JkZXItcmFkaXVzOiA0cHg7XG4gICAgICAgIGhlaWdodDogNDBweDtcbiAgICAgICAgbWFyZ2luLXRvcDogNCU7XG4gICAgfVxufVxuLml0ZW1faW5wdXR7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgIC0tcGFkZGluZy10b3A6IDA7XG4gICAgY29sb3I6ICM0MjQyNDIhaW1wb3J0YW50O1xufVxuLml0ZW1fc2VsZWN0e1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAtLXBhZGRpbmctdG9wOiAxJTtcbiAgICBjb2xvcjogIzQyNDI0MiFpbXBvcnRhbnQ7XG59XG4uaXRlbV9sYWJlbHtcbiAgICBjb2xvcjogI2IzYWVhZSAhaW1wb3J0YW50O1xuICAgIGZvbnQtd2VpZ2h0OiAzMDA7XG4gICAgZm9udC1zaXplOiAxM3B4O1xufVxuaW9uLWF2YXRhciAgeyAgICAgXG4gICAgd2lkdGg6MTAwJSAhaW1wb3J0YW50OyAgXG4gICAgaGVpZ2h0IDogMTAwJSAhaW1wb3J0YW50OyAgXG4gICAgbWF4LXdpZHRoOiAxMjBweCAhaW1wb3J0YW50OyAgLy9hbnkgc2l6ZVxuICAgIG1heC1oZWlnaHQ6IDEyMHB4ICFpbXBvcnRhbnQ7IC8vYW55IHNpemUgXG4gICAgfSIsIi5saW5lLWlucHV0IHtcbiAgbWFyZ2luLWJvdHRvbTogMCAhaW1wb3J0YW50O1xufVxuLmxpbmUtaW5wdXQgaW9uLWl0ZW0ge1xuICAtLWJvcmRlci1jb2xvcjogdHJhbnNwYXJlbnQhaW1wb3J0YW50O1xuICAtLWhpZ2hsaWdodC1oZWlnaHQ6IDA7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNkZWRlZGU7XG4gIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgaGVpZ2h0OiA0MHB4O1xuICBtYXJnaW4tdG9wOiA0JTtcbn1cblxuLml0ZW1faW5wdXQge1xuICBmb250LXNpemU6IDE0cHg7XG4gIC0tcGFkZGluZy10b3A6IDA7XG4gIGNvbG9yOiAjNDI0MjQyICFpbXBvcnRhbnQ7XG59XG5cbi5pdGVtX3NlbGVjdCB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgLS1wYWRkaW5nLXRvcDogMSU7XG4gIGNvbG9yOiAjNDI0MjQyICFpbXBvcnRhbnQ7XG59XG5cbi5pdGVtX2xhYmVsIHtcbiAgY29sb3I6ICNiM2FlYWUgIWltcG9ydGFudDtcbiAgZm9udC13ZWlnaHQ6IDMwMDtcbiAgZm9udC1zaXplOiAxM3B4O1xufVxuXG5pb24tYXZhdGFyIHtcbiAgd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcbiAgaGVpZ2h0OiAxMDAlICFpbXBvcnRhbnQ7XG4gIG1heC13aWR0aDogMTIwcHggIWltcG9ydGFudDtcbiAgbWF4LWhlaWdodDogMTIwcHggIWltcG9ydGFudDtcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/mobilee/mobilee.page.ts":
  /*!*****************************************!*\
    !*** ./src/app/mobilee/mobilee.page.ts ***!
    \*****************************************/

  /*! exports provided: MobileePage */

  /***/
  function srcAppMobileeMobileePageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "MobileePage", function () {
      return MobileePage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _providers_credential_provider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../providers/credential-provider */
    "./src/providers/credential-provider.ts");
    /* harmony import */


    var _providers_sms_provider__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../providers/sms-provider */
    "./src/providers/sms-provider.ts");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");

    var MobileePage = /*#__PURE__*/function () {
      function MobileePage(router, alertCtrl, toastController, smsPvdr, navCtrl, postPvdr, storage) {
        _classCallCheck(this, MobileePage);

        this.router = router;
        this.alertCtrl = alertCtrl;
        this.toastController = toastController;
        this.smsPvdr = smsPvdr;
        this.navCtrl = navCtrl;
        this.postPvdr = postPvdr;
        this.storage = storage;
        this.c_bool = 0;
        this.num = Math.floor(Math.random() * 1000000) + 7;
      }

      _createClass(MobileePage, [{
        key: "toHomePage",
        value: function toHomePage() {
          this.router.navigate(['tabs']);
        }
      }, {
        key: "goBack",
        value: function goBack() {
          this.router.navigate(['choose']);
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "presentAlert",
        value: function presentAlert() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
            var alert;
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    _context.next = 2;
                    return this.alertCtrl.create({
                      message: 'Please enter your mobile number with selected country code.',
                      subHeader: 'Required.',
                      buttons: ['DISMISS']
                    });

                  case 2:
                    alert = _context.sent;
                    _context.next = 5;
                    return alert.present();

                  case 5:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        }
      }, {
        key: "presentAlertSent",
        value: function presentAlertSent() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
            var alert;
            return regeneratorRuntime.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    _context2.next = 2;
                    return this.alertCtrl.create({
                      message: 'Please enter the code you recieved.',
                      subHeader: 'Message Sent.',
                      buttons: ['OK']
                    });

                  case 2:
                    alert = _context2.sent;
                    _context2.next = 5;
                    return alert.present();

                  case 5:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this);
          }));
        }
      }, {
        key: "presentAlertNotSent",
        value: function presentAlertNotSent() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
            var alert;
            return regeneratorRuntime.wrap(function _callee3$(_context3) {
              while (1) {
                switch (_context3.prev = _context3.next) {
                  case 0:
                    _context3.next = 2;
                    return this.alertCtrl.create({
                      message: 'Please check your internet connection.',
                      subHeader: 'Message Not Sent.',
                      buttons: ['DISMISS']
                    });

                  case 2:
                    alert = _context3.sent;
                    _context3.next = 5;
                    return alert.present();

                  case 5:
                  case "end":
                    return _context3.stop();
                }
              }
            }, _callee3, this);
          }));
        }
      }, {
        key: "presentAlertError",
        value: function presentAlertError() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
            var alert;
            return regeneratorRuntime.wrap(function _callee4$(_context4) {
              while (1) {
                switch (_context4.prev = _context4.next) {
                  case 0:
                    _context4.next = 2;
                    return this.alertCtrl.create({
                      message: 'Invalid code entered.',
                      subHeader: 'Warning.',
                      buttons: ['Dismiss']
                    });

                  case 2:
                    alert = _context4.sent;
                    _context4.next = 5;
                    return alert.present();

                  case 5:
                  case "end":
                    return _context4.stop();
                }
              }
            }, _callee4, this);
          }));
        }
      }, {
        key: "presentToast",
        value: function presentToast() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
            var toast;
            return regeneratorRuntime.wrap(function _callee5$(_context5) {
              while (1) {
                switch (_context5.prev = _context5.next) {
                  case 0:
                    _context5.next = 2;
                    return this.toastController.create({
                      message: 'Welcome to Brixy Live!',
                      duration: 3000
                    });

                  case 2:
                    toast = _context5.sent;
                    toast.present();

                  case 4:
                  case "end":
                    return _context5.stop();
                }
              }
            }, _callee5, this);
          }));
        }
      }, {
        key: "presentAlertInvalidCountry",
        value: function presentAlertInvalidCountry(mobile) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee6() {
            var alert;
            return regeneratorRuntime.wrap(function _callee6$(_context6) {
              while (1) {
                switch (_context6.prev = _context6.next) {
                  case 0:
                    _context6.next = 2;
                    return this.alertCtrl.create({
                      message: mobile + ': Invalid country code selected.',
                      subHeader: 'Warning.',
                      buttons: ['Dismiss']
                    });

                  case 2:
                    alert = _context6.sent;
                    _context6.next = 5;
                    return alert.present();

                  case 5:
                  case "end":
                    return _context6.stop();
                }
              }
            }, _callee6, this);
          }));
        }
      }, {
        key: "checkOTP",
        value: function checkOTP() {
          // if(this.inpt_country === undefined){
          //     this.c_bool = 0;
          // } else {
          this.c_bool = 1; // var str = new String(this.inpt_mobile); 
          // if(str.charAt(0) === '0'){
          //   this.inpt_mobile = this.inpt_mobile.substr(1);
          // }
          //}
        }
      }, {
        key: "sendOTP",
        value: function sendOTP() {
          var _this = this;

          //  this.checkOTP();
          var phone = new Number(this.inpt_mobile); //var country_code = new Number(this.inpt_country);

          var country_code = "1";
          var mobile = country_code.toString() + phone.toString();
          this.complete_mobile = mobile;

          if (phone.toString().length != 10) {
            this.presentAlert();
          } else {
            var body = {
              mobile_num: mobile,
              code: this.num
            };
            console.log("pickyouup:" + JSON.stringify(body));
            this.smsPvdr.smsData(body, 'send-sms.php').subscribe(function (data) {
              console.log("notice:" + JSON.stringify(data));

              if (data.result === 'error') {
                _this.presentAlertInvalidCountry(mobile);
              } else {
                if (data.success) {
                  _this.presentAlertSent();
                } else {
                  _this.presentAlertNotSent();
                }
              }
            });
          }
        }
      }, {
        key: "login",
        value: function login() {
          var v_code = new Number(this.inpt_code);

          if (v_code == this.num) {
            this.presentToast();
            this.storage.set('login_used', "mobile");
            this.storage.set('mobile_num', this.complete_mobile);
            this.mobileconnect();
          } else {
            this.presentAlertError();
          }
        }
      }, {
        key: "mobileconnect",
        value: function mobileconnect() {
          var _this2 = this;

          var body = {
            action: "checkmobile",
            mobile_num: this.complete_mobile
          };
          this.postPvdr.postData(body, 'credentials-api.php').subscribe(function (data) {
            var body2 = {
              action: 'getUserIdMobile',
              mobile_num: _this2.complete_mobile,
              login_type: 3
            };

            _this2.postPvdr.postData(body2, 'credentials-api.php').subscribe(function (data) {
              if (data.success) {
                _this2.storage.set("user_id", data.user_id);

                console.log("user_id: mobile used:" + data.user_id);
              }
            });

            _this2.router.navigate(["/tabs"]);
          });
        }
      }]);

      return MobileePage;
    }();

    MobileePage.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"]
      }, {
        type: _providers_sms_provider__WEBPACK_IMPORTED_MODULE_5__["SmsProvider"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"]
      }, {
        type: _providers_credential_provider__WEBPACK_IMPORTED_MODULE_4__["PostProvider"]
      }, {
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_6__["Storage"]
      }];
    };

    MobileePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-mobilee',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./mobilee.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/mobilee/mobilee.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./mobilee.page.scss */
      "./src/app/mobilee/mobilee.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"], _providers_sms_provider__WEBPACK_IMPORTED_MODULE_5__["SmsProvider"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"], _providers_credential_provider__WEBPACK_IMPORTED_MODULE_4__["PostProvider"], _ionic_storage__WEBPACK_IMPORTED_MODULE_6__["Storage"]])], MobileePage);
    /***/
  }
}]);
//# sourceMappingURL=mobilee-mobilee-module-es5.js.map