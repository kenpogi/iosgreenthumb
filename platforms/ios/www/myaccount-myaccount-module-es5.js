function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["myaccount-myaccount-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/myaccount/myaccount.page.html":
  /*!*************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/myaccount/myaccount.page.html ***!
    \*************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppMyaccountMyaccountPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header mode=\"ios\" no-border class=\"for_header\">\n  <ion-toolbar mode=\"ios\" class=\"for_background\">\n      <ion-buttons slot=\"secondary\" (click)=\"goBack()\" >\n        <ion-button mode=\"ios\" style=\"font-size: 15px;\">\n          Back\n      </ion-button>\n      </ion-buttons>\n      <ion-buttons slot=\"primary\" mode=\"ios\">\n        <ion-button mode=\"ios\" (click)=\"goInvite()\">\n          <img src=\"assets/greenthumb-images/group_add.png\" style=\"width: 30px !important;\">\n        </ion-button>\n        <ion-button mode=\"ios\" (click)=\"goViewprofile()\">\n          <img src=\"assets/greenthumb-images/user_circle.png\" style=\"width: 21px !important;\">\n        </ion-button>\n        <ion-button mode=\"ios\" (click)=\"goHelp()\">\n          <img src=\"assets/greenthumb-images/helpicon.png\" style=\"width: 21px !important;\">\n        </ion-button>\n      </ion-buttons>\n    </ion-toolbar>\n</ion-header>\n\n<ion-content mode=\"ios\" class=\"for_background\" style=\"--overflow: hidden\">\n  <div class=\"for_div\">\n    <div class=\"for_scroll\">\n    <ion-row class=\"for_userborder\">\n      <ion-col size=\"1\"></ion-col>\n      <ion-col size=\"10\" text-center>\n        <ion-item id=\"for_thumbnails\" class=\"for_item\" mode=\"ios\" lines=\"none\">\n          <ion-thumbnail slot=\"start\">\n            <!-- <img src=\"assets/greenthumb-images/userpicdark.png\"> -->\n            <div *ngIf=\"!picture\">\n              <img src=\"assets/greenthumb-images/userpicdark.png\">\n            </div>\n            <div *ngIf=\"picture\">\n              <img [src] = \"complete_pic\">\n            </div>\n            <ion-icon mode=\"ios\" (click)=\"update_photo()\" class=\"upload_photo\" name=\"camera\"></ion-icon>\n          </ion-thumbnail>\n          <ion-label>\n            <h2 style=\"font-size: 17px;\"><b>{{username}}</b></h2>\n            <h3 style=\"font-size: 14px;\">{{city}}</h3>\n            <ion-row style=\"padding-top: 2%;\">\n              <ion-col text-left>\n                <!-- <ion-icon class=\"for_staricon\" name=\"star\" mode=\"ios\" size=\"medium\" color=\"warning\"></ion-icon>\n                <ion-icon class=\"for_staricon\" name=\"star\" mode=\"ios\" size=\"medium\" color=\"warning\"></ion-icon>\n                <ion-icon class=\"for_staricon\" name=\"star\" mode=\"ios\" size=\"medium\" color=\"warning\"></ion-icon>\n                <ion-icon class=\"for_staricon\" name=\"star\" mode=\"ios\" size=\"medium\" color=\"warning\"></ion-icon>\n                <ion-icon class=\"for_staricon\" name=\"star-half\" mode=\"ios\" size=\"medium\" color=\"warning\"></ion-icon>&nbsp;\n                <span style=\"font-size: 15px;\">(5)</span> -->\n                <ionic4-star-rating #rating\n                activeIcon = \"ios-star\"\n                defaultIcon = \"ios-star-outline\"\n                activeColor = \"#ffce00\" \n                defaultColor = \"#ffce00\"\n                readonly=\"false\"\n                rating=\"{{user_rating}}\"\n                fontSize = \"15px\">\n              </ionic4-star-rating>\n              </ion-col>\n            </ion-row>\n          </ion-label>\n        </ion-item>\n      </ion-col>\n      <ion-col size=\"1\"></ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col text-center>\n        <p class=\"for_verify\"><b>VERIFY YOUR ACCOUNT TO BUILD REPUTATION</b></p>\n        <ion-list  class=\"for_accountitems\">\n          <ion-item *ngIf=\"login_type== 4 || login_type==2 \" mode=\"ios\" id=\"for_eachitem\">\n            <ion-icon name=\"mail\" slot=\"start\" color=\"primary\" mode=\"ios\"></ion-icon>\n            <ion-label>Email Verified</ion-label>\n           \n            <ion-icon  name=\"checkmark\" slot=\"end\" color=\"primary\" mode=\"ios\" size=\"large\"></ion-icon>\n            \n            \n          </ion-item>\n          <ion-item mode=\"ios\" id=\"for_eachitem\">\n            <ion-icon name=\"camera\" slot=\"start\" color=\"primary\" mode=\"ios\"></ion-icon>\n            <ion-label>Image Added</ion-label>\n            \n              <ion-icon  *ngIf=\"!picture\" name=\"checkmark\" slot=\"end\" color=\"tertiary\" mode=\"ios\" size=\"large\"></ion-icon>\n           \n              <ion-icon *ngIf=\"picture\" name=\"checkmark\" slot=\"end\" color=\"primary\" mode=\"ios\" size=\"large\"></ion-icon>\n            \n            \n          </ion-item>\n          <ion-item *ngIf=\"login_type== 3 \" mode=\"ios\" id=\"for_eachitem\">\n            <ion-icon name=\"call\" slot=\"start\" color=\"primary\" mode=\"ios\"></ion-icon>\n            <ion-label>Verify Phone</ion-label>\n           \n            <ion-icon  name=\"checkmark\" slot=\"end\" color=\"primary\" mode=\"ios\" size=\"large\"></ion-icon>\n            \n            \n          </ion-item>\n          <ion-item mode=\"ios\" id=\"for_eachitem\">\n            <ion-icon name=\"add-circle\" slot=\"start\" color=\"primary\" mode=\"ios\"></ion-icon>\n            <ion-label>Join Truyou</ion-label>\n            <ion-icon name=\"checkmark\" slot=\"end\" color=\"tertiary\" mode=\"ios\" size=\"large\"></ion-icon>\n          </ion-item>\n          <ion-item *ngIf=\"login_type== 1 \" mode=\"ios\" id=\"for_eachitem\">\n            <ion-icon name=\"logo-facebook\" slot=\"start\" color=\"primary\" mode=\"ios\"></ion-icon>\n            <ion-label>Facebook Connected</ion-label>\n           \n            <ion-icon  name=\"checkmark\" slot=\"end\" color=\"primary\" mode=\"ios\" size=\"large\"></ion-icon>\n            \n            \n          </ion-item>\n        </ion-list>\n        <ion-label color=\"secondary\">\n          <p class=\"for_verify\">Learn how reputation improves your profile</p>\n        </ion-label>\n        <ion-list class=\"for_accountitems\">\n          <ion-item mode=\"ios\" id=\"for_eachitem\" (click)=\"goWallet()\">\n            <ion-icon name=\"wallet\" slot=\"start\" color=\"primary\" mode=\"ios\"></ion-icon>\n            <ion-label style=\"font-weight: bold;font-size: 15px;\">CASH POINTS</ion-label>\n          </ion-item>\n        </ion-list>\n        <ion-list class=\"for_accountitems\">\n          <ion-list-header class=\"for_eacheader\">OFFERS</ion-list-header>\n          <ion-item mode=\"ios\" id=\"for_eachitem\" (click)=\"goOffers()\">\n            <img src=\"assets/greenthumb-images/shop_bag.png\" class=\"for_eachimg\">\n            <ion-label>Selling</ion-label>\n          </ion-item>\n          <ion-item mode=\"ios\" id=\"for_eachitem\" (click)=\"goOffers()\">\n            <ion-icon name=\"cart\" slot=\"start\" color=\"primary\" mode=\"ios\"></ion-icon>\n            <ion-label>Buying</ion-label>\n          </ion-item>\n        </ion-list>\n        <ion-list class=\"for_accountitems\">\n          <ion-list-header class=\"for_eacheader\">PAYMENT METHOD</ion-list-header>\n          <ion-item mode=\"ios\" id=\"for_eachitem\" (click)=\"goApple()\">\n            <img src=\"assets/greenthumb-images/applepay.png\" class=\"for_eachimg2\">\n            <ion-label>Apple Pay</ion-label>\n          </ion-item>\n          <ion-item mode=\"ios\" id=\"for_eachitem\" (click)=\"goGoogle()\">\n            <img src=\"assets/greenthumb-images/googlepay.png\" class=\"for_eachimg2\">\n            <ion-label>Google Pay</ion-label>\n          </ion-item>\n        </ion-list>\n        <ion-list class=\"for_accountitems\">\n          <ion-list-header class=\"for_eacheader\">SUBSCRIPTION</ion-list-header>\n          <ion-item mode=\"ios\" id=\"for_eachitem\" (click)=\"goSubscribe()\">\n            <img src=\"assets/greenthumb-images/subscribe.png\" class=\"for_eachimg\">\n            <ion-label>Subscribe</ion-label>\n          </ion-item>\n          <ion-item mode=\"ios\" id=\"for_eachitem\">\n            <img src=\"assets/greenthumb-images/history.png\" class=\"for_eachimg\">\n            <ion-label>History</ion-label>\n          </ion-item>\n          <ion-item mode=\"ios\" id=\"for_eachitem\">\n            <ion-icon name=\"settings\" slot=\"start\" color=\"primary\" mode=\"ios\"></ion-icon>\n            <ion-label>Settings</ion-label>\n          </ion-item>\n        </ion-list>\n        <ion-list class=\"for_accountitems\">\n          <ion-list-header class=\"for_eacheader\">SELLER INTERACTIONS</ion-list-header>\n          <ion-item mode=\"ios\" id=\"for_eachitem\" (click)=\"goArchive()\">\n            <ion-icon name=\"heart\" slot=\"start\" color=\"primary\" mode=\"ios\"></ion-icon>\n            <ion-label>Saved items</ion-label>\n          </ion-item>\n          <ion-item mode=\"ios\" id=\"for_eachitem\" (click)=\"goFollow()\">\n            <ion-icon name=\"people\" slot=\"start\" color=\"primary\" mode=\"ios\"></ion-icon>\n            <ion-label>Followers/Following</ion-label>\n          </ion-item>\n        </ion-list>\n        <ion-list class=\"for_accountitems\">\n          <ion-list-header class=\"for_eacheader\">ACCOUNT</ion-list-header>\n          <ion-item mode=\"ios\" id=\"for_eachitem\" (click)=\"goViewprofile()\">\n            <ion-icon name=\"person\" slot=\"start\" color=\"primary\" mode=\"ios\"></ion-icon>\n            <ion-label>View profile</ion-label>\n          </ion-item>\n          <ion-item mode=\"ios\" id=\"for_eachitem\" (click)=\"openRegistermodal()\">\n            <ion-icon name=\"settings\" slot=\"start\" color=\"primary\" mode=\"ios\"></ion-icon>\n            <ion-label>Account settings</ion-label>\n          </ion-item>\n          <ion-item mode=\"ios\" id=\"for_eachitem\" (click)=\"goLogout()\">\n            <ion-icon name=\"log-out\" slot=\"start\" color=\"primary\" mode=\"ios\"></ion-icon>\n            <ion-label>Logout</ion-label>\n          </ion-item>\n        </ion-list>\n        <ion-list class=\"for_accountitems\">\n          <ion-list-header class=\"for_eacheader\">HELP CENTER</ion-list-header>\n          <ion-item mode=\"ios\" id=\"for_eachitem\" (click)=\"goHelp()\">\n            <ion-icon name=\"help-circle\" slot=\"start\" color=\"primary\" mode=\"ios\"></ion-icon>\n            <ion-label>Help</ion-label>\n          </ion-item>\n          <ion-item mode=\"ios\" id=\"for_eachitem\" (click)=\"goCommunityForum()\">\n            <ion-icon name=\"chatboxes\" slot=\"start\" color=\"primary\" mode=\"ios\"></ion-icon>\n            <ion-label>Community forums</ion-label>\n          </ion-item>\n        </ion-list>\n        <ion-list class=\"for_invite\">\n          <ion-item mode=\"ios\" id=\"for_eachitem\" lines=\"none\" style=\"--background: transparent;\" (click)=\"goInvite()\">\n            <img src=\"assets/greenthumb-images/group_add2.png\" class=\"for_eachimg2\">\n            <ion-label color=\"light\" class=\"for_invitetext\">Invite friends</ion-label>\n            <ion-icon color=\"light\" name=\"arrow-forward\" slot=\"end\" mode=\"ios\"></ion-icon>\n          </ion-item>\n        </ion-list>\n       \n      </ion-col>\n    </ion-row>\n    \n  </div>\n  </div>\n</ion-content>\n";
    /***/
  },

  /***/
  "./src/app/myaccount/myaccount.module.ts":
  /*!***********************************************!*\
    !*** ./src/app/myaccount/myaccount.module.ts ***!
    \***********************************************/

  /*! exports provided: MyaccountPageModule */

  /***/
  function srcAppMyaccountMyaccountModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "MyaccountPageModule", function () {
      return MyaccountPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var ionic4_star_rating__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ionic4-star-rating */
    "./node_modules/ionic4-star-rating/dist/index.js");
    /* harmony import */


    var _myaccount_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./myaccount.page */
    "./src/app/myaccount/myaccount.page.ts");

    var routes = [{
      path: '',
      component: _myaccount_page__WEBPACK_IMPORTED_MODULE_7__["MyaccountPage"]
    }];

    var MyaccountPageModule = function MyaccountPageModule() {
      _classCallCheck(this, MyaccountPageModule);
    };

    MyaccountPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], ionic4_star_rating__WEBPACK_IMPORTED_MODULE_6__["StarRatingModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      declarations: [_myaccount_page__WEBPACK_IMPORTED_MODULE_7__["MyaccountPage"]]
    })], MyaccountPageModule);
    /***/
  },

  /***/
  "./src/app/myaccount/myaccount.page.scss":
  /*!***********************************************!*\
    !*** ./src/app/myaccount/myaccount.page.scss ***!
    \***********************************************/

  /*! exports provided: default */

  /***/
  function srcAppMyaccountMyaccountPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".for_header {\n  background: var(--ion-color-tertiary);\n  padding-bottom: 3%;\n}\n\n.for_background {\n  --background: var(--ion-color-tertiary);\n}\n\n#for_thumbnails ion-thumbnail {\n  width: 100% !important;\n  height: 100% !important;\n  max-width: 5rem !important;\n  max-height: 5rem !important;\n}\n\n#for_thumbnails ion-thumbnail img {\n  border: 1px solid rgba(0, 0, 0, 0.12);\n  width: 100% !important;\n  height: 100% !important;\n  max-width: 5rem !important;\n  max-height: 5rem !important;\n  border-radius: 50%;\n}\n\n.for_div {\n  background: white;\n  padding-bottom: 5px;\n  border-top-left-radius: 40px;\n  border-top-right-radius: 40px;\n  padding-top: 5%;\n  overflow-y: auto;\n  height: 100%;\n}\n\n.for_scroll {\n  overflow-y: auto;\n  height: 100%;\n}\n\nion-item {\n  --padding-start: 0% !important;\n}\n\n.for_item {\n  --background: transparent;\n}\n\n.for_staricon {\n  font-size: 24px;\n  margin-bottom: -3%;\n}\n\n.for_userborder {\n  border-bottom: 1px solid #ebf1f0;\n}\n\n.for_verify {\n  font-size: 13px;\n  padding: 0;\n}\n\n.for_accountitems {\n  box-shadow: 0 1px 8px #e2f0cb;\n  border-radius: 10px;\n  border: 1px solid #e2f0cb;\n  margin: 10px;\n  margin-top: 15px;\n  padding: 0;\n  padding-left: 15px;\n  padding-right: 3px;\n  margin-bottom: 15px;\n}\n\n.for_invite {\n  background: #76c961;\n  border-radius: 0;\n  box-shadow: none;\n  border: none;\n  margin: 10px;\n  margin-top: 15px;\n  padding: 0;\n  padding-left: 15px;\n  padding-right: 3px;\n  margin-bottom: 15px;\n}\n\n#for_eachitem {\n  font-size: 14px;\n  --inner-padding-end: 0px !important;\n  --border-color: #e2f0cb;\n}\n\n.for_eacheader {\n  font-size: 15px;\n  padding-left: 0;\n  font-weight: bold;\n  border-bottom: 1px solid #ebf1f0;\n}\n\n.for_eachimg {\n  width: 17px;\n  margin-right: 16px;\n  margin-top: 7px;\n  margin-left: 2px;\n}\n\n.for_eachimg2 {\n  width: 25px;\n  margin-top: 2px;\n  margin-right: 12px;\n  margin-left: 2px;\n}\n\n.for_invitetext {\n  font-size: 15px;\n  font-weight: bold;\n  text-align: center;\n}\n\n.upload_photo {\n  background: #76c961;\n  color: white;\n  padding: 3px;\n  border-radius: 50%;\n  font-size: 20px;\n  position: absolute;\n  top: 65%;\n  left: 52px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZW5kcmljay9EZXNrdG9wL1RlbG1vIFNvbHV0aW9ucy9Gb3JQcm9kdWN0aW9uL21heTEwMjAyMS9sYXN0IHNldHVwL2dyZWVudGh1bWJhcHAgY29weS9zcmMvYXBwL215YWNjb3VudC9teWFjY291bnQucGFnZS5zY3NzIiwic3JjL2FwcC9teWFjY291bnQvbXlhY2NvdW50LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLHFDQUFBO0VBQ0Esa0JBQUE7QUNDRjs7QURDQTtFQUNFLHVDQUFBO0FDRUY7O0FERUk7RUFDQSxzQkFBQTtFQUNBLHVCQUFBO0VBQ0EsMEJBQUE7RUFDQSwyQkFBQTtBQ0NKOztBREFNO0VBQ0UscUNBQUE7RUFDQSxzQkFBQTtFQUNBLHVCQUFBO0VBQ0EsMEJBQUE7RUFDQSwyQkFBQTtFQUNBLGtCQUFBO0FDRVI7O0FERUE7RUFDRSxpQkFBQTtFQUNBLG1CQUFBO0VBQ0EsNEJBQUE7RUFDQSw2QkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNDLFlBQUE7QUNDSDs7QURDQTtFQUNFLGdCQUFBO0VBQ0MsWUFBQTtBQ0VIOztBREFBO0VBQ0UsOEJBQUE7QUNHRjs7QUREQTtFQUNFLHlCQUFBO0FDSUY7O0FERkE7RUFDRSxlQUFBO0VBQ0Esa0JBQUE7QUNLRjs7QURIQTtFQUNFLGdDQUFBO0FDTUY7O0FESkE7RUFDRSxlQUFBO0VBQ0EsVUFBQTtBQ09GOztBRExBO0VBQ0UsNkJBQUE7RUFDQSxtQkFBQTtFQUNBLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0VBQ0EsVUFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtBQ1FGOztBRE5BO0VBQ0UsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtFQUNBLFVBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7QUNTRjs7QURQQTtFQUNFLGVBQUE7RUFDQSxtQ0FBQTtFQUNBLHVCQUFBO0FDVUY7O0FEUkE7RUFDRSxlQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0NBQUE7QUNXRjs7QURUQTtFQUNFLFdBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQ1lGOztBRFZBO0VBQ0UsV0FBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0FDYUY7O0FEWEE7RUFDRSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtBQ2NGOztBRFpBO0VBQ0UsbUJBQUE7RUFDRSxZQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLFVBQUE7QUNlSiIsImZpbGUiOiJzcmMvYXBwL215YWNjb3VudC9teWFjY291bnQucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmZvcl9oZWFkZXJ7XG4gIGJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci10ZXJ0aWFyeSk7XG4gIHBhZGRpbmctYm90dG9tOiAzJTtcbn1cbi5mb3JfYmFja2dyb3VuZHtcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItdGVydGlhcnkpO1xufVxuI2Zvcl90aHVtYm5haWxzIFxuICB7XG4gICAgaW9uLXRodW1ibmFpbCB7XG4gICAgd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcbiAgICBoZWlnaHQ6IDEwMCUgIWltcG9ydGFudDtcbiAgICBtYXgtd2lkdGg6IDVyZW0gIWltcG9ydGFudDtcbiAgICBtYXgtaGVpZ2h0OiA1cmVtICFpbXBvcnRhbnQ7XG4gICAgICBpbWd7XG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkIHJnYmEoMCwgMCwgMCwgMC4xMik7XG4gICAgICAgIHdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XG4gICAgICAgIGhlaWdodDogMTAwJSAhaW1wb3J0YW50O1xuICAgICAgICBtYXgtd2lkdGg6IDVyZW0gIWltcG9ydGFudDtcbiAgICAgICAgbWF4LWhlaWdodDogNXJlbSAhaW1wb3J0YW50O1xuICAgICAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgfVxuICB9XG59XG4uZm9yX2RpdntcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIHBhZGRpbmctYm90dG9tOiA1cHg7XG4gIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDQwcHg7XG4gIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiA0MHB4O1xuICBwYWRkaW5nLXRvcDogNSU7IFxuICBvdmVyZmxvdy15OiBhdXRvO1xuICAgaGVpZ2h0OjEwMCU7XG59XG4uZm9yX3Njcm9sbHtcbiAgb3ZlcmZsb3cteTogYXV0bztcbiAgIGhlaWdodDoxMDAlO1xufVxuaW9uLWl0ZW0ge1xuICAtLXBhZGRpbmctc3RhcnQ6IDAlICFpbXBvcnRhbnQ7XG59XG4uZm9yX2l0ZW17XG4gIC0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG59XG4uZm9yX3N0YXJpY29ue1xuICBmb250LXNpemU6IDI0cHg7XG4gIG1hcmdpbi1ib3R0b206IC0zJTtcbn1cbi5mb3JfdXNlcmJvcmRlcntcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNlYmYxZjA7XG59XG4uZm9yX3ZlcmlmeXtcbiAgZm9udC1zaXplOiAxM3B4O1xuICBwYWRkaW5nOiAwO1xufVxuLmZvcl9hY2NvdW50aXRlbXN7XG4gIGJveC1zaGFkb3c6IDAgMXB4IDhweCAjZTJmMGNiO1xuICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjZTJmMGNiO1xuICBtYXJnaW46IDEwcHg7XG4gIG1hcmdpbi10b3A6IDE1cHg7XG4gIHBhZGRpbmc6IDA7XG4gIHBhZGRpbmctbGVmdDogMTVweDtcbiAgcGFkZGluZy1yaWdodDogM3B4O1xuICBtYXJnaW4tYm90dG9tOiAxNXB4O1xufVxuLmZvcl9pbnZpdGV7XG4gIGJhY2tncm91bmQ6ICM3NmM5NjE7XG4gIGJvcmRlci1yYWRpdXM6IDA7XG4gIGJveC1zaGFkb3c6IG5vbmU7XG4gIGJvcmRlcjogbm9uZTtcbiAgbWFyZ2luOiAxMHB4O1xuICBtYXJnaW4tdG9wOiAxNXB4O1xuICBwYWRkaW5nOiAwO1xuICBwYWRkaW5nLWxlZnQ6IDE1cHg7XG4gIHBhZGRpbmctcmlnaHQ6IDNweDtcbiAgbWFyZ2luLWJvdHRvbTogMTVweDtcbn1cbiNmb3JfZWFjaGl0ZW17XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgLS1pbm5lci1wYWRkaW5nLWVuZDogMHB4ICFpbXBvcnRhbnQ7XG4gIC0tYm9yZGVyLWNvbG9yOiAjZTJmMGNiO1xufVxuLmZvcl9lYWNoZWFkZXJ7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgcGFkZGluZy1sZWZ0OiAwO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNlYmYxZjA7XG59XG4uZm9yX2VhY2hpbWd7XG4gIHdpZHRoOiAxN3B4O1xuICBtYXJnaW4tcmlnaHQ6IDE2cHg7XG4gIG1hcmdpbi10b3A6IDdweDtcbiAgbWFyZ2luLWxlZnQ6IDJweDtcbn1cbi5mb3JfZWFjaGltZzJ7XG4gIHdpZHRoOiAyNXB4O1xuICBtYXJnaW4tdG9wOiAycHg7XG4gIG1hcmdpbi1yaWdodDogMTJweDtcbiAgbWFyZ2luLWxlZnQ6IDJweDtcbn1cbi5mb3JfaW52aXRldGV4dHtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLnVwbG9hZF9waG90b3tcbiAgYmFja2dyb3VuZDogIzc2Yzk2MTtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgcGFkZGluZzogM3B4O1xuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICBmb250LXNpemU6IDIwcHg7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogNjUlO1xuICAgIGxlZnQ6IDUycHg7XG59IiwiLmZvcl9oZWFkZXIge1xuICBiYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItdGVydGlhcnkpO1xuICBwYWRkaW5nLWJvdHRvbTogMyU7XG59XG5cbi5mb3JfYmFja2dyb3VuZCB7XG4gIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLXRlcnRpYXJ5KTtcbn1cblxuI2Zvcl90aHVtYm5haWxzIGlvbi10aHVtYm5haWwge1xuICB3aWR0aDogMTAwJSAhaW1wb3J0YW50O1xuICBoZWlnaHQ6IDEwMCUgIWltcG9ydGFudDtcbiAgbWF4LXdpZHRoOiA1cmVtICFpbXBvcnRhbnQ7XG4gIG1heC1oZWlnaHQ6IDVyZW0gIWltcG9ydGFudDtcbn1cbiNmb3JfdGh1bWJuYWlscyBpb24tdGh1bWJuYWlsIGltZyB7XG4gIGJvcmRlcjogMXB4IHNvbGlkIHJnYmEoMCwgMCwgMCwgMC4xMik7XG4gIHdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XG4gIGhlaWdodDogMTAwJSAhaW1wb3J0YW50O1xuICBtYXgtd2lkdGg6IDVyZW0gIWltcG9ydGFudDtcbiAgbWF4LWhlaWdodDogNXJlbSAhaW1wb3J0YW50O1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG59XG5cbi5mb3JfZGl2IHtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIHBhZGRpbmctYm90dG9tOiA1cHg7XG4gIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDQwcHg7XG4gIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiA0MHB4O1xuICBwYWRkaW5nLXRvcDogNSU7XG4gIG92ZXJmbG93LXk6IGF1dG87XG4gIGhlaWdodDogMTAwJTtcbn1cblxuLmZvcl9zY3JvbGwge1xuICBvdmVyZmxvdy15OiBhdXRvO1xuICBoZWlnaHQ6IDEwMCU7XG59XG5cbmlvbi1pdGVtIHtcbiAgLS1wYWRkaW5nLXN0YXJ0OiAwJSAhaW1wb3J0YW50O1xufVxuXG4uZm9yX2l0ZW0ge1xuICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xufVxuXG4uZm9yX3N0YXJpY29uIHtcbiAgZm9udC1zaXplOiAyNHB4O1xuICBtYXJnaW4tYm90dG9tOiAtMyU7XG59XG5cbi5mb3JfdXNlcmJvcmRlciB7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZWJmMWYwO1xufVxuXG4uZm9yX3ZlcmlmeSB7XG4gIGZvbnQtc2l6ZTogMTNweDtcbiAgcGFkZGluZzogMDtcbn1cblxuLmZvcl9hY2NvdW50aXRlbXMge1xuICBib3gtc2hhZG93OiAwIDFweCA4cHggI2UyZjBjYjtcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgYm9yZGVyOiAxcHggc29saWQgI2UyZjBjYjtcbiAgbWFyZ2luOiAxMHB4O1xuICBtYXJnaW4tdG9wOiAxNXB4O1xuICBwYWRkaW5nOiAwO1xuICBwYWRkaW5nLWxlZnQ6IDE1cHg7XG4gIHBhZGRpbmctcmlnaHQ6IDNweDtcbiAgbWFyZ2luLWJvdHRvbTogMTVweDtcbn1cblxuLmZvcl9pbnZpdGUge1xuICBiYWNrZ3JvdW5kOiAjNzZjOTYxO1xuICBib3JkZXItcmFkaXVzOiAwO1xuICBib3gtc2hhZG93OiBub25lO1xuICBib3JkZXI6IG5vbmU7XG4gIG1hcmdpbjogMTBweDtcbiAgbWFyZ2luLXRvcDogMTVweDtcbiAgcGFkZGluZzogMDtcbiAgcGFkZGluZy1sZWZ0OiAxNXB4O1xuICBwYWRkaW5nLXJpZ2h0OiAzcHg7XG4gIG1hcmdpbi1ib3R0b206IDE1cHg7XG59XG5cbiNmb3JfZWFjaGl0ZW0ge1xuICBmb250LXNpemU6IDE0cHg7XG4gIC0taW5uZXItcGFkZGluZy1lbmQ6IDBweCAhaW1wb3J0YW50O1xuICAtLWJvcmRlci1jb2xvcjogI2UyZjBjYjtcbn1cblxuLmZvcl9lYWNoZWFkZXIge1xuICBmb250LXNpemU6IDE1cHg7XG4gIHBhZGRpbmctbGVmdDogMDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZWJmMWYwO1xufVxuXG4uZm9yX2VhY2hpbWcge1xuICB3aWR0aDogMTdweDtcbiAgbWFyZ2luLXJpZ2h0OiAxNnB4O1xuICBtYXJnaW4tdG9wOiA3cHg7XG4gIG1hcmdpbi1sZWZ0OiAycHg7XG59XG5cbi5mb3JfZWFjaGltZzIge1xuICB3aWR0aDogMjVweDtcbiAgbWFyZ2luLXRvcDogMnB4O1xuICBtYXJnaW4tcmlnaHQ6IDEycHg7XG4gIG1hcmdpbi1sZWZ0OiAycHg7XG59XG5cbi5mb3JfaW52aXRldGV4dCB7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLnVwbG9hZF9waG90byB7XG4gIGJhY2tncm91bmQ6ICM3NmM5NjE7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgcGFkZGluZzogM3B4O1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG4gIGZvbnQtc2l6ZTogMjBweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDY1JTtcbiAgbGVmdDogNTJweDtcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/myaccount/myaccount.page.ts":
  /*!*********************************************!*\
    !*** ./src/app/myaccount/myaccount.page.ts ***!
    \*********************************************/

  /*! exports provided: MyaccountPage */

  /***/
  function srcAppMyaccountMyaccountPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "MyaccountPage", function () {
      return MyaccountPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _providers_credential_provider__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../providers/credential-provider */
    "./src/providers/credential-provider.ts");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _register_register_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../register/register.page */
    "./src/app/register/register.page.ts");

    var MyaccountPage = /*#__PURE__*/function () {
      function MyaccountPage(router, postPvdr, modalController, navCtrl, storage) {
        _classCallCheck(this, MyaccountPage);

        this.router = router;
        this.postPvdr = postPvdr;
        this.modalController = modalController;
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.login_user_id = "";
        this.user_rating = "";
        this.vip_package = "";
      }

      _createClass(MyaccountPage, [{
        key: "goHelp",
        value: function goHelp() {
          //window.location.href="http://greenthumbtrade.com/help";
          this.navCtrl.navigateForward(['tabs/help'], {
            animated: false
          });
        }
      }, {
        key: "goCommunityForum",
        value: function goCommunityForum() {
          this.navCtrl.navigateForward(['tabs/communityforum'], {
            animated: false
          });
        }
      }, {
        key: "goInvite",
        value: function goInvite() {
          this.router.navigate(['invite']);
        }
      }, {
        key: "goSubscribe",
        value: function goSubscribe() {
          this.router.navigate(['subscribe']);
        }
      }, {
        key: "goApple",
        value: function goApple() {
          this.router.navigate(['applepay']);
        }
      }, {
        key: "goGoogle",
        value: function goGoogle() {
          this.router.navigate(['googlepay']);
        }
      }, {
        key: "goArchive",
        value: function goArchive() {
          this.router.navigate(['archieve']);
        }
      }, {
        key: "goOffers",
        value: function goOffers() {
          this.router.navigate(['tabs/offers']);
        }
      }, {
        key: "openRegistermodal",
        value: function openRegistermodal() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
            var modal;
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    _context.next = 2;
                    return this.modalController.create({
                      component: _register_register_page__WEBPACK_IMPORTED_MODULE_6__["RegisterPage"],
                      cssClass: 'registermodalstyle'
                    });

                  case 2:
                    modal = _context.sent;
                    _context.next = 5;
                    return modal.present();

                  case 5:
                    return _context.abrupt("return", _context.sent);

                  case 6:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        }
      }, {
        key: "goViewprofile",
        value: function goViewprofile() {
          this.router.navigate(['tabs/viewprofile']);
          var navigationExtras = {
            queryParams: {
              user_profile_id: this.login_user_id
            }
          };
          console.log("user_pfo:" + this.login_user_id);
          this.navCtrl.navigateForward(['viewitemsellerprofile'], navigationExtras);
        }
      }, {
        key: "goAccountsettings",
        value: function goAccountsettings() {
          this.router.navigate(['accountsettings']);
        }
      }, {
        key: "goBack",
        value: function goBack() {
          this.router.navigate(['tabs']);
        }
      }, {
        key: "goFavorites",
        value: function goFavorites() {
          this.router.navigate(['tabs/tab2']);
        }
      }, {
        key: "goSettings",
        value: function goSettings() {
          this.router.navigate(['settings']);
        }
      }, {
        key: "goVIP",
        value: function goVIP() {
          this.router.navigate(['vipackage']);
        }
      }, {
        key: "goFollow",
        value: function goFollow() {
          this.router.navigate(['tabs/sellersifollow']);
        }
      }, {
        key: "goEditProfile",
        value: function goEditProfile() {
          this.router.navigate(['editprofile']);
        }
      }, {
        key: "goLogout",
        value: function goLogout() {
          this.router.navigate(['logoutscreen']);
        }
      }, {
        key: "goWallet",
        value: function goWallet() {
          this.router.navigate(['wallet']);
        }
      }, {
        key: "goPaymentRequest",
        value: function goPaymentRequest() {
          this.router.navigate(['payoneerid']);
        }
      }, {
        key: "goPublished",
        value: function goPublished() {
          this.router.navigate(['mybroadcast']);
        }
      }, {
        key: "goStore",
        value: function goStore() {
          this.router.navigate(['tab4']);
        }
      }, {
        key: "goVIPPackage",
        value: function goVIPPackage() {
          this.router.navigate(['vippackage']);
        }
      }, {
        key: "goPaymentmethod",
        value: function goPaymentmethod() {
          this.router.navigate(['paymentsettings']);
        }
      }, {
        key: "update_photo",
        value: function update_photo() {
          this.router.navigate(['uploadphoto']);
        }
      }, {
        key: "checkSubscription",
        value: function checkSubscription() {
          var _this = this;

          this.storage.get("user_id").then(function (user_id) {
            var body2 = {
              action: 'checkSubscription',
              user_id: user_id
            };

            _this.postPvdr.postData(body2, 'subscription.php').subscribe(function (data) {
              if (data.success) {
                for (var key in data.result) {
                  _this.vip_package = data.result[key].vip_package_id;
                }
              }
            });
          });
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {//this.loadUser();
        }
      }, {
        key: "ionViewWillEnter",
        value: function ionViewWillEnter() {
          this.loadUser(); //this.plotData();
          //this.checkSubscription();
        }
      }, {
        key: "plotData",
        value: function plotData() {
          var _this2 = this;

          this.storage.get('greenthumb_user_id').then(function (user_id) {
            var body = {
              action: 'getUsername',
              user_id: user_id
            };
            console.log(JSON.stringify(body));

            _this2.postPvdr.postData(body, 'user.php').subscribe(function (data) {
              return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
                return regeneratorRuntime.wrap(function _callee2$(_context2) {
                  while (1) {
                    switch (_context2.prev = _context2.next) {
                      case 0:
                        this.username = data.username;

                      case 1:
                      case "end":
                        return _context2.stop();
                    }
                  }
                }, _callee2, this);
              }));
            });
          });
        }
      }, {
        key: "loadUser",
        value: function loadUser() {
          var _this3 = this;

          this.storage.get("greenthumb_user_id").then(function (user_id) {
            _this3.login_user_id = user_id;
            var body = {
              action: 'getuserdata',
              user_id: user_id
            };

            _this3.postPvdr.postData(body, 'credentials-api.php').subscribe(function (data) {
              return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this3, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
                return regeneratorRuntime.wrap(function _callee3$(_context3) {
                  while (1) {
                    switch (_context3.prev = _context3.next) {
                      case 0:
                        if (data.success) {
                          this.id = data.result.id;
                          this.picture = data.result.photo;
                          this.username = data.result.nickname;
                          this.storage.set('id', this.id);
                          this.storage.set('picture', this.picture);
                          this.complete_pic = this.postPvdr.myServer() + "/greenthumb/images/" + this.picture;
                          console.log("mycompletePic:" + this.complete_pic);
                          this.id = data.result.id;
                          this.fname = data.result.fname;
                          this.lname = data.result.lname;
                          this.city = data.result.city;
                          this.country = data.result.country;
                          this.user_locator_id = data.result.user_locator_id;
                          this.user_rating = data.result.user_rating;
                          this.login_type = data.result.login_type_id;
                          console.log("login_type:" + this.login_type); // console.log("user_leveldd:"+this.user_level);
                          // console.log("user_locator_id:"+this.user_locator_id);
                        } else {
                          this.id = "0";
                          this.fname = "Anonymous";
                          this.lname = "User";
                          this.city = "Unknown";
                          this.country = "Address";
                          this.user_locator_id = "0";
                          this.user_level = 1;
                        }

                      case 1:
                      case "end":
                        return _context3.stop();
                    }
                  }
                }, _callee3, this);
              }));
            });
          });
        }
      }]);

      return MyaccountPage;
    }();

    MyaccountPage.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }, {
        type: _providers_credential_provider__WEBPACK_IMPORTED_MODULE_3__["PostProvider"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ModalController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"]
      }, {
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"]
      }];
    };

    MyaccountPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-myaccount',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./myaccount.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/myaccount/myaccount.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./myaccount.page.scss */
      "./src/app/myaccount/myaccount.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _providers_credential_provider__WEBPACK_IMPORTED_MODULE_3__["PostProvider"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ModalController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"], _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"]])], MyaccountPage);
    /***/
  }
}]);
//# sourceMappingURL=myaccount-myaccount-module-es5.js.map