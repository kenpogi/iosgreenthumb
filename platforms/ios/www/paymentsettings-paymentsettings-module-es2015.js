(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["paymentsettings-paymentsettings-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/paymentsettings/paymentsettings.page.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/paymentsettings/paymentsettings.page.html ***!
  \*************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header mode=\"ios\">\n  <ion-toolbar mode=\"ios\">\n    <ion-buttons slot=\"secondary\" (click)=\"goBack()\">\n      <ion-button mode=\"ios\">\n          <ion-icon name=\"arrow-back\"  mode=\"ios\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n\n      <ion-title>Payment Method</ion-title>\n\n      <ion-buttons slot=\"primary\" mode=\"ios\">\n          <ion-button mode=\"ios\">\n              <img src=\"assets/icon/brixylogo.png\" style=\"width: 32px;\" mode=\"ios\">\n          </ion-button>\n        </ion-buttons>\n    </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n  <ion-card mode=\"ios\">\n    <ion-card-content mode=\"ios\">\n      <ion-item lines=\"none\" mode=\"ios\">\n        <ion-thumbnail slot=\"start\" mode=\"ios\">\n          <img src=\"assets/icon/payoneericon.png\">\n        </ion-thumbnail>\n        <ion-label mode=\"ios\">\n          <h3 style=\"font-weight: bold;\">Payoneer Settings</h3>\n        </ion-label>\n        <ion-icon name=\"arrow-forward\" style=\"font-size: 20px;\" color=\"primary\" slot=\"end\" mode=\"ios\"></ion-icon>\n      </ion-item>\n    </ion-card-content>\n  </ion-card>\n\n  <ion-card mode=\"ios\" style=\"margin-top: -3%;\">\n    <ion-card-content mode=\"ios\">\n      <ion-item lines=\"none\" mode=\"ios\">\n        <ion-thumbnail slot=\"start\" mode=\"ios\">\n          <img src=\"assets/icon/paypal.png\">\n        </ion-thumbnail>\n        <ion-label mode=\"ios\">\n          <h3 style=\"font-weight: bold;\">Paypal Settings</h3>\n        </ion-label>\n        <ion-icon name=\"arrow-forward\" style=\"font-size: 20px;\" color=\"primary\" slot=\"end\" mode=\"ios\"></ion-icon>\n      </ion-item>\n    </ion-card-content>\n  </ion-card>\n\n  <ion-card mode=\"ios\" style=\"margin-top: -3%;\">\n    <ion-card-content mode=\"ios\">\n      <ion-item lines=\"none\" mode=\"ios\">\n        <ion-thumbnail slot=\"start\" mode=\"ios\">\n          <img src=\"assets/icon/googlepay.png\">\n        </ion-thumbnail>\n        <ion-label mode=\"ios\">\n          <h3 style=\"font-weight: bold;\">Google Pay</h3>\n        </ion-label>\n        <ion-icon name=\"arrow-forward\" style=\"font-size: 20px;\" color=\"primary\" slot=\"end\" mode=\"ios\"></ion-icon>\n      </ion-item>\n    </ion-card-content>\n  </ion-card>\n\n  <ion-card mode=\"ios\" style=\"margin-top: -3%;\">\n    <ion-card-content mode=\"ios\">\n      <ion-item lines=\"none\" mode=\"ios\">\n        <ion-thumbnail slot=\"start\" mode=\"ios\">\n          <img src=\"assets/icon/applepay.png\">\n        </ion-thumbnail>\n        <ion-label mode=\"ios\">\n          <h3 style=\"font-weight: bold;\">Apple Pay</h3>\n        </ion-label>\n        <ion-icon name=\"arrow-forward\" style=\"font-size: 20px;\" color=\"primary\" slot=\"end\" mode=\"ios\"></ion-icon>\n      </ion-item>\n    </ion-card-content>\n  </ion-card>\n\n  \n  \n</ion-content>\n");

/***/ }),

/***/ "./src/app/paymentsettings/paymentsettings-routing.module.ts":
/*!*******************************************************************!*\
  !*** ./src/app/paymentsettings/paymentsettings-routing.module.ts ***!
  \*******************************************************************/
/*! exports provided: PaymentsettingsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaymentsettingsPageRoutingModule", function() { return PaymentsettingsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _paymentsettings_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./paymentsettings.page */ "./src/app/paymentsettings/paymentsettings.page.ts");




const routes = [
    {
        path: '',
        component: _paymentsettings_page__WEBPACK_IMPORTED_MODULE_3__["PaymentsettingsPage"]
    }
];
let PaymentsettingsPageRoutingModule = class PaymentsettingsPageRoutingModule {
};
PaymentsettingsPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], PaymentsettingsPageRoutingModule);



/***/ }),

/***/ "./src/app/paymentsettings/paymentsettings.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/paymentsettings/paymentsettings.module.ts ***!
  \***********************************************************/
/*! exports provided: PaymentsettingsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaymentsettingsPageModule", function() { return PaymentsettingsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _paymentsettings_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./paymentsettings-routing.module */ "./src/app/paymentsettings/paymentsettings-routing.module.ts");
/* harmony import */ var _paymentsettings_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./paymentsettings.page */ "./src/app/paymentsettings/paymentsettings.page.ts");







let PaymentsettingsPageModule = class PaymentsettingsPageModule {
};
PaymentsettingsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _paymentsettings_routing_module__WEBPACK_IMPORTED_MODULE_5__["PaymentsettingsPageRoutingModule"]
        ],
        declarations: [_paymentsettings_page__WEBPACK_IMPORTED_MODULE_6__["PaymentsettingsPage"]]
    })
], PaymentsettingsPageModule);



/***/ }),

/***/ "./src/app/paymentsettings/paymentsettings.page.scss":
/*!***********************************************************!*\
  !*** ./src/app/paymentsettings/paymentsettings.page.scss ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-item {\n  --padding-start: 0% !important;\n  --inner-padding-end: 0% !important;\n}\n\n.item-detail-icon {\n  color: var(--ion-color-primary);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZW5kcmljay9EZXNrdG9wL1RlbG1vIFNvbHV0aW9ucy9Gb3JQcm9kdWN0aW9uL21heTEwMjAyMS9sYXN0IHNldHVwL2dyZWVudGh1bWJhcHAgY29weS9zcmMvYXBwL3BheW1lbnRzZXR0aW5ncy9wYXltZW50c2V0dGluZ3MucGFnZS5zY3NzIiwic3JjL2FwcC9wYXltZW50c2V0dGluZ3MvcGF5bWVudHNldHRpbmdzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQTtFQUNJLDhCQUFBO0VBQ0Esa0NBQUE7QUNBSjs7QURFRTtFQUNFLCtCQUFBO0FDQ0oiLCJmaWxlIjoic3JjL2FwcC9wYXltZW50c2V0dGluZ3MvcGF5bWVudHNldHRpbmdzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIlxuaW9uLWl0ZW0ge1xuICAgIC0tcGFkZGluZy1zdGFydDogMCUgIWltcG9ydGFudDtcbiAgICAtLWlubmVyLXBhZGRpbmctZW5kOiAwJSAhaW1wb3J0YW50O1xuICB9XG4gIC5pdGVtLWRldGFpbC1pY29uIHtcbiAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xufSIsImlvbi1pdGVtIHtcbiAgLS1wYWRkaW5nLXN0YXJ0OiAwJSAhaW1wb3J0YW50O1xuICAtLWlubmVyLXBhZGRpbmctZW5kOiAwJSAhaW1wb3J0YW50O1xufVxuXG4uaXRlbS1kZXRhaWwtaWNvbiB7XG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XG59Il19 */");

/***/ }),

/***/ "./src/app/paymentsettings/paymentsettings.page.ts":
/*!*********************************************************!*\
  !*** ./src/app/paymentsettings/paymentsettings.page.ts ***!
  \*********************************************************/
/*! exports provided: PaymentsettingsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaymentsettingsPage", function() { return PaymentsettingsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");



let PaymentsettingsPage = class PaymentsettingsPage {
    constructor(router) {
        this.router = router;
    }
    ngOnInit() {
    }
    goBack() {
        // this.router.navigate(['myaccount']);
        window.history.back();
    }
};
PaymentsettingsPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
PaymentsettingsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-paymentsettings',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./paymentsettings.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/paymentsettings/paymentsettings.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./paymentsettings.page.scss */ "./src/app/paymentsettings/paymentsettings.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
], PaymentsettingsPage);



/***/ })

}]);
//# sourceMappingURL=paymentsettings-paymentsettings-module-es2015.js.map