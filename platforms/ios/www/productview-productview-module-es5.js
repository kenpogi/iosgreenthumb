function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["productview-productview-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/productview/productview.page.html":
  /*!*****************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/productview/productview.page.html ***!
    \*****************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppProductviewProductviewPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n<ion-content>\n  <ion-card mode=\"ios\" no-margin class=\"for_card\" style=\"box-shadow:none\">\n    <ion-card-header class=\"for_cardheader\">\n      <ion-card-subtitle class=\"for_back\" mode=\"ios\" (click)=\"goBack()\">Back</ion-card-subtitle>\n      <!-- <ion-icon class=\"for_back\" mode=\"ios\" name=\"arrow-back\" (click)=\"goBack()\">Back</ion-icon> -->\n      <ion-card-subtitle *ngIf=\"!ownProfile\" class=\"for_report\" mode=\"ios\" (click)=\"goHelp()\">Help</ion-card-subtitle>\n      <img *ngIf=\"!ImageArray\" src=\"assets/greenthumb-images/tomato.png\" class=\"for_imgfruit\"> \n\n      <ion-slides *ngIf=\"ImageArray\" [options]=\"slideOptsOne\" pager=\"true\" mode=\"ios\" id=\"slide_forpromo\">\n        <ion-slide *ngFor=\"let slide of ImageArray\" mode=\"ios\">\n          <div class=\"slide\" mode=\"ios\">\n            <ion-card mode=\"ios\" class=\"promocard\">\n              <ion-card-header mode=\"ios\" class=\"promoheader\" style=\"padding: 7px;\">\n                <ion-row>\n                 \n                  <ion-col no-padding text-right>\n                    <img src=\"{{slide.picture_filename}}\" class=\"for_imgfruit\">\n                  </ion-col>\n    \n                </ion-row>\n              </ion-card-header>\n            </ion-card>\n          </div>\n        </ion-slide>\n      </ion-slides>\n\n    </ion-card-header>\n  </ion-card> \n  \n  <ion-row style=\"margin-top: -25px;\">\n    <ion-col text-left>\n      <ion-chip mode=\"ios\" (click)=\"goSellerprof()\" style=\"--background: white; margin-left: 20px;\">\n        <ion-avatar>\n          <div *ngIf=\"!picture\">\n            <img src=\"assets/greenthumb-images/userpicdark.png\">\n          </div>\n          <div *ngIf=\"picture\">\n            <img [src] = \"complete_pic\">\n          </div>\n        </ion-avatar>\n        <ion-label style=\"font-size: 12px;\" >by {{item_seller}}</ion-label>\n      </ion-chip>\n    </ion-col>\n    <ion-col text-right>\n      <ion-icon *ngIf=\"!ownProfile\" mode=\"ios\" class=\"for_social\" (click)=\"saveItem()\" [color]=\"statusSave ? 'danger' : 'light'\" name=\"heart\"></ion-icon>&nbsp;\n      <!-- <ion-icon mode=\"ios\" class=\"for_social\" color=\"light\" name=\"heart\"></ion-icon>&nbsp; -->\n      <ion-icon mode=\"ios\" class=\"for_social\" color=\"light\" name=\"share-alt\"></ion-icon>\n    </ion-col>\n  </ion-row>\n  <ion-row padding style=\"padding-top: 10px;padding-bottom: 0;\">\n    <ion-col text-left size=\"7\">\n      <ionic4-star-rating #rating\n        activeIcon = \"ios-star\"\n        defaultIcon = \"ios-star-outline\"\n        activeColor = \"#ffce00\" \n        defaultColor = \"#ffce00\"\n        readonly=\"false\"\n        rating=\"{{user_rating}}\"\n        fontSize = \"15px\">\n      </ionic4-star-rating>&nbsp;\n      <!-- <span class=\"for_spanstar\">({{user_rating}})</span> -->\n      <ion-label color=\"dark\"><h1 style=\"margin-bottom: -2px;\"><b>{{item_name}}</b></h1></ion-label>\n      <ion-label class=\"for_categname\">{{item_category}}</ion-label>\n    </ion-col>\n    <ion-col text-right size=\"5\">\n      <ion-label color=\"secondary\"><h1 class=\"for_pr\"><b>${{item_price}}</b></h1></ion-label>\n    </ion-col>\n  </ion-row>\n  <ion-row style=\"margin-top: -5px;padding-top: 0;padding-bottom: 0;\" padding >\n    <ion-col size=\"12\" no-padding text-left> \n      <ion-icon class=\"for_pin\" color=\"primary\" name=\"pin\" mode=\"ios\"></ion-icon>\n      <ion-label class=\"for_locname\" style=\"font-size: 13px;text-transform:capitalize;\" color=\"dark\">\n        {{item_location}}</ion-label>\n    </ion-col>\n  </ion-row>\n  <ion-row padding style=\"padding-top: 0;padding-bottom: 5px;\">\n    <ion-col>\n      <ion-item mode=\"ios\" class=\"for_iteminfo\">\n        <ion-label color=\"dark\" class=\"for_label\"><b>Quantity and Unit</b></ion-label>\n        <ion-label color=\"medium\" slot=\"end\" class=\"for_text\">{{item_quantity}} \n          <label *ngIf=\"item_stock_id=='1'\">\n            <label *ngIf=\"item_quantity>1\">pieces</label>\n            <label *ngIf=\"item_quantity<=1\">piece</label>\n          </label>\n          <label *ngIf=\"item_stock_id=='2'\">\n            <label *ngIf=\"item_quantity>1\">pounds</label>\n            <label *ngIf=\"item_quantity<=1\">pound</label>\n          </label>\n          <label *ngIf=\"item_stock_id=='3'\">\n            <label *ngIf=\"item_quantity>1\">ounces</label>\n            <label *ngIf=\"item_quantity<=1\">ounce</label>\n          </label>\n          <label *ngIf=\"item_stock_id=='4'\">\n            {{item_other_stock}}\n          </label>\n        </ion-label>\n      </ion-item>\n      <ion-item mode=\"ios\" class=\"for_iteminforest\">\n        <ion-label color=\"dark\" class=\"for_label\"><b>Availability</b></ion-label>\n        <ion-label color=\"medium\" slot=\"end\" class=\"for_text\">In Stock</ion-label>\n      </ion-item>\n      <ion-item mode=\"ios\" class=\"for_iteminforest\">\n        <ion-label color=\"dark\" class=\"for_label\"><b>Delivery Type</b></ion-label>\n        <ion-label color=\"medium\" slot=\"end\" class=\"for_text\">Local Pickup</ion-label>\n      </ion-item>\n    </ion-col>\n  </ion-row>\n  <ion-row padding style=\"padding-top: 0;\">\n    <ion-col text-left no-padding size=\"4\">\n      <ion-label style=\"font-size: 14px;margin-left: 5px;\" color=\"dark\" class=\"for_label\"><b>Description</b></ion-label>\n    </ion-col>\n    <ion-col no-padding size=\"8\" text-right>\n      <p class=\"for_txtoverview\">\n        {{item_description}}\n      </p>\n    </ion-col>\n  </ion-row>\n</ion-content>\n<div id=\"overlay\" padding>\n  <ion-row *ngIf=\"!ownProfile\">\n    <ion-col size=\"2\" no-padding>\n      <ion-button color=\"danger\" expand=\"block\" mode=\"ios\" (click)=\"goReport()\">\n        <ion-icon name=\"warning\" mode=\"ios\"></ion-icon>\n      </ion-button>\n    </ion-col>\n    <ion-col size=\"4\" no-padding>\n      <ion-button fill=\"outline\" expand=\"block\"  (click)=\"goMessage()\" mode=\"ios\">\n        MESSAGE\n      </ion-button>\n    </ion-col>\n    <ion-col size=\"6\" no-padding>\n      <ion-button *ngIf=\"!pending_purchase\" expand=\"block\" (click)=\"purchaseProduct()\" mode=\"ios\">\n        PURCHASE ITEM\n      </ion-button>\n      <ion-button  *ngIf=\"pending_purchase\" expand=\"block\" (click)=\"cancelPurchase()\" mode=\"ios\">\n        CANCEL PURCHASE\n      </ion-button>\n    </ion-col>\n  </ion-row>\n  <ion-row *ngIf=\"ownProfile\">\n    <ion-col size=\"2\" no-padding>\n      <ion-button id=\"for_btn\" fill=\"outline\" expand=\"block\" mode=\"ios\" (click)=\"goOffers()\">\n        <ion-icon name=\"basket\" mode=\"md\"></ion-icon>\n      </ion-button>\n    </ion-col>\n    <ion-col size=\"4\" no-padding>\n      <ion-button id=\"for_btn\" (click)=\"deleteItem(item_id)\" color=\"danger\" expand=\"block\"  mode=\"ios\">\n        DELETE\n      </ion-button>\n    </ion-col>\n    <ion-col size=\"6\" no-padding>\n      <ion-button expand=\"block\" (click)=\"goEdit(item_id)\" mode=\"ios\" >\n        <ion-icon mode=\"md\" name=\"create\"></ion-icon>&nbsp;&nbsp;UPDATE ITEM\n      </ion-button>\n    </ion-col>\n  </ion-row>\n</div>\n";
    /***/
  },

  /***/
  "./src/app/productview/productview-routing.module.ts":
  /*!***********************************************************!*\
    !*** ./src/app/productview/productview-routing.module.ts ***!
    \***********************************************************/

  /*! exports provided: ProductviewPageRoutingModule */

  /***/
  function srcAppProductviewProductviewRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ProductviewPageRoutingModule", function () {
      return ProductviewPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _productview_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./productview.page */
    "./src/app/productview/productview.page.ts");

    var routes = [{
      path: '',
      component: _productview_page__WEBPACK_IMPORTED_MODULE_3__["ProductviewPage"]
    }];

    var ProductviewPageRoutingModule = function ProductviewPageRoutingModule() {
      _classCallCheck(this, ProductviewPageRoutingModule);
    };

    ProductviewPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], ProductviewPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/productview/productview.module.ts":
  /*!***************************************************!*\
    !*** ./src/app/productview/productview.module.ts ***!
    \***************************************************/

  /*! exports provided: ProductviewPageModule */

  /***/
  function srcAppProductviewProductviewModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ProductviewPageModule", function () {
      return ProductviewPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _productview_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./productview-routing.module */
    "./src/app/productview/productview-routing.module.ts");
    /* harmony import */


    var ionic4_star_rating__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ionic4-star-rating */
    "./node_modules/ionic4-star-rating/dist/index.js");
    /* harmony import */


    var _productview_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./productview.page */
    "./src/app/productview/productview.page.ts");

    var ProductviewPageModule = function ProductviewPageModule() {
      _classCallCheck(this, ProductviewPageModule);
    };

    ProductviewPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], ionic4_star_rating__WEBPACK_IMPORTED_MODULE_6__["StarRatingModule"], _productview_routing_module__WEBPACK_IMPORTED_MODULE_5__["ProductviewPageRoutingModule"]],
      declarations: [_productview_page__WEBPACK_IMPORTED_MODULE_7__["ProductviewPage"]]
    })], ProductviewPageModule);
    /***/
  },

  /***/
  "./src/app/productview/productview.page.scss":
  /*!***************************************************!*\
    !*** ./src/app/productview/productview.page.scss ***!
    \***************************************************/

  /*! exports provided: default */

  /***/
  function srcAppProductviewProductviewPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".for_card {\n  border-top-left-radius: 0;\n  border-top-right-radius: 0;\n  border-bottom-right-radius: 0px;\n  border-bottom-left-radius: 30px;\n}\n\n.for_cardheader {\n  background: linear-gradient(135deg, #e2f0cb 8%, #e2f0cb 83%);\n  padding-top: 30px;\n}\n\n.for_editimage {\n  --background: white;\n  margin-top: -25px;\n  margin-right: 0;\n  border: 1px solid #e2f0cb;\n}\n\n#for_btn {\n  --padding-start: 0;\n  --padding-end: 0;\n}\n\n.for_pr {\n  margin-bottom: -2px;\n  font-size: 35px;\n}\n\n.for_social {\n  zoom: 1.5;\n  background: #76c961;\n  border-radius: 25px;\n  padding: 4px;\n}\n\n.for_iteminfo {\n  --padding-start:0;\n  font-size: 14px;\n  --background: transparent;\n}\n\n.for_iteminforest {\n  --padding-start:0;\n  font-size: 14px;\n  margin-top: -10px;\n  --background: transparent;\n}\n\n.for_label {\n  margin-bottom: -2px;\n}\n\n.for_text {\n  margin-bottom: -2px;\n  text-align: right;\n  margin-right: 0;\n}\n\n.for_pin {\n  margin-bottom: -3px;\n}\n\n.for_txtoverview {\n  font-size: 14px;\n  margin-top: 0;\n  color: #989aa2;\n  padding-right: 12px;\n  padding-left: 0;\n}\n\n.for_back {\n  position: absolute;\n  left: 5%;\n  top: 5%;\n  font-size: 15px;\n  color: black;\n  margin: 0;\n  font-weight: normal;\n  text-transform: capitalize;\n  font-family: inherit;\n}\n\n.for_report {\n  position: absolute;\n  right: 5%;\n  top: 5%;\n  font-size: 15px;\n  color: black;\n  margin: 0;\n  font-weight: normal;\n  text-transform: capitalize;\n  font-family: inherit;\n}\n\n.for_spanstar {\n  font-size: 14px;\n  font-weight: normal;\n}\n\n.for_staricon {\n  font-size: 18px;\n  margin-bottom: -4px;\n}\n\n#overlay {\n  width: 100%;\n  height: 85px;\n  background: white;\n  z-index: 20;\n  bottom: 0%;\n  left: 0;\n  border-top: 1px solid #e2e2e2;\n}\n\nion-avatar {\n  width: 100% !important;\n  max-width: 25px !important;\n}\n\nion-avatar img {\n  border: 1px solid rgba(0, 0, 0, 0.12);\n  width: 100% !important;\n  height: 100% !important;\n  max-width: 25px !important;\n  max-height: 25px !important;\n  border-radius: 50%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZW5kcmljay9EZXNrdG9wL1RlbG1vIFNvbHV0aW9ucy9Gb3JQcm9kdWN0aW9uL21heTEwMjAyMS9sYXN0IHNldHVwL2dyZWVudGh1bWJhcHAgY29weS9zcmMvYXBwL3Byb2R1Y3R2aWV3L3Byb2R1Y3R2aWV3LnBhZ2Uuc2NzcyIsInNyYy9hcHAvcHJvZHVjdHZpZXcvcHJvZHVjdHZpZXcucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVFO0VBQ0UseUJBQUE7RUFDQSwwQkFBQTtFQUNBLCtCQUFBO0VBQ0EsK0JBQUE7QUNESjs7QURHRTtFQUdFLDREQUFBO0VBQ0EsaUJBQUE7QUNBSjs7QURFRTtFQUNFLG1CQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0VBQ0EseUJBQUE7QUNDSjs7QURDRTtFQUNFLGtCQUFBO0VBQ0EsZ0JBQUE7QUNFSjs7QURBRTtFQUNFLG1CQUFBO0VBQW9CLGVBQUE7QUNJeEI7O0FERkU7RUFDRSxTQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7QUNLSjs7QURIRTtFQUNFLGlCQUFBO0VBQWtCLGVBQUE7RUFDbEIseUJBQUE7QUNPSjs7QURMRTtFQUNFLGlCQUFBO0VBQWtCLGVBQUE7RUFDbEIsaUJBQUE7RUFDQSx5QkFBQTtBQ1NKOztBRFBFO0VBQ0UsbUJBQUE7QUNVSjs7QURSRTtFQUNFLG1CQUFBO0VBQW9CLGlCQUFBO0VBQ3BCLGVBQUE7QUNZSjs7QURWQTtFQUNFLG1CQUFBO0FDYUY7O0FEVkE7RUFDRSxlQUFBO0VBQ0EsYUFBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7QUNhRjs7QURYQTtFQUtFLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLE9BQUE7RUFDQSxlQUFBO0VBQ0EsWUFBQTtFQUNBLFNBQUE7RUFDQSxtQkFBQTtFQUNBLDBCQUFBO0VBQ0Esb0JBQUE7QUNVRjs7QURSQTtFQUNFLGtCQUFBO0VBQ0UsU0FBQTtFQUNBLE9BQUE7RUFDQSxlQUFBO0VBQ0EsWUFBQTtFQUNBLFNBQUE7RUFDQSxtQkFBQTtFQUNBLDBCQUFBO0VBQ0Esb0JBQUE7QUNXSjs7QURUQTtFQUNFLGVBQUE7RUFBZ0IsbUJBQUE7QUNhbEI7O0FEWEE7RUFDRSxlQUFBO0VBQ0EsbUJBQUE7QUNjRjs7QURaQTtFQUNFLFdBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxXQUFBO0VBQ0EsVUFBQTtFQUNBLE9BQUE7RUFDQSw2QkFBQTtBQ2VGOztBRGJBO0VBQ0Usc0JBQUE7RUFDQSwwQkFBQTtBQ2dCRjs7QURmRTtFQUNFLHFDQUFBO0VBQ0Esc0JBQUE7RUFDQSx1QkFBQTtFQUNBLDBCQUFBO0VBQ0EsMkJBQUE7RUFDQSxrQkFBQTtBQ2lCSiIsImZpbGUiOiJzcmMvYXBwL3Byb2R1Y3R2aWV3L3Byb2R1Y3R2aWV3LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIlxuICBcbiAgLmZvcl9jYXJke1xuICAgIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDA7XG4gICAgYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDA7XG4gICAgYm9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6IDBweDtcbiAgICBib3JkZXItYm90dG9tLWxlZnQtcmFkaXVzOiAzMHB4O1xuICB9XG4gIC5mb3JfY2FyZGhlYWRlciB7XG4gICAgYmFja2dyb3VuZDogLW1vei1saW5lYXItZ3JhZGllbnQoLTQ1ZGVnLCNlMmYwY2IgOCUsICNlMmYwY2IgODMlKTtcbiAgICBiYWNrZ3JvdW5kOiAtd2Via2l0LWxpbmVhci1ncmFkaWVudCgtNDVkZWcsI2UyZjBjYiA4JSwgI2UyZjBjYiA4MyUpO1xuICAgIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCgxMzVkZWcsICNlMmYwY2IgOCUsICNlMmYwY2IgODMlKTtcbiAgICBwYWRkaW5nLXRvcDogMzBweDtcbiAgfVxuICAuZm9yX2VkaXRpbWFnZXtcbiAgICAtLWJhY2tncm91bmQ6IHdoaXRlO1xuICAgIG1hcmdpbi10b3A6IC0yNXB4O1xuICAgIG1hcmdpbi1yaWdodDogMDtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjZTJmMGNiO1xuICB9XG4gICNmb3JfYnRue1xuICAgIC0tcGFkZGluZy1zdGFydDogMDtcbiAgICAtLXBhZGRpbmctZW5kOiAwO1xuICB9XG4gIC5mb3JfcHJ7XG4gICAgbWFyZ2luLWJvdHRvbTogLTJweDtmb250LXNpemU6IDM1cHg7XG4gIH1cbiAgLmZvcl9zb2NpYWx7XG4gICAgem9vbTogMS41O1xuICAgIGJhY2tncm91bmQ6ICM3NmM5NjE7XG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcbiAgICBwYWRkaW5nOiA0cHg7XG4gIH1cbiAgLmZvcl9pdGVtaW5mb3tcbiAgICAtLXBhZGRpbmctc3RhcnQ6MDtmb250LXNpemU6IDE0cHg7XG4gICAgLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgfVxuICAuZm9yX2l0ZW1pbmZvcmVzdHtcbiAgICAtLXBhZGRpbmctc3RhcnQ6MDtmb250LXNpemU6IDE0cHg7XG4gICAgbWFyZ2luLXRvcDogLTEwcHg7XG4gICAgLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgfVxuICAuZm9yX2xhYmVse1xuICAgIG1hcmdpbi1ib3R0b206IC0ycHg7XG4gIH1cbiAgLmZvcl90ZXh0e1xuICAgIG1hcmdpbi1ib3R0b206IC0ycHg7dGV4dC1hbGlnbjogcmlnaHQ7XG4gICAgbWFyZ2luLXJpZ2h0OiAwO1xuICB9XG4uZm9yX3BpbntcbiAgbWFyZ2luLWJvdHRvbTogLTNweDtcbn1cblxuLmZvcl90eHRvdmVydmlld3tcbiAgZm9udC1zaXplOiAxNHB4O1xuICBtYXJnaW4tdG9wOiAwO1xuICBjb2xvcjogIzk4OWFhMjtcbiAgcGFkZGluZy1yaWdodDogMTJweDtcbiAgcGFkZGluZy1sZWZ0OiAwO1xufVxuLmZvcl9iYWNre1xuICAvLyBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIC8vIGxlZnQ6IDIlO1xuICAvLyB0b3A6IDUlO1xuICAvLyBmb250LXNpemU6IDIwcHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogNSU7XG4gIHRvcDogNSU7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgY29sb3I6IGJsYWNrO1xuICBtYXJnaW46IDA7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xuICBmb250LWZhbWlseTogaW5oZXJpdDtcbn1cbi5mb3JfcmVwb3J0e1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgcmlnaHQ6IDUlO1xuICAgIHRvcDogNSU7XG4gICAgZm9udC1zaXplOiAxNXB4O1xuICAgIGNvbG9yOiBibGFjaztcbiAgICBtYXJnaW46IDA7XG4gICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbiAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbiAgICBmb250LWZhbWlseTogaW5oZXJpdDtcbn1cbi5mb3Jfc3BhbnN0YXJ7XG4gIGZvbnQtc2l6ZTogMTRweDtmb250LXdlaWdodDogbm9ybWFsO1xufVxuLmZvcl9zdGFyaWNvbntcbiAgZm9udC1zaXplOiAxOHB4O1xuICBtYXJnaW4tYm90dG9tOiAtNHB4O1xufVxuI292ZXJsYXl7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDg1cHg7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICB6LWluZGV4OiAyMDtcbiAgYm90dG9tOiAwJTtcbiAgbGVmdDogMDtcbiAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICNlMmUyZTI7XG59XG5pb24tYXZhdGFyICB7ICAgICBcbiAgd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcbiAgbWF4LXdpZHRoOiAyNXB4ICFpbXBvcnRhbnQ7XG4gIGltZ3tcbiAgICBib3JkZXI6IDFweCBzb2xpZCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xuICAgIHdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XG4gICAgaGVpZ2h0OiAxMDAlICFpbXBvcnRhbnQ7XG4gICAgbWF4LXdpZHRoOiAyNXB4ICFpbXBvcnRhbnQ7XG4gICAgbWF4LWhlaWdodDogMjVweCAhaW1wb3J0YW50O1xuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgfVxufVxuICAiLCIuZm9yX2NhcmQge1xuICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiAwO1xuICBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogMDtcbiAgYm9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6IDBweDtcbiAgYm9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czogMzBweDtcbn1cblxuLmZvcl9jYXJkaGVhZGVyIHtcbiAgYmFja2dyb3VuZDogLW1vei1saW5lYXItZ3JhZGllbnQoLTQ1ZGVnLCAjZTJmMGNiIDglLCAjZTJmMGNiIDgzJSk7XG4gIGJhY2tncm91bmQ6IC13ZWJraXQtbGluZWFyLWdyYWRpZW50KC00NWRlZywgI2UyZjBjYiA4JSwgI2UyZjBjYiA4MyUpO1xuICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoMTM1ZGVnLCAjZTJmMGNiIDglLCAjZTJmMGNiIDgzJSk7XG4gIHBhZGRpbmctdG9wOiAzMHB4O1xufVxuXG4uZm9yX2VkaXRpbWFnZSB7XG4gIC0tYmFja2dyb3VuZDogd2hpdGU7XG4gIG1hcmdpbi10b3A6IC0yNXB4O1xuICBtYXJnaW4tcmlnaHQ6IDA7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNlMmYwY2I7XG59XG5cbiNmb3JfYnRuIHtcbiAgLS1wYWRkaW5nLXN0YXJ0OiAwO1xuICAtLXBhZGRpbmctZW5kOiAwO1xufVxuXG4uZm9yX3ByIHtcbiAgbWFyZ2luLWJvdHRvbTogLTJweDtcbiAgZm9udC1zaXplOiAzNXB4O1xufVxuXG4uZm9yX3NvY2lhbCB7XG4gIHpvb206IDEuNTtcbiAgYmFja2dyb3VuZDogIzc2Yzk2MTtcbiAgYm9yZGVyLXJhZGl1czogMjVweDtcbiAgcGFkZGluZzogNHB4O1xufVxuXG4uZm9yX2l0ZW1pbmZvIHtcbiAgLS1wYWRkaW5nLXN0YXJ0OjA7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbn1cblxuLmZvcl9pdGVtaW5mb3Jlc3Qge1xuICAtLXBhZGRpbmctc3RhcnQ6MDtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBtYXJnaW4tdG9wOiAtMTBweDtcbiAgLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbn1cblxuLmZvcl9sYWJlbCB7XG4gIG1hcmdpbi1ib3R0b206IC0ycHg7XG59XG5cbi5mb3JfdGV4dCB7XG4gIG1hcmdpbi1ib3R0b206IC0ycHg7XG4gIHRleHQtYWxpZ246IHJpZ2h0O1xuICBtYXJnaW4tcmlnaHQ6IDA7XG59XG5cbi5mb3JfcGluIHtcbiAgbWFyZ2luLWJvdHRvbTogLTNweDtcbn1cblxuLmZvcl90eHRvdmVydmlldyB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgbWFyZ2luLXRvcDogMDtcbiAgY29sb3I6ICM5ODlhYTI7XG4gIHBhZGRpbmctcmlnaHQ6IDEycHg7XG4gIHBhZGRpbmctbGVmdDogMDtcbn1cblxuLmZvcl9iYWNrIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiA1JTtcbiAgdG9wOiA1JTtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBjb2xvcjogYmxhY2s7XG4gIG1hcmdpbjogMDtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbiAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG4gIGZvbnQtZmFtaWx5OiBpbmhlcml0O1xufVxuXG4uZm9yX3JlcG9ydCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgcmlnaHQ6IDUlO1xuICB0b3A6IDUlO1xuICBmb250LXNpemU6IDE1cHg7XG4gIGNvbG9yOiBibGFjaztcbiAgbWFyZ2luOiAwO1xuICBmb250LXdlaWdodDogbm9ybWFsO1xuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbiAgZm9udC1mYW1pbHk6IGluaGVyaXQ7XG59XG5cbi5mb3Jfc3BhbnN0YXIge1xuICBmb250LXNpemU6IDE0cHg7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG59XG5cbi5mb3Jfc3Rhcmljb24ge1xuICBmb250LXNpemU6IDE4cHg7XG4gIG1hcmdpbi1ib3R0b206IC00cHg7XG59XG5cbiNvdmVybGF5IHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogODVweDtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIHotaW5kZXg6IDIwO1xuICBib3R0b206IDAlO1xuICBsZWZ0OiAwO1xuICBib3JkZXItdG9wOiAxcHggc29saWQgI2UyZTJlMjtcbn1cblxuaW9uLWF2YXRhciB7XG4gIHdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XG4gIG1heC13aWR0aDogMjVweCAhaW1wb3J0YW50O1xufVxuaW9uLWF2YXRhciBpbWcge1xuICBib3JkZXI6IDFweCBzb2xpZCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xuICB3aWR0aDogMTAwJSAhaW1wb3J0YW50O1xuICBoZWlnaHQ6IDEwMCUgIWltcG9ydGFudDtcbiAgbWF4LXdpZHRoOiAyNXB4ICFpbXBvcnRhbnQ7XG4gIG1heC1oZWlnaHQ6IDI1cHggIWltcG9ydGFudDtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/productview/productview.page.ts":
  /*!*************************************************!*\
    !*** ./src/app/productview/productview.page.ts ***!
    \*************************************************/

  /*! exports provided: ProductviewPage */

  /***/
  function srcAppProductviewProductviewPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ProductviewPage", function () {
      return ProductviewPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _report_report_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../report/report.page */
    "./src/app/report/report.page.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _shared_model_item_model__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../shared/model/item.model */
    "./src/app/shared/model/item.model.ts");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var _providers_credential_provider__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../../providers/credential-provider */
    "./src/providers/credential-provider.ts");
    /* harmony import */


    var _purchaseproduct_purchaseproduct_page__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ../purchaseproduct/purchaseproduct.page */
    "./src/app/purchaseproduct/purchaseproduct.page.ts");

    var ProductviewPage = /*#__PURE__*/function () {
      function ProductviewPage(router, postPvdr, toastController, modalController, alertCtrl, navCtrl, route, storage) {
        var _this = this;

        _classCallCheck(this, ProductviewPage);

        // this.ImageArray = [
        //   {'image':'assets/greenthumb-images/bunny2.png',
        //     'name': 'Lorem ipsum dolor 1',
        //     'text': 'the'},
        //   {'image':'assets/greenthumb-images/bunny2.png',
        //   'name': 'Lorem ipsum dolor 2',
        //   'text': 'the'},
        //   {'image':'assets/greenthumb-images/bunny2.png',
        //   'name': 'Lorem ipsum dolor 3',
        //   'text': 'the'}
        //]
        this.router = router;
        this.postPvdr = postPvdr;
        this.toastController = toastController;
        this.modalController = modalController;
        this.alertCtrl = alertCtrl;
        this.navCtrl = navCtrl;
        this.route = route;
        this.storage = storage;
        this.item_id = ""; //itemList: Item[] = [];

        this.login_user_id = "";
        this.statusSave = false;
        this.ImageArray = [];
        this.item_name = "";
        this.item_category = "";
        this.item_price = "";
        this.item_seller = "";
        this.item_user_id = "";
        this.item_saveStatus = "";
        this.item_description = "";
        this.item_location = "";
        this.pending_purchase = false;
        this.ownProfile = false;
        this.slideOptsOne = {
          initialSlide: 0,
          slidesPerView: 1,
          autoplay: true
        };
        this.route.queryParams.subscribe(function (params) {
          _this.item_id = params["item_id"];
          console.log("params in productview:" + JSON.stringify(params));
        });
      } // goReport(){
      //    this.router.navigate((['report']));
      // }


      _createClass(ProductviewPage, [{
        key: "goHelp",
        value: function goHelp() {
          this.router.navigate(['tabs/help']);
        }
      }, {
        key: "goReport",
        value: function goReport() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
            var modal;
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    _context.next = 2;
                    return this.modalController.create({
                      component: _report_report_page__WEBPACK_IMPORTED_MODULE_3__["ReportPage"],
                      cssClass: 'liveprofilemodalstyle',
                      componentProps: {
                        reportedId: this.item_id,
                        reportedName: this.item_name
                      }
                    });

                  case 2:
                    modal = _context.sent;
                    modal.onDidDismiss().then(function (data) {
                      //const user = data['data']; // Here's your selected user!
                      console.log("dismiss of report item modal"); //this.plotFollowStatus();
                    });
                    _context.next = 6;
                    return modal.present();

                  case 6:
                    return _context.abrupt("return", _context.sent);

                  case 7:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        }
      }, {
        key: "goBack",
        value: function goBack() {
          window.history.back();
        }
      }, {
        key: "goMessage",
        value: function goMessage() {
          this.navCtrl.navigateRoot(['/message/' + this.item_user_id + "-separator-" + this.item_id + "-separator-" + '0']);
        }
      }, {
        key: "goSellerprof",
        value: function goSellerprof() {
          // this.router.navigate((['viewitemsellerprofile']));
          var navigationExtras = {
            queryParams: {
              user_profile_id: this.item_user_id
            }
          };
          this.navCtrl.navigateForward(['viewitemsellerprofile'], navigationExtras);
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "ionViewWillEnter",
        value: function ionViewWillEnter() {
          this.plotData();
        }
      }, {
        key: "plotData",
        value: function plotData() {
          var _this2 = this;

          this.storage.get('greenthumb_user_id').then(function (user_id) {
            _this2.login_user_id = user_id;
            var body = {
              action: 'viewPostItem',
              item_id: _this2.item_id,
              user_id: user_id
            };
            console.log(JSON.stringify(body));

            _this2.postPvdr.postData(body, 'post_item.php').subscribe(function (data) {
              return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
                var items, key;
                return regeneratorRuntime.wrap(function _callee2$(_context2) {
                  while (1) {
                    switch (_context2.prev = _context2.next) {
                      case 0:
                        if (data.success) {
                          items = []; //var pictureProfile: string = '';

                          for (key in data.result) {
                            // items.push(new Item(
                            //   data.result[key].item_id,
                            //   data.result[key].user_id,
                            //   data.result[key].username,
                            //   data.result[key].price,
                            //   data.result[key].title,
                            //   data.result[key].category,
                            //   data.result[key].location,
                            //   data.result[key].saveStatus,
                            //   data.result[key].date,
                            //   data.result[key].time
                            //   )
                            // );
                            this.item_name = data.result[key].title;
                            this.item_category = data.result[key].category;
                            this.item_seller = data.result[key].username;
                            this.item_user_id = data.result[key].user_id;
                            this.item_price = data.result[key].price;
                            this.item_location = data.result[key].location + ' ' + data.result[key].city;
                            this.item_saveStatus = data.result[key].saveStatus;
                            this.item_description = data.result[key].description;
                            this.item_quantity = data.result[key].quantity;
                            this.item_stock_id = data.result[key].stocks_id;
                            this.item_other_stock = data.result[key].others_stock;
                            this.picture = data.result[key].profile_photo;
                            this.complete_pic = this.postPvdr.myServer() + "/greenthumb/images/" + this.picture;
                            this.user_rating = data.result[key].user_rating;
                          }

                          this.ownProfile = this.login_user_id == this.item_user_id ? true : false;
                        }

                      case 1:
                      case "end":
                        return _context2.stop();
                    }
                  }
                }, _callee2, this);
              }));
            });

            var body3 = {
              action: 'SaveStatus',
              item_id: _this2.item_id,
              user_id: user_id
            };
            console.log("body3:" + JSON.stringify(body3));

            _this2.postPvdr.postData(body3, 'save_item.php').subscribe(function (data) {
              return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
                return regeneratorRuntime.wrap(function _callee3$(_context3) {
                  while (1) {
                    switch (_context3.prev = _context3.next) {
                      case 0:
                        if (data.success) {
                          if (data.result == "1") {
                            this.statusSave = true;
                          }
                        }

                        console.log("statusSave:" + this.statusSave);
                        console.log("wr:" + data.result);

                      case 3:
                      case "end":
                        return _context3.stop();
                    }
                  }
                }, _callee3, this);
              }));
            });

            var body4 = {
              action: 'getItemPhotos',
              item_id: _this2.item_id
            };
            console.log(JSON.stringify(body4));

            _this2.postPvdr.postData(body4, 'post_item.php').subscribe(function (data) {
              return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
                var itemsImage, key;
                return regeneratorRuntime.wrap(function _callee4$(_context4) {
                  while (1) {
                    switch (_context4.prev = _context4.next) {
                      case 0:
                        if (data.success) {
                          itemsImage = []; //var pictureProfile: string = '';

                          for (key in data.result) {
                            this.ImageArray.push(new _shared_model_item_model__WEBPACK_IMPORTED_MODULE_5__["ItemPicture"](this.postPvdr.myServer() + "/greenthumb/images/posted_items/images/" + data.result[key].item_photo));
                          }
                        }

                      case 1:
                      case "end":
                        return _context4.stop();
                    }
                  }
                }, _callee4, this);
              }));
            });

            var bodyqwer = {
              action: 'checkPurchasePending',
              item_id: _this2.item_id,
              user_id: _this2.login_user_id,
              item_user_id: _this2.item_user_id
            };

            _this2.postPvdr.postData(bodyqwer, 'save_item.php').subscribe(function (data) {
              return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
                return regeneratorRuntime.wrap(function _callee5$(_context5) {
                  while (1) {
                    switch (_context5.prev = _context5.next) {
                      case 0:
                        if (data.success) {
                          if (data.pending_purchase) {
                            this.pending_purchase = true;
                          }
                        }

                      case 1:
                      case "end":
                        return _context5.stop();
                    }
                  }
                }, _callee5, this);
              }));
            });
          });
        }
      }, {
        key: "cancelPurchase",
        value: function cancelPurchase() {
          var _this3 = this;

          this.alertCtrl.create({
            header: "Greenthumb Trade",
            message: "Are you sure to cancel the purchase of this item?",
            buttons: [{
              text: "No",
              handler: function handler() {
                console.log("Disagree clicked");
              }
            }, {
              text: "Yes",
              handler: function handler() {
                console.log("Agree clicked");

                _this3.agreeCancel();
              }
            }]
          }).then(function (res) {
            res.present();
          });
        }
      }, {
        key: "agreeCancel",
        value: function agreeCancel() {
          var _this4 = this;

          var body = {
            action: 'cancelPurchase',
            item_id: this.item_id,
            user_id: this.login_user_id,
            item_user_id: this.item_user_id
          };
          this.postPvdr.postData(body, 'save_item.php').subscribe(function (data) {
            if (data.success) {
              _this4.toastSuccessDelete('Purchase Successfully Canceled');

              _this4.pending_purchase = false;
            }
          });
        }
      }, {
        key: "saveItem",
        value: function saveItem() {
          var _this5 = this;

          var body = {
            action: 'saveItem',
            item_id: this.item_id,
            user_id: this.login_user_id
          };
          this.postPvdr.postData(body, 'save_item.php').subscribe(function (data) {
            if (data.success) {
              _this5.statusSave = !_this5.statusSave;
            }
          });
        }
      }, {
        key: "purchaseProduct",
        value: function purchaseProduct() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee6() {
            var modal;
            return regeneratorRuntime.wrap(function _callee6$(_context6) {
              while (1) {
                switch (_context6.prev = _context6.next) {
                  case 0:
                    _context6.next = 2;
                    return this.modalController.create({
                      component: _purchaseproduct_purchaseproduct_page__WEBPACK_IMPORTED_MODULE_8__["PurchaseproductPage"],
                      cssClass: 'purchaseProduct',
                      componentProps: {
                        item_id: this.item_id,
                        item_user_id: this.item_user_id,
                        login_user_id: this.login_user_id,
                        item_name: this.item_name
                      }
                    });

                  case 2:
                    modal = _context6.sent;
                    modal.onDidDismiss().then(function (data) {// this.category_id = data['data'].id; // Here's your selected category!
                      // this.category_name = data['data'].value;
                      // console.log("category_name:"+this.category_name);
                      // console.log("data:"+data);
                      // this.navCtrl.navigateRoot(['/message/'+this.login_user_id+"-separator-"+
                      //                                                   this.item_id]);
                    });
                    _context6.next = 6;
                    return modal.present();

                  case 6:
                    return _context6.abrupt("return", _context6.sent);

                  case 7:
                  case "end":
                    return _context6.stop();
                }
              }
            }, _callee6, this);
          }));
        }
      }, {
        key: "presentToast",
        value: function presentToast() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee7() {
            var toast;
            return regeneratorRuntime.wrap(function _callee7$(_context7) {
              while (1) {
                switch (_context7.prev = _context7.next) {
                  case 0:
                    _context7.next = 2;
                    return this.toastController.create({
                      message: 'You have a pending purchase with these item.',
                      duration: 3000
                    });

                  case 2:
                    toast = _context7.sent;
                    toast.present();

                  case 4:
                  case "end":
                    return _context7.stop();
                }
              }
            }, _callee7, this);
          }));
        }
      }, {
        key: "goOffers",
        value: function goOffers() {
          this.router.navigate(['tabs/offers']);
        }
      }, {
        key: "deleteItem",
        value: function deleteItem(item_id) {
          var _this6 = this;

          this.alertCtrl.create({
            header: "Are you sure to delete this item?",
            message: "",
            buttons: [{
              text: "No",
              handler: function handler() {
                console.log("Disagree clicked");
              }
            }, {
              text: "Yes",
              handler: function handler() {
                console.log("Agree clicked");

                _this6.confirmDelete(item_id);
              }
            }]
          }).then(function (res) {
            res.present();
          });
        }
      }, {
        key: "confirmDelete",
        value: function confirmDelete(item_id) {
          var _this7 = this;

          var body = {
            action: 'deleteItem',
            item_id: item_id
          };
          this.postPvdr.postData(body, 'save_item.php').subscribe(function (data) {
            if (data.success) {
              _this7.toastSuccessDelete('Item Successfully Deleted');

              _this7.navCtrl.navigateRoot(['tabs/tab1']);
            }
          });
        }
      }, {
        key: "toastSuccessDelete",
        value: function toastSuccessDelete(myMess) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee8() {
            var toast;
            return regeneratorRuntime.wrap(function _callee8$(_context8) {
              while (1) {
                switch (_context8.prev = _context8.next) {
                  case 0:
                    _context8.next = 2;
                    return this.toastController.create({
                      message: myMess,
                      duration: 3000
                    });

                  case 2:
                    toast = _context8.sent;
                    toast.present();

                  case 4:
                  case "end":
                    return _context8.stop();
                }
              }
            }, _callee8, this);
          }));
        }
      }, {
        key: "goEdit",
        value: function goEdit(item_id) {
          var navigationExtras = {
            queryParams: {
              item_id: item_id
            }
          };
          this.navCtrl.navigateRoot(['edititem'], navigationExtras);
        }
      }]);

      return ProductviewPage;
    }();

    ProductviewPage.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }, {
        type: _providers_credential_provider__WEBPACK_IMPORTED_MODULE_7__["PostProvider"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
      }, {
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_6__["Storage"]
      }];
    };

    ProductviewPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-productview',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./productview.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/productview/productview.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./productview.page.scss */
      "./src/app/productview/productview.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _providers_credential_provider__WEBPACK_IMPORTED_MODULE_7__["PostProvider"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _ionic_storage__WEBPACK_IMPORTED_MODULE_6__["Storage"]])], ProductviewPage);
    /***/
  }
}]);
//# sourceMappingURL=productview-productview-module-es5.js.map