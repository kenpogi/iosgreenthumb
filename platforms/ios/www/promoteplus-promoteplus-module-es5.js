function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["promoteplus-promoteplus-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/promoteplus/promoteplus.page.html":
  /*!*****************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/promoteplus/promoteplus.page.html ***!
    \*****************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPromoteplusPromoteplusPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header mode=\"ios\" no-border>\n  <ion-toolbar mode=\"ios\">\n      <ion-buttons slot=\"secondary\" (click)=\"goBack()\">\n        <ion-button mode=\"ios\" style=\"font-size: 15px;\">\n            Back\n        </ion-button>\n      </ion-buttons>\n      <ion-title color=\"secondary\">Promote Plus</ion-title>\n    </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-row justify-content-center align-items-center style='height: 100%'>\n    <ion-col  text-center>\n      <img src=\"assets/greenthumb-images/promoteplus.jpg\" class=\"for_img\">\n      <ion-label color=\"secondary\"><h2 class=\"for_title\">Get an average of 14x more views each day</h2></ion-label>\n      <p style=\"margin-bottom: 0;\">$19.99/mo after 3 day trial period</p>\n      <p style=\"margin-top: 0;\">No obligation, cancel anytime</p>\n      <ion-label color=\"secondary\"><h2 class=\"for_learnmore\">Learn More</h2></ion-label>\n      <ion-row padding>\n        <ion-col text-center>\n          <ion-button expand=\"block\" mode=\"ios\">START FREE TRIAL</ion-button>\n        </ion-col>\n      </ion-row>\n      <p class=\"for_info\">\n        Your Google Play account will be charged $19.99 per month for \n        the Promote Plus Subscription at the end of your 3-day free \n        trial unless you cancel the subscription before the end of\n         the trial period. If you do not have a free trial period, \n         your Google Play account will be charged when you complete \n         your purchase. Your subscription will automatically renew \n         each month unless you cancel the subscription in your \n         Google Play account before the end of the current billing \n         period.\n      </p>\n      <p class=\"for_info2\">\n        By purchasing the Promote Plus Subscription, you agree to \n        <a color=\"#679733\">Green Thumb's Terms</a> and\n        <a color=\"#679733\">Privacy Policy</a>\n      </p>\n    </ion-col>\n  </ion-row>\n</ion-content>\n";
    /***/
  },

  /***/
  "./src/app/promoteplus/promoteplus-routing.module.ts":
  /*!***********************************************************!*\
    !*** ./src/app/promoteplus/promoteplus-routing.module.ts ***!
    \***********************************************************/

  /*! exports provided: PromoteplusPageRoutingModule */

  /***/
  function srcAppPromoteplusPromoteplusRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PromoteplusPageRoutingModule", function () {
      return PromoteplusPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _promoteplus_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./promoteplus.page */
    "./src/app/promoteplus/promoteplus.page.ts");

    var routes = [{
      path: '',
      component: _promoteplus_page__WEBPACK_IMPORTED_MODULE_3__["PromoteplusPage"]
    }];

    var PromoteplusPageRoutingModule = function PromoteplusPageRoutingModule() {
      _classCallCheck(this, PromoteplusPageRoutingModule);
    };

    PromoteplusPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], PromoteplusPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/promoteplus/promoteplus.module.ts":
  /*!***************************************************!*\
    !*** ./src/app/promoteplus/promoteplus.module.ts ***!
    \***************************************************/

  /*! exports provided: PromoteplusPageModule */

  /***/
  function srcAppPromoteplusPromoteplusModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PromoteplusPageModule", function () {
      return PromoteplusPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _promoteplus_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./promoteplus-routing.module */
    "./src/app/promoteplus/promoteplus-routing.module.ts");
    /* harmony import */


    var _promoteplus_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./promoteplus.page */
    "./src/app/promoteplus/promoteplus.page.ts");

    var PromoteplusPageModule = function PromoteplusPageModule() {
      _classCallCheck(this, PromoteplusPageModule);
    };

    PromoteplusPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _promoteplus_routing_module__WEBPACK_IMPORTED_MODULE_5__["PromoteplusPageRoutingModule"]],
      declarations: [_promoteplus_page__WEBPACK_IMPORTED_MODULE_6__["PromoteplusPage"]]
    })], PromoteplusPageModule);
    /***/
  },

  /***/
  "./src/app/promoteplus/promoteplus.page.scss":
  /*!***************************************************!*\
    !*** ./src/app/promoteplus/promoteplus.page.scss ***!
    \***************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPromoteplusPromoteplusPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".for_title {\n  font-size: 17px;\n  font-weight: bold;\n}\n\n.for_img {\n  width: 30%;\n  padding-bottom: 10px;\n}\n\n.for_learnmore {\n  font-size: 15px;\n  font-weight: bold;\n}\n\n.for_info {\n  padding-left: 20px;\n  padding-right: 20px;\n  font-size: 13px;\n  margin: 0;\n}\n\n.for_info2 {\n  font-size: 14px;\n  margin-top: 20px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZW5kcmljay9EZXNrdG9wL1RlbG1vIFNvbHV0aW9ucy9Gb3JQcm9kdWN0aW9uL21heTEwMjAyMS9sYXN0IHNldHVwL2dyZWVudGh1bWJhcHAgY29weS9zcmMvYXBwL3Byb21vdGVwbHVzL3Byb21vdGVwbHVzLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcHJvbW90ZXBsdXMvcHJvbW90ZXBsdXMucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0csZUFBQTtFQUNBLGlCQUFBO0FDQ0g7O0FEQ0E7RUFDSSxVQUFBO0VBQ0Esb0JBQUE7QUNFSjs7QURBQTtFQUNJLGVBQUE7RUFDQSxpQkFBQTtBQ0dKOztBRERBO0VBQ0ksa0JBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7RUFDQSxTQUFBO0FDSUo7O0FERkE7RUFDSSxlQUFBO0VBQ0EsZ0JBQUE7QUNLSiIsImZpbGUiOiJzcmMvYXBwL3Byb21vdGVwbHVzL3Byb21vdGVwbHVzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5mb3JfdGl0bGV7XG4gICBmb250LXNpemU6IDE3cHg7XG4gICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5mb3JfaW1ne1xuICAgIHdpZHRoOiAzMCU7XG4gICAgcGFkZGluZy1ib3R0b206IDEwcHg7XG59XG4uZm9yX2xlYXJubW9yZXtcbiAgICBmb250LXNpemU6IDE1cHg7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG4uZm9yX2luZm97XG4gICAgcGFkZGluZy1sZWZ0OiAyMHB4O1xuICAgIHBhZGRpbmctcmlnaHQ6IDIwcHg7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIG1hcmdpbjogMDtcbn1cbi5mb3JfaW5mbzJ7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgIG1hcmdpbi10b3A6IDIwcHg7XG59IiwiLmZvcl90aXRsZSB7XG4gIGZvbnQtc2l6ZTogMTdweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG5cbi5mb3JfaW1nIHtcbiAgd2lkdGg6IDMwJTtcbiAgcGFkZGluZy1ib3R0b206IDEwcHg7XG59XG5cbi5mb3JfbGVhcm5tb3JlIHtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cblxuLmZvcl9pbmZvIHtcbiAgcGFkZGluZy1sZWZ0OiAyMHB4O1xuICBwYWRkaW5nLXJpZ2h0OiAyMHB4O1xuICBmb250LXNpemU6IDEzcHg7XG4gIG1hcmdpbjogMDtcbn1cblxuLmZvcl9pbmZvMiB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgbWFyZ2luLXRvcDogMjBweDtcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/promoteplus/promoteplus.page.ts":
  /*!*************************************************!*\
    !*** ./src/app/promoteplus/promoteplus.page.ts ***!
    \*************************************************/

  /*! exports provided: PromoteplusPage */

  /***/
  function srcAppPromoteplusPromoteplusPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PromoteplusPage", function () {
      return PromoteplusPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");

    var PromoteplusPage = /*#__PURE__*/function () {
      function PromoteplusPage(router) {
        _classCallCheck(this, PromoteplusPage);

        this.router = router;
      }

      _createClass(PromoteplusPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "goBack",
        value: function goBack() {
          window.history.back();
        }
      }]);

      return PromoteplusPage;
    }();

    PromoteplusPage.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }];
    };

    PromoteplusPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-promoteplus',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./promoteplus.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/promoteplus/promoteplus.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./promoteplus.page.scss */
      "./src/app/promoteplus/promoteplus.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])], PromoteplusPage);
    /***/
  }
}]);
//# sourceMappingURL=promoteplus-promoteplus-module-es5.js.map