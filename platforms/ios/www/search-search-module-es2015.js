(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["search-search-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/search/search.page.html":
/*!*******************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/search/search.page.html ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header no-border>\n  <ion-toolbar mode=\"ios\">\n      <ion-buttons slot=\"secondary\" (click)=\"goBack()\">\n        <ion-button mode=\"ios\">\n            <ion-icon name=\"arrow-back\"  mode=\"ios\"></ion-icon>\n        </ion-button>\n      </ion-buttons>\n\n      <ion-searchbar clearInput placeholder=\"Search Profile\" mode=\"ios\" [(ngModel)]=\"search\"></ion-searchbar>\n\n        <ion-button style=\"margin-top: 1.5%;\" (click)=\"searchProfile()\" size=\"small\" slot=\"primary\" mode=\"ios\" >\n            SEARCH\n        </ion-button>\n    </ion-toolbar>\n</ion-header>\n\n<ion-content mode=\"ios\">\n  <ion-grid>\n    <ion-row>\n      <ion-col>\n        <ion-list mode=\"ios\">\n          <ion-item id=\"for_thumbnail\" mode=\"ios\" (click)=\"openliveprofile(user?.id)\" *ngFor=\"let user of userProfiles\">\n            <ion-thumbnail slot=\"start\" mode=\"ios\">\n              <div *ngIf=\"!user?.picture\">\n                <img src=\"assets/icon/brixylogo.png\" mode=\"ios\">\n              </div>\n              <div *ngIf=\"user?.picture\">\n                <img [src] = \"user?.picture\" mode=\"ios\">\n              </div> \n            </ion-thumbnail>\n            <ion-label mode=\"ios\">\n              <h2 style=\"font-size: 18px;\" mode=\"ios\">{{user?.fname+\" \"+user?.lname}}</h2>\n              <h3 style=\"font-size: 16px;\" mode=\"ios\"><img src=\"assets/icon/location.png\" class=\"colored_pin\">  {{user?.city+\", \"+user?.country}}</h3>\n              <h6 mode=\"ios\" style=\"font-size: 15px;font-weight: lighter;padding-top: 2%;\"> \n                <img src=\"assets/icon/knight2.png\" class=\"badge_knight2\">&nbsp;{{user?.badge_name}}&nbsp;&nbsp;&nbsp;&nbsp;\n                <img src=\"assets/icon/medal.png\" class=\"badge_knight2\">&nbsp;{{user?.user_level}}&nbsp;&nbsp;&nbsp;&nbsp;\n                <img src=\"assets/icon/follow.png\" class=\"badge_knight2\">&nbsp;{{user?.follower}}</h6>\n            </ion-label>\n          </ion-item>\n        </ion-list>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n  \n</ion-content>\n");

/***/ }),

/***/ "./src/app/search/search-routing.module.ts":
/*!*************************************************!*\
  !*** ./src/app/search/search-routing.module.ts ***!
  \*************************************************/
/*! exports provided: SearchPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchPageRoutingModule", function() { return SearchPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _search_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./search.page */ "./src/app/search/search.page.ts");




const routes = [
    {
        path: '',
        component: _search_page__WEBPACK_IMPORTED_MODULE_3__["SearchPage"]
    }
];
let SearchPageRoutingModule = class SearchPageRoutingModule {
};
SearchPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], SearchPageRoutingModule);



/***/ }),

/***/ "./src/app/search/search.module.ts":
/*!*****************************************!*\
  !*** ./src/app/search/search.module.ts ***!
  \*****************************************/
/*! exports provided: SearchPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchPageModule", function() { return SearchPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _search_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./search-routing.module */ "./src/app/search/search-routing.module.ts");
/* harmony import */ var _search_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./search.page */ "./src/app/search/search.page.ts");







let SearchPageModule = class SearchPageModule {
};
SearchPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _search_routing_module__WEBPACK_IMPORTED_MODULE_5__["SearchPageRoutingModule"]
        ],
        declarations: [_search_page__WEBPACK_IMPORTED_MODULE_6__["SearchPage"]]
    })
], SearchPageModule);



/***/ }),

/***/ "./src/app/search/search.page.scss":
/*!*****************************************!*\
  !*** ./src/app/search/search.page.scss ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".item-has-focus {\n  --highlight-background: #1dc1e6 !important;\n}\n\nion-item {\n  --padding-start: 0% !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZW5kcmljay9EZXNrdG9wL1RlbG1vIFNvbHV0aW9ucy9Gb3JQcm9kdWN0aW9uL21heTEwMjAyMS9sYXN0IHNldHVwL2dyZWVudGh1bWJhcHAgY29weS9zcmMvYXBwL3NlYXJjaC9zZWFyY2gucGFnZS5zY3NzIiwic3JjL2FwcC9zZWFyY2gvc2VhcmNoLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLDBDQUFBO0FDQ0o7O0FERUU7RUFDRSw4QkFBQTtBQ0NKIiwiZmlsZSI6InNyYy9hcHAvc2VhcmNoL3NlYXJjaC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaXRlbS1oYXMtZm9jdXN7XG4gICAgLS1oaWdobGlnaHQtYmFja2dyb3VuZDogIzFkYzFlNiAgIWltcG9ydGFudDtcbiAgfVxuXG4gIGlvbi1pdGVtIHtcbiAgICAtLXBhZGRpbmctc3RhcnQ6IDAlICFpbXBvcnRhbnQ7XG4gIH0iLCIuaXRlbS1oYXMtZm9jdXMge1xuICAtLWhpZ2hsaWdodC1iYWNrZ3JvdW5kOiAjMWRjMWU2ICFpbXBvcnRhbnQ7XG59XG5cbmlvbi1pdGVtIHtcbiAgLS1wYWRkaW5nLXN0YXJ0OiAwJSAhaW1wb3J0YW50O1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/search/search.page.ts":
/*!***************************************!*\
  !*** ./src/app/search/search.page.ts ***!
  \***************************************/
/*! exports provided: SearchPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchPage", function() { return SearchPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _providers_credential_provider__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../providers/credential-provider */ "./src/providers/credential-provider.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _shared_model_follow_model__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../shared/model/follow.model */ "./src/app/shared/model/follow.model.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _liveprofile_liveprofile_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../liveprofile/liveprofile.page */ "./src/app/liveprofile/liveprofile.page.ts");








let SearchPage = class SearchPage {
    constructor(router, postPvdr, modalController, storage) {
        this.router = router;
        this.postPvdr = postPvdr;
        this.modalController = modalController;
        this.storage = storage;
        this.search = '';
    }
    ngOnInit() {
    }
    searchProfile() {
        let body = {
            action: 'searchProfile',
            search: this.search
        };
        console.log("searchbody:" + JSON.stringify(body));
        this.postPvdr.postData(body, 'followers.php').subscribe(data => {
            if (data.success) {
                const followers1 = [];
                var picture = '';
                console.log("result search Profile:" + JSON.stringify(data));
                for (const key in data.result) {
                    picture = (data.result[key].profile_photo == '') ? '' :
                        "http://" + this.postPvdr.myServer() + "/brixy-live/images/" + data.result[key].profile_photo;
                    followers1.push(new _shared_model_follow_model__WEBPACK_IMPORTED_MODULE_5__["Follow"](data.result[key].user_id, data.result[key].fname, data.result[key].lname, data.result[key].city, data.result[key].country, picture, (data.result[key].profile_photo == '') ? '' :
                        this.postPvdr.myServer() + "/greenthumb/images/" + data.result[key].profile_photo));
                }
                this.userProfiles = followers1;
            }
        });
    }
    goBack() {
        this.router.navigate(['tabs']);
    }
    openliveprofile(user_id) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _liveprofile_liveprofile_page__WEBPACK_IMPORTED_MODULE_7__["LiveprofilePage"],
                cssClass: 'liveprofilemodalstyle',
                componentProps: {
                    liveStreamProfileId: user_id
                }
            });
            return yield modal.present();
        });
    }
};
SearchPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _providers_credential_provider__WEBPACK_IMPORTED_MODULE_3__["PostProvider"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ModalController"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"] }
];
SearchPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-search',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./search.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/search/search.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./search.page.scss */ "./src/app/search/search.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
        _providers_credential_provider__WEBPACK_IMPORTED_MODULE_3__["PostProvider"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ModalController"],
        _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"]])
], SearchPage);



/***/ })

}]);
//# sourceMappingURL=search-search-module-es2015.js.map