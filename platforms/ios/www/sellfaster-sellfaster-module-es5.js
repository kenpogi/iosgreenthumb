function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["sellfaster-sellfaster-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/sellfaster/sellfaster.page.html":
  /*!***************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/sellfaster/sellfaster.page.html ***!
    \***************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppSellfasterSellfasterPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header mode=\"ios\" no-border>\n  <ion-toolbar mode=\"ios\">\n      <ion-buttons slot=\"secondary\" (click)=\"goBack()\">\n        <ion-button mode=\"ios\" style=\"font-size: 15px;\">\n            Back\n        </ion-button>\n      </ion-buttons>\n      <ion-title color=\"secondary\">Sell faster</ion-title>\n    </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div text-center>\n    <img *ngIf=\"!ImageArray\" src=\"assets/greenthumb-images/tomato.png\" class=\"for_imgfruit\"> \n\n        <ion-slides *ngIf=\"ImageArray\" [options]=\"slideOptsOne\" pager=\"true\" mode=\"ios\" id=\"slide_forpromo\">\n          <ion-slide *ngFor=\"let slide of ImageArray\" mode=\"ios\">\n            <div class=\"slide\" mode=\"ios\">\n              <ion-card mode=\"ios\" class=\"promocard\">\n                <ion-card-header mode=\"ios\" class=\"promoheader\">\n                  <ion-row>\n                  \n                    <ion-col no-padding text-right>\n                      <img src=\"{{slide.picture_filename}}\" class=\"for_imgfruit\">\n                    </ion-col>\n      \n                  </ion-row>\n                </ion-card-header>\n              </ion-card>\n            </div>\n          </ion-slide>\n        </ion-slides>\n  </div>\n  <ion-row padding style=\"padding-bottom: 0;\">\n    <!-- <ion-col size=\"4\" text-center>\n      <ion-card mode=\"ios\" class=\"btn_promote\">\n        <ion-card-header no-padding class=\"btn_header\">\n          <ion-card-subtitle class=\"btn_headertxt\">BEST VALUE</ion-card-subtitle>\n        </ion-card-header>\n        <ion-card-content class=\"btn_content\">\n          <ion-label color=\"secondary\"><p class=\"for_promotxt\">Promote Plus</p></ion-label>\n          <ion-button mode=\"ios\" size=\"small\" shape=\"round\" class=\"for_btnpromote\">FREE TRIAL</ion-button>\n        </ion-card-content>\n      </ion-card>\n      <ion-icon style=\"zoom: 1.5;\" mode=\"ios\" color=\"primary\" name=\"arrow-dropdown-circle\"></ion-icon>\n    </ion-col> -->\n    <ion-col *ngFor=\"let promotion of promotions\" size=\"4\" text-center>\n      <ion-card mode=\"ios\" (click)=\"promoteItem(promotion.id)\" class=\"btn_promoteinactive\">\n        <ion-card-content class=\"btn_contentinactive\">\n          <ion-label color=\"dark\">\n            <p class=\"for_promotxt\" *ngIf=\"promotion.id == 4\">Promote 7 days</p>\n            <p class=\"for_promotxt\" *ngIf=\"promotion.id == 3\">Promote 5 days</p>\n            <p class=\"for_promotxt\" *ngIf=\"promotion.id == 2\">Promote 1 day</p>\n          </ion-label>\n          <ion-label color=\"dark\"><p class=\"for_promodollar\">${{promotion.rate}}</p></ion-label>\n        </ion-card-content>\n      </ion-card>\n    </ion-col>\n    <!-- <ion-col size=\"4\" text-center>\n      <ion-card mode=\"ios\" class=\"btn_promoteinactive\">\n        <ion-card-content class=\"btn_contentinactive\">\n          <ion-label color=\"dark\"><p class=\"for_promotxt\">Promote 1 day</p></ion-label>\n          <ion-label color=\"dark\"><p class=\"for_promodollar\">$2.99</p></ion-label>\n        </ion-card-content>\n      </ion-card>\n    </ion-col> -->\n  </ion-row>\n  <ion-list mode=\"ios\">\n    <ion-item mode=\"ios\" lines=\"none\" style=\"height: 30px;\">\n      <ion-icon name=\"checkmark\" color=\"primary\" mode=\"ios\" slot=\"start\"></ion-icon>\n      <ion-label style=\"font-size: 13px;\">Get an average of 14x more views each day</ion-label>\n    </ion-item>\n    <ion-item mode=\"ios\" lines=\"none\" style=\"height: 30px;\">\n      <ion-icon name=\"checkmark\" color=\"primary\" mode=\"ios\" slot=\"start\"></ion-icon>\n      <ion-label style=\"font-size: 13px;\">Promote for multiple days</ion-label>\n    </ion-item>\n    <ion-item mode=\"ios\" lines=\"none\" style=\"height: 30px;\">\n      <ion-icon name=\"checkmark\" color=\"primary\" mode=\"ios\" slot=\"start\"></ion-icon>\n      <ion-label style=\"font-size: 13px;\">Switch promotion to other items</ion-label>\n    </ion-item>\n  </ion-list>\n  <ion-row>\n    <ion-col text-center>\n      <ion-label color=\"secondary\" style=\"font-size: 14px;\">How does promoting works? \n        <ion-icon class=\"for_helpicon\" mode=\"ios\" color=\"secondary\" name=\"help-circle\"></ion-icon></ion-label>\n    </ion-col>\n  </ion-row>\n  <ion-row padding>\n    <ion-col text-center>\n      <ion-button expand=\"block\" (click)=\"goBack()\" mode=\"ios\">\n        DONE\n      </ion-button>\n      <!-- <ion-button expand=\"block\" (click)=\"goPromoteplus()\" mode=\"ios\">\n        NEXT\n      </ion-button> -->\n    </ion-col>\n  </ion-row>\n \n  \n</ion-content>\n";
    /***/
  },

  /***/
  "./src/app/sellfaster/sellfaster-routing.module.ts":
  /*!*********************************************************!*\
    !*** ./src/app/sellfaster/sellfaster-routing.module.ts ***!
    \*********************************************************/

  /*! exports provided: SellfasterPageRoutingModule */

  /***/
  function srcAppSellfasterSellfasterRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SellfasterPageRoutingModule", function () {
      return SellfasterPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _sellfaster_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./sellfaster.page */
    "./src/app/sellfaster/sellfaster.page.ts");

    var routes = [{
      path: '',
      component: _sellfaster_page__WEBPACK_IMPORTED_MODULE_3__["SellfasterPage"]
    }];

    var SellfasterPageRoutingModule = function SellfasterPageRoutingModule() {
      _classCallCheck(this, SellfasterPageRoutingModule);
    };

    SellfasterPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], SellfasterPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/sellfaster/sellfaster.module.ts":
  /*!*************************************************!*\
    !*** ./src/app/sellfaster/sellfaster.module.ts ***!
    \*************************************************/

  /*! exports provided: SellfasterPageModule */

  /***/
  function srcAppSellfasterSellfasterModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SellfasterPageModule", function () {
      return SellfasterPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _sellfaster_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./sellfaster-routing.module */
    "./src/app/sellfaster/sellfaster-routing.module.ts");
    /* harmony import */


    var _sellfaster_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./sellfaster.page */
    "./src/app/sellfaster/sellfaster.page.ts");

    var SellfasterPageModule = function SellfasterPageModule() {
      _classCallCheck(this, SellfasterPageModule);
    };

    SellfasterPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _sellfaster_routing_module__WEBPACK_IMPORTED_MODULE_5__["SellfasterPageRoutingModule"]],
      declarations: [_sellfaster_page__WEBPACK_IMPORTED_MODULE_6__["SellfasterPage"]]
    })], SellfasterPageModule);
    /***/
  },

  /***/
  "./src/app/sellfaster/sellfaster.page.scss":
  /*!*************************************************!*\
    !*** ./src/app/sellfaster/sellfaster.page.scss ***!
    \*************************************************/

  /*! exports provided: default */

  /***/
  function srcAppSellfasterSellfasterPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".btn_promote {\n  height: 110px;\n  --background: transparent;\n  border: 1px solid #76c961;\n  border-radius: 5px;\n  box-shadow: none;\n  margin: 0;\n}\n\n.btn_promoteinactive {\n  height: 110px;\n  --background: transparent;\n  border: 1px solid #e2f0cb;\n  border-radius: 5px;\n  box-shadow: none;\n  margin: 0;\n}\n\n.btn_header {\n  background: #76c961;\n  height: 22px;\n}\n\n.btn_headertxt {\n  font-size: 13px;\n  color: white;\n  padding: 3px;\n}\n\n.for_promotxt {\n  font-size: 14px;\n  font-weight: bold;\n  padding: 0;\n  line-height: 1.2;\n}\n\n.for_btnpromote {\n  --background: #fc7000;\n  height: 20px;\n  width: 73px;\n  font-size: 9px;\n}\n\n.btn_content {\n  padding: 10px;\n}\n\n.btn_contentinactive {\n  padding: 15px;\n  margin-top: 10%;\n}\n\n.for_promodollar {\n  color: #679733 !important;\n  padding: 0;\n  margin-top: 5px;\n}\n\n.for_helpicon {\n  font-size: 20px;\n  margin-bottom: -5px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZW5kcmljay9EZXNrdG9wL1RlbG1vIFNvbHV0aW9ucy9Gb3JQcm9kdWN0aW9uL21heTEwMjAyMS9sYXN0IHNldHVwL2dyZWVudGh1bWJhcHAgY29weS9zcmMvYXBwL3NlbGxmYXN0ZXIvc2VsbGZhc3Rlci5wYWdlLnNjc3MiLCJzcmMvYXBwL3NlbGxmYXN0ZXIvc2VsbGZhc3Rlci5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxhQUFBO0VBQ0EseUJBQUE7RUFDQSx5QkFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxTQUFBO0FDQ0o7O0FEQ0E7RUFDSSxhQUFBO0VBQ0EseUJBQUE7RUFDQSx5QkFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxTQUFBO0FDRUo7O0FEQUE7RUFDSSxtQkFBQTtFQUNBLFlBQUE7QUNHSjs7QUREQTtFQUNJLGVBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtBQ0lKOztBREZBO0VBQ0ksZUFBQTtFQUNBLGlCQUFBO0VBQ0EsVUFBQTtFQUNBLGdCQUFBO0FDS0o7O0FESEE7RUFDSSxxQkFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsY0FBQTtBQ01KOztBREpBO0VBQ0ksYUFBQTtBQ09KOztBRExBO0VBQ0ksYUFBQTtFQUNBLGVBQUE7QUNRSjs7QUROQTtFQUNJLHlCQUFBO0VBQ0EsVUFBQTtFQUNBLGVBQUE7QUNTSjs7QURQQTtFQUNJLGVBQUE7RUFDQSxtQkFBQTtBQ1VKIiwiZmlsZSI6InNyYy9hcHAvc2VsbGZhc3Rlci9zZWxsZmFzdGVyLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5idG5fcHJvbW90ZXtcbiAgICBoZWlnaHQ6IDExMHB4O1xuICAgIC0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG4gICAgYm9yZGVyOiAxcHggc29saWQgIzc2Yzk2MTtcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgYm94LXNoYWRvdzogbm9uZTtcbiAgICBtYXJnaW46IDA7XG59XG4uYnRuX3Byb21vdGVpbmFjdGl2ZXtcbiAgICBoZWlnaHQ6IDExMHB4O1xuICAgIC0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG4gICAgYm9yZGVyOiAxcHggc29saWQgI2UyZjBjYjtcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgYm94LXNoYWRvdzogbm9uZTtcbiAgICBtYXJnaW46IDA7XG59XG4uYnRuX2hlYWRlcntcbiAgICBiYWNrZ3JvdW5kOiAjNzZjOTYxO1xuICAgIGhlaWdodDogMjJweDtcbn1cbi5idG5faGVhZGVydHh0e1xuICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgcGFkZGluZzogM3B4O1xufVxuLmZvcl9wcm9tb3R4dHtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgcGFkZGluZzogMDtcbiAgICBsaW5lLWhlaWdodDogMS4yO1xufVxuLmZvcl9idG5wcm9tb3Rle1xuICAgIC0tYmFja2dyb3VuZDogI2ZjNzAwMDtcbiAgICBoZWlnaHQ6IDIwcHg7XG4gICAgd2lkdGg6IDczcHg7XG4gICAgZm9udC1zaXplOiA5cHg7XG59XG4uYnRuX2NvbnRlbnR7XG4gICAgcGFkZGluZzogMTBweDtcbn1cbi5idG5fY29udGVudGluYWN0aXZle1xuICAgIHBhZGRpbmc6IDE1cHg7XG4gICAgbWFyZ2luLXRvcDogMTAlO1xufVxuLmZvcl9wcm9tb2RvbGxhcntcbiAgICBjb2xvcjogIzY3OTczMyAhaW1wb3J0YW50O1xuICAgIHBhZGRpbmc6IDA7XG4gICAgbWFyZ2luLXRvcDogNXB4O1xufVxuLmZvcl9oZWxwaWNvbntcbiAgICBmb250LXNpemU6IDIwcHg7XG4gICAgbWFyZ2luLWJvdHRvbTogLTVweDtcbn0iLCIuYnRuX3Byb21vdGUge1xuICBoZWlnaHQ6IDExMHB4O1xuICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICBib3JkZXI6IDFweCBzb2xpZCAjNzZjOTYxO1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIGJveC1zaGFkb3c6IG5vbmU7XG4gIG1hcmdpbjogMDtcbn1cblxuLmJ0bl9wcm9tb3RlaW5hY3RpdmUge1xuICBoZWlnaHQ6IDExMHB4O1xuICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICBib3JkZXI6IDFweCBzb2xpZCAjZTJmMGNiO1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIGJveC1zaGFkb3c6IG5vbmU7XG4gIG1hcmdpbjogMDtcbn1cblxuLmJ0bl9oZWFkZXIge1xuICBiYWNrZ3JvdW5kOiAjNzZjOTYxO1xuICBoZWlnaHQ6IDIycHg7XG59XG5cbi5idG5faGVhZGVydHh0IHtcbiAgZm9udC1zaXplOiAxM3B4O1xuICBjb2xvcjogd2hpdGU7XG4gIHBhZGRpbmc6IDNweDtcbn1cblxuLmZvcl9wcm9tb3R4dCB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIHBhZGRpbmc6IDA7XG4gIGxpbmUtaGVpZ2h0OiAxLjI7XG59XG5cbi5mb3JfYnRucHJvbW90ZSB7XG4gIC0tYmFja2dyb3VuZDogI2ZjNzAwMDtcbiAgaGVpZ2h0OiAyMHB4O1xuICB3aWR0aDogNzNweDtcbiAgZm9udC1zaXplOiA5cHg7XG59XG5cbi5idG5fY29udGVudCB7XG4gIHBhZGRpbmc6IDEwcHg7XG59XG5cbi5idG5fY29udGVudGluYWN0aXZlIHtcbiAgcGFkZGluZzogMTVweDtcbiAgbWFyZ2luLXRvcDogMTAlO1xufVxuXG4uZm9yX3Byb21vZG9sbGFyIHtcbiAgY29sb3I6ICM2Nzk3MzMgIWltcG9ydGFudDtcbiAgcGFkZGluZzogMDtcbiAgbWFyZ2luLXRvcDogNXB4O1xufVxuXG4uZm9yX2hlbHBpY29uIHtcbiAgZm9udC1zaXplOiAyMHB4O1xuICBtYXJnaW4tYm90dG9tOiAtNXB4O1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/sellfaster/sellfaster.page.ts":
  /*!***********************************************!*\
    !*** ./src/app/sellfaster/sellfaster.page.ts ***!
    \***********************************************/

  /*! exports provided: SellfasterPage */

  /***/
  function srcAppSellfasterSellfasterPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SellfasterPage", function () {
      return SellfasterPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _shared_model_item_model__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../shared/model/item.model */
    "./src/app/shared/model/item.model.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _providers_credential_provider__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../../providers/credential-provider */
    "./src/providers/credential-provider.ts");
    /* harmony import */


    var _shared_model_promotion_model__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../shared/model/promotion.model */
    "./src/app/shared/model/promotion.model.ts");

    var SellfasterPage = /*#__PURE__*/function () {
      function SellfasterPage(router, storage, toastController, postPvdr, route) {
        var _this = this;

        _classCallCheck(this, SellfasterPage);

        this.router = router;
        this.storage = storage;
        this.toastController = toastController;
        this.postPvdr = postPvdr;
        this.route = route;
        this.item_id = "";
        this.ImageArray = [];
        this.promotions = [];
        this.slideOptsOne = {
          initialSlide: 0,
          slidesPerView: 1,
          autoplay: true
        };
        this.route.queryParams.subscribe(function (params) {
          _this.item_id = params["item_id"];
          console.log("item_id sell faster:" + _this.item_id);
        });
      }

      _createClass(SellfasterPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.plotData();
        }
      }, {
        key: "plotData",
        value: function plotData() {
          var _this2 = this;

          var body4 = {
            action: 'getItemPhotos',
            item_id: this.item_id
          };
          console.log(JSON.stringify(body4));
          this.postPvdr.postData(body4, 'post_item.php').subscribe(function (data) {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var itemsImage, key;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      if (data.success) {
                        itemsImage = []; //var pictureProfile: string = '';

                        for (key in data.result) {
                          itemsImage.push(new _shared_model_item_model__WEBPACK_IMPORTED_MODULE_4__["ItemPicture"](this.postPvdr.myServer() + "/greenthumb/images/posted_items/images/" + data.result[key].item_photo));
                          console.log("item_photo:" + data.result[key].item_photo);
                        }

                        this.ImageArray = itemsImage;
                      }

                    case 1:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          });
          var body = {
            action: 'getPromotionRate'
          };
          this.postPvdr.postData(body, 'promotion.php').subscribe(function (data) {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var promotionRate, x, key;
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      if (data.success) {
                        promotionRate = []; //var pictureProfile: string = '';

                        x = 0;

                        for (key in data.result) {
                          promotionRate.push(new _shared_model_promotion_model__WEBPACK_IMPORTED_MODULE_7__["promotionModel"](data.result[key].id, data.result[key].rate));
                        }

                        this.promotions = promotionRate;
                      }

                    case 1:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          });
        }
      }, {
        key: "promoteItem",
        value: function promoteItem(promote_id, promote_rate) {
          var _this3 = this;

          // to do: check promotion first for validation
          var body = {
            action: 'promoteItem',
            item_id: this.item_id
          };
          this.postPvdr.postData(body, 'promotion.php').subscribe(function (data) {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this3, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
              return regeneratorRuntime.wrap(function _callee3$(_context3) {
                while (1) {
                  switch (_context3.prev = _context3.next) {
                    case 0:
                      if (data.success) {
                        this.presentToast("Your Item has been promoted");
                      }

                    case 1:
                    case "end":
                      return _context3.stop();
                  }
                }
              }, _callee3, this);
            }));
          });
        }
      }, {
        key: "goPromoteplus",
        value: function goPromoteplus() {
          this.router.navigate(['promoteplus']);
        }
      }, {
        key: "goBack",
        value: function goBack() {
          window.history.back();
        }
      }, {
        key: "presentToast",
        value: function presentToast(x) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
            var toast;
            return regeneratorRuntime.wrap(function _callee4$(_context4) {
              while (1) {
                switch (_context4.prev = _context4.next) {
                  case 0:
                    _context4.next = 2;
                    return this.toastController.create({
                      message: x,
                      duration: 3000
                    });

                  case 2:
                    toast = _context4.sent;
                    toast.present();

                  case 4:
                  case "end":
                    return _context4.stop();
                }
              }
            }, _callee4, this);
          }));
        }
      }]);

      return SellfasterPage;
    }();

    SellfasterPage.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
      }, {
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_2__["Storage"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ToastController"]
      }, {
        type: _providers_credential_provider__WEBPACK_IMPORTED_MODULE_6__["PostProvider"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]
      }];
    };

    SellfasterPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-sellfaster',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./sellfaster.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/sellfaster/sellfaster.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./sellfaster.page.scss */
      "./src/app/sellfaster/sellfaster.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_2__["Storage"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ToastController"], _providers_credential_provider__WEBPACK_IMPORTED_MODULE_6__["PostProvider"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]])], SellfasterPage);
    /***/
  },

  /***/
  "./src/app/shared/model/promotion.model.ts":
  /*!*************************************************!*\
    !*** ./src/app/shared/model/promotion.model.ts ***!
    \*************************************************/

  /*! exports provided: promotionModel */

  /***/
  function srcAppSharedModelPromotionModelTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "promotionModel", function () {
      return promotionModel;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");

    var promotionModel = function promotionModel(id, rate) {
      _classCallCheck(this, promotionModel);

      this.id = id;
      this.rate = rate;
    };
    /***/

  }
}]);
//# sourceMappingURL=sellfaster-sellfaster-module-es5.js.map