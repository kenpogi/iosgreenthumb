(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["sellingproductview-sellingproductview-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/sellingproductview/sellingproductview.page.html":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/sellingproductview/sellingproductview.page.html ***!
  \*******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n<ion-content>\n  <ion-card mode=\"ios\" no-margin class=\"for_card\">\n    <ion-card-header class=\"for_cardheader\">\n      <ion-icon class=\"for_back\" mode=\"ios\" name=\"arrow-back\" (click)=\"goBack()\"></ion-icon>\n      <img src=\"assets/greenthumb-images/tomato.png\" class=\"for_imgfruit\">\n    </ion-card-header>\n  </ion-card>\n  <ion-row style=\"margin-top: -25px;\">\n    <ion-col text-right>\n      <ion-chip mode=\"ios\" (click)=\"goSellerprof()\" style=\"--background: white; margin-top: -20px;\">\n        <ion-avatar>\n          <img src=\"assets/greenthumb-images/userpic.png\">\n        </ion-avatar>\n        <ion-label style=\"font-size: 12px;\" >by Ken Telmo</ion-label>\n      </ion-chip>&nbsp;\n      <ion-icon mode=\"ios\" class=\"for_social\" color=\"light\" name=\"share-alt\"></ion-icon>\n    </ion-col>\n  </ion-row>\n  <ion-row padding style=\"padding-top: 10px;padding-bottom: 0;\">\n    <ion-col text-left size=\"7\">\n      <ion-icon class=\"for_staricon\" name=\"star\" mode=\"ios\" size=\"medium\" color=\"warning\"></ion-icon>\n      <ion-icon class=\"for_staricon\" name=\"star\" mode=\"ios\" size=\"medium\" color=\"warning\"></ion-icon>\n      <ion-icon class=\"for_staricon\" name=\"star\" mode=\"ios\" size=\"medium\" color=\"warning\"></ion-icon>\n      <ion-icon class=\"for_staricon\" name=\"star\" mode=\"ios\" size=\"medium\" color=\"warning\"></ion-icon>\n      <ion-icon class=\"for_staricon\" name=\"star-half\" mode=\"ios\" size=\"medium\" color=\"warning\"></ion-icon>&nbsp;\n      <span class=\"for_spanstar\">(5)</span>\n      <ion-label color=\"dark\"><h1 style=\"margin-bottom: -2px;\"><b>Item Name</b></h1></ion-label>\n      <ion-label class=\"for_categname\">Category</ion-label>\n    </ion-col>\n    <ion-col text-right size=\"5\">\n      <ion-label color=\"secondary\"><h1 class=\"for_pr\"><b>$678</b></h1></ion-label>\n    </ion-col>\n  </ion-row>\n  <ion-row style=\"margin-top: -5px;padding-top: 0;padding-bottom: 0;\" padding >\n    <ion-col size=\"12\" no-padding text-left> \n      <ion-icon class=\"for_pin\" color=\"primary\" name=\"pin\" mode=\"ios\"></ion-icon>\n      <ion-label class=\"for_locname\" style=\"font-size: 13px;text-transform:capitalize;\" color=\"dark\">\n        Tacloban City Leyte Philipines</ion-label>\n    </ion-col>\n  </ion-row>\n  <ion-row padding style=\"padding-top: 0;padding-bottom: 5px;\">\n    <ion-col>\n      <ion-item mode=\"ios\" class=\"for_iteminfo\">\n        <ion-label color=\"dark\" class=\"for_label\"><b>Availability</b></ion-label>\n        <ion-label color=\"medium\" slot=\"end\" class=\"for_text\">In Stock</ion-label>\n      </ion-item>\n      <ion-item mode=\"ios\" class=\"for_iteminforest\">\n        <ion-label color=\"dark\" class=\"for_label\"><b>Delivery Type</b></ion-label>\n        <ion-label color=\"medium\" slot=\"end\" class=\"for_text\">Local Pickup</ion-label>\n      </ion-item>\n    </ion-col>\n  </ion-row>\n  <ion-row padding style=\"padding-top: 0;\">\n    <ion-col no-padding>\n      <p text-justify class=\"for_txtoverview\">\n        Lorem ipsum dolor sit amet, consectetur adipiscing elit. \n        Fusce convallis pellentesque metus id lacinia. Nunc dapibus pulvinar auctor. \n        Duis nec sem at orci commodo viverra id in ipsum. \n      </p>\n    </ion-col>\n  </ion-row>\n  \n        <ion-row padding>\n          <ion-col size=\"5\" no-padding>\n            <ion-button (click)=\"goEdit()\" fill=\"outline\" expand=\"block\" mode=\"ios\">\n              <ion-icon name=\"create\" mode=\"ios\"></ion-icon>&nbsp;EDIT ITEM\n            </ion-button>\n          </ion-col>\n          <ion-col size=\"5\" no-padding>\n            <ion-button expand=\"block\"mode=\"ios\">\n              MARK AS SOLD\n            </ion-button>\n          </ion-col>\n          <ion-col size=\"2\" no-padding>\n            <ion-button color=\"danger\" expand=\"block\" mode=\"ios\">\n              <ion-icon name=\"trash\" mode=\"ios\"></ion-icon>\n            </ion-button>\n          </ion-col>\n        </ion-row>\n      \n\n</ion-content>\n");

/***/ }),

/***/ "./src/app/sellingproductview/sellingproductview-routing.module.ts":
/*!*************************************************************************!*\
  !*** ./src/app/sellingproductview/sellingproductview-routing.module.ts ***!
  \*************************************************************************/
/*! exports provided: SellingproductviewPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SellingproductviewPageRoutingModule", function() { return SellingproductviewPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _sellingproductview_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./sellingproductview.page */ "./src/app/sellingproductview/sellingproductview.page.ts");




const routes = [
    {
        path: '',
        component: _sellingproductview_page__WEBPACK_IMPORTED_MODULE_3__["SellingproductviewPage"]
    }
];
let SellingproductviewPageRoutingModule = class SellingproductviewPageRoutingModule {
};
SellingproductviewPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], SellingproductviewPageRoutingModule);



/***/ }),

/***/ "./src/app/sellingproductview/sellingproductview.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/sellingproductview/sellingproductview.module.ts ***!
  \*****************************************************************/
/*! exports provided: SellingproductviewPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SellingproductviewPageModule", function() { return SellingproductviewPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _sellingproductview_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./sellingproductview-routing.module */ "./src/app/sellingproductview/sellingproductview-routing.module.ts");
/* harmony import */ var _sellingproductview_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./sellingproductview.page */ "./src/app/sellingproductview/sellingproductview.page.ts");







let SellingproductviewPageModule = class SellingproductviewPageModule {
};
SellingproductviewPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _sellingproductview_routing_module__WEBPACK_IMPORTED_MODULE_5__["SellingproductviewPageRoutingModule"]
        ],
        declarations: [_sellingproductview_page__WEBPACK_IMPORTED_MODULE_6__["SellingproductviewPage"]]
    })
], SellingproductviewPageModule);



/***/ }),

/***/ "./src/app/sellingproductview/sellingproductview.page.scss":
/*!*****************************************************************!*\
  !*** ./src/app/sellingproductview/sellingproductview.page.scss ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".for_card {\n  border-top-left-radius: 0;\n  border-top-right-radius: 0;\n  border-bottom-right-radius: 0px;\n  border-bottom-left-radius: 30px;\n}\n\n.for_cardheader {\n  background: linear-gradient(135deg, #e2f0cb 8%, #679733 83%);\n  padding: 30px 20px;\n}\n\n.for_editimage {\n  --background: white;\n  margin-top: -25px;\n  margin-right: 0;\n  border: 1px solid #e2f0cb;\n}\n\n.for_pr {\n  margin-bottom: -2px;\n  font-size: 35px;\n}\n\n.for_social {\n  zoom: 1.5;\n  background: #76c961;\n  border-radius: 25px;\n  padding: 4px;\n}\n\n.for_iteminfo {\n  --padding-start:0;\n  font-size: 14px;\n  --background: transparent;\n}\n\n.for_iteminforest {\n  --padding-start:0;\n  font-size: 14px;\n  margin-top: -10px;\n  --background: transparent;\n}\n\n.for_label {\n  margin-bottom: -2px;\n}\n\n.for_text {\n  margin-bottom: -2px;\n  text-align: right;\n  margin-right: 0;\n}\n\n.for_pin {\n  margin-bottom: -3px;\n}\n\n.for_txtoverview {\n  font-size: 13px;\n  padding-left: 5px;\n  margin-top: 5px;\n  padding-right: 5px;\n  color: #666666;\n}\n\n.for_back {\n  position: absolute;\n  left: 2%;\n  top: 5%;\n  font-size: 20px;\n}\n\n.for_report {\n  position: absolute;\n  right: 2%;\n  top: 5%;\n  font-size: 15px;\n  color: black;\n  margin: 0;\n  font-weight: normal;\n  text-transform: capitalize;\n}\n\n.for_spanstar {\n  font-size: 14px;\n  font-weight: normal;\n}\n\n.for_staricon {\n  font-size: 18px;\n  margin-bottom: -4px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZW5kcmljay9EZXNrdG9wL1RlbG1vIFNvbHV0aW9ucy9Gb3JQcm9kdWN0aW9uL21heTEwMjAyMS9sYXN0IHNldHVwL2dyZWVudGh1bWJhcHAgY29weS9zcmMvYXBwL3NlbGxpbmdwcm9kdWN0dmlldy9zZWxsaW5ncHJvZHVjdHZpZXcucGFnZS5zY3NzIiwic3JjL2FwcC9zZWxsaW5ncHJvZHVjdHZpZXcvc2VsbGluZ3Byb2R1Y3R2aWV3LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFRTtFQUNFLHlCQUFBO0VBQ0EsMEJBQUE7RUFDQSwrQkFBQTtFQUNBLCtCQUFBO0FDREo7O0FER0U7RUFHRSw0REFBQTtFQUNBLGtCQUFBO0FDQUo7O0FERUU7RUFDRSxtQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLHlCQUFBO0FDQ0o7O0FEQ0U7RUFDRSxtQkFBQTtFQUFvQixlQUFBO0FDR3hCOztBRERFO0VBQ0UsU0FBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0FDSUo7O0FERkU7RUFDRSxpQkFBQTtFQUFrQixlQUFBO0VBQ2xCLHlCQUFBO0FDTUo7O0FESkU7RUFDRSxpQkFBQTtFQUFrQixlQUFBO0VBQ2xCLGlCQUFBO0VBQ0EseUJBQUE7QUNRSjs7QURORTtFQUNFLG1CQUFBO0FDU0o7O0FEUEU7RUFDRSxtQkFBQTtFQUFvQixpQkFBQTtFQUNwQixlQUFBO0FDV0o7O0FEVEE7RUFDRSxtQkFBQTtBQ1lGOztBRFRBO0VBQ0ksZUFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsY0FBQTtBQ1lKOztBRFZBO0VBQ0Usa0JBQUE7RUFDQSxRQUFBO0VBQ0EsT0FBQTtFQUNBLGVBQUE7QUNhRjs7QURYQTtFQUNFLGtCQUFBO0VBQ0UsU0FBQTtFQUNBLE9BQUE7RUFDQSxlQUFBO0VBQ0EsWUFBQTtFQUNBLFNBQUE7RUFDQSxtQkFBQTtFQUNBLDBCQUFBO0FDY0o7O0FEWkE7RUFDRSxlQUFBO0VBQWdCLG1CQUFBO0FDZ0JsQjs7QURkQTtFQUNFLGVBQUE7RUFDQSxtQkFBQTtBQ2lCRiIsImZpbGUiOiJzcmMvYXBwL3NlbGxpbmdwcm9kdWN0dmlldy9zZWxsaW5ncHJvZHVjdHZpZXcucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXG4gIFxuICAuZm9yX2NhcmR7XG4gICAgYm9yZGVyLXRvcC1sZWZ0LXJhZGl1czogMDtcbiAgICBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogMDtcbiAgICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogMHB4O1xuICAgIGJvcmRlci1ib3R0b20tbGVmdC1yYWRpdXM6IDMwcHg7XG4gIH1cbiAgLmZvcl9jYXJkaGVhZGVyIHtcbiAgICBiYWNrZ3JvdW5kOiAtbW96LWxpbmVhci1ncmFkaWVudCgtNDVkZWcsI2UyZjBjYiA4JSwgIzY3OTczMyA4MyUpO1xuICAgIGJhY2tncm91bmQ6IC13ZWJraXQtbGluZWFyLWdyYWRpZW50KC00NWRlZywjZTJmMGNiIDglLCAjNjc5NzMzIDgzJSk7XG4gICAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDEzNWRlZywgI2UyZjBjYiA4JSwgIzY3OTczMyA4MyUpO1xuICAgIHBhZGRpbmc6IDMwcHggMjBweDtcbiAgfVxuICAuZm9yX2VkaXRpbWFnZXtcbiAgICAtLWJhY2tncm91bmQ6IHdoaXRlO1xuICAgIG1hcmdpbi10b3A6IC0yNXB4O1xuICAgIG1hcmdpbi1yaWdodDogMDtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjZTJmMGNiO1xuICB9XG4gIC5mb3JfcHJ7XG4gICAgbWFyZ2luLWJvdHRvbTogLTJweDtmb250LXNpemU6IDM1cHg7XG4gIH1cbiAgLmZvcl9zb2NpYWx7XG4gICAgem9vbTogMS41O1xuICAgIGJhY2tncm91bmQ6ICM3NmM5NjE7XG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcbiAgICBwYWRkaW5nOiA0cHg7XG4gIH1cbiAgLmZvcl9pdGVtaW5mb3tcbiAgICAtLXBhZGRpbmctc3RhcnQ6MDtmb250LXNpemU6IDE0cHg7XG4gICAgLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgfVxuICAuZm9yX2l0ZW1pbmZvcmVzdHtcbiAgICAtLXBhZGRpbmctc3RhcnQ6MDtmb250LXNpemU6IDE0cHg7XG4gICAgbWFyZ2luLXRvcDogLTEwcHg7XG4gICAgLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgfVxuICAuZm9yX2xhYmVse1xuICAgIG1hcmdpbi1ib3R0b206IC0ycHg7XG4gIH1cbiAgLmZvcl90ZXh0e1xuICAgIG1hcmdpbi1ib3R0b206IC0ycHg7dGV4dC1hbGlnbjogcmlnaHQ7XG4gICAgbWFyZ2luLXJpZ2h0OiAwO1xuICB9XG4uZm9yX3BpbntcbiAgbWFyZ2luLWJvdHRvbTogLTNweDtcbn1cblxuLmZvcl90eHRvdmVydmlld3tcbiAgICBmb250LXNpemU6IDEzcHg7XG4gICAgcGFkZGluZy1sZWZ0OiA1cHg7XG4gICAgbWFyZ2luLXRvcDogNXB4O1xuICAgIHBhZGRpbmctcmlnaHQ6IDVweDtcbiAgICBjb2xvcjogIzY2NjY2Njtcbn1cbi5mb3JfYmFja3tcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiAyJTtcbiAgdG9wOiA1JTtcbiAgZm9udC1zaXplOiAyMHB4O1xufVxuLmZvcl9yZXBvcnR7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICByaWdodDogMiU7XG4gICAgdG9wOiA1JTtcbiAgICBmb250LXNpemU6IDE1cHg7XG4gICAgY29sb3I6IGJsYWNrO1xuICAgIG1hcmdpbjogMDtcbiAgICBmb250LXdlaWdodDogbm9ybWFsO1xuICAgIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xufVxuLmZvcl9zcGFuc3RhcntcbiAgZm9udC1zaXplOiAxNHB4O2ZvbnQtd2VpZ2h0OiBub3JtYWw7XG59XG4uZm9yX3N0YXJpY29ue1xuICBmb250LXNpemU6IDE4cHg7XG4gIG1hcmdpbi1ib3R0b206IC00cHg7XG59XG4gICIsIi5mb3JfY2FyZCB7XG4gIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDA7XG4gIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiAwO1xuICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogMHB4O1xuICBib3JkZXItYm90dG9tLWxlZnQtcmFkaXVzOiAzMHB4O1xufVxuXG4uZm9yX2NhcmRoZWFkZXIge1xuICBiYWNrZ3JvdW5kOiAtbW96LWxpbmVhci1ncmFkaWVudCgtNDVkZWcsICNlMmYwY2IgOCUsICM2Nzk3MzMgODMlKTtcbiAgYmFja2dyb3VuZDogLXdlYmtpdC1saW5lYXItZ3JhZGllbnQoLTQ1ZGVnLCAjZTJmMGNiIDglLCAjNjc5NzMzIDgzJSk7XG4gIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCgxMzVkZWcsICNlMmYwY2IgOCUsICM2Nzk3MzMgODMlKTtcbiAgcGFkZGluZzogMzBweCAyMHB4O1xufVxuXG4uZm9yX2VkaXRpbWFnZSB7XG4gIC0tYmFja2dyb3VuZDogd2hpdGU7XG4gIG1hcmdpbi10b3A6IC0yNXB4O1xuICBtYXJnaW4tcmlnaHQ6IDA7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNlMmYwY2I7XG59XG5cbi5mb3JfcHIge1xuICBtYXJnaW4tYm90dG9tOiAtMnB4O1xuICBmb250LXNpemU6IDM1cHg7XG59XG5cbi5mb3Jfc29jaWFsIHtcbiAgem9vbTogMS41O1xuICBiYWNrZ3JvdW5kOiAjNzZjOTYxO1xuICBib3JkZXItcmFkaXVzOiAyNXB4O1xuICBwYWRkaW5nOiA0cHg7XG59XG5cbi5mb3JfaXRlbWluZm8ge1xuICAtLXBhZGRpbmctc3RhcnQ6MDtcbiAgZm9udC1zaXplOiAxNHB4O1xuICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xufVxuXG4uZm9yX2l0ZW1pbmZvcmVzdCB7XG4gIC0tcGFkZGluZy1zdGFydDowO1xuICBmb250LXNpemU6IDE0cHg7XG4gIG1hcmdpbi10b3A6IC0xMHB4O1xuICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xufVxuXG4uZm9yX2xhYmVsIHtcbiAgbWFyZ2luLWJvdHRvbTogLTJweDtcbn1cblxuLmZvcl90ZXh0IHtcbiAgbWFyZ2luLWJvdHRvbTogLTJweDtcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gIG1hcmdpbi1yaWdodDogMDtcbn1cblxuLmZvcl9waW4ge1xuICBtYXJnaW4tYm90dG9tOiAtM3B4O1xufVxuXG4uZm9yX3R4dG92ZXJ2aWV3IHtcbiAgZm9udC1zaXplOiAxM3B4O1xuICBwYWRkaW5nLWxlZnQ6IDVweDtcbiAgbWFyZ2luLXRvcDogNXB4O1xuICBwYWRkaW5nLXJpZ2h0OiA1cHg7XG4gIGNvbG9yOiAjNjY2NjY2O1xufVxuXG4uZm9yX2JhY2sge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGxlZnQ6IDIlO1xuICB0b3A6IDUlO1xuICBmb250LXNpemU6IDIwcHg7XG59XG5cbi5mb3JfcmVwb3J0IHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICByaWdodDogMiU7XG4gIHRvcDogNSU7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgY29sb3I6IGJsYWNrO1xuICBtYXJnaW46IDA7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xufVxuXG4uZm9yX3NwYW5zdGFyIHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBmb250LXdlaWdodDogbm9ybWFsO1xufVxuXG4uZm9yX3N0YXJpY29uIHtcbiAgZm9udC1zaXplOiAxOHB4O1xuICBtYXJnaW4tYm90dG9tOiAtNHB4O1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/sellingproductview/sellingproductview.page.ts":
/*!***************************************************************!*\
  !*** ./src/app/sellingproductview/sellingproductview.page.ts ***!
  \***************************************************************/
/*! exports provided: SellingproductviewPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SellingproductviewPage", function() { return SellingproductviewPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");



let SellingproductviewPage = class SellingproductviewPage {
    constructor(router) {
        this.router = router;
    }
    goBack() {
        window.history.back();
    }
    goSellerprof() {
        this.router.navigate((['viewitemsellerprofile']));
    }
    goEdit() {
        this.router.navigate((['edititem']));
    }
    ngOnInit() {
    }
};
SellingproductviewPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
SellingproductviewPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-sellingproductview',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./sellingproductview.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/sellingproductview/sellingproductview.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./sellingproductview.page.scss */ "./src/app/sellingproductview/sellingproductview.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
], SellingproductviewPage);



/***/ })

}]);
//# sourceMappingURL=sellingproductview-sellingproductview-module-es2015.js.map