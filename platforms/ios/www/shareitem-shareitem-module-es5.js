function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["shareitem-shareitem-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/shareitem/shareitem.page.html":
  /*!*************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shareitem/shareitem.page.html ***!
    \*************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppShareitemShareitemPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n<ion-content text-center>\n    <ion-card mode=\"ios\" no-margin class=\"for_card\" style=\"box-shadow:none\">\n      <ion-card-header class=\"for_cardheader\">\n        <ion-icon class=\"for_back\" mode=\"ios\" name=\"arrow-back\" (click)=\"goBack()\"></ion-icon>\n        <img *ngIf=\"!ImageArray\" src=\"assets/greenthumb-images/tomato.png\" class=\"for_imgfruit\"> \n\n        <ion-slides *ngIf=\"ImageArray\" [options]=\"slideOptsOne\" pager=\"true\" mode=\"ios\" id=\"slide_forpromo\">\n          <ion-slide *ngFor=\"let slide of ImageArray\" mode=\"ios\">\n            <div class=\"slide\" mode=\"ios\">\n              <ion-card mode=\"ios\" class=\"promocard\">\n                <ion-card-header mode=\"ios\" class=\"promoheader\" style=\"padding: 7px;\">\n                  <ion-row>\n                  \n                    <ion-col no-padding text-right>\n                      <img src=\"{{slide.picture_filename}}\" class=\"for_imgfruit\">\n                    </ion-col>\n      \n                  </ion-row>\n                </ion-card-header>\n              </ion-card>\n            </div>\n          </ion-slide>\n        </ion-slides>\n\n      </ion-card-header>\n    </ion-card>\n    <ion-row style=\"margin-top: -25px;\">\n      <ion-col text-left>\n        <ion-chip mode=\"ios\" (click)=\"goSellerprof()\" style=\"--background: white;margin-left: 20px;\">\n          <ion-avatar>\n            <div *ngIf=\"!picture\">\n              <img src=\"assets/greenthumb-images/userpic.png\">\n            </div>\n            <div *ngIf=\"picture\">\n              <img [src] = \"complete_pic\">\n            </div>\n          </ion-avatar>\n          <ion-label style=\"font-size: 12px;\" >by {{username}}</ion-label>\n        </ion-chip>\n      </ion-col>\n      <ion-col text-right>\n        <ion-icon mode=\"ios\" class=\"for_social\" color=\"light\" name=\"logo-facebook\"></ion-icon>&nbsp;\n        <ion-icon mode=\"ios\" class=\"for_social\" color=\"light\" name=\"logo-instagram\"></ion-icon>\n      </ion-col>\n    </ion-row>\n    <ion-row padding style=\"padding-top: 10px;padding-bottom: 0;\">\n      <ion-col text-left size=\"7\">\n        <ion-label color=\"dark\"><h1 style=\"margin-bottom: -2px;\"><b>{{title}}</b></h1></ion-label>\n        <ion-label class=\"for_categname\">{{category_name}}</ion-label>\n      </ion-col>\n      <ion-col text-right size=\"5\">\n        <ion-label color=\"secondary\"><h1 class=\"for_pr\"><b>${{price}}</b></h1></ion-label>\n      </ion-col>\n    </ion-row>\n    <ion-row style=\"margin-top: -5px;padding-top: 0;padding-bottom: 0;\" padding >\n      <ion-col size=\"12\" no-padding text-left> \n        <ion-icon class=\"for_pin\" color=\"primary\" name=\"pin\" mode=\"ios\"></ion-icon>\n        <ion-label class=\"for_locname\" style=\"font-size: 13px;text-transform:capitalize;\" color=\"dark\">\n          {{location}} {{district_area}} {{zip_code}}</ion-label>\n      </ion-col>\n    </ion-row>\n    <ion-row padding style=\"padding-top: 0;padding-bottom: 5px;\">\n      <ion-col>\n        <ion-item mode=\"ios\" class=\"for_iteminfo\">\n          <ion-label color=\"dark\" class=\"for_label\"><b>Quantity and Unit</b></ion-label>\n          <ion-label color=\"medium\" slot=\"end\" class=\"for_text\">{{quantity}} {{stocks_name}}</ion-label>\n        </ion-item>\n        <ion-item mode=\"ios\" class=\"for_iteminforest\">\n          <ion-label color=\"dark\" class=\"for_label\"><b>Availability</b></ion-label>\n          <ion-label color=\"medium\" slot=\"end\" class=\"for_text\">In Stock</ion-label>\n        </ion-item>\n        <ion-item mode=\"ios\" class=\"for_iteminforest\">\n          <ion-label color=\"dark\" class=\"for_label\"><b>Delivery Type</b></ion-label>\n          <ion-label color=\"medium\" slot=\"end\" class=\"for_text\">{{delivery_type}}</ion-label>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n    <ion-row padding style=\"padding-top: 0;\">\n      <ion-col text-left no-padding size=\"4\">\n        <ion-label style=\"font-size: 14px;margin-left: 5px;\" color=\"dark\" class=\"for_label\"><b>Description</b></ion-label>\n      </ion-col>\n      <ion-col no-padding size=\"8\" text-right>\n        <p class=\"for_txtoverview\">\n          {{description}}\n        </p>\n      </ion-col>\n    </ion-row>\n</ion-content>\n<div class=\"postitem-overlay\" padding>\n  <ion-button expand=\"block\" (click)=\"goItemposted()\" mode=\"ios\" fill=\"outline\" color=\"primary\">\n    POST\n  </ion-button>\n  <div class=\"postitem_container\">\n    <ul class=\"postitem_progressbar\">\n      <li class=\"whenfocus active\"><span>Photo</span></li>\n      <li class=\"whenfocus active\"><span>Details</span></li>\n      <li class=\"whenfocus active\"><span>Price</span></li>\n      <li class=\"whenfocus active\"><span>Delivery</span></li>\n      <li class=\"whenfocus\"><span>Post</span></li>\n    </ul>\n  </div>\n</div>\n";
    /***/
  },

  /***/
  "./src/app/shareitem/shareitem-routing.module.ts":
  /*!*******************************************************!*\
    !*** ./src/app/shareitem/shareitem-routing.module.ts ***!
    \*******************************************************/

  /*! exports provided: ShareitemPageRoutingModule */

  /***/
  function srcAppShareitemShareitemRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ShareitemPageRoutingModule", function () {
      return ShareitemPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _shareitem_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./shareitem.page */
    "./src/app/shareitem/shareitem.page.ts");

    var routes = [{
      path: '',
      component: _shareitem_page__WEBPACK_IMPORTED_MODULE_3__["ShareitemPage"]
    }];

    var ShareitemPageRoutingModule = function ShareitemPageRoutingModule() {
      _classCallCheck(this, ShareitemPageRoutingModule);
    };

    ShareitemPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], ShareitemPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/shareitem/shareitem.module.ts":
  /*!***********************************************!*\
    !*** ./src/app/shareitem/shareitem.module.ts ***!
    \***********************************************/

  /*! exports provided: ShareitemPageModule */

  /***/
  function srcAppShareitemShareitemModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ShareitemPageModule", function () {
      return ShareitemPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _shareitem_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./shareitem-routing.module */
    "./src/app/shareitem/shareitem-routing.module.ts");
    /* harmony import */


    var _shareitem_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./shareitem.page */
    "./src/app/shareitem/shareitem.page.ts");

    var ShareitemPageModule = function ShareitemPageModule() {
      _classCallCheck(this, ShareitemPageModule);
    };

    ShareitemPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _shareitem_routing_module__WEBPACK_IMPORTED_MODULE_5__["ShareitemPageRoutingModule"]],
      declarations: [_shareitem_page__WEBPACK_IMPORTED_MODULE_6__["ShareitemPage"]]
    })], ShareitemPageModule);
    /***/
  },

  /***/
  "./src/app/shareitem/shareitem.page.scss":
  /*!***********************************************!*\
    !*** ./src/app/shareitem/shareitem.page.scss ***!
    \***********************************************/

  /*! exports provided: default */

  /***/
  function srcAppShareitemShareitemPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".for_content {\n  width: 100%;\n  height: 850px;\n}\n\n.for_card {\n  border-top-left-radius: 0;\n  border-top-right-radius: 0;\n  border-bottom-right-radius: 0px;\n  border-bottom-left-radius: 30px;\n}\n\n.for_cardheader {\n  background: linear-gradient(135deg, #e2f0cb 8%, #e2f0cb 83%);\n  padding-top: 30px;\n}\n\n.for_editimage {\n  --background: white;\n  margin-top: -25px;\n  margin-right: 0;\n  border: 1px solid #e2f0cb;\n}\n\n.for_pr {\n  margin-bottom: -2px;\n  font-size: 35px;\n}\n\n.for_social {\n  zoom: 1.5;\n  background: #76c961;\n  border-radius: 25px;\n  padding: 4px;\n}\n\n.for_iteminfo {\n  --padding-start:0;\n  font-size: 14px;\n  --background: transparent;\n}\n\n.for_iteminforest {\n  --padding-start:0;\n  font-size: 14px;\n  margin-top: -10px;\n  --background: transparent;\n}\n\n.for_label {\n  margin-bottom: -2px;\n}\n\n.for_text {\n  margin-bottom: -2px;\n  text-align: right;\n  margin-right: 0;\n}\n\n.for_pin {\n  margin-bottom: -3px;\n}\n\n.for_txtoverview {\n  font-size: 14px;\n  margin-top: 0;\n  color: #989aa2;\n  padding-right: 12px;\n  padding-left: 0;\n}\n\n.for_back {\n  position: absolute;\n  left: 2%;\n  top: 5%;\n  font-size: 20px;\n}\n\nion-avatar {\n  width: 100% !important;\n  max-width: 25px !important;\n}\n\nion-avatar img {\n  border: 1px solid rgba(0, 0, 0, 0.12);\n  width: 100% !important;\n  height: 100% !important;\n  max-width: 25px !important;\n  max-height: 25px !important;\n  border-radius: 50%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZW5kcmljay9EZXNrdG9wL1RlbG1vIFNvbHV0aW9ucy9Gb3JQcm9kdWN0aW9uL21heTEwMjAyMS9sYXN0IHNldHVwL2dyZWVudGh1bWJhcHAgY29weS9zcmMvYXBwL3NoYXJlaXRlbS9zaGFyZWl0ZW0ucGFnZS5zY3NzIiwic3JjL2FwcC9zaGFyZWl0ZW0vc2hhcmVpdGVtLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFdBQUE7RUFDQSxhQUFBO0FDQ0o7O0FER0U7RUFDRSx5QkFBQTtFQUNBLDBCQUFBO0VBQ0EsK0JBQUE7RUFDQSwrQkFBQTtBQ0FKOztBREVFO0VBR0UsNERBQUE7RUFDQSxpQkFBQTtBQ0NKOztBRENFO0VBQ0UsbUJBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7RUFDQSx5QkFBQTtBQ0VKOztBREFFO0VBQ0UsbUJBQUE7RUFBb0IsZUFBQTtBQ0l4Qjs7QURGRTtFQUNFLFNBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtBQ0tKOztBREhFO0VBQ0UsaUJBQUE7RUFBa0IsZUFBQTtFQUNsQix5QkFBQTtBQ09KOztBRExFO0VBQ0UsaUJBQUE7RUFBa0IsZUFBQTtFQUNsQixpQkFBQTtFQUNBLHlCQUFBO0FDU0o7O0FEUEU7RUFDRSxtQkFBQTtBQ1VKOztBRFJFO0VBQ0UsbUJBQUE7RUFBb0IsaUJBQUE7RUFDcEIsZUFBQTtBQ1lKOztBRFZBO0VBQ0UsbUJBQUE7QUNhRjs7QURWQTtFQUNFLGVBQUE7RUFDQSxhQUFBO0VBQ0EsY0FBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtBQ2FGOztBRFhBO0VBQ0Usa0JBQUE7RUFDQSxRQUFBO0VBQ0EsT0FBQTtFQUNBLGVBQUE7QUNjRjs7QURaQTtFQUNFLHNCQUFBO0VBQ0EsMEJBQUE7QUNlRjs7QURkRTtFQUNFLHFDQUFBO0VBQ0Esc0JBQUE7RUFDQSx1QkFBQTtFQUNBLDBCQUFBO0VBQ0EsMkJBQUE7RUFDQSxrQkFBQTtBQ2dCSiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlaXRlbS9zaGFyZWl0ZW0ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmZvcl9jb250ZW50IHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDg1MHB4O1xuICB9IFxuXG4gIFxuICAuZm9yX2NhcmR7XG4gICAgYm9yZGVyLXRvcC1sZWZ0LXJhZGl1czogMDtcbiAgICBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogMDtcbiAgICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogMHB4O1xuICAgIGJvcmRlci1ib3R0b20tbGVmdC1yYWRpdXM6IDMwcHg7XG4gIH1cbiAgLmZvcl9jYXJkaGVhZGVyIHtcbiAgICBiYWNrZ3JvdW5kOiAtbW96LWxpbmVhci1ncmFkaWVudCgtNDVkZWcsI2UyZjBjYiA4JSwgI2UyZjBjYiA4MyUpO1xuICAgIGJhY2tncm91bmQ6IC13ZWJraXQtbGluZWFyLWdyYWRpZW50KC00NWRlZywjZTJmMGNiIDglLCAjZTJmMGNiIDgzJSk7XG4gICAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDEzNWRlZywgI2UyZjBjYiA4JSwgI2UyZjBjYiA4MyUpO1xuICAgIHBhZGRpbmctdG9wOiAzMHB4O1xuICB9XG4gIC5mb3JfZWRpdGltYWdle1xuICAgIC0tYmFja2dyb3VuZDogd2hpdGU7XG4gICAgbWFyZ2luLXRvcDogLTI1cHg7XG4gICAgbWFyZ2luLXJpZ2h0OiAwO1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICNlMmYwY2I7XG4gIH1cbiAgLmZvcl9wcntcbiAgICBtYXJnaW4tYm90dG9tOiAtMnB4O2ZvbnQtc2l6ZTogMzVweDtcbiAgfVxuICAuZm9yX3NvY2lhbHtcbiAgICB6b29tOiAxLjU7XG4gICAgYmFja2dyb3VuZDogIzc2Yzk2MTtcbiAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xuICAgIHBhZGRpbmc6IDRweDtcbiAgfVxuICAuZm9yX2l0ZW1pbmZve1xuICAgIC0tcGFkZGluZy1zdGFydDowO2ZvbnQtc2l6ZTogMTRweDtcbiAgICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICB9XG4gIC5mb3JfaXRlbWluZm9yZXN0e1xuICAgIC0tcGFkZGluZy1zdGFydDowO2ZvbnQtc2l6ZTogMTRweDtcbiAgICBtYXJnaW4tdG9wOiAtMTBweDtcbiAgICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICB9XG4gIC5mb3JfbGFiZWx7XG4gICAgbWFyZ2luLWJvdHRvbTogLTJweDtcbiAgfVxuICAuZm9yX3RleHR7XG4gICAgbWFyZ2luLWJvdHRvbTogLTJweDt0ZXh0LWFsaWduOiByaWdodDtcbiAgICBtYXJnaW4tcmlnaHQ6IDA7XG4gIH1cbi5mb3JfcGlue1xuICBtYXJnaW4tYm90dG9tOiAtM3B4O1xufVxuXG4uZm9yX3R4dG92ZXJ2aWV3e1xuICBmb250LXNpemU6IDE0cHg7XG4gIG1hcmdpbi10b3A6IDA7XG4gIGNvbG9yOiAjOTg5YWEyO1xuICBwYWRkaW5nLXJpZ2h0OiAxMnB4O1xuICBwYWRkaW5nLWxlZnQ6IDA7XG59XG4uZm9yX2JhY2t7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogMiU7XG4gIHRvcDogNSU7XG4gIGZvbnQtc2l6ZTogMjBweDtcbn1cbmlvbi1hdmF0YXIgIHsgICAgIFxuICB3aWR0aDogMTAwJSAhaW1wb3J0YW50O1xuICBtYXgtd2lkdGg6IDI1cHggIWltcG9ydGFudDtcbiAgaW1ne1xuICAgIGJvcmRlcjogMXB4IHNvbGlkIHJnYmEoMCwgMCwgMCwgMC4xMik7XG4gICAgd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcbiAgICBoZWlnaHQ6IDEwMCUgIWltcG9ydGFudDtcbiAgICBtYXgtd2lkdGg6IDI1cHggIWltcG9ydGFudDtcbiAgICBtYXgtaGVpZ2h0OiAyNXB4ICFpbXBvcnRhbnQ7XG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xuICB9XG59XG5cbiAgIiwiLmZvcl9jb250ZW50IHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogODUwcHg7XG59XG5cbi5mb3JfY2FyZCB7XG4gIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDA7XG4gIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiAwO1xuICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogMHB4O1xuICBib3JkZXItYm90dG9tLWxlZnQtcmFkaXVzOiAzMHB4O1xufVxuXG4uZm9yX2NhcmRoZWFkZXIge1xuICBiYWNrZ3JvdW5kOiAtbW96LWxpbmVhci1ncmFkaWVudCgtNDVkZWcsICNlMmYwY2IgOCUsICNlMmYwY2IgODMlKTtcbiAgYmFja2dyb3VuZDogLXdlYmtpdC1saW5lYXItZ3JhZGllbnQoLTQ1ZGVnLCAjZTJmMGNiIDglLCAjZTJmMGNiIDgzJSk7XG4gIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCgxMzVkZWcsICNlMmYwY2IgOCUsICNlMmYwY2IgODMlKTtcbiAgcGFkZGluZy10b3A6IDMwcHg7XG59XG5cbi5mb3JfZWRpdGltYWdlIHtcbiAgLS1iYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgbWFyZ2luLXRvcDogLTI1cHg7XG4gIG1hcmdpbi1yaWdodDogMDtcbiAgYm9yZGVyOiAxcHggc29saWQgI2UyZjBjYjtcbn1cblxuLmZvcl9wciB7XG4gIG1hcmdpbi1ib3R0b206IC0ycHg7XG4gIGZvbnQtc2l6ZTogMzVweDtcbn1cblxuLmZvcl9zb2NpYWwge1xuICB6b29tOiAxLjU7XG4gIGJhY2tncm91bmQ6ICM3NmM5NjE7XG4gIGJvcmRlci1yYWRpdXM6IDI1cHg7XG4gIHBhZGRpbmc6IDRweDtcbn1cblxuLmZvcl9pdGVtaW5mbyB7XG4gIC0tcGFkZGluZy1zdGFydDowO1xuICBmb250LXNpemU6IDE0cHg7XG4gIC0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG59XG5cbi5mb3JfaXRlbWluZm9yZXN0IHtcbiAgLS1wYWRkaW5nLXN0YXJ0OjA7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgbWFyZ2luLXRvcDogLTEwcHg7XG4gIC0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG59XG5cbi5mb3JfbGFiZWwge1xuICBtYXJnaW4tYm90dG9tOiAtMnB4O1xufVxuXG4uZm9yX3RleHQge1xuICBtYXJnaW4tYm90dG9tOiAtMnB4O1xuICB0ZXh0LWFsaWduOiByaWdodDtcbiAgbWFyZ2luLXJpZ2h0OiAwO1xufVxuXG4uZm9yX3BpbiB7XG4gIG1hcmdpbi1ib3R0b206IC0zcHg7XG59XG5cbi5mb3JfdHh0b3ZlcnZpZXcge1xuICBmb250LXNpemU6IDE0cHg7XG4gIG1hcmdpbi10b3A6IDA7XG4gIGNvbG9yOiAjOTg5YWEyO1xuICBwYWRkaW5nLXJpZ2h0OiAxMnB4O1xuICBwYWRkaW5nLWxlZnQ6IDA7XG59XG5cbi5mb3JfYmFjayB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogMiU7XG4gIHRvcDogNSU7XG4gIGZvbnQtc2l6ZTogMjBweDtcbn1cblxuaW9uLWF2YXRhciB7XG4gIHdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XG4gIG1heC13aWR0aDogMjVweCAhaW1wb3J0YW50O1xufVxuaW9uLWF2YXRhciBpbWcge1xuICBib3JkZXI6IDFweCBzb2xpZCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xuICB3aWR0aDogMTAwJSAhaW1wb3J0YW50O1xuICBoZWlnaHQ6IDEwMCUgIWltcG9ydGFudDtcbiAgbWF4LXdpZHRoOiAyNXB4ICFpbXBvcnRhbnQ7XG4gIG1heC1oZWlnaHQ6IDI1cHggIWltcG9ydGFudDtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/shareitem/shareitem.page.ts":
  /*!*********************************************!*\
    !*** ./src/app/shareitem/shareitem.page.ts ***!
    \*********************************************/

  /*! exports provided: ShareitemPage */

  /***/
  function srcAppShareitemShareitemPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ShareitemPage", function () {
      return ShareitemPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var _providers_credential_provider__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../providers/credential-provider */
    "./src/providers/credential-provider.ts");
    /* harmony import */


    var _shared_model_item_model__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../shared/model/item.model */
    "./src/app/shared/model/item.model.ts");

    var ShareitemPage = /*#__PURE__*/function () {
      function ShareitemPage(router, storage, postPvdr, navCtrl, platform) {
        var _this = this;

        _classCallCheck(this, ShareitemPage);

        this.router = router;
        this.storage = storage;
        this.postPvdr = postPvdr;
        this.navCtrl = navCtrl;
        this.platform = platform;
        this.ImageArray = [];
        this.username = "";
        this.slideOptsOne = {
          initialSlide: 0,
          slidesPerView: 1,
          autoplay: true
        };
        this.subscription = this.platform.backButton.subscribe(function () {
          console.log('Handler was called!');

          _this.goBack();
        });
      }

      _createClass(ShareitemPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "ionViewWillLeave",
        value: function ionViewWillLeave() {
          this.subscription.unsubscribe();
        }
      }, {
        key: "ionViewWillEnter",
        value: function ionViewWillEnter() {
          var _this2 = this;

          this.storage.get("greenthumb_user_id").then(function (user_id) {
            _this2.storage.get("greenthumb_title").then(function (title) {
              _this2.storage.get("greenthumb_description").then(function (val) {
                _this2.storage.get("greenthumb_price").then(function (price) {
                  _this2.storage.get("greenthumb_location").then(function (item) {
                    _this2.storage.get("greenthumb_itempost_pic").then(function (pictures) {
                      _this2.user_id = user_id;
                      _this2.category_id = val['category_id'];
                      _this2.category_name = val['category_name'];
                      _this2.stocks_id = val['stocks_id'];
                      _this2.stocks_name = val['stocks_name'];
                      _this2.quantity = val['quantity'];
                      _this2.title = title;
                      _this2.description = val['description'];
                      _this2.price = price;
                      _this2.zip_code = item['zip_code'];
                      _this2.location = item['location'];
                      _this2.district_area = item['district_area'];
                      _this2.delivery_type_id = item['delivery_type_id'];
                      _this2.delivery_type = item['delivery_type'];
                      _this2.item_pictures = pictures;

                      for (var key in pictures) {
                        // console.log("filenames:"+pictures[key].picture_filename)
                        _this2.ImageArray.push(new _shared_model_item_model__WEBPACK_IMPORTED_MODULE_6__["ItemPicture"](_this2.postPvdr.myServer() + "/greenthumb/images/posted_items/images/" + pictures[key].picture_filename));
                      }
                    });
                  });
                });
              });
            });

            var body = {
              action: 'getUsername',
              user_id: user_id
            };
            console.log(JSON.stringify(body));

            _this2.postPvdr.postData(body, 'user.php').subscribe(function (data) {
              return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                return regeneratorRuntime.wrap(function _callee$(_context) {
                  while (1) {
                    switch (_context.prev = _context.next) {
                      case 0:
                        this.username = data.username;
                        this.picture = data.profile_photo;
                        this.complete_pic = this.postPvdr.myServer() + "/greenthumb/images/" + this.picture;

                      case 3:
                      case "end":
                        return _context.stop();
                    }
                  }
                }, _callee, this);
              }));
            });
          });
        }
      }, {
        key: "goHelp",
        value: function goHelp() {
          this.navCtrl.navigateForward(['help'], {
            animated: false
          });
        }
      }, {
        key: "goBack",
        value: function goBack() {
          //this.navCtrl.navigateForward((['selectlocation']), { animated: false, });
          window.history.back();
        }
      }, {
        key: "goSellerprof",
        value: function goSellerprof() {}
      }, {
        key: "goItemposted",
        value: function goItemposted() {
          var _this3 = this;

          var body = {
            action: 'postItem',
            user_id: this.user_id,
            category_id: this.category_id,
            stocks_id: this.stocks_id,
            quantity: this.quantity,
            title: this.title,
            description: this.description,
            price: this.price,
            zip_code: this.zip_code,
            location: this.location,
            district_area: this.district_area,
            delivery_type_id: this.delivery_type_id,
            item_pictures: this.item_pictures
          };
          console.log(JSON.stringify(body));
          this.postPvdr.postData(body, 'post_item.php').subscribe(function (data) {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this3, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var navigationExtras;
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      if (data.success) {
                        // this.router.navigate(['itemposted']);
                        console.log("item_id from db:" + data.item_id_redsult);
                        navigationExtras = {
                          queryParams: {
                            item_id: data.item_id_redsult
                          }
                        };
                        this.navCtrl.navigateForward(['itemposted'], navigationExtras);
                      }

                    case 1:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          });
        }
      }]);

      return ShareitemPage;
    }();

    ShareitemPage.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }, {
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"]
      }, {
        type: _providers_credential_provider__WEBPACK_IMPORTED_MODULE_5__["PostProvider"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"]
      }];
    };

    ShareitemPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-shareitem',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./shareitem.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/shareitem/shareitem.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./shareitem.page.scss */
      "./src/app/shareitem/shareitem.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"], _providers_credential_provider__WEBPACK_IMPORTED_MODULE_5__["PostProvider"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"]])], ShareitemPage);
    /***/
  }
}]);
//# sourceMappingURL=shareitem-shareitem-module-es5.js.map