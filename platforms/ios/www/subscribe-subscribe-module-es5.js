function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["subscribe-subscribe-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/subscribe/subscribe.page.html":
  /*!*************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/subscribe/subscribe.page.html ***!
    \*************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppSubscribeSubscribePageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header mode=\"ios\" no-border>\n  <ion-toolbar mode=\"ios\">\n      <ion-buttons slot=\"secondary\" (click)=\"goBack()\">\n        <ion-button mode=\"ios\" style=\"font-size: 15px;\">\n            Back\n        </ion-button>\n      </ion-buttons>\n\n      <ion-title color=\"secondary\">Subscribe</ion-title>\n    </ion-toolbar>\n</ion-header>\n<ion-content mode=\"ios\">\n\n  <ion-card mode=\"ios\" no-padding style=\"box-shadow: none;\"> \n    <ion-card-header text-left mode=\"ios\" no-padding>\n      <ion-card-subtitle color=\"medium\" style=\"padding-bottom: 0.5%;\"><b>Go Premium</b></ion-card-subtitle>\n      <ion-card-title style=\"font-weight: bolder;\">Choose a Greenthumb Package</ion-card-title>\n    </ion-card-header><br>\n    <ion-card-content mode=\"ios\" no-padding>\n      <ion-row>\n        <ion-col no-padding>\n          <ion-list mode=\"ios\">\n            <ion-item mode=\"ios\" lines=\"none\" style=\"font-size: 13px;\">\n              <ion-icon class=\"item_icon\" name=\"checkmark\" color=\"primary\" slot=\"start\" mode=\"ios\"></ion-icon>\n              <ion-label class=\"item_package\" mode=\"ios\">LOREM</ion-label>\n            </ion-item>\n            <ion-item mode=\"ios\" lines=\"none\" class=\"item_sec\">\n              <ion-icon class=\"item_icon\" name=\"checkmark\" color=\"primary\" slot=\"start\" mode=\"ios\"></ion-icon>\n              <ion-label class=\"item_label\" mode=\"ios\">Sed ut perspiciatis</ion-label>\n            </ion-item>\n            <ion-item mode=\"ios\" lines=\"none\" class=\"item_sec\">\n              <ion-icon class=\"item_icon\" name=\"checkmark\" color=\"primary\" slot=\"start\" mode=\"ios\"></ion-icon>\n              <ion-label class=\"item_label\" mode=\"ios\">Sed ut perspiciatis</ion-label>\n            </ion-item>\n            <ion-item mode=\"ios\" lines=\"none\" class=\"item_sec\">\n              <ion-icon class=\"item_icon\" name=\"checkmark\" color=\"primary\" slot=\"start\" mode=\"ios\"></ion-icon>\n              <ion-label class=\"item_label\" mode=\"ios\">Sed ut perspiciatis</ion-label>\n            </ion-item>\n            <ion-item mode=\"ios\" lines=\"none\" class=\"item_sec\">\n              <ion-icon class=\"item_icon\" name=\"checkmark\" color=\"primary\" slot=\"start\" mode=\"ios\"></ion-icon>\n              <ion-label class=\"item_label\" mode=\"ios\">Sed ut perspiciatis</ion-label>\n            </ion-item>\n            <ion-item mode=\"ios\" lines=\"none\" class=\"item_sec\">\n              <ion-icon class=\"item_icon\" name=\"checkmark\" color=\"primary\" slot=\"start\" mode=\"ios\"></ion-icon>\n              <ion-label class=\"item_label\" mode=\"ios\">Sed ut perspiciatis</ion-label>\n            </ion-item>\n          </ion-list>\n        </ion-col>\n        <ion-col no-padding></ion-col>\n      </ion-row>\n    </ion-card-content>\n  </ion-card>\n\n      <ion-row padding  text-center style=\"padding-bottom: 0;\">\n          <ion-col>\n              <ion-button *ngIf=\"!withPackage\" (click)=\"verificationSubscription()\" expand=\"block\" mode=\"ios\">SUBSCRIBE</ion-button>\n              <ion-button *ngIf=\"withPackage\" (click)=\"verifyCancel()\" color=\"danger\" expand=\"block\" mode=\"ios\">CANCEL SUBSCRIPTION</ion-button>\n              \n            </ion-col>\n      </ion-row>\n\n      <ion-row padding text-center style=\"padding-top: 0;\">\n        <ion-col>\n          <h5 style=\"font-weight: bold;\">Lorem Title here</h5>\n          <p text-center style=\"color: #737373;font-size: 13px;padding-left: 20px;padding-right: 20px;\">\n            At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum \n            deleniti atque corrupti\n          </p>\n        </ion-col>\n    </ion-row>\n  \n\n</ion-content>\n";
    /***/
  },

  /***/
  "./src/app/subscribe/subscribe-routing.module.ts":
  /*!*******************************************************!*\
    !*** ./src/app/subscribe/subscribe-routing.module.ts ***!
    \*******************************************************/

  /*! exports provided: SubscribePageRoutingModule */

  /***/
  function srcAppSubscribeSubscribeRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SubscribePageRoutingModule", function () {
      return SubscribePageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _subscribe_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./subscribe.page */
    "./src/app/subscribe/subscribe.page.ts");

    var routes = [{
      path: '',
      component: _subscribe_page__WEBPACK_IMPORTED_MODULE_3__["SubscribePage"]
    }];

    var SubscribePageRoutingModule = function SubscribePageRoutingModule() {
      _classCallCheck(this, SubscribePageRoutingModule);
    };

    SubscribePageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], SubscribePageRoutingModule);
    /***/
  },

  /***/
  "./src/app/subscribe/subscribe.module.ts":
  /*!***********************************************!*\
    !*** ./src/app/subscribe/subscribe.module.ts ***!
    \***********************************************/

  /*! exports provided: SubscribePageModule */

  /***/
  function srcAppSubscribeSubscribeModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SubscribePageModule", function () {
      return SubscribePageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _subscribe_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./subscribe-routing.module */
    "./src/app/subscribe/subscribe-routing.module.ts");
    /* harmony import */


    var _subscribe_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./subscribe.page */
    "./src/app/subscribe/subscribe.page.ts");

    var SubscribePageModule = function SubscribePageModule() {
      _classCallCheck(this, SubscribePageModule);
    };

    SubscribePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _subscribe_routing_module__WEBPACK_IMPORTED_MODULE_5__["SubscribePageRoutingModule"]],
      declarations: [_subscribe_page__WEBPACK_IMPORTED_MODULE_6__["SubscribePage"]]
    })], SubscribePageModule);
    /***/
  },

  /***/
  "./src/app/subscribe/subscribe.page.scss":
  /*!***********************************************!*\
    !*** ./src/app/subscribe/subscribe.page.scss ***!
    \***********************************************/

  /*! exports provided: default */

  /***/
  function srcAppSubscribeSubscribePageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".lvlicon {\n  width: 20px !important;\n  height: 20px;\n  margin: auto;\n  margin-bottom: -2%;\n  margin-right: 3%;\n}\n\n.lvlnum {\n  margin: 0;\n  margin-top: 4%;\n  font-family: Verdana, Geneva, Tahoma, sans-serif;\n  font-size: 18px;\n}\n\nion-item {\n  --padding-start: 0% !important;\n  --inner-padding-end: 0;\n}\n\n#radio_uncheck {\n  --background: white;\n  width: 100%;\n  margin: auto;\n  --background: var(--ion-item-background,transparent);\n  border-radius: 8px;\n  transform: translateZ(0);\n  transition: transform 0.5s cubic-bezier(0.12, 0.72, 0.29, 1);\n  font-size: 14px;\n  box-shadow: 0 4px 16px rgba(0, 0, 0, 0.12);\n  color: black;\n}\n\n.item-radio-checked {\n  --background: linear-gradient(to top left, #1dc1e6 0%, #1dc1e696 95%) !important;\n  width: 100%;\n  margin: auto;\n  --background: var(--ion-item-background,transparent);\n  border-radius: 8px;\n  transform: translateZ(0);\n  transition: transform 0.5s cubic-bezier(0.12, 0.72, 0.29, 1);\n  font-size: 14px;\n  box-shadow: 0 4px 16px rgba(0, 0, 0, 0.12);\n  color: white !important;\n}\n\n.item_icon {\n  margin-bottom: 0;\n  margin-right: 5px;\n}\n\n.item_package {\n  margin-bottom: 0;\n  font-weight: bolder;\n  text-transform: uppercase;\n  font-size: 16px;\n}\n\n.item_label {\n  margin-bottom: 0;\n  text-transform: capitalize;\n}\n\n.item_sec {\n  font-size: 13px;\n  margin-top: -8%;\n  --background: transparent;\n}\n\n.bk_treasure {\n  --background: none;\n  background-image: url(\"/assets/icon/treasure.png\");\n  background-position: center center;\n  background-repeat: no-repeat;\n  background-size: cover;\n}\n\n.item-block {\n  --background:linear-gradient(to top left, #1dc1e6 0%, #1dc1e696 95%) !important;\n  width: 100%;\n  margin: auto;\n  --background: var(--ion-item-background,transparent);\n  border-radius: 8px;\n  transform: translateZ(0);\n  transition: transform 0.5s cubic-bezier(0.12, 0.72, 0.29, 1);\n  font-size: 14px;\n  box-shadow: 0 4px 16px rgba(0, 0, 0, 0.12);\n  color: green;\n}\n\n.prename {\n  margin-top: 8%;\n  font-size: 12px;\n  font-weight: lighter !important;\n  text-transform: uppercase;\n}\n\n.pretext {\n  font-size: 12px;\n  margin-top: 1%;\n  font-weight: lighter !important;\n}\n\n.item_radio {\n  float: right;\n  margin-top: -5%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZW5kcmljay9EZXNrdG9wL1RlbG1vIFNvbHV0aW9ucy9Gb3JQcm9kdWN0aW9uL21heTEwMjAyMS9sYXN0IHNldHVwL2dyZWVudGh1bWJhcHAgY29weS9zcmMvYXBwL3N1YnNjcmliZS9zdWJzY3JpYmUucGFnZS5zY3NzIiwic3JjL2FwcC9zdWJzY3JpYmUvc3Vic2NyaWJlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHNCQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0FDQ0o7O0FERUk7RUFDRSxTQUFBO0VBQ0EsY0FBQTtFQUNBLGdEQUFBO0VBRUEsZUFBQTtBQ0FOOztBREdJO0VBQ0UsOEJBQUE7RUFDQSxzQkFBQTtBQ0FOOztBREdJO0VBQ0UsbUJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUFpQixvREFBQTtFQUNqQixrQkFBQTtFQUVBLHdCQUFBO0VBR0EsNERBQUE7RUFFQSxlQUFBO0VBRUEsMENBQUE7RUFDQSxZQUFBO0FDQ047O0FEQ0k7RUFDRSxnRkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQWlCLG9EQUFBO0VBQ2pCLGtCQUFBO0VBRUEsd0JBQUE7RUFHQSw0REFBQTtFQUVBLGVBQUE7RUFFQSwwQ0FBQTtFQUNBLHVCQUFBO0FDR047O0FEQUU7RUFDRSxnQkFBQTtFQUNBLGlCQUFBO0FDR0o7O0FEREU7RUFDRSxnQkFBQTtFQUNBLG1CQUFBO0VBQ0EseUJBQUE7RUFDQSxlQUFBO0FDSUo7O0FERkU7RUFDRSxnQkFBQTtFQUNBLDBCQUFBO0FDS0o7O0FESEU7RUFDRSxlQUFBO0VBQ0EsZUFBQTtFQUNBLHlCQUFBO0FDTUo7O0FESkU7RUFDRSxrQkFBQTtFQUNFLGtEQUFBO0VBQ0Esa0NBQUE7RUFDQSw0QkFBQTtFQUNBLHNCQUFBO0FDT047O0FETEU7RUFDRSwrRUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQWlCLG9EQUFBO0VBQ2pCLGtCQUFBO0VBRUEsd0JBQUE7RUFHQSw0REFBQTtFQUVBLGVBQUE7RUFFQSwwQ0FBQTtFQUNBLFlBQUE7QUNTSjs7QURQRTtFQUNFLGNBQUE7RUFDQSxlQUFBO0VBQ0EsK0JBQUE7RUFDQSx5QkFBQTtBQ1VKOztBRFJFO0VBQ0UsZUFBQTtFQUNBLGNBQUE7RUFDQSwrQkFBQTtBQ1dKOztBRFRFO0VBQ0UsWUFBQTtFQUNBLGVBQUE7QUNZSiIsImZpbGUiOiJzcmMvYXBwL3N1YnNjcmliZS9zdWJzY3JpYmUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmx2bGljb257XG4gICAgd2lkdGg6IDIwcHggIWltcG9ydGFudDtcbiAgICBoZWlnaHQ6IDIwcHg7XG4gICAgbWFyZ2luOiBhdXRvO1xuICAgIG1hcmdpbi1ib3R0b206IC0yJTtcbiAgICBtYXJnaW4tcmlnaHQ6IDMlO1xuICAgIC8vIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgfSAgXG4gICAgLmx2bG51bXtcbiAgICAgIG1hcmdpbjogMDtcbiAgICAgIG1hcmdpbi10b3A6IDQlO1xuICAgICAgZm9udC1mYW1pbHk6IFZlcmRhbmEsIEdlbmV2YSwgVGFob21hLCBzYW5zLXNlcmlmO1xuICAgICAgLy8gZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgICAgZm9udC1zaXplOiAxOHB4O1xuICAgICAgXG4gICAgfVxuICAgIGlvbi1pdGVtIHtcbiAgICAgIC0tcGFkZGluZy1zdGFydDogMCUgIWltcG9ydGFudDtcbiAgICAgIC0taW5uZXItcGFkZGluZy1lbmQ6IDA7XG4gICAgICBcbiAgICB9XG4gICAgI3JhZGlvX3VuY2hlY2t7XG4gICAgICAtLWJhY2tncm91bmQ6IHdoaXRlO1xuICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICBtYXJnaW46IGF1dG87ICAgIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWl0ZW0tYmFja2dyb3VuZCx0cmFuc3BhcmVudCk7XG4gICAgICBib3JkZXItcmFkaXVzOiA4cHg7XG4gICAgICAtd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlWigwKTtcbiAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWigwKTtcbiAgICAgIC13ZWJraXQtdHJhbnNpdGlvbjogLXdlYmtpdC10cmFuc2Zvcm0gLjVzIGN1YmljLWJlemllciguMTIsLjcyLC4yOSwxKTtcbiAgICAgIHRyYW5zaXRpb246IC13ZWJraXQtdHJhbnNmb3JtIC41cyBjdWJpYy1iZXppZXIoLjEyLC43MiwuMjksMSk7XG4gICAgICB0cmFuc2l0aW9uOiB0cmFuc2Zvcm0gLjVzIGN1YmljLWJlemllciguMTIsLjcyLC4yOSwxKTtcbiAgICAgIHRyYW5zaXRpb246IHRyYW5zZm9ybSAuNXMgY3ViaWMtYmV6aWVyKC4xMiwuNzIsLjI5LDEpLC13ZWJraXQtdHJhbnNmb3JtIC41cyBjdWJpYy1iZXppZXIoLjEyLC43MiwuMjksMSk7XG4gICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAtd2Via2l0LWJveC1zaGFkb3c6IDAgNHB4IDE2cHggcmdiYSgwLDAsMCwuMTIpO1xuICAgICAgYm94LXNoYWRvdzogMCA0cHggMTZweCByZ2JhKDAsMCwwLC4xMik7XG4gICAgICBjb2xvcjogYmxhY2s7XG4gICAgfVxuICAgIC5pdGVtLXJhZGlvLWNoZWNrZWR7XG4gICAgICAtLWJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCh0byB0b3AgbGVmdCwgIzFkYzFlNiAwJSwgIzFkYzFlNjk2IDk1JSkgIWltcG9ydGFudDtcbiAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgbWFyZ2luOiBhdXRvOyAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1pdGVtLWJhY2tncm91bmQsdHJhbnNwYXJlbnQpO1xuICAgICAgYm9yZGVyLXJhZGl1czogOHB4O1xuICAgICAgLXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZVooMCk7XG4gICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVooMCk7XG4gICAgICAtd2Via2l0LXRyYW5zaXRpb246IC13ZWJraXQtdHJhbnNmb3JtIC41cyBjdWJpYy1iZXppZXIoLjEyLC43MiwuMjksMSk7XG4gICAgICB0cmFuc2l0aW9uOiAtd2Via2l0LXRyYW5zZm9ybSAuNXMgY3ViaWMtYmV6aWVyKC4xMiwuNzIsLjI5LDEpO1xuICAgICAgdHJhbnNpdGlvbjogdHJhbnNmb3JtIC41cyBjdWJpYy1iZXppZXIoLjEyLC43MiwuMjksMSk7XG4gICAgICB0cmFuc2l0aW9uOiB0cmFuc2Zvcm0gLjVzIGN1YmljLWJlemllciguMTIsLjcyLC4yOSwxKSwtd2Via2l0LXRyYW5zZm9ybSAuNXMgY3ViaWMtYmV6aWVyKC4xMiwuNzIsLjI5LDEpO1xuICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDRweCAxNnB4IHJnYmEoMCwwLDAsLjEyKTtcbiAgICAgIGJveC1zaGFkb3c6IDAgNHB4IDE2cHggcmdiYSgwLDAsMCwuMTIpO1xuICAgICAgY29sb3I6IHdoaXRlICFpbXBvcnRhbnQ7XG4gICAgfVxuICBcbiAgLml0ZW1faWNvbntcbiAgICBtYXJnaW4tYm90dG9tOiAwO1xuICAgIG1hcmdpbi1yaWdodDogNXB4O1xuICB9XG4gIC5pdGVtX3BhY2thZ2V7XG4gICAgbWFyZ2luLWJvdHRvbTogMDtcbiAgICBmb250LXdlaWdodDogYm9sZGVyO1xuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgZm9udC1zaXplOiAxNnB4O1xuICB9XG4gIC5pdGVtX2xhYmVse1xuICAgIG1hcmdpbi1ib3R0b206IDA7XG4gICAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG4gIH1cbiAgLml0ZW1fc2Vje1xuICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgICBtYXJnaW4tdG9wOiAtOCU7XG4gICAgLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgfVxuICAuYmtfdHJlYXN1cmV7XG4gICAgLS1iYWNrZ3JvdW5kOiBub25lO1xuICAgICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKCcvYXNzZXRzL2ljb24vdHJlYXN1cmUucG5nJyk7XG4gICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXIgY2VudGVyO1xuICAgICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIH1cbiAgLml0ZW0tYmxvY2t7XG4gICAgLS1iYWNrZ3JvdW5kOmxpbmVhci1ncmFkaWVudCh0byB0b3AgbGVmdCwgIzFkYzFlNiAwJSwgIzFkYzFlNjk2IDk1JSkgIWltcG9ydGFudDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBtYXJnaW46IGF1dG87ICAgIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWl0ZW0tYmFja2dyb3VuZCx0cmFuc3BhcmVudCk7XG4gICAgYm9yZGVyLXJhZGl1czogOHB4O1xuICAgIC13ZWJraXQtdHJhbnNmb3JtOiB0cmFuc2xhdGVaKDApO1xuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWigwKTtcbiAgICAtd2Via2l0LXRyYW5zaXRpb246IC13ZWJraXQtdHJhbnNmb3JtIC41cyBjdWJpYy1iZXppZXIoLjEyLC43MiwuMjksMSk7XG4gICAgdHJhbnNpdGlvbjogLXdlYmtpdC10cmFuc2Zvcm0gLjVzIGN1YmljLWJlemllciguMTIsLjcyLC4yOSwxKTtcbiAgICB0cmFuc2l0aW9uOiB0cmFuc2Zvcm0gLjVzIGN1YmljLWJlemllciguMTIsLjcyLC4yOSwxKTtcbiAgICB0cmFuc2l0aW9uOiB0cmFuc2Zvcm0gLjVzIGN1YmljLWJlemllciguMTIsLjcyLC4yOSwxKSwtd2Via2l0LXRyYW5zZm9ybSAuNXMgY3ViaWMtYmV6aWVyKC4xMiwuNzIsLjI5LDEpO1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAtd2Via2l0LWJveC1zaGFkb3c6IDAgNHB4IDE2cHggcmdiYSgwLDAsMCwuMTIpO1xuICAgIGJveC1zaGFkb3c6IDAgNHB4IDE2cHggcmdiYSgwLDAsMCwuMTIpO1xuICAgIGNvbG9yOiBncmVlbjtcbiAgfVxuICAucHJlbmFtZXtcbiAgICBtYXJnaW4tdG9wOiA4JTtcbiAgICBmb250LXNpemU6IDEycHg7XG4gICAgZm9udC13ZWlnaHQ6IGxpZ2h0ZXIgIWltcG9ydGFudDtcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICB9XG4gIC5wcmV0ZXh0e1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICBtYXJnaW4tdG9wOiAxJTtcbiAgICBmb250LXdlaWdodDogbGlnaHRlciAhaW1wb3J0YW50O1xuICB9XG4gIC5pdGVtX3JhZGlve1xuICAgIGZsb2F0OiByaWdodDtcbiAgICBtYXJnaW4tdG9wOiAtNSU7XG4gIH1cbiAgIiwiLmx2bGljb24ge1xuICB3aWR0aDogMjBweCAhaW1wb3J0YW50O1xuICBoZWlnaHQ6IDIwcHg7XG4gIG1hcmdpbjogYXV0bztcbiAgbWFyZ2luLWJvdHRvbTogLTIlO1xuICBtYXJnaW4tcmlnaHQ6IDMlO1xufVxuXG4ubHZsbnVtIHtcbiAgbWFyZ2luOiAwO1xuICBtYXJnaW4tdG9wOiA0JTtcbiAgZm9udC1mYW1pbHk6IFZlcmRhbmEsIEdlbmV2YSwgVGFob21hLCBzYW5zLXNlcmlmO1xuICBmb250LXNpemU6IDE4cHg7XG59XG5cbmlvbi1pdGVtIHtcbiAgLS1wYWRkaW5nLXN0YXJ0OiAwJSAhaW1wb3J0YW50O1xuICAtLWlubmVyLXBhZGRpbmctZW5kOiAwO1xufVxuXG4jcmFkaW9fdW5jaGVjayB7XG4gIC0tYmFja2dyb3VuZDogd2hpdGU7XG4gIHdpZHRoOiAxMDAlO1xuICBtYXJnaW46IGF1dG87XG4gIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWl0ZW0tYmFja2dyb3VuZCx0cmFuc3BhcmVudCk7XG4gIGJvcmRlci1yYWRpdXM6IDhweDtcbiAgLXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZVooMCk7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlWigwKTtcbiAgLXdlYmtpdC10cmFuc2l0aW9uOiAtd2Via2l0LXRyYW5zZm9ybSAwLjVzIGN1YmljLWJlemllcigwLjEyLCAwLjcyLCAwLjI5LCAxKTtcbiAgdHJhbnNpdGlvbjogLXdlYmtpdC10cmFuc2Zvcm0gMC41cyBjdWJpYy1iZXppZXIoMC4xMiwgMC43MiwgMC4yOSwgMSk7XG4gIHRyYW5zaXRpb246IHRyYW5zZm9ybSAwLjVzIGN1YmljLWJlemllcigwLjEyLCAwLjcyLCAwLjI5LCAxKTtcbiAgdHJhbnNpdGlvbjogdHJhbnNmb3JtIDAuNXMgY3ViaWMtYmV6aWVyKDAuMTIsIDAuNzIsIDAuMjksIDEpLCAtd2Via2l0LXRyYW5zZm9ybSAwLjVzIGN1YmljLWJlemllcigwLjEyLCAwLjcyLCAwLjI5LCAxKTtcbiAgZm9udC1zaXplOiAxNHB4O1xuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgNHB4IDE2cHggcmdiYSgwLCAwLCAwLCAwLjEyKTtcbiAgYm94LXNoYWRvdzogMCA0cHggMTZweCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xuICBjb2xvcjogYmxhY2s7XG59XG5cbi5pdGVtLXJhZGlvLWNoZWNrZWQge1xuICAtLWJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCh0byB0b3AgbGVmdCwgIzFkYzFlNiAwJSwgIzFkYzFlNjk2IDk1JSkgIWltcG9ydGFudDtcbiAgd2lkdGg6IDEwMCU7XG4gIG1hcmdpbjogYXV0bztcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24taXRlbS1iYWNrZ3JvdW5kLHRyYW5zcGFyZW50KTtcbiAgYm9yZGVyLXJhZGl1czogOHB4O1xuICAtd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlWigwKTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGVaKDApO1xuICAtd2Via2l0LXRyYW5zaXRpb246IC13ZWJraXQtdHJhbnNmb3JtIDAuNXMgY3ViaWMtYmV6aWVyKDAuMTIsIDAuNzIsIDAuMjksIDEpO1xuICB0cmFuc2l0aW9uOiAtd2Via2l0LXRyYW5zZm9ybSAwLjVzIGN1YmljLWJlemllcigwLjEyLCAwLjcyLCAwLjI5LCAxKTtcbiAgdHJhbnNpdGlvbjogdHJhbnNmb3JtIDAuNXMgY3ViaWMtYmV6aWVyKDAuMTIsIDAuNzIsIDAuMjksIDEpO1xuICB0cmFuc2l0aW9uOiB0cmFuc2Zvcm0gMC41cyBjdWJpYy1iZXppZXIoMC4xMiwgMC43MiwgMC4yOSwgMSksIC13ZWJraXQtdHJhbnNmb3JtIDAuNXMgY3ViaWMtYmV6aWVyKDAuMTIsIDAuNzIsIDAuMjksIDEpO1xuICBmb250LXNpemU6IDE0cHg7XG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCA0cHggMTZweCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xuICBib3gtc2hhZG93OiAwIDRweCAxNnB4IHJnYmEoMCwgMCwgMCwgMC4xMik7XG4gIGNvbG9yOiB3aGl0ZSAhaW1wb3J0YW50O1xufVxuXG4uaXRlbV9pY29uIHtcbiAgbWFyZ2luLWJvdHRvbTogMDtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XG59XG5cbi5pdGVtX3BhY2thZ2Uge1xuICBtYXJnaW4tYm90dG9tOiAwO1xuICBmb250LXdlaWdodDogYm9sZGVyO1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBmb250LXNpemU6IDE2cHg7XG59XG5cbi5pdGVtX2xhYmVsIHtcbiAgbWFyZ2luLWJvdHRvbTogMDtcbiAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG59XG5cbi5pdGVtX3NlYyB7XG4gIGZvbnQtc2l6ZTogMTNweDtcbiAgbWFyZ2luLXRvcDogLTglO1xuICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xufVxuXG4uYmtfdHJlYXN1cmUge1xuICAtLWJhY2tncm91bmQ6IG5vbmU7XG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybChcIi9hc3NldHMvaWNvbi90cmVhc3VyZS5wbmdcIik7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlciBjZW50ZXI7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG59XG5cbi5pdGVtLWJsb2NrIHtcbiAgLS1iYWNrZ3JvdW5kOmxpbmVhci1ncmFkaWVudCh0byB0b3AgbGVmdCwgIzFkYzFlNiAwJSwgIzFkYzFlNjk2IDk1JSkgIWltcG9ydGFudDtcbiAgd2lkdGg6IDEwMCU7XG4gIG1hcmdpbjogYXV0bztcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24taXRlbS1iYWNrZ3JvdW5kLHRyYW5zcGFyZW50KTtcbiAgYm9yZGVyLXJhZGl1czogOHB4O1xuICAtd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlWigwKTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGVaKDApO1xuICAtd2Via2l0LXRyYW5zaXRpb246IC13ZWJraXQtdHJhbnNmb3JtIDAuNXMgY3ViaWMtYmV6aWVyKDAuMTIsIDAuNzIsIDAuMjksIDEpO1xuICB0cmFuc2l0aW9uOiAtd2Via2l0LXRyYW5zZm9ybSAwLjVzIGN1YmljLWJlemllcigwLjEyLCAwLjcyLCAwLjI5LCAxKTtcbiAgdHJhbnNpdGlvbjogdHJhbnNmb3JtIDAuNXMgY3ViaWMtYmV6aWVyKDAuMTIsIDAuNzIsIDAuMjksIDEpO1xuICB0cmFuc2l0aW9uOiB0cmFuc2Zvcm0gMC41cyBjdWJpYy1iZXppZXIoMC4xMiwgMC43MiwgMC4yOSwgMSksIC13ZWJraXQtdHJhbnNmb3JtIDAuNXMgY3ViaWMtYmV6aWVyKDAuMTIsIDAuNzIsIDAuMjksIDEpO1xuICBmb250LXNpemU6IDE0cHg7XG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCA0cHggMTZweCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xuICBib3gtc2hhZG93OiAwIDRweCAxNnB4IHJnYmEoMCwgMCwgMCwgMC4xMik7XG4gIGNvbG9yOiBncmVlbjtcbn1cblxuLnByZW5hbWUge1xuICBtYXJnaW4tdG9wOiA4JTtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBmb250LXdlaWdodDogbGlnaHRlciAhaW1wb3J0YW50O1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xufVxuXG4ucHJldGV4dCB7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgbWFyZ2luLXRvcDogMSU7XG4gIGZvbnQtd2VpZ2h0OiBsaWdodGVyICFpbXBvcnRhbnQ7XG59XG5cbi5pdGVtX3JhZGlvIHtcbiAgZmxvYXQ6IHJpZ2h0O1xuICBtYXJnaW4tdG9wOiAtNSU7XG59Il19 */";
    /***/
  },

  /***/
  "./src/app/subscribe/subscribe.page.ts":
  /*!*********************************************!*\
    !*** ./src/app/subscribe/subscribe.page.ts ***!
    \*********************************************/

  /*! exports provided: SubscribePage */

  /***/
  function srcAppSubscribeSubscribePageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SubscribePage", function () {
      return SubscribePage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _providers_credential_provider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../providers/credential-provider */
    "./src/providers/credential-provider.ts");

    var SubscribePage = /*#__PURE__*/function () {
      function SubscribePage(storage, alertCtrl, postPvdr, toastController) {
        _classCallCheck(this, SubscribePage);

        this.storage = storage;
        this.alertCtrl = alertCtrl;
        this.postPvdr = postPvdr;
        this.toastController = toastController;
        this.withPackage = false;
        this.login_user_id = "";
      }

      _createClass(SubscribePage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.plotData();
        }
      }, {
        key: "goBack",
        value: function goBack() {
          window.history.back();
        }
      }, {
        key: "plotData",
        value: function plotData() {
          var _this = this;

          this.storage.get("greenthumb_user_id").then(function (user_id) {
            _this.login_user_id = user_id;
            var body2 = {
              action: 'checkSubscription',
              user_id: user_id
            };
            console.log("subscription:" + JSON.stringify(body2));

            _this.postPvdr.postData(body2, 'subscription.php').subscribe(function (data) {
              if (data.success) {
                _this.withPackage = true;
              }
            });
          });
        }
      }, {
        key: "verificationSubscription",
        value: function verificationSubscription() {
          var _this2 = this;

          this.alertCtrl.create({
            header: 'Subscription',
            message: 'Are you sure you want to subscribe?',
            cssClass: 'food',
            buttons: [{
              text: 'CANCEL',
              role: 'cancel',
              handler: function handler() {
                console.log('Cancel verification!');
              }
            }, {
              text: 'YES',
              handler: function handler() {
                var body2 = {
                  action: 'checkSubscription',
                  user_id: _this2.login_user_id
                };
                console.log("subscription:" + JSON.stringify(body2));

                _this2.postPvdr.postData(body2, 'subscription.php').subscribe(function (data) {
                  if (data.success) {
                    //this.withPackage = true;
                    var _body = {
                      action: 'monthlySubscription',
                      user_id: _this2.login_user_id,
                      monthly_subscription: 1
                    };

                    _this2.postPvdr.postData(_body, 'subscription.php').subscribe(function (data) {
                      if (data.success) {
                        _this2.withPackage = true;
                      }
                    });

                    _this2.presentToast("Package Successfully Subscribed.");
                  } else {
                    _this2.iSubscribe();
                  }
                });
              }
            }]
          }).then(function (res) {
            res.present();
          });
        }
      }, {
        key: "iSubscribe",
        value: function iSubscribe() {
          var _this3 = this;

          this.storage.get("greenthumb_user_id").then(function (user_id) {
            var body2 = {
              action: 'subscribePackage',
              user_id: user_id
            };
            console.log("looua:" + JSON.stringify(body2));

            _this3.postPvdr.postData(body2, 'subscription.php').subscribe(function (data) {
              if (data.success) {
                _this3.presentToast("Package Successfully Subscribed.");

                _this3.withPackage = true; //this.monthlySubscription = true;
              }
            });
          });
        }
      }, {
        key: "verifyCancel",
        value: function verifyCancel() {
          var _this4 = this;

          this.alertCtrl.create({
            header: 'Subscription Package',
            message: 'Are you sure you want to cancel Subscription?',
            cssClass: 'food',
            buttons: [{
              text: 'CANCEL',
              role: 'cancel',
              handler: function handler() {
                console.log('Cancel subscription!');
              }
            }, {
              text: 'YES',
              handler: function handler() {
                _this4.cancelSubscription();
              }
            }]
          }).then(function (res) {
            res.present();
          });
        }
      }, {
        key: "cancelSubscription",
        value: function cancelSubscription() {
          var _this5 = this;

          this.storage.get("greenthumb_user_id").then(function (user_id) {
            var body2 = {
              action: 'monthlySubscription',
              user_id: user_id,
              monthly_subscription: 0
            };
            console.log("looua:" + JSON.stringify(body2));

            _this5.postPvdr.postData(body2, 'subscription.php').subscribe(function (data) {
              if (data.success) {
                _this5.withPackage = false;
              }
            });
          });
        }
      }, {
        key: "presentToast",
        value: function presentToast(m) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
            var toast;
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    _context.next = 2;
                    return this.toastController.create({
                      message: m,
                      duration: 3000
                    });

                  case 2:
                    toast = _context.sent;
                    toast.present();

                  case 4:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        }
      }]);

      return SubscribePage;
    }();

    SubscribePage.ctorParameters = function () {
      return [{
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_2__["Storage"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"]
      }, {
        type: _providers_credential_provider__WEBPACK_IMPORTED_MODULE_4__["PostProvider"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"]
      }];
    };

    SubscribePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-subscribe',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./subscribe.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/subscribe/subscribe.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./subscribe.page.scss */
      "./src/app/subscribe/subscribe.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_storage__WEBPACK_IMPORTED_MODULE_2__["Storage"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"], _providers_credential_provider__WEBPACK_IMPORTED_MODULE_4__["PostProvider"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"]])], SubscribePage);
    /***/
  }
}]);
//# sourceMappingURL=subscribe-subscribe-module-es5.js.map