function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["systemnotif-systemnotif-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/systemnotif/systemnotif.page.html":
  /*!*****************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/systemnotif/systemnotif.page.html ***!
    \*****************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppSystemnotifSystemnotifPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header mode=\"ios\">\n  <ion-toolbar mode=\"ios\">\n      <ion-buttons slot=\"secondary\" (click)=\"goBack()\" >\n        <ion-button  mode=\"ios\">\n            <ion-icon name=\"arrow-back\"  mode=\"ios\"></ion-icon>\n        </ion-button>\n      </ion-buttons>\n\n      <ion-title mode=\"ios\">Brixy Notification</ion-title>\n\n\n    </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n  <ion-grid>\n    <ion-row >\n      <ion-col>\n        <ion-card mode=\"ios\" *ngFor=\"let notif of systemNotif\" no-margin text-left>\n          <ion-card-header mode=\"ios\" class=\"card_header\">\n            <ion-card-title mode=\"ios\" class=\"card_title\">Brixy Notification</ion-card-title>\n          </ion-card-header>\n          <ion-card-content mode=\"ios\" class=\"card_content\">\n            {{notif.notification}}\n            <br>\n            <div text-right class=\"card_time\">{{notif.date}} {{notif.time}}</div>\n          </ion-card-content>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n\n    </ion-grid>\n\n</ion-content>\n";
    /***/
  },

  /***/
  "./src/app/systemnotif/systemnotif-routing.module.ts":
  /*!***********************************************************!*\
    !*** ./src/app/systemnotif/systemnotif-routing.module.ts ***!
    \***********************************************************/

  /*! exports provided: SystemnotifPageRoutingModule */

  /***/
  function srcAppSystemnotifSystemnotifRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SystemnotifPageRoutingModule", function () {
      return SystemnotifPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _systemnotif_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./systemnotif.page */
    "./src/app/systemnotif/systemnotif.page.ts");

    var routes = [{
      path: '',
      component: _systemnotif_page__WEBPACK_IMPORTED_MODULE_3__["SystemnotifPage"]
    }];

    var SystemnotifPageRoutingModule = function SystemnotifPageRoutingModule() {
      _classCallCheck(this, SystemnotifPageRoutingModule);
    };

    SystemnotifPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], SystemnotifPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/systemnotif/systemnotif.module.ts":
  /*!***************************************************!*\
    !*** ./src/app/systemnotif/systemnotif.module.ts ***!
    \***************************************************/

  /*! exports provided: SystemnotifPageModule */

  /***/
  function srcAppSystemnotifSystemnotifModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SystemnotifPageModule", function () {
      return SystemnotifPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _systemnotif_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./systemnotif-routing.module */
    "./src/app/systemnotif/systemnotif-routing.module.ts");
    /* harmony import */


    var _systemnotif_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./systemnotif.page */
    "./src/app/systemnotif/systemnotif.page.ts");

    var SystemnotifPageModule = function SystemnotifPageModule() {
      _classCallCheck(this, SystemnotifPageModule);
    };

    SystemnotifPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _systemnotif_routing_module__WEBPACK_IMPORTED_MODULE_5__["SystemnotifPageRoutingModule"]],
      declarations: [_systemnotif_page__WEBPACK_IMPORTED_MODULE_6__["SystemnotifPage"]]
    })], SystemnotifPageModule);
    /***/
  },

  /***/
  "./src/app/systemnotif/systemnotif.page.scss":
  /*!***************************************************!*\
    !*** ./src/app/systemnotif/systemnotif.page.scss ***!
    \***************************************************/

  /*! exports provided: default */

  /***/
  function srcAppSystemnotifSystemnotifPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".card_header {\n  padding-bottom: 0;\n}\n\n.card_title {\n  font-size: 15px;\n}\n\n.card_content {\n  font-size: 14px;\n  padding-top: 8px;\n  line-height: 1.1;\n}\n\n.card_time {\n  padding-top: 3%;\n  color: black;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZW5kcmljay9EZXNrdG9wL1RlbG1vIFNvbHV0aW9ucy9Gb3JQcm9kdWN0aW9uL21heTEwMjAyMS9sYXN0IHNldHVwL2dyZWVudGh1bWJhcHAgY29weS9zcmMvYXBwL3N5c3RlbW5vdGlmL3N5c3RlbW5vdGlmLnBhZ2Uuc2NzcyIsInNyYy9hcHAvc3lzdGVtbm90aWYvc3lzdGVtbm90aWYucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksaUJBQUE7QUNDSjs7QURDQTtFQUNJLGVBQUE7QUNFSjs7QURBQTtFQUNJLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0FDR0o7O0FEREE7RUFDSSxlQUFBO0VBQ0EsWUFBQTtBQ0lKIiwiZmlsZSI6InNyYy9hcHAvc3lzdGVtbm90aWYvc3lzdGVtbm90aWYucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNhcmRfaGVhZGVye1xuICAgIHBhZGRpbmctYm90dG9tOiAwO1xufVxuLmNhcmRfdGl0bGV7XG4gICAgZm9udC1zaXplOiAxNXB4O1xufVxuLmNhcmRfY29udGVudHtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgcGFkZGluZy10b3A6IDhweDtcbiAgICBsaW5lLWhlaWdodDogMS4xO1xufVxuLmNhcmRfdGltZXtcbiAgICBwYWRkaW5nLXRvcDogMyU7XG4gICAgY29sb3I6YmxhY2tcbn1cbiIsIi5jYXJkX2hlYWRlciB7XG4gIHBhZGRpbmctYm90dG9tOiAwO1xufVxuXG4uY2FyZF90aXRsZSB7XG4gIGZvbnQtc2l6ZTogMTVweDtcbn1cblxuLmNhcmRfY29udGVudCB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgcGFkZGluZy10b3A6IDhweDtcbiAgbGluZS1oZWlnaHQ6IDEuMTtcbn1cblxuLmNhcmRfdGltZSB7XG4gIHBhZGRpbmctdG9wOiAzJTtcbiAgY29sb3I6IGJsYWNrO1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/systemnotif/systemnotif.page.ts":
  /*!*************************************************!*\
    !*** ./src/app/systemnotif/systemnotif.page.ts ***!
    \*************************************************/

  /*! exports provided: SystemnotifPage */

  /***/
  function srcAppSystemnotifSystemnotifPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SystemnotifPage", function () {
      return SystemnotifPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _shared_model_systemnotif_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../shared/model/systemnotif.model */
    "./src/app/shared/model/systemnotif.model.ts");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var _providers_credential_provider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../providers/credential-provider */
    "./src/providers/credential-provider.ts");

    var SystemnotifPage = /*#__PURE__*/function () {
      function SystemnotifPage(storage, postPvdr) {
        _classCallCheck(this, SystemnotifPage);

        this.storage = storage;
        this.postPvdr = postPvdr;
        this.systemNotif = [];
      }

      _createClass(SystemnotifPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.plotData();
        }
      }, {
        key: "plotData",
        value: function plotData() {
          var _this = this;

          this.storage.get("user_id").then(function (user_id) {
            var body3 = {
              action: 'getMySystemNotifications',
              user_id: user_id
            };

            _this.postPvdr.postData(body3, 'system_notification.php').subscribe(function (data) {
              if (data.success) {
                var notificationList = [];

                for (var key in data.result) {
                  notificationList.push(new _shared_model_systemnotif_model__WEBPACK_IMPORTED_MODULE_2__["SystemNotif"](data.result[key].id, data.result[key].notification_message, data.result[key].date, data.result[key].time, "", ''));
                }

                _this.systemNotif = notificationList;
              }
            });
          });
        }
      }, {
        key: "goBack",
        value: function goBack() {
          window.history.back();
        }
      }]);

      return SystemnotifPage;
    }();

    SystemnotifPage.ctorParameters = function () {
      return [{
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"]
      }, {
        type: _providers_credential_provider__WEBPACK_IMPORTED_MODULE_4__["PostProvider"]
      }];
    };

    SystemnotifPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-systemnotif',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./systemnotif.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/systemnotif/systemnotif.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./systemnotif.page.scss */
      "./src/app/systemnotif/systemnotif.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"], _providers_credential_provider__WEBPACK_IMPORTED_MODULE_4__["PostProvider"]])], SystemnotifPage);
    /***/
  }
}]);
//# sourceMappingURL=systemnotif-systemnotif-module-es5.js.map