(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["tab1-tab1-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/tab1/tab1.page.html":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/tab1/tab1.page.html ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header no-border mode=\"ios\">\n  <ion-toolbar mode=\"ios\">\n    \n      \n      <ion-item id=\"customsearchbartab1\" style=\"--padding-start:0;--inline-padding-end:0;--background:transparent\" lines=\"none\">\n        <ion-input (keyup.enter)=\"searchItem()\" placeholder=\"Search for items\" [(ngModel)]=\"searchQuery\" mode=\"ios\">\n        </ion-input>\n        <ion-icon name=\"search\" slot=\"start\" mode=\"ios\"></ion-icon>\n      </ion-item>\n\n      <ion-buttons  slot=\"primary\" mode=\"ios\" class=\"btnheader\" (click)=\"goLocation()\"> \n        <ion-button mode=\"ios\" style=\"margin-right: -5px;\">\n          <ion-icon src=\"assets/greenthumb-images/location_pin.svg\" size=\"large\"></ion-icon>\n        </ion-button>\n      </ion-buttons>\n\n      <ion-buttons slot=\"primary\" mode=\"ios\" class=\"btnheader\" (click)=\"goOffers()\"> \n        <ion-button mode=\"ios\" style=\"margin-right: 0;\">\n          <ion-icon name=\"basket\" mode=\"md\" size=\"large\"></ion-icon>\n        </ion-button>\n      </ion-buttons>\n    </ion-toolbar>\n</ion-header>\n<ion-content>\n  <ion-refresher slot=\"fixed\" (ionRefresh)=\"doRefresh($event)\">\n    <ion-refresher-content></ion-refresher-content>\n  </ion-refresher>\n  <ion-row *ngIf=\"hideMe\">\n    <ion-col>\n      <ion-card mode=\"ios\" no-margin text-left style=\"box-shadow: none;\">\n        <ion-card-header mode=\"ios\" class=\"card_header\">\n          <ion-row>\n            <ion-col text-left no-padding>\n              <ion-card-title mode=\"ios\" color=\"secondary\" class=\"card_title\">What's New ?</ion-card-title>\n            </ion-col>\n            <ion-col text-right no-padding>\n              <div class=\"card_hide\" (click)=\"hide()\">Hide</div>\n            </ion-col>\n          </ion-row>\n        </ion-card-header>\n        <ion-card-content *ngFor=\"let announce of announcement\"  mode=\"ios\" class=\"card_content\">\n          {{announce.title}}\n          <br>\n          <div class=\"card_txt\">{{announce.announcement}}</div>\n          <div style=\"font-weight: bold;padding-top: 8px;\" class=\"card_hide gotoAnnounce\" \n          (click)=\"allAnnouncements()\">Go Archive</div>\n        </ion-card-content>\n      </ion-card>\n    </ion-col>\n  </ion-row>\n  <div class=\"forpad_ifhide\" style=\"padding-top: 10px\">\n    <ion-slides [options]=\"slideOptsOne\" pager=\"true\" mode=\"ios\" id=\"slide_forpromoss\">\n      <ion-slide *ngFor=\"let slide of ImageArray\" mode=\"ios\">\n        <div class=\"slide\" mode=\"ios\">\n          <ion-card mode=\"ios\" class=\"promocard\">\n            <ion-card-header mode=\"ios\" class=\"promoheader\">\n              <ion-row>\n                <ion-col text-left size=\"5\" no-padding class=\"ion-align-self-center\" style=\"padding-bottom: 15px;\">\n                  <ion-card-subtitle class=\"promo_smalltxt\">{{slide.text}}</ion-card-subtitle>\n                  <ion-card-title class=\"promo_largetxt\">Organic</ion-card-title>\n                  <ion-card-subtitle style=\"margin-top: -3%;\" class=\"promo_smalltxt\">Way of Life</ion-card-subtitle>\n                  <ion-button mode=\"ios\" size=\"small\" class=\"btnshop\">SHOP NOW</ion-button>\n                </ion-col>\n                <ion-col no-padding size=\"7\" text-right>\n                  <img src=\"{{slide.image}}\" style=\"margin: auto;\">\n                </ion-col>\n              </ion-row>\n            </ion-card-header>\n          </ion-card>\n        </div>\n      </ion-slide>\n    </ion-slides>\n    <ion-row padding class=\"for_sortfeed\">\n      <ion-col size=\"6\">\n        <ion-item mode=\"ios\" lines=\"none\" style=\"--padding-start: 0;\">\n          <ion-select text-right expand=\"block\" style=\"max-width: 100%;\"  mode=\"ios\" okText=\"Sort\" cancelText=\"Dismiss\">\n            <ion-label>Sort feed</ion-label>\n            <ion-select-option value=\"Sort feed\" selected>Sort feed</ion-select-option>\n            <ion-select-option value=\"Latest\">Latest</ion-select-option>\n            <ion-select-option value=\"Local Pickup\">Local Pickup</ion-select-option>\n            <ion-select-option value=\"Shipping\">Shipping</ion-select-option>\n            <ion-select-option value=\"Sellers I follow\">Sellers I follow</ion-select-option>\n            <ion-select-option value=\"Closest Location\">Closest Location</ion-select-option>\n          </ion-select>\n        </ion-item>\n      </ion-col>\n      <ion-col size=\"6\" text-right>\n        <ion-item text-right mode=\"ios\" lines=\"none\" style=\"--padding-start: 0;position: absolute;right: 0;\">\n          <ion-select text-right expand=\"block\" style=\"max-width: 100%;\"  mode=\"ios\" okText=\"Sort\" cancelText=\"Dismiss\">\n            <ion-label>Price</ion-label>\n            <ion-select-option value=\"Any\" selected>Price: Any</ion-select-option>\n            <ion-select-option value=\"Lowest to Highest\">Lowest to Highest</ion-select-option>\n            <ion-select-option value=\"Highest to Lowest\">Highest to Lowest</ion-select-option>\n          </ion-select>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n    \n    <!-- List of items -->\n\n    <ion-grid style=\"text-align: center;\" *ngIf=\"loader\"> \n\n        <ion-spinner name=\"crescent\"></ion-spinner>\n    </ion-grid>\n\n    <ion-grid *ngIf=\"!searchedTurn\">\n\n      <!-- for promoted Items-->\n      \n      <ion-row *ngFor=\"let item of itemListPromoted\" >\n        <ion-col>\n          <ion-card mode=\"ios\" class=\"for_items\" >\n            <ion-card-content no-padding>\n              <ion-row>\n                <ion-col text-left size=\"5\">\n                  <ion-item lines=\"none\" class=\"for_itemms\" (click)=\"goSellerprof(item.user_id)\">\n                    <ion-avatar slot=\"start\">\n                     \n                      <div *ngIf=\"!item?.profile_photo\">\n                        <img src=\"assets/greenthumb-images/userpic.png\">\n                      </div>\n                      <div *ngIf=\"item?.profile_photo\">\n                        <img [src] = \"item.profile_photo\" >\n                      </div> \n                    </ion-avatar>\n                    <ion-label class=\"for_name\">{{item.username}}</ion-label>\n                  </ion-item>\n                  <img (click)=\"goProductview(item.id)\" class=\"for_itemmimg\" [src] = \"item.item_cover_photo\">\n                </ion-col>\n                <ion-col text-left size=\"7\">\n                  <ion-row (click)=\"goProductview(item.id)\">\n                    <ion-col no-padding text-left>\n                      <ion-label color=\"danger\">Promoted</ion-label>\n                      <ion-label (click)=\"goProductview(item.id)\" color=\"dark\"><h2 style=\"padding-bottom: 2.5%;\" class=\"for_itemmlocation\">\n                    \n                        <div class=\"for_unitqty\">{{item.quantity}} \n                          <label *ngIf=\"item.stocks_id=='1'\">\n                            <label *ngIf=\"item.quantity>1\">pieces</label>\n                            <label *ngIf=\"item.quantity<=1\">piece</label>\n                          </label>\n                          <label *ngIf=\"item.stocks_id=='2'\">\n                            <label *ngIf=\"item.quantity>1\">pounds</label>\n                            <label *ngIf=\"item.quantity<=1\">pound</label>\n                          </label>\n                          <label *ngIf=\"item.stocks_id=='3'\">\n                            <label *ngIf=\"item.quantity>1\">ounces</label>\n                            <label *ngIf=\"item.quantity<=1\">ounce</label>\n                          </label>\n                          <label *ngIf=\"item.stocks_id=='4'\">\n                            {{item.others_stock}}\n                          </label>\n                        </div>\n                          </h2></ion-label>\n                      <ion-label color=\"secondary\"><h1 class=\"for_pprice\">${{item.price}}</h1></ion-label>\n                    </ion-col>\n                    <ion-col no-padding text-right>\n                      <div class=\"for_category\">{{item.category}}</div>\n                      <div style=\"margin-top: 5px;\">\n                          <ionic4-star-rating #rating\n                            activeIcon = \"ios-star\"\n                            defaultIcon = \"ios-star-outline\"\n                            activeColor = \"#ffce00\" \n                            defaultColor = \"#ffce00\"\n                            readonly=\"false\"\n                            rating=\"{{item.user_rating}}\"\n                            fontSize = \"15px\">\n                          </ionic4-star-rating>\n                      </div>\n                    </ion-col>\n                  </ion-row>\n                  <ion-row style=\"margin-top: -2px;\" (click)=\"goProductview(item.id)\">\n                    <ion-col no-padding text-left>\n                      <ion-label color=\"dark\">\n                        <h2 class=\"for_itemmname\">{{item.title}}</h2></ion-label>\n                    </ion-col>\n                  </ion-row>\n                  <ion-label ><h2 class=\"for_itemmlocation\">\n                    <ion-icon class=\"for_pin\" mode=\"ios\" color=\"primary\" name=\"pin\"></ion-icon>&nbsp;{{item.location}}\n                  </h2></ion-label>\n                  <ion-row>\n                    <ion-col no-padding text-left size=\"9\" class=\"for_borderitem\" (click)=\"goProductview(item.id)\">\n                      <ion-row>\n                        <ion-col text-left no-padding>\n                          <ion-label color=\"secondary\"><h2 class=\"for_itemmbtn\">\n                              &nbsp;{{item.date}}\n                            </h2></ion-label>\n                        </ion-col>\n                        <ion-col text-left no-padding>\n                          <ion-label color=\"secondary\"><h2 class=\"for_itemmbtn\">\n                              &nbsp;{{item.time}}\n                            </h2></ion-label>\n                        </ion-col>\n                      </ion-row>\n                    </ion-col>\n                    <ion-col no-padding text-center size=\"3\" (click)=\"saveThis(item.id)\">\n                      <ion-icon style=\"zoom: 2;margin-top: -5px;\" name=\"heart\" mode=\"ios\"\n                      id=\"class{{item.id}}\" [class]=\"(item.save_status == '1') ? 'myHeartSave' : 'myHeartUnSave'\"></ion-icon>\n                    </ion-col>\n                  </ion-row>\n                </ion-col>\n              </ion-row>\n            </ion-card-content>\n          </ion-card>\n        </ion-col>\n      </ion-row>\n\n      <!---->\n\n      <ion-row *ngFor=\"let item of itemList\" >\n        <ion-col>\n          <ion-card mode=\"ios\" class=\"for_items\" >\n            <ion-card-content no-padding>\n              <ion-row>\n                <ion-col text-left size=\"5\">\n                  <ion-item lines=\"none\" class=\"for_itemms\" (click)=\"goSellerprof(item.user_id)\">\n                    <ion-avatar slot=\"start\">\n                     \n                      <div *ngIf=\"!item?.profile_photo\">\n                        <img src=\"assets/greenthumb-images/userpic.png\">\n                      </div>\n                      <div *ngIf=\"item?.profile_photo\">\n                        <img [src] = \"item.profile_photo\" >\n                      </div> \n                    </ion-avatar>\n                    <ion-label class=\"for_name\">{{item.username}}</ion-label>\n                  </ion-item>\n                  <img (click)=\"goProductview(item.id)\" class=\"for_itemmimg\" [src] = \"item.item_cover_photo\">\n                </ion-col>\n                <ion-col text-left size=\"7\">\n                  <ion-row (click)=\"goProductview(item.id)\">\n                    <ion-col no-padding text-left>\n                      <ion-label (click)=\"goProductview(item.id)\" color=\"dark\"><h2 style=\"padding-bottom: 2.5%;\" class=\"for_itemmlocation\">\n                    \n                        <div class=\"for_unitqty\">{{item.quantity}} \n                          <label *ngIf=\"item.stocks_id=='1'\">\n                            <label *ngIf=\"item.quantity>1\">pieces</label>\n                            <label *ngIf=\"item.quantity<=1\">piece</label>\n                          </label>\n                          <label *ngIf=\"item.stocks_id=='2'\">\n                            <label *ngIf=\"item.quantity>1\">pounds</label>\n                            <label *ngIf=\"item.quantity<=1\">pound</label>\n                          </label>\n                          <label *ngIf=\"item.stocks_id=='3'\">\n                            <label *ngIf=\"item.quantity>1\">ounces</label>\n                            <label *ngIf=\"item.quantity<=1\">ounce</label>\n                          </label>\n                          <label *ngIf=\"item.stocks_id=='4'\">\n                            {{item.others_stock}}\n                          </label>\n                        </div>\n                          </h2></ion-label>\n                      <ion-label color=\"secondary\"><h1 class=\"for_pprice\">${{item.price}}</h1></ion-label>\n                    </ion-col>\n                    <ion-col no-padding text-right>\n                      <div class=\"for_category\">{{item.category}}</div>\n                      <div style=\"margin-top: 5px;\">\n                          <ionic4-star-rating #rating\n                            activeIcon = \"ios-star\"\n                            defaultIcon = \"ios-star-outline\"\n                            activeColor = \"#ffce00\" \n                            defaultColor = \"#ffce00\"\n                            readonly=\"false\"\n                            rating=\"{{item.user_rating}}\"\n                            fontSize = \"15px\">\n                          </ionic4-star-rating>\n                      </div>\n                    </ion-col>\n                  </ion-row>\n                  <ion-row style=\"margin-top: -2px;\" (click)=\"goProductview(item.id)\">\n                    <ion-col no-padding text-left>\n                      <ion-label color=\"dark\">\n                        <h2 class=\"for_itemmname\">{{item.title}}</h2></ion-label>\n                    </ion-col>\n                  </ion-row>\n                  <ion-label ><h2 class=\"for_itemmlocation\">\n                    <ion-icon class=\"for_pin\" mode=\"ios\" color=\"primary\" name=\"pin\"></ion-icon>&nbsp;{{item.location}}\n                  </h2></ion-label>\n                  <ion-row>\n                    <ion-col no-padding text-left size=\"9\" class=\"for_borderitem\" (click)=\"goProductview(item.id)\">\n                      <ion-row>\n                        <ion-col text-left no-padding>\n                          <ion-label color=\"secondary\"><h2 class=\"for_itemmbtn\">\n                              &nbsp;{{item.date}}\n                            </h2></ion-label>\n                        </ion-col>\n                        <ion-col text-left no-padding>\n                          <ion-label color=\"secondary\"><h2 class=\"for_itemmbtn\">\n                              &nbsp;{{item.time}}\n                            </h2></ion-label>\n                        </ion-col>\n                      </ion-row>\n                    </ion-col>\n                    <ion-col no-padding text-center size=\"3\" (click)=\"saveThis(item.id)\">\n                      <ion-icon style=\"zoom: 2;margin-top: -5px;\" name=\"heart\" mode=\"ios\"\n                      id=\"class{{item.id}}\" [class]=\"(item.save_status == '1') ? 'myHeartSave' : 'myHeartUnSave'\"></ion-icon>\n                    </ion-col>\n                  </ion-row>\n                </ion-col>\n              </ion-row>\n            </ion-card-content>\n          </ion-card>\n        </ion-col>\n      </ion-row>\n\n    </ion-grid>\n\n    <!-- List of searched items -->\n    <ion-grid *ngIf=\"searchedTurn\">\n\n      <p *ngIf=\"noResultsFound\" style=\"text-align: center;\">No results found</p>\n\n      <ion-row *ngFor=\"let item of itemListSearched\" >\n        <ion-col>\n          <ion-card mode=\"ios\" class=\"for_items\" >\n            <ion-card-content no-padding>\n              <ion-row>\n                <ion-col text-left size=\"5\">\n                  <ion-item lines=\"none\" class=\"for_itemms\" (click)=\"goSellerprof(item.user_id)\">\n                    <ion-avatar slot=\"start\">\n                      <div *ngIf=\"!item?.profile_photo\">\n                        <img src=\"assets/greenthumb-images/userpic.png\">\n                      </div>\n                      <div *ngIf=\"item?.profile_photo\">\n                        <img [src] = \"item.profile_photo\" >\n                      </div> \n                    </ion-avatar>\n                    <ion-label class=\"for_name\">{{item.username}}</ion-label>\n                  </ion-item>\n                  <img (click)=\"goProductview(item.id)\" class=\"for_itemmimg\" [src] = \"item.item_cover_photo\">\n                </ion-col>\n                <ion-col text-left size=\"7\">\n                  <ion-row (click)=\"goProductview(item.id)\">\n                    <ion-col no-padding text-left>\n                      <ion-label (click)=\"goProductview(item.id)\" color=\"dark\"><h2 style=\"padding-bottom: 2.5%;\" class=\"for_itemmlocation\">\n                  \n                        <div class=\"for_unitqty\">{{item.quantity}} \n                          <label *ngIf=\"item.stocks_id=='1'\">\n                            <label *ngIf=\"item.quantity>1\">pieces</label>\n                            <label *ngIf=\"item.quantity<=1\">piece</label>\n                          </label>\n                          <label *ngIf=\"item.stocks_id=='2'\">\n                            <label *ngIf=\"item.quantity>1\">pounds</label>\n                            <label *ngIf=\"item.quantity<=1\">pound</label>\n                          </label>\n                          <label *ngIf=\"item.stocks_id=='3'\">\n                            <label *ngIf=\"item.quantity>1\">ounces</label>\n                            <label *ngIf=\"item.quantity<=1\">ounce</label>\n                          </label>\n                          <label *ngIf=\"item.stocks_id=='4'\">\n                            {{item.others_stock}}\n                          </label>\n                        </div>\n                      </h2></ion-label>\n                      <ion-label color=\"secondary\"><h1 class=\"for_pprice\">${{item.price}}</h1></ion-label>\n                    </ion-col>\n                    <ion-col no-padding text-right>\n                      <div class=\"for_category\">{{item.category}}</div>\n                      <div style=\"margin-top: 5px;\">\n                        <ionic4-star-rating #rating\n                          activeIcon = \"ios-star\"\n                          defaultIcon = \"ios-star-outline\"\n                          activeColor = \"#ffce00\" \n                          defaultColor = \"#ffce00\"\n                          readonly=\"false\"\n                          rating=\"{{item.user_rating}}\"\n                          fontSize = \"15px\">\n                        </ionic4-star-rating>\n                    </div>\n                    </ion-col>\n                  </ion-row>\n                  <ion-row style=\"margin-top: -2px;\" (click)=\"goProductview(item.id)\">\n                    <ion-col no-padding text-left>\n                      <ion-label color=\"dark\">\n                        <h2 class=\"for_itemmname\">{{item.title}}</h2></ion-label>\n                    </ion-col>\n                  </ion-row>\n                  <ion-label ><h2 class=\"for_itemmlocation\">\n                    <ion-icon class=\"for_pin\" mode=\"ios\" color=\"primary\" name=\"pin\"></ion-icon>&nbsp;{{item.location}}\n                  </h2></ion-label>\n                  <ion-row>\n                    <ion-col no-padding text-left size=\"9\" class=\"for_borderitem\" (click)=\"goProductview(item.id)\">\n                      <ion-row>\n                        <ion-col text-left no-padding>\n                          <ion-label color=\"secondary\"><h2 class=\"for_itemmbtn\">\n                              &nbsp;{{item.date}}\n                            </h2></ion-label>\n                        </ion-col>\n                        <ion-col text-left no-padding>\n                          <ion-label color=\"secondary\"><h2 class=\"for_itemmbtn\">\n                              &nbsp;{{item.time}}\n                            </h2></ion-label>\n                        </ion-col>\n                      </ion-row>\n                    </ion-col>\n                    <ion-col no-padding text-center size=\"3\" (click)=\"saveThis(item.id)\">\n                      <ion-icon style=\"zoom: 2;margin-top: -5px;\" name=\"heart\" mode=\"ios\"\n                      id=\"class{{item.id}}\" [class]=\"(item.save_status == '1') ? 'myHeartSave' : 'myHeartUnSave'\"></ion-icon>\n                    </ion-col>\n                  </ion-row>\n                </ion-col>\n              </ion-row>\n            </ion-card-content>\n          </ion-card>\n        </ion-col>\n      </ion-row>\n\n    </ion-grid>\n\n    \n\n  <br></div>\n</ion-content>");

/***/ }),

/***/ "./src/app/tab1/tab1.module.ts":
/*!*************************************!*\
  !*** ./src/app/tab1/tab1.module.ts ***!
  \*************************************/
/*! exports provided: Tab1PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab1PageModule", function() { return Tab1PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _tab1_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./tab1.page */ "./src/app/tab1/tab1.page.ts");
/* harmony import */ var _shared_header_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../shared/header.module */ "./src/app/shared/header.module.ts");
/* harmony import */ var ionic4_star_rating__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ionic4-star-rating */ "./node_modules/ionic4-star-rating/dist/index.js");









const routes = [
    {
        path: '',
        component: _tab1_page__WEBPACK_IMPORTED_MODULE_6__["Tab1Page"],
    }
];
let Tab1PageModule = class Tab1PageModule {
};
Tab1PageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            ionic4_star_rating__WEBPACK_IMPORTED_MODULE_8__["StarRatingModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
            _shared_header_module__WEBPACK_IMPORTED_MODULE_7__["HeaderModule"]
        ],
        declarations: [_tab1_page__WEBPACK_IMPORTED_MODULE_6__["Tab1Page"]]
    })
], Tab1PageModule);



/***/ }),

/***/ "./src/app/tab1/tab1.page.scss":
/*!*************************************!*\
  !*** ./src/app/tab1/tab1.page.scss ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".btnheader {\n  margin-top: 0.5%;\n}\n\n.card_header {\n  padding-bottom: 0;\n  padding-top: 7px;\n}\n\n.card_title {\n  font-size: 15px;\n  text-transform: uppercase;\n}\n\n.card_content {\n  font-size: 13px;\n  padding-top: 8px;\n  padding-bottom: 0px;\n  line-height: 1.1;\n  color: black;\n}\n\n.card_txt {\n  padding-top: 1.2%;\n  color: #679733;\n  font-size: 12px;\n}\n\n.card_hide {\n  color: #679733;\n  font-size: 13px;\n}\n\n.gotoAnnounce {\n  text-align: right;\n}\n\n.promocard {\n  margin-top: 0;\n  background: #e2f0cb;\n  box-shadow: none;\n  border-radius: 0;\n}\n\n.promoheader {\n  padding-top: 10px;\n  padding-bottom: 0px;\n  padding-right: 13px;\n  padding-left: 17px;\n}\n\n.promo_smalltxt {\n  text-transform: uppercase;\n  color: #679733;\n  font-size: 13px;\n  font-weight: bold;\n}\n\n.promo_largetxt {\n  text-transform: uppercase;\n  color: black;\n  font-size: 27px;\n  font-weight: bolder;\n  margin-top: -4.2%;\n}\n\n.btnshop {\n  font-size: 11px;\n  margin-top: 10%;\n  --border-radius: 3px;\n}\n\n.myHeartUnfollow {\n  color: #dfd5d5;\n}\n\n.myHeartFollow {\n  color: red;\n}\n\n.myHeartUnSave {\n  color: #d3dbc9;\n}\n\n.myHeartSave {\n  color: red;\n}\n\nion-select {\n  font-size: 13px;\n}\n\n.for_sortfeed {\n  padding-left: 0;\n  padding-top: 0;\n  margin-top: -5%;\n  border-bottom: 1px solid #e2f0cb;\n  padding-bottom: 0;\n  margin-bottom: 3%;\n}\n\n.for_items {\n  margin-top: 0%;\n  box-shadow: 0 1px 10px #e2f0cb;\n  border-radius: 10px;\n  border: 1px solid #e2f0cb;\n  margin-bottom: 0;\n}\n\n.for_chip {\n  --background: transparent;\n  margin: 0;\n  padding-right: 0;\n  padding-left: 15px;\n}\n\n.for_itemimg {\n  margin: auto;\n  width: 90%;\n  margin-top: -15px;\n  padding-bottom: 5px;\n}\n\n.for_price {\n  font-size: 25px;\n  font-weight: bolder;\n}\n\n.for_itemname {\n  font-size: 14px;\n  font-weight: bolder;\n}\n\n.for_itemcateg {\n  text-transform: uppercase;\n  font-size: 12px;\n  font-weight: lighter;\n}\n\n.for_itemlocation {\n  font-size: 13px;\n  font-weight: lighter;\n}\n\n.for_spanstar {\n  font-size: 14px;\n  font-weight: normal;\n}\n\n.for_staricon {\n  font-size: 18px;\n  margin-bottom: -4px;\n}\n\n.for_pin {\n  font-size: 15px;\n  margin-bottom: -1%;\n  margin-left: -1%;\n}\n\n.for_itembtn {\n  font-size: 12px;\n  color: #679733;\n}\n\n.for_itembtnicon {\n  font-size: 20px;\n  margin-bottom: -7%;\n  margin-right: 2%;\n}\n\n.my-custom-class .alert-wrapper {\n  background: #e5e5e5;\n}\n\n.for_itemms {\n  --padding-start: 0;\n  margin-top: -8px;\n  --inner-padding-end: 0;\n  --background: transparent;\n}\n\n.for_itemms ion-avatar {\n  width: 100% !important;\n  height: 100% !important;\n  max-width: 25px !important;\n  max-height: 25px !important;\n  margin-right: 5px;\n  margin-top: 0;\n}\n\n.for_itemms ion-avatar img {\n  border: 1px solid rgba(0, 0, 0, 0.12);\n  width: 100% !important;\n  height: 100% !important;\n  max-width: 25px !important;\n  max-height: 25px !important;\n  margin-right: 5px;\n  margin-top: 0;\n  border-radius: 50%;\n}\n\n.for_name {\n  font-size: 12px;\n  text-transform: capitalize;\n  margin-top: 0;\n  color: black;\n}\n\n.for_itemmimg {\n  margin: auto;\n  width: 90%;\n  margin-top: -10px;\n  padding-bottom: 5px;\n}\n\n.for_pprice {\n  font-size: 23px;\n  font-weight: bolder;\n}\n\n.for_category {\n  background: #76c961;\n  padding: 3px;\n  border-top-right-radius: 10px;\n  margin-top: -5px;\n  margin-right: -5px;\n  border-bottom-left-radius: 10px;\n  text-transform: capitalize;\n  font-size: 12px;\n  font-weight: lighter;\n  color: white;\n  text-align: center;\n}\n\n.for_itemmname {\n  font-size: 15px;\n  font-weight: bolder;\n}\n\n.for_itemmlocation {\n  font-size: 12.5px;\n  font-weight: lighter;\n  color: #989aa2;\n  padding-right: 5px;\n  padding-bottom: 2.5%;\n}\n\n.for_borderitem {\n  border-top: 1px solid #e2f0cb;\n  padding-top: 2.5%;\n}\n\n.for_itemmbtn {\n  font-size: 11px;\n  color: #679733;\n}\n\n.for_unitqty {\n  font-size: 11px;\n  text-align: left;\n  color: #989aa2;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZW5kcmljay9EZXNrdG9wL1RlbG1vIFNvbHV0aW9ucy9Gb3JQcm9kdWN0aW9uL21heTEwMjAyMS9sYXN0IHNldHVwL2dyZWVudGh1bWJhcHAgY29weS9zcmMvYXBwL3RhYjEvdGFiMS5wYWdlLnNjc3MiLCJzcmMvYXBwL3RhYjEvdGFiMS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxnQkFBQTtBQ0NKOztBRENBO0VBQ0ksaUJBQUE7RUFDQSxnQkFBQTtBQ0VKOztBREFBO0VBQ0ksZUFBQTtFQUNBLHlCQUFBO0FDR0o7O0FEREE7RUFDSSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsWUFBQTtBQ0lKOztBREZBO0VBQ0ksaUJBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtBQ0tKOztBREhBO0VBQ0ksY0FBQTtFQUNBLGVBQUE7QUNNSjs7QURKQTtFQUNJLGlCQUFBO0FDT0o7O0FETEE7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0FDUUo7O0FETkE7RUFDSSxpQkFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtBQ1NKOztBRFBBO0VBQ0kseUJBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0FDVUo7O0FEUkE7RUFDSSx5QkFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0EsbUJBQUE7RUFDQSxpQkFBQTtBQ1dKOztBRFRBO0VBQ0ksZUFBQTtFQUNBLGVBQUE7RUFDQSxvQkFBQTtBQ1lKOztBRFZBO0VBQ0ksY0FBQTtBQ2FKOztBRFhBO0VBQ0ksVUFBQTtBQ2NKOztBRFhBO0VBQ0ksY0FBQTtBQ2NKOztBRFpBO0VBQ0ksVUFBQTtBQ2VKOztBRFhBO0VBQ0ksZUFBQTtBQ2NKOztBRFpBO0VBQ0ksZUFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0VBQ0EsZ0NBQUE7RUFDQSxpQkFBQTtFQUNBLGlCQUFBO0FDZUo7O0FEYkE7RUFDSSxjQUFBO0VBQ0EsOEJBQUE7RUFDQSxtQkFBQTtFQUNBLHlCQUFBO0VBQ0EsZ0JBQUE7QUNnQko7O0FEZEE7RUFDSSx5QkFBQTtFQUNBLFNBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0FDaUJKOztBRGZBO0VBQ0ksWUFBQTtFQUNBLFVBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0FDa0JKOztBRGhCQTtFQUNJLGVBQUE7RUFDQSxtQkFBQTtBQ21CSjs7QURoQkE7RUFDSSxlQUFBO0VBQ0EsbUJBQUE7QUNtQko7O0FEakJBO0VBQ0kseUJBQUE7RUFDQSxlQUFBO0VBQ0Esb0JBQUE7QUNvQko7O0FEbEJBO0VBQ0ksZUFBQTtFQUNBLG9CQUFBO0FDcUJKOztBRG5CQTtFQUNJLGVBQUE7RUFBZ0IsbUJBQUE7QUN1QnBCOztBRHJCRTtFQUNFLGVBQUE7RUFDQSxtQkFBQTtBQ3dCSjs7QUR0QkE7RUFDSSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtBQ3lCSjs7QUR2QkE7RUFDSSxlQUFBO0VBQ0EsY0FBQTtBQzBCSjs7QUR4QkE7RUFDSSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtBQzJCSjs7QUR6QkE7RUFDSSxtQkFBQTtBQzRCSjs7QUR4QkE7RUFDSSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0Esc0JBQUE7RUFDQSx5QkFBQTtBQzJCSjs7QUQxQkk7RUFDSSxzQkFBQTtFQUNBLHVCQUFBO0VBQ0EsMEJBQUE7RUFDQSwyQkFBQTtFQUNBLGlCQUFBO0VBQWtCLGFBQUE7QUM2QjFCOztBRDVCUTtFQUNJLHFDQUFBO0VBQ0Esc0JBQUE7RUFDQSx1QkFBQTtFQUNBLDBCQUFBO0VBQ0EsMkJBQUE7RUFDQSxpQkFBQTtFQUFrQixhQUFBO0VBQ2xCLGtCQUFBO0FDK0JaOztBRDFCQTtFQUNJLGVBQUE7RUFDQSwwQkFBQTtFQUNBLGFBQUE7RUFDQSxZQUFBO0FDNkJKOztBRDNCQTtFQUNJLFlBQUE7RUFDQSxVQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtBQzhCSjs7QUQ1QkE7RUFDSSxlQUFBO0VBQ0EsbUJBQUE7QUMrQko7O0FEN0JBO0VBQ0ksbUJBQUE7RUFDQSxZQUFBO0VBQ0EsNkJBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsK0JBQUE7RUFDQSwwQkFBQTtFQUNBLGVBQUE7RUFDQSxvQkFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtBQ2dDSjs7QUQ5QkE7RUFDSSxlQUFBO0VBQ0EsbUJBQUE7QUNpQ0o7O0FEL0JBO0VBQ0ksaUJBQUE7RUFDQSxvQkFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtFQUNBLG9CQUFBO0FDa0NKOztBRGhDQTtFQUNJLDZCQUFBO0VBQ0EsaUJBQUE7QUNtQ0o7O0FEakNBO0VBQ0ksZUFBQTtFQUNBLGNBQUE7QUNvQ0o7O0FEbENBO0VBQ0ksZUFBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtBQ3FDSiIsImZpbGUiOiJzcmMvYXBwL3RhYjEvdGFiMS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYnRuaGVhZGVye1xuICAgIG1hcmdpbi10b3A6IDAuNSU7XG59XG4uY2FyZF9oZWFkZXJ7XG4gICAgcGFkZGluZy1ib3R0b206IDA7XG4gICAgcGFkZGluZy10b3A6IDdweDtcbn1cbi5jYXJkX3RpdGxle1xuICAgIGZvbnQtc2l6ZTogMTVweDtcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xufVxuLmNhcmRfY29udGVudHtcbiAgICBmb250LXNpemU6IDEzcHg7XG4gICAgcGFkZGluZy10b3A6IDhweDtcbiAgICBwYWRkaW5nLWJvdHRvbTogMHB4O1xuICAgIGxpbmUtaGVpZ2h0OiAxLjE7XG4gICAgY29sb3I6IGJsYWNrO1xufVxuLmNhcmRfdHh0e1xuICAgIHBhZGRpbmctdG9wOiAxLjIlO1xuICAgIGNvbG9yOiAjNjc5NzMzO1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbn1cbi5jYXJkX2hpZGV7XG4gICAgY29sb3I6ICM2Nzk3MzM7XG4gICAgZm9udC1zaXplOiAxM3B4O1xufVxuLmdvdG9Bbm5vdW5jZXtcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcbn1cbi5wcm9tb2NhcmR7XG4gICAgbWFyZ2luLXRvcDogMDtcbiAgICBiYWNrZ3JvdW5kOiAjZTJmMGNiO1xuICAgIGJveC1zaGFkb3c6IG5vbmU7XG4gICAgYm9yZGVyLXJhZGl1czogMDtcbn1cbi5wcm9tb2hlYWRlcntcbiAgICBwYWRkaW5nLXRvcDogMTBweDtcbiAgICBwYWRkaW5nLWJvdHRvbTogMHB4O1xuICAgIHBhZGRpbmctcmlnaHQ6IDEzcHg7XG4gICAgcGFkZGluZy1sZWZ0OiAxN3B4O1xufVxuLnByb21vX3NtYWxsdHh0e1xuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgY29sb3I6ICM2Nzk3MzM7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLnByb21vX2xhcmdldHh0e1xuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgY29sb3I6IGJsYWNrO1xuICAgIGZvbnQtc2l6ZTogMjdweDtcbiAgICBmb250LXdlaWdodDogYm9sZGVyO1xuICAgIG1hcmdpbi10b3A6IC00LjIlO1xufVxuLmJ0bnNob3B7XG4gICAgZm9udC1zaXplOiAxMXB4O1xuICAgIG1hcmdpbi10b3A6IDEwJTtcbiAgICAtLWJvcmRlci1yYWRpdXM6IDNweDtcbn1cbi5teUhlYXJ0VW5mb2xsb3d7XG4gICAgY29sb3I6IHJnYigyMjMsIDIxMywgMjEzKTtcbn1cbi5teUhlYXJ0Rm9sbG93e1xuICAgIGNvbG9yOiByZWQ7XG59XG5cbi5teUhlYXJ0VW5TYXZle1xuICAgIGNvbG9yOiAjZDNkYmM5O1xufVxuLm15SGVhcnRTYXZle1xuICAgIGNvbG9yOiByZWQ7XG59XG5cblxuaW9uLXNlbGVjdHtcbiAgICBmb250LXNpemU6IDEzcHg7XG59XG4uZm9yX3NvcnRmZWVke1xuICAgIHBhZGRpbmctbGVmdDogMDtcbiAgICBwYWRkaW5nLXRvcDogMDtcbiAgICBtYXJnaW4tdG9wOiAtNSU7XG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNlMmYwY2I7XG4gICAgcGFkZGluZy1ib3R0b206IDA7XG4gICAgbWFyZ2luLWJvdHRvbTogMyU7XG59XG4uZm9yX2l0ZW1ze1xuICAgIG1hcmdpbi10b3A6IDAlO1xuICAgIGJveC1zaGFkb3c6IDAgMXB4IDEwcHggI2UyZjBjYjtcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICNlMmYwY2I7XG4gICAgbWFyZ2luLWJvdHRvbTogMDtcbn1cbi5mb3JfY2hpcHtcbiAgICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICAgIG1hcmdpbjogMDtcbiAgICBwYWRkaW5nLXJpZ2h0OiAwO1xuICAgIHBhZGRpbmctbGVmdDogMTVweDtcbn1cbi5mb3JfaXRlbWltZ3tcbiAgICBtYXJnaW46IGF1dG87XG4gICAgd2lkdGg6IDkwJTtcbiAgICBtYXJnaW4tdG9wOiAtMTVweDtcbiAgICBwYWRkaW5nLWJvdHRvbTogNXB4O1xufVxuLmZvcl9wcmljZXtcbiAgICBmb250LXNpemU6IDI1cHg7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbn1cblxuLmZvcl9pdGVtbmFtZXtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbn1cbi5mb3JfaXRlbWNhdGVne1xuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgZm9udC1zaXplOiAxMnB4O1xuICAgIGZvbnQtd2VpZ2h0OiBsaWdodGVyO1xufVxuLmZvcl9pdGVtbG9jYXRpb257XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIGZvbnQtd2VpZ2h0OiBsaWdodGVyO1xufVxuLmZvcl9zcGFuc3RhcntcbiAgICBmb250LXNpemU6IDE0cHg7Zm9udC13ZWlnaHQ6IG5vcm1hbDtcbiAgfVxuICAuZm9yX3N0YXJpY29ue1xuICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICBtYXJnaW4tYm90dG9tOiAtNHB4O1xuICB9XG4uZm9yX3BpbntcbiAgICBmb250LXNpemU6IDE1cHg7XG4gICAgbWFyZ2luLWJvdHRvbTogLTElO1xuICAgIG1hcmdpbi1sZWZ0OiAtMSU7XG59XG4uZm9yX2l0ZW1idG57XG4gICAgZm9udC1zaXplOiAxMnB4O1xuICAgIGNvbG9yOiAjNjc5NzMzO1xufVxuLmZvcl9pdGVtYnRuaWNvbntcbiAgICBmb250LXNpemU6IDIwcHg7XG4gICAgbWFyZ2luLWJvdHRvbTogLTclO1xuICAgIG1hcmdpbi1yaWdodDogMiU7XG59XG4ubXktY3VzdG9tLWNsYXNzIC5hbGVydC13cmFwcGVyIHtcbiAgICBiYWNrZ3JvdW5kOiAjZTVlNWU1O1xuICB9XG5cblxuLmZvcl9pdGVtbXN7XG4gICAgLS1wYWRkaW5nLXN0YXJ0OiAwO1xuICAgIG1hcmdpbi10b3A6IC04cHg7XG4gICAgLS1pbm5lci1wYWRkaW5nLWVuZDogMDtcbiAgICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICAgIGlvbi1hdmF0YXIgIHsgICAgIFxuICAgICAgICB3aWR0aDogMTAwJSAhaW1wb3J0YW50O1xuICAgICAgICBoZWlnaHQ6IDEwMCUgIWltcG9ydGFudDtcbiAgICAgICAgbWF4LXdpZHRoOiAyNXB4ICFpbXBvcnRhbnQ7XG4gICAgICAgIG1heC1oZWlnaHQ6IDI1cHggIWltcG9ydGFudDtcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiA1cHg7bWFyZ2luLXRvcDogMDtcbiAgICAgICAgaW1ne1xuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgcmdiYSgwLCAwLCAwLCAwLjEyKTtcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XG4gICAgICAgICAgICBoZWlnaHQ6IDEwMCUgIWltcG9ydGFudDtcbiAgICAgICAgICAgIG1heC13aWR0aDogMjVweCAhaW1wb3J0YW50O1xuICAgICAgICAgICAgbWF4LWhlaWdodDogMjVweCAhaW1wb3J0YW50O1xuICAgICAgICAgICAgbWFyZ2luLXJpZ2h0OiA1cHg7bWFyZ2luLXRvcDogMDtcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICAgICAgfVxuICAgIH1cbiAgfVxuICBcbi5mb3JfbmFtZXtcbiAgICBmb250LXNpemU6IDEycHg7XG4gICAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG4gICAgbWFyZ2luLXRvcDogMDtcbiAgICBjb2xvcjogYmxhY2s7XG59XG4uZm9yX2l0ZW1taW1ne1xuICAgIG1hcmdpbjogYXV0bztcbiAgICB3aWR0aDogOTAlO1xuICAgIG1hcmdpbi10b3A6IC0xMHB4O1xuICAgIHBhZGRpbmctYm90dG9tOiA1cHg7XG59XG4uZm9yX3BwcmljZXtcbiAgICBmb250LXNpemU6IDIzcHg7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbn1cbi5mb3JfY2F0ZWdvcnl7XG4gICAgYmFja2dyb3VuZDogIzc2Yzk2MTtcbiAgICBwYWRkaW5nOiAzcHg7XG4gICAgYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDEwcHg7XG4gICAgbWFyZ2luLXRvcDogLTVweDtcbiAgICBtYXJnaW4tcmlnaHQ6IC01cHg7XG4gICAgYm9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czogMTBweDtcbiAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbiAgICBmb250LXNpemU6IDEycHg7XG4gICAgZm9udC13ZWlnaHQ6IGxpZ2h0ZXI7XG4gICAgY29sb3I6IHdoaXRlO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5mb3JfaXRlbW1uYW1le1xuICAgIGZvbnQtc2l6ZTogMTVweDtcbiAgICBmb250LXdlaWdodDogYm9sZGVyO1xufVxuLmZvcl9pdGVtbWxvY2F0aW9ue1xuICAgIGZvbnQtc2l6ZTogMTIuNXB4O1xuICAgIGZvbnQtd2VpZ2h0OiBsaWdodGVyO1xuICAgIGNvbG9yOiAjOTg5YWEyO1xuICAgIHBhZGRpbmctcmlnaHQ6IDVweDtcbiAgICBwYWRkaW5nLWJvdHRvbTogMi41JTtcbn1cbi5mb3JfYm9yZGVyaXRlbXtcbiAgICBib3JkZXItdG9wOiAxcHggc29saWQgI2UyZjBjYjtcbiAgICBwYWRkaW5nLXRvcDogMi41JTtcbn1cbi5mb3JfaXRlbW1idG57XG4gICAgZm9udC1zaXplOiAxMXB4O1xuICAgIGNvbG9yOiAjNjc5NzMzO1xufVxuLmZvcl91bml0cXR5e1xuICAgIGZvbnQtc2l6ZTogMTFweDtcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuICAgIGNvbG9yOiAjOTg5YWEyO1xufSIsIi5idG5oZWFkZXIge1xuICBtYXJnaW4tdG9wOiAwLjUlO1xufVxuXG4uY2FyZF9oZWFkZXIge1xuICBwYWRkaW5nLWJvdHRvbTogMDtcbiAgcGFkZGluZy10b3A6IDdweDtcbn1cblxuLmNhcmRfdGl0bGUge1xuICBmb250LXNpemU6IDE1cHg7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG59XG5cbi5jYXJkX2NvbnRlbnQge1xuICBmb250LXNpemU6IDEzcHg7XG4gIHBhZGRpbmctdG9wOiA4cHg7XG4gIHBhZGRpbmctYm90dG9tOiAwcHg7XG4gIGxpbmUtaGVpZ2h0OiAxLjE7XG4gIGNvbG9yOiBibGFjaztcbn1cblxuLmNhcmRfdHh0IHtcbiAgcGFkZGluZy10b3A6IDEuMiU7XG4gIGNvbG9yOiAjNjc5NzMzO1xuICBmb250LXNpemU6IDEycHg7XG59XG5cbi5jYXJkX2hpZGUge1xuICBjb2xvcjogIzY3OTczMztcbiAgZm9udC1zaXplOiAxM3B4O1xufVxuXG4uZ290b0Fubm91bmNlIHtcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XG59XG5cbi5wcm9tb2NhcmQge1xuICBtYXJnaW4tdG9wOiAwO1xuICBiYWNrZ3JvdW5kOiAjZTJmMGNiO1xuICBib3gtc2hhZG93OiBub25lO1xuICBib3JkZXItcmFkaXVzOiAwO1xufVxuXG4ucHJvbW9oZWFkZXIge1xuICBwYWRkaW5nLXRvcDogMTBweDtcbiAgcGFkZGluZy1ib3R0b206IDBweDtcbiAgcGFkZGluZy1yaWdodDogMTNweDtcbiAgcGFkZGluZy1sZWZ0OiAxN3B4O1xufVxuXG4ucHJvbW9fc21hbGx0eHQge1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBjb2xvcjogIzY3OTczMztcbiAgZm9udC1zaXplOiAxM3B4O1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cblxuLnByb21vX2xhcmdldHh0IHtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgY29sb3I6IGJsYWNrO1xuICBmb250LXNpemU6IDI3cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkZXI7XG4gIG1hcmdpbi10b3A6IC00LjIlO1xufVxuXG4uYnRuc2hvcCB7XG4gIGZvbnQtc2l6ZTogMTFweDtcbiAgbWFyZ2luLXRvcDogMTAlO1xuICAtLWJvcmRlci1yYWRpdXM6IDNweDtcbn1cblxuLm15SGVhcnRVbmZvbGxvdyB7XG4gIGNvbG9yOiAjZGZkNWQ1O1xufVxuXG4ubXlIZWFydEZvbGxvdyB7XG4gIGNvbG9yOiByZWQ7XG59XG5cbi5teUhlYXJ0VW5TYXZlIHtcbiAgY29sb3I6ICNkM2RiYzk7XG59XG5cbi5teUhlYXJ0U2F2ZSB7XG4gIGNvbG9yOiByZWQ7XG59XG5cbmlvbi1zZWxlY3Qge1xuICBmb250LXNpemU6IDEzcHg7XG59XG5cbi5mb3Jfc29ydGZlZWQge1xuICBwYWRkaW5nLWxlZnQ6IDA7XG4gIHBhZGRpbmctdG9wOiAwO1xuICBtYXJnaW4tdG9wOiAtNSU7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZTJmMGNiO1xuICBwYWRkaW5nLWJvdHRvbTogMDtcbiAgbWFyZ2luLWJvdHRvbTogMyU7XG59XG5cbi5mb3JfaXRlbXMge1xuICBtYXJnaW4tdG9wOiAwJTtcbiAgYm94LXNoYWRvdzogMCAxcHggMTBweCAjZTJmMGNiO1xuICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjZTJmMGNiO1xuICBtYXJnaW4tYm90dG9tOiAwO1xufVxuXG4uZm9yX2NoaXAge1xuICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICBtYXJnaW46IDA7XG4gIHBhZGRpbmctcmlnaHQ6IDA7XG4gIHBhZGRpbmctbGVmdDogMTVweDtcbn1cblxuLmZvcl9pdGVtaW1nIHtcbiAgbWFyZ2luOiBhdXRvO1xuICB3aWR0aDogOTAlO1xuICBtYXJnaW4tdG9wOiAtMTVweDtcbiAgcGFkZGluZy1ib3R0b206IDVweDtcbn1cblxuLmZvcl9wcmljZSB7XG4gIGZvbnQtc2l6ZTogMjVweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbn1cblxuLmZvcl9pdGVtbmFtZSB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbn1cblxuLmZvcl9pdGVtY2F0ZWcge1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBmb250LXNpemU6IDEycHg7XG4gIGZvbnQtd2VpZ2h0OiBsaWdodGVyO1xufVxuXG4uZm9yX2l0ZW1sb2NhdGlvbiB7XG4gIGZvbnQtc2l6ZTogMTNweDtcbiAgZm9udC13ZWlnaHQ6IGxpZ2h0ZXI7XG59XG5cbi5mb3Jfc3BhbnN0YXIge1xuICBmb250LXNpemU6IDE0cHg7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG59XG5cbi5mb3Jfc3Rhcmljb24ge1xuICBmb250LXNpemU6IDE4cHg7XG4gIG1hcmdpbi1ib3R0b206IC00cHg7XG59XG5cbi5mb3JfcGluIHtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBtYXJnaW4tYm90dG9tOiAtMSU7XG4gIG1hcmdpbi1sZWZ0OiAtMSU7XG59XG5cbi5mb3JfaXRlbWJ0biB7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgY29sb3I6ICM2Nzk3MzM7XG59XG5cbi5mb3JfaXRlbWJ0bmljb24ge1xuICBmb250LXNpemU6IDIwcHg7XG4gIG1hcmdpbi1ib3R0b206IC03JTtcbiAgbWFyZ2luLXJpZ2h0OiAyJTtcbn1cblxuLm15LWN1c3RvbS1jbGFzcyAuYWxlcnQtd3JhcHBlciB7XG4gIGJhY2tncm91bmQ6ICNlNWU1ZTU7XG59XG5cbi5mb3JfaXRlbW1zIHtcbiAgLS1wYWRkaW5nLXN0YXJ0OiAwO1xuICBtYXJnaW4tdG9wOiAtOHB4O1xuICAtLWlubmVyLXBhZGRpbmctZW5kOiAwO1xuICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xufVxuLmZvcl9pdGVtbXMgaW9uLWF2YXRhciB7XG4gIHdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XG4gIGhlaWdodDogMTAwJSAhaW1wb3J0YW50O1xuICBtYXgtd2lkdGg6IDI1cHggIWltcG9ydGFudDtcbiAgbWF4LWhlaWdodDogMjVweCAhaW1wb3J0YW50O1xuICBtYXJnaW4tcmlnaHQ6IDVweDtcbiAgbWFyZ2luLXRvcDogMDtcbn1cbi5mb3JfaXRlbW1zIGlvbi1hdmF0YXIgaW1nIHtcbiAgYm9yZGVyOiAxcHggc29saWQgcmdiYSgwLCAwLCAwLCAwLjEyKTtcbiAgd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcbiAgaGVpZ2h0OiAxMDAlICFpbXBvcnRhbnQ7XG4gIG1heC13aWR0aDogMjVweCAhaW1wb3J0YW50O1xuICBtYXgtaGVpZ2h0OiAyNXB4ICFpbXBvcnRhbnQ7XG4gIG1hcmdpbi1yaWdodDogNXB4O1xuICBtYXJnaW4tdG9wOiAwO1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG59XG5cbi5mb3JfbmFtZSB7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG4gIG1hcmdpbi10b3A6IDA7XG4gIGNvbG9yOiBibGFjaztcbn1cblxuLmZvcl9pdGVtbWltZyB7XG4gIG1hcmdpbjogYXV0bztcbiAgd2lkdGg6IDkwJTtcbiAgbWFyZ2luLXRvcDogLTEwcHg7XG4gIHBhZGRpbmctYm90dG9tOiA1cHg7XG59XG5cbi5mb3JfcHByaWNlIHtcbiAgZm9udC1zaXplOiAyM3B4O1xuICBmb250LXdlaWdodDogYm9sZGVyO1xufVxuXG4uZm9yX2NhdGVnb3J5IHtcbiAgYmFja2dyb3VuZDogIzc2Yzk2MTtcbiAgcGFkZGluZzogM3B4O1xuICBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogMTBweDtcbiAgbWFyZ2luLXRvcDogLTVweDtcbiAgbWFyZ2luLXJpZ2h0OiAtNXB4O1xuICBib3JkZXItYm90dG9tLWxlZnQtcmFkaXVzOiAxMHB4O1xuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBmb250LXdlaWdodDogbGlnaHRlcjtcbiAgY29sb3I6IHdoaXRlO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5mb3JfaXRlbW1uYW1lIHtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBmb250LXdlaWdodDogYm9sZGVyO1xufVxuXG4uZm9yX2l0ZW1tbG9jYXRpb24ge1xuICBmb250LXNpemU6IDEyLjVweDtcbiAgZm9udC13ZWlnaHQ6IGxpZ2h0ZXI7XG4gIGNvbG9yOiAjOTg5YWEyO1xuICBwYWRkaW5nLXJpZ2h0OiA1cHg7XG4gIHBhZGRpbmctYm90dG9tOiAyLjUlO1xufVxuXG4uZm9yX2JvcmRlcml0ZW0ge1xuICBib3JkZXItdG9wOiAxcHggc29saWQgI2UyZjBjYjtcbiAgcGFkZGluZy10b3A6IDIuNSU7XG59XG5cbi5mb3JfaXRlbW1idG4ge1xuICBmb250LXNpemU6IDExcHg7XG4gIGNvbG9yOiAjNjc5NzMzO1xufVxuXG4uZm9yX3VuaXRxdHkge1xuICBmb250LXNpemU6IDExcHg7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIGNvbG9yOiAjOTg5YWEyO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/tab1/tab1.page.ts":
/*!***********************************!*\
  !*** ./src/app/tab1/tab1.page.ts ***!
  \***********************************/
/*! exports provided: Tab1Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab1Page", function() { return Tab1Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _shared_model_announcement_model__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../shared/model/announcement.model */ "./src/app/shared/model/announcement.model.ts");
/* harmony import */ var _providers_credential_provider__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../providers/credential-provider */ "./src/providers/credential-provider.ts");
/* harmony import */ var _shared_model_item_model__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../shared/model/item.model */ "./src/app/shared/model/item.model.ts");
/* harmony import */ var _register_register_page__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../register/register.page */ "./src/app/register/register.page.ts");










let Tab1Page = class Tab1Page {
    constructor(router, postPvdr, zone, modalController, navCtrl, storage, route) {
        this.router = router;
        this.postPvdr = postPvdr;
        this.zone = zone;
        this.modalController = modalController;
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.route = route;
        this.ImageArray = [];
        this.hideMe = false;
        this.login_user_id = 0;
        this.searchQuery = "";
        this.itemList = [];
        this.itemListPromoted = [];
        this.status = false;
        this.itemListSearched = [];
        this.announcement = [];
        this.category_id = "";
        this.counter = 0;
        this.searchedTurn = false;
        this.noResultsFound = false;
        this.loader = true;
        this.slideOptsOne = {
            initialSlide: 0,
            slidesPerView: 1,
            autoplay: true
        };
        this.ImageArray = [
            { 'image': 'assets/greenthumb-images/bunny2.png',
                'name': 'Lorem ipsum dolor 1',
                'text': 'the' },
            { 'image': 'assets/greenthumb-images/bunny2.png',
                'name': 'Lorem ipsum dolor 2',
                'text': 'the' },
            { 'image': 'assets/greenthumb-images/bunny2.png',
                'name': 'Lorem ipsum dolor 3',
                'text': 'the' }
        ];
        this.route.queryParams.subscribe(params => {
            this.category_id = params["category_id"];
            console.log("params:" + JSON.stringify(params));
            console.log("categor_id:" + this.category_id);
        });
    }
    goLocation() {
        this.navCtrl.navigateForward(['location']);
    }
    goProductview(item_id) {
        // this.router.navigate(['productview']);
        let navigationExtras = {
            queryParams: { item_id }
        };
        this.navCtrl.navigateForward(['productview'], navigationExtras);
    }
    goOffers() {
        this.router.navigate(['tabs/offers']);
    }
    goSellerprof(user_profile_id) {
        // this.router.navigate((['viewitemsellerprofile']));
        let navigationExtras = {
            queryParams: { user_profile_id }
        };
        this.navCtrl.navigateForward(['viewitemsellerprofile'], navigationExtras);
    }
    eventHandler(keyCode) {
        console.log("key:" + keyCode);
    }
    openRegistermodal() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _register_register_page__WEBPACK_IMPORTED_MODULE_8__["RegisterPage"],
                cssClass: 'registermodalstyle'
            });
            modal.onDidDismiss()
                .then((data) => {
                let body = {
                    action: 'notNowUpdate',
                    user_id: this.login_user_id,
                };
                this.postPvdr.postData(body, 'user.php').subscribe(data => {
                    if (data.success) {
                    }
                });
            });
            return yield modal.present();
        });
    }
    followThis(user_id) {
        let body = {
            action: 'follow',
            user_id: user_id,
            followed_by: this.login_user_id,
        };
        console.log("follow:" + JSON.stringify(body));
        this.postPvdr.postData(body, 'followers.php').subscribe(data => {
            console.log(data);
            if (data.success) {
                // uncomment this when you add system notifications
                // this.followClass = "myHeartFollow";
                // this.status = !this.status; 
                // if(this.status){
                //   let body2 = {
                //     action : 'addNotification',
                //     user_id : this.user_id,
                //     followed_by: user_id,
                //     notification_message : "has followed you",
                //     status : this.status
                //   };
                //   this.postPvdr.postData(body2, 'system_notification.php').subscribe(data => {
                //   })
                // }
            }
        });
    }
    saveThis(item_id) {
        console.log("gi saved lamang pud");
        let body = {
            action: 'saveItem',
            item_id: item_id,
            user_id: this.login_user_id,
        };
        this.postPvdr.postData(body, 'save_item.php').subscribe(data => {
            if (data.success) {
                if (data.saveStatus == '1') {
                    console.log('1 ko');
                    var element = document.getElementById("class" + item_id);
                    element.classList.remove("myHeartUnSave");
                    element.classList.add("myHeartSave");
                    console.log("classList:" + element.classList);
                }
                else {
                    console.log("0 ko");
                    var element = document.getElementById("class" + item_id);
                    element.classList.remove("myHeartSave");
                    element.classList.add("myHeartUnSave");
                }
            }
        });
    }
    searchItem() {
        let body = {
            action: 'getPostItemSearchedQuery',
            searchQuery: this.searchQuery,
            user_id: this.login_user_id,
        };
        console.log(JSON.stringify(body));
        this.postPvdr.postData(body, 'post_item.php').subscribe((data) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            if (data.success) {
                const itemsSearched = [];
                //var pictureProfile: string = '';
                var x = 0;
                for (const key in data.result) {
                    itemsSearched.push(new _shared_model_item_model__WEBPACK_IMPORTED_MODULE_7__["Item"](data.result[key].item_id, data.result[key].user_id, data.result[key].username, (data.result[key].profile_photo == '') ? '' :
                        this.postPvdr.myServer() + "/greenthumb/images/" + data.result[key].profile_photo, data.result[key].price, data.result[key].title, data.result[key].stocks_id, data.result[key].others_stock, parseInt(data.result[key].quantity), data.result[key].category, data.result[key].location, data.result[key].saveStatus, data.result[key].status, this.postPvdr.myServer() + "/greenthumb/images/posted_items/images/" + data.result[key].item_photo, data.result[key].user_rating, data.result[key].date, data.result[key].time));
                    x++;
                }
                if (x == 0) {
                    this.noResultsFound = true;
                }
                else {
                    this.noResultsFound = false;
                }
                this.searchedTurn = true;
                console.log("this.serachedTurn:" + this.searchedTurn);
                this.itemListSearched = itemsSearched;
            }
        }));
    }
    check_user_exists() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.storage.get("greenthumb_user_id").then((user_id) => {
                let body = {
                    action: 'check_user_exists',
                    user_id: user_id
                };
                console.log("pira ka kilo:" + JSON.stringify(body));
                this.postPvdr.postData(body, 'credentials-api.php').subscribe(data => {
                    if (data.success) {
                        this.openRegistermodal();
                    }
                });
            });
        });
    }
    hide() {
        this.hideMe = false;
        let body321 = {
            action: 'hideAnnouncement',
            user_id: this.login_user_id,
        };
        this.postPvdr.postData(body321, 'system_notification.php').subscribe(data => {
            if (data.success) {
                // updated na
            }
        });
    }
    allAnnouncements() {
        this.storage.set("greenthumb_segment", 1);
        this.router.navigate(['tabs/tab5']);
    }
    ngOnInit() {
    }
    ionViewWillEnter() {
        //this.plotData();
        //this.loader = true;
        console.log("this is ionviewwill enter tab 1");
        setInterval(() => {
            this.zone.run(() => {
                this.getAnnouncement();
            });
        }, 7000);
        if (this.category_id != undefined) {
            this.searchedPosted();
        }
        else {
            this.plotData();
        }
        this.check_user_exists();
    }
    searchedPosted() {
        console.log("searchedPosted" + this.counter++);
        let body = {
            action: 'getPostItemsSearched',
            category_id: this.category_id,
            user_id: this.login_user_id,
        };
        console.log(JSON.stringify(body));
        this.postPvdr.postData(body, 'post_item.php').subscribe((data) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            if (data.success) {
                const itemsSearched = [];
                //var pictureProfile: string = '';
                var x = 0;
                for (const key in data.result) {
                    itemsSearched.push(new _shared_model_item_model__WEBPACK_IMPORTED_MODULE_7__["Item"](data.result[key].item_id, data.result[key].user_id, data.result[key].username, (data.result[key].profile_photo == '') ? '' :
                        this.postPvdr.myServer() + "/greenthumb/images/" + data.result[key].profile_photo, data.result[key].price, data.result[key].title, data.result[key].stocks_id, data.result[key].others_stock, parseInt(data.result[key].quantity), data.result[key].category, data.result[key].location, data.result[key].saveStatus, data.result[key].status, this.postPvdr.myServer() + "/greenthumb/images/posted_items/images/" + data.result[key].item_photo, data.result[key].user_rating, data.result[key].date, data.result[key].time));
                    x++;
                }
                if (x == 0) {
                    this.noResultsFound = true;
                }
                else {
                    this.noResultsFound = false;
                }
                this.searchedTurn = true;
                console.log("this.serachedTurn:" + this.searchedTurn);
                this.itemListSearched = itemsSearched;
            }
        }));
    }
    plotData() {
        // for announcement
        //console.log("plotData");
        this.getAnnouncement();
        this.storage.get('greenthumb_user_id').then((user_id) => {
            // for promotion
            let bodyPromotion = {
                action: 'getPromotedPostItems',
                user_id: user_id,
            };
            console.log("plot data promotion:" + JSON.stringify(bodyPromotion));
            this.postPvdr.postData(bodyPromotion, 'post_item.php').subscribe((data) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                if (data.success) {
                    const itemsPromoted = [];
                    //var pictureProfile: string = '';
                    for (const key in data.result) {
                        // pictureProfile = (data.result[key].profile_photo == '') ? '' :
                        // this.postPvdr.myServer()+"/brixy-live/images/"+data.result[key].profile_photo;
                        itemsPromoted.push(new _shared_model_item_model__WEBPACK_IMPORTED_MODULE_7__["Item"](data.result[key].item_id, data.result[key].user_id, data.result[key].username, (data.result[key].profile_photo == '') ? '' :
                            this.postPvdr.myServer() + "/greenthumb/images/" + data.result[key].profile_photo, data.result[key].price, data.result[key].title, data.result[key].stocks_id, data.result[key].others_stock, parseInt(data.result[key].quantity), data.result[key].category, data.result[key].location, data.result[key].saveStatus, data.result[key].status, this.postPvdr.myServer() + "/greenthumb/images/posted_items/images/" + data.result[key].item_photo, data.result[key].user_rating, data.result[key].date, data.result[key].time));
                        console.log("hello user_rating" + data.result[key].user_rating);
                    }
                    // this.searchedTurn = false;
                    this.itemListPromoted = itemsPromoted;
                    this.loader = false;
                }
            }));
            let body = {
                action: 'getPostItems',
                user_id: user_id,
            };
            console.log("plot data tab1:" + JSON.stringify(body));
            this.postPvdr.postData(body, 'post_item.php').subscribe((data) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                if (data.success) {
                    const items = [];
                    //var pictureProfile: string = '';
                    for (const key in data.result) {
                        // pictureProfile = (data.result[key].profile_photo == '') ? '' :
                        // this.postPvdr.myServer()+"/brixy-live/images/"+data.result[key].profile_photo;
                        items.push(new _shared_model_item_model__WEBPACK_IMPORTED_MODULE_7__["Item"](data.result[key].item_id, data.result[key].user_id, data.result[key].username, (data.result[key].profile_photo == '') ? '' :
                            this.postPvdr.myServer() + "/greenthumb/images/" + data.result[key].profile_photo, data.result[key].price, data.result[key].title, data.result[key].stocks_id, data.result[key].others_stock, parseInt(data.result[key].quantity), data.result[key].category, data.result[key].location, data.result[key].saveStatus, data.result[key].status, this.postPvdr.myServer() + "/greenthumb/images/posted_items/images/" + data.result[key].item_photo, data.result[key].user_rating, data.result[key].date, data.result[key].time));
                        console.log("hello user_rating" + data.result[key].user_rating);
                    }
                    this.searchedTurn = false;
                    this.itemList = items;
                    this.loader = false;
                }
            }));
        });
    }
    logRatingChange(event) {
        //this.myRate = event;
    }
    getAnnouncement() {
        this.storage.get('greenthumb_user_id').then((user_id) => {
            this.login_user_id = user_id;
            let body321 = {
                action: 'getAnnouncement',
                user_id: user_id
            };
            //console.log("storyalang:"+JSON.stringify(body321));
            this.postPvdr.postData(body321, 'system_notification.php').subscribe(data => {
                if (data.success) {
                    const announcementsList = [];
                    var x = 0;
                    for (const key in data.result) {
                        announcementsList.push(new _shared_model_announcement_model__WEBPACK_IMPORTED_MODULE_5__["Announcement"](data.result[key].id, data.result[key].title, data.result[key].announcment, data.result[key].date, data.result[key].time));
                        x++;
                    }
                    if (x == 0) {
                        this.hideMe = false;
                    }
                    else {
                        this.hideMe = true;
                    }
                    this.announcement = announcementsList;
                }
            });
        });
    }
    doRefresh(event) {
        console.log('Begin async operation');
        //this.loadUser();
        this.plotData();
        setTimeout(() => {
            console.log('Async operation has ended');
            event.target.complete();
            this.searchQuery = '';
        }, 2000);
    }
}; // end
Tab1Page.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _providers_credential_provider__WEBPACK_IMPORTED_MODULE_6__["PostProvider"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('myInput', null),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
], Tab1Page.prototype, "myInput", void 0);
Tab1Page = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-tab1',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./tab1.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/tab1/tab1.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./tab1.page.scss */ "./src/app/tab1/tab1.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
        _providers_credential_provider__WEBPACK_IMPORTED_MODULE_6__["PostProvider"],
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"],
        _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"],
        _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
], Tab1Page);



/***/ })

}]);
//# sourceMappingURL=tab1-tab1-module-es2015.js.map