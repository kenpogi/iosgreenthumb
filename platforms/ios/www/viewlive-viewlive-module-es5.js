function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["viewlive-viewlive-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/viewlive/viewlive.page.html":
  /*!***********************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/viewlive/viewlive.page.html ***!
    \***********************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppViewliveViewlivePageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n<ion-content mode=\"ios\">\n  <div class=\"my-overlay\" text-center>\n      <ion-row >\n          <ion-col text-left size=\"8\">\n            <ion-list mode=\"ios\" style=\"background-color: transparent;\">\n              <ion-item  mode=\"ios\" lines=\"none\" class=\"item_profile\">\n                <ion-thumbnail slot=\"start\" [ngClass]=\"vip_package == '1' ? 'commanderAvatar':\n                (vip_package == '2'? 'generalAvatar' : \n                (vip_package == '3' ? 'captainAvatar' : ''))\"  mode=\"ios\" (click)=\"openliveprofile()\">\n                    <div *ngIf=\"!user_photo\">\n                      <img src=\"assets/icon/brixylogo.png\">\n                    </div>\n                    <div *ngIf=\"user_photo\">\n                     <img [src] = \"user_photo\">\n                    </div> \n                </ion-thumbnail>\n                <ion-label mode=\"ios\" color=\"light\">\n                  <h3 mode=\"ios\" style=\"font-family: system-ui;\">{{fname+\" \"+lname}}</h3>\n                  <h6 class=\"lvlnum\" mode=\"ios\" style=\"font-size: 12px;\"> \n                    <img src=\"assets/icon/medal.png\" class=\"badge_knight3\">&nbsp;{{user_level}}&nbsp;&nbsp;\n                    <img src=\"assets/icon/follow.png\" class=\"badge_knight3\">&nbsp;{{followers}}&nbsp;&nbsp;\n                    <img src=\"assets/icon/viewer.png\" class=\"badge_knight3\">&nbsp;{{following}}</h6>\n                </ion-label>\n              </ion-item>\n            </ion-list>\n          </ion-col>\n          <ion-col text-left size=\"4\">\n            <div class=\"thumnails\">\n              <div class=\"list-thumbnail\">\n                <div class=\"img-thumb\">\n                  <div style=\"display: inline-block;\" *ngFor=\"let view of viewersPic;\">\n                    <img [src] = \"view?.viewerPic\"  [ngClass]=\"view?.vip_packageOfUser == '1' ? 'commanderClassThumbnail':\n                    (view?.vip_packageOfUser == '2'? 'generalClassThumbnail' : \n                    (view?.vip_packageOfUser == '3' ? 'captainClassThumbnail' : ''))\">\n                  </div>\n                </div>\n              </div>\n            </div>\n          </ion-col>\n        </ion-row>\n        <ion-row padding style=\"margin-top: -10%;\">\n          <ion-col text-left>\n            <ion-row>\n              <ion-col style=\"padding-left: 0;\">\n                <img src=\"assets/gif/lips.gif\" class=\"brixy-icon\" (click)=\"goStore()\">\n              </ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col style=\"padding-left: 0;margin-top: 8%;\">\n                <img (click)=\"follow()\" [ngClass]=\"status ? 'myHeartFollow' : 'myHeartUnfollow'\">\n              </ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col style=\"padding-left: 0;margin-top: 8%;\">\n                <img src=\"assets/icon/giftbox.png\" (click)=\"openAllgifts()\" class=\"brixy-icon\" >\n              </ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col style=\"padding-left: 0;margin-top: 11%;\">\n                <img src=\"assets/icon/chat.png\" style=\"width: 23% !important;\" (click)=\"goTab5()\" class=\"brixy-icon\" >\n              </ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col style=\"padding-left: 0;margin-top: 8%;\">\n                <img src=\"assets/icon/close.png\" (click)=\"goHome()\" class=\"brixy-icon\" >\n              </ion-col>\n            </ion-row>\n          </ion-col>\n          <ion-col text-right style=\"visibility: hidden;\">\n            <ion-icon (click)=\"goHome()\" class=\"close_icon\" name=\"close\" mode=\"ios\" mode=\"ios\"></ion-icon>\n          </ion-col>\n        </ion-row>\n\n  </div>\n  <!-- <img src=\"assets/icon/girl.gif\" style=\"height: 100%;\"> -->\n  <!-- <iframe width=\"350\" height=\"600\" src=\"https://brixylive.com:5443/WebRTCAppEE/play.html?name=stream44\" frameborder=\"0\" allowfullscreen></iframe> -->\n  <video id=\"remoteVideo\" autoplay playsinline controls style=\"height: 95%\"></video>\n  <div class=\"my-overlay\" style=\"bottom:0%;\" text-left>\n    <div class=\"overlay_message\">\n      <div class=\"wrapper\">\n        <ion-row>\n          <ion-col>\n          <p *ngFor=\"let message of messages\"\n          [ngClass]=\"message.vip_packageOfUser == '1' ? 'commanderClass':\n          (message.vip_packageOfUser == '2'? 'generalClass' : \n          (message.vip_packageOfUser == '3' ? 'captainClass' : ''))\" >\n            <ion-label color=\"warning\"><b>{{message.user_id}}</b></ion-label>&nbsp;\n            <ion-text color=\"light\">\n            {{message.msg}}</ion-text>\n          </p> \n          <p style=\"visibility: hidden;margin-bottom: -12%;\">\n              <ion-label color=\"warning\"><b>userid544</b></ion-label>&nbsp;\n              <ion-text color=\"light\">fdfdsffdf dffdfd sffd fdffdfds ffdfdff dfdsffd fd ffd fdsffdfdffdfdsffdfdf\n              </ion-text>\n          </p>\n          </ion-col>\n        </ion-row>\n      </div>\n    </div>\n    \n    <ion-toolbar color=\"light\">\n      <ion-row align-items-center>\n        <ion-col size=\"10\">\n          <ion-textarea color=\"dark\" placeholder=\"Write message\" class=\"message-input\" rows=\"1\" [(ngModel)]=\"message\"></ion-textarea>\n        </ion-col>\n        <ion-col size=\"2\">\n          <ion-button mode=\"ios\" expand=\"block\" fill=\"clear\" [disabled]=\"message === ''\" class=\"msg-btn\"\n            (click)=\"sendMessage()\">\n            <ion-icon name=\"ios-send\" slot=\"icon-only\" mode=\"ios\" color=\"primary\"></ion-icon>\n          </ion-button>\n        </ion-col>\n      </ion-row>\n    </ion-toolbar>\n</div>\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/viewlive/viewlive.module.ts":
  /*!*********************************************!*\
    !*** ./src/app/viewlive/viewlive.module.ts ***!
    \*********************************************/

  /*! exports provided: ViewlivePageModule */

  /***/
  function srcAppViewliveViewliveModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ViewlivePageModule", function () {
      return ViewlivePageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _viewlive_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./viewlive.page */
    "./src/app/viewlive/viewlive.page.ts");

    var routes = [{
      path: '',
      component: _viewlive_page__WEBPACK_IMPORTED_MODULE_6__["ViewlivePage"]
    }];

    var ViewlivePageModule = function ViewlivePageModule() {
      _classCallCheck(this, ViewlivePageModule);
    };

    ViewlivePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      declarations: [_viewlive_page__WEBPACK_IMPORTED_MODULE_6__["ViewlivePage"]]
    })], ViewlivePageModule);
    /***/
  },

  /***/
  "./src/app/viewlive/viewlive.page.scss":
  /*!*********************************************!*\
    !*** ./src/app/viewlive/viewlive.page.scss ***!
    \*********************************************/

  /*! exports provided: default */

  /***/
  function srcAppViewliveViewlivePageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".my-overlay {\n  position: fixed;\n  width: 100%;\n  z-index: 20;\n}\n\n.overlay_message {\n  height: 300px;\n  display: flex;\n  flex-direction: column-reverse;\n  align-items: flex-end;\n  overflow-y: auto;\n  padding-left: 55px;\n  padding-right: 2px;\n  z-index: 99;\n}\n\n.overlay_message p {\n  padding: 8px;\n  background: rgba(0, 0, 0, 0.4);\n  border-radius: 50px;\n}\n\n.commanderClass {\n  border: 3px solid rgba(245, 5, 5, 0.4);\n  color: rgba(245, 5, 5, 0.4);\n}\n\n.generalClass {\n  border: 3px solid rgba(18, 4, 212, 0.4);\n  color: rgba(18, 4, 212, 0.4);\n}\n\n.captainClass {\n  border: 3px solid rgba(238, 234, 13, 0.4);\n  color: rgba(238, 234, 13, 0.4);\n}\n\n.commanderClassThumbnail {\n  border: 1px solid rgba(245, 5, 5, 0.4);\n}\n\n.generalClassThumbnail {\n  border: 1px solid rgba(18, 4, 212, 0.4);\n}\n\n.captainClassThumbnail {\n  border: 1px solid rgba(238, 234, 13, 0.4);\n}\n\n.commanderAvatar {\n  border: 3px solid rgba(245, 5, 5, 0.4);\n  color: rgba(245, 5, 5, 0.4);\n}\n\n.generalAvatar {\n  border: 3px solid rgba(18, 4, 212, 0.4);\n  color: rgba(18, 4, 212, 0.4);\n}\n\n.captainAvatar {\n  border: 3px solid rgba(238, 234, 13, 0.4);\n  color: rgba(238, 234, 13, 0.4);\n}\n\n.item_profile {\n  --background: rgba(0, 0, 0, 0.3);\n  border: 1px solid #1dc1e6;\n  border-radius: 30px;\n}\n\n.close_icon {\n  zoom: 3;\n  color: white;\n  position: fixed;\n  right: 2%;\n  top: 8.5%;\n}\n\n.thumnails {\n  overflow-x: scroll;\n  overflow-y: hidden;\n  margin-top: 5%;\n}\n\n.thumnails .list-thumbnail {\n  height: 100%;\n  white-space: nowrap;\n}\n\n.thumnails .list-thumbnail .img-thumb img {\n  display: inline-block;\n  border-radius: 50%;\n  padding: 3px;\n  width: 40px;\n  height: 40px;\n  margin: 0 2px 0 0;\n  line-height: 60px;\n}\n\n::-webkit-scrollbar {\n  display: none;\n}\n\n.myHeartFollow {\n  width: 23% !important;\n  height: auto;\n  content: url(\"/assets/icon/followed.png\");\n}\n\n.myHeartUnfollow {\n  width: 23% !important;\n  height: auto;\n  content: url(\"/assets/icon/follow.png\");\n}\n\n.lvlnum {\n  margin: 0;\n  margin-top: 4%;\n  font-weight: bolder;\n  font-family: Verdana, Geneva, Tahoma, sans-serif;\n}\n\nion-item {\n  --padding-start: 0% !important;\n}\n\n.brixy-icon {\n  width: 23% !important;\n  height: auto;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZW5kcmljay9EZXNrdG9wL1RlbG1vIFNvbHV0aW9ucy9Gb3JQcm9kdWN0aW9uL21heTEwMjAyMS9sYXN0IHNldHVwL2dyZWVudGh1bWJhcHAgY29weS9zcmMvYXBwL3ZpZXdsaXZlL3ZpZXdsaXZlLnBhZ2Uuc2NzcyIsInNyYy9hcHAvdmlld2xpdmUvdmlld2xpdmUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksZUFBQTtFQUNBLFdBQUE7RUFDQSxXQUFBO0FDQ0o7O0FEQ0U7RUFDRSxhQUFBO0VBQ0EsYUFBQTtFQUNBLDhCQUFBO0VBQ0EscUJBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0FDRUo7O0FEREk7RUFDRSxZQUFBO0VBQ0EsOEJBQUE7RUFDQSxtQkFBQTtBQ0dOOztBREdFO0VBRUksc0NBQUE7RUFDQSwyQkFBQTtBQ0ROOztBREtFO0VBRUUsdUNBQUE7RUFDQSw0QkFBQTtBQ0hKOztBREtFO0VBRUUseUNBQUE7RUFDQSw4QkFBQTtBQ0hKOztBRFFFO0VBQ0Usc0NBQUE7QUNMSjs7QURRRTtFQUNFLHVDQUFBO0FDTEo7O0FET0U7RUFDRSx5Q0FBQTtBQ0pKOztBRFFFO0VBRUUsc0NBQUE7RUFDQSwyQkFBQTtBQ05KOztBRFVFO0VBRUUsdUNBQUE7RUFDQSw0QkFBQTtBQ1JKOztBRFVFO0VBRUUseUNBQUE7RUFDQSw4QkFBQTtBQ1JKOztBRFdBO0VBQ0UsZ0NBQUE7RUFDQSx5QkFBQTtFQUNBLG1CQUFBO0FDUkY7O0FEVUE7RUFDRSxPQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxTQUFBO0VBQ0EsU0FBQTtBQ1BGOztBRFNFO0VBQ0Usa0JBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7QUNOSjs7QURPSTtFQUNFLFlBQUE7RUFDQSxtQkFBQTtBQ0xOOztBRE1NO0VBQ0UscUJBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0EsaUJBQUE7QUNKUjs7QURRRTtFQUNFLGFBQUE7QUNMSjs7QURPRTtFQUNFLHFCQUFBO0VBQ0EsWUFBQTtFQUNBLHlDQUFBO0FDSko7O0FETUU7RUFDRSxxQkFBQTtFQUNBLFlBQUE7RUFDQSx1Q0FBQTtBQ0hKOztBREtFO0VBQ0UsU0FBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtFQUNBLGdEQUFBO0FDRko7O0FESUU7RUFDRSw4QkFBQTtBQ0RKOztBREdBO0VBQ0UscUJBQUE7RUFDQSxZQUFBO0FDQUYiLCJmaWxlIjoic3JjL2FwcC92aWV3bGl2ZS92aWV3bGl2ZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubXktb3ZlcmxheSB7XG4gICAgcG9zaXRpb246IGZpeGVkO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIHotaW5kZXg6IDIwO1xuICB9XG4gIC5vdmVybGF5X21lc3NhZ2Uge1xuICAgIGhlaWdodDogMzAwcHg7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uLXJldmVyc2U7IFxuICAgIGFsaWduLWl0ZW1zOiBmbGV4LWVuZDtcbiAgICBvdmVyZmxvdy15OiBhdXRvO1xuICAgIHBhZGRpbmctbGVmdDogNTVweDtcbiAgICBwYWRkaW5nLXJpZ2h0OiAycHg7XG4gICAgei1pbmRleDogOTk7XG4gICAgcHtcbiAgICAgIHBhZGRpbmc6IDhweDtcbiAgICAgIGJhY2tncm91bmQ6IHJnYmEoMCwgMCwgMCwgMC40KTtcbiAgICAgIGJvcmRlci1yYWRpdXM6IDUwcHg7XG4gICAgfVxuICB9XG5cblxuICAvLyBmb3IgbWVzc2FnZVxuICAuY29tbWFuZGVyQ2xhc3Mge1xuICAgIFxuICAgICAgYm9yZGVyOiAzcHggc29saWQgcmdiYSgyNDUsIDUsIDUsIDAuNCk7XG4gICAgICBjb2xvcjogIHJnYmEoMjQ1LCA1LCA1LCAwLjQpO1xuICAgIFxuICB9XG5cbiAgLmdlbmVyYWxDbGFzcyB7XG4gICAgXG4gICAgYm9yZGVyOiAzcHggc29saWQgcmdiYSgxOCwgNCwgMjEyLCAwLjQpO1xuICAgIGNvbG9yOiAgcmdiYSgxOCwgNCwgMjEyLCAwLjQpO1xuICB9XG4gIC5jYXB0YWluQ2xhc3Mge1xuICAgIFxuICAgIGJvcmRlcjogM3B4IHNvbGlkIHJnYmEoMjM4LCAyMzQsIDEzLCAwLjQpO1xuICAgIGNvbG9yOiAgIHJnYmEoMjM4LCAyMzQsIDEzLCAwLjQpO1xuICB9XG5cbiAgLy8gZm9yIHRodW1ibmFpbFxuXG4gIC5jb21tYW5kZXJDbGFzc1RodW1ibmFpbCB7XG4gICAgYm9yZGVyOiAxcHggc29saWQgcmdiYSgyNDUsIDUsIDUsIDAuNCk7XG4gIH1cblxuICAuZ2VuZXJhbENsYXNzVGh1bWJuYWlsIHtcbiAgICBib3JkZXI6IDFweCBzb2xpZCByZ2JhKDE4LCA0LCAyMTIsIDAuNCk7XG4gIH1cbiAgLmNhcHRhaW5DbGFzc1RodW1ibmFpbCB7XG4gICAgYm9yZGVyOiAxcHggc29saWQgcmdiYSgyMzgsIDIzNCwgMTMsIDAuNCk7XG4gIH1cblxuICAvLyBmb3IgYXZhdGFyXG4gIC5jb21tYW5kZXJBdmF0YXIge1xuICAgIFxuICAgIGJvcmRlcjogM3B4IHNvbGlkIHJnYmEoMjQ1LCA1LCA1LCAwLjQpO1xuICAgIGNvbG9yOiAgcmdiYSgyNDUsIDUsIDUsIDAuNCk7XG4gIFxuICB9XG5cbiAgLmdlbmVyYWxBdmF0YXIge1xuICAgIFxuICAgIGJvcmRlcjogM3B4IHNvbGlkIHJnYmEoMTgsIDQsIDIxMiwgMC40KTtcbiAgICBjb2xvcjogIHJnYmEoMTgsIDQsIDIxMiwgMC40KTtcbiAgfVxuICAuY2FwdGFpbkF2YXRhciB7XG4gICAgXG4gICAgYm9yZGVyOiAzcHggc29saWQgcmdiYSgyMzgsIDIzNCwgMTMsIDAuNCk7XG4gICAgY29sb3I6ICAgcmdiYSgyMzgsIDIzNCwgMTMsIDAuNCk7XG4gIH1cblxuLml0ZW1fcHJvZmlsZXtcbiAgLS1iYWNrZ3JvdW5kOiByZ2JhKDAsIDAsIDAsIDAuMyk7XG4gIGJvcmRlcjogMXB4IHNvbGlkIzFkYzFlNjtcbiAgYm9yZGVyLXJhZGl1czogMzBweDtcbn1cbi5jbG9zZV9pY29ue1xuICB6b29tOiAzLjA7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgcG9zaXRpb246IGZpeGVkO1xuICByaWdodDogMiU7XG4gIHRvcDogOC41JTtcbn1cbiAgLnRodW1uYWlsc3tcbiAgICBvdmVyZmxvdy14OiBzY3JvbGw7XG4gICAgb3ZlcmZsb3cteTogaGlkZGVuO1xuICAgIG1hcmdpbi10b3A6IDUlO1xuICAgIC5saXN0LXRodW1ibmFpbHtcbiAgICAgIGhlaWdodDogMTAwJTtcbiAgICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gICAgICAuaW1nLXRodW1iIGltZ3tcbiAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgICAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgICAgIHBhZGRpbmc6IDNweDtcbiAgICAgICAgd2lkdGg6IDQwcHg7XG4gICAgICAgIGhlaWdodDogNDBweDtcbiAgICAgICAgbWFyZ2luOjAgMnB4IDAgMDsgXG4gICAgICAgIGxpbmUtaGVpZ2h0OiA2MHB4O1xuICAgICAgfVxuICAgIH1cbiAgfVxuICA6Oi13ZWJraXQtc2Nyb2xsYmFyIHsgXG4gICAgZGlzcGxheTogbm9uZTsgXG4gIH1cbiAgLm15SGVhcnRGb2xsb3d7XG4gICAgd2lkdGg6IDIzJSAhaW1wb3J0YW50O1xuICAgIGhlaWdodDogYXV0bztcbiAgICBjb250ZW50OnVybChcIi9hc3NldHMvaWNvbi9mb2xsb3dlZC5wbmdcIik7XG4gIH1cbiAgLm15SGVhcnRVbmZvbGxvd3tcbiAgICB3aWR0aDogMjMlICFpbXBvcnRhbnQ7XG4gICAgaGVpZ2h0OiBhdXRvO1xuICAgIGNvbnRlbnQ6dXJsKFwiL2Fzc2V0cy9pY29uL2ZvbGxvdy5wbmdcIik7XG4gIH1cbiAgLmx2bG51bXtcbiAgICBtYXJnaW46IDA7XG4gICAgbWFyZ2luLXRvcDogNCU7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbiAgICBmb250LWZhbWlseTogVmVyZGFuYSwgR2VuZXZhLCBUYWhvbWEsIHNhbnMtc2VyaWY7XG4gIH1cbiAgaW9uLWl0ZW0ge1xuICAgIC0tcGFkZGluZy1zdGFydDogMCUgIWltcG9ydGFudDtcbn1cbi5icml4eS1pY29ue1xuICB3aWR0aDogMjMlICFpbXBvcnRhbnQ7XG4gIGhlaWdodDogYXV0bztcbn1cblxuIiwiLm15LW92ZXJsYXkge1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIHdpZHRoOiAxMDAlO1xuICB6LWluZGV4OiAyMDtcbn1cblxuLm92ZXJsYXlfbWVzc2FnZSB7XG4gIGhlaWdodDogMzAwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW4tcmV2ZXJzZTtcbiAgYWxpZ24taXRlbXM6IGZsZXgtZW5kO1xuICBvdmVyZmxvdy15OiBhdXRvO1xuICBwYWRkaW5nLWxlZnQ6IDU1cHg7XG4gIHBhZGRpbmctcmlnaHQ6IDJweDtcbiAgei1pbmRleDogOTk7XG59XG4ub3ZlcmxheV9tZXNzYWdlIHAge1xuICBwYWRkaW5nOiA4cHg7XG4gIGJhY2tncm91bmQ6IHJnYmEoMCwgMCwgMCwgMC40KTtcbiAgYm9yZGVyLXJhZGl1czogNTBweDtcbn1cblxuLmNvbW1hbmRlckNsYXNzIHtcbiAgYm9yZGVyOiAzcHggc29saWQgcmdiYSgyNDUsIDUsIDUsIDAuNCk7XG4gIGNvbG9yOiByZ2JhKDI0NSwgNSwgNSwgMC40KTtcbn1cblxuLmdlbmVyYWxDbGFzcyB7XG4gIGJvcmRlcjogM3B4IHNvbGlkIHJnYmEoMTgsIDQsIDIxMiwgMC40KTtcbiAgY29sb3I6IHJnYmEoMTgsIDQsIDIxMiwgMC40KTtcbn1cblxuLmNhcHRhaW5DbGFzcyB7XG4gIGJvcmRlcjogM3B4IHNvbGlkIHJnYmEoMjM4LCAyMzQsIDEzLCAwLjQpO1xuICBjb2xvcjogcmdiYSgyMzgsIDIzNCwgMTMsIDAuNCk7XG59XG5cbi5jb21tYW5kZXJDbGFzc1RodW1ibmFpbCB7XG4gIGJvcmRlcjogMXB4IHNvbGlkIHJnYmEoMjQ1LCA1LCA1LCAwLjQpO1xufVxuXG4uZ2VuZXJhbENsYXNzVGh1bWJuYWlsIHtcbiAgYm9yZGVyOiAxcHggc29saWQgcmdiYSgxOCwgNCwgMjEyLCAwLjQpO1xufVxuXG4uY2FwdGFpbkNsYXNzVGh1bWJuYWlsIHtcbiAgYm9yZGVyOiAxcHggc29saWQgcmdiYSgyMzgsIDIzNCwgMTMsIDAuNCk7XG59XG5cbi5jb21tYW5kZXJBdmF0YXIge1xuICBib3JkZXI6IDNweCBzb2xpZCByZ2JhKDI0NSwgNSwgNSwgMC40KTtcbiAgY29sb3I6IHJnYmEoMjQ1LCA1LCA1LCAwLjQpO1xufVxuXG4uZ2VuZXJhbEF2YXRhciB7XG4gIGJvcmRlcjogM3B4IHNvbGlkIHJnYmEoMTgsIDQsIDIxMiwgMC40KTtcbiAgY29sb3I6IHJnYmEoMTgsIDQsIDIxMiwgMC40KTtcbn1cblxuLmNhcHRhaW5BdmF0YXIge1xuICBib3JkZXI6IDNweCBzb2xpZCByZ2JhKDIzOCwgMjM0LCAxMywgMC40KTtcbiAgY29sb3I6IHJnYmEoMjM4LCAyMzQsIDEzLCAwLjQpO1xufVxuXG4uaXRlbV9wcm9maWxlIHtcbiAgLS1iYWNrZ3JvdW5kOiByZ2JhKDAsIDAsIDAsIDAuMyk7XG4gIGJvcmRlcjogMXB4IHNvbGlkICMxZGMxZTY7XG4gIGJvcmRlci1yYWRpdXM6IDMwcHg7XG59XG5cbi5jbG9zZV9pY29uIHtcbiAgem9vbTogMztcbiAgY29sb3I6IHdoaXRlO1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIHJpZ2h0OiAyJTtcbiAgdG9wOiA4LjUlO1xufVxuXG4udGh1bW5haWxzIHtcbiAgb3ZlcmZsb3cteDogc2Nyb2xsO1xuICBvdmVyZmxvdy15OiBoaWRkZW47XG4gIG1hcmdpbi10b3A6IDUlO1xufVxuLnRodW1uYWlscyAubGlzdC10aHVtYm5haWwge1xuICBoZWlnaHQ6IDEwMCU7XG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XG59XG4udGh1bW5haWxzIC5saXN0LXRodW1ibmFpbCAuaW1nLXRodW1iIGltZyB7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICBwYWRkaW5nOiAzcHg7XG4gIHdpZHRoOiA0MHB4O1xuICBoZWlnaHQ6IDQwcHg7XG4gIG1hcmdpbjogMCAycHggMCAwO1xuICBsaW5lLWhlaWdodDogNjBweDtcbn1cblxuOjotd2Via2l0LXNjcm9sbGJhciB7XG4gIGRpc3BsYXk6IG5vbmU7XG59XG5cbi5teUhlYXJ0Rm9sbG93IHtcbiAgd2lkdGg6IDIzJSAhaW1wb3J0YW50O1xuICBoZWlnaHQ6IGF1dG87XG4gIGNvbnRlbnQ6IHVybChcIi9hc3NldHMvaWNvbi9mb2xsb3dlZC5wbmdcIik7XG59XG5cbi5teUhlYXJ0VW5mb2xsb3cge1xuICB3aWR0aDogMjMlICFpbXBvcnRhbnQ7XG4gIGhlaWdodDogYXV0bztcbiAgY29udGVudDogdXJsKFwiL2Fzc2V0cy9pY29uL2ZvbGxvdy5wbmdcIik7XG59XG5cbi5sdmxudW0ge1xuICBtYXJnaW46IDA7XG4gIG1hcmdpbi10b3A6IDQlO1xuICBmb250LXdlaWdodDogYm9sZGVyO1xuICBmb250LWZhbWlseTogVmVyZGFuYSwgR2VuZXZhLCBUYWhvbWEsIHNhbnMtc2VyaWY7XG59XG5cbmlvbi1pdGVtIHtcbiAgLS1wYWRkaW5nLXN0YXJ0OiAwJSAhaW1wb3J0YW50O1xufVxuXG4uYnJpeHktaWNvbiB7XG4gIHdpZHRoOiAyMyUgIWltcG9ydGFudDtcbiAgaGVpZ2h0OiBhdXRvO1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/viewlive/viewlive.page.ts":
  /*!*******************************************!*\
    !*** ./src/app/viewlive/viewlive.page.ts ***!
    \*******************************************/

  /*! exports provided: ViewlivePage */

  /***/
  function srcAppViewliveViewlivePageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ViewlivePage", function () {
      return ViewlivePage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var ngx_socket_io__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ngx-socket-io */
    "./node_modules/ngx-socket-io/fesm2015/ngx-socket-io.js");
    /* harmony import */


    var _allgifts_allgifts_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../allgifts/allgifts.page */
    "./src/app/allgifts/allgifts.page.ts");
    /* harmony import */


    var _liveprofile_liveprofile_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../liveprofile/liveprofile.page */
    "./src/app/liveprofile/liveprofile.page.ts");
    /* harmony import */


    var _providers_credential_provider__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ../../providers/credential-provider */
    "./src/providers/credential-provider.ts");

    var ViewlivePage = /*#__PURE__*/function () {
      function ViewlivePage(router, modalController, activatedRoute, loadingCtrl, storage, socket, postPvdr, navCtrl) {
        _classCallCheck(this, ViewlivePage);

        this.router = router;
        this.modalController = modalController;
        this.activatedRoute = activatedRoute;
        this.loadingCtrl = loadingCtrl;
        this.storage = storage;
        this.socket = socket;
        this.postPvdr = postPvdr;
        this.navCtrl = navCtrl;
        this.message = '';
        this.messages = [];
        this.vip_package = "";
        this.viewersPic = [];
        this.followClass = "myHeartFollow";
        this.status = false;
        this.vip_packageOfUser = "";
      }

      _createClass(ViewlivePage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var paramId = this.activatedRoute.snapshot.paramMap.get("id");
          console.log("paramId:" + paramId);
          var res = paramId.split("_");
          this.livestreamId = res[0];
          this.stream_key = res[1];
          console.log("livestreamId:" + this.livestreamId);
          console.log("stream_keykeykey:" + this.stream_key);
          this.plotDetails();
          this.start(this.stream_key);
          this.checkSubscription();
        }
      }, {
        key: "loading",
        value: function loading(image) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    _context.next = 2;
                    return this.loadingCtrl.create({
                      message: '<img src="' + image + '" style="height: 25px" height="auto" alt="loading...">',
                      translucent: true,
                      showBackdrop: false,
                      spinner: null,
                      duration: 2000
                    });

                  case 2:
                    this.loader = _context.sent;
                    this.loader.present();

                  case 4:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        }
      }, {
        key: "checkSubscription",
        value: function checkSubscription() {
          var _this = this;

          var body2 = {
            action: 'checkSubscription',
            user_id: this.livestreamId
          };
          this.postPvdr.postData(body2, 'subscription.php').subscribe(function (data) {
            if (data.success) {
              for (var key in data.result) {
                _this.vip_package = data.result[key].vip_package_id;
              }
            }
          });
        }
      }, {
        key: "ngAfterViewInit",
        value: function ngAfterViewInit() {}
      }, {
        key: "openGifts",
        value: function openGifts() {
          this.router.navigate(['allgifts']);
        }
      }, {
        key: "goTab5",
        value: function goTab5() {
          this.navCtrl.navigateForward(['/message/' + this.livestreamId]);
        }
      }, {
        key: "goHome",
        value: function goHome() {
          this.router.navigate(['tabs']);
        }
      }, {
        key: "goStore",
        value: function goStore() {
          this.router.navigate(['tab4']);
        }
      }, {
        key: "ionViewWillLeave",
        value: function ionViewWillLeave() {
          console.log("will leave message and disconnect to socket");
          this.socket.disconnect();
        }
      }, {
        key: "plotDetails",
        value: function plotDetails() {
          var _this2 = this;

          console.log("initialize plot details");
          var body = {
            action: 'getUser_liveData',
            user_id: this.livestreamId
          };
          this.postPvdr.postData(body, 'credentials-api.php').subscribe(function (data) {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var key;
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      if (data.success) {
                        for (key in data.result) {
                          this.user_id = data.result[key].user_id;
                          this.fname = data.result[key].fname;
                          this.lname = data.result[key].lname;
                          this.city = data.result[key].city;
                          this.user_photo = data.result[key].profile_photo == '' ? '' : this.postPvdr.myServer() + "/brixy-live/images/" + data.result[key].profile_photo;
                          this.user_level = data.result[key].user_level;
                          this.followers = data.result[key].followers;
                          this.following = data.result[key].following;
                        }
                      }

                    case 1:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          });
          this.plotFollowStatus();
          this.storage.get("user_id").then(function (user_id) {
            _this2.login_user_id = user_id;
            var body2 = {
              action: 'getuserdata',
              user_id: user_id
            };

            _this2.postPvdr.postData(body2, 'credentials-api.php').subscribe(function (data) {
              return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
                var _this3 = this;

                var body3;
                return regeneratorRuntime.wrap(function _callee3$(_context3) {
                  while (1) {
                    switch (_context3.prev = _context3.next) {
                      case 0:
                        if (data.success) {
                          if (data.result.nickname == "") {
                            if (data.result.login_type_id == "3") {
                              this.login_nickname = data.result.mobile_num;
                            } else if (data.result.login_type_id == "4") {
                              this.login_nickname = data.result.email;
                            } else {
                              this.login_nickname = data.result.fname;
                            }
                          } else {
                            this.login_nickname = data.result.nickname;
                          }

                          this.login_profile_photo = data.result.photo;
                          body3 = {
                            action: 'checkSubscription',
                            user_id: user_id
                          };
                          this.postPvdr.postData(body3, 'subscription.php').subscribe(function (data) {
                            if (data.success) {
                              for (var key in data.result) {
                                _this3.vip_packageOfUser = data.result[key].vip_package_id;
                              }

                              if (_this3.vip_packageOfUser == '1') {
                                _this3.loading("assets/gif/boquet.gif");
                              } else if (_this3.vip_packageOfUser == '2') {
                                _this3.loading("assets/gif/highfive.gif");
                              } else if (_this3.vip_packageOfUser == '3') {
                                _this3.loading("assets/gif/puppy.gif");
                              }
                            }

                            _this3.socket.connect(); // Join chatroom


                            console.log("user_id:" + _this3.user_id + "room:" + _this3.stream_key);

                            _this3.socket.emit('joinRoom', {
                              user_id: _this3.login_nickname,
                              room: _this3.stream_key,
                              profile_picture: _this3.login_profile_photo,
                              vip_packageOfUser: _this3.vip_packageOfUser
                            });

                            _this3.socket.fromEvent('message').subscribe(function (message) {
                              _this3.messages.push(message);

                              console.log("human og send" + JSON.stringify(message));
                            });

                            _this3.socket.fromEvent('joinRoomMessage').subscribe(function (message) {
                              console.log("message from server:" + JSON.stringify(message));

                              _this3.messages.push(message);

                              _this3.addPictureViewer(message['profile_picture'], message['vip_packageOfUser']);
                            });

                            _this3.socket.fromEvent('leaveRoom').subscribe(function (message) {
                              console.log("message from server leave:" + JSON.stringify(message));

                              _this3.messages.push(message);
                            });
                          });
                        } // end of success


                      case 1:
                      case "end":
                        return _context3.stop();
                    }
                  }
                }, _callee3, this);
              }));
            });
          }); // end of user_id storage
        }
      }, {
        key: "addPictureViewer",
        value: function addPictureViewer(joinPic, vip_packageOfUser) {
          console.log("picture of those who join:" + joinPic);
          var viewPic = joinPic == '' ? "assets/icon/brixylogo.png" : this.postPvdr.myServer() + "/brixy-live/images/" + joinPic;
          var body = {
            viewerPic: viewPic,
            vip_packageOfUser: vip_packageOfUser
          };
          this.viewersPic.push(body);
          console.log("vi:" + JSON.stringify(this.viewersPic));
        }
      }, {
        key: "sendMessage",
        value: function sendMessage() {
          var _this4 = this;

          this.socket.emit('send-message', {
            user_id: this.login_nickname + " : ",
            room: this.stream_key,
            text: this.message,
            vip_packageOfUser: this.vip_packageOfUser
          });
          var body = {
            action: 'insertConvoInGC',
            user_id: this.login_user_id,
            stream_id: this.stream_key,
            message: this.message
          };
          this.postPvdr.postData(body, 'messages.php').subscribe(function (data) {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this4, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
              return regeneratorRuntime.wrap(function _callee4$(_context4) {
                while (1) {
                  switch (_context4.prev = _context4.next) {
                    case 0:
                      if (data.success) {}

                    case 1:
                    case "end":
                      return _context4.stop();
                  }
                }
              }, _callee4);
            }));
          });
          this.message = '';
        }
      }, {
        key: "openAllgifts",
        value: function openAllgifts() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
            var modal;
            return regeneratorRuntime.wrap(function _callee5$(_context5) {
              while (1) {
                switch (_context5.prev = _context5.next) {
                  case 0:
                    _context5.next = 2;
                    return this.modalController.create({
                      component: _allgifts_allgifts_page__WEBPACK_IMPORTED_MODULE_6__["AllgiftsPage"],
                      cssClass: 'allgiftsmodalstyle',
                      componentProps: {
                        'live_user_id': this.livestreamId
                      }
                    });

                  case 2:
                    modal = _context5.sent;
                    _context5.next = 5;
                    return modal.present();

                  case 5:
                    return _context5.abrupt("return", _context5.sent);

                  case 6:
                  case "end":
                    return _context5.stop();
                }
              }
            }, _callee5, this);
          }));
        }
      }, {
        key: "openliveprofile",
        value: function openliveprofile() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee6() {
            var _this5 = this;

            var modal;
            return regeneratorRuntime.wrap(function _callee6$(_context6) {
              while (1) {
                switch (_context6.prev = _context6.next) {
                  case 0:
                    _context6.next = 2;
                    return this.modalController.create({
                      component: _liveprofile_liveprofile_page__WEBPACK_IMPORTED_MODULE_7__["LiveprofilePage"],
                      cssClass: 'liveprofilemodalstyle',
                      componentProps: {
                        liveStreamProfileId: this.livestreamId
                      }
                    });

                  case 2:
                    modal = _context6.sent;
                    modal.onDidDismiss().then(function (data) {
                      //const user = data['data']; // Here's your selected user!
                      console.log("dismiss of liveprofilepage modal");

                      _this5.plotFollowStatus();
                    });
                    _context6.next = 6;
                    return modal.present();

                  case 6:
                    return _context6.abrupt("return", _context6.sent);

                  case 7:
                  case "end":
                    return _context6.stop();
                }
              }
            }, _callee6, this);
          }));
        }
      }, {
        key: "plotFollowStatus",
        value: function plotFollowStatus() {
          var _this6 = this;

          console.log("plotFollowStatus");
          this.storage.get('user_id').then(function (user_id) {
            var body2 = {
              action: 'followerStatus',
              followed_by: user_id,
              user_id: _this6.livestreamId
            };

            _this6.postPvdr.postData(body2, 'followers.php').subscribe(function (data) {
              return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this6, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee7() {
                return regeneratorRuntime.wrap(function _callee7$(_context7) {
                  while (1) {
                    switch (_context7.prev = _context7.next) {
                      case 0:
                        if (data.success) {
                          if (data.result == "1") {
                            this.status = true;
                            console.log("this.status start:" + this.status);
                          } else {
                            this.status = false;
                          }
                        }

                      case 1:
                      case "end":
                        return _context7.stop();
                    }
                  }
                }, _callee7, this);
              }));
            });
          });
        }
      }, {
        key: "start",
        value: function start(streamKey) {
          var pc_config = {
            'iceServers': [{
              'urls': 'stun:stun.l.google.com:19302'
            }]
          };
          var sdpConstraints = {
            OfferToReceiveAudio: true,
            OfferToReceiveVideo: true
          };
          var mediaConstraints = {
            video: false,
            audio: false
          }; // var appName = location.pathname.substring(0, location.pathname.lastIndexOf("/")+1);
          //var websocketURL = "wss://brixylive.com:5443/WebRTCAppEE/websocket";

          var websocketURL = "ws://livebrixylive.com:5080/WebRTCAppEE/websocket";
          var webRTCAdaptor = new WebRTCAdaptor({
            websocket_url: websocketURL,
            mediaConstraints: mediaConstraints,
            peerconnection_config: pc_config,
            sdp_constraints: sdpConstraints,
            remoteVideoId: "remoteVideo",
            isPlayMode: true,
            callback: function callback(info) {
              if (info == "initialized") {
                console.log("initialized:" + streamKey);
                webRTCAdaptor.getStreamInfo(streamKey); //webRTCAdaptor.play(streamKey);
              } else if (info == "streamInformation") {
                console.log("stream information");
                webRTCAdaptor.play(streamKey);
              } else if (info == "joined") {
                //joined the stream
                console.log("joined");
                alert("joined"); //	  join_button.disabled = true;
                //	  leave_button.disabled = false;
              } else if (info == "leaved") {
                //leaved the stream
                console.log("leaved"); //	  join_button.disabled = false;
                //	  leave_button.disabled = true;
              }
            },
            callbackError: function callbackError(error) {
              //some of the possible errors, NotFoundError, SecurityError,PermissionDeniedError
              console.log("error callback: " + error);
              alert(error);
            }
          });
        }
      }, {
        key: "follow",
        value: function follow() {
          var _this7 = this;

          this.storage.get('user_id').then(function (user_id) {
            console.log("user_id follow:" + user_id);
            var body = {
              action: 'follow',
              user_id: _this7.user_id,
              followed_by: user_id,
              status: _this7.status
            };

            _this7.postPvdr.postData(body, 'followers.php').subscribe(function (data) {
              console.log(data);

              if (data.success) {
                _this7.followClass = "myHeartFollow";
                _this7.status = !_this7.status;
              }
            });
          });
        }
      }]);

      return ViewlivePage;
    }();

    ViewlivePage.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"]
      }, {
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"]
      }, {
        type: ngx_socket_io__WEBPACK_IMPORTED_MODULE_5__["Socket"]
      }, {
        type: _providers_credential_provider__WEBPACK_IMPORTED_MODULE_8__["PostProvider"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"]
      }];
    };

    ViewlivePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-viewlive',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./viewlive.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/viewlive/viewlive.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./viewlive.page.scss */
      "./src/app/viewlive/viewlive.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"], _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"], ngx_socket_io__WEBPACK_IMPORTED_MODULE_5__["Socket"], _providers_credential_provider__WEBPACK_IMPORTED_MODULE_8__["PostProvider"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"]])], ViewlivePage);
    /***/
  }
}]);
//# sourceMappingURL=viewlive-viewlive-module-es5.js.map