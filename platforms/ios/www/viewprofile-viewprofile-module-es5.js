function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["viewprofile-viewprofile-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/viewprofile/viewprofile.page.html":
  /*!*****************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/viewprofile/viewprofile.page.html ***!
    \*****************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppViewprofileViewprofilePageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header mode=\"ios\" no-border>\n  <ion-toolbar mode=\"ios\">\n      <ion-buttons slot=\"secondary\" (click)=\"goBack()\">\n        <ion-button mode=\"ios\" style=\"font-size: 15px;\">\n            Back\n        </ion-button>\n      </ion-buttons>\n      <ion-buttons slot=\"primary\" (click)=\"goHelp()\">\n        <ion-button mode=\"ios\" style=\"font-size: 15px;\">\n            Help\n        </ion-button>\n      </ion-buttons>\n    </ion-toolbar>\n</ion-header>\n<ion-content>\n  <ion-row>\n    <ion-col text-center>\n        <ion-avatar style=\"margin: auto;\">\n          <img src=\"assets/greenthumb-images/userpicdark.png\">\n          <ion-icon mode=\"ios\" (click)=\"update_photo()\" class=\"upload_photo\" name=\"camera\"></ion-icon>\n        </ion-avatar>\n        <ion-label>\n          <h2 class=\"for_selname\"><b>{{username}}</b></h2>\n          <h3 class=\"for_location\">Tacloban City, Philippines</h3>\n          <ion-row>\n            <ion-col text-center no-padding>\n              <!-- <ion-icon class=\"for_staricon\" name=\"star\" mode=\"ios\" size=\"medium\" color=\"warning\"></ion-icon>\n              <ion-icon class=\"for_staricon\" name=\"star\" mode=\"ios\" size=\"medium\" color=\"warning\"></ion-icon>\n              <ion-icon class=\"for_staricon\" name=\"star\" mode=\"ios\" size=\"medium\" color=\"warning\"></ion-icon>\n              <ion-icon class=\"for_staricon\" name=\"star\" mode=\"ios\" size=\"medium\" color=\"warning\"></ion-icon>\n              <ion-icon class=\"for_staricon\" name=\"star-half\" mode=\"ios\" size=\"medium\" color=\"warning\"></ion-icon>&nbsp; -->\n              <ionic4-star-rating #rating\n                          activeIcon = \"ios-star\"\n                          defaultIcon = \"ios-star-outline\"\n                          activeColor = \"#ffce00\" \n                          defaultColor = \"#ffce00\"\n                          readonly=\"false\"\n                          rating=\"{{user_rating}}\"\n                          fontSize = \"15px\">\n                        </ionic4-star-rating>\n              <!-- <span style=\"font-size: 15px;\">({{user_rating}})</span> -->\n            </ion-col>\n          </ion-row>\n        </ion-label>\n    </ion-col>\n  </ion-row>\n  <ion-row padding class=\"for_sortfeed\">\n    <ion-col size=\"6\">\n      <ion-item mode=\"ios\" lines=\"none\" style=\"--padding-start: 0;\">\n        <ion-select text-right expand=\"block\" style=\"max-width: 100%;\"  mode=\"ios\" okText=\"Okay\" cancelText=\"Dismiss\">\n          <ion-label>Sort feed</ion-label>\n          <ion-select-option value=\"Sort feed\" selected>Sort feed</ion-select-option>\n          <ion-select-option value=\"Latest\">Latest</ion-select-option>\n          <ion-select-option value=\"Local Pickup\">Local Pickup</ion-select-option>\n          <ion-select-option value=\"Shipping\">Shipping</ion-select-option>\n          <ion-select-option value=\"Sellers I follow\">Sellers I follow</ion-select-option>\n          <ion-select-option value=\"Closest Location\">Closest Location</ion-select-option>\n        </ion-select>\n      </ion-item>\n    </ion-col>\n    <ion-col size=\"6\" text-right>\n      <ion-item text-right mode=\"ios\" lines=\"none\" style=\"--padding-start: 0;position: absolute;right: 0;\">\n        <ion-select text-right expand=\"block\" style=\"max-width: 100%;\"  mode=\"ios\" okText=\"Okay\" cancelText=\"Dismiss\">\n          <ion-label>Price</ion-label>\n          <ion-select-option value=\"Any\" selected>Price: Any</ion-select-option>\n          <ion-select-option value=\"Lowest to Highest\">Lowest to Highest</ion-select-option>\n          <ion-select-option value=\"Highest to Lowest\">Highest to Lowest</ion-select-option>\n        </ion-select>\n      </ion-item>\n    </ion-col>\n  </ion-row>\n\n      \n    <!-- List of items -->\n\n\n    <ion-row *ngFor=\"let item of itemList\" >\n      <ion-col>\n        <ion-card mode=\"ios\" class=\"for_items\" >\n          <ion-card-content no-padding>\n            <ion-row>\n              <ion-col text-left size=\"5\">\n                <ion-item lines=\"none\" class=\"for_itemms\" (click)=\"goSellerprof(item.user_id)\">\n                  <ion-avatar slot=\"start\">\n                    <img src=\"assets/greenthumb-images/userpic.png\">\n                  </ion-avatar>\n                  <ion-label class=\"for_name\">{{item.username}}</ion-label>\n                </ion-item>\n                <img (click)=\"goProductview(item.id)\" class=\"for_itemmimg\" src=\"assets/greenthumb-images/banana.png\">\n              </ion-col>\n              <ion-col text-left size=\"7\">\n                <ion-row (click)=\"goProductview(item.id)\">\n                  <ion-col no-padding text-left>\n                    <div class=\"for_unitqty\">3000 pieces</div>\n                    <ion-label color=\"secondary\"><h1 class=\"for_pprice\">${{item.price}}</h1></ion-label>\n                  </ion-col>\n                  <ion-col no-padding text-right>\n                    <div class=\"for_category\">{{item.category}}</div>\n                    <div style=\"margin-top: 5px;\">\n                      <ionic4-star-rating #rating\n                          activeIcon = \"ios-star\"\n                          defaultIcon = \"ios-star-outline\"\n                          activeColor = \"#ffce00\" \n                          defaultColor = \"#ffce00\"\n                          readonly=\"false\"\n                          rating=\"{{item.user_rating}}\"\n                          fontSize = \"15px\">\n                        </ionic4-star-rating>\n                    </div>\n                  </ion-col>\n                </ion-row>\n                <ion-row style=\"margin-top: -2px;\" (click)=\"goProductview(item.id)\">\n                  <ion-col no-padding text-left>\n                    <ion-label color=\"dark\">\n                      <h2 class=\"for_itemmname\">{{item.title}}</h2></ion-label>\n                  </ion-col>\n                </ion-row>\n                \n                <ion-label (click)=\"goProductview(item.id)\" color=\"dark\"><h2 style=\"padding-bottom: 2.5%;\" class=\"for_itemmlocation\">\n                  <ion-icon class=\"for_pin\" mode=\"ios\" color=\"primary\" name=\"pin\"></ion-icon>&nbsp;{{item.location}}\n                </h2></ion-label>\n                <ion-row>\n                  <ion-col no-padding text-left size=\"12\" class=\"for_borderitem\" (click)=\"goProductview(item.id)\">\n                    <ion-row>\n                      <ion-col text-left no-padding>\n                        <ion-label color=\"secondary\"><h2 class=\"for_itemmbtn\">\n                            &nbsp;{{item.date}}\n                          </h2></ion-label>\n                      </ion-col>\n                      <ion-col text-left no-padding>\n                        <ion-label color=\"secondary\"><h2 class=\"for_itemmbtn\">\n                            &nbsp;{{item.time}}\n                          </h2></ion-label>\n                      </ion-col>\n                    </ion-row>\n                  </ion-col>\n                </ion-row>\n              </ion-col>\n            </ion-row>\n          </ion-card-content>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n\n    <!-- <ion-row>\n      <ion-col>\n        <ion-card mode=\"ios\" class=\"for_items\" (click)=\"goProductview()\">\n          <ion-card-content no-padding>\n            <ion-row style=\"margin-left: -2%;\">\n              <ion-col text-left size=\"5\" style=\"padding-right: 0;\">\n                <ion-chip class=\"ion-margin-start for_chip\">\n                  <ion-avatar>\n                    <img src=\"assets/greenthumb-images/userpic.png\">\n                  </ion-avatar>\n                  <ion-label style=\"font-size: 12px;\" color=\"secondary\">by Seller Name</ion-label>\n                </ion-chip><br>\n                  <img class=\"for_itemimg\" src=\"assets/greenthumb-images/banana.png\">\n              </ion-col>\n              <ion-col text-left size=\"7\">\n                <ion-label color=\"secondary\"><h1 class=\"for_price\">$600.99</h1></ion-label>\n                <ion-label color=\"dark\"><h2 class=\"for_itemname\">Lorem Item Name</h2></ion-label>\n                <ion-label color=\"secondary\"><h6 class=\"for_itemcateg\">Category Name</h6></ion-label>\n                <ion-label color=\"dark\"><h2 class=\"for_itemlocation\">\n                  <ion-icon class=\"for_pin\" mode=\"ios\" color=\"primary\" name=\"pin\"></ion-icon>&nbsp;New York, New York City\n                </h2></ion-label>\n                <ion-row>\n                  <ion-col text-left>\n                    <a href=\"#\" style=\"text-decoration: none;\">\n                      <ion-label color=\"secondary\"><h2 class=\"for_itembtn\">\n                        <ion-icon class=\"for_itembtnicon\" mode=\"ios\" color=\"danger\" name=\"heart\"></ion-icon>&nbsp;Follow\n                      </h2></ion-label>\n                    </a>\n                  </ion-col>\n                  <ion-col text-left>\n                    <a href=\"#\" style=\"text-decoration: none;\">\n                      <ion-label color=\"secondary\"><h2 class=\"for_itembtn\">\n                        <ion-icon class=\"for_itembtnicon\" mode=\"ios\" color=\"primary\" name=\"heart\"></ion-icon>&nbsp;Save\n                      </h2></ion-label>\n                    </a>\n                  </ion-col>\n                </ion-row>\n              </ion-col>\n            </ion-row>\n          </ion-card-content>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col>\n        <ion-card mode=\"ios\" class=\"for_items\" (click)=\"goProductview()\">\n          <ion-card-content no-padding>\n            <ion-row style=\"margin-left: -2%;\">\n              <ion-col text-left size=\"5\" style=\"padding-right: 0;\">\n                <ion-chip class=\"ion-margin-start for_chip\">\n                  <ion-avatar>\n                    <img src=\"assets/greenthumb-images/userpic.png\">\n                  </ion-avatar>\n                  <ion-label style=\"font-size: 12px;\" color=\"secondary\">by Seller Name</ion-label>\n                </ion-chip><br>\n                  <img class=\"for_itemimg\" src=\"assets/greenthumb-images/apple.png\">\n              </ion-col>\n              <ion-col text-left size=\"7\">\n                <ion-label color=\"secondary\"><h1 class=\"for_price\">$600.99</h1></ion-label>\n                <ion-label color=\"dark\"><h2 class=\"for_itemname\">Lorem Item Name</h2></ion-label>\n                <ion-label color=\"secondary\"><h6 class=\"for_itemcateg\">Category Name</h6></ion-label>\n                <ion-label color=\"dark\"><h2 class=\"for_itemlocation\">\n                  <ion-icon class=\"for_pin\" mode=\"ios\" color=\"primary\" name=\"pin\"></ion-icon>&nbsp;New York, New York City\n                </h2></ion-label>\n                <ion-row>\n                  <ion-col text-left>\n                    <a href=\"#\" style=\"text-decoration: none;\">\n                      <ion-label color=\"secondary\"><h2 class=\"for_itembtn\">\n                        <ion-icon class=\"for_itembtnicon\" mode=\"ios\" color=\"danger\" name=\"heart\"></ion-icon>&nbsp;Follow\n                      </h2></ion-label>\n                    </a>\n                  </ion-col>\n                  <ion-col text-left>\n                    <a href=\"#\" style=\"text-decoration: none;\">\n                      <ion-label color=\"secondary\"><h2 class=\"for_itembtn\">\n                        <ion-icon class=\"for_itembtnicon\" mode=\"ios\" color=\"primary\" name=\"heart\"></ion-icon>&nbsp;Save\n                      </h2></ion-label>\n                    </a>\n                  </ion-col>\n                </ion-row>\n              </ion-col>\n            </ion-row>\n          </ion-card-content>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col>\n        <ion-card mode=\"ios\" class=\"for_items\" (click)=\"goProductview()\">\n          <ion-card-content no-padding>\n            <ion-row style=\"margin-left: -2%;\">\n              <ion-col text-left size=\"5\" style=\"padding-right: 0;\">\n                <ion-chip class=\"ion-margin-start for_chip\">\n                  <ion-avatar>\n                    <img src=\"assets/greenthumb-images/userpic.png\">\n                  </ion-avatar>\n                  <ion-label style=\"font-size: 12px;\" color=\"secondary\">by Seller Name</ion-label>\n                </ion-chip><br>\n                  <img class=\"for_itemimg\" src=\"assets/greenthumb-images/orange.png\">\n              </ion-col>\n              <ion-col text-left size=\"7\">\n                <ion-label color=\"secondary\"><h1 class=\"for_price\">$600.99</h1></ion-label>\n                <ion-label color=\"dark\"><h2 class=\"for_itemname\">Lorem Item Name</h2></ion-label>\n                <ion-label color=\"secondary\"><h6 class=\"for_itemcateg\">Category Name</h6></ion-label>\n                <ion-label color=\"dark\"><h2 class=\"for_itemlocation\">\n                  <ion-icon class=\"for_pin\" mode=\"ios\" color=\"primary\" name=\"pin\"></ion-icon>&nbsp;New York, New York City\n                </h2></ion-label>\n                <ion-row>\n                  <ion-col text-left>\n                    <a href=\"#\" style=\"text-decoration: none;\">\n                      <ion-label color=\"secondary\"><h2 class=\"for_itembtn\">\n                        <ion-icon class=\"for_itembtnicon\" mode=\"ios\" color=\"danger\" name=\"heart\"></ion-icon>&nbsp;Follow\n                      </h2></ion-label>\n                    </a>\n                  </ion-col>\n                  <ion-col text-left>\n                    <a href=\"#\" style=\"text-decoration: none;\">\n                      <ion-label color=\"secondary\"><h2 class=\"for_itembtn\">\n                        <ion-icon class=\"for_itembtnicon\" mode=\"ios\" color=\"primary\" name=\"heart\"></ion-icon>&nbsp;Save\n                      </h2></ion-label>\n                    </a>\n                  </ion-col>\n                </ion-row>\n              </ion-col>\n            </ion-row>\n          </ion-card-content>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col>\n        <ion-card mode=\"ios\" class=\"for_items\" (click)=\"goProductview()\">\n          <ion-card-content no-padding>\n            <ion-row style=\"margin-left: -2%;\">\n              <ion-col text-left size=\"5\" style=\"padding-right: 0;\">\n                <ion-chip class=\"ion-margin-start for_chip\">\n                  <ion-avatar>\n                    <img src=\"assets/greenthumb-images/userpic.png\">\n                  </ion-avatar>\n                  <ion-label style=\"font-size: 12px;\" color=\"secondary\">by Seller Name</ion-label>\n                </ion-chip><br>\n                  <img class=\"for_itemimg\" src=\"assets/greenthumb-images/tomato.png\">\n              </ion-col>\n              <ion-col text-left size=\"7\">\n                <ion-label color=\"secondary\"><h1 class=\"for_price\">$600.99</h1></ion-label>\n                <ion-label color=\"dark\"><h2 class=\"for_itemname\">Lorem Item Name</h2></ion-label>\n                <ion-label color=\"secondary\"><h6 class=\"for_itemcateg\">Category Name</h6></ion-label>\n                <ion-label color=\"dark\"><h2 class=\"for_itemlocation\">\n                  <ion-icon class=\"for_pin\" mode=\"ios\" color=\"primary\" name=\"pin\"></ion-icon>&nbsp;New York, New York City\n                </h2></ion-label>\n                <ion-row>\n                  <ion-col text-left>\n                    <a href=\"#\" style=\"text-decoration: none;\">\n                      <ion-label color=\"secondary\"><h2 class=\"for_itembtn\">\n                        <ion-icon class=\"for_itembtnicon\" mode=\"ios\" color=\"danger\" name=\"heart\"></ion-icon>&nbsp;Follow\n                      </h2></ion-label>\n                    </a>\n                  </ion-col>\n                  <ion-col text-left>\n                    <a href=\"#\" style=\"text-decoration: none;\">\n                      <ion-label color=\"secondary\"><h2 class=\"for_itembtn\">\n                        <ion-icon class=\"for_itembtnicon\" mode=\"ios\" color=\"primary\" name=\"heart\"></ion-icon>&nbsp;Save\n                      </h2></ion-label>\n                    </a>\n                  </ion-col>\n                </ion-row>\n              </ion-col>\n            </ion-row>\n          </ion-card-content>\n        </ion-card>\n      </ion-col>\n    </ion-row> -->\n\n</ion-content>\n";
    /***/
  },

  /***/
  "./src/app/viewprofile/viewprofile-routing.module.ts":
  /*!***********************************************************!*\
    !*** ./src/app/viewprofile/viewprofile-routing.module.ts ***!
    \***********************************************************/

  /*! exports provided: ViewprofilePageRoutingModule */

  /***/
  function srcAppViewprofileViewprofileRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ViewprofilePageRoutingModule", function () {
      return ViewprofilePageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _viewprofile_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./viewprofile.page */
    "./src/app/viewprofile/viewprofile.page.ts");

    var routes = [{
      path: '',
      component: _viewprofile_page__WEBPACK_IMPORTED_MODULE_3__["ViewprofilePage"]
    }];

    var ViewprofilePageRoutingModule = function ViewprofilePageRoutingModule() {
      _classCallCheck(this, ViewprofilePageRoutingModule);
    };

    ViewprofilePageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], ViewprofilePageRoutingModule);
    /***/
  },

  /***/
  "./src/app/viewprofile/viewprofile.module.ts":
  /*!***************************************************!*\
    !*** ./src/app/viewprofile/viewprofile.module.ts ***!
    \***************************************************/

  /*! exports provided: ViewprofilePageModule */

  /***/
  function srcAppViewprofileViewprofileModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ViewprofilePageModule", function () {
      return ViewprofilePageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _viewprofile_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./viewprofile-routing.module */
    "./src/app/viewprofile/viewprofile-routing.module.ts");
    /* harmony import */


    var ionic4_star_rating__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ionic4-star-rating */
    "./node_modules/ionic4-star-rating/dist/index.js");
    /* harmony import */


    var _viewprofile_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./viewprofile.page */
    "./src/app/viewprofile/viewprofile.page.ts");

    var ViewprofilePageModule = function ViewprofilePageModule() {
      _classCallCheck(this, ViewprofilePageModule);
    };

    ViewprofilePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], ionic4_star_rating__WEBPACK_IMPORTED_MODULE_6__["StarRatingModule"], _viewprofile_routing_module__WEBPACK_IMPORTED_MODULE_5__["ViewprofilePageRoutingModule"]],
      declarations: [_viewprofile_page__WEBPACK_IMPORTED_MODULE_7__["ViewprofilePage"]]
    })], ViewprofilePageModule);
    /***/
  },

  /***/
  "./src/app/viewprofile/viewprofile.page.scss":
  /*!***************************************************!*\
    !*** ./src/app/viewprofile/viewprofile.page.scss ***!
    \***************************************************/

  /*! exports provided: default */

  /***/
  function srcAppViewprofileViewprofilePageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".for_selname {\n  font-size: 17px;\n  color: #679733;\n  margin-top: 7px;\n}\n\n.for_location {\n  font-size: 14px;\n  margin-top: -2px;\n}\n\n.for_staricon {\n  font-size: 24px;\n  margin-bottom: -2%;\n}\n\nion-select {\n  font-size: 13px;\n}\n\n.for_sortfeed {\n  padding-left: 0;\n  border-bottom: 1px solid #e2f0cb;\n  padding-bottom: 0;\n  margin-bottom: 10px;\n}\n\n.for_items {\n  margin-top: 0%;\n  box-shadow: 0 1px 10px #e2f0cb;\n  border-radius: 10px;\n  border: 1px solid #e2f0cb;\n  margin-bottom: 0;\n}\n\n.for_chip {\n  --background: transparent;\n  margin: 0;\n  padding-right: 0;\n  padding-left: 15px;\n}\n\n.for_itemimg {\n  padding-left: 5px;\n  margin: auto;\n  width: 90%;\n  height: 70%;\n}\n\n.for_price {\n  font-size: 20px;\n  font-weight: bolder;\n  padding-top: 5px;\n}\n\n.for_itemname {\n  font-size: 15px;\n  font-weight: bolder;\n}\n\n.for_itemcateg {\n  text-transform: uppercase;\n  font-size: 12px;\n  font-weight: lighter;\n}\n\n.for_itemlocation {\n  font-size: 13px;\n  font-weight: lighter;\n}\n\n.for_pin {\n  font-size: 15px;\n  margin-bottom: -1%;\n  margin-left: -1%;\n}\n\n.for_itembtn {\n  font-size: 11px;\n}\n\n.for_itembtnicon {\n  font-size: 20px;\n  margin-bottom: -7%;\n  margin-right: 2%;\n}\n\n.upload_photo {\n  background: #76c961;\n  color: white;\n  padding: 3px;\n  border-radius: 50%;\n  font-size: 18px;\n  position: absolute;\n  top: 40%;\n  left: 53%;\n}\n\n.for_itemms {\n  --padding-start: 0;\n  margin-top: -8px;\n  --inner-padding-end: 0;\n  --background: transparent;\n}\n\n.for_itemms ion-avatar {\n  width: 100% !important;\n  height: 100% !important;\n  max-width: 25px !important;\n  max-height: 25px !important;\n  margin-right: 5px;\n  margin-top: 0;\n}\n\n.for_itemms ion-avatar img {\n  border: 1px solid rgba(0, 0, 0, 0.12);\n  width: 100% !important;\n  height: 100% !important;\n  max-width: 25px !important;\n  max-height: 25px !important;\n  margin-right: 5px;\n  margin-top: 0;\n  border-radius: 50%;\n}\n\n.for_name {\n  font-size: 12px;\n  text-transform: capitalize;\n  margin-top: 0;\n  color: black;\n}\n\n.for_itemmimg {\n  margin: auto;\n  width: 90%;\n  margin-top: -10px;\n  padding-bottom: 5px;\n}\n\n.for_pprice {\n  font-size: 20px;\n  font-weight: bolder;\n}\n\n.for_category {\n  background: #76c961;\n  padding: 3px;\n  border-top-right-radius: 10px;\n  margin-top: -5px;\n  margin-right: -5px;\n  border-bottom-left-radius: 10px;\n  text-transform: capitalize;\n  font-size: 12px;\n  font-weight: lighter;\n  color: white;\n  text-align: center;\n}\n\n.for_itemmname {\n  font-size: 14px;\n  font-weight: bolder;\n}\n\n.for_itemmlocation {\n  font-size: 13px;\n  font-weight: lighter;\n}\n\n.for_borderitem {\n  border-top: 1px solid #e2f0cb;\n  padding-top: 2.5%;\n}\n\n.for_itemmbtn {\n  font-size: 11px;\n  color: #679733;\n}\n\n.for_unitqty {\n  font-size: 11px;\n  text-align: left;\n  color: #989aa2;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZW5kcmljay9EZXNrdG9wL1RlbG1vIFNvbHV0aW9ucy9Gb3JQcm9kdWN0aW9uL21heTEwMjAyMS9sYXN0IHNldHVwL2dyZWVudGh1bWJhcHAgY29weS9zcmMvYXBwL3ZpZXdwcm9maWxlL3ZpZXdwcm9maWxlLnBhZ2Uuc2NzcyIsInNyYy9hcHAvdmlld3Byb2ZpbGUvdmlld3Byb2ZpbGUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksZUFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0FDQ0o7O0FEQ0E7RUFDSSxlQUFBO0VBQ0EsZ0JBQUE7QUNFSjs7QURBQTtFQUNJLGVBQUE7RUFDQSxrQkFBQTtBQ0dKOztBRERFO0VBQ0UsZUFBQTtBQ0lKOztBREZFO0VBQ0UsZUFBQTtFQUNBLGdDQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtBQ0tKOztBREZBO0VBQ0ksY0FBQTtFQUNBLDhCQUFBO0VBQ0EsbUJBQUE7RUFDQSx5QkFBQTtFQUNBLGdCQUFBO0FDS0o7O0FESEE7RUFDSSx5QkFBQTtFQUNBLFNBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0FDTUo7O0FESkE7RUFDSSxpQkFBQTtFQUNBLFlBQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtBQ09KOztBRExBO0VBQ0ksZUFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7QUNRSjs7QUROQTtFQUNJLGVBQUE7RUFDQSxtQkFBQTtBQ1NKOztBRFBBO0VBQ0kseUJBQUE7RUFDQSxlQUFBO0VBQ0Esb0JBQUE7QUNVSjs7QURSQTtFQUNJLGVBQUE7RUFDQSxvQkFBQTtBQ1dKOztBRFRBO0VBQ0ksZUFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7QUNZSjs7QURWQTtFQUNJLGVBQUE7QUNhSjs7QURYQTtFQUNJLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0FDY0o7O0FEWkE7RUFDSSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxRQUFBO0VBQ0EsU0FBQTtBQ2VKOztBRFBFO0VBQ0Usa0JBQUE7RUFDQSxnQkFBQTtFQUNBLHNCQUFBO0VBQ0EseUJBQUE7QUNVSjs7QURUSTtFQUNJLHNCQUFBO0VBQ0EsdUJBQUE7RUFDQSwwQkFBQTtFQUNBLDJCQUFBO0VBQ0EsaUJBQUE7RUFBa0IsYUFBQTtBQ1kxQjs7QURYUTtFQUNJLHFDQUFBO0VBQ0Esc0JBQUE7RUFDQSx1QkFBQTtFQUNBLDBCQUFBO0VBQ0EsMkJBQUE7RUFDQSxpQkFBQTtFQUFrQixhQUFBO0VBQ2xCLGtCQUFBO0FDY1o7O0FEVEE7RUFDSSxlQUFBO0VBQ0EsMEJBQUE7RUFDQSxhQUFBO0VBQ0EsWUFBQTtBQ1lKOztBRFZBO0VBQ0ksWUFBQTtFQUNBLFVBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0FDYUo7O0FEWEE7RUFDSSxlQUFBO0VBQ0EsbUJBQUE7QUNjSjs7QURaQTtFQUNJLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLDZCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLCtCQUFBO0VBQ0EsMEJBQUE7RUFDQSxlQUFBO0VBQ0Esb0JBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7QUNlSjs7QURiQTtFQUNJLGVBQUE7RUFDQSxtQkFBQTtBQ2dCSjs7QURkQTtFQUNJLGVBQUE7RUFDQSxvQkFBQTtBQ2lCSjs7QURmQTtFQUNJLDZCQUFBO0VBQ0EsaUJBQUE7QUNrQko7O0FEaEJBO0VBQ0ksZUFBQTtFQUNBLGNBQUE7QUNtQko7O0FEakJBO0VBQ0ksZUFBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtBQ29CSiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdwcm9maWxlL3ZpZXdwcm9maWxlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5mb3Jfc2VsbmFtZXtcbiAgICBmb250LXNpemU6IDE3cHg7XG4gICAgY29sb3I6ICM2Nzk3MzM7XG4gICAgbWFyZ2luLXRvcDogN3B4O1xufVxuLmZvcl9sb2NhdGlvbntcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgbWFyZ2luLXRvcDogLTJweDtcbn1cbi5mb3Jfc3Rhcmljb257XG4gICAgZm9udC1zaXplOiAyNHB4O1xuICAgIG1hcmdpbi1ib3R0b206IC0yJTtcbiAgfVxuICBpb24tc2VsZWN0e1xuICAgIGZvbnQtc2l6ZTogMTNweDtcbn1cbiAgLmZvcl9zb3J0ZmVlZHtcbiAgICBwYWRkaW5nLWxlZnQ6IDA7XG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNlMmYwY2I7XG4gICAgcGFkZGluZy1ib3R0b206IDA7XG4gICAgbWFyZ2luLWJvdHRvbTogMTBweDtcbn1cblxuLmZvcl9pdGVtc3tcbiAgICBtYXJnaW4tdG9wOiAwJTtcbiAgICBib3gtc2hhZG93OiAwIDFweCAxMHB4ICNlMmYwY2I7XG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjZTJmMGNiO1xuICAgIG1hcmdpbi1ib3R0b206IDA7XG59XG4uZm9yX2NoaXB7XG4gICAgLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgICBtYXJnaW46IDA7XG4gICAgcGFkZGluZy1yaWdodDogMDtcbiAgICBwYWRkaW5nLWxlZnQ6IDE1cHg7XG59XG4uZm9yX2l0ZW1pbWd7XG4gICAgcGFkZGluZy1sZWZ0OiA1cHg7XG4gICAgbWFyZ2luOiBhdXRvO1xuICAgIHdpZHRoOiA5MCU7XG4gICAgaGVpZ2h0OiA3MCU7XG59XG4uZm9yX3ByaWNle1xuICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICBmb250LXdlaWdodDogYm9sZGVyO1xuICAgIHBhZGRpbmctdG9wOiA1cHg7XG59XG4uZm9yX2l0ZW1uYW1le1xuICAgIGZvbnQtc2l6ZTogMTVweDtcbiAgICBmb250LXdlaWdodDogYm9sZGVyO1xufVxuLmZvcl9pdGVtY2F0ZWd7XG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgICBmb250LXNpemU6IDEycHg7XG4gICAgZm9udC13ZWlnaHQ6IGxpZ2h0ZXI7XG59XG4uZm9yX2l0ZW1sb2NhdGlvbntcbiAgICBmb250LXNpemU6IDEzcHg7XG4gICAgZm9udC13ZWlnaHQ6IGxpZ2h0ZXI7XG59XG4uZm9yX3BpbntcbiAgICBmb250LXNpemU6IDE1cHg7XG4gICAgbWFyZ2luLWJvdHRvbTogLTElO1xuICAgIG1hcmdpbi1sZWZ0OiAtMSU7XG59XG4uZm9yX2l0ZW1idG57XG4gICAgZm9udC1zaXplOiAxMXB4Oztcbn1cbi5mb3JfaXRlbWJ0bmljb257XG4gICAgZm9udC1zaXplOiAyMHB4O1xuICAgIG1hcmdpbi1ib3R0b206IC03JTtcbiAgICBtYXJnaW4tcmlnaHQ6IDIlO1xufVxuLnVwbG9hZF9waG90b3tcbiAgICBiYWNrZ3JvdW5kOiAjNzZjOTYxO1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBwYWRkaW5nOiAzcHg7XG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiA0MCU7XG4gICAgbGVmdDogNTMlO1xuICB9XG5cblxuICBcblxuICBcblxuICAuZm9yX2l0ZW1tc3tcbiAgICAtLXBhZGRpbmctc3RhcnQ6IDA7XG4gICAgbWFyZ2luLXRvcDogLThweDtcbiAgICAtLWlubmVyLXBhZGRpbmctZW5kOiAwO1xuICAgIC0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG4gICAgaW9uLWF2YXRhciAgeyAgICAgXG4gICAgICAgIHdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XG4gICAgICAgIGhlaWdodDogMTAwJSAhaW1wb3J0YW50O1xuICAgICAgICBtYXgtd2lkdGg6IDI1cHggIWltcG9ydGFudDtcbiAgICAgICAgbWF4LWhlaWdodDogMjVweCAhaW1wb3J0YW50O1xuICAgICAgICBtYXJnaW4tcmlnaHQ6IDVweDttYXJnaW4tdG9wOiAwO1xuICAgICAgICBpbWd7XG4gICAgICAgICAgICBib3JkZXI6IDFweCBzb2xpZCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xuICAgICAgICAgICAgd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcbiAgICAgICAgICAgIGhlaWdodDogMTAwJSAhaW1wb3J0YW50O1xuICAgICAgICAgICAgbWF4LXdpZHRoOiAyNXB4ICFpbXBvcnRhbnQ7XG4gICAgICAgICAgICBtYXgtaGVpZ2h0OiAyNXB4ICFpbXBvcnRhbnQ7XG4gICAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDVweDttYXJnaW4tdG9wOiAwO1xuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAgICAgICB9XG4gICAgfVxuICB9XG4gIFxuLmZvcl9uYW1le1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbiAgICBtYXJnaW4tdG9wOiAwO1xuICAgIGNvbG9yOiBibGFjaztcbn1cbi5mb3JfaXRlbW1pbWd7XG4gICAgbWFyZ2luOiBhdXRvO1xuICAgIHdpZHRoOiA5MCU7XG4gICAgbWFyZ2luLXRvcDogLTEwcHg7XG4gICAgcGFkZGluZy1ib3R0b206IDVweDtcbn1cbi5mb3JfcHByaWNle1xuICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICBmb250LXdlaWdodDogYm9sZGVyO1xufVxuLmZvcl9jYXRlZ29yeXtcbiAgICBiYWNrZ3JvdW5kOiAjNzZjOTYxO1xuICAgIHBhZGRpbmc6IDNweDtcbiAgICBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogMTBweDtcbiAgICBtYXJnaW4tdG9wOiAtNXB4O1xuICAgIG1hcmdpbi1yaWdodDogLTVweDtcbiAgICBib3JkZXItYm90dG9tLWxlZnQtcmFkaXVzOiAxMHB4O1xuICAgIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICBmb250LXdlaWdodDogbGlnaHRlcjtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLmZvcl9pdGVtbW5hbWV7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkZXI7XG59XG4uZm9yX2l0ZW1tbG9jYXRpb257XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIGZvbnQtd2VpZ2h0OiBsaWdodGVyO1xufVxuLmZvcl9ib3JkZXJpdGVte1xuICAgIGJvcmRlci10b3A6IDFweCBzb2xpZCAjZTJmMGNiO1xuICAgIHBhZGRpbmctdG9wOiAyLjUlO1xufVxuLmZvcl9pdGVtbWJ0bntcbiAgICBmb250LXNpemU6IDExcHg7XG4gICAgY29sb3I6ICM2Nzk3MzM7XG59XG4uZm9yX3VuaXRxdHl7XG4gICAgZm9udC1zaXplOiAxMXB4O1xuICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgY29sb3I6ICM5ODlhYTI7XG59IiwiLmZvcl9zZWxuYW1lIHtcbiAgZm9udC1zaXplOiAxN3B4O1xuICBjb2xvcjogIzY3OTczMztcbiAgbWFyZ2luLXRvcDogN3B4O1xufVxuXG4uZm9yX2xvY2F0aW9uIHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBtYXJnaW4tdG9wOiAtMnB4O1xufVxuXG4uZm9yX3N0YXJpY29uIHtcbiAgZm9udC1zaXplOiAyNHB4O1xuICBtYXJnaW4tYm90dG9tOiAtMiU7XG59XG5cbmlvbi1zZWxlY3Qge1xuICBmb250LXNpemU6IDEzcHg7XG59XG5cbi5mb3Jfc29ydGZlZWQge1xuICBwYWRkaW5nLWxlZnQ6IDA7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZTJmMGNiO1xuICBwYWRkaW5nLWJvdHRvbTogMDtcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcbn1cblxuLmZvcl9pdGVtcyB7XG4gIG1hcmdpbi10b3A6IDAlO1xuICBib3gtc2hhZG93OiAwIDFweCAxMHB4ICNlMmYwY2I7XG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNlMmYwY2I7XG4gIG1hcmdpbi1ib3R0b206IDA7XG59XG5cbi5mb3JfY2hpcCB7XG4gIC0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG4gIG1hcmdpbjogMDtcbiAgcGFkZGluZy1yaWdodDogMDtcbiAgcGFkZGluZy1sZWZ0OiAxNXB4O1xufVxuXG4uZm9yX2l0ZW1pbWcge1xuICBwYWRkaW5nLWxlZnQ6IDVweDtcbiAgbWFyZ2luOiBhdXRvO1xuICB3aWR0aDogOTAlO1xuICBoZWlnaHQ6IDcwJTtcbn1cblxuLmZvcl9wcmljZSB7XG4gIGZvbnQtc2l6ZTogMjBweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbiAgcGFkZGluZy10b3A6IDVweDtcbn1cblxuLmZvcl9pdGVtbmFtZSB7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbn1cblxuLmZvcl9pdGVtY2F0ZWcge1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBmb250LXNpemU6IDEycHg7XG4gIGZvbnQtd2VpZ2h0OiBsaWdodGVyO1xufVxuXG4uZm9yX2l0ZW1sb2NhdGlvbiB7XG4gIGZvbnQtc2l6ZTogMTNweDtcbiAgZm9udC13ZWlnaHQ6IGxpZ2h0ZXI7XG59XG5cbi5mb3JfcGluIHtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBtYXJnaW4tYm90dG9tOiAtMSU7XG4gIG1hcmdpbi1sZWZ0OiAtMSU7XG59XG5cbi5mb3JfaXRlbWJ0biB7XG4gIGZvbnQtc2l6ZTogMTFweDtcbn1cblxuLmZvcl9pdGVtYnRuaWNvbiB7XG4gIGZvbnQtc2l6ZTogMjBweDtcbiAgbWFyZ2luLWJvdHRvbTogLTclO1xuICBtYXJnaW4tcmlnaHQ6IDIlO1xufVxuXG4udXBsb2FkX3Bob3RvIHtcbiAgYmFja2dyb3VuZDogIzc2Yzk2MTtcbiAgY29sb3I6IHdoaXRlO1xuICBwYWRkaW5nOiAzcHg7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgZm9udC1zaXplOiAxOHB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogNDAlO1xuICBsZWZ0OiA1MyU7XG59XG5cbi5mb3JfaXRlbW1zIHtcbiAgLS1wYWRkaW5nLXN0YXJ0OiAwO1xuICBtYXJnaW4tdG9wOiAtOHB4O1xuICAtLWlubmVyLXBhZGRpbmctZW5kOiAwO1xuICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xufVxuLmZvcl9pdGVtbXMgaW9uLWF2YXRhciB7XG4gIHdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XG4gIGhlaWdodDogMTAwJSAhaW1wb3J0YW50O1xuICBtYXgtd2lkdGg6IDI1cHggIWltcG9ydGFudDtcbiAgbWF4LWhlaWdodDogMjVweCAhaW1wb3J0YW50O1xuICBtYXJnaW4tcmlnaHQ6IDVweDtcbiAgbWFyZ2luLXRvcDogMDtcbn1cbi5mb3JfaXRlbW1zIGlvbi1hdmF0YXIgaW1nIHtcbiAgYm9yZGVyOiAxcHggc29saWQgcmdiYSgwLCAwLCAwLCAwLjEyKTtcbiAgd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcbiAgaGVpZ2h0OiAxMDAlICFpbXBvcnRhbnQ7XG4gIG1heC13aWR0aDogMjVweCAhaW1wb3J0YW50O1xuICBtYXgtaGVpZ2h0OiAyNXB4ICFpbXBvcnRhbnQ7XG4gIG1hcmdpbi1yaWdodDogNXB4O1xuICBtYXJnaW4tdG9wOiAwO1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG59XG5cbi5mb3JfbmFtZSB7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG4gIG1hcmdpbi10b3A6IDA7XG4gIGNvbG9yOiBibGFjaztcbn1cblxuLmZvcl9pdGVtbWltZyB7XG4gIG1hcmdpbjogYXV0bztcbiAgd2lkdGg6IDkwJTtcbiAgbWFyZ2luLXRvcDogLTEwcHg7XG4gIHBhZGRpbmctYm90dG9tOiA1cHg7XG59XG5cbi5mb3JfcHByaWNlIHtcbiAgZm9udC1zaXplOiAyMHB4O1xuICBmb250LXdlaWdodDogYm9sZGVyO1xufVxuXG4uZm9yX2NhdGVnb3J5IHtcbiAgYmFja2dyb3VuZDogIzc2Yzk2MTtcbiAgcGFkZGluZzogM3B4O1xuICBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogMTBweDtcbiAgbWFyZ2luLXRvcDogLTVweDtcbiAgbWFyZ2luLXJpZ2h0OiAtNXB4O1xuICBib3JkZXItYm90dG9tLWxlZnQtcmFkaXVzOiAxMHB4O1xuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBmb250LXdlaWdodDogbGlnaHRlcjtcbiAgY29sb3I6IHdoaXRlO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5mb3JfaXRlbW1uYW1lIHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBmb250LXdlaWdodDogYm9sZGVyO1xufVxuXG4uZm9yX2l0ZW1tbG9jYXRpb24ge1xuICBmb250LXNpemU6IDEzcHg7XG4gIGZvbnQtd2VpZ2h0OiBsaWdodGVyO1xufVxuXG4uZm9yX2JvcmRlcml0ZW0ge1xuICBib3JkZXItdG9wOiAxcHggc29saWQgI2UyZjBjYjtcbiAgcGFkZGluZy10b3A6IDIuNSU7XG59XG5cbi5mb3JfaXRlbW1idG4ge1xuICBmb250LXNpemU6IDExcHg7XG4gIGNvbG9yOiAjNjc5NzMzO1xufVxuXG4uZm9yX3VuaXRxdHkge1xuICBmb250LXNpemU6IDExcHg7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIGNvbG9yOiAjOTg5YWEyO1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/viewprofile/viewprofile.page.ts":
  /*!*************************************************!*\
    !*** ./src/app/viewprofile/viewprofile.page.ts ***!
    \*************************************************/

  /*! exports provided: ViewprofilePage */

  /***/
  function srcAppViewprofileViewprofilePageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ViewprofilePage", function () {
      return ViewprofilePage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _providers_credential_provider__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../providers/credential-provider */
    "./src/providers/credential-provider.ts");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var _shared_model_item_model__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../shared/model/item.model */
    "./src/app/shared/model/item.model.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");

    var ViewprofilePage = /*#__PURE__*/function () {
      function ViewprofilePage(router, postPvdr, navCtrl, storage) {
        _classCallCheck(this, ViewprofilePage);

        this.router = router;
        this.postPvdr = postPvdr;
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.itemList = [];
        this.user_rating = "";
      }

      _createClass(ViewprofilePage, [{
        key: "goBack",
        value: function goBack() {
          // this.router.navigate(['help']);
          window.history.back();
        }
      }, {
        key: "goProductview",
        value: function goProductview(item_id) {
          // this.router.navigate(['productview']);
          var navigationExtras = {
            queryParams: {
              item_id: item_id
            }
          };
          this.navCtrl.navigateForward(['productview'], navigationExtras);
        }
      }, {
        key: "update_photo",
        value: function update_photo() {
          this.router.navigate(['uploadphoto']);
        }
      }, {
        key: "goHelp",
        value: function goHelp() {
          //window.location.href="http://greenthumbtrade.com/help";
          this.router.navigate(['tabs/help']);
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {
          this.plotData();
        }
      }, {
        key: "goSellerprof",
        value: function goSellerprof(user_profile_id) {
          // this.router.navigate((['viewitemsellerprofile']));
          var navigationExtras = {
            queryParams: {
              user_profile_id: user_profile_id
            }
          };
          this.navCtrl.navigateForward(['viewitemsellerprofile'], navigationExtras);
        }
      }, {
        key: "plotData",
        value: function plotData() {
          var _this = this;

          this.storage.get('greenthumb_user_id').then(function (user_id) {
            var body = {
              action: 'getUsername',
              user_id: user_id
            };
            console.log(JSON.stringify(body));

            _this.postPvdr.postData(body, 'user.php').subscribe(function (data) {
              return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                return regeneratorRuntime.wrap(function _callee$(_context) {
                  while (1) {
                    switch (_context.prev = _context.next) {
                      case 0:
                        this.username = data.username;

                      case 1:
                      case "end":
                        return _context.stop();
                    }
                  }
                }, _callee, this);
              }));
            });

            var body2 = {
              action: 'getSellerPost',
              user_id: user_id
            }; //console.log(JSON.stringify(body2));

            _this.postPvdr.postData(body2, 'post_item.php').subscribe(function (data) {
              return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
                var items, key;
                return regeneratorRuntime.wrap(function _callee2$(_context2) {
                  while (1) {
                    switch (_context2.prev = _context2.next) {
                      case 0:
                        if (data.success) {
                          items = []; //var pictureProfile: string = '';

                          for (key in data.result) {
                            // pictureProfile = (data.result[key].profile_photo == '') ? '' :
                            // this.postPvdr.myServer()+"/brixy-live/images/"+data.result[key].profile_photo;
                            items.push(new _shared_model_item_model__WEBPACK_IMPORTED_MODULE_5__["Item"](data.result[key].item_id, data.result[key].user_id, data.result[key].username, data.result[key].profile_photo == '' ? '' : this.postPvdr.myServer() + "/greenthumb/images/" + data.result[key].profile_photo, data.result[key].price, data.result[key].title, data.result[key].stocks_id, data.result[key].others_stock, parseInt(data.result[key].quantity), data.result[key].category, data.result[key].location, data.result[key].saveStatus, data.result[key].status, this.postPvdr.myServer() + "/greenthumb/images/posted_items/images/" + data.result[key].item_photo, data.result[key].user_rating, data.result[key].date, data.result[key].time));
                          }

                          this.itemList = items;
                        }

                      case 1:
                      case "end":
                        return _context2.stop();
                    }
                  }
                }, _callee2, this);
              }));
            });
          });
        }
      }]);

      return ViewprofilePage;
    }();

    ViewprofilePage.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }, {
        type: _providers_credential_provider__WEBPACK_IMPORTED_MODULE_3__["PostProvider"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["NavController"]
      }, {
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"]
      }];
    };

    ViewprofilePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-viewprofile',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./viewprofile.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/viewprofile/viewprofile.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./viewprofile.page.scss */
      "./src/app/viewprofile/viewprofile.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _providers_credential_provider__WEBPACK_IMPORTED_MODULE_3__["PostProvider"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["NavController"], _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"]])], ViewprofilePage);
    /***/
  }
}]);
//# sourceMappingURL=viewprofile-viewprofile-module-es5.js.map