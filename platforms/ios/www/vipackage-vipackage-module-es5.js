function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["vipackage-vipackage-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/vipackage/vipackage.page.html":
  /*!*************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/vipackage/vipackage.page.html ***!
    \*************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppVipackageVipackagePageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header mode=\"ios\">\n  <ion-toolbar mode=\"ios\">\n    <ion-buttons slot=\"secondary\" (click)=\"goBack()\">\n      <ion-button mode=\"ios\">\n          <ion-icon name=\"arrow-back\"  mode=\"ios\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n\n      <ion-title>VIP Package</ion-title>\n\n      <ion-buttons slot=\"primary\" mode=\"ios\">\n          <ion-button mode=\"ios\">\n              <img src=\"assets/icon/brixylogo.png\" style=\"width: 32px;\" mode=\"ios\">\n          </ion-button>\n        </ion-buttons>\n    </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n</ion-content>\n";
    /***/
  },

  /***/
  "./src/app/vipackage/vipackage-routing.module.ts":
  /*!*******************************************************!*\
    !*** ./src/app/vipackage/vipackage-routing.module.ts ***!
    \*******************************************************/

  /*! exports provided: VipackagePageRoutingModule */

  /***/
  function srcAppVipackageVipackageRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "VipackagePageRoutingModule", function () {
      return VipackagePageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _vipackage_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./vipackage.page */
    "./src/app/vipackage/vipackage.page.ts");

    var routes = [{
      path: '',
      component: _vipackage_page__WEBPACK_IMPORTED_MODULE_3__["VipackagePage"]
    }];

    var VipackagePageRoutingModule = function VipackagePageRoutingModule() {
      _classCallCheck(this, VipackagePageRoutingModule);
    };

    VipackagePageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], VipackagePageRoutingModule);
    /***/
  },

  /***/
  "./src/app/vipackage/vipackage.module.ts":
  /*!***********************************************!*\
    !*** ./src/app/vipackage/vipackage.module.ts ***!
    \***********************************************/

  /*! exports provided: VipackagePageModule */

  /***/
  function srcAppVipackageVipackageModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "VipackagePageModule", function () {
      return VipackagePageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _vipackage_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./vipackage-routing.module */
    "./src/app/vipackage/vipackage-routing.module.ts");
    /* harmony import */


    var _vipackage_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./vipackage.page */
    "./src/app/vipackage/vipackage.page.ts");

    var VipackagePageModule = function VipackagePageModule() {
      _classCallCheck(this, VipackagePageModule);
    };

    VipackagePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _vipackage_routing_module__WEBPACK_IMPORTED_MODULE_5__["VipackagePageRoutingModule"]],
      declarations: [_vipackage_page__WEBPACK_IMPORTED_MODULE_6__["VipackagePage"]]
    })], VipackagePageModule);
    /***/
  },

  /***/
  "./src/app/vipackage/vipackage.page.scss":
  /*!***********************************************!*\
    !*** ./src/app/vipackage/vipackage.page.scss ***!
    \***********************************************/

  /*! exports provided: default */

  /***/
  function srcAppVipackageVipackagePageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpcGFja2FnZS92aXBhY2thZ2UucGFnZS5zY3NzIn0= */";
    /***/
  },

  /***/
  "./src/app/vipackage/vipackage.page.ts":
  /*!*********************************************!*\
    !*** ./src/app/vipackage/vipackage.page.ts ***!
    \*********************************************/

  /*! exports provided: VipackagePage */

  /***/
  function srcAppVipackageVipackagePageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "VipackagePage", function () {
      return VipackagePage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");

    var VipackagePage = /*#__PURE__*/function () {
      function VipackagePage(router) {
        _classCallCheck(this, VipackagePage);

        this.router = router;
      }

      _createClass(VipackagePage, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "goBack",
        value: function goBack() {
          // this.router.navigate(['myaccount']);
          window.history.back();
        }
      }]);

      return VipackagePage;
    }();

    VipackagePage.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }];
    };

    VipackagePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-vipackage',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./vipackage.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/vipackage/vipackage.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./vipackage.page.scss */
      "./src/app/vipackage/vipackage.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])], VipackagePage);
    /***/
  }
}]);
//# sourceMappingURL=vipackage-vipackage-module-es5.js.map