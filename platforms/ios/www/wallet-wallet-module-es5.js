function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["wallet-wallet-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/wallet/wallet.page.html":
  /*!*******************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/wallet/wallet.page.html ***!
    \*******************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppWalletWalletPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header mode=\"ios\" no-border>\n  <ion-toolbar mode=\"ios\">\n    <ion-buttons slot=\"secondary\" (click)=\"goBack()\">\n        <ion-button mode=\"ios\" style=\"font-size: 15px;\">\n            Back\n        </ion-button>\n    </ion-buttons>\n\n      <ion-title color=\"secondary\">Points</ion-title>\n    </ion-toolbar>\n</ion-header>\n<ion-content mode=\"ios\" padding>\n    <ion-card mode=\"ios\" class=\"for_card\">\n        <ion-row>\n            <ion-col padding>\n                <br>\n                <ion-label color=\"secondary\" class=\"for_bal\">Current Points Earned</ion-label>\n                <ion-label><h1 class=\"for_numbal\">$600</h1></ion-label>\n            </ion-col>\n            <ion-col text-center>\n                <img src=\"assets/greenthumb-images/bank.png\" style=\"width: 90%;\">\n            </ion-col>\n        </ion-row>\n    </ion-card>\n    <!-- <ion-list class=\"line-input\" padding>\n        <ion-row>\n          <ion-col>\n            <ion-label class=\"item_label\">RELOAD</ion-label>\n            <ion-item>\n              <ion-input placeholder=\"Enter Amount\" mode=\"ios\" type=\"text\" class=\"item_input\" size=\"small\">\n              </ion-input>\n            </ion-item>\n          </ion-col>\n        </ion-row>\n      </ion-list> -->\n      <ion-list mode=\"ios\" style=\"margin-top: 20px;\">\n        <ion-list-header mode=\"ios\" style=\"padding-left: 0;\">\n            TRANSACTION HISTORY\n          </ion-list-header>\n        <ion-item mode=\"ios\">\n            <ion-avatar slot=\"start\">\n                <img src=\"assets/greenthumb-images/money-bag.png\"> \n            </ion-avatar>\n            <ion-label mode=\"ios\">\n                <h3><b>Lorem ipsum</b></h3>\n                <p class=\"for_datetime\" mode=\"ios\">12/12/2020 4:05 PM</p>\n            </ion-label>\n            <ion-label mode=\"ios\" slot=\"end\" text-right color=\"secondary\" class=\"for_labelend\">\n                <h1><b>$800</b></h1>\n            </ion-label>\n        </ion-item> \n        <ion-item mode=\"ios\">\n            <ion-avatar slot=\"start\">\n                <img src=\"assets/greenthumb-images/money-bag.png\"> \n            </ion-avatar>\n            <ion-label mode=\"ios\">\n                <h3><b>Lorem ipsum</b></h3>\n                <p class=\"for_datetime\" mode=\"ios\">12/12/2020 4:05 PM</p>\n            </ion-label>\n            <ion-label mode=\"ios\" slot=\"end\" text-right color=\"secondary\" class=\"for_labelend\">\n                <h1><b>$800</b></h1>\n            </ion-label>\n        </ion-item> \n        <ion-item mode=\"ios\">\n            <ion-avatar slot=\"start\">\n                <img src=\"assets/greenthumb-images/money-bag.png\"> \n            </ion-avatar>\n            <ion-label mode=\"ios\">\n                <h3><b>Lorem ipsum</b></h3>\n                <p class=\"for_datetime\" mode=\"ios\">12/12/2020 4:05 PM</p>\n            </ion-label>\n            <ion-label mode=\"ios\" slot=\"end\" text-right color=\"secondary\" class=\"for_labelend\">\n                <h1><b>$800</b></h1>\n            </ion-label>\n        </ion-item> \n     </ion-list>\n</ion-content>\n";
    /***/
  },

  /***/
  "./src/app/wallet/wallet.module.ts":
  /*!*****************************************!*\
    !*** ./src/app/wallet/wallet.module.ts ***!
    \*****************************************/

  /*! exports provided: WalletPageModule */

  /***/
  function srcAppWalletWalletModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "WalletPageModule", function () {
      return WalletPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _wallet_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./wallet.page */
    "./src/app/wallet/wallet.page.ts");

    var routes = [{
      path: '',
      component: _wallet_page__WEBPACK_IMPORTED_MODULE_6__["WalletPage"]
    }];

    var WalletPageModule = function WalletPageModule() {
      _classCallCheck(this, WalletPageModule);
    };

    WalletPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      declarations: [_wallet_page__WEBPACK_IMPORTED_MODULE_6__["WalletPage"]]
    })], WalletPageModule);
    /***/
  },

  /***/
  "./src/app/wallet/wallet.page.scss":
  /*!*****************************************!*\
    !*** ./src/app/wallet/wallet.page.scss ***!
    \*****************************************/

  /*! exports provided: default */

  /***/
  function srcAppWalletWalletPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".for_bank {\n  --background: none;\n  background-image: url(\"/assets/greenthumb-images/bank.png\");\n  background-position: center center;\n  background-repeat: no-repeat;\n  background-size: cover;\n}\n\n.line-input {\n  margin-bottom: 0 !important;\n  padding-left: 0;\n  padding-right: 0;\n}\n\n.line-input ion-item {\n  --border-color: transparent!important;\n  --highlight-height: 0;\n  border: 1px solid #dedede;\n  border-radius: 4px;\n  height: 40px;\n  margin-top: 4%;\n}\n\n.item_input {\n  font-size: 14px;\n  --padding-top: 0;\n  color: #424242 !important;\n  --padding-start: 10px !important;\n}\n\n.item_label {\n  color: #679733 !important;\n  font-size: 14px;\n  font-weight: bold;\n}\n\n.for_card {\n  margin: 0;\n  background: #e2f0cb;\n  height: 140px;\n}\n\n.for_bal {\n  font-size: 14px;\n  font-weight: bold;\n}\n\n.for_numbal {\n  font-weight: bold;\n  font-size: 40px;\n  color: black;\n}\n\n.for_datetime {\n  padding: 0;\n  font-size: 12px;\n  color: #00000066;\n}\n\n.for_labelend {\n  margin-left: -100px;\n  align-self: flex-end;\n}\n\nion-avatar {\n  width: 100% !important;\n  height: 100% !important;\n  max-width: 40px !important;\n  max-height: 40px !important;\n}\n\nion-avatar img {\n  width: 100% !important;\n  height: 100% !important;\n  max-width: 40px !important;\n  max-height: 40px !important;\n}\n\nion-item {\n  --padding-start: 0% !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZW5kcmljay9EZXNrdG9wL1RlbG1vIFNvbHV0aW9ucy9Gb3JQcm9kdWN0aW9uL21heTEwMjAyMS9sYXN0IHNldHVwL2dyZWVudGh1bWJhcHAgY29weS9zcmMvYXBwL3dhbGxldC93YWxsZXQucGFnZS5zY3NzIiwic3JjL2FwcC93YWxsZXQvd2FsbGV0LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGtCQUFBO0VBQ0UsMkRBQUE7RUFDQSxrQ0FBQTtFQUNBLDRCQUFBO0VBQ0Esc0JBQUE7QUNDSjs7QURDQTtFQUNFLDJCQUFBO0VBQ0EsZUFBQTtFQUNFLGdCQUFBO0FDRUo7O0FEQUU7RUFDSSxxQ0FBQTtFQUNBLHFCQUFBO0VBQ0EseUJBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxjQUFBO0FDRU47O0FEQ0E7RUFDRSxlQUFBO0VBQ0UsZ0JBQUE7RUFDQSx5QkFBQTtFQUNBLGdDQUFBO0FDRUo7O0FEQUE7RUFDRSx5QkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtBQ0dGOztBRERBO0VBQ0UsU0FBQTtFQUNBLG1CQUFBO0VBQ0EsYUFBQTtBQ0lGOztBREZBO0VBQ0UsZUFBQTtFQUNBLGlCQUFBO0FDS0Y7O0FESEE7RUFDRSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0FDTUY7O0FESkE7RUFDRSxVQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0FDT0Y7O0FETEE7RUFDRSxtQkFBQTtFQUNBLG9CQUFBO0FDUUY7O0FETkE7RUFDRSxzQkFBQTtFQUNBLHVCQUFBO0VBQ0EsMEJBQUE7RUFDQSwyQkFBQTtBQ1NGOztBRFJNO0VBQ0ksc0JBQUE7RUFDQSx1QkFBQTtFQUNBLDBCQUFBO0VBQ0EsMkJBQUE7QUNVVjs7QURQRTtFQUNFLDhCQUFBO0FDVUoiLCJmaWxlIjoic3JjL2FwcC93YWxsZXQvd2FsbGV0LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5mb3JfYmFua3tcbiAgLS1iYWNrZ3JvdW5kOiBub25lO1xuICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybCgnL2Fzc2V0cy9ncmVlbnRodW1iLWltYWdlcy9iYW5rLnBuZycpO1xuICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlciBjZW50ZXI7XG4gICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xufVxuLmxpbmUtaW5wdXQge1xuICBtYXJnaW4tYm90dG9tOiAwIWltcG9ydGFudDtcbiAgcGFkZGluZy1sZWZ0OiAwO1xuICAgIHBhZGRpbmctcmlnaHQ6IDA7XG5cbiAgaW9uLWl0ZW0ge1xuICAgICAgLS1ib3JkZXItY29sb3I6IHRyYW5zcGFyZW50IWltcG9ydGFudDtcbiAgICAgIC0taGlnaGxpZ2h0LWhlaWdodDogMDtcbiAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNkZWRlZGU7XG4gICAgICBib3JkZXItcmFkaXVzOiA0cHg7XG4gICAgICBoZWlnaHQ6IDQwcHg7XG4gICAgICBtYXJnaW4tdG9wOiA0JTtcbiAgfVxufVxuLml0ZW1faW5wdXR7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgICAtLXBhZGRpbmctdG9wOiAwO1xuICAgIGNvbG9yOiAjNDI0MjQyICFpbXBvcnRhbnQ7XG4gICAgLS1wYWRkaW5nLXN0YXJ0OiAxMHB4ICFpbXBvcnRhbnQ7XG59XG4uaXRlbV9sYWJlbHtcbiAgY29sb3I6ICM2Nzk3MzMgIWltcG9ydGFudDtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5mb3JfY2FyZHtcbiAgbWFyZ2luOiAwO1xuICBiYWNrZ3JvdW5kOiAjZTJmMGNiO1xuICBoZWlnaHQ6IDE0MHB4O1xufVxuLmZvcl9iYWx7XG4gIGZvbnQtc2l6ZToxNHB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5mb3JfbnVtYmFse1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgZm9udC1zaXplOiA0MHB4O1xuICBjb2xvcjogYmxhY2s7XG59XG4uZm9yX2RhdGV0aW1le1xuICBwYWRkaW5nOiAwO1xuICBmb250LXNpemU6IDEycHg7XG4gIGNvbG9yOiAjMDAwMDAwNjZcbn1cbi5mb3JfbGFiZWxlbmR7XG4gIG1hcmdpbi1sZWZ0OiAtMTAwcHg7XG4gIGFsaWduLXNlbGY6IGZsZXgtZW5kO1xufVxuaW9uLWF2YXRhcntcbiAgd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcbiAgaGVpZ2h0OiAxMDAlICFpbXBvcnRhbnQ7XG4gIG1heC13aWR0aDogNDBweCAhaW1wb3J0YW50O1xuICBtYXgtaGVpZ2h0OiA0MHB4ICFpbXBvcnRhbnQ7XG4gICAgICBpbWd7XG4gICAgICAgICAgd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcbiAgICAgICAgICBoZWlnaHQ6IDEwMCUgIWltcG9ydGFudDtcbiAgICAgICAgICBtYXgtd2lkdGg6IDQwcHggIWltcG9ydGFudDtcbiAgICAgICAgICBtYXgtaGVpZ2h0OiA0MHB4ICFpbXBvcnRhbnQ7XG4gICAgICB9XG59XG4gIGlvbi1pdGVtIHtcbiAgICAtLXBhZGRpbmctc3RhcnQ6IDAlICFpbXBvcnRhbnQ7XG59IiwiLmZvcl9iYW5rIHtcbiAgLS1iYWNrZ3JvdW5kOiBub25lO1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCIvYXNzZXRzL2dyZWVudGh1bWItaW1hZ2VzL2JhbmsucG5nXCIpO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXIgY2VudGVyO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xufVxuXG4ubGluZS1pbnB1dCB7XG4gIG1hcmdpbi1ib3R0b206IDAgIWltcG9ydGFudDtcbiAgcGFkZGluZy1sZWZ0OiAwO1xuICBwYWRkaW5nLXJpZ2h0OiAwO1xufVxuLmxpbmUtaW5wdXQgaW9uLWl0ZW0ge1xuICAtLWJvcmRlci1jb2xvcjogdHJhbnNwYXJlbnQhaW1wb3J0YW50O1xuICAtLWhpZ2hsaWdodC1oZWlnaHQ6IDA7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNkZWRlZGU7XG4gIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgaGVpZ2h0OiA0MHB4O1xuICBtYXJnaW4tdG9wOiA0JTtcbn1cblxuLml0ZW1faW5wdXQge1xuICBmb250LXNpemU6IDE0cHg7XG4gIC0tcGFkZGluZy10b3A6IDA7XG4gIGNvbG9yOiAjNDI0MjQyICFpbXBvcnRhbnQ7XG4gIC0tcGFkZGluZy1zdGFydDogMTBweCAhaW1wb3J0YW50O1xufVxuXG4uaXRlbV9sYWJlbCB7XG4gIGNvbG9yOiAjNjc5NzMzICFpbXBvcnRhbnQ7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG5cbi5mb3JfY2FyZCB7XG4gIG1hcmdpbjogMDtcbiAgYmFja2dyb3VuZDogI2UyZjBjYjtcbiAgaGVpZ2h0OiAxNDBweDtcbn1cblxuLmZvcl9iYWwge1xuICBmb250LXNpemU6IDE0cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuXG4uZm9yX251bWJhbCB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBmb250LXNpemU6IDQwcHg7XG4gIGNvbG9yOiBibGFjaztcbn1cblxuLmZvcl9kYXRldGltZSB7XG4gIHBhZGRpbmc6IDA7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgY29sb3I6ICMwMDAwMDA2Njtcbn1cblxuLmZvcl9sYWJlbGVuZCB7XG4gIG1hcmdpbi1sZWZ0OiAtMTAwcHg7XG4gIGFsaWduLXNlbGY6IGZsZXgtZW5kO1xufVxuXG5pb24tYXZhdGFyIHtcbiAgd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcbiAgaGVpZ2h0OiAxMDAlICFpbXBvcnRhbnQ7XG4gIG1heC13aWR0aDogNDBweCAhaW1wb3J0YW50O1xuICBtYXgtaGVpZ2h0OiA0MHB4ICFpbXBvcnRhbnQ7XG59XG5pb24tYXZhdGFyIGltZyB7XG4gIHdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XG4gIGhlaWdodDogMTAwJSAhaW1wb3J0YW50O1xuICBtYXgtd2lkdGg6IDQwcHggIWltcG9ydGFudDtcbiAgbWF4LWhlaWdodDogNDBweCAhaW1wb3J0YW50O1xufVxuXG5pb24taXRlbSB7XG4gIC0tcGFkZGluZy1zdGFydDogMCUgIWltcG9ydGFudDtcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/wallet/wallet.page.ts":
  /*!***************************************!*\
    !*** ./src/app/wallet/wallet.page.ts ***!
    \***************************************/

  /*! exports provided: WalletPage */

  /***/
  function srcAppWalletWalletPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "WalletPage", function () {
      return WalletPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");

    var WalletPage = /*#__PURE__*/function () {
      function WalletPage(router) {
        _classCallCheck(this, WalletPage);

        this.router = router;
      }

      _createClass(WalletPage, [{
        key: "goBack",
        value: function goBack() {
          // this.router.navigate(['myaccount']);
          window.history.back();
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return WalletPage;
    }();

    WalletPage.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }];
    };

    WalletPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-wallet',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./wallet.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/wallet/wallet.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./wallet.page.scss */
      "./src/app/wallet/wallet.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])], WalletPage);
    /***/
  }
}]);
//# sourceMappingURL=wallet-wallet-module-es5.js.map