import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { Storage } from '@ionic/storage';
import { ToastController } from '@ionic/angular';


@Component({
  selector: 'app-accountsettings',
  templateUrl: './accountsettings.page.html',
  styleUrls: ['./accountsettings.page.scss'],
})
export class AccountsettingsPage implements OnInit {

  constructor(
    private router: Router,
    public afAuth: AngularFireAuth,
    public toastController: ToastController,
    private storage: Storage
  ) { }

  ngOnInit() {
  }

  goAbout(){
    //window.location.href="http://greenthumbtrade.com";
  }
  goBack(){
    window.history.back();
    //this.navCtrl.navigateForward((['describeitem']), { animated: false, });
  }
  upload_photo(){
    this.router.navigate(['uploadphoto']);
  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: 'You are now logged out.',
      duration: 3000
    });
    toast.present();
  }

async signOut(){
  this.afAuth.auth.signOut().then(async () => {
    
    await this.storage.clear();
    this.presentToast();
    this.router.navigate(['choose']);
  });
}

}
