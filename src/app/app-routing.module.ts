import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  { path: 'ads', loadChildren: './ads/ads.module#AdsPageModule' },
  { path: 'guide', loadChildren: './guide/guide.module#GuidePageModule' },
  { path: 'choose', loadChildren: './choose/choose.module#ChoosePageModule' },
  { path: 'mobilee', loadChildren: './mobilee/mobilee.module#MobileePageModule' },
  { path: 'email-login', loadChildren: './email-login/email-login.module#EmailLoginPageModule' },
  { path: 'email-code/:email_code', loadChildren: './email-code/email-code.module#EmailCodePageModule' },
  { path: 'tabs', loadChildren: './tabs/tabs.module#TabsPageModule' },
  { path: 'brixyloads', loadChildren: './brixyloads/brixyloads.module#BrixyloadsPageModule' },
  { path: 'tab3', loadChildren: './tab3/tab3.module#Tab3PageModule' },
  { path: 'termsconditions', loadChildren: './termsconditions/termsconditions.module#TermsconditionsPageModule' },
  { path: 'tab4', loadChildren: './tab4/tab4.module#Tab4PageModule' },
  { path: 'tab5', loadChildren: './tab5/tab5.module#Tab5PageModule' },
  { path: 'tab6', loadChildren: './tab6/tab6.module#Tab6PageModule' },
  { path: 'myaccount', loadChildren: './myaccount/myaccount.module#MyaccountPageModule' },
  { path: 'settings', loadChildren: './settings/settings.module#SettingsPageModule' },
  { path: 'editprofile', loadChildren: './editprofile/editprofile.module#EditprofilePageModule' },
  { path: 'message', loadChildren: './message/message.module#MessagePageModule' },
  { 
    path: 'message/:id', 
    loadChildren: './message/message.module#MessagePageModule' 
  },
  { path: 'viewlive', loadChildren: './viewlive/viewlive.module#ViewlivePageModule' },
  { 
    path: 'viewlive/:id', 
    loadChildren: './viewlive/viewlive.module#ViewlivePageModule' 
  },
  { path: 'allgifts', loadChildren: './allgifts/allgifts.module#AllgiftsPageModule' },
  { path: 'brixyvideo', loadChildren: './brixyvideo/brixyvideo.module#BrixyvideoPageModule' },
  { path: 'mybroadcast', loadChildren: './mybroadcast/mybroadcast.module#MybroadcastPageModule' },
  { path: 'sendgiftbronzemodal', loadChildren: './sendgiftbronzemodal/sendgiftbronzemodal.module#SendgiftbronzemodalPageModule' },
  { path: 'sendgiftsilvermodal', loadChildren: './sendgiftsilvermodal/sendgiftsilvermodal.module#SendgiftsilvermodalPageModule' },
  { path: 'sendgiftgoldmodal', loadChildren: './sendgiftgoldmodal/sendgiftgoldmodal.module#SendgiftgoldmodalPageModule' },
  { path: 'sendgiftpopularmodal', loadChildren: './sendgiftpopularmodal/sendgiftpopularmodal.module#SendgiftpopularmodalPageModule' },
  { path: 'terms', loadChildren: './terms/terms.module#TermsPageModule' },
  { path: 'privacy', loadChildren: './privacy/privacy.module#PrivacyPageModule' },
  { path: 'readytogolive', loadChildren: './readytogolive/readytogolive.module#ReadytogolivePageModule' },
  { path: 'wallet', loadChildren: './wallet/wallet.module#WalletPageModule' },
  { path: 'paymentrequest', loadChildren: './paymentrequest/paymentrequest.module#PaymentrequestPageModule' },
  { path: 'historypreview', loadChildren: './historypreview/historypreview.module#HistorypreviewPageModule' },
  { path: 'historygiftpreview', loadChildren: './historygiftpreview/historygiftpreview.module#HistorygiftpreviewPageModule' },
  { path: 'termsconditionslogin', loadChildren: './termsconditionslogin/termsconditionslogin.module#TermsconditionsloginPageModule' },
  { path: 'liveprofile', loadChildren: './liveprofile/liveprofile.module#LiveprofilePageModule' },
  { path: 'register', loadChildren: './register/register.module#RegisterPageModule' },
  { path: 'uploadphoto', loadChildren: './uploadphoto/uploadphoto.module#UploadphotoPageModule' },
  { path: 'livestream', loadChildren: './livestream/livestream.module#LivestreamPageModule' },
  {
    path: 'email-login',
    loadChildren: () => import('./email-login/email-login.module').then( m => m.EmailLoginPageModule)
  },
  {
    path: 'email-code',
    loadChildren: () => import('./email-code/email-code.module').then( m => m.EmailCodePageModule)
  },
  {
    path: 'purchasecoins',
    loadChildren: () => import('./purchasecoins/purchasecoins.module').then( m => m.PurchasecoinsPageModule)
  },
  {
    path: 'search',
    loadChildren: () => import('./search/search.module').then( m => m.SearchPageModule)
  },
  {
    path: 'forgotpassword',
    loadChildren: () => import('./forgotpassword/forgotpassword.module').then( m => m.ForgotpasswordPageModule)
  },
  {
    path: 'paymentrequest',
    loadChildren: () => import('./paymentrequest/paymentrequest.module').then( m => m.PaymentrequestPageModule)
  },
  {
    path: 'reportuser',
    loadChildren: () => import('./reportuser/reportuser.module').then( m => m.ReportuserPageModule)
  },
  {
    path: 'vippackage',
    loadChildren: () => import('./vippackage/vippackage.module').then( m => m.VippackagePageModule)
  },
  {
    path: 'vipackage',
    loadChildren: () => import('./vipackage/vipackage.module').then( m => m.VipackagePageModule)
  },
  {
    path: 'payoneerid',
    loadChildren: () => import('./payoneerid/payoneerid.module').then( m => m.PayoneeridPageModule)
  },
  {
    path: 'paymentsettings',
    loadChildren: () => import('./paymentsettings/paymentsettings.module').then( m => m.PaymentsettingsPageModule)
  },
  {
    path: 'systemnotif',
    loadChildren: () => import('./systemnotif/systemnotif.module').then( m => m.SystemnotifPageModule)
  },
  {
    path: 'help',
    loadChildren: () => import('./help/help.module').then( m => m.HelpPageModule)
  },
  {
    path: 'describeitem',
    loadChildren: () => import('./describeitem/describeitem.module').then( m => m.DescribeitemPageModule)
  },
  {
    path: 'categories',
    loadChildren: () => import('./categories/categories.module').then( m => m.CategoriesPageModule)
  },
  {
    path: 'setprice',
    loadChildren: () => import('./setprice/setprice.module').then( m => m.SetpricePageModule)
  },
  {
    path: 'selectlocation',
    loadChildren: () => import('./selectlocation/selectlocation.module').then( m => m.SelectlocationPageModule)
  },
  {
    path: 'shareitem',
    loadChildren: () => import('./shareitem/shareitem.module').then( m => m.ShareitemPageModule)
  },
  {
    path: 'itemposted',
    loadChildren: () => import('./itemposted/itemposted.module').then( m => m.ItempostedPageModule)
  },
  {
    path: 'invite',
    loadChildren: () => import('./invite/invite.module').then( m => m.InvitePageModule)
  },
  {
    path: 'viewprofile',
    loadChildren: () => import('./viewprofile/viewprofile.module').then( m => m.ViewprofilePageModule)
  },
  {
    path: 'selectcategories',
    loadChildren: () => import('./selectcategories/selectcategories.module').then( m => m.SelectcategoriesPageModule)
  },
  {
    path: 'productview',
    loadChildren: () => import('./productview/productview.module').then( m => m.ProductviewPageModule)
  },
  {
    path: 'report',
    loadChildren: () => import('./report/report.module').then( m => m.ReportPageModule)
  },
  {
    path: 'viewitemsellerprofile',
    loadChildren: () => import('./viewitemsellerprofile/viewitemsellerprofile.module').then( m => m.ViewitemsellerprofilePageModule)
  },
  {
    path: 'accountsettings',
    loadChildren: () => import('./accountsettings/accountsettings.module').then( m => m.AccountsettingsPageModule)
  },
  {
    path: 'offers',
    loadChildren: () => import('./offers/offers.module').then( m => m.OffersPageModule)
  },
  {
    path: 'sellfaster',
    loadChildren: () => import('./sellfaster/sellfaster.module').then( m => m.SellfasterPageModule)
  },
  {
    path: 'sellingproductview',
    loadChildren: () => import('./sellingproductview/sellingproductview.module').then( m => m.SellingproductviewPageModule)
  },
  {
    path: 'edititem',
    loadChildren: () => import('./edititem/edititem.module').then( m => m.EdititemPageModule)
  },
  {
    path: 'selectcategoriessub',
    loadChildren: () => import('./selectcategoriessub/selectcategoriessub.module').then( m => m.SelectcategoriessubPageModule)
  },
  {
    path: 'categoriessub',
    loadChildren: () => import('./categoriessub/categoriessub.module').then( m => m.CategoriessubPageModule)
  },
  {
    path: 'location',
    loadChildren: () => import('./location/location.module').then( m => m.LocationPageModule)
  },
  {
    path: 'editlocation',
    loadChildren: () => import('./editlocation/editlocation.module').then( m => m.EditlocationPageModule)
  },
  {
    path: 'archieve',
    loadChildren: () => import('./archieve/archieve.module').then( m => m.ArchievePageModule)
  },
  {
    path: 'sellersifollow',
    loadChildren: () => import('./sellersifollow/sellersifollow.module').then( m => m.SellersifollowPageModule)
  },
  {
    path: 'promoteplus',
    loadChildren: () => import('./promoteplus/promoteplus.module').then( m => m.PromoteplusPageModule)
  },
  {
    path: 'userpolicy',
    loadChildren: () => import('./userpolicy/userpolicy.module').then( m => m.UserpolicyPageModule)
  },
  {
    path: 'privacypolicy',
    loadChildren: () => import('./privacypolicy/privacypolicy.module').then( m => m.PrivacypolicyPageModule)
  },
  {
    path: 'communityforum',
    loadChildren: () => import('./communityforum/communityforum.module').then( m => m.CommunityforumPageModule)
  },
  {
    path: 'suspendmodal',
    loadChildren: () => import('./suspendmodal/suspendmodal.module').then( m => m.SuspendmodalPageModule)
  },
  {
    path: 'applepay',
    loadChildren: () => import('./applepay/applepay.module').then( m => m.ApplepayPageModule)
  },
  {
    path: 'googlepay',
    loadChildren: () => import('./googlepay/googlepay.module').then( m => m.GooglepayPageModule)
  },
  {
    path: 'logoutscreen',
    loadChildren: () => import('./logoutscreen/logoutscreen.module').then( m => m.LogoutscreenPageModule)
  },
  {
    path: 'webprofile',
    loadChildren: () => import('./webprofile/webprofile.module').then( m => m.WebprofilePageModule)
  },
  {
    path: 'webproduct',
    loadChildren: () => import('./webproduct/webproduct.module').then( m => m.WebproductPageModule)
  },
  {
    path: 'rate',
    loadChildren: () => import('./rate/rate.module').then( m => m.RatePageModule)
  },
  {
    path: 'purchaseproduct',
    loadChildren: () => import('./purchaseproduct/purchaseproduct.module').then( m => m.PurchaseproductPageModule)
  },
  {
    path: 'subscribe',
    loadChildren: () => import('./subscribe/subscribe.module').then( m => m.SubscribePageModule)
  },





];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
