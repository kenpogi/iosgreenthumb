import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { ToastController } from '@ionic/angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { VideoPlayer } from '@ionic-native/video-player/ngx';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player/ngx';
import { GiftbronzeComponent } from './giftbronze/giftbronze.component';
import { GiftgoldComponent } from './giftgold/giftgold.component';
import { GiftsilverComponent } from './giftsilver/giftsilver.component';
import { GiftpopularComponent } from './giftpopular/giftpopular.component';
import { SendgiftbronzemodalPageModule } from './sendgiftbronzemodal/sendgiftbronzemodal.module';
import { SendgiftsilvermodalPageModule } from './sendgiftsilvermodal/sendgiftsilvermodal.module';
import { SendgiftgoldmodalPageModule } from './sendgiftgoldmodal/sendgiftgoldmodal.module';
import { SendgiftpopularmodalPageModule } from './sendgiftpopularmodal/sendgiftpopularmodal.module';
import { HistorypreviewPageModule } from './historypreview/historypreview.module';
import { HistorygiftpreviewPageModule } from './historygiftpreview/historygiftpreview.module';
import { AllgiftsPageModule } from './allgifts/allgifts.module';
import { LiveprofilePageModule } from './liveprofile/liveprofile.module';
import { RatePageModule } from './rate/rate.module';
import { ReportuserPageModule } from './reportuser/reportuser.module';
import { VippackagePageModule } from './vippackage/vippackage.module';
import { RegisterPageModule } from './register/register.module';
import { PurchasecoinsPageModule } from './purchasecoins/purchasecoins.module';
import { SelectcategoriesPageModule } from './selectcategories/selectcategories.module';
import { SelectcategoriessubPageModule } from './selectcategoriessub/selectcategoriessub.module';
import { CategoriessubPageModule } from './categoriessub/categoriessub.module';
import { ReportPageModule } from './report/report.module';
import { UserpolicyPageModule } from './userpolicy/userpolicy.module';
import { PrivacypolicyPageModule } from './privacypolicy/privacypolicy.module';
import { SuspendmodalPageModule } from './suspendmodal/suspendmodal.module';

import { PurchaseproductPageModule } from './purchaseproduct/purchaseproduct.module';

import { PayreqconfirmationPageModule } from './payreqconfirmation/payreqconfirmation.module';

import {FirebaseUIModule, firebase, firebaseui} from 'firebaseui-angular';

import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { Vibration } from '@ionic-native/vibration/ngx';

import {AngularFireModule} from '@angular/fire';
import {AngularFireAuthModule} from '@angular/fire/auth';
import { environment } from '../environments/environment';
import { PostProvider } from '../providers/credential-provider';
import { ChatMessages } from '../providers/chat-messages.provider';
import { SmsProvider } from '../providers/sms-provider';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { Camera } from '@ionic-native/camera/ngx';
import { FileTransfer } from '@ionic-native/file-transfer/ngx';
import { File } from '@ionic-native/file/ngx';
import { Crop } from '@ionic-native/crop/ngx';
import {SuperTabsModule} from '@ionic-super-tabs/angular';

import { AppMinimize } from '@ionic-native/app-minimize/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NativeGeocoder } from '@ionic-native/native-geocoder/ngx';


import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
const config: SocketIoConfig = { url: 'http://178.128.65.70:3001', options: {} };

//const config: SocketIoConfig = { url: 'http://192.168.1.9:3001', options: {} };

const firebaseUiAuthConfig: firebaseui.auth.Config = {
  signInSuccessUrl: '/tabs',
  siteName: 'Greenthumb Trade',
  signInFlow: 'popup',
  signInOptions: [
    firebaseui.auth.AnonymousAuthProvider.PROVIDER_ID
  ],
  tosUrl: '/termsconditions',
  privacyPolicyUrl: '/termsconditions',
  credentialHelper: firebaseui.auth.CredentialHelper.NONE
};


@NgModule({
  declarations: [AppComponent,GiftbronzeComponent,GiftgoldComponent,
    GiftsilverComponent,GiftpopularComponent],
  entryComponents: [GiftbronzeComponent,GiftgoldComponent,
    GiftsilverComponent,GiftpopularComponent],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpModule,
    HttpClientModule,
    IonicStorageModule.forRoot(),
    SendgiftbronzemodalPageModule,
    SendgiftsilvermodalPageModule,
    SendgiftgoldmodalPageModule,
    SendgiftpopularmodalPageModule,
    PurchaseproductPageModule,
    PurchasecoinsPageModule,
    PayreqconfirmationPageModule,
    HistorypreviewPageModule,
    HistorygiftpreviewPageModule,
    AllgiftsPageModule,
    RegisterPageModule,
    VippackagePageModule,
    LiveprofilePageModule,
    RatePageModule,
    ReportuserPageModule,
    SelectcategoriesPageModule,
    SelectcategoriessubPageModule,
    CategoriessubPageModule,
    ReportPageModule,
    UserpolicyPageModule,
    PrivacypolicyPageModule,
    SuspendmodalPageModule,
    SuperTabsModule.forRoot(),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    FirebaseUIModule.forRoot(firebaseUiAuthConfig),
    SocketIoModule.forRoot(config)
  ],
  providers: [
    AndroidPermissions,
    Camera,
    Crop,
    StatusBar,
    SplashScreen,
    FileTransfer,
    File,
    PostProvider,
    ChatMessages,
    SmsProvider,
    ToastController,
    VideoPlayer,
    AppMinimize,
    LocalNotifications,
    Geolocation,
    NativeGeocoder,
    Vibration,
    YoutubeVideoPlayer,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
