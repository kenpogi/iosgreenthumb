import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-applepay',
  templateUrl: './applepay.page.html',
  styleUrls: ['./applepay.page.scss'],
})
export class ApplepayPage implements OnInit {

  constructor(
    private router: Router
  ) { }

  goBack(){
    // this.router.navigate(['help']);
    window.history.back();
  }

  ngOnInit() {
  }

}
