import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ArchievePageRoutingModule } from './archieve-routing.module';
import { StarRatingModule } from 'ionic4-star-rating';
import { ArchievePage } from './archieve.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    StarRatingModule,
    ArchievePageRoutingModule
  ],
  declarations: [ArchievePage]
})
export class ArchievePageModule {}
