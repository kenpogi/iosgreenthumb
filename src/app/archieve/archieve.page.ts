import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import {  NavController } from '@ionic/angular';
import { PostProvider } from '../../providers/credential-provider';
import { Item } from '../shared/model/item.model';

@Component({
  selector: 'app-archieve',
  templateUrl: './archieve.page.html',
  styleUrls: ['./archieve.page.scss'],
})
export class ArchievePage implements OnInit {

  
  itemList: Item[] = [];

  login_user_id : string = "";

  constructor(
    private router: Router,
    private postPvdr: PostProvider,
    private storage: Storage,
    public navCtrl: NavController,
  ) { }

  ngOnInit() {
    this.plotData();
  }

  plotData(){
    this.storage.get('greenthumb_user_id').then((user_id) => {
      this.login_user_id = user_id;
      let body = {
        action : 'getArchivedItems',
        user_id : user_id
      };
      console.log(JSON.stringify(body));
      this.postPvdr.postData(body, 'post_item.php').subscribe(async data => {
        
        if(data.success){

          const items: Item[] = [];

          //var pictureProfile: string = '';

          for(const key in data.result){
            // pictureProfile = (data.result[key].profile_photo == '') ? '' :
            // this.postPvdr.myServer()+"/brixy-live/images/"+data.result[key].profile_photo;
            items.push(new Item(
              data.result[key].item_id,
              data.result[key].user_id,
              data.result[key].username,
              (data.result[key].profile_photo == '') ? '' :
              this.postPvdr.myServer()+"/greenthumb/images/"+data.result[key].profile_photo,
              data.result[key].price,
              data.result[key].title,
              data.result[key].stocks_id,
              data.result[key].others_stock,
              parseInt(data.result[key].quantity),
              data.result[key].category,
              data.result[key].location,
              data.result[key].saveStatus,
              data.result[key].status,
              this.postPvdr.myServer()+"/greenthumb/images/posted_items/images/"+data.result[key].item_photo,
              data.result[key].user_rating,
              data.result[key].date,
              data.result[key].time
              )
              
            );
          }

          this.itemList = items;
        }
      });
    });
  }

  goProductview(item_id){
    // this.router.navigate(['productview']);
     let navigationExtras: any = {
         queryParams: { item_id}
     };
     this.navCtrl.navigateForward(['productview'], navigationExtras);
   }

   followThis(user_id){
    let body = {
      action : 'follow',
      user_id : user_id,
      followed_by : this.login_user_id,
      //status : this.status
    };
    console.log("follow:"+JSON.stringify(body));
    this.postPvdr.postData(body, 'followers.php').subscribe(data => {
      console.log(data);
      if(data.success){
        // uncomment this when you add system notifications
          // this.followClass = "myHeartFollow";
          // this.status = !this.status; 
          // if(this.status){

          //   let body2 = {
          //     action : 'addNotification',
          //     user_id : this.user_id,
          //     followed_by: user_id,
          //     notification_message : "has followed you",
          //     status : this.status
          //   };
    
          //   this.postPvdr.postData(body2, 'system_notification.php').subscribe(data => {

          //   })
          // }
      }
    });
  }

  goBack(){
    window.history.back();
  }

  saveThis(item_id){
    console.log("gi saved lamang pud");
    let body = {
      action : 'saveItem',
      item_id : item_id,
      user_id : this.login_user_id,
    };

    this.postPvdr.postData(body, 'save_item.php').subscribe(data => {

      if(data.success){
        if(data.saveStatus == '1'){
          console.log('1 ko');
          var element = document.getElementById("class"+item_id);
          element.classList.remove("myHeartUnSave");
          element.classList.add("myHeartSave");
          console.log("classList:"+element.classList);
        }
        else{
          console.log("0 ko");
          var element = document.getElementById("class"+item_id);
          element.classList.remove("myHeartSave");
          element.classList.add("myHeartUnSave");
        }
      }
      
    });
    
    
  }

}
