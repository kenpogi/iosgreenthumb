import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NavController, ModalController } from '@ionic/angular';
import { IonSlides } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { PostProvider } from '../../providers/credential-provider';
import { CategoriessubPage } from '../categoriessub/categoriessub.page';
import { CategoryMain } from '../shared/model/category.model';
import { Tab1Page } from '../tab1/tab1.page';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.page.html',
  styleUrls: ['./categories.page.scss'],
})
export class CategoriesPage implements OnInit {

  @ViewChild('slides', { static: true }) slider: IonSlides;
  segment = 0;

  login_user_id : string;
  data : any;
  categoryList : CategoryMain[] = [];

  constructor(
    private router: Router,
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    private postPvdr: PostProvider,
    private storage: Storage,
  ) { }

  async segmentChanged() {
    await this.slider.slideTo(this.segment);
  }

  async slideChanged() {
    this.segment = await this.slider.getActiveIndex();
  }

  ngOnInit() {
  }

  ionViewWillEnter() {

    this.plotData();

  }


  plotData(){
    
    

    this.storage.get('greenthumb_user_id').then((user_id) => {
      this.login_user_id = user_id;
      let body321 = {
        action : 'getCategoriesMain',
        user_id : user_id
      };
      //console.log("storyalang:"+JSON.stringify(body321));
      this.postPvdr.postData(body321, 'category.php').subscribe(data => {
      

        if(data.success){
          
          const categoryList: CategoryMain[] = [];

          for(const key in data.result){


            categoryList.push(new CategoryMain(
              data.result[key].id,
              data.result[key].category_main,
              (data.result[key].category_main_photo == '') ? '' :
              this.postPvdr.myServer()+"/greenthumb/images/categories/main/"+data.result[key].category_main_photo
              ));
          }

          this.categoryList = categoryList;

        }
      });

      

    });
  }

  goBack(){
    //this.router.navigate(['describeitem']);
    window.history.back();
  }
  async selected(x, val){
    var data = {
      id: x,
      val
    }
    const modal = await this.modalCtrl.create({
            component: CategoriessubPage,
            cssClass: 'categories',
            id: 'modal21',
            componentProps: { data}
          });
    
            modal.onDidDismiss()
            .then((datafrom) => {
              // console.log("hamon2");
              // console.log("iba:"+JSON.stringify(datafrom));  
              // console.log("val:"+datafrom['data'].value);
              //console.log("iba2dfd:"+JSON.stringify(data2)); 
              
              //this.navCtrl.navigateForward(['tabs/tab1'], true, data2);

              let navigationExtras: any = {
                queryParams: { category_id : datafrom['data'].id}
            };
            this.navCtrl.navigateForward(['tabs/tab1'], navigationExtras);
            });
    
          return await modal.present();
  }

}
