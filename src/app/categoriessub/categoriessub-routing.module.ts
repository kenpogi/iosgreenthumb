import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CategoriessubPage } from './categoriessub.page';

const routes: Routes = [
  {
    path: '',
    component: CategoriessubPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CategoriessubPageRoutingModule {}
