import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CategoriessubPageRoutingModule } from './categoriessub-routing.module';

import { CategoriessubPage } from './categoriessub.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CategoriessubPageRoutingModule
  ],
  declarations: [CategoriessubPage]
})
export class CategoriessubPageModule {}
