import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CategoriessubPage } from './categoriessub.page';

describe('CategoriessubPage', () => {
  let component: CategoriessubPage;
  let fixture: ComponentFixture<CategoriessubPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategoriessubPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CategoriessubPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
