import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { Storage } from '@ionic/storage';
import { LoadingController, ToastController, AlertController, ModalController } from '@ionic/angular';
import { PostProvider } from '../../providers/credential-provider';
import { UserpolicyPage } from '../userpolicy/userpolicy.page';
import { SuspendmodalPageModule } from '../suspendmodal/suspendmodal.module';
import { SuspendmodalPage } from '../suspendmodal/suspendmodal.page';

@Component({
  selector: 'app-choose',
  templateUrl: './choose.page.html',
  styleUrls: ['./choose.page.scss'],
})
export class ChoosePage implements OnInit {
  user:any = {};
  userData : any;
  loader:any;
  ui = null;
  login_user_id : string;


  login_email : string;
  login_pass : string;

  constructor(
    private router: Router,
    public alertCtrl: AlertController,
    private loadingCtrl : LoadingController,
    public toastController: ToastController,
    public afAuth: AngularFireAuth,
    private modalController: ModalController,
    private postPvdr: PostProvider,
    private storage: Storage
    ) { }

    async loading(){
      //show loading
      this.loader = await this.loadingCtrl.create({
        message: '<img src="assets/greenthumb-images/greenthumblogo.png" style="height: 25px" height="auto" alt="loading...">',
        translucent: true,
        showBackdrop: false,
        spinner: null,
        duration: 2000
      });
      this.loader.present();
    }

    async presentToastFailed() {
      const toast = await this.toastController.create({
        message: 'Login Failed.',
        duration: 3000
      });
      toast.present();
    }
    prepareToast(){
      if(this.userData.first_name === null || this.userData.first_name === undefined || this.userData.first_name === 'null'){
        //this.presentToastPreGoogle();
        // let element: HTMLElement = document.getElementsByClassName('firebaseui-idp-google')[0] as HTMLElement;
        // element.click();
        this.loader.dismiss();
      } else {
        this.storage.set('email', this.userData.email);
        this.storage.set('login_used', "google");
        this.storage.set('fname', this.userData.first_name);

        let body = {
          action : 'getUserId',
          email : this.userData.email,
          login_type: 2
        };

        this.postPvdr.postData(body, 'credentials-api.php').subscribe(data => {
          if(data.success){
            this.storage.set("greenthumb_user_id", data.user_id);
            this.login_user_id = data.user_id
            console.log("user_id: google used:"+data.user_id);
            this.loader.dismiss();
            this.presentToast();
            this.validateNotNow();
          }
        });
        
        
      }
    }
    async presentToast() {
      const toast = await this.toastController.create({
        message: 'Welcome to Greenthumb, '+this.userData.first_name+'!',
        duration: 3000
      });
      toast.present();
    }
    // async presentToastPreGoogle() {
    //   const toast = await this.toastController.create({
    //     message: 'You can now login using Google. Please tap again the button.',
    //     duration: 5000
    //   });
    //   toast.present();
    // }
    async presentAlertnoFbEmail() {
      const alert = await this.alertCtrl.create({
      message: 'An error occurred during the sign-up registration of your Facebook account. Please verify your email in Facebook, or login through Google or SMS to register.',
      subHeader: 'Verify email in Facebook.',
      buttons: ['OK']
     });
     await alert.present(); 
  }
    
  bypassAuth(){
    this.router.navigate(["/tabs"]);
  }

  
  validateNotNow(){

    let body = {
      action : 'getNotNow',
      user_id : this.login_user_id,
    };

    this.postPvdr.postData(body, 'user.php').subscribe(data => {

      if(data.success){
        if((parseInt(data.suspended)==3)){
          this.suspendModal();
        }
        else{
          this.router.navigate(["/tabs"]);
        }
      }
      
    });
    
  }

  async suspendModal(){
    const modal = await this.modalController.create({
            component: SuspendmodalPage,
            cssClass: ''
          });
          return await modal.present();
  }

  async goToUserPolicy(){
    const modal = await this.modalController.create({
            component: UserpolicyPage,
            cssClass: ''
          });
          return await modal.present();
  }

  forgotpass(){
    this.router.navigate(['forgotpassword']);
  }
  mobile(){
    this.router.navigate(['/mobilee']);
  }

  // emailLogin(){
  //   this.router.navigate(['/email-login']);
  // }
  signUp(){
    console.log("email:"+this.login_email);
   // console.log("password:"+this.password);

    let body = {
      action : 'emailLogin',
      email : this.login_email,
      //password : this.password
    };

    console.log("bodybodykoko:"+JSON.stringify(body));
    this.postPvdr.postData(body, 'credentials-api.php').subscribe(data => {
      if(data.success){
        this.storage.set("greenthumb_user_id", data.user_id);
        // console.log("user_id:"+data.user_id);
        // console.log("email_code:"+data.email_code);

        let body2 = {
          action : 'send-email-code',
          sentTo : this.login_email,
          sentToCode : data.email_code
        };

        console.log("bodyisda:"+JSON.stringify(body2));
        this.postPvdr.sendCode(body2).subscribe(data2 => {
         //console.log("status:"+data.email_code);
         this.router.navigate(['/email-code', data.email_code]);
        });

      }
    });


  }
  // toGuest(){
  //   this.router.navigate(['tabs']);
  // }

  ngOnInit() {
   // this.afAuth.auth.signOut();
    this.storage.get("greenthumb_user_id").then((user_id) => {


      console.log("user_id in choose:"+user_id);
      if( user_id != null){
        this.presentToast();
        this.router.navigate(["/tabs"]);
      }  
      

   });
  }
   
  
}
