import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CommunityforumPage } from './communityforum.page';

const routes: Routes = [
  {
    path: '',
    component: CommunityforumPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CommunityforumPageRoutingModule {}
