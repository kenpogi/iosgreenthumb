import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CommunityforumPageRoutingModule } from './communityforum-routing.module';

import { CommunityforumPage } from './communityforum.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CommunityforumPageRoutingModule
  ],
  declarations: [CommunityforumPage]
})
export class CommunityforumPageModule {}
