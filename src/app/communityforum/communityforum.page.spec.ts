import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CommunityforumPage } from './communityforum.page';

describe('CommunityforumPage', () => {
  let component: CommunityforumPage;
  let fixture: ComponentFixture<CommunityforumPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommunityforumPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CommunityforumPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
