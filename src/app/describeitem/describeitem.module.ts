import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DescribeitemPageRoutingModule } from './describeitem-routing.module';

import { DescribeitemPage } from './describeitem.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DescribeitemPageRoutingModule
  ],
  declarations: [DescribeitemPage]
})
export class DescribeitemPageModule {}
