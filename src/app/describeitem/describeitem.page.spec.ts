import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DescribeitemPage } from './describeitem.page';

describe('DescribeitemPage', () => {
  let component: DescribeitemPage;
  let fixture: ComponentFixture<DescribeitemPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DescribeitemPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DescribeitemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
