import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NavController, ModalController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { SelectcategoriesPage } from '../selectcategories/selectcategories.page';

@Component({
  selector: 'app-describeitem',
  templateUrl: './describeitem.page.html',
  styleUrls: ['./describeitem.page.scss'],
})
export class DescribeitemPage implements OnInit {


  category_id: number = 1;
  stocks_id: number = 1;
  stocks_name : string = "";
  quantity: number;
  description: string = "";
  category_name: string = "";
  otherStock: string = "";
  custom_category: string ="";

  constructor(
    private router: Router,
    public navCtrl: NavController,
    public toastController: ToastController,
    public modalController: ModalController,
    private storage: Storage,
  ) { }

  ngOnInit() {
    this.stocks_id = 1;
    // this.storage.get("greenthumb_itempost_pic").then((pictures)=>{

    //   for (var key in pictures) {
    //     console.log("filenames:"+pictures[key].picture_filename)
    //   }
    // });
  }

  changeStock(id){
    this.stocks_id = id;
  }

  async goCategories(){
    //this.router.navigate(['categories']);


      const modal = await this.modalController.create({
        component: SelectcategoriesPage,
        cssClass: 'categories',
        id: 'myModal' ,
        //componentProps: { coin: coins}
      });

        modal.onDidDismiss()
        .then((data) => {
          console.log("data:"+JSON.stringify(data));
          this.category_id = data['data'].id; // Here's your selected category!
          this.category_name = data['data'].value;
          console.log("category_name:"+this.category_name);
        });

      return await modal.present();

    }
  
  goHelp(){
    //window.location.href="http://greenthumbtrade.com/help";
    this.navCtrl.navigateForward((['tabs/help']), { animated: false, });
  }
  goBack(){
    window.history.back();
    //this.navCtrl.navigateForward((['tab3']), { animated: false, });
  }
  goSetprice(){

    if(this.stocks_id == 1){
      this.stocks_name = "per piece";
    }
    else if(this.stocks_id == 2){
      this.stocks_name = "per pound";
    }
    else if (this.stocks_id == 3){
      this.stocks_name = "per ounce";
    }
    else{
      this.stocks_name = this.otherStock;
    }

    let data = {
      category_id : this.category_id,
      stocks_id : this.stocks_id,
      stocks_name : this.stocks_name,
      quantity : this.quantity,
      description : this.description,
      otherStock : this.otherStock,
      category_name : this.category_name
      };
      
      
      this.storage.set("greenthumb_description", data);
      // this.storage.get("description").then((val) => {
      //   console.log("category_id", val['category_id']);
      //   console.log("stocks_id", val['stocks_id']);
      //   console.log("quantity", val['quantity']);
      //   console.log("description", val['description']);
      // })
      if(this.category_name == ""){
        this.presentToast("Please Select Category");
      }
      else if(this.quantity == null){
        this.presentToast("Please Input Quantity");
      }
      else if(this.description == ""){
        this.presentToast("Please Input Description of the Item");
      }
      else{
        console.log("stocks_id:"+this.stocks_id);
        this.navCtrl.navigateForward((['setprice']), { animated: false, });
      }
    
  }

  async presentToast(x) {
    const toast = await this.toastController.create({
      message: x,
      duration: 3000
    });
    toast.present();
  }

}
