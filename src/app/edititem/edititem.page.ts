import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalController, NavController, ToastController, AlertController, ActionSheetController } from '@ionic/angular';
import { Item, ItemPicture } from '../shared/model/item.model';
import { Storage } from '@ionic/storage';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { PostProvider } from '../../providers/credential-provider';
import { SelectcategoriesPage } from '../selectcategories/selectcategories.page';
import { Crop } from '@ionic-native/crop/ngx';
import { File } from '@ionic-native/file/ngx';


import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';


@Component({
  selector: 'app-edititem',
  templateUrl: './edititem.page.html',
  styleUrls: ['./edititem.page.scss'],
})
export class EdititemPage implements OnInit {

  item_id: string = "";
  //itemList: Item[] = [];

  croppedImagepath = "";
  isLoading = false;

  login_user_id : string = "";

  user_id : string = "";

  statusSave: boolean = false;

  ImageArray: ItemPicture[] = [];

  item_name : string = "";
  item_category : string = "";
  item_price : string = "";
  item_seller : string = "";
  item_user_id : string = "";
  item_saveStatus : string = "";
  item_description : string = "";
  item_location : string = "";
  item_zipcode : number = 0;

  
  fileName : string = "";
  category_name: string = "";

  picture: any;
  complete_pic: any;


  category_id: number = 1;

  pictures: ItemPicture[] = [];

  public photos: any;
  public base64Image: string;
  public fileImage: string;
  public responseData: any;
  userData = { user_id: "", token: "", imageB64: "" };
  rand = Math.floor(Math.random() * 1000000) + 8;

  constructor(
    private router: Router,
    private postPvdr: PostProvider,
    public modalController: ModalController,
    public navCtrl: NavController,
    private toastController: ToastController,
    private alertCtrl: AlertController,
    private transfer: FileTransfer,
    private crop: Crop,
    public actionSheetController: ActionSheetController,
    private route: ActivatedRoute,
    private storage: Storage,
    private camera: Camera,
    private file: File
  ) { 

    this.route.queryParams.subscribe(params => {
      this.item_id = params["item_id"];
      console.log("params in edit item:"+JSON.stringify(params));
     
    });
  }

  ngOnInit() {
  }
  goBack(){
    window.history.back();
  }

  ionViewWillEnter(){
    this.plotData();
  }

  pickImage(sourceType) {
    const options: CameraOptions = {
      quality: 100,
      sourceType: sourceType,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      
    }
    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
     // this.croppedImagePath = 'data:image/jpeg;base64,' + imageData;
    
     this.cropImage(imageData)
    }, (err) => {
      // Handle error
    });
  }

  // takePhoto() {
  //   console.log("coming here");

  //   this.pickImage(this.camera.PictureSourceType.CAMERA);
  // }

  async selectImage() {
    const actionSheet = await this.actionSheetController.create({
      header: "Select Image source",
      buttons: [{
        text: 'Load from Library',
        handler: () => {
          this.pickImage(this.camera.PictureSourceType.PHOTOLIBRARY);
        }
      },
      {
        text: 'Use Camera',
        handler: () => {
          this.pickImage(this.camera.PictureSourceType.CAMERA);
        }
      },
      {
        text: 'Cancel',
        role: 'cancel'
      }
      ]
    });
    await actionSheet.present();
  }

  plotData(){

    this.storage.get('greenthumb_user_id').then((user_id) => {
      this.login_user_id = user_id;

      

      let body = {
        action : 'viewPostItem',
        item_id : this.item_id,
        user_id : user_id
      };
      console.log(JSON.stringify(body));
      this.postPvdr.postData(body, 'post_item.php').subscribe(async data => {
        
        if(data.success){

          const items: Item[] = [];

          //var pictureProfile: string = '';

          for(const key in data.result){
            this.item_name = data.result[key].title;
            this.item_category = data.result[key].category;
            this.item_seller = data.result[key].username;

            this.item_user_id = data.result[key].user_id;
            this.item_price = data.result[key].price;
            this.item_location = data.result[key].location;
            this.item_zipcode = data.result[key].zip_code;
            this.item_saveStatus = data.result[key].saveStatus;
            this.item_description = data.result[key].description;

            this.picture = data.result[key].profile_photo;
            this.complete_pic = this.postPvdr.myServer()+"/greenthumb/images/"+this.picture;
          }

          
        }
      });


      let body4 = {
        action : 'getItemPhotos',
        item_id : this.item_id,
       // user_id : user_id
      };
      console.log(JSON.stringify(body4));
      this.postPvdr.postData(body4, 'post_item.php').subscribe(async data => {
        
        if(data.success){

          const itemsImage: ItemPicture[] = [];

          //var pictureProfile: string = '';

          for(const key in data.result){
            itemsImage.push(new ItemPicture(
              this.postPvdr.myServer()+"/greenthumb/images/posted_items/images/"+data.result[key].item_photo,
              )
            );

              console.log("item_photo:"+data.result[key].item_photo);
          }
          this.ImageArray = itemsImage;
        }
      });


    });
  }

  cropImage(fileUrl) {
  
    this.crop.crop(fileUrl, { quality: 50 })
      .then(
        newPath => {
          this.showCroppedImage(newPath.split('?')[0])
        },
        error => {
          alert('Error cropping image' + error);
        }
      );
  }

  goSellfaster(){
    //this.router.navigate(['sellfaster']);
    let navigationExtras: any = {
      queryParams: { item_id: this.item_id}
   };
      this.navCtrl.navigateForward(['sellfaster'], navigationExtras);
    
  }

  showCroppedImage(ImagePath) {
    this.isLoading = true;
    var copyPath = ImagePath;
    var splitPath = copyPath.split('/');
    var imageName = splitPath[splitPath.length - 1];
    var filePath = ImagePath.split(imageName)[0];

    this.file.readAsDataURL(filePath, imageName).then(base64 => {
      this.croppedImagepath = base64;
      this.base64Image = base64;
      // this.photos.push(this.base64Image);
      // this.photos.reverse();
      this.sendData(base64);
      this.isLoading = false;
    }, error => {
      alert('Error in showing image' + error);
      this.isLoading = false;
    });
  }

  sendData(imageData) {

    const fileTransfer: FileTransferObject = this.transfer.create();

    this.rand = Math.floor(Math.random() * 1000000) + 8;

    this.fileName = "greenthumb_"+this.rand+"_"+this.login_user_id+".jpg";
    console.log("this.filename for transfer:"+this.fileName);

    this.userData.imageB64 = imageData;
    this.userData.user_id = "1";
    this.userData.token = "222";
    //console.log(this.userData);
   // console.log("image:"+this.base64Image);

    let options: FileUploadOptions = {
      fileKey: 'photo',
      fileName: this.fileName,
      chunkedMode: false,
      httpMethod: 'post',
      mimeType: "image/jpeg",
      headers: {}
    }

    fileTransfer.upload(this.base64Image, this.postPvdr.myServer()+'/greenthumb/images/posted_items/images_upload.php', options)
    .then((data) => {
      

      this.ImageArray.push(new ItemPicture(this.postPvdr.myServer()+'/greenthumb/images/posted_items/images/'+this.fileName));
     // this.storage.set("greenthumb_itempost_pic",this.pictures);

      let body = {
        action : 'updateItemPhoto',
        item_id : this.item_id,
        item_photo : this.fileName
      };
      this.postPvdr.postData(body, 'post_item.php').subscribe(async data => {


        if(data.success){

          
        }


        

      });

    }, (err) => {
      console.log(err);
      alert("Please check your internet connection and try again.");
    });
   
    // this.authService.postData(this.userData, "userImage").then(
    //   result => {
    //     this.responseData = result;
    //   },
    //   err => {
        
    //   }
    // );
  }

  deletePhoto(item_photo, index) {
    this.alertCtrl.create({
      header: "Sure you want to delete this photo? There is NO undo!",
      message: "",
      buttons: [
        {
          text: "No",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Yes",
          handler: () => {
            console.log("Agree clicked");
            this.ImageArray.splice(index, 1);
            this.confirmDelete(item_photo);
          }
        }
      ]
    }).then(res => {
      res.present();
    });
    
  }
  confirmDelete(index){

    var res = index.split("/posted_items/images/");
     // this.livestreamId = res[0];
    var item_photo = res[1];

    console.log("item_photo:"+item_photo);

    let body4 = {
      action : 'deleteItemPhoto',
      item_photo
     // user_id : user_id
    };
    this.postPvdr.postData(body4, 'save_item.php').subscribe(async data => {
      
      if(data.success){

      }
    });

  }

  

  saveEdit(){
    let body = {
      action : 'updateItem',
      item_id : this.item_id,
      item_name : this.item_name,
      item_price : this.item_price,
      item_category : this.category_id,
      item_description: this.item_description,
      item_location : this.item_location,
      item_zipcode : this.item_zipcode,
      user_id : this.login_user_id
    };
    console.log(JSON.stringify(body));
    this.postPvdr.postData(body, 'post_item.php').subscribe(async data => {
      
      if(data.success){
        this.presentToast();
      }
    });
  }
  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Item has been updated.',
      duration: 3000
    });
    toast.present();
  }

  async goCategories(){
    //this.router.navigate(['categories']);


      const modal = await this.modalController.create({
        component: SelectcategoriesPage,
        cssClass: 'categories',
        id: 'myModal' ,
        //componentProps: { coin: coins}
      });

        modal.onDidDismiss()
        .then((data) => {
          console.log("data:"+JSON.stringify(data));
          this.category_id = data['data'].id; // Here's your selected category!
          this.item_category = data['data'].value;
          //console.log("category_name:"+this.category_name);
        });

      return await modal.present();

    }
  

}
