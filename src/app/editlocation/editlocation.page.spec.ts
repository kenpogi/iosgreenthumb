import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EditlocationPage } from './editlocation.page';

describe('EditlocationPage', () => {
  let component: EditlocationPage;
  let fixture: ComponentFixture<EditlocationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditlocationPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EditlocationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
