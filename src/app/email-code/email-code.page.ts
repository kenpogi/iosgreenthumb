import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';

@Component({
  selector: 'app-email-code',
  templateUrl: './email-code.page.html',
  styleUrls: ['./email-code.page.scss'],
})
export class EmailCodePage implements OnInit {

  email_code : string ;

  server_code : string;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { 

  }

  ngOnInit() {
    this.server_code = this.activatedRoute.snapshot.paramMap.get("email_code");
    console.log("email_code:"+this.server_code);
  }


  confirm(){
    if(this.email_code==this.server_code){
      console.log("alright pareho kamo");
      this.router.navigate(["/tabs"]);
    }
    else{
      console.log("mag-iba man ang code friend");
    }
  }

}
