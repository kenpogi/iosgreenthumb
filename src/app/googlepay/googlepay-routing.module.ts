import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GooglepayPage } from './googlepay.page';

const routes: Routes = [
  {
    path: '',
    component: GooglepayPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GooglepayPageRoutingModule {}
