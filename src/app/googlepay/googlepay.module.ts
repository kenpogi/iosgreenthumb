import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GooglepayPageRoutingModule } from './googlepay-routing.module';

import { GooglepayPage } from './googlepay.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GooglepayPageRoutingModule
  ],
  declarations: [GooglepayPage]
})
export class GooglepayPageModule {}
