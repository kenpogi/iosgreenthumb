import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GooglepayPage } from './googlepay.page';

describe('GooglepayPage', () => {
  let component: GooglepayPage;
  let fixture: ComponentFixture<GooglepayPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GooglepayPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GooglepayPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
