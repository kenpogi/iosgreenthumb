import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-googlepay',
  templateUrl: './googlepay.page.html',
  styleUrls: ['./googlepay.page.scss'],
})
export class GooglepayPage implements OnInit {

  constructor(
    private router: Router
  ) { }

  goBack(){
    // this.router.navigate(['help']);
    window.history.back();
  }

  ngOnInit() {
  }

}
