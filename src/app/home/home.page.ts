import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  
  
  ImageArray: any = [];
  
  

  constructor(public navCtrl: NavController,
    private router: Router) {


      this.ImageArray = [
        {'image':'assets/greenthumb-images/bunny2.png',
          'name': 'Lorem ipsum dolor 1',
          'text': 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque'},
        {'image':'assets/greenthumb-images/bunny1.png',
        'name': 'Lorem ipsum dolor 2',
        'text': 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque'},
        {'image':'assets/greenthumb-images/bunny3.png',
        'name': 'Lorem ipsum dolor 3',
        'text': 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque'}
      ]

  }

  goTerms(){
    this.router.navigate(['termsconditionslogin']);
  }

  slideOptsOne = {
    initialSlide: 0,
    slidesPerView: 1,
    autoplay:true
   };

  


   
   


}

