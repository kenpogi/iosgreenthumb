import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-invite',
  templateUrl: './invite.page.html',
  styleUrls: ['./invite.page.scss'],
})
export class InvitePage implements OnInit {

  constructor(
    private router: Router
  ) { }

  goBack(){
    // this.router.navigate(['help']);
    window.history.back();
  }

  ngOnInit() {
  }

}
