import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ItempostedPage } from './itemposted.page';

const routes: Routes = [
  {
    path: '',
    component: ItempostedPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ItempostedPageRoutingModule {}
