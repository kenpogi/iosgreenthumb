import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ItempostedPageRoutingModule } from './itemposted-routing.module';

import { ItempostedPage } from './itemposted.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ItempostedPageRoutingModule
  ],
  declarations: [ItempostedPage]
})
export class ItempostedPageModule {}
