import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ItempostedPage } from './itemposted.page';

describe('ItempostedPage', () => {
  let component: ItempostedPage;
  let fixture: ComponentFixture<ItempostedPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItempostedPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ItempostedPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
