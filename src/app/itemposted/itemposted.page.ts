import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { PostProvider } from '../../providers/credential-provider'; 
import { ItemPicture } from '../shared/model/item.model';

@Component({
  selector: 'app-itemposted',
  templateUrl: './itemposted.page.html',
  styleUrls: ['./itemposted.page.scss'],
})
export class ItempostedPage implements OnInit {


  user_id : string;
  category_id : number;
  stocks_id : number;
  stocks_name : string;
  quantity : number;
  title : string;
  description : string;
  price : number;
  zip_code : number;
  location : string;
  district_area : string;
  delivery_type_id : number;
  delivery_type : string;
  category_name : string;

  username: string = "";

  ImageArray: ItemPicture[] = [];
  picture: any;
  complete_pic: any;

  item_id : string ; 

  constructor(
    private router: Router,
    private storage: Storage,
    private postPvdr: PostProvider,
    public navCtrl: NavController,
    private route: ActivatedRoute
  ) {

    this.route.queryParams.subscribe(params => {
      this.item_id = params["item_id"];
      console.log("item_id itemposted:"+this.item_id);
    });
   }

  ngOnInit() {
  }


  slideOptsOne = {
    initialSlide: 0,
    slidesPerView: 1,
    autoplay:true
   };

  ionViewWillEnter() {




    this.storage.get("greenthumb_user_id").then((user_id) => {

      this.storage.get("greenthumb_title").then((title) => {
          
          this.storage.get("greenthumb_description").then((val) => {

            this.storage.get("greenthumb_price").then((price) => {

              this.storage.get("greenthumb_location").then((item) => {

                  this.user_id = user_id;
                  this.category_id = val['category_id'];
                  this.category_name = val['category_name'];
                  this.stocks_id = val['stocks_id'];
                  this.stocks_name = val['stocks_name'];
                  this.quantity = val['quantity'];
                  this.title = title;
                  this.description = val['quantity'];
                  this.price = price;
                  this.zip_code = item['zip_code'];
                  this.location = item['location'];
                  this.district_area = item['district_area'];
                  this.delivery_type_id = item['delivery_type_id'];
                  this.delivery_type = item['delivery_type'];
                  
               

              });

            });


          });
      });

      let body = {
        action : 'getUsername',
        user_id : user_id,
      };
      console.log(JSON.stringify(body));
      this.postPvdr.postData(body, 'user.php').subscribe(async data => {
  
        this.username = data.username;

        this.picture = data.profile_photo;
        this.complete_pic = this.postPvdr.myServer()+"/greenthumb/images/"+this.picture;

        
      });
    });

    this.storage.get("greenthumb_itempost_pic").then((pictures)=>{

      for (var key in pictures) {
        // console.log("filenames:"+pictures[key].picture_filename)
        this.ImageArray.push(new ItemPicture(
        this.postPvdr.myServer()+"/greenthumb/images/posted_items/images/"+pictures[key].picture_filename,
        )
        );
      }

    });
  }

  goHelp(){
    this.router.navigate(['tabs/help']);
  }
  goSellerprof(){

  }
  goSellfaster(){
    //this.router.navigate(['sellfaster']);
    let navigationExtras: any = {
      queryParams: { item_id: this.item_id}
   };
      this.navCtrl.navigateForward(['sellfaster'], navigationExtras);
    
  }
  goBack(){
    //this.navCtrl.navigateForward((['shareitem']), { animated: false, });
  }
  goHome(){
    //this.router.navigate(['tabs/tab1']);
    this.navCtrl.navigateRoot(['tabs/tab1']);
  }
  goPostagain(){
    this.router.navigate(['tab3']);
  }

}
