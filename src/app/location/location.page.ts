import { Component, OnInit, ViewChild, ElementRef  } from '@angular/core';
import { Router } from '@angular/router';
import {  Platform } from '@ionic/angular';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Storage } from '@ionic/storage';
import { PostProvider } from '../../providers/credential-provider';
import { NativeGeocoder, NativeGeocoderResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder/ngx';


declare var google;

@Component({
  selector: 'app-location',
  templateUrl: './location.page.html',
  styleUrls: ['./location.page.scss'],
})
export class LocationPage implements OnInit {

  @ViewChild('map', { static: false }) mapElement: ElementRef;

  map: any;

  lat: any;
  lng: any;
  myCurrentLocation : string = '';

  login_user_id = '';

  constructor(
    private router: Router,
    private nativeGeocoder: NativeGeocoder,
    private plt: Platform,
    private storage: Storage,
    private postPvdr: PostProvider,
    private geo : Geolocation
  ) { 
    this.getCurrentLocation();
  }

  ngOnInit() {
  }

  goBack(){
    window.history.back();
  }

  getCurrentLocation(){


    this.storage.get("greenthumb_user_id").then((user_id) => {
      this.login_user_id = user_id;
      let body = {
        action : 'check_location',
        user_id : user_id
      };
      this.postPvdr.postData(body, 'user.php').subscribe(async data => {
        if(data.success){

          if(data.my_location != ''){
            
            this.lat = data.latitude;
            this.lng = data.longitude;
            this.myCurrentLocation = data.my_location;


            this.drawMap();

          }
          else{
            this.getGeoLocation();
          }


          


        } 

      });
    });


    
    
    //this.plotData();

  }

  getGeoLocation(){
    this.geo.getCurrentPosition({
      timeout: 10000,
      enableHighAccuracy: true
    }).then((res)=>{
      this.lat = res.coords.latitude;
      this.lng = res.coords.longitude;
      
      let options: NativeGeocoderOptions = {
        useLocale: true,
        maxResults: 5
      };
    
      this.nativeGeocoder.reverseGeocode(this.lat, this.lng, options)
      .then((result: NativeGeocoderResult[]) =>{
        console.log(JSON.stringify(result[0]));
        // console.log("re:"+result[0].locality);
        // console.log("are:"+result[0].subAdministrativeArea);
        // console.log("th:"+result[0].subThoroughfare);
        this.myCurrentLocation = result[0].thoroughfare+' '
        +result[0].locality+', '+result[0].subAdministrativeArea


        let body = {
          action : 'updateLocation',
          user_id : this.login_user_id,
          latitude: this.lat,
          longitude: this.lng,
          my_location : this.myCurrentLocation
        };
        this.postPvdr.postData(body, 'user.php').subscribe(async data => {
       
          
        });


      })
      .catch((error: any) => {
        console.log(error);
        this.getGeoLocation();
      });

      this.drawMap();

    }).catch((er)=>{
      this.getGeoLocation();
    });
  }

  drawMap(){
    let mapOptions = {
      zoom: 13,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      mapTypeControl: false,
      streetViewControl: false,
      fullscreenControl: false
    }
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

    let latLng = new google.maps.LatLng(this.lat, this.lng);
    this.map.setCenter(latLng);
    this.map.setZoom(14);
  }

  resetLocation(){
    this.myCurrentLocation = '';
  }

  ionViewDidEnter(){
    this.plt.ready().then(() => {

      console.log("location load:");

      // let mapOptions = {
      //   zoom: 13,
      //   mapTypeId: google.maps.MapTypeId.ROADMAP,
      //   mapTypeControl: false,
      //   streetViewControl: false,
      //   fullscreenControl: false
      // }
      // this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

      // let latLng = new google.maps.LatLng(30, 30);
      // this.map.setCenter(latLng);
      // this.map.setZoom(5);
      // console.log("i go from here");
     // console.log("sdf:"+JSON.stringify(this.map));
  
      // this.geo.getCurrentPosition().then(pos => {
      //   let latLng = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
      //   this.map.setCenter(latLng);
      //   this.map.setZoom(16);
      //   console.log("sdf:"+JSON.stringify(this.map));
      // }).catch((error) => {
      //   console.log('Error getting location', error);
      // });


    });
  }


}
