import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LogoutscreenPage } from './logoutscreen.page';

const routes: Routes = [
  {
    path: '',
    component: LogoutscreenPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LogoutscreenPageRoutingModule {}
