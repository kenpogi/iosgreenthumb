import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LogoutscreenPageRoutingModule } from './logoutscreen-routing.module';

import { LogoutscreenPage } from './logoutscreen.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LogoutscreenPageRoutingModule
  ],
  declarations: [LogoutscreenPage]
})
export class LogoutscreenPageModule {}
