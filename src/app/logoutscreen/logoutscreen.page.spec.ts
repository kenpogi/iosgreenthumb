import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LogoutscreenPage } from './logoutscreen.page';

describe('LogoutscreenPage', () => {
  let component: LogoutscreenPage;
  let fixture: ComponentFixture<LogoutscreenPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogoutscreenPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LogoutscreenPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
