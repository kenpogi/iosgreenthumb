import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Socket } from 'ngx-socket-io';
import { ToastController, AlertController } from '@ionic/angular';
import { ChatMessages} from '../../providers/chat-messages.provider';
import { PostProvider } from '../../providers/credential-provider';
import { Storage } from '@ionic/storage';
import { chat, Conversations } from '../shared/model/chat.model';
import { NavController } from '@ionic/angular';
import { Item } from '../shared/model/item.model';

@Component({
  selector: 'app-message',
  templateUrl: './message.page.html',
  styleUrls: ['./message.page.scss'],
})
export class MessagePage implements OnInit, OnDestroy {

  message = '';
  messages = [];
  currentUser = '';

  receiversId = '';
  session_id = '';
  user_id = '';
  item_id = '';

  login_user_id: any ;

  user_rating: string ;

  nickname = '';

  conv_image = '';

  messagesFromDB: chat[] = [];

  item_name : string = "";
  item_category : string = "";
  item_price : string = "";
  item_seller : string = "";
  item_user_id : string = "";
  item_saveStatus : string = "";
  item_status : string = "";
  item_description : string = "";
  item_location : string = "";
  item_quantity : number;

  item_date : string = "";
  item_time : string = "";

  stocks_id : string = "";
  item_other_stock : string;

  item_cover_photo : string = "";
  picture: any;
  complete_pic: any;

  loader = false;

  withPurchasedPending = false;
  quantity_purchased : any;
  total_amount : any;

  ownProfile : boolean = false;
  alreadySold = false;

  fromPurchase = false;

  constructor(
    private router: Router,
    private chatMessages: ChatMessages,
    private storage: Storage,
    private alertCtrl: AlertController,
    public activatedRoute: ActivatedRoute,
    private postPvdr: PostProvider,
    private socket: Socket, private toastCtrl: ToastController,
    public navCtrl: NavController) { }

  goTab5(){
    //this.router.navigate(['tabs/tab5']);
    window.history.back();
    //this.navCtrl.navigateRoot(['tabs/tab5']);
  }

  // sendMessage() {
  //   let body = {
  //     action : "addMessage"
  //   };
  //   this.chatMessages.sendMessage(body, 'credentials-api.php').subscribe(data => {
  //     if(data.success){

  //     }
  //   });


  // }

  ngOnInit(){} 

  ngOnDestroy(){
    this.socket.disconnect();
  }
  
  async ionViewWillEnter() {

    var paramId = this.activatedRoute.snapshot.paramMap.get("id");

    var res = paramId.split("-separator-");
     // this.livestreamId = res[0];
      this.item_id = res[1];

    this.receiversId = res[0];

    this.fromPurchase = res[2]=='1' ? true : false;

    await this.plotData(this.receiversId);
    




    // let body = {
    //   action : "addMessage"
    // };
  //   this.chatMessages
  //     .getMessages(body, 'credentials-api.php')
  //     .distinctUntilChanged()
  //     .filter((message) => message.trim().length > 0)
  //     .throttleTime(1000)
  //     .scan((acc: string, message: string, index: number) =>
  //         `${message}(${index + 1})`
  //       , 1)
  //     .subscribe((message: string) => {
  //       // const currentTime = moment().format('hh:mm:ss a');
  //       // const messageWithTimestamp = `${currentTime}: ${message}`;
  //       // this.messages.push(messageWithTimestamp);
  //     });
  // }
  }

  sendMessage() {
    this.loader = true;
    this.socket.emit('send-message', { 
      user_id: this.user_id, 
      room: this.session_id,
       text: this.message,
       vip_packageOfUser: "" });

 

    let body = {
      action : 'insertConvo',
      user_id : this.user_id,
      receiversId : this.receiversId,
      session_id : this.session_id,
      message : this.message,
      item_id : this.item_id
    };
    console.log("sessionId in inserting new message:"+this.session_id);
    this.postPvdr.postData(body, 'messages.php').subscribe(async data => {
      if(data.success){
        

      }

    });



    this.message = '';

  }

  ionViewWillLeave() {

    console.log("will leave message and disconnect to socket");

    if(this.session_id != ''){

      let body = {
        action : 'updateConvoStatus',
        receiversId : this.receiversId,
        session_id : this.session_id
      };
      this.postPvdr.postData(body, 'messages.php').subscribe(async data => {
        if(data.success){
          
  
        }
  
      });

    }

    
    this.socket.disconnect();
  }

 async plotData(val){

    await this.storage.get("greenthumb_user_id").then( async (user_id) => {

      this.login_user_id = user_id;
      let body = {
        action : 'getSessionId',
        user_id : user_id,
        receiversId : val,
        item_id : this.item_id
      };
      console.log("getSession:"+JSON.stringify(body));
       this.postPvdr.postData(body, 'messages.php').subscribe( async data => {
        if( data.success){
          this.session_id =  data.session_id;
          this.user_id =  user_id;


          if(this.fromPurchase){
            let body = {
              action : 'insertInitialConvo',
              user_id : this.user_id,
              session_id : this.session_id,
              message : this.message,
            };
            this.postPvdr.postData(body, 'messages.php').subscribe(async data => {
              // if(data.success){
                
        
              // }
        
            });
          }

          console.log("hey123");
          this.messagesFromDB = [];
      
          this.socket.connect();
      
      
          // Join chatroom
          console.log("user_id:"+this.user_id+"room:"+this.session_id);
          this.socket.emit('joinRoom', { 
            user_id: this.user_id, room: this.session_id, profile_picture: '', vip_packageOfUser: ""});
      
          //let name = `user-${new Date().getTime()}`;
          let name = this.nickname;
          this.currentUser = name;
      
          //this.socket.emit('set-name', name);
      
          // this.socket.fromEvent('users-changed').subscribe(data => {
          //   let user = data['user'];
          //   if (data['event'] === 'left') {
          //     this.showToast('User left: ' + user);
          //   } else {
          //     this.showToast('User joined: ' + user);
          //   }
          // });
      
          this.socket.fromEvent('message').subscribe(message => {
            this.messages.push(message);
            console.log("human og send"+ JSON.stringify(message));
            this.loader = false;
          });
      
          this.socket.fromEvent('joinRoomMessage').subscribe(message => {
            console.log("message from server:"+message);
            
          });


          if(data.session_chat != '0'){
            let body3 = {
              action : 'getConvo',
              session_id : this.session_id

            };
            console.log("getConvo:"+JSON.stringify(body3));
            this.postPvdr.postData(body3, 'messages.php').subscribe(async data => {
              if(data.success){
                

                for(const key in data.result){
                  this.messagesFromDB.push(new chat(
                    data.result[key].id,
                    data.result[key].session_id,
                    data.result[key].messaged_user_id,
                    data.result[key].message,
                    data.result[key].date,
                    data.result[key].time,
                    data.result[key].status
                    ));
                }

      
                
      
              }
      
            });
          }

          if(data.session_id != ''){

            let body = {
              action : 'updateConvoStatus',
              receiversId : this.receiversId,
              session_id : data.session_id
            };
            this.postPvdr.postData(body, 'messages.php').subscribe(async data => {
              if(data.success){
                
        
              }
        
            });
      
          }


        }

      });


       // plot Data for product
        let body123 = {
          action : 'viewPostItemforMessage',
          item_id : this.item_id,
          user_id : this.login_user_id
        };
        console.log(JSON.stringify(body123));
        this.postPvdr.postData(body123, 'post_item.php').subscribe(async data => {
          
          if(data.success){

            const items: Item[] = [];

            //var pictureProfile: string = '';

            for(const key in data.result){
              // items.push(new Item(
              //   data.result[key].item_id,
              //   data.result[key].user_id,
              //   data.result[key].username,
              //   data.result[key].price,
              //   data.result[key].title,
              //   data.result[key].category,
              //   data.result[key].location,
              //   data.result[key].saveStatus,
              //   data.result[key].date,
              //   data.result[key].time
              //   )
              // );

              this.item_name = data.result[key].title;
              this.item_category = data.result[key].category;
              this.item_seller = data.result[key].username;

              this.item_user_id = data.result[key].user_id;
              this.item_price = data.result[key].price;
              this.item_location = data.result[key].location + ' '+ data.result[key].city;
              this.item_saveStatus = data.result[key].saveStatus;
              this.item_description = data.result[key].description;

              this.item_quantity = parseInt(data.result[key].quantity);

              this.item_date = data.result[key].date;
              this.item_time = data.result[key].time;

              this.item_status = data.result[key].status;

              this.stocks_id = data.result[key].stocks_id;

              this.item_other_stock = data.result[key].others_stock;
              

              this.item_cover_photo = (data.result[key].item_photo == '') ? '' :
              this.postPvdr.myServer()+"/greenthumb/images/posted_items/images/"+data.result[key].item_photo;

              this.picture = data.result[key].profile_photo;
              this.complete_pic = this.postPvdr.myServer()+"/greenthumb/images/"+this.picture;

              this.user_rating = data.result[key].user_rating;
            }

            this.ownProfile = this.login_user_id == this.item_user_id ? true : false;
            
          }
        });


        // get purchased pending

        let body212 = {
          action : 'getPurchasePending',
          user_id : user_id,
          user_id_2 : val,
          item_id : this.item_id
        };
        console.log('getpurchasepending:'+JSON.stringify(body212));
        this.postPvdr.postData(body212, 'save_item.php').subscribe(async data => {
          if(data.success){
           
    
            if(data.pending_purchase){
              this.withPurchasedPending = true;
              this.quantity_purchased = data.quantity_purchased;
              this.total_amount = data.total_amount;

              if(data.mystatus == "3"){
                this.alreadySold = true;
              }
              
            }
            else{

            }
    
          }
    
        });



    });


    let body2 = {
      action : 'getuserdata',
      user_id : val
    };
    this.postPvdr.postData(body2, 'credentials-api.php').subscribe(async data => {
      if(data.success){
       

        if(data.result.nickname == ""){
          
          if(data.result.login_type_id == "3"){
            this.nickname = data.result.mobile_num;
          }
          else if(data.result.login_type_id == "4"){
            this.nickname = data.result.email;
          }
          else{
            this.nickname = data.result.fname;
          }
        }
        else{
          this.nickname = data.result.nickname;
        }

        this.conv_image = (data.result.photo == '') ? '' :
        this.postPvdr.myServer()+"/greenthumb/images/"+data.result.photo;
      }

    });

    

   

  }

  confirmMarkSold(){
    this.alertCtrl.create({
      header: "Greenthumb Trade",
      message: "Are you sure to mark this as sold?",
      buttons: [
        {
          text: "No",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Yes",
          handler: () => {
            console.log("Agree clicked");
            this.markSold();
          }
        }
      ]
    }).then(res => {
      res.present();
    });
  }

  markSold(){
    console.log("gi sold lamang pud");
    let body = {
      action : 'markSoldItem',
      item_id : this.item_id,
      user_id : this.login_user_id,
      purchased_user_id : this.receiversId,
      item_status : this.item_status,
      item_name : this.item_name,
      quantity_purchased : this.quantity_purchased
    };

    console.log(JSON.stringify(body));
    this.postPvdr.postData(body, 'save_item.php').subscribe(data => {

      if(data.success){

        this.alreadySold = true;
        // if(data.saveStatus == '1'){
        //   console.log('1 ko');
        //   var element = document.getElementById("class"+item_id);
        //   element.classList.remove("myHeartUnSave");
        //   element.classList.add("myHeartSave");
        //   console.log("classList:"+element.classList);
        // }
        // else{
        //   console.log("0 ko");
        //   var element = document.getElementById("class"+item_id);
        //   element.classList.remove("myHeartSave");
        //   element.classList.add("myHeartUnSave");
        // }
      }
      
    });
    
    
  }

  async showToast(msg) {
    let toast = await this.toastCtrl.create({
      message: msg,
      position: 'top',
      duration: 2000
    });
    toast.present();
  }

}
