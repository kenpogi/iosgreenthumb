import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PostProvider } from '../../providers/credential-provider';
import { Storage } from '@ionic/storage';
import { NavController, ModalController } from '@ionic/angular';
import { RegisterPage } from '../register/register.page';

@Component({
  selector: 'app-myaccount',
  templateUrl: './myaccount.page.html',
  styleUrls: ['./myaccount.page.scss'],
})
export class MyaccountPage implements OnInit {
  user: any;
  id: any;
  picture: any;
  fname: any;
  lname: any;
  city: any;
  country: any;
  complete_pic: any;

  login_type : any;

  user_level: number;
  user_locator_id : string;
  user_experience : number;
  gold_bar : number;
  bracket_to : number;
  user_coins : number;

  totalFollower: number;
  totalFollowing: number;
  badge_name : string;

  username: string;


  login_user_id = "";

  user_rating: string = "";

  vip_package = "";
 
  constructor(
    private router: Router,
    private postPvdr: PostProvider,
    private modalController: ModalController,
    public navCtrl: NavController,
    private storage: Storage
    ) { }
    
  goHelp(){
      //window.location.href="http://greenthumbtrade.com/help";
      this.navCtrl.navigateForward((['tabs/help']), { animated: false, });
    }
    goCommunityForum(){
      this.navCtrl.navigateForward((['tabs/communityforum']), { animated: false, });
    }
    goInvite(){
      this.router.navigate(['invite']);
    }
    goSubscribe(){
      this.router.navigate(['subscribe']);
    }
    goApple(){
      this.router.navigate(['applepay']);
    }
    goGoogle(){
      this.router.navigate(['googlepay']);
    }
    goArchive(){
      this.router.navigate(['archieve']);
    }
    goOffers(){
      this.router.navigate(['tabs/offers']);
    }

    async openRegistermodal() {
          const modal = await this.modalController.create({
            component: RegisterPage,
            cssClass: 'registermodalstyle'
          });

      // modal.onDidDismiss()
      //   .then((data) => {
      //     let body = {
      //       action : 'notNowUpdate',
      //       user_id : this.login_user_id,
      //     };
  
      //     this.postPvdr.postData(body, 'user.php').subscribe(data => {

      //       if(data.success){
      //       }
            
      //     });
      //   });


          return await modal.present();
        }


    goViewprofile(){
      this.router.navigate(['tabs/viewprofile']);

      let navigationExtras: any = {
        queryParams: { user_profile_id : this.login_user_id}
     };

     console.log("user_pfo:"+this.login_user_id);
        this.navCtrl.navigateForward(['viewitemsellerprofile'], navigationExtras);
    }
    goAccountsettings(){
      this.router.navigate(['accountsettings']);
    }
  goBack(){
    this.router.navigate(['tabs']);
  }
  goFavorites(){
    this.router.navigate(['tabs/tab2']);
  }
  goSettings(){
    this.router.navigate(['settings']);
  }
  goVIP(){
    this.router.navigate(['vipackage']);
  }
  goFollow(){
    this.router.navigate(['tabs/sellersifollow']);
  }
  goEditProfile()
  {
    this.router.navigate(['editprofile']);
  }
  goLogout(){
    this.router.navigate(['logoutscreen']);
  }
  goWallet(){
    this.router.navigate(['wallet']);
  }
  goPaymentRequest(){
    this.router.navigate(['payoneerid']);
  }
  goPublished(){
    this.router.navigate(['mybroadcast']);
  }
  goStore(){
    this.router.navigate(['tab4']);
 }
 goVIPPackage(){
  this.router.navigate(['vippackage']);
 }
 goPaymentmethod(){
  this.router.navigate(['paymentsettings']);
 }
 update_photo(){
  this.router.navigate(['uploadphoto']);
}

checkSubscription(){

  this.storage.get("user_id").then((user_id) => {

    let body2 = {
      action : 'checkSubscription',
      user_id : user_id
    };
    this.postPvdr.postData(body2, 'subscription.php').subscribe(data => {
      
      if(data.success){
        for(const key in data.result){
          this.vip_package = data.result[key].vip_package_id;
        }
      }
    });

  });
}

  ngOnInit() {
    //this.loadUser();
  }



  ionViewWillEnter() {
    this.loadUser();
    //this.plotData();
    //this.checkSubscription();
  }

  plotData(){

    this.storage.get('greenthumb_user_id').then((user_id) => {
      

      

      let body = {
        action : 'getUsername',
        user_id : user_id,
      };
      console.log(JSON.stringify(body));
      this.postPvdr.postData(body, 'user.php').subscribe(async data => {
  
        this.username = data.username;

        
      });
    });
      
  }

   loadUser(){

      this.storage.get("greenthumb_user_id").then((user_id) => {

        this.login_user_id = user_id;
              let body = {
                action : 'getuserdata',
                user_id : user_id
              };

              this.postPvdr.postData(body, 'credentials-api.php').subscribe(async data => {
                if(data.success){
                  this.id = data.result.id;
                  this.picture = data.result.photo;

                  this.username = data.result.nickname;

                  this.storage.set('id', this.id);
                  this.storage.set('picture', this.picture);
                
                  this.complete_pic = this.postPvdr.myServer()+"/greenthumb/images/"+this.picture;
                  console.log("mycompletePic:"+this.complete_pic);
                  this.id = data.result.id;
                  this.fname = data.result.fname;
                  this.lname = data.result.lname;
                  this.city = data.result.city;
                  this.country = data.result.country;
                  this.user_locator_id = data.result.user_locator_id;

                  this.user_rating = data.result.user_rating;

                  this.login_type = data.result.login_type_id;

                  console.log("login_type:"+this.login_type);
                  // console.log("user_leveldd:"+this.user_level);
                  // console.log("user_locator_id:"+this.user_locator_id);
                } else {
                  this.id = "0";
                  this.fname = "Anonymous";
                  this.lname = "User";
                  this.city = "Unknown";
                  this.country = "Address";
                  this.user_locator_id = "0";
                  this.user_level = 1;
                }
              });
      });
    
    
  }

}
