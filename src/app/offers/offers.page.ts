import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { IonSlides, NavController, AlertController, ToastController } from '@ionic/angular';
import { Item } from '../shared/model/item.model';

import { Storage } from '@ionic/storage';
import { PostProvider } from '../../providers/credential-provider';

@Component({
  selector: 'app-offers',
  templateUrl: './offers.page.html',
  styleUrls: ['./offers.page.scss'],
})
export class OffersPage implements OnInit {

  @ViewChild('slides', { static: true }) slider: IonSlides;
  


  @ViewChild('slidesSelling', { static: true }) sliderSelling: IonSlides;

  segmentSelling = 0;

  @ViewChild('slidesBuying', { static: true }) sliderBuying: IonSlides;

  segmentBuying = 0;

  segment = 0;

  fromRate : boolean = false;

  itemList: Item[] = [];
  itemListArchive: Item[] = [];
  buyItemList: Item[] = [];

  buyItemListArchive: Item[] = [];

  login_user_id: string = "";

  constructor(
    private router: Router,
    private toastController: ToastController,
    private postPvdr: PostProvider,
    private storage: Storage,
    private alertCtrl: AlertController,
    private route: ActivatedRoute,
    public navCtrl: NavController,
  ) {

    this.route.queryParams.subscribe(params => {
      this.fromRate = params["fromRate"];
     // console.log("params in productview:"+JSON.stringify(params));
     
    });
   }

  async segmentChanged() {
    await this.slider.slideTo(this.segment);
  }

  async slideChanged() {
    this.segment = await this.slider.getActiveIndex();
  }

  ngOnInit() {
  //  this.plotData();
  }
  ionViewWillEnter(){
    this.plotData();
  }

  plotData(){

    if(this.fromRate){
      this.segmentSelling = 1;
    }
    this.storage.get('greenthumb_user_id').then((user_id) => {
      this.login_user_id = user_id;
      let body2 = {
        action : 'getSellerPost',
        user_id : user_id,
      };
      console.log(JSON.stringify(body2));
      this.postPvdr.postData(body2, 'post_item.php').subscribe(async data => {
        
        if(data.success){
  
          const items: Item[] = [];
  
          //var pictureProfile: string = '';
  
          var x = 0;
  
          for(const key in data.result){
            items.push(new Item(
              data.result[key].item_id,
              data.result[key].user_id,
              data.result[key].username,
              (data.result[key].profile_photo == '') ? '' :
              this.postPvdr.myServer()+"/greenthumb/images/"+data.result[key].profile_photo,
              data.result[key].price,
              data.result[key].title,
              data.result[key].stocks_id,
              data.result[key].others_stock,
              parseInt(data.result[key].quantity),
              data.result[key].category,
              data.result[key].location,
              data.result[key].saveStatus,
              data.result[key].status,
              this.postPvdr.myServer()+"/greenthumb/images/posted_items/images/"+data.result[key].item_photo,
              data.result[key].user_rating,
              data.result[key].date,
              data.result[key].time
              )
            );
  
          }
          this.itemList = items;
        }
      });


      let body2Archive = {
        action : 'getSellerPostArchive',
        user_id : user_id,
      };
      //console.log(JSON.stringify(body2));
      this.postPvdr.postData(body2Archive, 'post_item.php').subscribe(async data => {
        
        if(data.success){
  
          const itemsArchive: Item[] = [];
  
          //var pictureProfile: string = '';
  
          var x = 0;
  
          for(const key in data.result){
            itemsArchive.push(new Item(
              data.result[key].item_id,
              data.result[key].user_id,
              data.result[key].username,
              (data.result[key].profile_photo == '') ? '' :
              this.postPvdr.myServer()+"/greenthumb/images/"+data.result[key].profile_photo,
              data.result[key].price,
              data.result[key].title,
              data.result[key].stocks_id,
              data.result[key].others_stock,
              parseInt(data.result[key].quantity),
              data.result[key].category,
              data.result[key].location,
              data.result[key].saveStatus,
              data.result[key].status,
              this.postPvdr.myServer()+"/greenthumb/images/posted_items/images/"+data.result[key].item_photo,
              data.result[key].user_rating,
              data.result[key].date,
              data.result[key].time
              )
            );
  
          }
          this.itemListArchive = itemsArchive;
        }
      });

      let body21 = {
        action : 'getBuying',
        user_id : user_id,
      };
      console.log(JSON.stringify(body21));
      this.postPvdr.postData(body21, 'post_item.php').subscribe(async data => {
        
        if(data.success){
  
          const buyitems: Item[] = [];
  
          //var pictureProfile: string = '';
  
          var x = 0;
  
          for(const key in data.result){
            buyitems.push(new Item(
              data.result[key].item_id,
              data.result[key].user_id,
              data.result[key].username,
              (data.result[key].profile_photo == '') ? '' :
              this.postPvdr.myServer()+"/greenthumb/images/"+data.result[key].profile_photo,
              data.result[key].price,
              data.result[key].title,
              data.result[key].stocks_id,
              data.result[key].others_stock,
              parseInt(data.result[key].quantity),
              data.result[key].category,
              data.result[key].location,
              data.result[key].saveStatus,
              data.result[key].status,
              this.postPvdr.myServer()+"/greenthumb/images/posted_items/images/"+data.result[key].item_photo,
              data.result[key].user_rating,
              data.result[key].date,
              data.result[key].time
              )
            );
  
          }
          this.buyItemList = buyitems;
        }
      });

      let body21Archive = {
        action : 'getBuyingArchive',
        user_id : user_id,
      };
      //console.log(JSON.stringify(body21));
      this.postPvdr.postData(body21Archive, 'post_item.php').subscribe(async data => {
        
        if(data.success){
  
          const buyitemsArchive: Item[] = [];
  
          //var pictureProfile: string = '';
  
          var x = 0;
  
          for(const key in data.result){
            buyitemsArchive.push(new Item(
              data.result[key].item_id,
              data.result[key].user_id,
              data.result[key].username,
              (data.result[key].profile_photo == '') ? '' :
              this.postPvdr.myServer()+"/greenthumb/images/"+data.result[key].profile_photo,
              data.result[key].price,
              data.result[key].title,
              data.result[key].stocks_id,
              data.result[key].others_stock,
              parseInt(data.result[key].quantity),
              data.result[key].category,
              data.result[key].location,
              data.result[key].saveStatus,
              data.result[key].status,
              this.postPvdr.myServer()+"/greenthumb/images/posted_items/images/"+data.result[key].item_photo,
              data.result[key].user_rating,
              data.result[key].date,
              data.result[key].time
              )
            );
  
          }
          this.buyItemListArchive = buyitemsArchive;
        }
      });

      
    });
  }

  async segmentSellingChanged() {
    await this.sliderSelling.slideTo(this.segmentSelling);
  }

  async segmentBuyingChanged() {
    await this.sliderBuying.slideTo(this.segmentBuying);
  }

  async slideSellingChanged() {
    this.segmentSelling = await this.sliderSelling.getActiveIndex();
  }

  async slideBuyingChanged() {
    this.segmentBuying = await this.sliderBuying.getActiveIndex();
  }

  deleteItem(item_id){

    
    this.alertCtrl.create({
      header: "Are you sure to delete this item?",
      message: "",
      buttons: [
        {
          text: "No",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Yes",
          handler: () => {
            console.log("Agree clicked");
            this.confirmDelete(item_id);
          }
        }
      ]
    }).then(res => {
      res.present();
    });
    

  }

  confirmDelete(item_id){
    let body = {
      action : 'deleteItem',
      item_id : item_id
    };

    this.postPvdr.postData(body, 'save_item.php').subscribe(data => {

      if(data.success){
        this.toastSuccessDelete();
        this.plotData();
      }
      
    });
  }

  async toastSuccessDelete() {
    const toast = await this.toastController.create({
      message: 'Item Successfully Deleted',
      duration: 3000
    });
    toast.present();
  }

  markSold(item_id, item_status){
    //console.log("gi saved lamang pud");
    let body = {
      action : 'markSoldItem',
      item_id : item_id,
      user_id : this.login_user_id,
      item_status : item_status
    };

    this.postPvdr.postData(body, 'save_item.php').subscribe(data => {

      if(data.success){
        // if(data.saveStatus == '1'){
        //   console.log('1 ko');
        //   var element = document.getElementById("class"+item_id);
        //   element.classList.remove("myHeartUnSave");
        //   element.classList.add("myHeartSave");
        //   console.log("classList:"+element.classList);
        // }
        // else{
        //   console.log("0 ko");
        //   var element = document.getElementById("class"+item_id);
        //   element.classList.remove("myHeartSave");
        //   element.classList.add("myHeartUnSave");
        // }
      }
      
    });
    
    
  }

  goBack(){
    window.history.back();
  }
  goCategories(){
    this.router.navigate(['categories']);
  }
  goHelp(){
    this.router.navigate(['tabs/help']);
  }
  goSellingProductview(){
    this.router.navigate(['sellingproductview']);
  }
  goProductview(item_id){
    // this.router.navigate(['productview']);
     let navigationExtras: any = {
         queryParams: { item_id}
     };
     this.navCtrl.navigateForward(['productview'], navigationExtras);
   }
  goMessage(item_user_id, item_id){
    this.navCtrl.navigateRoot(['/message/'+item_user_id+"-separator-"+item_id]);
  }
  goEdit(item_id){
    let navigationExtras: any = {
      queryParams: { item_id}
    };
    this.navCtrl.navigateRoot(['edititem'], navigationExtras);
   }

}
