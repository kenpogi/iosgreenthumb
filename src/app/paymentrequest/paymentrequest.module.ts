import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PaymentrequestPageRoutingModule } from './paymentrequest-routing.module';

import { PaymentrequestPage } from './paymentrequest.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PaymentrequestPageRoutingModule
  ],
  declarations: [PaymentrequestPage]
})
export class PaymentrequestPageModule {}
