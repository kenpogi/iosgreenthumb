import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { paychartModel } from '../shared/model/paychart.model';
import { PostProvider } from '../../providers/credential-provider';
import { ModalController, ToastController } from '@ionic/angular';
import { PayreqconfirmationPage } from '../payreqconfirmation/payreqconfirmation.page';

@Component({
  selector: 'app-paymentrequest',
  templateUrl: './paymentrequest.page.html',
  styleUrls: ['./paymentrequest.page.scss'],
})
export class PaymentrequestPage implements OnInit {

  gold_bar : number;
  user_coins : number;
  cashout_status: number;

  paychartList : paychartModel[] = [];

  payoneerId: string = "";

  constructor(
    private storage: Storage,
    private postPvdr: PostProvider,
    public modalController: ModalController,
    private toastController: ToastController,
    ) { }

  goBack(){
    window.history.back();
  }

  ngOnInit() {
    this.plotData();
  }


  async paymentRequest(paychart){


    if(this.gold_bar<paychart.gold_bars){
      this.presentToast();
    }
    else{
      const modal = await this.modalController.create({
        component: PayreqconfirmationPage,
        cssClass: 'sendgiftmodal',
        componentProps: { paychart: paychart}
      });
  
        modal.onDidDismiss()
        .then((data) => {
          //const user = data['data']; // Here's your selected user!
          //this.showCoinsandGold();
        });
  
      return await modal.present();
    }

    

  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: 'You have insufficient balance of gold bars. ',
      duration: 3000
    });
    toast.present();
  }

  plotData(){
    this.storage.get("user_id").then((user_id) => {
      let body = {
        action : 'getuserdata',
        user_id : user_id
      };
      this.postPvdr.postData(body, 'credentials-api.php').subscribe(async data => {
        if(data.success){

          this.gold_bar = data.result.gold_bar;
          this.user_coins = data.result.user_coins;
          this.cashout_status = data.result.cashout_status;

        }
      });


      let body3 = {
        action : 'getPayoneerId',
        user_id : user_id
      };
      console.log("what?:"+JSON.stringify(body3));
      this.postPvdr.postData(body3, 'payment.php').subscribe(data => {
        
        if(data.success){

          for(const key in data.result){
            
              this.payoneerId = data.result[key].payment_id;
            
          }

          console.log("mypayoneerId:"+this.payoneerId);
        }
      });


    });

    let body2 = {
      action : 'showPaychart'
    };
    this.postPvdr.postData(body2, 'payment.php').subscribe(data => {
      
      if(data.success){
        const paychart: paychartModel[] = [];
        for(const key in data.result){

          paychart.push(new paychartModel(
            data.result[key].id,
            data.result[key].gold_bars,
            data.result[key].usd_amount
          ));
        }

        this.paychartList = paychart;
      }
    });
  }

}
