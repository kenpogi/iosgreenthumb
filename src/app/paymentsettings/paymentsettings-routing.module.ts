import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PaymentsettingsPage } from './paymentsettings.page';

const routes: Routes = [
  {
    path: '',
    component: PaymentsettingsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PaymentsettingsPageRoutingModule {}
