import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PaymentsettingsPageRoutingModule } from './paymentsettings-routing.module';

import { PaymentsettingsPage } from './paymentsettings.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PaymentsettingsPageRoutingModule
  ],
  declarations: [PaymentsettingsPage]
})
export class PaymentsettingsPageModule {}
