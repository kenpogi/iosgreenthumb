import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-paymentsettings',
  templateUrl: './paymentsettings.page.html',
  styleUrls: ['./paymentsettings.page.scss'],
})
export class PaymentsettingsPage implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }
  goBack(){
    // this.router.navigate(['myaccount']);
    window.history.back();
  }

}
