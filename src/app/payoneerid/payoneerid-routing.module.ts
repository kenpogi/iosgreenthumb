import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PayoneeridPage } from './payoneerid.page';

const routes: Routes = [
  {
    path: '',
    component: PayoneeridPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PayoneeridPageRoutingModule {}
