import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PayoneeridPageRoutingModule } from './payoneerid-routing.module';

import { PayoneeridPage } from './payoneerid.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PayoneeridPageRoutingModule
  ],
  declarations: [PayoneeridPage]
})
export class PayoneeridPageModule {}
