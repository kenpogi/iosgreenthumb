import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PayoneeridPage } from './payoneerid.page';

describe('PayoneeridPage', () => {
  let component: PayoneeridPage;
  let fixture: ComponentFixture<PayoneeridPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayoneeridPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PayoneeridPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
