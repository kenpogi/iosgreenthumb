import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PayreqconfirmationPage } from './payreqconfirmation.page';

const routes: Routes = [
  {
    path: '',
    component: PayreqconfirmationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PayreqconfirmationPageRoutingModule {}
