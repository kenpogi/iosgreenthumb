import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PayreqconfirmationPageRoutingModule } from './payreqconfirmation-routing.module';

import { PayreqconfirmationPage } from './payreqconfirmation.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PayreqconfirmationPageRoutingModule
  ],
  declarations: [PayreqconfirmationPage]
})
export class PayreqconfirmationPageModule {}
