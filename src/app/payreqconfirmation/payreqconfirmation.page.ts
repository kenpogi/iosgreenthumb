import { Component, OnInit } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';
import { NavParams } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { paychartModel } from '../shared/model/paychart.model';
import { PostProvider } from '../../providers/credential-provider';

@Component({
  selector: 'app-payreqconfirmation',
  templateUrl: './payreqconfirmation.page.html',
  styleUrls: ['./payreqconfirmation.page.scss'],
})
export class PayreqconfirmationPage implements OnInit {

  paychart: paychartModel;

  constructor(
    navParams: NavParams, 
    private storage: Storage, 
    public modalCtrl: ModalController,
    private toastController: ToastController,
    private postPvdr: PostProvider
  ) {

    this.paychart = navParams.get('paychart');

   }

  ngOnInit() {
  }

  closeModal()
    {
      this.modalCtrl.dismiss();
    }

  confirmPaymentRequest(){

    // console.log("paychart:"+this.paychart.id);
    // console.log("paychart:"+this.paychart.gold_bars);
    // console.log("paychart:"+this.paychart.usd_amount);

    this.storage.get("user_id").then((user_id) => {

      let body2 = {
        action : 'requestPayment',
        user_id : user_id,
        gold_bars : this.paychart.gold_bars,
        usd_amount : this.paychart.usd_amount,
        
      };
      console.log("body:"+JSON.stringify(body2));
      this.postPvdr.postData(body2, 'payment.php').subscribe(data => {
        
        if(data.success){
          this.presentToast();
          this.modalCtrl.dismiss();
        }
      });

    });
  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Cashout Requested Successfully.',
      duration: 3000
    });
    toast.present();
  }

}
