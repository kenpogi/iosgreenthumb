import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-privacypolicy',
  templateUrl: './privacypolicy.page.html',
  styleUrls: ['./privacypolicy.page.scss'],
})
export class PrivacypolicyPage implements OnInit {

  constructor(private modalController: ModalController,) { }

  ngOnInit() {
  }

  goBack(){
    this.modalController.dismiss();
  }

}
