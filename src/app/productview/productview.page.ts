import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ReportPage } from '../report/report.page';
import { ModalController, NavController,ToastController,AlertController} from '@ionic/angular';
import { Item, ItemPicture } from '../shared/model/item.model';
import { Storage } from '@ionic/storage';

import { PostProvider } from '../../providers/credential-provider';
import { PurchaseproductPage } from '../purchaseproduct/purchaseproduct.page';

@Component({
  selector: 'app-productview',
  templateUrl: './productview.page.html',
  styleUrls: ['./productview.page.scss'],
})
export class ProductviewPage implements OnInit {



  item_id: string = "";
  //itemList: Item[] = [];

  login_user_id : string = "";

  statusSave: boolean = false;

  ImageArray: ItemPicture[] = [];

  item_name : string = "";
  item_category : string = "";
  item_price : string = "";
  item_seller : string = "";
  item_user_id : string = "";
  item_saveStatus : string = "";
  item_description : string = "";
  item_location : string = "";
  item_quantity : number ;
  item_stock_id : string ;
  item_other_stock : string;

  user_rating : string;

  pending_purchase = false;

  ownProfile : boolean = false;

  picture: any;
  complete_pic: any;

  constructor(
    private router: Router,
    private postPvdr: PostProvider,
    private toastController: ToastController,
    public modalController: ModalController,
    private alertCtrl: AlertController,
    public navCtrl: NavController,
    private route: ActivatedRoute,
    private storage: Storage
  ) {

    // this.ImageArray = [
    //   {'image':'assets/greenthumb-images/bunny2.png',
    //     'name': 'Lorem ipsum dolor 1',
    //     'text': 'the'},
    //   {'image':'assets/greenthumb-images/bunny2.png',
    //   'name': 'Lorem ipsum dolor 2',
    //   'text': 'the'},
    //   {'image':'assets/greenthumb-images/bunny2.png',
    //   'name': 'Lorem ipsum dolor 3',
    //   'text': 'the'}
    //]


    this.route.queryParams.subscribe(params => {
      this.item_id = params["item_id"];
      console.log("params in productview:"+JSON.stringify(params));
     
    });


   }

  // goReport(){
  //    this.router.navigate((['report']));
  // }
  goHelp(){
    this.router.navigate(['tabs/help']);
  }
  async goReport() {
        const modal = await this.modalController.create({
          component: ReportPage,
          cssClass: 'liveprofilemodalstyle',
          componentProps: { 
            reportedId : this.item_id,
            reportedName: this.item_name
          }
        });
    
    
        modal.onDidDismiss()
        .then((data) => {
          //const user = data['data']; // Here's your selected user!
          console.log("dismiss of report item modal")
          //this.plotFollowStatus();
        });
    
    
        return await modal.present();
  }

  goBack(){
    window.history.back();
  }
  goMessage(){
    this.navCtrl.navigateRoot(['/message/'+this.item_user_id+"-separator-"+this.item_id+"-separator-"+'0']);
 }
 goSellerprof(){
  // this.router.navigate((['viewitemsellerprofile']));
  let navigationExtras: any = {
   queryParams: { user_profile_id : this.item_user_id}
};
   this.navCtrl.navigateForward(['viewitemsellerprofile'], navigationExtras);
 }

  ngOnInit() {
    
  }

  ionViewWillEnter(){
    this.plotData();
  }

  plotData(){
    
    this.storage.get('greenthumb_user_id').then((user_id) => {
      this.login_user_id = user_id;

      

      let body = {
        action : 'viewPostItem',
        item_id : this.item_id,
        user_id : user_id
      };
      console.log(JSON.stringify(body));
      this.postPvdr.postData(body, 'post_item.php').subscribe(async data => {
        
        if(data.success){

          const items: Item[] = [];

          //var pictureProfile: string = '';

          for(const key in data.result){
            // items.push(new Item(
            //   data.result[key].item_id,
            //   data.result[key].user_id,
            //   data.result[key].username,
            //   data.result[key].price,
            //   data.result[key].title,
            //   data.result[key].category,
            //   data.result[key].location,
            //   data.result[key].saveStatus,
            //   data.result[key].date,
            //   data.result[key].time
            //   )
            // );

            this.item_name = data.result[key].title;
            this.item_category = data.result[key].category;
            this.item_seller = data.result[key].username;

            this.item_user_id = data.result[key].user_id;
            this.item_price = data.result[key].price;
            this.item_location = data.result[key].location + ' '+ data.result[key].city;
            this.item_saveStatus = data.result[key].saveStatus;
            this.item_description = data.result[key].description;

            this.item_quantity = data.result[key].quantity;
            this.item_stock_id = data.result[key].stocks_id;
            this.item_other_stock = data.result[key].others_stock;

            this.picture = data.result[key].profile_photo;
            this.complete_pic = this.postPvdr.myServer()+"/greenthumb/images/"+this.picture;

            this.user_rating = data.result[key].user_rating;
          }

          this.ownProfile = this.login_user_id == this.item_user_id ? true : false;
          
        }
      });


      let body3 = {
        action : 'SaveStatus',
        item_id : this.item_id,
        user_id : user_id

      };
      console.log("body3:"+JSON.stringify(body3));
      this.postPvdr.postData(body3, 'save_item.php').subscribe(async data => {
        
        if(data.success){
          if(data.result=="1"){
            this.statusSave = true;
          }
        }
        console.log("statusSave:"+this.statusSave);
        console.log("wr:"+data.result);
      });


      let body4 = {
        action : 'getItemPhotos',
        item_id : this.item_id,
       // user_id : user_id
      };
      console.log(JSON.stringify(body4));
      this.postPvdr.postData(body4, 'post_item.php').subscribe(async data => {
        
        if(data.success){

          const itemsImage: ItemPicture[] = [];

          //var pictureProfile: string = '';

          for(const key in data.result){
            this.ImageArray.push(new ItemPicture(
              this.postPvdr.myServer()+"/greenthumb/images/posted_items/images/"+data.result[key].item_photo,
              )
            );

          
          }
        }
      });
      

      let bodyqwer = {
        action : 'checkPurchasePending',
        item_id : this.item_id,
        user_id : this.login_user_id,
        item_user_id : this.item_user_id,
      };
  
      this.postPvdr.postData(bodyqwer, 'save_item.php').subscribe(async data => {
  
        if(data.success){
  
          if(data.pending_purchase){
            this.pending_purchase = true;
          }
        }

      });


    });
  }

  slideOptsOne = {
    initialSlide: 0,
    slidesPerView: 1,
    autoplay:true
   };


  cancelPurchase(){
    this.alertCtrl.create({
      header: "Greenthumb Trade",
      message: "Are you sure to cancel the purchase of this item?",
      buttons: [
        {
          text: "No",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Yes",
          handler: () => {
            console.log("Agree clicked");
            this.agreeCancel();
          }
        }
      ]
    }).then(res => {
      res.present();
    });
  }

  agreeCancel(){

    let body = {
      action : 'cancelPurchase',
      item_id : this.item_id,
      user_id : this.login_user_id,
      item_user_id : this.item_user_id,
    };

    this.postPvdr.postData(body, 'save_item.php').subscribe(data => {

      if(data.success){
        this.toastSuccessDelete('Purchase Successfully Canceled');
        this.pending_purchase = false;
      }
      
    });

  }

  saveItem(){

     
    let body = {
      action : 'saveItem',
      item_id : this.item_id,
      user_id : this.login_user_id,
    };

    this.postPvdr.postData(body, 'save_item.php').subscribe(data => {

      if(data.success){
        this.statusSave = !this.statusSave; 
      }
      
    });

  }

  async purchaseProduct() {


    // let body = {
    //   action : 'checkPurchasePending',
    //   item_id : this.item_id,
    //   user_id : this.login_user_id,
    //   item_user_id : this.item_user_id,
    // };

    // this.postPvdr.postData(body, 'save_item.php').subscribe(async data => {

    //   if(data.success){

    //     if(data.pending_purchase){
    //       this.presentToast();
    //       this.navCtrl.navigateRoot(['/message/'+this.item_user_id+"-separator-"+
    //                                                         this.item_id]);
    //     }
    //     else{
          const modal = await this.modalController.create({
                  component: PurchaseproductPage,
                  cssClass: 'purchaseProduct',
                  componentProps: { item_id: this.item_id, item_user_id : this.item_user_id,
                     login_user_id: this.login_user_id, item_name : this.item_name}
                });
        
                modal.onDidDismiss()
                .then((data) => {
                  // this.category_id = data['data'].id; // Here's your selected category!
                  // this.category_name = data['data'].value;
                  // console.log("category_name:"+this.category_name);
                  // console.log("data:"+data);
                  // this.navCtrl.navigateRoot(['/message/'+this.login_user_id+"-separator-"+
                  //                                                   this.item_id]);
                });
        
             
                return await modal.present();
    //     }

        
    //   }
      
    // });

        
  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: 'You have a pending purchase with these item.',
      duration: 3000
    });
    toast.present();
  }

  goOffers(){
    this.router.navigate(['tabs/offers']);
  }

  deleteItem(item_id){

    
    this.alertCtrl.create({
      header: "Are you sure to delete this item?",
      message: "",
      buttons: [
        {
          text: "No",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Yes",
          handler: () => {
            console.log("Agree clicked");
            this.confirmDelete(item_id);
          }
        }
      ]
    }).then(res => {
      res.present();
    });
    

  }

  confirmDelete(item_id){
    let body = {
      action : 'deleteItem',
      item_id : item_id
    };

    this.postPvdr.postData(body, 'save_item.php').subscribe(data => {

      if(data.success){
        this.toastSuccessDelete('Item Successfully Deleted');
        this.navCtrl.navigateRoot(['tabs/tab1']);
      }
      
    });
  }

  async toastSuccessDelete(myMess) {
    const toast = await this.toastController.create({
      message: myMess,
      duration: 3000
    });
    toast.present();
  }

  goEdit(item_id){
    let navigationExtras: any = {
      queryParams: { item_id}
    };
    this.navCtrl.navigateRoot(['edititem'], navigationExtras);
   }



}
