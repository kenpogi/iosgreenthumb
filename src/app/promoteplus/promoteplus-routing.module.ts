import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PromoteplusPage } from './promoteplus.page';

const routes: Routes = [
  {
    path: '',
    component: PromoteplusPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PromoteplusPageRoutingModule {}
