import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PromoteplusPageRoutingModule } from './promoteplus-routing.module';

import { PromoteplusPage } from './promoteplus.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PromoteplusPageRoutingModule
  ],
  declarations: [PromoteplusPage]
})
export class PromoteplusPageModule {}
