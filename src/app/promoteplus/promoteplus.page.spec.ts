import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PromoteplusPage } from './promoteplus.page';

describe('PromoteplusPage', () => {
  let component: PromoteplusPage;
  let fixture: ComponentFixture<PromoteplusPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PromoteplusPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PromoteplusPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
