import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PurchasecoinsPage } from './purchasecoins.page';

const routes: Routes = [
  {
    path: '',
    component: PurchasecoinsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PurchasecoinsPageRoutingModule {}
