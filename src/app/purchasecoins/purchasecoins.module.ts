import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PurchasecoinsPageRoutingModule } from './purchasecoins-routing.module';

import { PurchasecoinsPage } from './purchasecoins.page';




@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PurchasecoinsPageRoutingModule
  ],
  declarations: [PurchasecoinsPage]
})
export class PurchasecoinsPageModule {}
