import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PurchasecoinsPage } from './purchasecoins.page';

describe('PurchasecoinsPage', () => {
  let component: PurchasecoinsPage;
  let fixture: ComponentFixture<PurchasecoinsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchasecoinsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PurchasecoinsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
