import { Component, OnInit } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';
import { NavParams } from '@ionic/angular';
import { coinModel } from '../shared/model/coin.model';
import { Storage } from '@ionic/storage';
import { PostProvider } from '../../providers/credential-provider';

@Component({
  selector: 'app-purchasecoins',
  templateUrl: './purchasecoins.page.html',
  styleUrls: ['./purchasecoins.page.scss'],
})
export class PurchasecoinsPage implements OnInit {

  coin : coinModel;

  constructor(
    navParams: NavParams, 
    private storage: Storage, 
    public modalCtrl: ModalController,
    private toastController: ToastController,
    private postPvdr: PostProvider
    ) {

    this.coin = navParams.get('coin');

   }

  ngOnInit() {
  }

  closeModal()
    {
      this.modalCtrl.dismiss();
    }

    async presentToast() {
      const toast = await this.toastController.create({
        message: 'Purchased the coins successfully.',
        duration: 3000
      });
      toast.present();
    }

    purchaseCoins(){

      console.log("mipalit lamang pud");
      console.log("coin Id:"+this.coin.id);
      console.log("coin coins:"+this.coin.coins);
      console.log("coin amount:"+this.coin.amount);
      this.modalCtrl.dismiss();

      this.storage.get("user_id").then((user_id) => {

        let body2 = {
          action : 'purchaseCoins',
          user_id : user_id,
          coin_id : this.coin.id,
          coins : this.coin.coins,
          amount : this.coin.amount
          
        };
        this.postPvdr.postData(body2, 'brixy-store.php').subscribe(data => {
          
          if(data.success){
            this.presentToast();
             
          }
        });

      });



    }

}
