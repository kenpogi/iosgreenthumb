import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PurchaseproductPage } from './purchaseproduct.page';

const routes: Routes = [
  {
    path: '',
    component: PurchaseproductPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PurchaseproductPageRoutingModule {}
