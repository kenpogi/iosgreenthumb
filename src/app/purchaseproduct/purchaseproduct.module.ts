import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PurchaseproductPageRoutingModule } from './purchaseproduct-routing.module';

import { PurchaseproductPage } from './purchaseproduct.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PurchaseproductPageRoutingModule
  ],
  declarations: [PurchaseproductPage]
})
export class PurchaseproductPageModule {}
