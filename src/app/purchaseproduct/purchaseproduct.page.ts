import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams, ToastController, NavController } from '@ionic/angular';
import { PostProvider } from '../../providers/credential-provider';

@Component({
  selector: 'app-purchaseproduct',
  templateUrl: './purchaseproduct.page.html',
  styleUrls: ['./purchaseproduct.page.scss'],
})
export class PurchaseproductPage implements OnInit {

  purchase_quantity : number;
  item_id : string = "";
  item_user_id : string = "";
  login_user_id : string = "";
  item_name : string = "";

  constructor(
    navParams: NavParams,
    private postPvdr: PostProvider,
    public modalCtrl: ModalController,
    private toastController: ToastController,
    public navCtrl: NavController
  ) {

     this.item_id = navParams.get('item_id');
     this.item_user_id = navParams.get('item_user_id');
     this.login_user_id = navParams.get('login_user_id');
     this.item_name = navParams.get('item_name');
   }

  ngOnInit() {
  }

  closeModal()
   {
     this.modalCtrl.dismiss(this.purchase_quantity);
   }

   purchaseProduct(){

    let body = {
      action : 'purchaseItem',
      item_id : this.item_id,
      user_id : this.login_user_id,
      item_user_id : this.item_user_id,
      purchase_quantity : this.purchase_quantity,
      item_name : this.item_name
    };

    this.postPvdr.postData(body, 'save_item.php').subscribe(data => {

      if(data.success){

        if(data.invalid_quantity){
          this.presentToast();
        }
        else{
          this.navCtrl.navigateRoot(['/message/'+this.item_user_id+"-separator-"+
                                                            this.item_id+"-separator-"+'1']);
          this.modalCtrl.dismiss(this.purchase_quantity);
        }
        

        
      }
      
    });
   }

  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Quantity must be lesser than or equal to the item stocks.',
      duration: 3000
    });
    toast.present();
  }

}
