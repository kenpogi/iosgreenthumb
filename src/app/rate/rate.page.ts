import { Component, OnInit, Input } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';
import { PostProvider } from '../../providers/credential-provider';

@Component({
  selector: 'app-rate',
  templateUrl: './rate.page.html',
  styleUrls: ['./rate.page.scss'],
})
export class RatePage implements OnInit {

  myRate : string = "" ;
  @Input() login_user_id: any;
  @Input() rate_user_id: any;
  @Input() notif_id: any;

  constructor(
    public modalController: ModalController,
    private postPvdr: PostProvider,
    private toastController: ToastController,) { }

  ngOnInit() {
  }

  logRatingChange(event){
    this.myRate = event;
  }

  submitRate(){
    console.log("rate_user_id:"+this.rate_user_id);
    console.log("login_user_id:"+this.login_user_id);
    console.log("notif_id:"+this.notif_id);
    if(this.myRate==''){
      this.presentToast('Please select a rating');
    }
    else{
      let body = {
        action : 'rateUser',
        rate_user_id : this.rate_user_id,
        login_user_id : this.login_user_id,
        myRate : this.myRate,
        notif_id : this.notif_id
      };
  
      this.postPvdr.postData(body, 'rating.php').subscribe(data => {
  
        if(data.success){
          this.modalController.dismiss(true);

          this.presentToast('Thank you for your rating');
          
        }
        
      });
    }
    
  }

  close(){
    this.modalController.dismiss(false);
  }

  async presentToast(message) {
    const toast = await this.toastController.create({
      message,
      duration: 3000
    });
    toast.present();
  }

}
