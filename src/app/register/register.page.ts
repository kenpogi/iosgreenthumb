import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { ToastController } from '@ionic/angular';
import { PostProvider } from '../../providers/credential-provider';
import { ModalController } from '@ionic/angular';
import { UserpolicyPage } from '../userpolicy/userpolicy.page';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  login_user_id : string;
  email : string;
  fname : string;
  lname : string;
  mobile_num : string;

  bdate : string;
  city : string;
  country : string;

  inpt_fname : string;
  inpt_lname : string;
  inpt_nickname : string;
  inpt_birthdate : string;
  inpt_gender : string;
  inpt_city : string;
  inpt_country : string;
  inpt_email : string;
  inpt_mobile_num : string;


  constructor(
    private storage: Storage,
    private toastController: ToastController,
    private postPvdr: PostProvider,
    private modalController: ModalController,
    ) { }
  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Congratulations. Your account has been updated.',
      duration: 3000
    });
    toast.present();
  }
  async presentToastLack() {
    const toast = await this.toastController.create({
      message: 'Please fill in all fields.',
      duration: 3000
    });
    toast.present();
  }

  ngOnInit() {
    this.pre_register();
  }

  pre_register(){


    this.storage.get("greenthumb_user_id").then((user_id) => {

      this.login_user_id = user_id;
      let body = {
        action : 'getUserData',
        user_id : user_id
      };
      this.postPvdr.postData(body, 'user.php').subscribe(async data => {
        if(data.success){
          this.email = data.result.email;
          this.fname = data.result.fname;
          this.lname = data.result.lname;

          this.bdate = data.result.bdate;
          this.city = data.result.city;
          this.country = data.result.country;

          this.mobile_num = data.result.mobile_num;
        } 
      });
    });

    // this.storage.get('login_used').then((val) => {
    //   this.login_used = val;
    // });
    // this.storage.get('email').then((val) => {
    //   this.email = val;
    // });
    // this.storage.get('fname').then((val) => {
    //   this.fname = val;
    // });
    // this.storage.get('mobile_num').then((val) => {
    //   this.mobile_num = val;
    // });
  }


  register(){

    let body1 = {
      action : "register",
      fname : this.inpt_fname,
      lname : this.inpt_lname,
      // nickname : this.inpt_nickname,
      birthdate : this.inpt_birthdate.substring(0, 10),
      // gender : this.inpt_gender,
      city : this.inpt_city,
      country : this.inpt_country,
      email : this.inpt_email,
      mobile_num :this.inpt_mobile_num,
      user_id : this.login_user_id
    };
    console.log("bodybody1:"+JSON.stringify(body1));
    if(this.inpt_fname !== undefined && this.inpt_lname !== undefined && this.inpt_birthdate !== undefined && this.inpt_city !== undefined && this.inpt_country !== undefined && this.inpt_email !== undefined && this.inpt_mobile_num !== undefined){
     
      let body = {
        action : "register",
        fname : this.inpt_fname,
        lname : this.inpt_lname,
        // nickname : this.inpt_nickname,
        birthdate : this.inpt_birthdate.substring(0, 10),
        // gender : this.inpt_gender,
        city : this.inpt_city,
        country : this.inpt_country,
        email : this.inpt_email,
        mobile_num :this.inpt_mobile_num,
        user_id : this.login_user_id
      };
      console.log("bodybody:"+JSON.stringify(body));
      this.postPvdr.postData(body, 'credentials-api.php').subscribe(data => {
        if(data.success){
          this.presentToast();
          this.modalController.dismiss();
        }
      });
    } else{
      this.presentToastLack();
    }
  }

  async goToUserPolicy(){
    const modal = await this.modalController.create({
            component: UserpolicyPage,
            cssClass: ''
          });
          return await modal.present();
  }
  cancel(){


    this.modalController.dismiss();
  }
}
