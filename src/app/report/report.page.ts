import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { reportReason } from '../shared/model/report.model';
import { PostProvider } from '../../providers/credential-provider';
import { Storage } from '@ionic/storage';
import { ModalController, AlertController} from '@ionic/angular';

@Component({
  selector: 'app-report',
  templateUrl: './report.page.html',
  styleUrls: ['./report.page.scss'],
})
export class ReportPage implements OnInit {

  @Input() reportedName: any;
  @Input() reportedId: any;

  theReason: string = '';
  otherReason: string = '';

  others: boolean = false;

  reasonList : reportReason[] = [];

  constructor(
    private router: Router,
    private postPvdr: PostProvider,
    private storage: Storage,
    public modalController: ModalController,
    public alertCtrl: AlertController, 
  ) { }

  goBack(){
    // this.router.navigate(['help']);
    //window.history.back();
    this.modalController.dismiss();
  }

  ngOnInit() {
    this.plotData();
    this.theReason = "Showing Inappropriate Behavior";
  }


  plotData(){

    let body = {
      action : 'reportReason',
    };
    this.postPvdr.postData(body, 'report.php').subscribe(async data => {
      
      if(data.success){
        for(const key in data.result){

          this.reasonList.push(new reportReason(
            data.result[key].id,
            data.result[key].reason

          ));
        }
      }
    });
  }

  othersClick(){
    this.others = !this.others;
    console.log("this.others:"+this.others);
    if(!this.others){
      this.otherReason = "";
    }
  }

  reportThisUser(){

    console.log("thereason:"+this.theReason);
    this.storage.get('greenthumb_user_id').then((user_id) => {

      if(this.others && this.otherReason.length>0){
        this.theReason = this.theReason +"     -     "+ this.otherReason
      }

      let body2 = {
        action : 'reportThisItem',
        reported_item : this.reportedId,
        reported_by : user_id,
        report_reason : this.theReason
  
      };
      console.log("report:"+JSON.stringify(body2));
      this.postPvdr.postData(body2, 'report.php').subscribe(async data => {
        
        if(data.success){
          this.goBack();

          this.alertCtrl.create({
            header: 'Report Successfully Sent!',
            message: '<b style="text-align: justify;font-weight:lighter">You have successfully reported <b style="color: #1dc1e6;font-weight: lighter;">'+ this.reportedName +'</b>,'+ 
            'it will take some time for our administrators to go through numerous user reports.'+
            ' Please bear with us, rest assured we will go through each one carefully and take the'+
            ' necessary actions to make your user experience in Greenthumb a pleasant one. We thank you'+
            ' for your patience. Please email screenshots of your report to Greenthumb@gmail.com</b>',
            cssClass: 'foo',
            
            buttons: [{
                text: 'CLOSE',
                role: 'cancel',
                handler: () => {
                    console.log('Done reporting!');
                }
            }]
          }).then(res => {
            res.present();
          });
        }
      });

    });
  }

}
