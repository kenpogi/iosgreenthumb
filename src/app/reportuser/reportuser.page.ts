import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { PostProvider } from '../../providers/credential-provider';
import { reportReason } from '../shared/model/report.model';
import { ModalController, AlertController} from '@ionic/angular';

@Component({
  selector: 'app-reportuser',
  templateUrl: './reportuser.page.html',
  styleUrls: ['./reportuser.page.scss'],
})
export class ReportuserPage implements OnInit {

  @Input() reportedName: any;
  @Input() reportedId: any;

  theReason: string = '';
  otherReason: string = '';

  others: boolean = false;

  reasonList : reportReason[] = [];

  constructor(
    private router: Router,
    private storage: Storage,
    public modalController: ModalController,
    public alertCtrl: AlertController, 
    private postPvdr: PostProvider
  ) { }

  ngOnInit() {
    this.plotData();
    this.theReason = "Showing Inappropriate Behavior";
  }

  goBack(){
    this.modalController.dismiss();
  }
  toHome(){
    this.router.navigate(['tabs']);
  }
  othersClick(){
    this.others = !this.others;
    console.log("this.others:"+this.others);
    if(!this.others){
      this.otherReason = "";
    }
  }

  changeReason(x){
    this.theReason = x;
  }

  reportThisUser(){

    console.log("thereason:"+this.theReason);
    this.storage.get('greenthumb_user_id').then((user_id) => {

      if(this.others && this.otherReason.length>0){
        this.theReason = this.theReason +"     -     "+ this.otherReason
      }

      let body2 = {
        action : 'reportThisUser',
        reported_user : this.reportedId,
        reported_by : user_id,
        report_reason : this.theReason
  
      };
      this.postPvdr.postData(body2, 'report.php').subscribe(async data => {
        
        if(data.success){
          this.goBack();

          this.alertCtrl.create({
            header: 'Report Successfully Sent!',
            message: '<b style="text-align: justify;font-weight:lighter">You have successfully reported <b style="color: #1dc1e6;font-weight: lighter;">'+ this.reportedName +'</b>,'+ 
            'it will take some time for our administrators to go through numerous user reports.'+
            ' Please bear with us, rest assured we will go through each one carefully and take the'+
            ' necessary actions to make your user experience in Greenthumb a pleasant one. We thank you'+
            ' for your patience. Please email screenshots of your report to Greenthumb@gmail.com</b>',
            cssClass: 'foo',
            
            buttons: [{
                text: 'CLOSE',
                role: 'cancel',
                handler: () => {
                    console.log('Done reporting!');
                }
            }]
          }).then(res => {
            res.present();
          });
        }
      });

    });
  }

  plotData(){

    let body = {
      action : 'reportReasonUser',
    };
    this.postPvdr.postData(body, 'report.php').subscribe(async data => {
      
      if(data.success){
        for(const key in data.result){

          this.reasonList.push(new reportReason(
            data.result[key].id,
            data.result[key].reason

          ));
        }
      }
    });
  }

}
