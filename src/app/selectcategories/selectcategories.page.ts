import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NavController, ModalController } from '@ionic/angular';
import { IonSlides } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { PostProvider } from '../../providers/credential-provider';
import { SelectcategoriessubPage } from '../selectcategoriessub/selectcategoriessub.page';
import { CategoryMain } from '../shared/model/category.model';

@Component({
  selector: 'app-selectcategories',
  templateUrl: './selectcategories.page.html',
  styleUrls: ['./selectcategories.page.scss'],
})
export class SelectcategoriesPage implements OnInit {

  @ViewChild('slides', { static: true }) slider: IonSlides;
  segment = 0;

  login_user_id : string;
  data : any;
  categoryList : CategoryMain[] = [];

  constructor(
    private router: Router,
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    private postPvdr: PostProvider,
    private storage: Storage,
  ) { }

  async segmentChanged() {
    await this.slider.slideTo(this.segment);
  }

  async slideChanged() {
    this.segment = await this.slider.getActiveIndex();
  }

  ngOnInit() {
    
  }
  ionViewWillEnter() {

    this.plotData();

  }


  plotData(){
    

    this.storage.get('greenthumb_user_id').then((user_id) => {
      this.login_user_id = user_id;
      let body321 = {
        action : 'getCategoriesMain',
        user_id : user_id
      };
      //console.log("storyalang:"+JSON.stringify(body321));
      this.postPvdr.postData(body321, 'category.php').subscribe(data => {
      

        if(data.success){
          
          const categoryList: CategoryMain[] = [];

          for(const key in data.result){


            categoryList.push(new CategoryMain(
              data.result[key].id,
              data.result[key].category_main,
              (data.result[key].category_main_photo == '') ? '' :
              this.postPvdr.myServer()+"/greenthumb/images/categories/main/"+data.result[key].category_main_photo
              ));
          }

          this.categoryList = categoryList;

        }
      });

      

    });
  }

  goBack(){
    //this.router.navigate(['describeitem']);
    //window.history.back();
    console.log("hamon");
    this.modalCtrl.dismiss();
  }

  async selected(x, val){
    var data = {
      id: x,
      val
    }
    const modal = await this.modalCtrl.create({
            component: SelectcategoriessubPage,
            cssClass: 'categories',
            id: 'modal2',
            componentProps: { data}
          });
    
            modal.onDidDismiss()
            .then((datafrom) => {
              // console.log("hamon2");
              // console.log("iba:"+JSON.stringify(datafrom));  
              // console.log("val:"+datafrom['data'].value);
              let data2 = { id : datafrom['data'].id, value : datafrom['data'].value};
              //console.log("iba2dfd:"+JSON.stringify(data2)); 
              this.modalCtrl.dismiss(data2,null,"myModal");
            });
    
          return await modal.present();
  }

  // selected(x, val){

  //   let data = { id : x, value : val};
  //   this.modalCtrl.dismiss(data);
  // }

}
