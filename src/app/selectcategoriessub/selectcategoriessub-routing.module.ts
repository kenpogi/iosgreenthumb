import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SelectcategoriessubPage } from './selectcategoriessub.page';

const routes: Routes = [
  {
    path: '',
    component: SelectcategoriessubPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SelectcategoriessubPageRoutingModule {}
