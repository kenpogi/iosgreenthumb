import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SelectcategoriessubPageRoutingModule } from './selectcategoriessub-routing.module';

import { SelectcategoriessubPage } from './selectcategoriessub.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SelectcategoriessubPageRoutingModule
  ],
  declarations: [SelectcategoriessubPage]
})
export class SelectcategoriessubPageModule {}
