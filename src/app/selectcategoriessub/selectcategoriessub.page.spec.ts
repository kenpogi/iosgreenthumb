import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SelectcategoriessubPage } from './selectcategoriessub.page';

describe('SelectcategoriessubPage', () => {
  let component: SelectcategoriessubPage;
  let fixture: ComponentFixture<SelectcategoriessubPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectcategoriessubPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SelectcategoriessubPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
