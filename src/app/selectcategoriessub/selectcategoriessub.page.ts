import { Component, OnInit } from '@angular/core';
import { NavParams } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { PostProvider } from '../../providers/credential-provider';
import { ModalController } from '@ionic/angular';
import { CategorySub } from '../shared/model/category.model';

@Component({
  selector: 'app-selectcategoriessub',
  templateUrl: './selectcategoriessub.page.html',
  styleUrls: ['./selectcategoriessub.page.scss'],
})
export class SelectcategoriessubPage implements OnInit {

  data: any;
  login_user_id: string;
  cat_main_id : string;

  categoryList : CategorySub[] = [];

  constructor(
    navParams: NavParams,
    private storage: Storage, 
    public modalCtrl: ModalController,
    private postPvdr: PostProvider
    ) { 

     this.data = navParams.get('data');
     this.cat_main_id = this.data['id'];
     console.log("data in sub:"+this.data['id']);
    }

  ngOnInit() {
    this.plotData();
  }

  selected(x, val){

    let data = { id : x, value : val};
    console.log("dataklaro:"+JSON.stringify(data));
    this.modalCtrl.dismiss(data, null, 'modal2');
  }

  plotData(){
    this.storage.get('greenthumb_user_id').then((user_id) => {
      this.login_user_id = user_id;
      let body321 = {
        action : 'getCategoriesSub',
        cat_main_id : this.cat_main_id
      };
      console.log("storyalangtulong:"+JSON.stringify(body321));
      this.postPvdr.postData(body321, 'category.php').subscribe(data => {
      

        if(data.success){
          
          const categoryList: CategorySub[] = [];

          for(const key in data.result){


            categoryList.push(new CategorySub(
              data.result[key].category_id,
              data.result[key].main_category_id,
              data.result[key].category,
              (data.result[key].category_photo == '') ? '' :
              this.postPvdr.myServer()+"/greenthumb/images/categories/sub/"+data.result[key].category_photo
              ));
          }

          this.categoryList = categoryList;

        }
      });

      

    });
  }

  goBack(){
    //this.router.navigate(['describeitem']);
    //window.history.back();
    this.modalCtrl.dismiss();
  }

}
