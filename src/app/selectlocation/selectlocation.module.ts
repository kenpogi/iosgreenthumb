import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SelectlocationPageRoutingModule } from './selectlocation-routing.module';

import { SelectlocationPage } from './selectlocation.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SelectlocationPageRoutingModule
  ],
  declarations: [SelectlocationPage]
})
export class SelectlocationPageModule {}
