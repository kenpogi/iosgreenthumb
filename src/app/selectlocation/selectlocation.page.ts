import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NavController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { PostProvider } from '../../providers/credential-provider';
import { NativeGeocoder, NativeGeocoderResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder/ngx';


@Component({
  selector: 'app-selectlocation',
  templateUrl: './selectlocation.page.html',
  styleUrls: ['./selectlocation.page.scss'],
})
export class SelectlocationPage implements OnInit {

  showMe:boolean = false;

  city : string = " ";
  zip_code : string = " ";
  district_area : string = "";

  delivery_type_id : number = 1;
  delivery_type : string = "";
  hideDiv:boolean = false;

  showDiv:boolean = false;

  map: any;

  lat: any;
  lng: any;
  myCurrentLocation : string = '';

  login_user_id = '';

  disableButton = false;

  constructor(
    private router: Router,
    private storage: Storage,
    public toastController: ToastController,
    private nativeGeocoder: NativeGeocoder,
    public navCtrl: NavController,
    private postPvdr: PostProvider,
    private geo : Geolocation
  ) { }

  ngOnInit() {
    this.showMe = true;
    this.showDiv = true;
  }
  
  show() {
    this.showMe = true;
  }
  hide() {
    this.showMe = false;
    this.disableButton = true;
    this.getCurrentLocation();
  }

  Divshow() {
    this.hideDiv = false;
    this.showDiv = true;
    this.showMe = false;
    this.delivery_type_id = 1;
  }
  Divhide() {
    this.showDiv = false;
    this.hideDiv = true;
    this.showMe = false;
    this.delivery_type_id = 2;
    
    
  }

  getCurrentLocation(){


    this.storage.get("greenthumb_user_id").then((user_id) => {
      this.login_user_id = user_id;
      let body = {
        action : 'check_location',
        user_id : user_id
      };
      this.postPvdr.postData(body, 'user.php').subscribe(async data => {
        if(data.success){

          if(data.my_location != ''){
            
            this.lat = data.latitude;
            this.lng = data.longitude;
            this.myCurrentLocation = data.my_location;


            this.city = data.my_location;

            this.disableButton = false;
            

            //this.drawMap();

          }
          else{
            this.getGeoLocation();
          }


          


        } 

      });
    });


    
    
    //this.plotData();

  }

  getGeoLocation(){
    this.geo.getCurrentPosition({
      timeout: 10000,
      enableHighAccuracy: true
    }).then((res)=>{
      this.lat = res.coords.latitude;
      this.lng = res.coords.longitude;
      
      let options: NativeGeocoderOptions = {
        useLocale: true,
        maxResults: 5
      };
    
      this.nativeGeocoder.reverseGeocode(this.lat, this.lng, options)
      .then((result: NativeGeocoderResult[]) =>{
        console.log(JSON.stringify(result[0]));
        // console.log("re:"+result[0].locality);
        // console.log("are:"+result[0].subAdministrativeArea);
        // console.log("th:"+result[0].subThoroughfare);
        this.myCurrentLocation = result[0].thoroughfare+' '
        +result[0].locality+', '+result[0].subAdministrativeArea+' '
        +result[0].countryName;

        this.city = result[0].thoroughfare+' '
        +result[0].locality;

        this.zip_code = "6500";

        this.disableButton = false;

      })
      .catch((error: any) => {
        console.log(error);
        this.getGeoLocation();
      });


      //this.drawMap();

    }).catch((er)=>{
      this.getGeoLocation();
    });
  }
 
  goHelp(){
    //window.location.href="http://greenthumbtrade.com/help";
    this.navCtrl.navigateForward((['tabs/help']), { animated: false, });
  }
  goBack(){
    window.history.back();
    //this.navCtrl.navigateForward((['setprice']), { animated: false, });
  }
  goShareitem(){



    if(this.zip_code == ""){
      this.presentToast("Please Input Zip Code");
    }
    else if(this.city == ""){
      this.presentToast("Please Input Location");
    }
    else{
      if(this.delivery_type_id == 1){
        this.delivery_type = "Local Pick-up";
      }
      else if(this.delivery_type_id == 2){
        this.delivery_type = "Shipping";
      }
      let data = {
        location : this.city,
        zip_code : this.zip_code,
        district_area : this.district_area,
        delivery_type_id : this.delivery_type_id,
        delivery_type : this.delivery_type
        };
        
        this.storage.set("greenthumb_location", data);
  
      this.navCtrl.navigateForward((['shareitem']), { animated: false, });
    }

    
  }

  async presentToast(x) {
    const toast = await this.toastController.create({
      message: x,
      duration: 3000
    });
    toast.present();
  }

}
