import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SellersifollowPageRoutingModule } from './sellersifollow-routing.module';

import { SellersifollowPage } from './sellersifollow.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SellersifollowPageRoutingModule
  ],
  declarations: [SellersifollowPage]
})
export class SellersifollowPageModule {}
