import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SellersifollowPage } from './sellersifollow.page';

describe('SellersifollowPage', () => {
  let component: SellersifollowPage;
  let fixture: ComponentFixture<SellersifollowPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellersifollowPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SellersifollowPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
