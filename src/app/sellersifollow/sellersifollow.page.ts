import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { IonSlides } from '@ionic/angular';
import { PostProvider } from '../../providers/credential-provider';
import { Storage } from '@ionic/storage';
import { Follow } from '../shared/model/follow.model';

import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-sellersifollow',
  templateUrl: './sellersifollow.page.html',
  styleUrls: ['./sellersifollow.page.scss'],
})
export class SellersifollowPage implements OnInit {

  @ViewChild('slides', { static: true }) slider: IonSlides;
  segment = 0;

  myFollowers: Follow[];
  iFollowed: Follow[];

  totalFollower: number;
  totalFollowing: number;

  constructor(
    private storage: Storage,
    public navCtrl: NavController,
    private postPvdr: PostProvider,
    private router: Router
  ) { }

  ngOnInit() {
    
  }
  goHelp(){
    //window.location.href="http://greenthumbtrade.com/help";
    this.router.navigate(['tabs/help']);
  }

  ionViewWillEnter() {
    this.plotData();
  }

  plotData(){
    console.log("folo");
    this.storage.get('greenthumb_user_id').then((user_id) => {
     


        // for followers

        let body = {
          action : 'showFollow',
          type : 1,
          user_id : user_id
        };
        this.postPvdr.postData(body, 'followers.php').subscribe(data => {
         // console.log(data);
          if(data.success){
            const followers: Follow[] = [];

            var picture: string = '';

            for(const key in data.result){


              // picture = (data.result[key].profile_photo == '') ? '' :
              // this.postPvdr.myServer()+"/brixy-live/images/"+data.result[key].profile_photo;

              followers.push(new Follow(
                data.result[key].user_id,
                data.result[key].fname,
                data.result[key].lname,
                data.result[key].city,
                data.result[key].country,
                data.result[key].username,
                (data.result[key].profile_photo == '') ? '' :
            this.postPvdr.myServer()+"/greenthumb/images/"+data.result[key].profile_photo,
              //console.log(data.result[key].user_id);
              ));
            }

            this.myFollowers = followers;
             
          }
        });


        let body2 = {
          action : 'getFollow',
          type : 1,
          user_id : user_id
        };
        this.postPvdr.postData(body2, 'followers.php').subscribe(data => {
          
          if(data.success){
            this.totalFollower = data.result;
             
          }
        });

        // for following

        let body3 = {
          action : 'showFollow',
          type : 2,
          user_id : user_id
        };
        this.postPvdr.postData(body3, 'followers.php').subscribe(data => {
         
          if(data.success){
            const followers1: Follow[] = [];

            var picture: string = '';

            for(const key in data.result){


              // picture = (data.result[key].profile_photo == '') ? '' :
              // this.postPvdr.myServer()+"/brixy-live/images/"+data.result[key].profile_photo;

              followers1.push(new Follow(
                data.result[key].user_id,
                data.result[key].fname,
                data.result[key].lname,
                data.result[key].city,
                data.result[key].country,
                data.result[key].username,
                (data.result[key].profile_photo == '') ? '' :
            this.postPvdr.myServer()+"/greenthumb/images/"+data.result[key].profile_photo,
                ));
              //console.log(data.result[key].user_id);
            }

            this.iFollowed = followers1;
             
          }
        });

        let body4 = {
          action : 'getFollow',
          type : 2,
          user_id : user_id
        };
        this.postPvdr.postData(body4, 'followers.php').subscribe(data => {
          
          if(data.success){
            this.totalFollowing = data.result;
          }
        });

      });
  }

  goBack(){
    window.history.back();
  }

  goSellerprofile(user_profile_id){
    // this.router.navigate((['viewitemsellerprofile']));
    let navigationExtras: any = {
     queryParams: { user_profile_id}
  };
     this.navCtrl.navigateForward(['viewitemsellerprofile'], navigationExtras);
   }

  async segmentChanged() {
    await this.slider.slideTo(this.segment);
  }

  async slideChanged() {
    this.segment = await this.slider.getActiveIndex();
    this.storage.set("segment", this.segment);
  }

}
