import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SellfasterPage } from './sellfaster.page';

const routes: Routes = [
  {
    path: '',
    component: SellfasterPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SellfasterPageRoutingModule {}
