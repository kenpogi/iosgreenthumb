import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SellfasterPageRoutingModule } from './sellfaster-routing.module';

import { SellfasterPage } from './sellfaster.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SellfasterPageRoutingModule
  ],
  declarations: [SellfasterPage]
})
export class SellfasterPageModule {}
