import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SellfasterPage } from './sellfaster.page';

describe('SellfasterPage', () => {
  let component: SellfasterPage;
  let fixture: ComponentFixture<SellfasterPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellfasterPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SellfasterPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
