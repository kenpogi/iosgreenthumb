import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Router, ActivatedRoute } from '@angular/router';
import { ItemPicture } from '../shared/model/item.model';

import { ToastController } from '@ionic/angular';
import { PostProvider } from '../../providers/credential-provider';
import { logging } from 'protractor';
import { promotionModel } from '../shared/model/promotion.model';

@Component({
  selector: 'app-sellfaster',
  templateUrl: './sellfaster.page.html',
  styleUrls: ['./sellfaster.page.scss'],
})
export class SellfasterPage implements OnInit {


  item_id : string = "";
  ImageArray: ItemPicture[] = [];

  promotions: promotionModel[] = [];

  constructor(
    private router: Router,
    private storage: Storage,
    public toastController: ToastController,
    private postPvdr: PostProvider,
    private route: ActivatedRoute
  ) { 
    this.route.queryParams.subscribe(params => {
      this.item_id = params["item_id"];
      console.log("item_id sell faster:"+this.item_id);
    });
  }

  ngOnInit() {
    this.plotData();
  }

  plotData(){
    let body4 = {
      action : 'getItemPhotos',
      item_id : this.item_id,
     // user_id : user_id
    };
    console.log(JSON.stringify(body4));
    this.postPvdr.postData(body4, 'post_item.php').subscribe(async data => {
      
      if(data.success){

        const itemsImage: ItemPicture[] = [];

        //var pictureProfile: string = '';

        for(const key in data.result){
          itemsImage.push(new ItemPicture(
            this.postPvdr.myServer()+"/greenthumb/images/posted_items/images/"+data.result[key].item_photo,
            )
          );

            console.log("item_photo:"+data.result[key].item_photo);
        }
        this.ImageArray = itemsImage;
      }
    });

    let body = {
      action : 'getPromotionRate',
    };
    this.postPvdr.postData(body, 'promotion.php').subscribe(async data => {


      if(data.success){

        const promotionRate: promotionModel[] = [];
      
        //var pictureProfile: string = '';

        var x = 0;

        for(const key in data.result){
          promotionRate.push(new promotionModel(
            data.result[key].id,
            data.result[key].rate,
            )
          );

        }

        this.promotions = promotionRate;


      }


      

    });


   
  }

  slideOptsOne = {
    initialSlide: 0,
    slidesPerView: 1,
    autoplay:true
   };


   promoteItem(promote_id,promote_rate){
     // to do: check promotion first for validation
    let body = {
      action : 'promoteItem',
      item_id : this.item_id
    };
    this.postPvdr.postData(body, 'promotion.php').subscribe(async data => {


      if(data.success){

        this.presentToast("Your Item has been promoted");

      }

    });
   }

  goPromoteplus(){
     this.router.navigate(['promoteplus']);
  }
  goBack(){
    window.history.back();
  }
  async presentToast(x) {
    const toast = await this.toastController.create({
      message: x,
      duration: 3000
    });
    toast.present();
  }

}
