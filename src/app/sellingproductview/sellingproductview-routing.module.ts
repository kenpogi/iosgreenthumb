import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SellingproductviewPage } from './sellingproductview.page';

const routes: Routes = [
  {
    path: '',
    component: SellingproductviewPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SellingproductviewPageRoutingModule {}
