import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SellingproductviewPageRoutingModule } from './sellingproductview-routing.module';

import { SellingproductviewPage } from './sellingproductview.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SellingproductviewPageRoutingModule
  ],
  declarations: [SellingproductviewPage]
})
export class SellingproductviewPageModule {}
