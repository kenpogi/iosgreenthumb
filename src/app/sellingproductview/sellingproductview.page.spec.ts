import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SellingproductviewPage } from './sellingproductview.page';

describe('SellingproductviewPage', () => {
  let component: SellingproductviewPage;
  let fixture: ComponentFixture<SellingproductviewPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellingproductviewPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SellingproductviewPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
