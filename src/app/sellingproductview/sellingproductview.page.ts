import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sellingproductview',
  templateUrl: './sellingproductview.page.html',
  styleUrls: ['./sellingproductview.page.scss'],
})
export class SellingproductviewPage implements OnInit {

  constructor(
    private router: Router
  ) { }


 goBack(){
   window.history.back();
 }

goSellerprof(){
 this.router.navigate((['viewitemsellerprofile']));
}
goEdit(){
  this.router.navigate((['edititem']));
 }

  ngOnInit() {
  }

}
