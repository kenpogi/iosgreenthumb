import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FollowerDataService {

  private id : String;
  constructor() { }

  setData(id){
    this.id =  id;
  }

  getData(){
    return this.id;
  }

}
