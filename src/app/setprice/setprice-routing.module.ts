import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SetpricePage } from './setprice.page';

const routes: Routes = [
  {
    path: '',
    component: SetpricePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SetpricePageRoutingModule {}
