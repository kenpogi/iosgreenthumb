import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SetpricePageRoutingModule } from './setprice-routing.module';

import { SetpricePage } from './setprice.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SetpricePageRoutingModule
  ],
  declarations: [SetpricePage]
})
export class SetpricePageModule {}
