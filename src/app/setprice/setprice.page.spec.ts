import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SetpricePage } from './setprice.page';

describe('SetpricePage', () => {
  let component: SetpricePage;
  let fixture: ComponentFixture<SetpricePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetpricePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SetpricePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
