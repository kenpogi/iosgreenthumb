import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NavController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-setprice',
  templateUrl: './setprice.page.html',
  styleUrls: ['./setprice.page.scss'],
})
export class SetpricePage implements OnInit {

  price: any;

  constructor(
    private router: Router,
    public navCtrl: NavController,
    public toastController: ToastController,
    private storage: Storage,
  ) { }

  ngOnInit() {
  }

  goHelp(){
    //window.location.href="http://greenthumbtrade.com/help";
    this.navCtrl.navigateForward((['tabs/help']), { animated: false, });
  }
  goBack(){
    window.history.back();
    //this.navCtrl.navigateForward((['describeitem']), { animated: false, });
  }
  goSelectlocation(){

    if(this.price == null || this.price==''){
      this.presentToast("Please Input Price of the Item");
    }
    else{
      this.storage.set("greenthumb_price", this.price);

      this.navCtrl.navigateForward((['selectlocation']), { animated: false, });
    }
    
  }

  async presentToast(x) {
    const toast = await this.toastController.create({
      message: x,
      duration: 3000
    });
    toast.present();
  }

}
