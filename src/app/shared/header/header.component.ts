import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {

  @Input() headerTitle: string;

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
  }

  toAccount(){
    this.router.navigate(['myaccount']);
  }

  toSystemNotif(){
    this.router.navigate(['systemnotif']);
  }

  toSearch(){
    this.router.navigate(['search']);
  }
}
