export class Announcement {
    constructor(
      public id: number,
      public title: string,
      public announcement: string,
      public date: string,
      public time: string
    ) {}
  }