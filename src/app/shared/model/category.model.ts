export class CategoryMain {
    constructor(
      public id: number,
      public category: string,
      public category_photo: string,
    ) {}
  }

  export class CategorySub {
    constructor(
      public id: number,
      public cat_main_id : number,
      public category: string,
      public category_photo: string,
    ) {}
  }