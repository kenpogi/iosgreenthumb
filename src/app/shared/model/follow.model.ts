export class Follow {
    constructor(
      public id: number,
      public fname: string,
      public lname: string,
      public city: string,
      public country: string,
      public username: string,
      public profile_photo : string,
    ) {}
  }

  export class RankingModel {
    constructor(
      public id: number,
      public fname: string,
      public lname: string,
      public city: string,
      public country: string,
      public picture: string,
      public user_level: number,
      public follower: number,
      public gift_sum: number,
      public badge_name: string
    ) {}
  }