export class giftModel {
    constructor(
      public id: number,
      public name: string,
      public image: string,
      public imagegif: string,
      public imageaudio: string,
      public imageaudio2: string,
      public price: string
      
    ) {}
  }

  export class giftTransactionModel {
    constructor(
      public image: string,
      public dateandtime: string,
      public imageaudio: string,
      public imageaudio2: string,
      public text: string
      
    ) {}
  }