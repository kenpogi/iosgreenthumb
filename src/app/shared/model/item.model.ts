export class Item {
  constructor(
    public id: string,
    public user_id: string,
    public username: string,
    public profile_photo : string,
    public price: string,
    public title: string,
    public stocks_id : string,
    public others_stock : string,
    public quantity: number,
    public category: string,
    public location: string,
    public save_status: string,
    public item_status: string,
    public item_cover_photo : string,
    public user_rating : string,
    public date : string,
    public time : string,
  ) {}
}

export class ItemPicture{

  constructor(
    public picture_filename: string,
  ){}
}
