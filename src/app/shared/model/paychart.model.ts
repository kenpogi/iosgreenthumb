export class paychartModel {
    constructor(
      public id: number,
      public gold_bars: number,
      public usd_amount: number
    ) {}
  }