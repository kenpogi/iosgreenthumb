export class reportReason {
    constructor(
      public id: number,
      public reason: string
    ) {}
  }