export class SystemNotif {
    constructor(
      public id: number,
      public notification: string,
      public date: string,
      public time: string,
      public with_rate : string,
      public rate_user_id : string
    ) {}
  }