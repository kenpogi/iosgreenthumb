export class UserLivestream {
    constructor(
      public id: string,
      public fname: string,
      public lname: string,
      public city: string,
      public picture: string,
      public level: number,
      public follower: number,
      public following: number
    ) {}
  }
  