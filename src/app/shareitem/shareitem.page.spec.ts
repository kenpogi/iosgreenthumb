import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ShareitemPage } from './shareitem.page';

describe('ShareitemPage', () => {
  let component: ShareitemPage;
  let fixture: ComponentFixture<ShareitemPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShareitemPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ShareitemPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
