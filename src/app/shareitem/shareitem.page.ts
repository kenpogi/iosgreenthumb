import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NavController, Platform  } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { PostProvider } from '../../providers/credential-provider'; 
import { ItemPicture } from '../shared/model/item.model';

@Component({
  selector: 'app-shareitem',
  templateUrl: './shareitem.page.html',
  styleUrls: ['./shareitem.page.scss'],
})
export class ShareitemPage implements OnInit {

  user_id : string;
  category_id : number;
  stocks_id : number;
  stocks_name : string;
  quantity : number;
  title : string;
  description : string;
  price : number;
  zip_code : number;
  location : string;
  district_area : string;
  delivery_type_id : number;
  delivery_type : string;
  category_name : string;

  item_pictures : any;

  ImageArray: ItemPicture[] = [];
  picture: any;
  complete_pic: any;


  subscription: any;
  username: string = "";

  constructor(
    private router: Router,
    private storage: Storage,
    private postPvdr: PostProvider,
    public navCtrl: NavController,
    private platform: Platform
  ) {

    this.subscription = this.platform.backButton.subscribe(()=>{
      console.log('Handler was called!');
      this.goBack();
    });

   }

  ngOnInit() {
    
  }

  slideOptsOne = {
    initialSlide: 0,
    slidesPerView: 1,
    autoplay:true
   };

  ionViewWillLeave(){
    this.subscription.unsubscribe();
}

  ionViewWillEnter() {

    this.storage.get("greenthumb_user_id").then((user_id) => {

      this.storage.get("greenthumb_title").then((title) => {
          
          this.storage.get("greenthumb_description").then((val) => {

            this.storage.get("greenthumb_price").then((price) => {

              this.storage.get("greenthumb_location").then((item) => {

                this.storage.get("greenthumb_itempost_pic").then((pictures)=>{

                  this.user_id = user_id;
                  this.category_id = val['category_id'];
                  this.category_name = val['category_name'];
                  this.stocks_id = val['stocks_id'];
                  this.stocks_name = val['stocks_name'];
                  this.quantity = val['quantity'];
                  this.title = title;
                  this.description = val['description'];
                  this.price = price;
                  this.zip_code = item['zip_code'];
                  this.location = item['location'];
                  this.district_area = item['district_area'];
                  this.delivery_type_id = item['delivery_type_id'];
                  this.delivery_type = item['delivery_type'];

                  this.item_pictures = pictures;


                  for (var key in pictures) {
                   // console.log("filenames:"+pictures[key].picture_filename)
                   this.ImageArray.push(new ItemPicture(
                    this.postPvdr.myServer()+"/greenthumb/images/posted_items/images/"+pictures[key].picture_filename,
                    )
                    );
                  }

                });
   
              });

            });


          });
      });

      let body = {
        action : 'getUsername',
        user_id : user_id,
      };
      console.log(JSON.stringify(body));
      this.postPvdr.postData(body, 'user.php').subscribe(async data => {
  
        this.username = data.username;

        this.picture = data.profile_photo;
        this.complete_pic = this.postPvdr.myServer()+"/greenthumb/images/"+this.picture;

        
      });
    });
  }

  goHelp(){
    this.navCtrl.navigateForward((['help']), { animated: false, });
  }
  goBack(){
    //this.navCtrl.navigateForward((['selectlocation']), { animated: false, });
    window.history.back();
  }
  goSellerprof(){

  }
  goItemposted(){

    


    let body = {
      action : 'postItem',
      user_id : this.user_id,
      category_id : this.category_id,
      stocks_id : this.stocks_id,
      quantity : this.quantity,
      title : this.title,
      description : this.description,
      price : this.price,
      zip_code : this.zip_code,
      location : this.location,
      district_area : this.district_area,
      delivery_type_id : this.delivery_type_id,
      item_pictures : this.item_pictures,
    };
    console.log(JSON.stringify(body));
    this.postPvdr.postData(body, 'post_item.php').subscribe(async data => {


      if(data.success){

       // this.router.navigate(['itemposted']);

       console.log("item_id from db:"+data.item_id_redsult);
        let navigationExtras: any = {
          queryParams: { item_id : data.item_id_redsult}
       };
          this.navCtrl.navigateForward(['itemposted'], navigationExtras);
      }


      

    });

    
  }

}
