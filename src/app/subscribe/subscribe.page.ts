import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';

import { ToastController, AlertController } from '@ionic/angular';

import { PostProvider } from '../../providers/credential-provider';

@Component({
  selector: 'app-subscribe',
  templateUrl: './subscribe.page.html',
  styleUrls: ['./subscribe.page.scss'],
})
export class SubscribePage implements OnInit {

  withPackage : boolean = false;

  login_user_id : string = "";

  constructor(
    private storage: Storage,
    public alertCtrl: AlertController,
    private postPvdr: PostProvider,
    private toastController: ToastController,
  ) { }

  ngOnInit() {
    this.plotData();
  }

  goBack(){
    window.history.back();
  }

  plotData(){
    this.storage.get("greenthumb_user_id").then((user_id) => {

      this.login_user_id = user_id;

      let body2 = {
        action : 'checkSubscription',
        user_id : user_id
      };
      console.log("subscription:"+JSON.stringify(body2));
      this.postPvdr.postData(body2, 'subscription.php').subscribe(data => {
        
        if(data.success){

          this.withPackage = true;


        }
        
      });

    });
  }

 

  verificationSubscription(){
    

    this.alertCtrl.create({
      header: 'Subscription',
      message: 'Are you sure you want to subscribe?',
      cssClass: 'food',
      
      buttons: [{
          text: 'CANCEL',
          role: 'cancel',
          handler: () => {
              console.log('Cancel verification!');
          }
      },
      {
        text: 'YES',
        handler: () => {

          let body2 = {
            action : 'checkSubscription',
            user_id : this.login_user_id
          };
          console.log("subscription:"+JSON.stringify(body2));
          this.postPvdr.postData(body2, 'subscription.php').subscribe(data => {
            
            if(data.success){
      
              //this.withPackage = true;
              let body2 = {
                action : 'monthlySubscription',
                user_id : this.login_user_id,
                monthly_subscription : 1
              };
              this.postPvdr.postData(body2, 'subscription.php').subscribe(data => {
                
                if(data.success){
                 
                  this.withPackage = true;
                }
              });
              
              this.presentToast("Package Successfully Subscribed.");
      
            }
            else{
              this.iSubscribe();
            }
            
          });


          
        }
      }
    ]
    }).then(res => {
      res.present();
    });
    
  }

  iSubscribe(){
    this.storage.get("greenthumb_user_id").then((user_id) => {
      let body2 = {
        action : 'subscribePackage',
        user_id : user_id,
      };
      console.log("looua:"+JSON.stringify(body2));
      this.postPvdr.postData(body2, 'subscription.php').subscribe(data => {
        
        if(data.success){
          this.presentToast("Package Successfully Subscribed.");
          this.withPackage = true;
          //this.monthlySubscription = true;
        }
      });

    });
  }

  verifyCancel(){

    this.alertCtrl.create({
            header: 'Subscription Package',
            message: 'Are you sure you want to cancel Subscription?',
            cssClass: 'food',
            
            buttons: [{
                text: 'CANCEL',
                role: 'cancel',
                handler: () => {
                    console.log('Cancel subscription!');
                }
            },
            {
              text: 'YES',
              handler: () => {
                this.cancelSubscription();
              }
            }
          ]
          }).then(res => {
            res.present();
          });
  }

  cancelSubscription(){

    this.storage.get("greenthumb_user_id").then((user_id) => {

      let body2 = {
        action : 'monthlySubscription',
        user_id : user_id,
        monthly_subscription : 0
      };
      console.log("looua:"+JSON.stringify(body2));
      this.postPvdr.postData(body2, 'subscription.php').subscribe(data => {
        
        if(data.success){
         
          this.withPackage = false;
        }
      });

    });
  

  }


  async presentToast(m) {
    const toast = await this.toastController.create({
      message: m,
      duration: 3000
    });
    toast.present();
  }

}
