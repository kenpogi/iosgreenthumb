import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SuspendmodalPage } from './suspendmodal.page';

const routes: Routes = [
  {
    path: '',
    component: SuspendmodalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SuspendmodalPageRoutingModule {}
