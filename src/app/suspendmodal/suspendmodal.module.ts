import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SuspendmodalPageRoutingModule } from './suspendmodal-routing.module';

import { SuspendmodalPage } from './suspendmodal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SuspendmodalPageRoutingModule
  ],
  declarations: [SuspendmodalPage]
})
export class SuspendmodalPageModule {}
