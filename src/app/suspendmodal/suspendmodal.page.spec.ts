import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SuspendmodalPage } from './suspendmodal.page';

describe('SuspendmodalPage', () => {
  let component: SuspendmodalPage;
  let fixture: ComponentFixture<SuspendmodalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuspendmodalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SuspendmodalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
