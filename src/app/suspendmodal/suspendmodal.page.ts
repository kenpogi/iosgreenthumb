import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ModalController } from '@ionic/angular';
import { UserpolicyPage } from '../userpolicy/userpolicy.page';

@Component({
  selector: 'app-suspendmodal',
  templateUrl: './suspendmodal.page.html',
  styleUrls: ['./suspendmodal.page.scss'],
})
export class SuspendmodalPage implements OnInit {

  constructor(
    private modalController: ModalController,
    private router: Router
  ) { }

  ngOnInit() {
  }

  goBack(){
    this.modalController.dismiss();
  }
  
  goToUserPolicy(){
    this.router.navigate(['tabs/userpolicy']);
  }

//   async goToUserPolicy(){
//     const modal = await this.modalController.create({
//       component: UserpolicyPage,
//       cssClass: ''
//     });
//     return await modal.present();
//   }

}
