import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SystemnotifPage } from './systemnotif.page';

const routes: Routes = [
  {
    path: '',
    component: SystemnotifPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SystemnotifPageRoutingModule {}
