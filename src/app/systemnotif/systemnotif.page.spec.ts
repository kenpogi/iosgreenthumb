import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SystemnotifPage } from './systemnotif.page';

describe('SystemnotifPage', () => {
  let component: SystemnotifPage;
  let fixture: ComponentFixture<SystemnotifPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SystemnotifPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SystemnotifPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
