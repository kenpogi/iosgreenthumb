import { Component, OnInit, NgZone, ViewChild,ElementRef  } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController, NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Announcement } from '../shared/model/announcement.model';
import { PostProvider } from '../../providers/credential-provider';
import { Item } from '../shared/model/item.model';
import { ActivatedRoute } from "@angular/router";
import { RegisterPage } from '../register/register.page';


@Component({
  selector: 'app-tab1',
  templateUrl: './tab1.page.html',
  styleUrls: ['./tab1.page.scss'],
})
export class Tab1Page implements OnInit {
  @ViewChild('myInput', null) myInput: ElementRef;
  
  ImageArray: any = [];
  hideMe:boolean = false;

  login_user_id = 0;

  searchQuery: string = "";

  itemList: Item[] = [];
  itemListPromoted: Item[] = [];

  status: boolean = false;

  itemListSearched: Item[] = [];


  announcement: Announcement[] = [];

  category_id : string = "";

  counter: number = 0;

  searchedTurn : boolean = false;

  noResultsFound : boolean = false;

  loader: boolean = true;
  

  constructor(
    private router: Router,
    private postPvdr: PostProvider,
    private zone: NgZone,
    private modalController: ModalController,
    public navCtrl: NavController,
    private storage: Storage,
    private route: ActivatedRoute
    ) {

      this.ImageArray = [
        {'image':'assets/greenthumb-images/bunny2.png',
          'name': 'Lorem ipsum dolor 1',
          'text': 'the'},
        {'image':'assets/greenthumb-images/bunny2.png',
        'name': 'Lorem ipsum dolor 2',
        'text': 'the'},
        {'image':'assets/greenthumb-images/bunny2.png',
        'name': 'Lorem ipsum dolor 3',
        'text': 'the'}
      ]


      this.route.queryParams.subscribe(params => {
        this.category_id = params["category_id"];
        console.log("params:"+JSON.stringify(params));
        console.log("categor_id:"+this.category_id);
       
      });

    }

    goLocation(){
      this.navCtrl.navigateForward(['location']);
    }
    goProductview(item_id){
     // this.router.navigate(['productview']);
      let navigationExtras: any = {
          queryParams: { item_id}
      };
      this.navCtrl.navigateForward(['productview'], navigationExtras);
    }
    goOffers(){
      this.router.navigate(['tabs/offers']);
    }

    goSellerprof(user_profile_id){
     // this.router.navigate((['viewitemsellerprofile']));
     let navigationExtras: any = {
      queryParams: { user_profile_id}
   };
      this.navCtrl.navigateForward(['viewitemsellerprofile'], navigationExtras);
    }

  eventHandler(keyCode) {
    console.log("key:"+keyCode);
  }

    async openRegistermodal() {
          const modal = await this.modalController.create({
            component: RegisterPage,
            cssClass: 'registermodalstyle'
          });

      modal.onDidDismiss()
        .then((data) => {
          let body = {
            action : 'notNowUpdate',
            user_id : this.login_user_id,
          };
  
          this.postPvdr.postData(body, 'user.php').subscribe(data => {

            if(data.success){
            }
            
          });
        });


          return await modal.present();
        }

        followThis(user_id){
          let body = {
            action : 'follow',
            user_id : user_id,
            followed_by : this.login_user_id,
            //status : this.status
          };
          console.log("follow:"+JSON.stringify(body));
          this.postPvdr.postData(body, 'followers.php').subscribe(data => {
            console.log(data);
            if(data.success){
              // uncomment this when you add system notifications
                // this.followClass = "myHeartFollow";
                // this.status = !this.status; 
                // if(this.status){
  
                //   let body2 = {
                //     action : 'addNotification',
                //     user_id : this.user_id,
                //     followed_by: user_id,
                //     notification_message : "has followed you",
                //     status : this.status
                //   };
          
                //   this.postPvdr.postData(body2, 'system_notification.php').subscribe(data => {
  
                //   })
                // }
            }
          });
        }
        saveThis(item_id){
          console.log("gi saved lamang pud");
          let body = {
            action : 'saveItem',
            item_id : item_id,
            user_id : this.login_user_id,
          };
  
          this.postPvdr.postData(body, 'save_item.php').subscribe(data => {

            if(data.success){
              if(data.saveStatus == '1'){
                console.log('1 ko');
                var element = document.getElementById("class"+item_id);
                element.classList.remove("myHeartUnSave");
                element.classList.add("myHeartSave");
                console.log("classList:"+element.classList);
              }
              else{
                console.log("0 ko");
                var element = document.getElementById("class"+item_id);
                element.classList.remove("myHeartSave");
                element.classList.add("myHeartUnSave");
              }
            }
            
          });
          
          
        }

        searchItem(){
          let body = {
            action : 'getPostItemSearchedQuery',
            searchQuery : this.searchQuery,
            user_id : this.login_user_id,
          };
          console.log(JSON.stringify(body));
          this.postPvdr.postData(body, 'post_item.php').subscribe(async data => {
            
            if(data.success){
      
              const itemsSearched: Item[] = [];
      
              //var pictureProfile: string = '';
      
              var x = 0;
      
              for(const key in data.result){
                itemsSearched.push(new Item(
                  data.result[key].item_id,
                  data.result[key].user_id,
                  data.result[key].username,
                  (data.result[key].profile_photo == '') ? '' :
              this.postPvdr.myServer()+"/greenthumb/images/"+data.result[key].profile_photo,
                  data.result[key].price,
                  data.result[key].title,
                  data.result[key].stocks_id,
                  data.result[key].others_stock,
                  parseInt(data.result[key].quantity),
                  data.result[key].category,
                  data.result[key].location,
                  data.result[key].saveStatus,
                  data.result[key].status,
                  this.postPvdr.myServer()+"/greenthumb/images/posted_items/images/"+data.result[key].item_photo,
                  data.result[key].user_rating,
                  data.result[key].date,
                  data.result[key].time
                  )
                );
      
                x++;
              }
      
              if(x==0){
                this.noResultsFound = true;
              }
              else{
                this.noResultsFound = false;
              }
              this.searchedTurn = true;
              console.log("this.serachedTurn:"+this.searchedTurn);
              this.itemListSearched = itemsSearched;
            }
          });

        }

    async check_user_exists(){

      this.storage.get("greenthumb_user_id").then((user_id) => {

        let body = {
          action : 'check_user_exists',
          user_id : user_id
        };
        console.log("pira ka kilo:"+JSON.stringify(body));
        this.postPvdr.postData(body, 'credentials-api.php').subscribe(data => {
         
          if(data.success){
            this.openRegistermodal();
          }
        }); 
      });
    }


    slideOptsOne = {
      initialSlide: 0,
      slidesPerView: 1,
      autoplay:true
     };
   
    hide() {
      this.hideMe = false;
      let body321 = {
        action : 'hideAnnouncement',
        user_id : this.login_user_id,
      };
      this.postPvdr.postData(body321, 'system_notification.php').subscribe(data => {
      

        if(data.success){
          // updated na

        }
      });
    }

    allAnnouncements(){
      this.storage.set("greenthumb_segment", 1);
      this.router.navigate(['tabs/tab5']);
    }

  ngOnInit() {
    
  }

  ionViewWillEnter() {
    
    //this.plotData();
    //this.loader = true;


  console.log("this is ionviewwill enter tab 1");
    setInterval(()=>{                           //<<<---using ()=> syntax
      this.zone.run(()=>{
        this.getAnnouncement();
      });
    }, 7000);

    if(this.category_id != undefined){
      this.searchedPosted();
    }
    else{
      this.plotData();
    }

    this.check_user_exists();
    
  }

  searchedPosted(){

    console.log("searchedPosted"+this.counter++);
    let body = {
      action : 'getPostItemsSearched',
      category_id : this.category_id,
      user_id : this.login_user_id,
    };
    console.log(JSON.stringify(body));
    this.postPvdr.postData(body, 'post_item.php').subscribe(async data => {
      
      if(data.success){

        const itemsSearched: Item[] = [];

        //var pictureProfile: string = '';

        var x = 0;

        for(const key in data.result){
          itemsSearched.push(new Item(
            data.result[key].item_id,
            data.result[key].user_id,
            data.result[key].username,
            (data.result[key].profile_photo == '') ? '' :
              this.postPvdr.myServer()+"/greenthumb/images/"+data.result[key].profile_photo,
            data.result[key].price,
            data.result[key].title,
            data.result[key].stocks_id,
            data.result[key].others_stock,
            parseInt(data.result[key].quantity),
            data.result[key].category,
            data.result[key].location,
            data.result[key].saveStatus,
            data.result[key].status,
            this.postPvdr.myServer()+"/greenthumb/images/posted_items/images/"+data.result[key].item_photo,
            data.result[key].user_rating,
            data.result[key].date,
            data.result[key].time
            )
          );

          x++;
        }

        if(x==0){
          this.noResultsFound = true;
        }
        else{
          this.noResultsFound = false;
        }

        this.searchedTurn = true;
        console.log("this.serachedTurn:"+this.searchedTurn);
        this.itemListSearched = itemsSearched;
      }
    });
  }


  plotData(){
    // for announcement
    //console.log("plotData");
    this.getAnnouncement();

    this.storage.get('greenthumb_user_id').then((user_id) => {



      // for promotion
      let bodyPromotion = {
        action : 'getPromotedPostItems',
        user_id : user_id,
      };
      console.log("plot data promotion:"+JSON.stringify(bodyPromotion));
      this.postPvdr.postData(bodyPromotion, 'post_item.php').subscribe(async data => {
        
        if(data.success){

          const itemsPromoted: Item[] = [];

          //var pictureProfile: string = '';

          for(const key in data.result){
            // pictureProfile = (data.result[key].profile_photo == '') ? '' :
            // this.postPvdr.myServer()+"/brixy-live/images/"+data.result[key].profile_photo;
            itemsPromoted.push(new Item(
              data.result[key].item_id,
              data.result[key].user_id,
              data.result[key].username,
              (data.result[key].profile_photo == '') ? '' :
                this.postPvdr.myServer()+"/greenthumb/images/"+data.result[key].profile_photo,
              data.result[key].price,
              data.result[key].title,
              data.result[key].stocks_id,
              data.result[key].others_stock,
              parseInt(data.result[key].quantity),
              data.result[key].category,
              data.result[key].location,
              data.result[key].saveStatus,
              data.result[key].status,
              this.postPvdr.myServer()+"/greenthumb/images/posted_items/images/"+data.result[key].item_photo,
              data.result[key].user_rating,
              data.result[key].date,
              data.result[key].time
              )

              
            );
            console.log("hello user_rating"+data.result[key].user_rating);

          }

         // this.searchedTurn = false;
          this.itemListPromoted = itemsPromoted;
          this.loader = false;
        }
       });




      let body = {
        action : 'getPostItems',
        user_id : user_id,
      };
      console.log("plot data tab1:"+JSON.stringify(body));
      this.postPvdr.postData(body, 'post_item.php').subscribe(async data => {
        
        if(data.success){

          const items: Item[] = [];

          //var pictureProfile: string = '';

          for(const key in data.result){
            // pictureProfile = (data.result[key].profile_photo == '') ? '' :
            // this.postPvdr.myServer()+"/brixy-live/images/"+data.result[key].profile_photo;
            items.push(new Item(
              data.result[key].item_id,
              data.result[key].user_id,
              data.result[key].username,
              (data.result[key].profile_photo == '') ? '' :
                this.postPvdr.myServer()+"/greenthumb/images/"+data.result[key].profile_photo,
              data.result[key].price,
              data.result[key].title,
              data.result[key].stocks_id,
              data.result[key].others_stock,
              parseInt(data.result[key].quantity),
              data.result[key].category,
              data.result[key].location,
              data.result[key].saveStatus,
              data.result[key].status,
              this.postPvdr.myServer()+"/greenthumb/images/posted_items/images/"+data.result[key].item_photo,
              data.result[key].user_rating,
              data.result[key].date,
              data.result[key].time
              )

              
            );
            console.log("hello user_rating"+data.result[key].user_rating);

          }

          this.searchedTurn = false;
          this.itemList = items;
          this.loader = false;
        }
       });

      });

  }

  logRatingChange(event){
    //this.myRate = event;
  }


  getAnnouncement(){

    this.storage.get('greenthumb_user_id').then((user_id) => {
      this.login_user_id = user_id;
      let body321 = {
        action : 'getAnnouncement',
        user_id : user_id
      };
      //console.log("storyalang:"+JSON.stringify(body321));
      this.postPvdr.postData(body321, 'system_notification.php').subscribe(data => {
      

        if(data.success){
          
          const announcementsList: Announcement[] = [];

          var x = 0;

          for(const key in data.result){


            announcementsList.push(new Announcement(
              data.result[key].id,
              data.result[key].title,
              data.result[key].announcment,
              data.result[key].date,
              data.result[key].time
              ));
              x++;
          }
          if(x==0){
            this.hideMe = false;
          }
          else{
            this.hideMe = true;
          }

          this.announcement = announcementsList;

        }
      });

      

    });
  }

  doRefresh(event) {
    console.log('Begin async operation');
    //this.loadUser();
    this.plotData();

    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
      this.searchQuery = '';
    }, 2000);
  }
 


}// end

