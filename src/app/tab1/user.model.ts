export class UserLive {
    constructor(
      public id: string,
      public stream_id: string,
      public fname: string,
      public lname: string,
      public city: string,
      public picture: string,
      public user_level: number,
      public badge_name: string,
      public viewers : number,
      public broadcast_topic: string
    ) {}
  }
  