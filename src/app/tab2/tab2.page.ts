import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { IonSlides } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { PostProvider } from '../../providers/credential-provider';
import { Follow } from '../shared/model/follow.model';
import { LiveprofilePage } from '../liveprofile/liveprofile.page';
import { ModalController } from '@ionic/angular';


@Component({
  selector: 'app-tab2',
  templateUrl: './tab2.page.html',
  styleUrls: ['./tab2.page.scss'],
})
export class Tab2Page implements OnInit {

  @ViewChild('slides', { static: true }) slider: IonSlides;
  segment = 0;

  myFollowers: Follow[];
  iFollowed: Follow[];

  totalFollower: number;
  totalFollowing: number;

  constructor(
    private router: Router,
    private storage: Storage,
    public modalController: ModalController,
    private postPvdr: PostProvider
    ) { }

  async segmentChanged() {
    await this.slider.slideTo(this.segment);
  }

  async slideChanged() {
    this.segment = await this.slider.getActiveIndex();
  }

  goTab1(){
    this.router.navigate(['tabs']);
  }

  toAccount(){
    this.router.navigate(['myaccount']);
  }

  goVideoTapped(id)
  {
    //this.router.navigate(['viewlive']);
    this.router.navigateByUrl('/viewlive/'+id);
  }

  ngOnInit() {
    
  }

  ionViewWillEnter() {
    this.plotData();
  }

  plotData(){
    console.log("folo");
    this.storage.get('greenthumb_user_id').then((user_id) => {
     


        // for followers

        let body = {
          action : 'showFollow',
          type : 1,
          user_id : user_id
        };
        this.postPvdr.postData(body, 'followers.php').subscribe(data => {
         // console.log(data);
          if(data.success){
            const followers: Follow[] = [];

            var picture: string = '';

            for(const key in data.result){


              picture = (data.result[key].profile_photo == '') ? '' :
              this.postPvdr.myServer()+"/brixy-live/images/"+data.result[key].profile_photo;

              followers.push(new Follow(
                data.result[key].user_id,
                data.result[key].fname,
                data.result[key].lname,
                data.result[key].city,
                data.result[key].country,
                picture,
                (data.result[key].profile_photo == '') ? '' :
            this.postPvdr.myServer()+"/greenthumb/images/"+data.result[key].profile_photo,
                ));
              //console.log(data.result[key].user_id);
            }

            this.myFollowers = followers;
             
          }
        });


        let body2 = {
          action : 'getFollow',
          type : 1,
          user_id : user_id
        };
        this.postPvdr.postData(body2, 'followers.php').subscribe(data => {
          
          if(data.success){
            this.totalFollower = data.result;
             
          }
        });

        // for following

        let body3 = {
          action : 'showFollow',
          type : 2,
          user_id : user_id
        };
        this.postPvdr.postData(body3, 'followers.php').subscribe(data => {
         
          if(data.success){
            const followers1: Follow[] = [];

            var picture: string = '';

            for(const key in data.result){


              picture = (data.result[key].profile_photo == '') ? '' :
              this.postPvdr.myServer()+"/brixy-live/images/"+data.result[key].profile_photo;

              followers1.push(new Follow(
                data.result[key].user_id,
                data.result[key].fname,
                data.result[key].lname,
                data.result[key].city,
                data.result[key].country,
                picture,
                (data.result[key].profile_photo == '') ? '' :
            this.postPvdr.myServer()+"/greenthumb/images/"+data.result[key].profile_photo,
                ));
              //console.log(data.result[key].user_id);
            }

            this.iFollowed = followers1;
             
          }
        });

        let body4 = {
          action : 'getFollow',
          type : 2,
          user_id : user_id
        };
        this.postPvdr.postData(body4, 'followers.php').subscribe(data => {
          
          if(data.success){
            this.totalFollowing = data.result;
          }
        });

      });
  }

  async openliveprofile(user_id) {
        const modal = await this.modalController.create({
          component: LiveprofilePage,
          cssClass: 'liveprofilemodalstyle',
          componentProps: { 
            liveStreamProfileId: user_id
          }
        });
        return await modal.present();
      }

}
