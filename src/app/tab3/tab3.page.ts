import { Component, OnInit, ViewChild } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { NavController, ToastController, AlertController  } from '@ionic/angular';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { PostProvider } from '../../providers/credential-provider';
import { ItemPicture } from '../shared/model/item.model';
import { Crop } from '@ionic-native/crop/ngx';
import { File } from '@ionic-native/file/ngx';

@Component({
  selector: 'app-tab3',
  templateUrl: './tab3.page.html',
  styleUrls: ['./tab3.page.scss'],
}) 
export class Tab3Page implements OnInit {


  greenthumb_title : string = "";
  

  login_user_id : string;

  user_id : string = "";
  fileName : string = "";
  image:any;

  croppedImagepath = "";
  isLoading = false;
  pictures: ItemPicture[] = [];

  public photos: any;
  public base64Image: string;
  public fileImage: string;
  public responseData: any;
  userData = { user_id: "", token: "", imageB64: "" };
  rand = Math.floor(Math.random() * 1000000) + 8;

  constructor(
    private router: Router,
    public navCtrl: NavController,
    public toastController: ToastController,
    private postPvdr: PostProvider,
    private camera: Camera,
    private crop: Crop,
    private transfer: FileTransfer,
    private alertCtrl: AlertController,
    private storage: Storage,
    private file: File
    
    ) { 
    }

   
  goTabs(){
    this.router.navigate(['tabs']);
  }
  goHelp(){
    //window.location.href="http://greenthumbtrade.com/help";
    this.navCtrl.navigateForward((['tabs/help']), { animated: false, });
  }
  goDescribe(){
    this.storage.set("greenthumb_title", this.greenthumb_title);
    if(this.greenthumb_title == ''){
      this.presentToast();
    }
    else{
      this.navCtrl.navigateForward((['describeitem']), { animated: false, });
    }
    
  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Please Provide Title of the Item.',
      duration: 3000
    });
    toast.present();
  }
  
  ngOnInit() {
    this.photos = [];
  }

  ionViewWillEnter(){
    this.plotData();
  }

  plotData(){
    this.storage.get("greenthumb_user_id").then((u_id) => {

      this.user_id = u_id;});
  }


  deletePhoto(index) {
    this.alertCtrl.create({
      header: "Sure you want to delete this photo? There is NO undo!",
      message: "",
      buttons: [
        {
          text: "No",
          handler: () => {
            console.log("Disagree clicked");
          }
        },
        {
          text: "Yes",
          handler: () => {
            console.log("Agree clicked");
            this.photos.splice(index, 1);
          }
        }
      ]
    }).then(res => {
      res.present();
    });
    
  }

  getImage() {
    console.log("getImage function");

    this.pickImage(this.camera.PictureSourceType.PHOTOLIBRARY);



    // const cameraOptions: CameraOptions = {
    //   quality: 70,
    //   destinationType: this.camera.DestinationType.DATA_URL,
    //   sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
    //   saveToPhotoAlbum: false,
    //   correctOrientation: true,
    //   allowEdit:true,
    //   targetHeight: 150,
    // }
    // this.camera.getPicture(cameraOptions).then((imageData) => {
    //   this.base64Image = 'data:image/jpeg;base64,' + imageData;
    //   this.photos.push(this.base64Image);
    //     this.photos.reverse();
    //     this.sendData(imageData);
    // }, (err) => {
      
    //   console.log(err);
    // });
  }

  takePhoto() {
    console.log("coming here");

    this.pickImage(this.camera.PictureSourceType.CAMERA);

    // const options: CameraOptions = {
    //   quality: 50,
    //   destinationType: this.camera.DestinationType.DATA_URL,
    //   encodingType: this.camera.EncodingType.JPEG,
    //   mediaType: this.camera.MediaType.PICTURE,
    //   correctOrientation: true,
    //   targetWidth: 450,
    //   targetHeight: 450,
    //   saveToPhotoAlbum: false
    // };

    // this.camera.getPicture(options).then(
    //   imageData => {
    //     this.base64Image = "data:image/jpeg;base64," + imageData;
    //     this.photos.push(this.base64Image);
    //     this.photos.reverse();
    //     this.sendData(imageData);
    //   },
    //   err => {
    //     console.log(err);
    //   }
    // );
  }



  pickImage(sourceType) {
    const options: CameraOptions = {
      quality: 100,
      sourceType: sourceType,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      // let base64Image = 'data:image/jpeg;base64,' + imageData;
      console.log("imageData before:"+imageData);
      this.cropImage(imageData)
    }, (err) => {
      // Handle error
    });
  }

  // async selectImage() {
  //   const actionSheet = await this.actionSheetController.create({
  //     header: "Select Image source",
  //     buttons: [{
  //       text: 'Load from Library',
  //       handler: () => {
  //         this.pickImage(this.camera.PictureSourceType.PHOTOLIBRARY);
  //       }
  //     },
  //     {
  //       text: 'Use Camera',
  //       handler: () => {
  //         this.pickImage(this.camera.PictureSourceType.CAMERA);
  //       }
  //     },
  //     {
  //       text: 'Cancel',
  //       role: 'cancel'
  //     }
  //     ]
  //   });
  //   await actionSheet.present();
  // }

  cropImage(fileUrl) {
    console.log("fileURL:"+fileUrl);
    this.crop.crop(fileUrl, { quality: 50 })
      .then(
        newPath => {
          this.showCroppedImage(newPath.split('?')[0])
        },
        error => {
          alert('Error cropping image' + error);
        }
      );
  }

  showCroppedImage(ImagePath) {
    this.isLoading = true;
    var copyPath = ImagePath;
    var splitPath = copyPath.split('/');
    var imageName = splitPath[splitPath.length - 1];
    var filePath = ImagePath.split(imageName)[0];

    this.file.readAsDataURL(filePath, imageName).then(base64 => {
      this.croppedImagepath = base64;
      this.base64Image = base64;
      this.photos.push(this.base64Image);
      this.photos.reverse();
      this.sendData(base64);
      this.isLoading = false;
    }, error => {
      alert('Error in showing image' + error);
      this.isLoading = false;
    });
  }






  sendData(imageData) {

    const fileTransfer: FileTransferObject = this.transfer.create();

    this.rand = Math.floor(Math.random() * 1000000) + 8;

    this.fileName = "greenthumb_"+this.rand+"_"+this.user_id+".jpg";

    this.userData.imageB64 = imageData;
    this.userData.user_id = "1";
    this.userData.token = "222";
    //console.log(this.userData);
   // console.log("image:"+this.base64Image);

    let options: FileUploadOptions = {
      fileKey: 'photo',
      fileName: this.fileName,
      chunkedMode: false,
      httpMethod: 'post',
      mimeType: "image/jpeg",
      headers: {}
    }

    fileTransfer.upload(this.base64Image, this.postPvdr.myServer()+'/greenthumb/images/posted_items/images_upload.php', options)
    .then((data) => {
      

      this.pictures.push(new ItemPicture(this.fileName));
      this.storage.set("greenthumb_itempost_pic",this.pictures);

    }, (err) => {
      console.log(err);
      alert("Please check your internet connection and try again.");
    });
   
    // this.authService.postData(this.userData, "userImage").then(
    //   result => {
    //     this.responseData = result;
    //   },
    //   err => {
        
    //   }
    // );
  }




}
