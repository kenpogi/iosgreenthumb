import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { IonSlides } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { NavController, ModalController } from '@ionic/angular';
import { LiveprofilePage } from '../liveprofile/liveprofile.page';
import { RatePage } from '../rate/rate.page';
import { PostProvider } from '../../providers/credential-provider';
import { Conversations, Users_search } from '../shared/model/chat.model';
import { Announcement } from '../shared/model/announcement.model';
import { SystemNotif } from '../shared/model/systemnotif.model';

import { IonContent } from '@ionic/angular';

@Component({
  selector: 'app-tab5',
  templateUrl: './tab5.page.html',
  styleUrls: ['./tab5.page.scss'],
})
export class Tab5Page implements OnInit {

  @ViewChild('slides', { static: true }) slider: IonSlides;

  @ViewChild(IonContent, { static: false }) content: IonContent;

  segment = 0;

  search_input: string;

  searchData: Users_search[] = [];
  
  systemNotif: SystemNotif[] = [];

  myConversations: Conversations[] = [];

  announcementAll: Announcement[] = [];

  myClass : boolean = false;

  login_user_id : string = '';

  loader: boolean = true;

  notif_message : number;
  notif_notif : number;
  notif_announcement : number;

  constructor(
    private router: Router,
    public modalController: ModalController,
    private storage: Storage,
    private postPvdr: PostProvider,
    public navCtrl: NavController
    ) { }

    async goRate(rate_user_id, notif_id){
      const modal = await this.modalController.create({
              component: RatePage,
              cssClass: 'ratingmodalstyle',
              componentProps: {
                rate_user_id,
                login_user_id : this.login_user_id,
                notif_id
              }

            });
        modal.onDidDismiss()
        .then((data) => {
          console.log("data"+JSON.stringify(data));
          if(data['data']){
            console.log("true pag-rate");
            //document.getElementById("notif"+notif_id).style.visibility = "hidden";
            //this.router.navigate(['tabs/offers']);

            let navigationExtras: any = {
              queryParams: { fromRate: true}
          };
          this.navCtrl.navigateForward(['tabs/offers'], navigationExtras);
          }
          else{
            console.log("wala mag-rate");
          }
        });
        
        return await modal.present();
    }
    goBack(){
      window.history.back();
    }
    goHelp(){
      this.router.navigate(['tabs/help']);
    }

    async segmentChanged() {
      await this.slider.slideTo(this.segment).then(()=>{
        console.log("slide to activated");
        this.content.scrollToTop();
       // this.scrollToTop();
      });
      
    }
  
    async slideChanged() {
      this.segment = await this.slider.getActiveIndex();
      this.storage.set("greenthumb_segment", this.segment);
      if(this.segment==1){
        let body21 = {
          action : 'UpdateNotif',
          type : 2,
          user_id : this.login_user_id
        };
        this.postPvdr.postData(body21, 'system_notification.php').subscribe(data => {
          
          if(data.success){
            this.plotNotif();
          }
        });

        
      }
      else if(this.segment==2){
        let body21 = {
          action : 'UpdateNotif',
          type : 3,
          user_id : this.login_user_id
        };
        this.postPvdr.postData(body21, 'system_notification.php').subscribe(data => {
         
          if(data.success){
            this.plotNotif();
          }
        });
        
      }

    }

  toAccount(){
    this.router.navigate(['myaccount']);
  }
  goMessage(val, item_id){
   // this.router.navigateByUrl('/message/'+val);
    this.navCtrl.navigateRoot(['/message/'+val+"-separator-"+item_id+"-separator-"+'0']);
  }

  

  ngOnInit() {
   // this.plotData();
   // this.initializeData();
   
  }

  ionViewWillEnter() {
    console.log("ionviewwillenter in tab5 conversation");
    
    this.plotData();

    this.storage.get("greenthumb_segment").then((seg)=>{
      if(seg!=null){
        this.segment = seg;
      } 
    });
  }

  ionViewDidEnter()
{
  console.log("ionViewDidEnter entered");
}



  filterData(ev: any){

    

    
    var val = ev.target.value;



    if(val && val.trim().length > 0 ){
      // this.searchData = this.searchData.filter((item) => {
      //   console.log("item:"+JSON.stringify(item));
      //   return (item.nickname.toLowerCase().indexOf(val.toLowerCase()) >-1);
      this.searchData = [];
      this.myClass = true;
      this.initializeData(val);
      // })
    }
    else if(val.length == 0){
      this.searchData = [];
      this.myClass = false;
    }
    else{
      this.searchData = [];
      this.myClass = false;
    }
    console.log("val.trim():"+val.trim());
    console.log("val.length():"+val.length);

  }

  searchedConvo(val){
    //this.router.navigateByUrl('/message/'+val);
    
    this.navCtrl.navigateRoot(['/message/'+val]);
  }


  searchProfile(){

    this.storage.get("user_id").then((user_id) => {
      let body = {
        action : 'getuserdata',
        user_id : user_id
      };
      this.postPvdr.postData(body, 'credentials-api.php').subscribe(async data => {
        if(data.success){


          // for(const key in data.result){
          //   pictureProfile = (data.result[key].profile_photo == '') ? '' :
          //   "http://"+this.postPvdr.myServer()+"/brixy-live/images/"+data.result[key].profile_photo;
          //   livestream_users.push(new UserLive(
          //     data.result[key].user_id,
          //     data.result[key].stream_id,
          //     data.result[key].fname,
          //     data.result[key].lname,
          //     data.result[key].city,
          //     pictureProfile,
          //     data.result[key].user_level,
          //     data.result[key].badge_name,
          //     data.result[key].viewers,
          //     data.result[key].broadcast_topic
          //     )
              
          //     );
          // }

        } 

      });
    });
  }

  async viewProfile(user_id){

    const modal = await this.modalController.create({
            component: LiveprofilePage,
            cssClass: 'liveprofilemodalstyle',
            componentProps: { 
              liveStreamProfileId: user_id
            }
          });
      
      
      
          return await modal.present();

  }

  plotData(){


    this.loader = true;

    // for all announcement

    this.storage.get('greenthumb_user_id').then((user_id) => {
      this.login_user_id = user_id;

      let body322= {
        action : 'getAllAnnouncement',
        user_id : user_id
      };
      //console.log("storyalang:"+JSON.stringify(body321));
      this.postPvdr.postData(body322, 'system_notification.php').subscribe(data => {
      

        if(data.success){
          
          const announcementsAllList: Announcement[] = [];


          for(const key in data.result){


            announcementsAllList.push(new Announcement(
              data.result[key].id,
              data.result[key].title,
              data.result[key].announcment,
              data.result[key].date,
              data.result[key].time
              ));
          }

          this.announcementAll = announcementsAllList;

        }
      });

      let body3 = {
        action : 'getMySystemNotifications',
        user_id : user_id
      };
      this.postPvdr.postData(body3, 'system_notification.php').subscribe(data => {
        
        if(data.success){


          const notificationList: SystemNotif[] = [];
          for(const key in data.result){
            
            notificationList.push(new SystemNotif(
              data.result[key].id,
              data.result[key].notification_message,
              data.result[key].date,
              data.result[key].time,
              data.result[key].with_rate,
              data.result[key].rate_user_id,
              ));

          }

          this.systemNotif = notificationList;
          
        }
      });


      this.plotNotif();


    });



    
    this.myConversations = [];




    this.storage.get("greenthumb_user_id").then((user_id) => {

      this.login_user_id = user_id;

      let body = {
        action : 'getConversation',
        user_id : user_id
      };
       console.log("body of plotdata:"+JSON.stringify(body));
      this.postPvdr.postData(body, 'messages.php').subscribe(async data => {
        if(data.success){

          var myNickname = "";

           for(const key in data.result){

            if(data.result[key].myNickname == ""){
              if(data.result[key].login_type_id == "3"){
                myNickname = data.result[key].mobile_num
              }
              else if(data.result[key].login_type_id == "4"){
                myNickname = data.result[key].email
              }
            }
            else{
              myNickname = data.result[key].myNickname;
            }

            this.myConversations.push(new Conversations(
              data.result[key].user_id,
              data.result[key].id,
              myNickname,
              (data.result[key].profile_photo == '') ? '' :
            this.postPvdr.myServer()+"/greenthumb/images/"+data.result[key].profile_photo,
              data.result[key].item_id,
              data.result[key].item_name,
              data.result[key].message,
              data.result[key].date_sent,
              data.result[key].time_sent,
              data.result[key].notif_status,
              data.result[key].user_sent
              )
              
            );
          }

          this.loader = false;
        } 

      });
    });

  }

  plotNotif(){
    let body21 = {
      action : 'getNotifMessage',
      type : 1,
      user_id : this.login_user_id
    };
    this.postPvdr.postData(body21, 'messages.php').subscribe(data => {
      console.log(data);
      if(data.success){

        this.notif_message = parseInt(data.notif_message);
        this.notif_notif = parseInt(data.notif_notif);
        this.notif_announcement = parseInt(data.notif_announcement);
        //console.log("notif_message:"+ data.notif_message +":"+data.notif_notif+':'+data.notif_announcement);
       
      }
    });
  }

  initializeData(val){

    this.storage.get("user_id").then((user_id) => {

      let body = {
        action : 'getUsersToChat',
        nickname : val,
        user_id: user_id
      };
     
      this.postPvdr.postData(body, 'messages.php').subscribe(async data => {
        if(data.success){

          var myNickname = "";

           for(const key in data.result){


            if(data.result[key].myNickname == ""){
              if(data.result[key].login_type_id == "3"){
                myNickname = data.result[key].mobile_num
              }
              else if(data.result[key].login_type_id == "4"){
                myNickname = data.result[key].email
              }
            }
            else{
              myNickname = data.result[key].myNickname;
            }

            this.searchData.push(new Users_search(
              data.result[key].user_id,
              myNickname,
              (data.result[key].profile_photo == '') ? '' :
           
              this.postPvdr.myServer()+"/brixy-live/images/"+data.result[key].profile_photo
              )
              
            );
          }

        } 

      });

    });

    // this.searchData = [
    //   {
    //     "name": "Argie",
    //     "code": "65"
    //   },
    //   {
    //     "name": "Argie1",
    //     "code": "234"
    //   },
    //   {
    //     "name": "Argie2",
    //     "code": "23"
    //   },
    //   {
    //     "name": "Diane",
    //     "code": "345"
    //   }
    //   ,
    //   {
    //     "name": "Diane2",
    //     "code": "4534"
    //   },
    //   {
    //     "name": "Diane3",
    //     "code": "fhf"
    //   },
    //   {
    //     "name": "Lyanna",
    //     "code": "as"
    //   }
    //   ,
    //   {
    //     "name": "Lyanna1",
    //     "code": "fdgfd"
    //   },
    //   {
    //     "name": "Lyanna2",
    //     "code": "mycsdfdsode"
    //   },
    //   {
    //     "name": "Lyanna3",
    //     "code": "sdfg"
    //   }

    // ]
  }

}
