import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import {SuperTabsModule} from '@ionic-super-tabs/angular';

import { IonicModule } from '@ionic/angular';

import { TabsPage } from './tabs.page';
import { DirectivesModule } from '../directives/directives.module';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children: [
      {
        path: 'tab1',
        children: [
          {
            path: '',
            loadChildren: '../tab1/tab1.module#Tab1PageModule'
          }
        ]
      },
      {
        path: 'itemposted',
        children: [
          {
            path: '',
            loadChildren: '../itemposted/itemposted.module#ItempostedPageModule'
          }
        ]
      },
      {
        path: 'communityforum',
        children: [
          {
            path: '',
            loadChildren: '../communityforum/communityforum.module#CommunityforumPageModule'
          }
        ]
      },
      {
        path: 'offers',
        children: [
          {
            path: '',
            loadChildren: '../offers/offers.module#OffersPageModule'
          }
        ]
      },
      {
        path: 'viewprofile',
        children: [
          {
            path: '',
            loadChildren: '../viewprofile/viewprofile.module#ViewprofilePageModule'
          }
        ]
      },
      {
        path: 'userpolicy',
        children: [
          {
            path: '',
            loadChildren: '../userpolicy/userpolicy.module#UserpolicyPageModule'
          }
        ]
      },
      {
        path: 'help',
        children: [
          {
            path: '',
            loadChildren: '../help/help.module#HelpPageModule'
          }
        ]
      },
      {
        path: 'sellersifollow',
        children: [
          {
            path: '',
            loadChildren: '../sellersifollow/sellersifollow.module#SellersifollowPageModule'
          }
        ]
      },
      {
        path: 'myaccount',
        children: [
          {
            path: '',
            loadChildren: '../myaccount/myaccount.module#MyaccountPageModule'
          }
        ]
      },
      {
        path: 'tab3',
        children: [
          {
            path: '',
            loadChildren: '../tab3/tab3.module#Tab3PageModule'
          }
        ]
       },
       {
        path: 'categories',
        children: [
          {
            path: '',
            loadChildren: '../categories/categories.module#CategoriesPageModule'
          }
        ]
      },
      {
        path: 'tab5',
        children: [
          {
            path: '',
            loadChildren: '../tab5/tab5.module#Tab5PageModule'
          }
        ]
      },
      {
        path: '',
        redirectTo: '/tabs/tab1',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/tab1',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SuperTabsModule,
    DirectivesModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TabsPage]
})
export class TabsPageModule {}
