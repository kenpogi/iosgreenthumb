import { Component, OnInit, NgZone, ViewChild  } from '@angular/core';
import { Platform,  AlertController, NavController, IonTabs} from '@ionic/angular';
import { Router } from '@angular/router';
import { AppMinimize } from '@ionic-native/app-minimize/ngx';
import { LocalNotifications, ELocalNotificationTriggerUnit } from '@ionic-native/local-notifications/ngx';
import { PostProvider } from '../../providers/credential-provider';
import { Storage } from '@ionic/storage';
import { Vibration } from '@ionic-native/vibration/ngx';
import { SwipeTabDirective } from '../directives/swipe-tab.directive';


@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.page.html',
  styleUrls: ['./tabs.page.scss'],
})
export class TabsPage implements OnInit {

  @ViewChild(SwipeTabDirective, {static: false}) swipeTabDirective: SwipeTabDirective;
  @ViewChild('myTabs', {static: false}) tabRef: IonTabs;

  subscription: any;

  notif_count : number;

  constructor(
    public platform: Platform, 
    public alertCtrl: AlertController, 
    private router: Router,
    private appMinimize: AppMinimize,
    private localNotifications: LocalNotifications,
    private postPvdr: PostProvider,
    private storage: Storage,
    private zone: NgZone,
    public navCtrl: NavController,
    private vibration: Vibration
    ) { 
    // this.platform.backButton.subscribe(() => {
    //   console.log("you got it all SM");
    // });
  }
  

  ngOnInit() {
    this.plotData();
  }

  goTab3(){
    //this.router.navigate(['tab3']);
    this.navCtrl.navigateRoot(['tab3']);
  }

  ionTabsDidChange($event) {
    console.log('[TabsPage] ionTabsDidChange, $event: ', $event);
    this.swipeTabDirective.onTabInitialized($event.tab);
  }

  onTabChange($event) {
    console.log('[TabsPage] onTabChange, $event: ', $event);
    this.tabRef.select($event);
  }

  ionViewDidEnter(){

    this.subscription = this.platform.backButton.subscribe(()=>{
      // navigator['app'].exitApp();
      this.router.navigate(['tabs']);
      console.log("hello darkness my old friend");

      this.alertCtrl.create({
       header: 'Greenthumb',
       message: 'Do you want to minimize the app?',
       buttons: [{
           text: 'Cancel',
           role: 'cancel',
           handler: () => {
               console.log('Application exit prevented!');
           }
       },{
           text: 'Minimize',
           handler: () => {
             //navigator['app'].exitApp(); // Close this application
             this.appMinimize.minimize();
           }
       }]
     }).then(res => {
       res.present();
     });

   });
  }

  plotData(){
    

    


    setInterval(()=>{                           //<<<---using ()=> syntax
        this.zone.run(()=>{

        //console.log("magbalik ka na naman");
        this.storage.get('greenthumb_user_id').then((user_id) => {



          let body2 = {
            action : 'getNewMessage',
            type : 1,
            user_id : user_id
          };
          console.log("getNewMessage:"+JSON.stringify(body2));
          this.postPvdr.postData(body2, 'messages.php').subscribe(data => {
            console.log(data);
            if(data.success){

              for(const key in data.result){



                if(data.result[key].user_sent != user_id){

                  this.localNotifications.schedule({
                    id: data.result[key].id,
                    title: "New Message",
                    text: data.result[key].myNickname+": "+data.result[key].message,
                    trigger: {
                      in: 1,
                      unit: ELocalNotificationTriggerUnit.SECOND,
                    },
                  });

                  this.vibration.vibrate(1000);
                  this.localNotifications.on('click').subscribe(()=>{


                    this.navCtrl.navigateRoot(['/message/'+data.result[key].user_id+"-separator-"+
                                                            data.result[key].item_id+"-separator-"+'0']);


                  });

                }
              }
             
            }
          });

          let body21 = {
            action : 'getNotifMessage',
            type : 1,
            user_id : user_id
          };
          this.postPvdr.postData(body21, 'messages.php').subscribe(data => {
            console.log(data);
            if(data.success){

              this.notif_count = parseInt(data.notif_message)
                                  + parseInt(data.notif_notif)
                                  + parseInt(data.notif_announcement);
              //console.log("notif_message:"+ data.notif_message +":"+data.notif_notif+':'+data.notif_announcement);
             
            }
          });

             // for announcement

            // console.log("getannouncement")
             let body4 = {
              action : 'getPushAnnouncement',
              user_id : user_id
            };
            this.postPvdr.postData(body4, 'system_notification.php').subscribe(data => {
              
              if(data.success){


                for(const key in data.result){
  
                    this.localNotifications.schedule({
                      id: data.result[key].id,
                      title: data.result[key].title,
                      text: data.result[key].announcment,
                      trigger: {
                        in: 1,
                        unit: ELocalNotificationTriggerUnit.SECOND,
                      },
                    });
  
                    this.vibration.vibrate(1000);
                    // This is for redirecting when the user click the notification
                    // this.localNotifications.on('click').subscribe(()=>{
  
                    //   this.navCtrl.navigateRoot(['/message/'+data.result[key].user_id]);
  
  
                    //  });

                  
                }


              }

            });

            // for system notifications

           // console.log("system notif");
           let body3 = {
            action : 'getSystemNotifications',
            user_id : user_id
          };
          this.postPvdr.postData(body3, 'system_notification.php').subscribe(data => {
            
            if(data.success){


              for(const key in data.result){


                  this.localNotifications.schedule({
                    id: data.result[key].id,
                    title: "Greenthumb Notification",
                    text: data.result[key].notification_message,
                    trigger: {
                      in: 1,
                      unit: ELocalNotificationTriggerUnit.SECOND,
                    },
                  });

                  this.vibration.vibrate(1000);
                  // This is for redirecting when the user click the notification
                  //this.localNotifications.on('click').subscribe(()=>{

                  //   this.navCtrl.navigateRoot(['/message/'+data.result[key].user_id]);


                  // });

                
              }


            }

          });

      
          }); // end of storage
  
       });// end of zone
        

    }, 5000);

    
      

    

    
  }

  ionViewWillLeave(){
      this.subscription.unsubscribe();
  }

}
