import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-termsconditions',
  templateUrl: './termsconditions.page.html',
  styleUrls: ['./termsconditions.page.scss'],
})
export class TermsconditionsPage implements OnInit {

  checked: boolean = false;

  constructor(private router: Router) { }

  goLive(){
    this.router.navigate(['readytogolive']);
  }
  goBack(){
    this.router.navigate(['tab3']);
  }

  agreeTermed(){

    this.checked = !this.checked;
    console.log("this.checked:"+this.checked);
  }

  ngOnInit() {
  }

}
