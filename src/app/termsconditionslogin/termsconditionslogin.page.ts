import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserpolicyPage } from '../userpolicy/userpolicy.page';
import { ModalController } from '@ionic/angular';
import { PrivacyPage } from '../privacy/privacy.page';
import { PrivacypolicyPage } from '../privacypolicy/privacypolicy.page';

@Component({
  selector: 'app-termsconditionslogin',
  templateUrl: './termsconditionslogin.page.html',
  styleUrls: ['./termsconditionslogin.page.scss'],
})
export class TermsconditionsloginPage implements OnInit {

  checked: boolean = false;

  constructor(private router: Router,private modalController: ModalController,) { }

  goBack(){
    this.router.navigate(['ads']);
  }
  agreeTermed(){
    this.router.navigate(['choose']);
  }
  ngOnInit() {
  }

  agreeTermedChecked(){

    this.checked = !this.checked;
    console.log("this.checked:"+this.checked);
  }

 terms_of_service(){
      
  }

  async user_policy(){
    const modal = await this.modalController.create({
            component: UserpolicyPage,
            cssClass: ''
          });
          return await modal.present();

  }
  async privacy_policy(){
    const modal = await this.modalController.create({
            component: PrivacypolicyPage,
            cssClass: ''
          });
          return await modal.present();
  }

}
