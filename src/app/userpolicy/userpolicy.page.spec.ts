import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UserpolicyPage } from './userpolicy.page';

describe('UserpolicyPage', () => {
  let component: UserpolicyPage;
  let fixture: ComponentFixture<UserpolicyPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserpolicyPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UserpolicyPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
