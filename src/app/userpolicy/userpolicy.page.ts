import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-userpolicy',
  templateUrl: './userpolicy.page.html',
  styleUrls: ['./userpolicy.page.scss'],
})
export class UserpolicyPage implements OnInit {

  constructor(private modalController: ModalController,) { }

  ngOnInit() {
  }

  // goBack(){
  //   this.modalController.dismiss();
  // }
  goBack(){
    window.history.back();
  }

}
