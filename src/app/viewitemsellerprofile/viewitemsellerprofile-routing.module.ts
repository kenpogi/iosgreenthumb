import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ViewitemsellerprofilePage } from './viewitemsellerprofile.page';

const routes: Routes = [
  {
    path: '',
    component: ViewitemsellerprofilePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ViewitemsellerprofilePageRoutingModule {}
