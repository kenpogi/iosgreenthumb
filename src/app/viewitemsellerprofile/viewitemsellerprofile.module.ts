import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ViewitemsellerprofilePageRoutingModule } from './viewitemsellerprofile-routing.module';
import { StarRatingModule } from 'ionic4-star-rating';
import { ViewitemsellerprofilePage } from './viewitemsellerprofile.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    StarRatingModule,
    ViewitemsellerprofilePageRoutingModule
  ],
  declarations: [ViewitemsellerprofilePage]
})
export class ViewitemsellerprofilePageModule {}
