import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ViewitemsellerprofilePage } from './viewitemsellerprofile.page';

describe('ViewitemsellerprofilePage', () => {
  let component: ViewitemsellerprofilePage;
  let fixture: ComponentFixture<ViewitemsellerprofilePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewitemsellerprofilePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ViewitemsellerprofilePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
