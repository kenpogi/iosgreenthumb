import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Storage } from '@ionic/storage';
import { NavController,  ModalController } from '@ionic/angular';
import { PostProvider } from '../../providers/credential-provider';
import { Item } from '../shared/model/item.model';
import { ReportuserPage } from '../reportuser/reportuser.page';

@Component({
  selector: 'app-viewitemsellerprofile',
  templateUrl: './viewitemsellerprofile.page.html',
  styleUrls: ['./viewitemsellerprofile.page.scss'],
})
export class ViewitemsellerprofilePage implements OnInit {

  user_profile_id : string = "";

  itemList: Item[] = [];
  login_type : any;

  seller_name : string = "";

  user_rating: string = "";

  picture: any;
  complete_pic: any;

  statusFollow: boolean = false;
  seller_location : string = "";

  totalFollower: number;
  totalFollowing: number;

  totalItems : number;

  ownProfile : boolean = false;

  login_user_id : string = "";

  loader: boolean = true;

  constructor(
    private router: Router,
    private postPvdr: PostProvider,
    public modalController: ModalController,
    private storage: Storage,
    public navCtrl: NavController,
    private route: ActivatedRoute,
  ) {
    
    this.route.queryParams.subscribe(params => {
      this.user_profile_id = params["user_profile_id"];
      console.log("user_profile_id:"+this.user_profile_id);
    });
   }

  goBack(){
    // this.router.navigate(['help']);
    window.history.back();
  }
  goProductview(item_id){
    // this.router.navigate(['productview']);
     let navigationExtras: any = {
         queryParams: { item_id}
     };
     this.navCtrl.navigateForward(['productview'], navigationExtras);
   }
  ngOnInit() {
    
  }

  ionViewWillEnter(){
    this.plotData();
  }

  goHelp(){
    //window.location.href="http://greenthumbtrade.com/help";
    this.router.navigate(['tabs/help']);
  }

  async reportUser() {
        const modal = await this.modalController.create({
          component: ReportuserPage,
          cssClass: 'liveprofilemodalstyle',
          componentProps: { 
            reportedId : this.user_profile_id,
            reportedName: this.seller_name,
          }
        });
    
    
        modal.onDidDismiss()
        .then((data) => {
          //const user = data['data']; // Here's your selected user!
          console.log("dismiss of liveprofilepage modal")
          //this.plotFollowStatus();
        });
    
    
        return await modal.present();
      }

  plotData(){

    this.loader = true;

    let body = {
      action : 'getuserdata',
      user_id : this.user_profile_id
    };

    this.postPvdr.postData(body, 'credentials-api.php').subscribe(async data => {
      
      if(data.success){

        this.seller_name = data.result.nickname;
        this.seller_location = data.result.city ;

        this.picture = data.result.photo;
        this.complete_pic = this.postPvdr.myServer()+"/greenthumb/images/"+this.picture;
        this.user_rating = data.result.user_rating;

        this.login_type = data.result.login_type_id;
       // console.log("krezi:"+data.result.username);
      }

    });

    



    let body223 = {
      action : 'getFollow',
      type : 1,
      user_id : this.user_profile_id
    };
    this.postPvdr.postData(body223, 'followers.php').subscribe(data => {
      
      if(data.success){
        this.totalFollower = data.result;
         
      }
    });

    let body432 = {
      action : 'getFollow',
      type : 2,
      user_id : this.user_profile_id
    };
    this.postPvdr.postData(body432, 'followers.php').subscribe(data => {
      
      if(data.success){
        this.totalFollowing = data.result;
      }
    });



    this.storage.get("greenthumb_user_id").then((user_id) => {
      this.login_user_id = user_id;


      this.ownProfile = this.login_user_id == this.user_profile_id ? true : false;

      let body3 = {
        action : 'followerStatus',
        followed_by : user_id,
        user_id : this.user_profile_id

      };
      this.postPvdr.postData(body3, 'followers.php').subscribe(async data => {
        
        if(data.success){
          if(data.result=="1"){
            this.statusFollow = true;
          }
        }
      });

    });

    let body2 = {
      action : 'getSellerPost',
      user_id : this.user_profile_id,
    };
    console.log(JSON.stringify(body2));
    this.postPvdr.postData(body2, 'post_item.php').subscribe(async data => {
      
      if(data.success){

        const items: Item[] = [];

        //var pictureProfile: string = '';

        var x = 0;

        for(const key in data.result){
          items.push(new Item(
            data.result[key].item_id,
            data.result[key].user_id,
            data.result[key].username,
            (data.result[key].profile_photo == '') ? '' :
              this.postPvdr.myServer()+"/greenthumb/images/"+data.result[key].profile_photo,
            data.result[key].price,
            data.result[key].title,
            data.result[key].stocks_id,
            data.result[key].others_stock,
            parseInt(data.result[key].quantity),
            data.result[key].category,
            data.result[key].location,
            data.result[key].saveStatus,
            data.result[key].status,
            this.postPvdr.myServer()+"/greenthumb/images/posted_items/images/"+data.result[key].item_photo,
            data.result[key].user_rating,
            data.result[key].date,
            data.result[key].time
            )
          );

          x++;

        }
        this.itemList = items;
        this.totalItems = x;
        this.loader = false;
      }
    });
     
  }

  follow(){

     
        let body = {
          action : 'follow',
          user_id : this.user_profile_id,
          followed_by : this.login_user_id,
          status : this.statusFollow
        };

        this.postPvdr.postData(body, 'followers.php').subscribe(data => {
          console.log(data);
          if(data.success){
            
              //this.followClass = "myHeartFollow";
              this.statusFollow = !this.statusFollow; 
              if(this.statusFollow){

                let body2 = {
                  action : 'addNotification',
                  user_id : this.user_profile_id,
                  followed_by: this.login_user_id,
                  notification_message : "has followed you",
                  status : this.statusFollow
                };
        
                this.postPvdr.postData(body2, 'system_notification.php').subscribe(data => {

                })
              }
          }
        });
   
  }

  goSellerprof(user_profile_id){
    // this.router.navigate((['viewitemsellerprofile']));
    let navigationExtras: any = {
     queryParams: { user_profile_id}
  };
     this.navCtrl.navigateForward(['viewitemsellerprofile'], navigationExtras);
   }

   saveThis(item_id){
   
    this.storage.get("greenthumb_user_id").then((user_id) => {
      let body = {
        action : 'saveItem',
        item_id : item_id,
        user_id : user_id,
      };

      this.postPvdr.postData(body, 'save_item.php').subscribe(data => {

        if(data.success){
          if(data.saveStatus == '1'){
            console.log('1 ko');
            var element = document.getElementById("class"+item_id);
            element.classList.remove("myHeartUnSave");
            element.classList.add("myHeartSave");
            console.log("classList:"+element.classList);
          }
          else{
            console.log("0 ko");
            var element = document.getElementById("class"+item_id);
            element.classList.remove("myHeartSave");
            element.classList.add("myHeartUnSave");
          }
        }
        
      });
      
    });
  }

}
