import { Component, AfterViewInit, OnInit} from '@angular/core';
import { Router, ActivatedRoute  } from '@angular/router';
import { ModalController, NavController, LoadingController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Socket } from 'ngx-socket-io';
import { AllgiftsPage } from '../allgifts/allgifts.page';
import { LiveprofilePage } from '../liveprofile/liveprofile.page';
import { UserLivestream } from '../shared/model/user.model';
import { PostProvider } from '../../providers/credential-provider';

declare var WebRTCAdaptor: any;

@Component({
  selector: 'app-viewlive',
  templateUrl: './viewlive.page.html',
  styleUrls: ['./viewlive.page.scss'],
})
export class ViewlivePage implements AfterViewInit, OnInit {

  livestreamId: string;
  stream_key: string;

  login_user_id : string;
  login_nickname: string;
  login_profile_photo : string;

  message = '';

  messages = [];

  vip_package = "";

  viewersPic = [];

  livestream: UserLivestream[];

  user_id: number;
  fname: string;
  lname: string;
  city: string;
  user_photo: string;
  user_level: number;
  followers: number;
  following: number;
  
  followClass = "myHeartFollow";

  status: boolean = false;


  loader: any;

  vip_packageOfUser: string = "";


  constructor(
    private router: Router,
    public modalController: ModalController,
    public activatedRoute: ActivatedRoute,
    private loadingCtrl : LoadingController,
    private storage: Storage,
    private socket: Socket,
    private postPvdr: PostProvider,
    public navCtrl: NavController
    ) { }


    ngOnInit() {
      var paramId = this.activatedRoute.snapshot.paramMap.get("id");
      console.log("paramId:"+paramId);
      var res = paramId.split("_");
      this.livestreamId = res[0];
      this.stream_key = res[1];

      console.log("livestreamId:"+this.livestreamId);
      console.log("stream_keykeykey:"+this.stream_key);
     
      this.plotDetails();
      this.start(this.stream_key);

      this.checkSubscription();
      
    }

    async loading(image){
      //show loading
      this.loader = await this.loadingCtrl.create({
        message: '<img src="'+image+'" style="height: 25px" height="auto" alt="loading...">',
        translucent: true,
        showBackdrop: false,
        spinner: null,
        duration: 2000
      });
      this.loader.present();
    }

    checkSubscription(){

      

        let body2 = {
          action : 'checkSubscription',
          user_id : this.livestreamId
        };
        this.postPvdr.postData(body2, 'subscription.php').subscribe(data => {
          
          if(data.success){
            for(const key in data.result){
              this.vip_package = data.result[key].vip_package_id;
            }
            
          }
        });
  
    }


    ngAfterViewInit() {
      
    }


  openGifts(){
    this.router.navigate(['allgifts']);
  }
  goTab5(){
    this.navCtrl.navigateForward(['/message/'+this.livestreamId]);
  }
  goHome(){
    this.router.navigate(['tabs']);
  }
  goStore(){
    this.router.navigate(['tab4']);
  }

  ionViewWillLeave() {

    console.log("will leave message and disconnect to socket")
    this.socket.disconnect();
  }


  plotDetails(){




    console.log("initialize plot details");

    let body = {
      action : 'getUser_liveData',
      user_id : this.livestreamId
    };
    this.postPvdr.postData(body, 'credentials-api.php').subscribe(async data => {
      
      if(data.success){


        for(const key in data.result){

            this.user_id = data.result[key].user_id;
            this.fname = data.result[key].fname;
            this.lname = data.result[key].lname;
            this.city = data.result[key].city;
            this.user_photo = (data.result[key].profile_photo == '') ? '' :
            this.postPvdr.myServer()+"/brixy-live/images/"+data.result[key].profile_photo;
            this.user_level = data.result[key].user_level;
            this.followers = data.result[key].followers;
            this.following = data.result[key].following;
         
        }

        

      }
    });


    

    this.plotFollowStatus();



    

    this.storage.get("user_id").then( (user_id) => {

      this.login_user_id = user_id;



      let body2 = {
        action : 'getuserdata',
        user_id : user_id
      };
      this.postPvdr.postData(body2, 'credentials-api.php').subscribe(async data => {
        if(data.success){
         
  
          if(data.result.nickname == ""){
            
            if(data.result.login_type_id == "3"){
              this.login_nickname = data.result.mobile_num;
            }
            else if(data.result.login_type_id == "4"){
              this.login_nickname = data.result.email;
            }
            else{
              this.login_nickname = data.result.fname;
            }
          }
          else{
            this.login_nickname = data.result.nickname;
          }
          this.login_profile_photo = data.result.photo;



          let body3 = {
            action : 'checkSubscription',
            user_id : user_id
          };
          this.postPvdr.postData(body3, 'subscription.php').subscribe(data => {
            
            if(data.success){
              for(const key in data.result){
                this.vip_packageOfUser = data.result[key].vip_package_id;
              }
              if(this.vip_packageOfUser == '1'){
                this.loading("assets/gif/boquet.gif");
              }
              else if(this.vip_packageOfUser == '2'){
                this.loading("assets/gif/highfive.gif");
              }
              else if(this.vip_packageOfUser == '3'){
                this.loading("assets/gif/puppy.gif");
              }
            }


                this.socket.connect();
            
            
                // Join chatroom
                console.log("user_id:"+this.user_id+"room:"+this.stream_key);
                this.socket.emit('joinRoom', { 
                  user_id: this.login_nickname,
                  room: this.stream_key,
                  profile_picture: this.login_profile_photo,
                  vip_packageOfUser: this.vip_packageOfUser
                  });

                this.socket.fromEvent('message').subscribe(message => {
                  this.messages.push(message);
                  console.log("human og send"+ JSON.stringify(message));
                });
            
                this.socket.fromEvent('joinRoomMessage').subscribe(message => {
                  console.log("message from server:"+JSON.stringify(message));
                  this.messages.push(message);
                  this.addPictureViewer(message['profile_picture'], message['vip_packageOfUser']);
                });

                this.socket.fromEvent('leaveRoom').subscribe(message => {
                  console.log("message from server leave:"+JSON.stringify(message));
                  this.messages.push(message);
                });

              });

        }// end of success
  
      });



    });// end of user_id storage

    
  }

  addPictureViewer(joinPic, vip_packageOfUser){
    console.log("picture of those who join:"+joinPic);
    var viewPic = joinPic == '' ? "assets/icon/brixylogo.png":
    this.postPvdr.myServer()+"/brixy-live/images/"+joinPic;

    let body = {
      viewerPic : viewPic,
      vip_packageOfUser
    }
    this.viewersPic.push(body);

    console.log("vi:"+JSON.stringify(this.viewersPic));
    

  }

  sendMessage() {
    this.socket.emit('send-message', { 
      user_id: this.login_nickname+" : ", 
      room: this.stream_key, 
      text: this.message,
      vip_packageOfUser: this.vip_packageOfUser
       });

 

    let body = {
      action : 'insertConvoInGC',
      user_id : this.login_user_id,
      stream_id : this.stream_key,
      message : this.message
    };
    this.postPvdr.postData(body, 'messages.php').subscribe(async data => {
      if(data.success){
        

      }

    });



    this.message = '';

  }

 


  
  async openAllgifts() {
    const modal = await this.modalController.create({
      component: AllgiftsPage,
      cssClass: 'allgiftsmodalstyle',
      componentProps: { 'live_user_id' : this.livestreamId}
      
    });
    return await modal.present();
  }

  async openliveprofile() {
    const modal = await this.modalController.create({
      component: LiveprofilePage,
      cssClass: 'liveprofilemodalstyle',
      componentProps: { 
        liveStreamProfileId: this.livestreamId
      }
    });


    modal.onDidDismiss()
    .then((data) => {
      //const user = data['data']; // Here's your selected user!
      console.log("dismiss of liveprofilepage modal")
      this.plotFollowStatus();
    });


    return await modal.present();
  }

  plotFollowStatus(){
    console.log("plotFollowStatus");
    this.storage.get('user_id').then((user_id) => {

      let body2 = {
        action : 'followerStatus',
        followed_by : user_id,
        user_id : this.livestreamId
  
      };
      this.postPvdr.postData(body2, 'followers.php').subscribe(async data => {
        
        if(data.success){
          if(data.result=="1"){
            this.status = true;
            console.log("this.status start:"+this.status);
          }
          else{
            this.status = false;
          }
        }
      });

    });
  }
  
  start(streamKey) {
 
    var pc_config = {
      'iceServers' : [ {
        'urls' : 'stun:stun.l.google.com:19302'
      } ]
    };
  
    var sdpConstraints = {
      OfferToReceiveAudio : true,
      OfferToReceiveVideo : true
        
    };
    var mediaConstraints = {
      video: false,
      audio: false
    };
          
   // var appName = location.pathname.substring(0, location.pathname.lastIndexOf("/")+1);



    //var websocketURL = "wss://brixylive.com:5443/WebRTCAppEE/websocket";
    var websocketURL = "ws://livebrixylive.com:5080/WebRTCAppEE/websocket";

    var webRTCAdaptor = new WebRTCAdaptor({
		  websocket_url: websocketURL,
		  mediaConstraints: mediaConstraints,
		  peerconnection_config: pc_config,
		  sdp_constraints: sdpConstraints,
		  remoteVideoId: "remoteVideo",
      isPlayMode: true,
		  callback: function(info) {
			  if (info == "initialized") {
          console.log("initialized:"+streamKey);
          webRTCAdaptor.getStreamInfo(streamKey);
          //webRTCAdaptor.play(streamKey);
        }
        else if (info == "streamInformation") {
					console.log("stream information");
					webRTCAdaptor.play(streamKey);
				}
			  else if (info == "joined") {
				  //joined the stream
          console.log("joined");
          alert("joined");
			//	  join_button.disabled = true;
			//	  leave_button.disabled = false;
			  }
			  else if (info == "leaved") {
				  //leaved the stream
				  console.log("leaved");
			//	  join_button.disabled = false;
			//	  leave_button.disabled = true;
			  }
		  },
		  callbackError: function(error) {
			  //some of the possible errors, NotFoundError, SecurityError,PermissionDeniedError
			  
			  console.log("error callback: " + error);
			  alert(error);
		  }
    });
    
  }

  follow(){



    this.storage.get('user_id').then((user_id) => {
     
      console.log("user_id follow:"+user_id);
        let body = {
          action : 'follow',
          user_id : this.user_id,
          followed_by : user_id,
          status : this.status
        };

        this.postPvdr.postData(body, 'followers.php').subscribe(data => {
          console.log(data);
          if(data.success){
            
              this.followClass = "myHeartFollow";
              this.status = !this.status; 
          }
        });
      });
   
  }

  // join() {
  //   webRTCAdaptor.join("stream4");
  // }

  // publish() {
  //    this.webRTCAdaptor.publish("stream4");
  // }

  // play() {
  //   this.webRTCAdaptor.play("stream4");
  // }

}
