import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PostProvider } from '../../providers/credential-provider';
import { Storage } from '@ionic/storage';
import { Item } from '../shared/model/item.model';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-viewprofile',
  templateUrl: './viewprofile.page.html',
  styleUrls: ['./viewprofile.page.scss'],
})
export class ViewprofilePage implements OnInit {

  itemList: Item[] = [];
  username : string;

  user_rating: string = "";

  constructor(
    private router: Router,
    private postPvdr: PostProvider,
    
    public navCtrl: NavController,
    private storage: Storage
  ) { }

  goBack(){
    // this.router.navigate(['help']);
    window.history.back();
  }
  goProductview(item_id){
    // this.router.navigate(['productview']);
     let navigationExtras: any = {
         queryParams: { item_id}
     };
     this.navCtrl.navigateForward(['productview'], navigationExtras);
   }
   update_photo(){
    this.router.navigate(['uploadphoto']);
  }
  goHelp(){
    //window.location.href="http://greenthumbtrade.com/help";
    this.router.navigate(['tabs/help']);
  }

  ngOnInit() {
    this.plotData();
  }

  goSellerprof(user_profile_id){
    // this.router.navigate((['viewitemsellerprofile']));
    let navigationExtras: any = {
     queryParams: { user_profile_id}
  };
     this.navCtrl.navigateForward(['viewitemsellerprofile'], navigationExtras);
   }
  
  plotData(){
    this.storage.get('greenthumb_user_id').then((user_id) => {
      
      let body = {
        action : 'getUsername',
        user_id : user_id,
      };
      console.log(JSON.stringify(body));
      this.postPvdr.postData(body, 'user.php').subscribe(async data => {
  
        this.username = data.username;

        
      });



      let body2 = {
        action : 'getSellerPost',
        user_id : user_id
      };
      //console.log(JSON.stringify(body2));
      this.postPvdr.postData(body2, 'post_item.php').subscribe(async data => {
        
        if(data.success){

          const items: Item[] = [];

          //var pictureProfile: string = '';

          for(const key in data.result){
            // pictureProfile = (data.result[key].profile_photo == '') ? '' :
            // this.postPvdr.myServer()+"/brixy-live/images/"+data.result[key].profile_photo;
            items.push(new Item(
              data.result[key].item_id,
              data.result[key].user_id,
              data.result[key].username,
              (data.result[key].profile_photo == '') ? '' :
              this.postPvdr.myServer()+"/greenthumb/images/"+data.result[key].profile_photo,
              data.result[key].price,
              data.result[key].title,
              data.result[key].stocks_id,
              data.result[key].others_stock,
              parseInt(data.result[key].quantity),
              data.result[key].category,
              data.result[key].location,
              data.result[key].saveStatus,
              data.result[key].status,
              this.postPvdr.myServer()+"/greenthumb/images/posted_items/images/"+data.result[key].item_photo,
              data.result[key].user_rating,
              data.result[key].date,
              data.result[key].time
              )
              
            );
          }

          this.itemList = items;
        }
      });


    });
  }

}
