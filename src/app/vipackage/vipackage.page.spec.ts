import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VipackagePage } from './vipackage.page';

describe('VipackagePage', () => {
  let component: VipackagePage;
  let fixture: ComponentFixture<VipackagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VipackagePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VipackagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
