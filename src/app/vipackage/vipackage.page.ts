import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-vipackage',
  templateUrl: './vipackage.page.html',
  styleUrls: ['./vipackage.page.scss'],
})
export class VipackagePage implements OnInit {

  constructor( private router: Router) { }

  ngOnInit() {
  }

  goBack(){
    // this.router.navigate(['myaccount']);
    window.history.back();
  }

}
