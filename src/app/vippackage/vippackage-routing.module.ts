import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VippackagePage } from './vippackage.page';

const routes: Routes = [
  {
    path: '',
    component: VippackagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VippackagePageRoutingModule {}
