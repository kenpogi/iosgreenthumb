import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VippackagePageRoutingModule } from './vippackage-routing.module';

import { VippackagePage } from './vippackage.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VippackagePageRoutingModule
  ],
  declarations: [VippackagePage]
})
export class VippackagePageModule {}
