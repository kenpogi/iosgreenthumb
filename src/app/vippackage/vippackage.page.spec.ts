import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VippackagePage } from './vippackage.page';

describe('VippackagePage', () => {
  let component: VippackagePage;
  let fixture: ComponentFixture<VippackagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VippackagePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VippackagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
