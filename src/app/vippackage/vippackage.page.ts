import { Component, OnInit } from '@angular/core';
import { ModalController, ToastController, AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { PostProvider } from '../../providers/credential-provider';

@Component({
  selector: 'app-vippackage',
  templateUrl: './vippackage.page.html',
  styleUrls: ['./vippackage.page.scss'],
})
export class VippackagePage implements OnInit {

  vippackage: string = "";

  withPackage : boolean = false;
  monthlySubscription : boolean = false;

  constructor(
    private storage: Storage, 
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    private toastController: ToastController,
    private postPvdr: PostProvider
  ) { }

  goBack(){
    window.history.back();
  }

  ngOnInit() {
    this.vippackage = "3";
    this.plotData();
  }

  

  plotData(){
    this.storage.get("user_id").then((user_id) => {

      let body2 = {
        action : 'checkSubscription',
        user_id : user_id
      };
      console.log("subscription:"+JSON.stringify(body2));
      this.postPvdr.postData(body2, 'subscription.php').subscribe(data => {
        
        if(data.success){

          let provide = "";
          for(const key in data.result){
            //this.vippackage = data.result[key].vip_package_id;
            console.log("provide:"+data.result[key].vip_package_id);
            provide = data.result[key].vip_package_id;
          }

          this.vippackage = ''+provide;
          this.withPackage = true;

          
          
          console.log("vippackage:"+this.vippackage);

          let body21 = {
            action : 'checkMonthlySubscription',
            user_id : user_id
          };
          console.log("subscription:"+JSON.stringify(body21));
          this.postPvdr.postData(body21, 'subscription.php').subscribe(data => {

            for(const key in data.result){

              if(data.result[key].vip_subscription=="1"){
                this.monthlySubscription = true;
              }
              else{
                this.monthlySubscription = false;
              }
              console.log("monthlySubscritpion:"+data.result[key].vip_subscription);
              
            }
            
            console.log("monthlySubscritpion:"+this.monthlySubscription);

          })


        }
        
      });

    });
  }


  subscribe(){


    if(!this.withPackage){

      this.alertCtrl.create({
        header: 'Subscription Package',
        message: 'Are you sure you want to subscribe this package?',
        cssClass: 'food',
        
        buttons: [{
            text: 'CANCEL',
            role: 'cancel',
            handler: () => {
                console.log('Cancel verification!');
            }
        },
        {
          text: 'YES',
          handler: () => {
            this.subscribeVerified();
          }
        }
      ]
      }).then(res => {
        res.present();
      });
      
    }
    else{
      this.presentToast("You are currently subscribed to a VIP Package.");
    }
    
  }

  subscribeVerified(){

    this.storage.get("user_id").then((user_id) => {

      let body2 = {
        action : 'subscribePackage',
        user_id : user_id,
        vippackage: this.vippackage
      };
      console.log("looua:"+JSON.stringify(body2));
      this.postPvdr.postData(body2, 'subscription.php').subscribe(data => {
        
        if(data.success){
          this.presentToast("Package Successfully Subscribed.");
          this.withPackage = true;
          this.monthlySubscription = true;
        }
      });

    });
  }


  addValue(e: Event){


    e.preventDefault();
      e.stopImmediatePropagation();
      e.cancelBubble = true;
      e.stopPropagation();

      // Doing other stuff here to control if checkbox should be checked or not!, or just let it be empty!

     

    if(this.monthlySubscription){
          this.verifyCancel();
        }
        else{
          this.verifySubscription();
        }
        return (false);
  }
  
  verifySubscription(){

    this.alertCtrl.create({
      header: 'Subscription Package',
      message: 'Are you sure you want to avail Monthly Subscription?',
      cssClass: 'food',
      
      buttons: [{
          text: 'CANCEL',
          role: 'cancel',
          handler: () => {
              console.log('Cancel subscription!');
              this.monthlySubscription = false;
          }
      },
      {
        text: 'YES',
        handler: () => {
          this.monthlySubscription = true;
          this.availSubscription();
        }
      }
    ]
    }).then(res => {
      res.present();
    });

  }

  verifyCancel(){

    this.alertCtrl.create({
            header: 'Subscription Package',
            message: 'Are you sure you want to cancel Monthly Subscription?',
            cssClass: 'food',
            
            buttons: [{
                text: 'CANCEL',
                role: 'cancel',
                handler: () => {
                    console.log('Cancel subscription!');
                    this.monthlySubscription = true;
                }
            },
            {
              text: 'YES',
              handler: () => {
                this.monthlySubscription = false;
                this.cancelSubscription();
              }
            }
          ]
          }).then(res => {
            res.present();
          });
  }

  availSubscription(){

    this.storage.get("user_id").then((user_id) => {

      let body2 = {
        action : 'monthlySubscription',
        user_id : user_id,
        vip_subscription : 1,
      };
      this.postPvdr.postData(body2, 'subscription.php').subscribe(data => {
        
        if(data.success){
          this.withPackage = true;
          this.monthlySubscription = false;
        }
      });

    });
  

  }

  cancelSubscription(){

    this.storage.get("user_id").then((user_id) => {

      let body2 = {
        action : 'monthlySubscription',
        user_id : user_id,
        vip_subscription : 0,
      };
      console.log("looua:"+JSON.stringify(body2));
      this.postPvdr.postData(body2, 'subscription.php').subscribe(data => {
        
        if(data.success){
         
          this.withPackage = true;
          this.monthlySubscription = false;
        }
      });

    });
  

  }

  async presentToast(toastMessage) {
    const toast = await this.toastController.create({
      message: toastMessage,
      duration: 3000
    });
    toast.present();
  }



}
