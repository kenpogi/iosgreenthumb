import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WebproductPage } from './webproduct.page';

const routes: Routes = [
  {
    path: '',
    component: WebproductPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WebproductPageRoutingModule {}
