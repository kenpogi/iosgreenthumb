import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { WebproductPage } from './webproduct.page';

describe('WebproductPage', () => {
  let component: WebproductPage;
  let fixture: ComponentFixture<WebproductPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebproductPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(WebproductPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
