import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-webproduct',
  templateUrl: './webproduct.page.html',
  styleUrls: ['./webproduct.page.scss'],
})
export class WebproductPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  goBack(){
    window.history.back();
  }

}
