import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { WebprofilePageRoutingModule } from './webprofile-routing.module';

import { WebprofilePage } from './webprofile.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    WebprofilePageRoutingModule
  ],
  declarations: [WebprofilePage]
})
export class WebprofilePageModule {}
