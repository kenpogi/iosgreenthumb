import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { WebprofilePage } from './webprofile.page';

describe('WebprofilePage', () => {
  let component: WebprofilePage;
  let fixture: ComponentFixture<WebprofilePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebprofilePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(WebprofilePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
