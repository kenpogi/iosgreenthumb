import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-webprofile',
  templateUrl: './webprofile.page.html',
  styleUrls: ['./webprofile.page.scss'],
})
export class WebprofilePage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  goBack(){
    window.history.back();
  }

}
