import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { map } from "rxjs/operators"; 
import { Observable } from 'rxjs';
import { chat } from '../app/shared/model/chat.model';

@Injectable()
export class ChatMessages {
	//server: string = "https://www.telmotestserver.website/brixy-live/server_api/"; // server address https://www.telmotestserver.website/brixy-live/server_api/ http://localhost/server_api/
	server: string = "http://192.168.1.5/brixy-live/server_api/";
	//server: string = "http://165.22.242.129/brixy-live/server_api/";

	constructor(public http : Http) {

	}

	postData(body, file){
		let type = "application/json; charset=UTF-8";
		let headers = new Headers({ 'Content-Type': type });
		let options = new RequestOptions({ headers: headers });

		return this.http.post(this.server + file, JSON.stringify(body), options)
		.pipe(map(res => res.json()));
		//.pipe(map(res => console.log(res)))
    }
    
    getMessages(body, file): Observable<chat[]> {
        //let apiURL = `${this.apiRoot}?term=${term}&media=music&limit=20&callback=JSONP_CALLBACK`;
        // return this.http.get(apiURL) (1)
        //     .map(res => { (2)
        //       return res.json().results.map(item => { (3)
        //         return new SearchItem( (4)
        //             item.trackName,
        //             item.artistName,
        //             item.trackViewUrl,
        //             item.artworkUrl30,
        //             item.artistId
        //         );
        //       });
        //     });
        let type = "application/json; charset=UTF-8";
		let headers = new Headers({ 'Content-Type': type });
		let options = new RequestOptions({ headers: headers });
        return this.http.post(this.server + file, JSON.stringify(body), options)
		.pipe(map(res => res.json()));
      }

     sendMessage(body, file) {
        let type = "application/json; charset=UTF-8";
		let headers = new Headers({ 'Content-Type': type });
		let options = new RequestOptions({ headers: headers });

		return this.http.post(this.server + file, JSON.stringify(body), options)
		.pipe(map(res => res.json()));
    }
}