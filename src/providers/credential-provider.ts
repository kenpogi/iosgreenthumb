import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { map } from "rxjs/operators"; 

@Injectable()
export class PostProvider {
	//server: string = "https://www.telmotestserver.website/brixy-live/server_api/"; // server address https://www.telmotestserver.website/brixy-live/server_api/ http://localhost/server_api/
	//server: string = "http://192.168.1.5/brixy-live/server_api/";

	//myServerIP: string = "165.22.242.129";

	ip: string = "greenthumbtrade.com";
	//ip: string = "192.168.1.9"
	//ip: string = "localhost"

	myServerIP: string = "https://"+this.ip+"/green123thumb456";
	//myServerIP: string = "http://"+this.ip+"";

	server: string = this.myServerIP+"/greenthumb/server_api/";


	emailServer: string = this.myServerIP+"/greenthumb-email/send-email.php";

	constructor(public http : Http) {

	}

	postData(body, file){
		let type = "application/json; charset=UTF-8";
		let headers = new Headers({ 'Content-Type': type });
		let options = new RequestOptions({ headers: headers });

		return this.http.post(this.server + file, JSON.stringify(body), options)
		.pipe(map(res => res.json()));
		//.pipe(map(res => console.log(res)))
	}

	sendCode(body){
		let type = "application/json; charset=UTF-8";
		let headers = new Headers({ 'Content-Type': type });
		let options = new RequestOptions({ headers: headers });

		console.log("emailServer:"+this.emailServer);
		return this.http.post(this.emailServer, JSON.stringify(body), options)
		.pipe(map(res => res.json()));
		//.pipe(map(res => console.log(res)))
	}
	myServer(){
		return this.myServerIP;
	}
	
}