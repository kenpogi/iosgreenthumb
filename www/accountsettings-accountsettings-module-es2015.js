(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["accountsettings-accountsettings-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/accountsettings/accountsettings.page.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/accountsettings/accountsettings.page.html ***!
  \*************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header mode=\"ios\" no-border>\n  <ion-toolbar mode=\"ios\">\n      <ion-buttons slot=\"secondary\" (click)=\"goBack()\">\n        <ion-button mode=\"ios\" style=\"font-size: 15px;\">\n            Back\n        </ion-button>\n      </ion-buttons>\n\n      <ion-title color=\"secondary\">Settings</ion-title>\n\n    </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div text-center class=\"profile_card\">\n    <div class=\"wrapper\">\n      <br><br>\n      <ion-avatar style=\"margin: auto;\">\n        <img src=\"assets/greenthumb-images/userpicdark.png\">\n      </ion-avatar>\n      <ion-icon mode=\"ios\" (click)=\"upload_photo()\" class=\"upload_photo\" name=\"camera\"></ion-icon>\n      <br><br>\n    </div>\n  </div>\n  <ion-list class=\"for_accountitems\" padding>\n    <ion-list-header class=\"item_label\">Account</ion-list-header>\n    <ion-item style=\"margin-top: 5px;\" mode=\"ios\" id=\"for_eachitem\">\n      <ion-label>Lorem Name</ion-label>\n      <ion-icon name=\"arrow-forward\" slot=\"end\" mode=\"ios\" class=\"for_seticon\"></ion-icon>\n    </ion-item>\n    <ion-item style=\"margin-top: 5px;\" mode=\"ios\" id=\"for_eachitem\">\n      <ion-label>Phone</ion-label>\n      <ion-note mode=\"ios\" color=\"secondary\" style=\"font-size: 13px;\">Add&nbsp;</ion-note>\n      <ion-icon name=\"arrow-forward\" slot=\"end\" mode=\"ios\" class=\"for_seticon\"></ion-icon>\n    </ion-item>\n    <ion-item style=\"margin-top: 5px;\" mode=\"ios\" id=\"for_eachitem\">\n      <ion-label>lorem@gmail.com</ion-label>\n      <ion-icon name=\"arrow-forward\" slot=\"end\" mode=\"ios\" class=\"for_seticon\"></ion-icon>\n    </ion-item>\n    <ion-item style=\"margin-top: 5px;\" mode=\"ios\" id=\"for_eachitem\">\n      <ion-label>TruYou</ion-label>\n      <ion-note mode=\"ios\" color=\"secondary\" style=\"font-size: 13px;\">Add&nbsp;</ion-note>\n      <ion-icon name=\"arrow-forward\" slot=\"end\" mode=\"ios\" class=\"for_seticon\"></ion-icon>\n    </ion-item>\n    <ion-item style=\"margin-top: 5px;\" mode=\"ios\" id=\"for_eachitem\">\n      <ion-label>Facebook Connected</ion-label>\n      <ion-note mode=\"ios\" color=\"secondary\" style=\"font-size: 13px;\">Facebook Name here&nbsp;</ion-note>\n      <ion-icon name=\"arrow-forward\" slot=\"end\" mode=\"ios\" class=\"for_seticon\"></ion-icon>\n    </ion-item>\n    <ion-item style=\"margin-top: 5px;\" mode=\"ios\" id=\"for_eachitem\">\n      <ion-label>Password</ion-label>\n      <ion-icon name=\"arrow-forward\" slot=\"end\" mode=\"ios\" class=\"for_seticon\"></ion-icon>\n    </ion-item>\n    <ion-item style=\"margin-top: 5px;\" mode=\"ios\" id=\"for_eachitem\">\n      <ion-label>Tacloban City Philippines</ion-label>\n      <ion-icon name=\"arrow-forward\" slot=\"end\" mode=\"ios\" class=\"for_seticon\"></ion-icon>\n    </ion-item>\n    <ion-list-header class=\"item_label\">Notifications</ion-list-header>\n    <ion-item style=\"margin-top: 5px;\" mode=\"ios\" id=\"for_eachitem\">\n      <ion-label>Email</ion-label>\n      <ion-icon name=\"arrow-forward\" slot=\"end\" mode=\"ios\" class=\"for_seticon\"></ion-icon>\n    </ion-item>\n    <ion-item style=\"margin-top: 5px;\" mode=\"ios\" id=\"for_eachitem\">\n      <ion-label>Push</ion-label>\n      <ion-icon name=\"arrow-forward\" slot=\"end\" mode=\"ios\" class=\"for_seticon\"></ion-icon>\n    </ion-item>\n  </ion-list>\n  \n  \n</ion-content>\n");

/***/ }),

/***/ "./src/app/accountsettings/accountsettings-routing.module.ts":
/*!*******************************************************************!*\
  !*** ./src/app/accountsettings/accountsettings-routing.module.ts ***!
  \*******************************************************************/
/*! exports provided: AccountsettingsPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccountsettingsPageRoutingModule", function() { return AccountsettingsPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _accountsettings_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./accountsettings.page */ "./src/app/accountsettings/accountsettings.page.ts");




const routes = [
    {
        path: '',
        component: _accountsettings_page__WEBPACK_IMPORTED_MODULE_3__["AccountsettingsPage"]
    }
];
let AccountsettingsPageRoutingModule = class AccountsettingsPageRoutingModule {
};
AccountsettingsPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], AccountsettingsPageRoutingModule);



/***/ }),

/***/ "./src/app/accountsettings/accountsettings.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/accountsettings/accountsettings.module.ts ***!
  \***********************************************************/
/*! exports provided: AccountsettingsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccountsettingsPageModule", function() { return AccountsettingsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _accountsettings_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./accountsettings-routing.module */ "./src/app/accountsettings/accountsettings-routing.module.ts");
/* harmony import */ var _accountsettings_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./accountsettings.page */ "./src/app/accountsettings/accountsettings.page.ts");







let AccountsettingsPageModule = class AccountsettingsPageModule {
};
AccountsettingsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _accountsettings_routing_module__WEBPACK_IMPORTED_MODULE_5__["AccountsettingsPageRoutingModule"]
        ],
        declarations: [_accountsettings_page__WEBPACK_IMPORTED_MODULE_6__["AccountsettingsPage"]]
    })
], AccountsettingsPageModule);



/***/ }),

/***/ "./src/app/accountsettings/accountsettings.page.scss":
/*!***********************************************************!*\
  !*** ./src/app/accountsettings/accountsettings.page.scss ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".item_label {\n  font-weight: bold;\n  font-size: 15px;\n  padding-left: 0;\n  color: #679733;\n  margin-bottom: -4%;\n}\n\n#for_eachitem {\n  font-size: 14px;\n  --inner-padding-end: 0px !important;\n  --border-color: #e2f0cb;\n  --padding-start: 0% !important;\n}\n\n.for_seticon {\n  font-size: 15px;\n  color: #9da2b3;\n}\n\n.for_btnout {\n  font-size: 15px;\n  height: 36;\n  font-weight: bold;\n}\n\n.upload_photo {\n  background: white;\n  color: black;\n  padding: 5px;\n  border-radius: 50%;\n  font-size: 20px;\n  position: absolute;\n  top: 65%;\n  left: 57%;\n}\n\n.profile_card {\n  color: white;\n  --background: black;\n  background-image: url(\"/assets/greenthumb-images/greenthumblogo.png\");\n  background-position: center center;\n  background-repeat: no-repeat;\n  background-size: cover;\n}\n\n.wrapper {\n  background-color: #e2f0cb80;\n  -webkit-backdrop-filter: blur(10px);\n          backdrop-filter: blur(10px);\n}\n\nion-avatar {\n  width: 100% !important;\n  height: 100% !important;\n  max-width: 120px !important;\n  max-height: 120px !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZW5kcmljay9EZXNrdG9wL1RlbG1vIFNvbHV0aW9ucy9Gb3JQcm9kdWN0aW9uL21heTEwMjAyMS9sYXN0IHNldHVwL2dyZWVudGh1bWJhcHAgY29weS9zcmMvYXBwL2FjY291bnRzZXR0aW5ncy9hY2NvdW50c2V0dGluZ3MucGFnZS5zY3NzIiwic3JjL2FwcC9hY2NvdW50c2V0dGluZ3MvYWNjb3VudHNldHRpbmdzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7QUNDSjs7QURDRTtFQUNFLGVBQUE7RUFDQSxtQ0FBQTtFQUNBLHVCQUFBO0VBQ0EsOEJBQUE7QUNFSjs7QURBRTtFQUNFLGVBQUE7RUFDQSxjQUFBO0FDR0o7O0FEREU7RUFDRSxlQUFBO0VBQWdCLFVBQUE7RUFBVyxpQkFBQTtBQ00vQjs7QURKRTtFQUNFLGlCQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxTQUFBO0FDT0o7O0FETEE7RUFDSSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxxRUFBQTtFQUNBLGtDQUFBO0VBQ0EsNEJBQUE7RUFDQSxzQkFBQTtBQ1FKOztBRE5BO0VBRUksMkJBQUE7RUFDQSxtQ0FBQTtVQUFBLDJCQUFBO0FDUUo7O0FETkE7RUFDSSxzQkFBQTtFQUNBLHVCQUFBO0VBQ0EsMkJBQUE7RUFDQSw0QkFBQTtBQ1NKIiwiZmlsZSI6InNyYy9hcHAvYWNjb3VudHNldHRpbmdzL2FjY291bnRzZXR0aW5ncy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaXRlbV9sYWJlbHtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICBmb250LXNpemU6IDE1cHg7XG4gICAgcGFkZGluZy1sZWZ0OiAwO1xuICAgIGNvbG9yOiAjNjc5NzMzO1xuICAgIG1hcmdpbi1ib3R0b206IC00JTtcbiAgfVxuICAjZm9yX2VhY2hpdGVte1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAtLWlubmVyLXBhZGRpbmctZW5kOiAwcHggIWltcG9ydGFudDtcbiAgICAtLWJvcmRlci1jb2xvcjogI2UyZjBjYjtcbiAgICAtLXBhZGRpbmctc3RhcnQ6IDAlICFpbXBvcnRhbnQ7XG4gIH1cbiAgLmZvcl9zZXRpY29ue1xuICAgIGZvbnQtc2l6ZTogMTVweDtcbiAgICBjb2xvcjogIzlkYTJiMztcbiAgfVxuICAuZm9yX2J0bm91dHtcbiAgICBmb250LXNpemU6IDE1cHg7aGVpZ2h0OiAzNjtmb250LXdlaWdodDogYm9sZDtcbiAgfVxuICAudXBsb2FkX3Bob3Rve1xuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuICAgIGNvbG9yOiBibGFjaztcbiAgICBwYWRkaW5nOiA1cHg7XG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiA2NSU7XG4gICAgbGVmdDogNTclO1xufVxuLnByb2ZpbGVfY2FyZHtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgLS1iYWNrZ3JvdW5kOiBibGFjaztcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoJy9hc3NldHMvZ3JlZW50aHVtYi1pbWFnZXMvZ3JlZW50aHVtYmxvZ28ucG5nJyk7XG4gICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyIGNlbnRlcjtcbiAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG59XG4ud3JhcHBlcntcbiAgICAvLyBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDI1NSwgMjU1LCAyNTUsIC4xNSk7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2UyZjBjYjgwO1xuICAgIGJhY2tkcm9wLWZpbHRlcjogYmx1cigxMHB4KTtcbn1cbmlvbi1hdmF0YXIgIHsgICAgIFxuICAgIHdpZHRoOjEwMCUgIWltcG9ydGFudDsgIFxuICAgIGhlaWdodCA6IDEwMCUgIWltcG9ydGFudDsgIFxuICAgIG1heC13aWR0aDogMTIwcHggIWltcG9ydGFudDsgIC8vYW55IHNpemVcbiAgICBtYXgtaGVpZ2h0OiAxMjBweCAhaW1wb3J0YW50OyAvL2FueSBzaXplIFxuICAgIH0iLCIuaXRlbV9sYWJlbCB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBmb250LXNpemU6IDE1cHg7XG4gIHBhZGRpbmctbGVmdDogMDtcbiAgY29sb3I6ICM2Nzk3MzM7XG4gIG1hcmdpbi1ib3R0b206IC00JTtcbn1cblxuI2Zvcl9lYWNoaXRlbSB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgLS1pbm5lci1wYWRkaW5nLWVuZDogMHB4ICFpbXBvcnRhbnQ7XG4gIC0tYm9yZGVyLWNvbG9yOiAjZTJmMGNiO1xuICAtLXBhZGRpbmctc3RhcnQ6IDAlICFpbXBvcnRhbnQ7XG59XG5cbi5mb3Jfc2V0aWNvbiB7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgY29sb3I6ICM5ZGEyYjM7XG59XG5cbi5mb3JfYnRub3V0IHtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBoZWlnaHQ6IDM2O1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cblxuLnVwbG9hZF9waG90byB7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBjb2xvcjogYmxhY2s7XG4gIHBhZGRpbmc6IDVweDtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICBmb250LXNpemU6IDIwcHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiA2NSU7XG4gIGxlZnQ6IDU3JTtcbn1cblxuLnByb2ZpbGVfY2FyZCB7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgLS1iYWNrZ3JvdW5kOiBibGFjaztcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiL2Fzc2V0cy9ncmVlbnRodW1iLWltYWdlcy9ncmVlbnRodW1ibG9nby5wbmdcIik7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlciBjZW50ZXI7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG59XG5cbi53cmFwcGVyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2UyZjBjYjgwO1xuICBiYWNrZHJvcC1maWx0ZXI6IGJsdXIoMTBweCk7XG59XG5cbmlvbi1hdmF0YXIge1xuICB3aWR0aDogMTAwJSAhaW1wb3J0YW50O1xuICBoZWlnaHQ6IDEwMCUgIWltcG9ydGFudDtcbiAgbWF4LXdpZHRoOiAxMjBweCAhaW1wb3J0YW50O1xuICBtYXgtaGVpZ2h0OiAxMjBweCAhaW1wb3J0YW50O1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/accountsettings/accountsettings.page.ts":
/*!*********************************************************!*\
  !*** ./src/app/accountsettings/accountsettings.page.ts ***!
  \*********************************************************/
/*! exports provided: AccountsettingsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccountsettingsPage", function() { return AccountsettingsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/es2015/index.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");






let AccountsettingsPage = class AccountsettingsPage {
    constructor(router, afAuth, toastController, storage) {
        this.router = router;
        this.afAuth = afAuth;
        this.toastController = toastController;
        this.storage = storage;
    }
    ngOnInit() {
    }
    goAbout() {
        //window.location.href="http://greenthumbtrade.com";
    }
    goBack() {
        window.history.back();
        //this.navCtrl.navigateForward((['describeitem']), { animated: false, });
    }
    upload_photo() {
        this.router.navigate(['uploadphoto']);
    }
    presentToast() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: 'You are now logged out.',
                duration: 3000
            });
            toast.present();
        });
    }
    signOut() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.afAuth.auth.signOut().then(() => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                yield this.storage.clear();
                this.presentToast();
                this.router.navigate(['choose']);
            }));
        });
    }
};
AccountsettingsPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__["AngularFireAuth"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ToastController"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"] }
];
AccountsettingsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-accountsettings',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./accountsettings.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/accountsettings/accountsettings.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./accountsettings.page.scss */ "./src/app/accountsettings/accountsettings.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
        _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__["AngularFireAuth"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ToastController"],
        _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"]])
], AccountsettingsPage);



/***/ })

}]);
//# sourceMappingURL=accountsettings-accountsettings-module-es2015.js.map