(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["applepay-applepay-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/applepay/applepay.page.html":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/applepay/applepay.page.html ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header mode=\"ios\" no-border>\n  <ion-toolbar mode=\"ios\">\n      <ion-buttons slot=\"secondary\" (click)=\"goBack()\">\n        <ion-button mode=\"ios\" style=\"font-size: 15px;\">\n            Back\n        </ion-button>\n      </ion-buttons>\n      <ion-title color=\"secondary\">Apple Pay</ion-title>\n    </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n</ion-content>\n");

/***/ }),

/***/ "./src/app/applepay/applepay-routing.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/applepay/applepay-routing.module.ts ***!
  \*****************************************************/
/*! exports provided: ApplepayPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApplepayPageRoutingModule", function() { return ApplepayPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _applepay_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./applepay.page */ "./src/app/applepay/applepay.page.ts");




const routes = [
    {
        path: '',
        component: _applepay_page__WEBPACK_IMPORTED_MODULE_3__["ApplepayPage"]
    }
];
let ApplepayPageRoutingModule = class ApplepayPageRoutingModule {
};
ApplepayPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ApplepayPageRoutingModule);



/***/ }),

/***/ "./src/app/applepay/applepay.module.ts":
/*!*********************************************!*\
  !*** ./src/app/applepay/applepay.module.ts ***!
  \*********************************************/
/*! exports provided: ApplepayPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApplepayPageModule", function() { return ApplepayPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _applepay_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./applepay-routing.module */ "./src/app/applepay/applepay-routing.module.ts");
/* harmony import */ var _applepay_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./applepay.page */ "./src/app/applepay/applepay.page.ts");







let ApplepayPageModule = class ApplepayPageModule {
};
ApplepayPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _applepay_routing_module__WEBPACK_IMPORTED_MODULE_5__["ApplepayPageRoutingModule"]
        ],
        declarations: [_applepay_page__WEBPACK_IMPORTED_MODULE_6__["ApplepayPage"]]
    })
], ApplepayPageModule);



/***/ }),

/***/ "./src/app/applepay/applepay.page.scss":
/*!*********************************************!*\
  !*** ./src/app/applepay/applepay.page.scss ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcGxlcGF5L2FwcGxlcGF5LnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/applepay/applepay.page.ts":
/*!*******************************************!*\
  !*** ./src/app/applepay/applepay.page.ts ***!
  \*******************************************/
/*! exports provided: ApplepayPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApplepayPage", function() { return ApplepayPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");



let ApplepayPage = class ApplepayPage {
    constructor(router) {
        this.router = router;
    }
    goBack() {
        // this.router.navigate(['help']);
        window.history.back();
    }
    ngOnInit() {
    }
};
ApplepayPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
ApplepayPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-applepay',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./applepay.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/applepay/applepay.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./applepay.page.scss */ "./src/app/applepay/applepay.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
], ApplepayPage);



/***/ })

}]);
//# sourceMappingURL=applepay-applepay-module-es2015.js.map