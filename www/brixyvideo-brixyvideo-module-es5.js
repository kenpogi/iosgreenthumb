function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["brixyvideo-brixyvideo-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/brixyvideo/brixyvideo.page.html":
  /*!***************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/brixyvideo/brixyvideo.page.html ***!
    \***************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppBrixyvideoBrixyvideoPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-content>\n        <iframe width=\"560\" autoplay height=\"315\" src=\"https://www.youtube.com/embed/EVnjZnJ5_cg?autoplay=1\" frameborder=\"0\" allowfullscreen></iframe>\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/brixyvideo/brixyvideo.module.ts":
  /*!*************************************************!*\
    !*** ./src/app/brixyvideo/brixyvideo.module.ts ***!
    \*************************************************/

  /*! exports provided: BrixyvideoPageModule */

  /***/
  function srcAppBrixyvideoBrixyvideoModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "BrixyvideoPageModule", function () {
      return BrixyvideoPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _brixyvideo_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./brixyvideo.page */
    "./src/app/brixyvideo/brixyvideo.page.ts");

    var routes = [{
      path: '',
      component: _brixyvideo_page__WEBPACK_IMPORTED_MODULE_6__["BrixyvideoPage"]
    }];

    var BrixyvideoPageModule = function BrixyvideoPageModule() {
      _classCallCheck(this, BrixyvideoPageModule);
    };

    BrixyvideoPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      declarations: [_brixyvideo_page__WEBPACK_IMPORTED_MODULE_6__["BrixyvideoPage"]]
    })], BrixyvideoPageModule);
    /***/
  },

  /***/
  "./src/app/brixyvideo/brixyvideo.page.scss":
  /*!*************************************************!*\
    !*** ./src/app/brixyvideo/brixyvideo.page.scss ***!
    \*************************************************/

  /*! exports provided: default */

  /***/
  function srcAppBrixyvideoBrixyvideoPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2JyaXh5dmlkZW8vYnJpeHl2aWRlby5wYWdlLnNjc3MifQ== */";
    /***/
  },

  /***/
  "./src/app/brixyvideo/brixyvideo.page.ts":
  /*!***********************************************!*\
    !*** ./src/app/brixyvideo/brixyvideo.page.ts ***!
    \***********************************************/

  /*! exports provided: BrixyvideoPage */

  /***/
  function srcAppBrixyvideoBrixyvideoPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "BrixyvideoPage", function () {
      return BrixyvideoPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");

    var BrixyvideoPage = /*#__PURE__*/function () {
      function BrixyvideoPage(router) {
        _classCallCheck(this, BrixyvideoPage);

        this.router = router;
      }

      _createClass(BrixyvideoPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return BrixyvideoPage;
    }();

    BrixyvideoPage.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }];
    };

    BrixyvideoPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-brixyvideo',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./brixyvideo.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/brixyvideo/brixyvideo.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./brixyvideo.page.scss */
      "./src/app/brixyvideo/brixyvideo.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])], BrixyvideoPage);
    /***/
  }
}]);
//# sourceMappingURL=brixyvideo-brixyvideo-module-es5.js.map