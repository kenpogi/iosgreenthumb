(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["communityforum-communityforum-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/communityforum/communityforum.page.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/communityforum/communityforum.page.html ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header mode=\"ios\" no-border>\n  <ion-toolbar mode=\"ios\">\n      <ion-buttons slot=\"secondary\" (click)=\"goBack()\">\n        <ion-button mode=\"ios\" style=\"font-size: 15px;\">\n            Back\n        </ion-button>\n      </ion-buttons>\n\n      \n      <!-- <ion-title color=\"secondary\">Post an Item</ion-title>\n\n      <ion-buttons slot=\"primary\" (click)=\"goHelp()\">\n        <ion-button mode=\"ios\" style=\"font-size: 15px;\">\n            Help\n        </ion-button>\n      </ion-buttons> -->\n    </ion-toolbar>\n</ion-header>\n<ion-content>\n  <iframe src=\"https://www.greenthumbtrade.com/community/\" style=\"height: 100%; width: 100%;\"></iframe>\n\n</ion-content>\n");

/***/ }),

/***/ "./src/app/communityforum/communityforum-routing.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/communityforum/communityforum-routing.module.ts ***!
  \*****************************************************************/
/*! exports provided: CommunityforumPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommunityforumPageRoutingModule", function() { return CommunityforumPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _communityforum_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./communityforum.page */ "./src/app/communityforum/communityforum.page.ts");




const routes = [
    {
        path: '',
        component: _communityforum_page__WEBPACK_IMPORTED_MODULE_3__["CommunityforumPage"]
    }
];
let CommunityforumPageRoutingModule = class CommunityforumPageRoutingModule {
};
CommunityforumPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], CommunityforumPageRoutingModule);



/***/ }),

/***/ "./src/app/communityforum/communityforum.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/communityforum/communityforum.module.ts ***!
  \*********************************************************/
/*! exports provided: CommunityforumPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommunityforumPageModule", function() { return CommunityforumPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _communityforum_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./communityforum-routing.module */ "./src/app/communityforum/communityforum-routing.module.ts");
/* harmony import */ var _communityforum_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./communityforum.page */ "./src/app/communityforum/communityforum.page.ts");







let CommunityforumPageModule = class CommunityforumPageModule {
};
CommunityforumPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _communityforum_routing_module__WEBPACK_IMPORTED_MODULE_5__["CommunityforumPageRoutingModule"]
        ],
        declarations: [_communityforum_page__WEBPACK_IMPORTED_MODULE_6__["CommunityforumPage"]]
    })
], CommunityforumPageModule);



/***/ }),

/***/ "./src/app/communityforum/communityforum.page.scss":
/*!*********************************************************!*\
  !*** ./src/app/communityforum/communityforum.page.scss ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbW11bml0eWZvcnVtL2NvbW11bml0eWZvcnVtLnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/communityforum/communityforum.page.ts":
/*!*******************************************************!*\
  !*** ./src/app/communityforum/communityforum.page.ts ***!
  \*******************************************************/
/*! exports provided: CommunityforumPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommunityforumPage", function() { return CommunityforumPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let CommunityforumPage = class CommunityforumPage {
    constructor() { }
    ngOnInit() {
    }
    goBack() {
        window.history.back();
    }
};
CommunityforumPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-communityforum',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./communityforum.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/communityforum/communityforum.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./communityforum.page.scss */ "./src/app/communityforum/communityforum.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], CommunityforumPage);



/***/ })

}]);
//# sourceMappingURL=communityforum-communityforum-module-es2015.js.map