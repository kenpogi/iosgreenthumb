(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["edititem-edititem-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/edititem/edititem.page.html":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/edititem/edititem.page.html ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header mode=\"ios\" no-border>\n  <ion-toolbar mode=\"ios\">\n      <ion-buttons slot=\"secondary\" (click)=\"goBack()\">\n        <ion-button mode=\"ios\" style=\"font-size: 15px;\">\n            Back\n        </ion-button>\n      </ion-buttons>\n\n      <ion-title color=\"secondary\">Edit Item</ion-title>\n\n      <ion-buttons slot=\"primary\">\n        <ion-button mode=\"ios\" (click)=\"selectImage()\">\n          <ion-icon size=\"large\" name=\"camera\" mode=\"ios\" color=\"secondary\"></ion-icon>\n        </ion-button>\n      </ion-buttons>\n    </ion-toolbar>\n</ion-header>\n<ion-content padding class=\"for_post\">\n  <ion-button style=\"height: 40px;\" mode=\"ios\" (click)=\"goSellfaster()\">\n    <ion-icon name=\"trending-up\" mode=\"ios\"></ion-icon>&nbsp;&nbsp;SELL FASTER</ion-button>\n  <ion-row>\n    \n    \n\n    <ion-grid>\n\n      <ion-row *ngFor=\"let photo of ImageArray; let i = index\">\n        <ion-col *ngIf=\"i % 2 == 0\">\n          <ion-card class=\"block\" mode=\"ios\">\n            <ion-icon name=\"trash\" class=\"deleteIcon\" (click)=\"deletePhoto(photo.picture_filename, i)\"></ion-icon>\n            <img [src]=\"photo.picture_filename\" *ngIf=\"photo.picture_filename\" />\n          </ion-card>\n         </ion-col>\n      </ion-row>\n    </ion-grid>\n    <ion-grid>\n      <ion-row *ngFor=\"let photo of ImageArray; let i = index\">\n        <ion-col *ngIf=\"i % 2 != 0\">\n          <ion-card class=\"block\" mode=\"ios\">\n            <ion-icon name=\"trash\" class=\"deleteIcon\" (click)=\"deletePhoto(i)\"></ion-icon>\n            <img [src]=\"photo.picture_filename\" *ngIf=\"photo.picture_filename\" />\n          </ion-card>\n         </ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-row>\n  <br>  \n  <ion-list class=\"line-input\">\n    <ion-row>\n      <ion-col size=\"3\" text-left no-padding>\n        <p class=\"item_label\">Item Price</p>\n      </ion-col>\n      <ion-col size=\"9\" text-left no-padding>\n        <ion-item>\n          <ion-input mode=\"ios\" [(ngModel)]=\"item_price\" type=\"text\" class=\"item_input\" size=\"small\"></ion-input>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"3\" text-left no-padding>\n        <p class=\"item_label\">Item Name</p>\n      </ion-col>\n      <ion-col size=\"9\" text-left no-padding>\n        <ion-item>\n          <ion-input mode=\"ios\" [(ngModel)]=\"item_name\" type=\"text\" class=\"item_input\" size=\"small\"></ion-input>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"3\" text-left no-padding>\n        <p class=\"item_label\">Category</p>\n      </ion-col>\n      <ion-col size=\"9\" text-left no-padding>\n        <ion-item>\n          <ion-input mode=\"ios\" [(ngModel)]=\"item_category\" type=\"text\" class=\"item_input\" size=\"small\"></ion-input>\n          <ion-note (click)=\"goCategories()\" color=\"secondary\" mode=\"ios\" slot=\"end\" class=\"edit_txt\">Edit</ion-note>\n           <ion-icon (click)=\"goCategories()\" name=\"arrow-forward\" mode=\"ios\" slot=\"end\" color=\"secondary\" class=\"edit_icon\"></ion-icon>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"3\" text-left no-padding>\n        <p class=\"item_label\">Availability</p>\n      </ion-col>\n      <ion-col size=\"9\" text-left no-padding>\n        <ion-item mode=\"ios\" lines=\"none\" style=\"--padding-start: 0;\">\n          <ion-select text-right expand=\"block\" class=\"for_selavailability\"  mode=\"ios\" okText=\"Okay\" cancelText=\"Dismiss\">\n            <ion-label>Availability</ion-label>\n            <ion-select-option selected>In Stock</ion-select-option>\n            <ion-select-option>Out of Stock</ion-select-option>\n            <ion-select-option>Unavailable</ion-select-option>\n            <ion-select-option>Unsold</ion-select-option>\n          </ion-select>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"3\" text-left no-padding>\n        <p class=\"item_label\">City</p>\n      </ion-col>\n      <ion-col size=\"9\" text-left no-padding>\n        <ion-item>\n          <ion-input mode=\"ios\" [(ngModel)]=\"item_location\" type=\"text\" class=\"item_input\" size=\"small\"></ion-input>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"3\" text-left no-padding>\n        <p class=\"item_label\">Zip Code</p>\n      </ion-col>\n      <ion-col size=\"9\" text-left no-padding>\n        <ion-item>\n          <ion-input mode=\"ios\" [(ngModel)]=\"item_zipcode\" type=\"text\" class=\"item_input\" size=\"small\"></ion-input>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"12\" text-left no-padding>\n        <p class=\"item_label\">Description</p>\n      </ion-col>\n      <ion-col size=\"12\" text-left no-padding>\n        <ion-item mode=\"ios\" lines=\"none\" id=\"item_area\">\n          <ion-textarea [(ngModel)]=\"item_description\" mode=\"ios\"></ion-textarea>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n  </ion-list>\n\n</ion-content>\n<div class=\"edititem-overlay\" padding>\n  <ion-row>\n    <ion-col size=\"5\" no-padding>\n      <ion-button color=\"danger\" expand=\"block\" mode=\"ios\">\n        DELETE\n      </ion-button>\n    </ion-col>\n    <ion-col size=\"7\" no-padding>\n      <ion-button (click)=\"saveEdit()\" expand=\"block\" mode=\"ios\">\n        UPDATE\n      </ion-button>\n    </ion-col>\n  </ion-row>\n</div>\n");

/***/ }),

/***/ "./src/app/edititem/edititem-routing.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/edititem/edititem-routing.module.ts ***!
  \*****************************************************/
/*! exports provided: EdititemPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EdititemPageRoutingModule", function() { return EdititemPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _edititem_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./edititem.page */ "./src/app/edititem/edititem.page.ts");




const routes = [
    {
        path: '',
        component: _edititem_page__WEBPACK_IMPORTED_MODULE_3__["EdititemPage"]
    }
];
let EdititemPageRoutingModule = class EdititemPageRoutingModule {
};
EdititemPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], EdititemPageRoutingModule);



/***/ }),

/***/ "./src/app/edititem/edititem.module.ts":
/*!*********************************************!*\
  !*** ./src/app/edititem/edititem.module.ts ***!
  \*********************************************/
/*! exports provided: EdititemPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EdititemPageModule", function() { return EdititemPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _edititem_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./edititem-routing.module */ "./src/app/edititem/edititem-routing.module.ts");
/* harmony import */ var _edititem_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./edititem.page */ "./src/app/edititem/edititem.page.ts");







let EdititemPageModule = class EdititemPageModule {
};
EdititemPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _edititem_routing_module__WEBPACK_IMPORTED_MODULE_5__["EdititemPageRoutingModule"]
        ],
        declarations: [_edititem_page__WEBPACK_IMPORTED_MODULE_6__["EdititemPage"]]
    })
], EdititemPageModule);



/***/ }),

/***/ "./src/app/edititem/edititem.page.scss":
/*!*********************************************!*\
  !*** ./src/app/edititem/edititem.page.scss ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".for_post {\n  --background: none;\n  background-image: url(\"/assets/greenthumb-images/bunny2.png\");\n  background-position: center center;\n  background-repeat: no-repeat;\n  background-size: contain;\n}\n\n.for_post:before {\n  content: \"\";\n  position: absolute;\n  left: 0;\n  right: 0;\n  top: 0;\n  bottom: 0;\n  background: rgba(255, 255, 255, 0.9);\n}\n\n.line-input {\n  margin-bottom: 0 !important;\n  background: transparent;\n}\n\n.line-input ion-item {\n  --border-color: transparent!important;\n  --highlight-height: 0;\n  border: 1px solid #dedede;\n  border-radius: 4px;\n  height: 35px;\n}\n\n.line-input #item_area {\n  --padding-start: 0;\n  height: 70px;\n  padding: 10px;\n  padding-top: 0;\n  padding-bottom: 0;\n  padding-right: 0;\n}\n\n.line-input #item_area ion-textarea {\n  font-size: 14px;\n  font-weight: bold;\n  color: #679733;\n}\n\n.item_input {\n  font-size: 14px;\n  font-weight: bold;\n  color: #679733;\n  --padding-top: 0;\n  margin-top: -5px;\n}\n\n.edit_txt {\n  margin-top: -7px;\n  font-size: 12px;\n}\n\n.edit_icon {\n  font-size: 12px;\n  margin-top: 0px;\n  margin-left: 2px;\n}\n\n.item_select {\n  font-size: 14px;\n  --padding-top: 1%;\n  color: #424242 !important;\n  margin-top: -5px;\n}\n\n.item_label {\n  font-weight: 300;\n  padding: 0;\n  margin-top: 8px;\n  font-size: 13px;\n}\n\n.for_selavailability {\n  max-width: 100%;\n  font-size: 14px;\n  font-weight: bold;\n  color: #679733;\n  margin-top: -11px;\n}\n\n.block {\n  position: relative;\n  box-shadow: none;\n  margin: 0;\n}\n\n.block .deleteIcon {\n  position: absolute !important;\n  color: #ffffff !important;\n  right: 5%;\n  top: 3%;\n  font-size: 20px;\n}\n\n.block .deleteIcon:before {\n  font-size: 30px !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZW5kcmljay9EZXNrdG9wL1RlbG1vIFNvbHV0aW9ucy9Gb3JQcm9kdWN0aW9uL21heTEwMjAyMS9sYXN0IHNldHVwL2dyZWVudGh1bWJhcHAgY29weS9zcmMvYXBwL2VkaXRpdGVtL2VkaXRpdGVtLnBhZ2Uuc2NzcyIsInNyYy9hcHAvZWRpdGl0ZW0vZWRpdGl0ZW0ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNFO0VBQ0Usa0JBQUE7RUFDQSw2REFBQTtFQUNBLGtDQUFBO0VBQ0EsNEJBQUE7RUFDQSx3QkFBQTtBQ0FKOztBREVBO0VBQ0ksV0FBQTtFQUNBLGtCQUFBO0VBQ0EsT0FBQTtFQUFTLFFBQUE7RUFDVCxNQUFBO0VBQVEsU0FBQTtFQUNSLG9DQUFBO0FDR0o7O0FEREE7RUFDSSwyQkFBQTtFQUNBLHVCQUFBO0FDSUo7O0FESEk7RUFDSSxxQ0FBQTtFQUNBLHFCQUFBO0VBQ0EseUJBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7QUNLUjs7QURISTtFQUNJLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtBQ0tSOztBREhRO0VBQ0ksZUFBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtBQ0taOztBRERBO0VBQ0ksZUFBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7QUNJSjs7QURGQTtFQUNFLGdCQUFBO0VBQ0EsZUFBQTtBQ0tGOztBREhBO0VBQ0UsZUFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQ01GOztBREpBO0VBQ0ksZUFBQTtFQUNBLGlCQUFBO0VBQ0EseUJBQUE7RUFDQSxnQkFBQTtBQ09KOztBRExBO0VBQ0ksZ0JBQUE7RUFDQSxVQUFBO0VBQ0EsZUFBQTtFQUNBLGVBQUE7QUNRSjs7QUROQTtFQUNJLGVBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7QUNTSjs7QURQQTtFQUNFLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxTQUFBO0FDVUY7O0FEVEU7RUFDRSw2QkFBQTtFQUNBLHlCQUFBO0VBQ0EsU0FBQTtFQUNBLE9BQUE7RUFDQSxlQUFBO0FDV0o7O0FEVEU7RUFDRSwwQkFBQTtBQ1dKIiwiZmlsZSI6InNyYy9hcHAvZWRpdGl0ZW0vZWRpdGl0ZW0ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXG4gIC5mb3JfcG9zdHtcbiAgICAtLWJhY2tncm91bmQ6IG5vbmU7XG4gICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKCcvYXNzZXRzL2dyZWVudGh1bWItaW1hZ2VzL2J1bm55Mi5wbmcnKTtcbiAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXIgY2VudGVyO1xuICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gICAgYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xufVxuLmZvcl9wb3N0OmJlZm9yZSB7XG4gICAgY29udGVudDogXCJcIjtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgbGVmdDogMDsgcmlnaHQ6IDA7XG4gICAgdG9wOiAwOyBib3R0b206IDA7XG4gICAgYmFja2dyb3VuZDogcmdiYSgyNTUsMjU1LDI1NSw5MCUpO1xuICB9XG4ubGluZS1pbnB1dCB7XG4gICAgbWFyZ2luLWJvdHRvbTogMCFpbXBvcnRhbnQ7XG4gICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG4gICAgaW9uLWl0ZW0ge1xuICAgICAgICAtLWJvcmRlci1jb2xvcjogdHJhbnNwYXJlbnQhaW1wb3J0YW50O1xuICAgICAgICAtLWhpZ2hsaWdodC1oZWlnaHQ6IDA7XG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNkZWRlZGU7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgICAgICAgaGVpZ2h0OiAzNXB4O1xuICAgIH1cbiAgICAjaXRlbV9hcmVhe1xuICAgICAgICAtLXBhZGRpbmctc3RhcnQ6IDA7XG4gICAgICAgIGhlaWdodDogNzBweDtcbiAgICAgICAgcGFkZGluZzogMTBweDtcbiAgICAgICAgcGFkZGluZy10b3A6IDA7XG4gICAgICAgIHBhZGRpbmctYm90dG9tOiAwO1xuICAgICAgICBwYWRkaW5nLXJpZ2h0OiAwO1xuXG4gICAgICAgIGlvbi10ZXh0YXJlYXtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgICAgICAgICAgY29sb3I6ICM2Nzk3MzM7XG4gICAgICAgIH0gICBcbiAgICB9XG59XG4uaXRlbV9pbnB1dHtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgY29sb3I6ICM2Nzk3MzM7XG4gICAgLS1wYWRkaW5nLXRvcDogMDtcbiAgICBtYXJnaW4tdG9wOiAtNXB4O1xufVxuLmVkaXRfdHh0e1xuICBtYXJnaW4tdG9wOiAtN3B4O1xuICBmb250LXNpemU6IDEycHg7XG59XG4uZWRpdF9pY29ue1xuICBmb250LXNpemU6IDEycHg7XG4gIG1hcmdpbi10b3A6IDBweDtcbiAgbWFyZ2luLWxlZnQ6IDJweDtcbn1cbi5pdGVtX3NlbGVjdHtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgLS1wYWRkaW5nLXRvcDogMSU7XG4gICAgY29sb3I6ICM0MjQyNDIhaW1wb3J0YW50O1xuICAgIG1hcmdpbi10b3A6IC01cHg7XG59XG4uaXRlbV9sYWJlbHtcbiAgICBmb250LXdlaWdodDogMzAwO1xuICAgIHBhZGRpbmc6IDA7XG4gICAgbWFyZ2luLXRvcDogOHB4O1xuICAgIGZvbnQtc2l6ZTogMTNweDtcbn1cbi5mb3Jfc2VsYXZhaWxhYmlsaXR5e1xuICAgIG1heC13aWR0aDogMTAwJTtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgY29sb3I6ICM2Nzk3MzM7XG4gICAgbWFyZ2luLXRvcDogLTExcHg7XG59XG4uYmxvY2sge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGJveC1zaGFkb3c6IG5vbmU7XG4gIG1hcmdpbjogMDtcbiAgLmRlbGV0ZUljb24ge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZSAhaW1wb3J0YW50O1xuICAgIGNvbG9yOiAjZmZmZmZmICFpbXBvcnRhbnQ7XG4gICAgcmlnaHQ6IDUlO1xuICAgIHRvcDogMyU7XG4gICAgZm9udC1zaXplOiAyMHB4O1xuICB9XG4gIC5kZWxldGVJY29uOmJlZm9yZSB7XG4gICAgZm9udC1zaXplOiAzMHB4ICFpbXBvcnRhbnQ7XG4gIH1cbiAgfVxuXG4gICIsIi5mb3JfcG9zdCB7XG4gIC0tYmFja2dyb3VuZDogbm9uZTtcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiL2Fzc2V0cy9ncmVlbnRodW1iLWltYWdlcy9idW5ueTIucG5nXCIpO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXIgY2VudGVyO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XG59XG5cbi5mb3JfcG9zdDpiZWZvcmUge1xuICBjb250ZW50OiBcIlwiO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGxlZnQ6IDA7XG4gIHJpZ2h0OiAwO1xuICB0b3A6IDA7XG4gIGJvdHRvbTogMDtcbiAgYmFja2dyb3VuZDogcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjkpO1xufVxuXG4ubGluZS1pbnB1dCB7XG4gIG1hcmdpbi1ib3R0b206IDAgIWltcG9ydGFudDtcbiAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG59XG4ubGluZS1pbnB1dCBpb24taXRlbSB7XG4gIC0tYm9yZGVyLWNvbG9yOiB0cmFuc3BhcmVudCFpbXBvcnRhbnQ7XG4gIC0taGlnaGxpZ2h0LWhlaWdodDogMDtcbiAgYm9yZGVyOiAxcHggc29saWQgI2RlZGVkZTtcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xuICBoZWlnaHQ6IDM1cHg7XG59XG4ubGluZS1pbnB1dCAjaXRlbV9hcmVhIHtcbiAgLS1wYWRkaW5nLXN0YXJ0OiAwO1xuICBoZWlnaHQ6IDcwcHg7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIHBhZGRpbmctdG9wOiAwO1xuICBwYWRkaW5nLWJvdHRvbTogMDtcbiAgcGFkZGluZy1yaWdodDogMDtcbn1cbi5saW5lLWlucHV0ICNpdGVtX2FyZWEgaW9uLXRleHRhcmVhIHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgY29sb3I6ICM2Nzk3MzM7XG59XG5cbi5pdGVtX2lucHV0IHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgY29sb3I6ICM2Nzk3MzM7XG4gIC0tcGFkZGluZy10b3A6IDA7XG4gIG1hcmdpbi10b3A6IC01cHg7XG59XG5cbi5lZGl0X3R4dCB7XG4gIG1hcmdpbi10b3A6IC03cHg7XG4gIGZvbnQtc2l6ZTogMTJweDtcbn1cblxuLmVkaXRfaWNvbiB7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgbWFyZ2luLXRvcDogMHB4O1xuICBtYXJnaW4tbGVmdDogMnB4O1xufVxuXG4uaXRlbV9zZWxlY3Qge1xuICBmb250LXNpemU6IDE0cHg7XG4gIC0tcGFkZGluZy10b3A6IDElO1xuICBjb2xvcjogIzQyNDI0MiAhaW1wb3J0YW50O1xuICBtYXJnaW4tdG9wOiAtNXB4O1xufVxuXG4uaXRlbV9sYWJlbCB7XG4gIGZvbnQtd2VpZ2h0OiAzMDA7XG4gIHBhZGRpbmc6IDA7XG4gIG1hcmdpbi10b3A6IDhweDtcbiAgZm9udC1zaXplOiAxM3B4O1xufVxuXG4uZm9yX3NlbGF2YWlsYWJpbGl0eSB7XG4gIG1heC13aWR0aDogMTAwJTtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgY29sb3I6ICM2Nzk3MzM7XG4gIG1hcmdpbi10b3A6IC0xMXB4O1xufVxuXG4uYmxvY2sge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGJveC1zaGFkb3c6IG5vbmU7XG4gIG1hcmdpbjogMDtcbn1cbi5ibG9jayAuZGVsZXRlSWNvbiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZSAhaW1wb3J0YW50O1xuICBjb2xvcjogI2ZmZmZmZiAhaW1wb3J0YW50O1xuICByaWdodDogNSU7XG4gIHRvcDogMyU7XG4gIGZvbnQtc2l6ZTogMjBweDtcbn1cbi5ibG9jayAuZGVsZXRlSWNvbjpiZWZvcmUge1xuICBmb250LXNpemU6IDMwcHggIWltcG9ydGFudDtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/edititem/edititem.page.ts":
/*!*******************************************!*\
  !*** ./src/app/edititem/edititem.page.ts ***!
  \*******************************************/
/*! exports provided: EdititemPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EdititemPage", function() { return EdititemPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _shared_model_item_model__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../shared/model/item.model */ "./src/app/shared/model/item.model.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "./node_modules/@ionic-native/camera/ngx/index.js");
/* harmony import */ var _providers_credential_provider__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../providers/credential-provider */ "./src/providers/credential-provider.ts");
/* harmony import */ var _selectcategories_selectcategories_page__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../selectcategories/selectcategories.page */ "./src/app/selectcategories/selectcategories.page.ts");
/* harmony import */ var _ionic_native_crop_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic-native/crop/ngx */ "./node_modules/@ionic-native/crop/ngx/index.js");
/* harmony import */ var _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic-native/file/ngx */ "./node_modules/@ionic-native/file/ngx/index.js");
/* harmony import */ var _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ionic-native/file-transfer/ngx */ "./node_modules/@ionic-native/file-transfer/ngx/index.js");












let EdititemPage = class EdititemPage {
    constructor(router, postPvdr, modalController, navCtrl, toastController, alertCtrl, transfer, crop, actionSheetController, route, storage, camera, file) {
        this.router = router;
        this.postPvdr = postPvdr;
        this.modalController = modalController;
        this.navCtrl = navCtrl;
        this.toastController = toastController;
        this.alertCtrl = alertCtrl;
        this.transfer = transfer;
        this.crop = crop;
        this.actionSheetController = actionSheetController;
        this.route = route;
        this.storage = storage;
        this.camera = camera;
        this.file = file;
        this.item_id = "";
        //itemList: Item[] = [];
        this.croppedImagepath = "";
        this.isLoading = false;
        this.login_user_id = "";
        this.user_id = "";
        this.statusSave = false;
        this.ImageArray = [];
        this.item_name = "";
        this.item_category = "";
        this.item_price = "";
        this.item_seller = "";
        this.item_user_id = "";
        this.item_saveStatus = "";
        this.item_description = "";
        this.item_location = "";
        this.item_zipcode = 0;
        this.fileName = "";
        this.category_name = "";
        this.category_id = 1;
        this.pictures = [];
        this.userData = { user_id: "", token: "", imageB64: "" };
        this.rand = Math.floor(Math.random() * 1000000) + 8;
        this.route.queryParams.subscribe(params => {
            this.item_id = params["item_id"];
            console.log("params in edit item:" + JSON.stringify(params));
        });
    }
    ngOnInit() {
    }
    goBack() {
        window.history.back();
    }
    ionViewWillEnter() {
        this.plotData();
    }
    pickImage(sourceType) {
        const options = {
            quality: 100,
            sourceType: sourceType,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
        };
        this.camera.getPicture(options).then((imageData) => {
            // imageData is either a base64 encoded string or a file URI
            // this.croppedImagePath = 'data:image/jpeg;base64,' + imageData;
            this.cropImage(imageData);
        }, (err) => {
            // Handle error
        });
    }
    // takePhoto() {
    //   console.log("coming here");
    //   this.pickImage(this.camera.PictureSourceType.CAMERA);
    // }
    selectImage() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const actionSheet = yield this.actionSheetController.create({
                header: "Select Image source",
                buttons: [{
                        text: 'Load from Library',
                        handler: () => {
                            this.pickImage(this.camera.PictureSourceType.PHOTOLIBRARY);
                        }
                    },
                    {
                        text: 'Use Camera',
                        handler: () => {
                            this.pickImage(this.camera.PictureSourceType.CAMERA);
                        }
                    },
                    {
                        text: 'Cancel',
                        role: 'cancel'
                    }
                ]
            });
            yield actionSheet.present();
        });
    }
    plotData() {
        this.storage.get('greenthumb_user_id').then((user_id) => {
            this.login_user_id = user_id;
            let body = {
                action: 'viewPostItem',
                item_id: this.item_id,
                user_id: user_id
            };
            console.log(JSON.stringify(body));
            this.postPvdr.postData(body, 'post_item.php').subscribe((data) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                if (data.success) {
                    const items = [];
                    //var pictureProfile: string = '';
                    for (const key in data.result) {
                        this.item_name = data.result[key].title;
                        this.item_category = data.result[key].category;
                        this.item_seller = data.result[key].username;
                        this.item_user_id = data.result[key].user_id;
                        this.item_price = data.result[key].price;
                        this.item_location = data.result[key].location;
                        this.item_zipcode = data.result[key].zip_code;
                        this.item_saveStatus = data.result[key].saveStatus;
                        this.item_description = data.result[key].description;
                        this.picture = data.result[key].profile_photo;
                        this.complete_pic = this.postPvdr.myServer() + "/greenthumb/images/" + this.picture;
                    }
                }
            }));
            let body4 = {
                action: 'getItemPhotos',
                item_id: this.item_id,
            };
            console.log(JSON.stringify(body4));
            this.postPvdr.postData(body4, 'post_item.php').subscribe((data) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                if (data.success) {
                    const itemsImage = [];
                    //var pictureProfile: string = '';
                    for (const key in data.result) {
                        itemsImage.push(new _shared_model_item_model__WEBPACK_IMPORTED_MODULE_4__["ItemPicture"](this.postPvdr.myServer() + "/greenthumb/images/posted_items/images/" + data.result[key].item_photo));
                        console.log("item_photo:" + data.result[key].item_photo);
                    }
                    this.ImageArray = itemsImage;
                }
            }));
        });
    }
    cropImage(fileUrl) {
        this.crop.crop(fileUrl, { quality: 50 })
            .then(newPath => {
            this.showCroppedImage(newPath.split('?')[0]);
        }, error => {
            alert('Error cropping image' + error);
        });
    }
    goSellfaster() {
        //this.router.navigate(['sellfaster']);
        let navigationExtras = {
            queryParams: { item_id: this.item_id }
        };
        this.navCtrl.navigateForward(['sellfaster'], navigationExtras);
    }
    showCroppedImage(ImagePath) {
        this.isLoading = true;
        var copyPath = ImagePath;
        var splitPath = copyPath.split('/');
        var imageName = splitPath[splitPath.length - 1];
        var filePath = ImagePath.split(imageName)[0];
        this.file.readAsDataURL(filePath, imageName).then(base64 => {
            this.croppedImagepath = base64;
            this.base64Image = base64;
            // this.photos.push(this.base64Image);
            // this.photos.reverse();
            this.sendData(base64);
            this.isLoading = false;
        }, error => {
            alert('Error in showing image' + error);
            this.isLoading = false;
        });
    }
    sendData(imageData) {
        const fileTransfer = this.transfer.create();
        this.rand = Math.floor(Math.random() * 1000000) + 8;
        this.fileName = "greenthumb_" + this.rand + "_" + this.login_user_id + ".jpg";
        console.log("this.filename for transfer:" + this.fileName);
        this.userData.imageB64 = imageData;
        this.userData.user_id = "1";
        this.userData.token = "222";
        //console.log(this.userData);
        // console.log("image:"+this.base64Image);
        let options = {
            fileKey: 'photo',
            fileName: this.fileName,
            chunkedMode: false,
            httpMethod: 'post',
            mimeType: "image/jpeg",
            headers: {}
        };
        fileTransfer.upload(this.base64Image, this.postPvdr.myServer() + '/greenthumb/images/posted_items/images_upload.php', options)
            .then((data) => {
            this.ImageArray.push(new _shared_model_item_model__WEBPACK_IMPORTED_MODULE_4__["ItemPicture"](this.postPvdr.myServer() + '/greenthumb/images/posted_items/images/' + this.fileName));
            // this.storage.set("greenthumb_itempost_pic",this.pictures);
            let body = {
                action: 'updateItemPhoto',
                item_id: this.item_id,
                item_photo: this.fileName
            };
            this.postPvdr.postData(body, 'post_item.php').subscribe((data) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                if (data.success) {
                }
            }));
        }, (err) => {
            console.log(err);
            alert("Please check your internet connection and try again.");
        });
        // this.authService.postData(this.userData, "userImage").then(
        //   result => {
        //     this.responseData = result;
        //   },
        //   err => {
        //   }
        // );
    }
    deletePhoto(item_photo, index) {
        this.alertCtrl.create({
            header: "Sure you want to delete this photo? There is NO undo!",
            message: "",
            buttons: [
                {
                    text: "No",
                    handler: () => {
                        console.log("Disagree clicked");
                    }
                },
                {
                    text: "Yes",
                    handler: () => {
                        console.log("Agree clicked");
                        this.ImageArray.splice(index, 1);
                        this.confirmDelete(item_photo);
                    }
                }
            ]
        }).then(res => {
            res.present();
        });
    }
    confirmDelete(index) {
        var res = index.split("/posted_items/images/");
        // this.livestreamId = res[0];
        var item_photo = res[1];
        console.log("item_photo:" + item_photo);
        let body4 = {
            action: 'deleteItemPhoto',
            item_photo
            // user_id : user_id
        };
        this.postPvdr.postData(body4, 'save_item.php').subscribe((data) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            if (data.success) {
            }
        }));
    }
    saveEdit() {
        let body = {
            action: 'updateItem',
            item_id: this.item_id,
            item_name: this.item_name,
            item_price: this.item_price,
            item_category: this.category_id,
            item_description: this.item_description,
            item_location: this.item_location,
            item_zipcode: this.item_zipcode,
            user_id: this.login_user_id
        };
        console.log(JSON.stringify(body));
        this.postPvdr.postData(body, 'post_item.php').subscribe((data) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            if (data.success) {
                this.presentToast();
            }
        }));
    }
    presentToast() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: 'Item has been updated.',
                duration: 3000
            });
            toast.present();
        });
    }
    goCategories() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            //this.router.navigate(['categories']);
            const modal = yield this.modalController.create({
                component: _selectcategories_selectcategories_page__WEBPACK_IMPORTED_MODULE_8__["SelectcategoriesPage"],
                cssClass: 'categories',
                id: 'myModal',
            });
            modal.onDidDismiss()
                .then((data) => {
                console.log("data:" + JSON.stringify(data));
                this.category_id = data['data'].id; // Here's your selected category!
                this.item_category = data['data'].value;
                //console.log("category_name:"+this.category_name);
            });
            return yield modal.present();
        });
    }
};
EdititemPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _providers_credential_provider__WEBPACK_IMPORTED_MODULE_7__["PostProvider"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"] },
    { type: _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_11__["FileTransfer"] },
    { type: _ionic_native_crop_ngx__WEBPACK_IMPORTED_MODULE_9__["Crop"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ActionSheetController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"] },
    { type: _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_6__["Camera"] },
    { type: _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_10__["File"] }
];
EdititemPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-edititem',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./edititem.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/edititem/edititem.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./edititem.page.scss */ "./src/app/edititem/edititem.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
        _providers_credential_provider__WEBPACK_IMPORTED_MODULE_7__["PostProvider"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"],
        _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_11__["FileTransfer"],
        _ionic_native_crop_ngx__WEBPACK_IMPORTED_MODULE_9__["Crop"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ActionSheetController"],
        _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
        _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"],
        _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_6__["Camera"],
        _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_10__["File"]])
], EdititemPage);



/***/ })

}]);
//# sourceMappingURL=edititem-edititem-module-es2015.js.map