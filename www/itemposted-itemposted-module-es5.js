function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["itemposted-itemposted-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/itemposted/itemposted.page.html":
  /*!***************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/itemposted/itemposted.page.html ***!
    \***************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppItempostedItempostedPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header mode=\"ios\" no-border>\n  <ion-toolbar mode=\"ios\">\n      <ion-buttons slot=\"secondary\" (click)=\"goBack()\">\n        <ion-button mode=\"ios\" style=\"font-size: 15px;\">\n            Back\n        </ion-button>\n      </ion-buttons>\n\n      <ion-title color=\"secondary\">Posted!</ion-title>\n\n      <ion-buttons slot=\"primary\" (click)=\"goHelp()\">\n        <ion-button mode=\"ios\" style=\"font-size: 15px;\">\n            Help\n        </ion-button>\n      </ion-buttons>\n    </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-row padding style=\"padding-bottom: 0;\">\n    <ion-col text-center>\n      <ion-button style=\"height: 40px;\" mode=\"ios\" (click)=\"goSellfaster()\">\n        <ion-icon name=\"trending-up\" mode=\"ios\"></ion-icon>&nbsp;&nbsp;SELL FASTER</ion-button>\n      <p style=\"padding: 0;font-size: 13px;\">Soon thousands of local buyers will see this item! \n        Get an average of 14 times more views each day.</p>\n        \n    </ion-col>\n  </ion-row>\n  <ion-card mode=\"ios\" >\n    <ion-card mode=\"ios\" no-margin class=\"for_card\" style=\"box-shadow:none\">\n      <ion-card-header class=\"for_cardheader\">\n        <img *ngIf=\"!ImageArray\" src=\"assets/greenthumb-images/tomato.png\" class=\"for_imgfruit\"> \n\n        <ion-slides *ngIf=\"ImageArray\" [options]=\"slideOptsOne\" pager=\"true\" mode=\"ios\" id=\"slide_forpromo\">\n          <ion-slide *ngFor=\"let slide of ImageArray\" mode=\"ios\">\n            <div class=\"slide\" mode=\"ios\">\n              <ion-card mode=\"ios\" class=\"promocard\">\n                <ion-card-header mode=\"ios\" class=\"promoheader\" style=\"padding: 7px;\">\n                  <ion-row>\n                  \n                    <ion-col no-padding text-right>\n                      <img src=\"{{slide.picture_filename}}\" class=\"for_imgfruit\">\n                    </ion-col>\n      \n                  </ion-row>\n                </ion-card-header>\n              </ion-card>\n            </div>\n          </ion-slide>\n        </ion-slides>\n      </ion-card-header>\n    </ion-card>\n    <ion-row>\n      <ion-col text-right>\n        <ion-chip mode=\"ios\" (click)=\"goSellerprof()\" style=\"--background: white; margin-top: -35px;\">\n          <ion-avatar>\n            <div *ngIf=\"!picture\">\n              <img src=\"assets/greenthumb-images/userpic.png\">\n            </div>\n            <div *ngIf=\"picture\">\n              <img [src] = \"complete_pic\">\n            </div>\n          </ion-avatar>\n          <ion-label style=\"font-size: 12px;\" >by {{username}}</ion-label>\n        </ion-chip>\n      </ion-col>\n    </ion-row>\n    <ion-row padding style=\"padding-top: 10px;padding-bottom: 0;\">\n      <ion-col text-left size=\"7\">\n        <ion-label color=\"dark\"><h1 style=\"margin-bottom: -2px;\"><b>{{title}}</b></h1></ion-label>\n        <ion-label class=\"for_categname\">{{category_name}}</ion-label>\n      </ion-col>\n      <ion-col text-right size=\"5\">\n        <ion-label color=\"secondary\"><h1 class=\"for_pr\"><b>${{price}}</b></h1></ion-label>\n      </ion-col>\n    </ion-row>\n    <ion-row style=\"margin-top: -5px;padding-top: 0;padding-bottom: 0;\" padding >\n      <ion-col size=\"12\" no-padding text-left> \n        <ion-icon class=\"for_pin\" color=\"primary\" name=\"pin\" mode=\"ios\"></ion-icon>\n        <ion-label class=\"for_locname\" style=\"font-size: 13px;text-transform:capitalize;\" color=\"dark\">\n          {{location}} {{district_area}} {{zip_code}}</ion-label>\n      </ion-col>\n    </ion-row>\n    <ion-row padding style=\"padding-top: 0;padding-bottom: 5px;\">\n      <ion-col>\n        <ion-item mode=\"ios\" class=\"for_iteminfo\">\n          <ion-label color=\"dark\" class=\"for_label\"><b>Quantity</b></ion-label>\n          <ion-label color=\"medium\" slot=\"end\" class=\"for_text\">{{quantity}}</ion-label>\n        </ion-item>\n        <ion-item mode=\"ios\" class=\"for_iteminforest\">\n          <ion-label color=\"dark\" class=\"for_label\"><b>Unit</b></ion-label>\n          <ion-label color=\"medium\" slot=\"end\" class=\"for_text\">{{stocks_name}}</ion-label>\n        </ion-item>\n        <ion-item mode=\"ios\" class=\"for_iteminforest\">\n          <ion-label color=\"dark\" class=\"for_label\"><b>Delivery Type</b></ion-label>\n          <ion-label color=\"medium\" slot=\"end\" class=\"for_text\">{{delivery_type}}</ion-label>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n    <ion-row padding style=\"padding-top: 0;\">\n      <ion-col no-padding>\n        <p text-justify class=\"for_txtoverview\">\n          {{description}}\n        </p>\n      </ion-col>\n    </ion-row>\n  </ion-card>\n\n</ion-content>\n<div id=\"overlay\" padding>\n  <ion-row>\n    <ion-col>\n      <ion-button expand=\"block\" (click)=\"goHome()\" mode=\"ios\" color=\"primary\">\n        DONE\n      </ion-button>\n    </ion-col>\n  </ion-row>\n  <ion-row style=\"margin-top: -8px;\">\n    <ion-col>\n      <ion-button expand=\"block\" (click)=\"goPostagain()\" mode=\"ios\" fill=\"outline\" color=\"primary\">\n        POST ANOTHER ITEM\n      </ion-button>\n    </ion-col>\n  </ion-row>\n</div>\n";
    /***/
  },

  /***/
  "./src/app/itemposted/itemposted-routing.module.ts":
  /*!*********************************************************!*\
    !*** ./src/app/itemposted/itemposted-routing.module.ts ***!
    \*********************************************************/

  /*! exports provided: ItempostedPageRoutingModule */

  /***/
  function srcAppItempostedItempostedRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ItempostedPageRoutingModule", function () {
      return ItempostedPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _itemposted_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./itemposted.page */
    "./src/app/itemposted/itemposted.page.ts");

    var routes = [{
      path: '',
      component: _itemposted_page__WEBPACK_IMPORTED_MODULE_3__["ItempostedPage"]
    }];

    var ItempostedPageRoutingModule = function ItempostedPageRoutingModule() {
      _classCallCheck(this, ItempostedPageRoutingModule);
    };

    ItempostedPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], ItempostedPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/itemposted/itemposted.module.ts":
  /*!*************************************************!*\
    !*** ./src/app/itemposted/itemposted.module.ts ***!
    \*************************************************/

  /*! exports provided: ItempostedPageModule */

  /***/
  function srcAppItempostedItempostedModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ItempostedPageModule", function () {
      return ItempostedPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _itemposted_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./itemposted-routing.module */
    "./src/app/itemposted/itemposted-routing.module.ts");
    /* harmony import */


    var _itemposted_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./itemposted.page */
    "./src/app/itemposted/itemposted.page.ts");

    var ItempostedPageModule = function ItempostedPageModule() {
      _classCallCheck(this, ItempostedPageModule);
    };

    ItempostedPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _itemposted_routing_module__WEBPACK_IMPORTED_MODULE_5__["ItempostedPageRoutingModule"]],
      declarations: [_itemposted_page__WEBPACK_IMPORTED_MODULE_6__["ItempostedPage"]]
    })], ItempostedPageModule);
    /***/
  },

  /***/
  "./src/app/itemposted/itemposted.page.scss":
  /*!*************************************************!*\
    !*** ./src/app/itemposted/itemposted.page.scss ***!
    \*************************************************/

  /*! exports provided: default */

  /***/
  function srcAppItempostedItempostedPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".for_card {\n  border-top-left-radius: 0;\n  border-top-right-radius: 0;\n  border-bottom-right-radius: 0px;\n  border-bottom-left-radius: 30px;\n}\n\n.for_cardheader {\n  background: linear-gradient(135deg, #e2f0cb 8%, #e2f0cb 83%);\n  padding-top: 0;\n}\n\n.for_editimage {\n  --background: white;\n  margin-top: -25px;\n  margin-right: 0;\n  border: 1px solid #e2f0cb;\n}\n\n.for_pr {\n  margin-bottom: -2px;\n  font-size: 35px;\n}\n\n.for_social {\n  zoom: 1.5;\n  background: #76c961;\n  border-radius: 25px;\n  padding: 4px;\n}\n\n.for_iteminfo {\n  --padding-start:0;\n  font-size: 14px;\n  --background: transparent;\n}\n\n.for_iteminforest {\n  --padding-start:0;\n  font-size: 14px;\n  margin-top: -10px;\n  --background: transparent;\n}\n\n.for_label {\n  margin-bottom: -2px;\n}\n\n.for_text {\n  margin-bottom: -2px;\n  text-align: right;\n  margin-right: 0;\n}\n\n.for_pin {\n  margin-bottom: -3px;\n}\n\n.for_txtoverview {\n  font-size: 13px;\n  padding-left: 5px;\n  margin-top: 5px;\n  padding-right: 5px;\n  color: #666666;\n}\n\n.for_back {\n  position: absolute;\n  left: 2%;\n  top: 5%;\n  font-size: 20px;\n}\n\n#overlay {\n  width: 100%;\n  height: 150px;\n  background: white;\n  z-index: 20;\n  bottom: 0%;\n  left: 0;\n  border-top: 1px solid #e2e2e2;\n}\n\nion-avatar {\n  width: 100% !important;\n  max-width: 25px !important;\n}\n\nion-avatar img {\n  border: 1px solid rgba(0, 0, 0, 0.12);\n  width: 100% !important;\n  height: 100% !important;\n  max-width: 25px !important;\n  max-height: 25px !important;\n  border-radius: 50%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZW5kcmljay9EZXNrdG9wL1RlbG1vIFNvbHV0aW9ucy9Gb3JQcm9kdWN0aW9uL21heTEwMjAyMS9sYXN0IHNldHVwL2dyZWVudGh1bWJhcHAgY29weS9zcmMvYXBwL2l0ZW1wb3N0ZWQvaXRlbXBvc3RlZC5wYWdlLnNjc3MiLCJzcmMvYXBwL2l0ZW1wb3N0ZWQvaXRlbXBvc3RlZC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0U7RUFDRSx5QkFBQTtFQUNBLDBCQUFBO0VBQ0EsK0JBQUE7RUFDQSwrQkFBQTtBQ0FKOztBREVFO0VBR0UsNERBQUE7RUFDQSxjQUFBO0FDQ0o7O0FEQ0U7RUFDRSxtQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLHlCQUFBO0FDRUo7O0FEQUU7RUFDRSxtQkFBQTtFQUFvQixlQUFBO0FDSXhCOztBREZFO0VBQ0UsU0FBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0FDS0o7O0FESEU7RUFDRSxpQkFBQTtFQUFrQixlQUFBO0VBQ2xCLHlCQUFBO0FDT0o7O0FETEU7RUFDRSxpQkFBQTtFQUFrQixlQUFBO0VBQ2xCLGlCQUFBO0VBQ0EseUJBQUE7QUNTSjs7QURQRTtFQUNFLG1CQUFBO0FDVUo7O0FEUkU7RUFDRSxtQkFBQTtFQUFvQixpQkFBQTtFQUNwQixlQUFBO0FDWUo7O0FEVkE7RUFDRSxtQkFBQTtBQ2FGOztBRFZBO0VBQ0ksZUFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsY0FBQTtBQ2FKOztBRFhBO0VBQ0Usa0JBQUE7RUFDQSxRQUFBO0VBQ0EsT0FBQTtFQUNBLGVBQUE7QUNjRjs7QURaQTtFQUNFLFdBQUE7RUFDQSxhQUFBO0VBQ0EsaUJBQUE7RUFDQSxXQUFBO0VBQ0EsVUFBQTtFQUNBLE9BQUE7RUFDQSw2QkFBQTtBQ2VGOztBRGJBO0VBQ0Usc0JBQUE7RUFDQSwwQkFBQTtBQ2dCRjs7QURmRTtFQUNFLHFDQUFBO0VBQ0Esc0JBQUE7RUFDQSx1QkFBQTtFQUNBLDBCQUFBO0VBQ0EsMkJBQUE7RUFDQSxrQkFBQTtBQ2lCSiIsImZpbGUiOiJzcmMvYXBwL2l0ZW1wb3N0ZWQvaXRlbXBvc3RlZC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcbiAgLmZvcl9jYXJke1xuICAgIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDA7XG4gICAgYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDA7XG4gICAgYm9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6IDBweDtcbiAgICBib3JkZXItYm90dG9tLWxlZnQtcmFkaXVzOiAzMHB4O1xuICB9XG4gIC5mb3JfY2FyZGhlYWRlciB7XG4gICAgYmFja2dyb3VuZDogLW1vei1saW5lYXItZ3JhZGllbnQoLTQ1ZGVnLCNlMmYwY2IgOCUsICNlMmYwY2IgODMlKTtcbiAgICBiYWNrZ3JvdW5kOiAtd2Via2l0LWxpbmVhci1ncmFkaWVudCgtNDVkZWcsI2UyZjBjYiA4JSwgI2UyZjBjYiA4MyUpO1xuICAgIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCgxMzVkZWcsICNlMmYwY2IgOCUsICNlMmYwY2IgODMlKTtcbiAgICBwYWRkaW5nLXRvcDogMDtcbiAgfVxuICAuZm9yX2VkaXRpbWFnZXtcbiAgICAtLWJhY2tncm91bmQ6IHdoaXRlO1xuICAgIG1hcmdpbi10b3A6IC0yNXB4O1xuICAgIG1hcmdpbi1yaWdodDogMDtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjZTJmMGNiO1xuICB9XG4gIC5mb3JfcHJ7XG4gICAgbWFyZ2luLWJvdHRvbTogLTJweDtmb250LXNpemU6IDM1cHg7XG4gIH1cbiAgLmZvcl9zb2NpYWx7XG4gICAgem9vbTogMS41O1xuICAgIGJhY2tncm91bmQ6ICM3NmM5NjE7XG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcbiAgICBwYWRkaW5nOiA0cHg7XG4gIH1cbiAgLmZvcl9pdGVtaW5mb3tcbiAgICAtLXBhZGRpbmctc3RhcnQ6MDtmb250LXNpemU6IDE0cHg7XG4gICAgLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgfVxuICAuZm9yX2l0ZW1pbmZvcmVzdHtcbiAgICAtLXBhZGRpbmctc3RhcnQ6MDtmb250LXNpemU6IDE0cHg7XG4gICAgbWFyZ2luLXRvcDogLTEwcHg7XG4gICAgLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgfVxuICAuZm9yX2xhYmVse1xuICAgIG1hcmdpbi1ib3R0b206IC0ycHg7XG4gIH1cbiAgLmZvcl90ZXh0e1xuICAgIG1hcmdpbi1ib3R0b206IC0ycHg7dGV4dC1hbGlnbjogcmlnaHQ7XG4gICAgbWFyZ2luLXJpZ2h0OiAwO1xuICB9XG4uZm9yX3BpbntcbiAgbWFyZ2luLWJvdHRvbTogLTNweDtcbn1cblxuLmZvcl90eHRvdmVydmlld3tcbiAgICBmb250LXNpemU6IDEzcHg7XG4gICAgcGFkZGluZy1sZWZ0OiA1cHg7XG4gICAgbWFyZ2luLXRvcDogNXB4O1xuICAgIHBhZGRpbmctcmlnaHQ6IDVweDtcbiAgICBjb2xvcjogIzY2NjY2Njtcbn1cbi5mb3JfYmFja3tcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiAyJTtcbiAgdG9wOiA1JTtcbiAgZm9udC1zaXplOiAyMHB4O1xufVxuI292ZXJsYXl7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDE1MHB4O1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgei1pbmRleDogMjA7XG4gIGJvdHRvbTogMCU7XG4gIGxlZnQ6IDA7XG4gIGJvcmRlci10b3A6IDFweCBzb2xpZCAjZTJlMmUyO1xufVxuaW9uLWF2YXRhciAgeyAgICAgXG4gIHdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XG4gIG1heC13aWR0aDogMjVweCAhaW1wb3J0YW50O1xuICBpbWd7XG4gICAgYm9yZGVyOiAxcHggc29saWQgcmdiYSgwLCAwLCAwLCAwLjEyKTtcbiAgICB3aWR0aDogMTAwJSAhaW1wb3J0YW50O1xuICAgIGhlaWdodDogMTAwJSAhaW1wb3J0YW50O1xuICAgIG1heC13aWR0aDogMjVweCAhaW1wb3J0YW50O1xuICAgIG1heC1oZWlnaHQ6IDI1cHggIWltcG9ydGFudDtcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gIH1cbn0iLCIuZm9yX2NhcmQge1xuICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiAwO1xuICBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogMDtcbiAgYm9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6IDBweDtcbiAgYm9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czogMzBweDtcbn1cblxuLmZvcl9jYXJkaGVhZGVyIHtcbiAgYmFja2dyb3VuZDogLW1vei1saW5lYXItZ3JhZGllbnQoLTQ1ZGVnLCAjZTJmMGNiIDglLCAjZTJmMGNiIDgzJSk7XG4gIGJhY2tncm91bmQ6IC13ZWJraXQtbGluZWFyLWdyYWRpZW50KC00NWRlZywgI2UyZjBjYiA4JSwgI2UyZjBjYiA4MyUpO1xuICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoMTM1ZGVnLCAjZTJmMGNiIDglLCAjZTJmMGNiIDgzJSk7XG4gIHBhZGRpbmctdG9wOiAwO1xufVxuXG4uZm9yX2VkaXRpbWFnZSB7XG4gIC0tYmFja2dyb3VuZDogd2hpdGU7XG4gIG1hcmdpbi10b3A6IC0yNXB4O1xuICBtYXJnaW4tcmlnaHQ6IDA7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNlMmYwY2I7XG59XG5cbi5mb3JfcHIge1xuICBtYXJnaW4tYm90dG9tOiAtMnB4O1xuICBmb250LXNpemU6IDM1cHg7XG59XG5cbi5mb3Jfc29jaWFsIHtcbiAgem9vbTogMS41O1xuICBiYWNrZ3JvdW5kOiAjNzZjOTYxO1xuICBib3JkZXItcmFkaXVzOiAyNXB4O1xuICBwYWRkaW5nOiA0cHg7XG59XG5cbi5mb3JfaXRlbWluZm8ge1xuICAtLXBhZGRpbmctc3RhcnQ6MDtcbiAgZm9udC1zaXplOiAxNHB4O1xuICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xufVxuXG4uZm9yX2l0ZW1pbmZvcmVzdCB7XG4gIC0tcGFkZGluZy1zdGFydDowO1xuICBmb250LXNpemU6IDE0cHg7XG4gIG1hcmdpbi10b3A6IC0xMHB4O1xuICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xufVxuXG4uZm9yX2xhYmVsIHtcbiAgbWFyZ2luLWJvdHRvbTogLTJweDtcbn1cblxuLmZvcl90ZXh0IHtcbiAgbWFyZ2luLWJvdHRvbTogLTJweDtcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gIG1hcmdpbi1yaWdodDogMDtcbn1cblxuLmZvcl9waW4ge1xuICBtYXJnaW4tYm90dG9tOiAtM3B4O1xufVxuXG4uZm9yX3R4dG92ZXJ2aWV3IHtcbiAgZm9udC1zaXplOiAxM3B4O1xuICBwYWRkaW5nLWxlZnQ6IDVweDtcbiAgbWFyZ2luLXRvcDogNXB4O1xuICBwYWRkaW5nLXJpZ2h0OiA1cHg7XG4gIGNvbG9yOiAjNjY2NjY2O1xufVxuXG4uZm9yX2JhY2sge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGxlZnQ6IDIlO1xuICB0b3A6IDUlO1xuICBmb250LXNpemU6IDIwcHg7XG59XG5cbiNvdmVybGF5IHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTUwcHg7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICB6LWluZGV4OiAyMDtcbiAgYm90dG9tOiAwJTtcbiAgbGVmdDogMDtcbiAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICNlMmUyZTI7XG59XG5cbmlvbi1hdmF0YXIge1xuICB3aWR0aDogMTAwJSAhaW1wb3J0YW50O1xuICBtYXgtd2lkdGg6IDI1cHggIWltcG9ydGFudDtcbn1cbmlvbi1hdmF0YXIgaW1nIHtcbiAgYm9yZGVyOiAxcHggc29saWQgcmdiYSgwLCAwLCAwLCAwLjEyKTtcbiAgd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcbiAgaGVpZ2h0OiAxMDAlICFpbXBvcnRhbnQ7XG4gIG1heC13aWR0aDogMjVweCAhaW1wb3J0YW50O1xuICBtYXgtaGVpZ2h0OiAyNXB4ICFpbXBvcnRhbnQ7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/itemposted/itemposted.page.ts":
  /*!***********************************************!*\
    !*** ./src/app/itemposted/itemposted.page.ts ***!
    \***********************************************/

  /*! exports provided: ItempostedPage */

  /***/
  function srcAppItempostedItempostedPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ItempostedPage", function () {
      return ItempostedPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var _providers_credential_provider__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../providers/credential-provider */
    "./src/providers/credential-provider.ts");
    /* harmony import */


    var _shared_model_item_model__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../shared/model/item.model */
    "./src/app/shared/model/item.model.ts");

    var ItempostedPage = /*#__PURE__*/function () {
      function ItempostedPage(router, storage, postPvdr, navCtrl, route) {
        var _this = this;

        _classCallCheck(this, ItempostedPage);

        this.router = router;
        this.storage = storage;
        this.postPvdr = postPvdr;
        this.navCtrl = navCtrl;
        this.route = route;
        this.username = "";
        this.ImageArray = [];
        this.slideOptsOne = {
          initialSlide: 0,
          slidesPerView: 1,
          autoplay: true
        };
        this.route.queryParams.subscribe(function (params) {
          _this.item_id = params["item_id"];
          console.log("item_id itemposted:" + _this.item_id);
        });
      }

      _createClass(ItempostedPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "ionViewWillEnter",
        value: function ionViewWillEnter() {
          var _this2 = this;

          this.storage.get("greenthumb_user_id").then(function (user_id) {
            _this2.storage.get("greenthumb_title").then(function (title) {
              _this2.storage.get("greenthumb_description").then(function (val) {
                _this2.storage.get("greenthumb_price").then(function (price) {
                  _this2.storage.get("greenthumb_location").then(function (item) {
                    _this2.user_id = user_id;
                    _this2.category_id = val['category_id'];
                    _this2.category_name = val['category_name'];
                    _this2.stocks_id = val['stocks_id'];
                    _this2.stocks_name = val['stocks_name'];
                    _this2.quantity = val['quantity'];
                    _this2.title = title;
                    _this2.description = val['quantity'];
                    _this2.price = price;
                    _this2.zip_code = item['zip_code'];
                    _this2.location = item['location'];
                    _this2.district_area = item['district_area'];
                    _this2.delivery_type_id = item['delivery_type_id'];
                    _this2.delivery_type = item['delivery_type'];
                  });
                });
              });
            });

            var body = {
              action: 'getUsername',
              user_id: user_id
            };
            console.log(JSON.stringify(body));

            _this2.postPvdr.postData(body, 'user.php').subscribe(function (data) {
              return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                return regeneratorRuntime.wrap(function _callee$(_context) {
                  while (1) {
                    switch (_context.prev = _context.next) {
                      case 0:
                        this.username = data.username;
                        this.picture = data.profile_photo;
                        this.complete_pic = this.postPvdr.myServer() + "/greenthumb/images/" + this.picture;

                      case 3:
                      case "end":
                        return _context.stop();
                    }
                  }
                }, _callee, this);
              }));
            });
          });
          this.storage.get("greenthumb_itempost_pic").then(function (pictures) {
            for (var key in pictures) {
              // console.log("filenames:"+pictures[key].picture_filename)
              _this2.ImageArray.push(new _shared_model_item_model__WEBPACK_IMPORTED_MODULE_6__["ItemPicture"](_this2.postPvdr.myServer() + "/greenthumb/images/posted_items/images/" + pictures[key].picture_filename));
            }
          });
        }
      }, {
        key: "goHelp",
        value: function goHelp() {
          this.router.navigate(['tabs/help']);
        }
      }, {
        key: "goSellerprof",
        value: function goSellerprof() {}
      }, {
        key: "goSellfaster",
        value: function goSellfaster() {
          //this.router.navigate(['sellfaster']);
          var navigationExtras = {
            queryParams: {
              item_id: this.item_id
            }
          };
          this.navCtrl.navigateForward(['sellfaster'], navigationExtras);
        }
      }, {
        key: "goBack",
        value: function goBack() {//this.navCtrl.navigateForward((['shareitem']), { animated: false, });
        }
      }, {
        key: "goHome",
        value: function goHome() {
          //this.router.navigate(['tabs/tab1']);
          this.navCtrl.navigateRoot(['tabs/tab1']);
        }
      }, {
        key: "goPostagain",
        value: function goPostagain() {
          this.router.navigate(['tab3']);
        }
      }]);

      return ItempostedPage;
    }();

    ItempostedPage.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }, {
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"]
      }, {
        type: _providers_credential_provider__WEBPACK_IMPORTED_MODULE_5__["PostProvider"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
      }];
    };

    ItempostedPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-itemposted',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./itemposted.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/itemposted/itemposted.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./itemposted.page.scss */
      "./src/app/itemposted/itemposted.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"], _providers_credential_provider__WEBPACK_IMPORTED_MODULE_5__["PostProvider"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])], ItempostedPage);
    /***/
  }
}]);
//# sourceMappingURL=itemposted-itemposted-module-es5.js.map