function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["location-location-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/location/location.page.html":
  /*!***********************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/location/location.page.html ***!
    \***********************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppLocationLocationPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header mode=\"ios\" no-border>\n  <ion-toolbar mode=\"ios\">\n      <ion-buttons slot=\"secondary\" (click)=\"goBack()\">\n        <ion-button mode=\"ios\" style=\"font-size: 15px;\">\n            Back\n        </ion-button>\n      </ion-buttons>\n\n      <ion-title color=\"secondary\">Location</ion-title>\n\n      <ion-buttons slot=\"primary\">\n        <ion-button mode=\"ios\" (click)=\"resetLocation();\" style=\"font-size: 15px;\">\n            Reset\n        </ion-button>\n      </ion-buttons>\n    </ion-toolbar>\n</ion-header>\n\n<ion-content padding>\n  <ion-list class=\"for_accountitems\">\n    <ion-label class=\"item_label\" color=\"secondary\">LOCATION</ion-label>\n    <ion-item style=\"margin-top: 5px;\" mode=\"ios\" id=\"for_eachitem\">\n      <ion-label>{{myCurrentLocation}}</ion-label>\n     \n      <ion-note mode=\"ios\" (click)=\"getGeoLocation()\" color=\"secondary\" style=\"font-size: 13px;\">My Location</ion-note>\n    </ion-item>\n  </ion-list>\n\n  <div #map id=\"map\"></div>\n\n  <ion-list class=\"for_accountitems\">\n    <ion-label class=\"item_label\" color=\"secondary\">DISTANCE</ion-label>\n    <ion-label color=\"secondary\" text-center>\n      <p class=\"item_label2\">30 MILES</p>\n    </ion-label>\n    <ul class=\"range-labels\">\n      <li>5 mi</li>\n      <li>10 mi</li>\n      <li>20 mi</li>\n      <li>30 mi</li>\n      <li>Max</li>\n    </ul>\n      <ion-item mode=\"ios\" lines=\"none\" style=\"--padding-start: 0\">\n        <ion-range mode=\"ios\" min=\"1000\" max=\"2000\" step=\"250\" snaps=\"true\" color=\"secondary\" no-padding>\n        <ion-icon mode=\"ios\" slot=\"start\" size=\"large\" color=\"primary\" name=\"pin\"></ion-icon>\n        <ion-icon mode=\"ios\" slot=\"end\" color=\"primary\" name=\"car\" size=\"large\"></ion-icon>\n      </ion-range>\n    </ion-item>\n  </ion-list>\n  <ion-list style=\"margin-top: 20%;\">\n    <ion-button expand=\"block\" mode=\"ios\">APPLY</ion-button>\n  </ion-list>\n</ion-content>\n";
    /***/
  },

  /***/
  "./src/app/location/location-routing.module.ts":
  /*!*****************************************************!*\
    !*** ./src/app/location/location-routing.module.ts ***!
    \*****************************************************/

  /*! exports provided: LocationPageRoutingModule */

  /***/
  function srcAppLocationLocationRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "LocationPageRoutingModule", function () {
      return LocationPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _location_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./location.page */
    "./src/app/location/location.page.ts");

    var routes = [{
      path: '',
      component: _location_page__WEBPACK_IMPORTED_MODULE_3__["LocationPage"]
    }];

    var LocationPageRoutingModule = function LocationPageRoutingModule() {
      _classCallCheck(this, LocationPageRoutingModule);
    };

    LocationPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], LocationPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/location/location.module.ts":
  /*!*********************************************!*\
    !*** ./src/app/location/location.module.ts ***!
    \*********************************************/

  /*! exports provided: LocationPageModule */

  /***/
  function srcAppLocationLocationModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "LocationPageModule", function () {
      return LocationPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _location_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./location-routing.module */
    "./src/app/location/location-routing.module.ts");
    /* harmony import */


    var _location_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./location.page */
    "./src/app/location/location.page.ts");

    var LocationPageModule = function LocationPageModule() {
      _classCallCheck(this, LocationPageModule);
    };

    LocationPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _location_routing_module__WEBPACK_IMPORTED_MODULE_5__["LocationPageRoutingModule"]],
      declarations: [_location_page__WEBPACK_IMPORTED_MODULE_6__["LocationPage"]]
    })], LocationPageModule);
    /***/
  },

  /***/
  "./src/app/location/location.page.scss":
  /*!*********************************************!*\
    !*** ./src/app/location/location.page.scss ***!
    \*********************************************/

  /*! exports provided: default */

  /***/
  function srcAppLocationLocationPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "#map {\n  width: 100%;\n  height: 300px;\n  border: 1px solid red;\n  background-color: transparent;\n}\n\n.item_label {\n  font-size: 13px;\n  font-weight: bold;\n}\n\n.item_label2 {\n  font-size: 16px;\n  font-weight: bold;\n  padding-top: 20px;\n  padding-bottom: 20px;\n}\n\n.for_accountitems {\n  padding: 0;\n  margin-top: 5%;\n}\n\n#for_eachitem {\n  font-size: 14px;\n  --inner-padding-end: 0px !important;\n  --border-color: #e2f0cb;\n  --padding-start: 0% !important;\n}\n\n.range-labels {\n  margin: 0;\n  padding: 0;\n  list-style: none;\n}\n\n.range-labels li {\n  width: 19.3%;\n  text-align: center;\n  color: #959aad;\n  font-size: 12px;\n  display: inline-block;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZW5kcmljay9EZXNrdG9wL1RlbG1vIFNvbHV0aW9ucy9Gb3JQcm9kdWN0aW9uL21heTEwMjAyMS9sYXN0IHNldHVwL2dyZWVudGh1bWJhcHAgY29weS9zcmMvYXBwL2xvY2F0aW9uL2xvY2F0aW9uLnBhZ2Uuc2NzcyIsInNyYy9hcHAvbG9jYXRpb24vbG9jYXRpb24ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVFO0VBQ0ksV0FBQTtFQUNBLGFBQUE7RUFDQSxxQkFBQTtFQUNBLDZCQUFBO0FDRE47O0FES0E7RUFDSSxlQUFBO0VBQ0EsaUJBQUE7QUNGSjs7QURJRTtFQUNFLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGlCQUFBO0VBQ0Esb0JBQUE7QUNESjs7QURHSTtFQUNFLFVBQUE7RUFDQSxjQUFBO0FDQU47O0FERUk7RUFDRSxlQUFBO0VBQ0EsbUNBQUE7RUFDQSx1QkFBQTtFQUNBLDhCQUFBO0FDQ047O0FEQ0k7RUFDSSxTQUFBO0VBQ0EsVUFBQTtFQUNBLGdCQUFBO0FDRVI7O0FEQVE7RUFDSSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtFQUNBLHFCQUFBO0FDRVoiLCJmaWxlIjoic3JjL2FwcC9sb2NhdGlvbi9sb2NhdGlvbi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcblxuICAjbWFwIHtcbiAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgaGVpZ2h0OiAzMDBweDtcbiAgICAgIGJvcmRlcjogMXB4IHNvbGlkIHJlZDtcbiAgICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xuICAgIH1cblxuXG4uaXRlbV9sYWJlbHtcbiAgICBmb250LXNpemU6IDEzcHg7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIH1cbiAgLml0ZW1fbGFiZWwye1xuICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICBwYWRkaW5nLXRvcDogMjBweDtcbiAgICBwYWRkaW5nLWJvdHRvbTogMjBweDtcbiAgfVxuICAgIC5mb3JfYWNjb3VudGl0ZW1ze1xuICAgICAgcGFkZGluZzogMDtcbiAgICAgIG1hcmdpbi10b3A6IDUlO1xuICAgIH1cbiAgICAjZm9yX2VhY2hpdGVte1xuICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgLS1pbm5lci1wYWRkaW5nLWVuZDogMHB4ICFpbXBvcnRhbnQ7XG4gICAgICAtLWJvcmRlci1jb2xvcjogI2UyZjBjYjtcbiAgICAgIC0tcGFkZGluZy1zdGFydDogMCUgIWltcG9ydGFudDtcbiAgICB9XG4gICAgLnJhbmdlLWxhYmVscyB7XG4gICAgICAgIG1hcmdpbjogMDtcbiAgICAgICAgcGFkZGluZzogMDtcbiAgICAgICAgbGlzdC1zdHlsZTogbm9uZTtcbiAgICAgICAgXG4gICAgICAgIGxpIHtcbiAgICAgICAgICAgIHdpZHRoOiAxOS4zJTtcbiAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgICAgIGNvbG9yOiAjOTU5YWFkO1xuICAgICAgICAgICAgZm9udC1zaXplOiAxMnB4O1xuICAgICAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgICAgICB9XG4gICAgfSIsIiNtYXAge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAzMDBweDtcbiAgYm9yZGVyOiAxcHggc29saWQgcmVkO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcbn1cblxuLml0ZW1fbGFiZWwge1xuICBmb250LXNpemU6IDEzcHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuXG4uaXRlbV9sYWJlbDIge1xuICBmb250LXNpemU6IDE2cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBwYWRkaW5nLXRvcDogMjBweDtcbiAgcGFkZGluZy1ib3R0b206IDIwcHg7XG59XG5cbi5mb3JfYWNjb3VudGl0ZW1zIHtcbiAgcGFkZGluZzogMDtcbiAgbWFyZ2luLXRvcDogNSU7XG59XG5cbiNmb3JfZWFjaGl0ZW0ge1xuICBmb250LXNpemU6IDE0cHg7XG4gIC0taW5uZXItcGFkZGluZy1lbmQ6IDBweCAhaW1wb3J0YW50O1xuICAtLWJvcmRlci1jb2xvcjogI2UyZjBjYjtcbiAgLS1wYWRkaW5nLXN0YXJ0OiAwJSAhaW1wb3J0YW50O1xufVxuXG4ucmFuZ2UtbGFiZWxzIHtcbiAgbWFyZ2luOiAwO1xuICBwYWRkaW5nOiAwO1xuICBsaXN0LXN0eWxlOiBub25lO1xufVxuLnJhbmdlLWxhYmVscyBsaSB7XG4gIHdpZHRoOiAxOS4zJTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBjb2xvcjogIzk1OWFhZDtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG59Il19 */";
    /***/
  },

  /***/
  "./src/app/location/location.page.ts":
  /*!*******************************************!*\
    !*** ./src/app/location/location.page.ts ***!
    \*******************************************/

  /*! exports provided: LocationPage */

  /***/
  function srcAppLocationLocationPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "LocationPage", function () {
      return LocationPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic-native/geolocation/ngx */
    "./node_modules/@ionic-native/geolocation/ngx/index.js");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var _providers_credential_provider__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../../providers/credential-provider */
    "./src/providers/credential-provider.ts");
    /* harmony import */


    var _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ionic-native/native-geocoder/ngx */
    "./node_modules/@ionic-native/native-geocoder/ngx/index.js");

    var LocationPage = /*#__PURE__*/function () {
      function LocationPage(router, nativeGeocoder, plt, storage, postPvdr, geo) {
        _classCallCheck(this, LocationPage);

        this.router = router;
        this.nativeGeocoder = nativeGeocoder;
        this.plt = plt;
        this.storage = storage;
        this.postPvdr = postPvdr;
        this.geo = geo;
        this.myCurrentLocation = '';
        this.login_user_id = '';
        this.getCurrentLocation();
      }

      _createClass(LocationPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "goBack",
        value: function goBack() {
          window.history.back();
        }
      }, {
        key: "getCurrentLocation",
        value: function getCurrentLocation() {
          var _this = this;

          this.storage.get("greenthumb_user_id").then(function (user_id) {
            _this.login_user_id = user_id;
            var body = {
              action: 'check_location',
              user_id: user_id
            };

            _this.postPvdr.postData(body, 'user.php').subscribe(function (data) {
              return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                return regeneratorRuntime.wrap(function _callee$(_context) {
                  while (1) {
                    switch (_context.prev = _context.next) {
                      case 0:
                        if (data.success) {
                          if (data.my_location != '') {
                            this.lat = data.latitude;
                            this.lng = data.longitude;
                            this.myCurrentLocation = data.my_location;
                            this.drawMap();
                          } else {
                            this.getGeoLocation();
                          }
                        }

                      case 1:
                      case "end":
                        return _context.stop();
                    }
                  }
                }, _callee, this);
              }));
            });
          }); //this.plotData();
        }
      }, {
        key: "getGeoLocation",
        value: function getGeoLocation() {
          var _this2 = this;

          this.geo.getCurrentPosition({
            timeout: 10000,
            enableHighAccuracy: true
          }).then(function (res) {
            _this2.lat = res.coords.latitude;
            _this2.lng = res.coords.longitude;
            var options = {
              useLocale: true,
              maxResults: 5
            };

            _this2.nativeGeocoder.reverseGeocode(_this2.lat, _this2.lng, options).then(function (result) {
              console.log(JSON.stringify(result[0])); // console.log("re:"+result[0].locality);
              // console.log("are:"+result[0].subAdministrativeArea);
              // console.log("th:"+result[0].subThoroughfare);

              _this2.myCurrentLocation = result[0].thoroughfare + ' ' + result[0].locality + ', ' + result[0].subAdministrativeArea;
              var body = {
                action: 'updateLocation',
                user_id: _this2.login_user_id,
                latitude: _this2.lat,
                longitude: _this2.lng,
                my_location: _this2.myCurrentLocation
              };

              _this2.postPvdr.postData(body, 'user.php').subscribe(function (data) {
                return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
                  return regeneratorRuntime.wrap(function _callee2$(_context2) {
                    while (1) {
                      switch (_context2.prev = _context2.next) {
                        case 0:
                        case "end":
                          return _context2.stop();
                      }
                    }
                  }, _callee2);
                }));
              });
            })["catch"](function (error) {
              console.log(error);

              _this2.getGeoLocation();
            });

            _this2.drawMap();
          })["catch"](function (er) {
            _this2.getGeoLocation();
          });
        }
      }, {
        key: "drawMap",
        value: function drawMap() {
          var mapOptions = {
            zoom: 13,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            mapTypeControl: false,
            streetViewControl: false,
            fullscreenControl: false
          };
          this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
          var latLng = new google.maps.LatLng(this.lat, this.lng);
          this.map.setCenter(latLng);
          this.map.setZoom(14);
        }
      }, {
        key: "resetLocation",
        value: function resetLocation() {
          this.myCurrentLocation = '';
        }
      }, {
        key: "ionViewDidEnter",
        value: function ionViewDidEnter() {
          this.plt.ready().then(function () {
            console.log("location load:"); // let mapOptions = {
            //   zoom: 13,
            //   mapTypeId: google.maps.MapTypeId.ROADMAP,
            //   mapTypeControl: false,
            //   streetViewControl: false,
            //   fullscreenControl: false
            // }
            // this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
            // let latLng = new google.maps.LatLng(30, 30);
            // this.map.setCenter(latLng);
            // this.map.setZoom(5);
            // console.log("i go from here");
            // console.log("sdf:"+JSON.stringify(this.map));
            // this.geo.getCurrentPosition().then(pos => {
            //   let latLng = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
            //   this.map.setCenter(latLng);
            //   this.map.setZoom(16);
            //   console.log("sdf:"+JSON.stringify(this.map));
            // }).catch((error) => {
            //   console.log('Error getting location', error);
            // });
          });
        }
      }]);

      return LocationPage;
    }();

    LocationPage.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }, {
        type: _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_7__["NativeGeocoder"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"]
      }, {
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"]
      }, {
        type: _providers_credential_provider__WEBPACK_IMPORTED_MODULE_6__["PostProvider"]
      }, {
        type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_4__["Geolocation"]
      }];
    };

    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('map', {
      "static": false
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])], LocationPage.prototype, "mapElement", void 0);
    LocationPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-location',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./location.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/location/location.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./location.page.scss */
      "./src/app/location/location.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_7__["NativeGeocoder"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"], _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"], _providers_credential_provider__WEBPACK_IMPORTED_MODULE_6__["PostProvider"], _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_4__["Geolocation"]])], LocationPage);
    /***/
  }
}]);
//# sourceMappingURL=location-location-module-es5.js.map