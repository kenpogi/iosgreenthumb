(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["logoutscreen-logoutscreen-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/logoutscreen/logoutscreen.page.html":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/logoutscreen/logoutscreen.page.html ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header mode=\"ios\" no-border>\n  <ion-toolbar mode=\"ios\">\n      <ion-buttons slot=\"secondary\" (click)=\"goBack()\">\n        <ion-button mode=\"ios\" style=\"font-size: 15px;\">\n            Back\n        </ion-button>\n      </ion-buttons>\n\n      <ion-title color=\"secondary\">Logout</ion-title>\n\n      <ion-buttons slot=\"primary\" (click)=\"signOut()\">\n        <ion-button mode=\"ios\">\n          <ion-icon name=\"log-out\" mode=\"ios\"></ion-icon>\n        </ion-button>\n      </ion-buttons>\n\n    </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-card class=\"for_card\">\n  </ion-card>\n  <ion-label color=\"medium\"><p text-center>Version V900.3.5</p></ion-label>\n  <ion-list class=\"for_accountitems\" padding style=\"padding-top: 0;padding-bottom: 5px;\">\n    <ion-list-header class=\"item_label\">Info</ion-list-header>\n    <ion-item style=\"margin-top: 5px;\" mode=\"ios\" id=\"for_eachitem\">\n      <ion-label>About Green Thumb Trade</ion-label>\n      <ion-icon name=\"arrow-forward\" slot=\"end\" mode=\"ios\" class=\"for_seticon\"></ion-icon>\n    </ion-item>\n    <ion-item style=\"margin-top: 5px;\" mode=\"ios\" id=\"for_eachitem\">\n      <ion-label>About Telmo Solutions</ion-label>\n      <ion-icon name=\"arrow-forward\" slot=\"end\" mode=\"ios\" class=\"for_seticon\"></ion-icon>\n    </ion-item>\n    <ion-item style=\"margin-top: 5px;\" mode=\"ios\" id=\"for_eachitem\" (click)=\"signOut()\">\n      <ion-label>Logout</ion-label>\n      <ion-icon name=\"arrow-forward\" slot=\"end\" mode=\"ios\" class=\"for_seticon\"></ion-icon>\n    </ion-item>\n  </ion-list>\n  <p text-center style=\"padding-top: 0px;\">\n    <a  href=\"https://telmosolutions.com/\"><img src=\"assets/icon/telmologo.png\" style=\"width: 70%;\" mode=\"ios\"></a> \n   </p>\n   <p text-center style=\"margin-top: -8%;\" padding>\n     Designed & Developed by <a style=\"text-decoration: none;color: #09055e;font-weight: bold;\" href=\"https://telmosolutions.com/\">Telmo Solutions</a>\n   </p>\n  \n</ion-content>\n\n");

/***/ }),

/***/ "./src/app/logoutscreen/logoutscreen-routing.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/logoutscreen/logoutscreen-routing.module.ts ***!
  \*************************************************************/
/*! exports provided: LogoutscreenPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LogoutscreenPageRoutingModule", function() { return LogoutscreenPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _logoutscreen_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./logoutscreen.page */ "./src/app/logoutscreen/logoutscreen.page.ts");




const routes = [
    {
        path: '',
        component: _logoutscreen_page__WEBPACK_IMPORTED_MODULE_3__["LogoutscreenPage"]
    }
];
let LogoutscreenPageRoutingModule = class LogoutscreenPageRoutingModule {
};
LogoutscreenPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], LogoutscreenPageRoutingModule);



/***/ }),

/***/ "./src/app/logoutscreen/logoutscreen.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/logoutscreen/logoutscreen.module.ts ***!
  \*****************************************************/
/*! exports provided: LogoutscreenPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LogoutscreenPageModule", function() { return LogoutscreenPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _logoutscreen_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./logoutscreen-routing.module */ "./src/app/logoutscreen/logoutscreen-routing.module.ts");
/* harmony import */ var _logoutscreen_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./logoutscreen.page */ "./src/app/logoutscreen/logoutscreen.page.ts");







let LogoutscreenPageModule = class LogoutscreenPageModule {
};
LogoutscreenPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _logoutscreen_routing_module__WEBPACK_IMPORTED_MODULE_5__["LogoutscreenPageRoutingModule"]
        ],
        declarations: [_logoutscreen_page__WEBPACK_IMPORTED_MODULE_6__["LogoutscreenPage"]]
    })
], LogoutscreenPageModule);



/***/ }),

/***/ "./src/app/logoutscreen/logoutscreen.page.scss":
/*!*****************************************************!*\
  !*** ./src/app/logoutscreen/logoutscreen.page.scss ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".for_card {\n  box-shadow: none;\n  --background: none;\n  background-image: url(/assets/greenthumb-images/greenthumblogo.png);\n  background-position: center center;\n  background-repeat: no-repeat;\n  background-size: cover;\n  height: 50%;\n  margin-top: 0;\n}\n\n.item_label {\n  font-weight: bold;\n  font-size: 15px;\n  padding-left: 0;\n  color: #679733;\n  margin-bottom: -4%;\n}\n\n#for_eachitem {\n  font-size: 14px;\n  --inner-padding-end: 0px !important;\n  --border-color: #e2f0cb;\n  --padding-start: 0% !important;\n}\n\n.for_seticon {\n  font-size: 15px;\n  color: #9da2b3;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZW5kcmljay9EZXNrdG9wL1RlbG1vIFNvbHV0aW9ucy9Gb3JQcm9kdWN0aW9uL21heTEwMjAyMS9sYXN0IHNldHVwL2dyZWVudGh1bWJhcHAgY29weS9zcmMvYXBwL2xvZ291dHNjcmVlbi9sb2dvdXRzY3JlZW4ucGFnZS5zY3NzIiwic3JjL2FwcC9sb2dvdXRzY3JlZW4vbG9nb3V0c2NyZWVuLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxtRUFBQTtFQUNBLGtDQUFBO0VBQ0EsNEJBQUE7RUFDQSxzQkFBQTtFQUNBLFdBQUE7RUFDQSxhQUFBO0FDQ0o7O0FEQ0E7RUFDSSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtFQUNBLGtCQUFBO0FDRUo7O0FEQUU7RUFDRSxlQUFBO0VBQ0EsbUNBQUE7RUFDQSx1QkFBQTtFQUNBLDhCQUFBO0FDR0o7O0FEREU7RUFDRSxlQUFBO0VBQ0EsY0FBQTtBQ0lKIiwiZmlsZSI6InNyYy9hcHAvbG9nb3V0c2NyZWVuL2xvZ291dHNjcmVlbi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZm9yX2NhcmR7XG4gICAgYm94LXNoYWRvdzogbm9uZTtcbiAgICAtLWJhY2tncm91bmQ6IG5vbmU7XG4gICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKC9hc3NldHMvZ3JlZW50aHVtYi1pbWFnZXMvZ3JlZW50aHVtYmxvZ28ucG5nKTtcbiAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXIgY2VudGVyO1xuICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICBoZWlnaHQ6IDUwJTtcbiAgICBtYXJnaW4tdG9wOiAwO1xufVxuLml0ZW1fbGFiZWx7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgZm9udC1zaXplOiAxNXB4O1xuICAgIHBhZGRpbmctbGVmdDogMDtcbiAgICBjb2xvcjogIzY3OTczMztcbiAgICBtYXJnaW4tYm90dG9tOiAtNCU7XG4gIH1cbiAgI2Zvcl9lYWNoaXRlbXtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgLS1pbm5lci1wYWRkaW5nLWVuZDogMHB4ICFpbXBvcnRhbnQ7XG4gICAgLS1ib3JkZXItY29sb3I6ICNlMmYwY2I7XG4gICAgLS1wYWRkaW5nLXN0YXJ0OiAwJSAhaW1wb3J0YW50O1xuICB9XG4gIC5mb3Jfc2V0aWNvbntcbiAgICBmb250LXNpemU6IDE1cHg7XG4gICAgY29sb3I6ICM5ZGEyYjM7XG4gIH0iLCIuZm9yX2NhcmQge1xuICBib3gtc2hhZG93OiBub25lO1xuICAtLWJhY2tncm91bmQ6IG5vbmU7XG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybCgvYXNzZXRzL2dyZWVudGh1bWItaW1hZ2VzL2dyZWVudGh1bWJsb2dvLnBuZyk7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlciBjZW50ZXI7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIGhlaWdodDogNTAlO1xuICBtYXJnaW4tdG9wOiAwO1xufVxuXG4uaXRlbV9sYWJlbCB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBmb250LXNpemU6IDE1cHg7XG4gIHBhZGRpbmctbGVmdDogMDtcbiAgY29sb3I6ICM2Nzk3MzM7XG4gIG1hcmdpbi1ib3R0b206IC00JTtcbn1cblxuI2Zvcl9lYWNoaXRlbSB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgLS1pbm5lci1wYWRkaW5nLWVuZDogMHB4ICFpbXBvcnRhbnQ7XG4gIC0tYm9yZGVyLWNvbG9yOiAjZTJmMGNiO1xuICAtLXBhZGRpbmctc3RhcnQ6IDAlICFpbXBvcnRhbnQ7XG59XG5cbi5mb3Jfc2V0aWNvbiB7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgY29sb3I6ICM5ZGEyYjM7XG59Il19 */");

/***/ }),

/***/ "./src/app/logoutscreen/logoutscreen.page.ts":
/*!***************************************************!*\
  !*** ./src/app/logoutscreen/logoutscreen.page.ts ***!
  \***************************************************/
/*! exports provided: LogoutscreenPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LogoutscreenPage", function() { return LogoutscreenPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/es2015/index.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");






let LogoutscreenPage = class LogoutscreenPage {
    constructor(router, afAuth, toastController, storage) {
        this.router = router;
        this.afAuth = afAuth;
        this.toastController = toastController;
        this.storage = storage;
    }
    ngOnInit() {
    }
    goBack() {
        window.history.back();
    }
    // upload_photo(){
    //   this.router.navigate(['uploadphoto']);
    // }
    presentToast() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: 'You are now logged out.',
                duration: 3000
            });
            toast.present();
        });
    }
    signOut() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.afAuth.auth.signOut().then(() => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                yield this.storage.clear();
                this.presentToast();
                this.router.navigate(['choose']);
            }));
        });
    }
};
LogoutscreenPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__["AngularFireAuth"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ToastController"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"] }
];
LogoutscreenPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-logoutscreen',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./logoutscreen.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/logoutscreen/logoutscreen.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./logoutscreen.page.scss */ "./src/app/logoutscreen/logoutscreen.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
        _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__["AngularFireAuth"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ToastController"],
        _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"]])
], LogoutscreenPage);



/***/ })

}]);
//# sourceMappingURL=logoutscreen-logoutscreen-module-es2015.js.map