function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["mybroadcast-mybroadcast-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/mybroadcast/mybroadcast.page.html":
  /*!*****************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/mybroadcast/mybroadcast.page.html ***!
    \*****************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppMybroadcastMybroadcastPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header translucent mode=\"ios\">\n    <ion-toolbar mode=\"ios\">\n        <ion-buttons slot=\"secondary\" (click)=\"goBack()\">\n          <ion-button>\n              <ion-icon name=\"arrow-back\"  mode=\"ios\"></ion-icon>\n          </ion-button>\n        </ion-buttons>\n\n        <ion-title>My Broadcast</ion-title>\n\n        <ion-buttons slot=\"primary\">\n          <ion-button>\n              <img src=\"assets/icon/brixylogo.png\" style=\"width: 32px;\" mode=\"ios\">\n          </ion-button>\n        </ion-buttons>\n      </ion-toolbar>\n      <ion-segment style=\"padding: 5px\" color=\"secondary\" mode=\"ios\">\n          <ion-segment-button  checked layout=\"icon-start\" style=\"padding: 3px\" mode=\"ios\" value=\"0\">\n              <ion-label>My Live Broadcast</ion-label>\n              <ion-icon name=\"microphone\" mode=\"ios\"></ion-icon>\n          </ion-segment-button>\n          <ion-segment-button layout=\"icon-start\" mode=\"ios\">\n              <ion-label>My Story Archieved</ion-label>\n              <ion-icon name=\"archive\" mode=\"ios\"></ion-icon>\n          </ion-segment-button>\n      </ion-segment>\n</ion-header>\n\n<ion-content mode=\"ios\" padding style=\"padding-top: 0\">\n    <ion-row>\n        <ion-col style=\"color: black\" mode=\"ios\" text-left>\n            <ion-label><b>COINS:</b>&nbsp;&nbsp;</ion-label>\n            <ion-icon name=\"logo-bitcoin\" color=\"warning\" mode=\"ios\"></ion-icon>\n            <ion-text>0</ion-text>\n        </ion-col>\n        <ion-col style=\"color: black\" mode=\"ios\" text-right>\n            <ion-label><b>GEMS:</b>&nbsp;&nbsp;</ion-label>\n            <ion-icon name=\"star\" color=\"warning\" mode=\"ios\"></ion-icon>\n            <ion-text>0</ion-text>\n        </ion-col>\n      </ion-row>\n  <ion-item><br>\n    <ion-row>\n        <ion-col style=\"color: black\" mode=\"ios\">\n            <ion-label>Total Hours:&nbsp;&nbsp;</ion-label>\n            <ion-icon name=\"hourglass\" color=\"warning\" mode=\"ios\"></ion-icon>\n            <ion-note>00:00:00</ion-note>\n        </ion-col>\n      </ion-row>\n    </ion-item><br>\n    <ion-item mode=\"ios\" style=\"font-size: 13px;\">\n        <ion-label mode=\"ios\">Start Date:</ion-label>\n        <ion-datetime mode=\"ios\" displayFormat=\"MM/DD/YYYY\" [(ngModel)]=\"myDate\"></ion-datetime>\n    </ion-item>\n    <ion-item mode=\"ios\" style=\"font-size: 13px;\">\n        <ion-label mode=\"ios\">End Date:</ion-label>\n        <ion-datetime mode=\"ios\" displayFormat=\"MM/DD/YYYY\" [(ngModel)]=\"myDate\"></ion-datetime>\n    </ion-item>\n    <ion-button size=\"medium\" color=\"secondary\" shape=\"round\" mode=\"ios\" expand=\"full\">\n      SHOW\n    </ion-button>\n    <br>\n      <ion-list-header mode=\"ios\">\n        My Recent Sessions\n      </ion-list-header>\n    <ion-row style=\"border-bottom: groove;\">\n        <ion-col col-4>\n        <ion-label ><b>Date</b></ion-label>\n      </ion-col>\n      <ion-col col-4>\n        <ion-label ><b>Duration</b></ion-label>\n      </ion-col>\n        <ion-col col-4>\n        <ion-label ><b>Gems</b></ion-label>\n      </ion-col>\n        <ion-col col-4>\n        <ion-label ><b>Coins</b></ion-label>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col col-4 colspan=\"4\" text-center>\n        <br><br>\n          <img src=\"assets/icon/brixylogo.png\" style=\"width: 32px;\" mode=\"ios\">\n          <p style=\"line-height: 0;\">No data.</p>\n      </ion-col>\n     \n    </ion-row>\n   \n\n</ion-content>\n";
    /***/
  },

  /***/
  "./src/app/mybroadcast/mybroadcast.module.ts":
  /*!***************************************************!*\
    !*** ./src/app/mybroadcast/mybroadcast.module.ts ***!
    \***************************************************/

  /*! exports provided: MybroadcastPageModule */

  /***/
  function srcAppMybroadcastMybroadcastModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "MybroadcastPageModule", function () {
      return MybroadcastPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _mybroadcast_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./mybroadcast.page */
    "./src/app/mybroadcast/mybroadcast.page.ts");

    var routes = [{
      path: '',
      component: _mybroadcast_page__WEBPACK_IMPORTED_MODULE_6__["MybroadcastPage"]
    }];

    var MybroadcastPageModule = function MybroadcastPageModule() {
      _classCallCheck(this, MybroadcastPageModule);
    };

    MybroadcastPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      declarations: [_mybroadcast_page__WEBPACK_IMPORTED_MODULE_6__["MybroadcastPage"]]
    })], MybroadcastPageModule);
    /***/
  },

  /***/
  "./src/app/mybroadcast/mybroadcast.page.scss":
  /*!***************************************************!*\
    !*** ./src/app/mybroadcast/mybroadcast.page.scss ***!
    \***************************************************/

  /*! exports provided: default */

  /***/
  function srcAppMybroadcastMybroadcastPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".header .col {\n  background-color: lightgrey;\n}\n\n.col {\n  border: solid 1px grey;\n  border-bottom-style: none;\n  border-right-style: none;\n}\n\n.col:last-child {\n  border-right: solid 1px grey;\n}\n\n.row:last-child .col {\n  border-bottom: solid 1px grey;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZW5kcmljay9EZXNrdG9wL1RlbG1vIFNvbHV0aW9ucy9Gb3JQcm9kdWN0aW9uL21heTEwMjAyMS9sYXN0IHNldHVwL2dyZWVudGh1bWJhcHAgY29weS9zcmMvYXBwL215YnJvYWRjYXN0L215YnJvYWRjYXN0LnBhZ2Uuc2NzcyIsInNyYy9hcHAvbXlicm9hZGNhc3QvbXlicm9hZGNhc3QucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksMkJBQUE7QUNDSjs7QURFQTtFQUNJLHNCQUFBO0VBQ0EseUJBQUE7RUFDQSx3QkFBQTtBQ0NKOztBREVBO0VBQ0ksNEJBQUE7QUNDSjs7QURFQTtFQUNJLDZCQUFBO0FDQ0oiLCJmaWxlIjoic3JjL2FwcC9teWJyb2FkY2FzdC9teWJyb2FkY2FzdC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaGVhZGVyIC5jb2wge1xuICAgIGJhY2tncm91bmQtY29sb3I6bGlnaHRncmV5O1xufVxuXG4uY29sIHtcbiAgICBib3JkZXI6IHNvbGlkIDFweCBncmV5O1xuICAgIGJvcmRlci1ib3R0b20tc3R5bGU6IG5vbmU7XG4gICAgYm9yZGVyLXJpZ2h0LXN0eWxlOiBub25lO1xufVxuXG4uY29sOmxhc3QtY2hpbGQge1xuICAgIGJvcmRlci1yaWdodDogc29saWQgMXB4IGdyZXk7XG59XG5cbi5yb3c6bGFzdC1jaGlsZCAuY29sIHtcbiAgICBib3JkZXItYm90dG9tOiBzb2xpZCAxcHggZ3JleTtcbn0iLCIuaGVhZGVyIC5jb2wge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiBsaWdodGdyZXk7XG59XG5cbi5jb2wge1xuICBib3JkZXI6IHNvbGlkIDFweCBncmV5O1xuICBib3JkZXItYm90dG9tLXN0eWxlOiBub25lO1xuICBib3JkZXItcmlnaHQtc3R5bGU6IG5vbmU7XG59XG5cbi5jb2w6bGFzdC1jaGlsZCB7XG4gIGJvcmRlci1yaWdodDogc29saWQgMXB4IGdyZXk7XG59XG5cbi5yb3c6bGFzdC1jaGlsZCAuY29sIHtcbiAgYm9yZGVyLWJvdHRvbTogc29saWQgMXB4IGdyZXk7XG59Il19 */";
    /***/
  },

  /***/
  "./src/app/mybroadcast/mybroadcast.page.ts":
  /*!*************************************************!*\
    !*** ./src/app/mybroadcast/mybroadcast.page.ts ***!
    \*************************************************/

  /*! exports provided: MybroadcastPage */

  /***/
  function srcAppMybroadcastMybroadcastPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "MybroadcastPage", function () {
      return MybroadcastPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");

    var MybroadcastPage = /*#__PURE__*/function () {
      function MybroadcastPage(router) {
        _classCallCheck(this, MybroadcastPage);

        this.router = router;
        this.myDate = new Date().toISOString();
      }

      _createClass(MybroadcastPage, [{
        key: "goBack",
        value: function goBack() {
          this.router.navigate(['myaccount']);
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return MybroadcastPage;
    }();

    MybroadcastPage.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }];
    };

    MybroadcastPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-mybroadcast',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./mybroadcast.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/mybroadcast/mybroadcast.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./mybroadcast.page.scss */
      "./src/app/mybroadcast/mybroadcast.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])], MybroadcastPage);
    /***/
  }
}]);
//# sourceMappingURL=mybroadcast-mybroadcast-module-es5.js.map