(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["offers-offers-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/offers/offers.page.html":
/*!*******************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/offers/offers.page.html ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header mode=\"ios\" no-border>\n  <ion-toolbar mode=\"ios\">\n      <ion-buttons slot=\"secondary\" (click)=\"goBack()\">\n        <ion-button mode=\"ios\" style=\"font-size: 15px;\">\n            Back\n        </ion-button>\n      </ion-buttons>\n\n     <ion-title color=\"secondary\">Offers</ion-title>\n\n     <ion-buttons slot=\"primary\" (click)=\"goHelp()\">\n      <ion-button mode=\"ios\" style=\"font-size: 15px;\">\n          Help\n      </ion-button>\n    </ion-buttons>\n\n    </ion-toolbar>\n</ion-header>\n<ion-segment style=\"padding: 5px\" color=\"primary\" mode=\"ios\" (ionChange)=\"segmentChanged()\" [(ngModel)]=\"segment\">\n  <ion-segment-button style=\"padding: 3px\" mode=\"ios\" value=\"0\">\n      <ion-label>Selling</ion-label>\n  </ion-segment-button>  \n  <ion-segment-button style=\"padding: 3px\" mode=\"ios\" value=\"1\">\n      <ion-label>Buying</ion-label>\n  </ion-segment-button>\n</ion-segment>\n<ion-content >\n  <ion-slides #slides (ionSlideDidChange)=\"slideChanged()\" mode=\"ios\">\n    <!-- For selling -->\n    <ion-slide>\n      <ion-grid style=\"width: 50%;\">\n        \n\n        <ion-segment color=\"primary\" mode=\"ios\" style=\"padding:5px\" (ionChange)=\"segmentSellingChanged()\" [(ngModel)]=\"segmentSelling\"> \n          <ion-segment-button mode=\"ios\" value=\"0\" layout=\"icon-start\" style=\"padding: 3px\" checked>\n              <ion-label>ACTIVE</ion-label>\n          </ion-segment-button>\n          <ion-segment-button mode=\"ios\" value=\"1\" layout=\"icon-start\" style=\"padding: 3px\">\n              <ion-label>ARCHIVE</ion-label>\n          </ion-segment-button>\n        </ion-segment>\n\n        <ion-slides #slidesSelling (ionSlideDidChange)=\"slideSellingChanged()\" mode=\"ios\">\n          <!-- for Selling Active -->\n          <ion-slide>\n            <ion-row>\n              <ion-col no-padding>\n                <ion-list mode=\"ios\">\n                  <ion-row *ngFor=\"let item of itemList\">\n                    <ion-col>\n                      <ion-card mode=\"ios\" class=\"for_items\" >\n                        <ion-card-content no-padding>\n                          <ion-row>\n                            <ion-col text-left size=\"5\">\n                              <ion-row>\n                                <ion-col no-padding>\n                                  <div class=\"for_available\">Available</div>\n                                  <img (click)=\"goProductview(item.id)\" [src] = \"item.item_cover_photo\"  class=\"for_itemmimg\" >\n                                </ion-col>\n                              </ion-row>\n                            </ion-col>\n                            <ion-col text-left size=\"7\">\n                              <ion-row >\n                                <ion-col no-padding text-left>\n                                  <div class=\"for_unitqty\">{{item.quantity}}\n                                    <label *ngIf=\"item.stocks_id=='1'\">\n                                      <label *ngIf=\"item.quantity>1\">pieces</label>\n                                      <label *ngIf=\"item.quantity==1\">piece</label>\n                                    </label>\n                                    <label *ngIf=\"item.stocks_id=='2'\">\n                                      <label *ngIf=\"item.quantity>1\">pounds</label>\n                                      <label *ngIf=\"item.quantity==1\">pound</label>\n                                    </label>\n                                    <label *ngIf=\"item.stocks_id=='3'\">\n                                      <label *ngIf=\"item.quantity>1\">ounces</label>\n                                      <label *ngIf=\"item.quantity==1\">ounce</label>\n                                    </label>\n                                    <label *ngIf=\"item.stocks_id=='4'\">\n                                      {{item.others_stock}}\n                                    </label>\n                                  </div>\n                                  <ion-label color=\"secondary\"><h1 class=\"for_pprice\">${{item.price}}</h1></ion-label>\n                                </ion-col>\n                                <ion-col no-padding text-right>\n                                  <div class=\"for_category\">{{item.category}}</div>\n                                  <div style=\"margin-top: 5px;\">\n                                      <ionic4-star-rating #rating\n                                        activeIcon = \"ios-star\"\n                                        defaultIcon = \"ios-star-outline\"\n                                        activeColor = \"#ffce00\" \n                                        defaultColor = \"#ffce00\"\n                                        readonly=\"false\"\n                                        rating=\"{{item.user_rating}}\"\n                                        fontSize = \"15px\">\n                                      </ionic4-star-rating>\n                                  </div>\n                                </ion-col>\n                              </ion-row>\n                              <ion-row style=\"margin-top: -2px;\" >\n                                <ion-col no-padding text-left>\n                                  <ion-label color=\"dark\">\n                                    <h2 class=\"for_itemmname\">{{item.title}}</h2></ion-label>\n                                </ion-col>\n                              </ion-row>\n                              <ion-label  color=\"dark\"><h2 class=\"for_itemmlocation\">\n                                <ion-icon class=\"for_pin\" mode=\"ios\" color=\"primary\" name=\"pin\"></ion-icon>&nbsp;  {{item.location}}\n                              </h2></ion-label>\n                              <ion-row>\n                                <ion-col no-padding text-left size=\"12\" class=\"for_borderitem\">\n                                  <ion-row>\n                                    <ion-col text-left no-padding size=\"6\" (click)=\"goEdit(item.id)\">\n                                      <ion-item mode=\"ios\" lines=\"none\" class=\"for_itemsold\">\n                                        <ion-icon class=\"for_itembtnicon\" slot=\"start\" mode=\"ios\" color=\"secondary\" name=\"create\"></ion-icon>\n                                        <ion-label style=\"margin: 0;\" color=\"secondary\"><h2 class=\"for_itembtn\" style=\"margin-left: 3px;\"> Edit Item</h2></ion-label>\n                                      </ion-item>\n                                    </ion-col>\n                                    <ion-col text-left no-padding size=\"4\" (click)=\"deleteItem(item.id)\">\n                                      <ion-item mode=\"ios\" lines=\"none\" class=\"for_itemsold\">\n                                        <ion-icon class=\"for_itembtnicon\" slot=\"start\" mode=\"ios\" color=\"danger\" name=\"trash\"></ion-icon>\n                                        <ion-label style=\"margin: 0;\" color=\"danger\"><h2 class=\"for_itembtn\"> Delete</h2></ion-label>\n                                      </ion-item>\n                                    </ion-col>\n                                  </ion-row>\n                                </ion-col>\n                              </ion-row>\n                            </ion-col>\n                          </ion-row>\n                        </ion-card-content>\n                      </ion-card>\n                    </ion-col>\n                  </ion-row>\n    \n                </ion-list>\n              </ion-col>\n            </ion-row>\n          </ion-slide>\n          <!-- for Selling Archive -->\n          <ion-slide>\n            <ion-row>\n              <ion-col no-padding>\n                <ion-list mode=\"ios\">\n                  <ion-row *ngFor=\"let item of itemListArchive\">\n                    <ion-col>\n                      <ion-card mode=\"ios\" class=\"for_items\" >\n                        <ion-card-content no-padding>\n                          <ion-row>\n                            <ion-col text-left size=\"5\">\n                              <ion-row>\n                                <ion-col no-padding>\n                                  <div class=\"for_available\">Available</div>\n                                  <img (click)=\"goProductview(item.id)\" [src] = \"item.item_cover_photo\"  class=\"for_itemmimg\" >\n                                </ion-col>\n                              </ion-row>\n                            </ion-col>\n                            <ion-col text-left size=\"7\">\n                              <ion-row >\n                                <ion-col no-padding text-left>\n                                  <div class=\"for_unitqty\">{{item.quantity}} \n                                    <label *ngIf=\"item.stocks_id=='1'\">\n                                      <label *ngIf=\"item.quantity>1\">pieces</label>\n                                      <label *ngIf=\"item.quantity<=1\">piece</label>\n                                    </label>\n                                    <label *ngIf=\"item.stocks_id=='2'\">\n                                      <label *ngIf=\"item.quantity>1\">pounds</label>\n                                      <label *ngIf=\"item.quantity<=1\">pound</label>\n                                    </label>\n                                    <label *ngIf=\"item.stocks_id=='3'\">\n                                      <label *ngIf=\"item.quantity>1\">ounces</label>\n                                      <label *ngIf=\"item.quantity<=1\">ounce</label>\n                                    </label>\n                                    <label *ngIf=\"item.stocks_id=='4'\">\n                                      {{item.others_stock}}\n                                    </label>\n                                  </div>\n                                  <ion-label color=\"secondary\"><h1 class=\"for_pprice\">${{item.price}}</h1></ion-label>\n                                </ion-col>\n                                <ion-col no-padding text-right>\n                                  <div class=\"for_category\">{{item.category}}</div>\n                                  <div style=\"margin-top: 5px;\">\n                                      <ionic4-star-rating #rating\n                                        activeIcon = \"ios-star\"\n                                        defaultIcon = \"ios-star-outline\"\n                                        activeColor = \"#ffce00\" \n                                        defaultColor = \"#ffce00\"\n                                        readonly=\"false\"\n                                        rating=\"{{item.user_rating}}\"\n                                        fontSize = \"15px\">\n                                      </ionic4-star-rating>\n                                  </div>\n                                </ion-col>\n                              </ion-row>\n                              <ion-row style=\"margin-top: -2px;\" >\n                                <ion-col no-padding text-left>\n                                  <ion-label color=\"dark\">\n                                    <h2 class=\"for_itemmname\">{{item.title}}</h2></ion-label>\n                                </ion-col>\n                              </ion-row>\n                              <ion-label  color=\"dark\"><h2 class=\"for_itemmlocation\">\n                                <ion-icon class=\"for_pin\" mode=\"ios\" color=\"primary\" name=\"pin\"></ion-icon>&nbsp; {{item.location}}\n                              </h2></ion-label>\n                              <ion-row>\n                                <ion-col no-padding text-left size=\"12\" class=\"for_borderitem\">\n                                  <ion-row>\n                                    <ion-col text-left no-padding size=\"6\" (click)=\"goEdit(item.id)\">\n                                      <ion-item mode=\"ios\" lines=\"none\" class=\"for_itemsold\">\n                                        <ion-icon class=\"for_itembtnicon\" slot=\"start\" mode=\"ios\" color=\"secondary\" name=\"create\"></ion-icon>\n                                        <ion-label style=\"margin: 0;\" color=\"secondary\"><h2 class=\"for_itembtn\" style=\"margin-left: 3px;\"> Edit Item</h2></ion-label>\n                                      </ion-item>\n                                    </ion-col>\n                                    <ion-col text-left no-padding size=\"4\" (click)=\"deleteItem(item.id)\">\n                                      <ion-item mode=\"ios\" lines=\"none\" class=\"for_itemsold\">\n                                        <ion-icon class=\"for_itembtnicon\" slot=\"start\" mode=\"ios\" color=\"danger\" name=\"trash\"></ion-icon>\n                                        <ion-label style=\"margin: 0;\" color=\"danger\"><h2 class=\"for_itembtn\"> Delete</h2></ion-label>\n                                      </ion-item>\n                                    </ion-col>\n                                  </ion-row>\n                                </ion-col>\n                              </ion-row>\n                            </ion-col>\n                          </ion-row>\n                        </ion-card-content>\n                      </ion-card>\n                    </ion-col>\n                  </ion-row>\n    \n                </ion-list>\n              </ion-col>\n            </ion-row>\n\n          </ion-slide>\n\n        </ion-slides>\n\n       \n      </ion-grid>\n    </ion-slide>\n    \n    <!-- For buying -->\n    <ion-slide>\n      <ion-grid style=\"width: 50%;\">\n\n\n        <ion-segment color=\"primary\" mode=\"ios\" style=\"padding:5px\" (ionChange)=\"segmentBuyingChanged()\" [(ngModel)]=\"segmentBuying\"> \n          <ion-segment-button mode=\"ios\" value=\"0\" layout=\"icon-start\" style=\"padding: 3px\" checked>\n              <ion-label>ACTIVE</ion-label>\n          </ion-segment-button>\n          <ion-segment-button mode=\"ios\" value=\"1\" layout=\"icon-start\" style=\"padding: 3px\">\n              <ion-label>ARCHIVE</ion-label>\n          </ion-segment-button>\n        </ion-segment>\n\n        <ion-slides #slidesBuying (ionSlideDidChange)=\"slideBuyingChanged()\" mode=\"ios\">\n          <!-- for Buying Active -->\n          <ion-slide>\n\n            <ion-row>\n              <ion-col no-padding>\n                <ion-list mode=\"ios\">\n                  <ion-row class=\"for_eachrow\" *ngFor=\"let buy of buyItemList\"> \n                    <ion-col no-padding>\n                      <ion-card mode=\"ios\" class=\"for_items\" >\n                        <ion-card-content no-padding>\n                          <ion-row>\n                            <ion-col text-left size=\"5\">\n                              <div class=\"for_available\">Available</div>\n                              <ion-item lines=\"none\" class=\"for_itemms\" style=\"margin-bottom: -8px;\">\n                                <ion-avatar slot=\"start\">\n                                  <div *ngIf=\"!buy?.profile_photo\">\n                                    <img src=\"assets/greenthumb-images/userpic.png\">\n                                  </div>\n                                  <div *ngIf=\"buy?.profile_photo\">\n                                    <img [src] = \"buy.profile_photo\" >\n                                  </div> \n                                </ion-avatar>\n                                <ion-label class=\"for_name\">{{buy.username}}</ion-label>\n                              </ion-item>\n                              <img (click)=\"goProductview(buy.id)\" style=\"padding-bottom: 5px;\" [src] = \"buy.item_cover_photo\" class=\"for_itemmimg\">\n                            </ion-col>\n                            <ion-col text-left size=\"7\">\n                              <ion-row (click)=\"goProductview(buy.id)\">\n                                <ion-col no-padding text-left>\n                                  <div class=\"for_unitqty\">{{buy.quantity}} &nbsp;\n                                    <label *ngIf=\"buy.stocks_id=='1'\">\n                                      <label *ngIf=\"buy.quantity>1\">pieces</label>\n                                      <label *ngIf=\"buy.quantity<=1\">piece</label>\n                                    </label>\n                                    <label *ngIf=\"buy.stocks_id=='2'\">\n                                      <label *ngIf=\"buy.quantity>1\">pounds</label>\n                                      <label *ngIf=\"buy.quantity<=1\">pound</label>\n                                    </label>\n                                    <label *ngIf=\"buy.stocks_id=='3'\">\n                                      <label *ngIf=\"buy.quantity>1\">ounces</label>\n                                      <label *ngIf=\"buy.quantity<=1\">ounce</label>\n                                    </label>\n                                    <label *ngIf=\"buy.stocks_id=='4'\">\n                                      {{buy.others_stock}}\n                                    </label>\n                                  </div>\n                                 <ion-label color=\"secondary\"><h1 class=\"for_pprice\">${{buy.price}}</h1></ion-label>\n                                </ion-col>\n                                <ion-col no-padding text-right>\n                                  <div class=\"for_category\">{{buy.category}}</div>\n                                  <div style=\"margin-top: 5px;\">\n                                      <ionic4-star-rating #rating\n                                        activeIcon = \"ios-star\"\n                                        defaultIcon = \"ios-star-outline\"\n                                        activeColor = \"#ffce00\" \n                                        defaultColor = \"#ffce00\"\n                                        readonly=\"false\"\n                                        rating=\"{{buy.user_rating}}\"\n                                        fontSize = \"15px\">\n                                      </ionic4-star-rating>\n                                  </div>\n                                </ion-col>\n                              </ion-row>\n                              <ion-row style=\"margin-top: -2px;\" (click)=\"goProductview(buy.id)\">\n                                <ion-col no-padding text-left>\n                                  <ion-label color=\"dark\">\n                                    <h2 class=\"for_itemmname\">{{buy.title}}</h2></ion-label>\n                                </ion-col>\n                              </ion-row>\n                              <ion-label  color=\"dark\"><h2 class=\"for_itemmlocation\">\n                                <ion-icon class=\"for_pin\" mode=\"ios\" color=\"primary\" name=\"pin\"></ion-icon>&nbsp; {{buy.location}}\n                              </h2></ion-label>\n                              <ion-row>\n                                <ion-col no-padding text-left size=\"12\" class=\"for_borderitem\">\n                                  <ion-row>\n                                    <ion-col text-left no-padding size=\"6\">\n                                      <ion-item mode=\"ios\" lines=\"none\" class=\"for_itemsold\">\n                                        <ion-icon name=\"heart\" mode=\"ios\"\n                                         id=\"class{{buy.id}}\" [class]=\"(buy.save_status == '1') ? 'myHeartSave' : 'myHeartUnSave'\"></ion-icon>\n                                        <ion-label style=\"margin: 0;\" color=\"secondary\"><h2 class=\"for_itembtn\"> &nbsp;Save</h2></ion-label>\n                                      </ion-item>\n                                    </ion-col>\n                                    <ion-col text-left no-padding size=\"4\" (click)=\"goMessage(buy.user_id, buy.id)\">\n                                      <ion-item mode=\"ios\" lines=\"none\" class=\"for_itemsold\">\n                                        <ion-icon class=\"for_itembtnicon\" slot=\"start\" mode=\"ios\" color=\"secondary\" name=\"mail\"></ion-icon>\n                                        <ion-label style=\"margin: 0;\" color=\"secondary\"><h2 class=\"for_itembtn\">&nbsp; Message</h2></ion-label>\n                                      </ion-item>\n                                    </ion-col>\n                                  </ion-row>\n                                </ion-col>\n                              </ion-row>\n                            </ion-col>\n                          </ion-row>\n                        </ion-card-content>\n                      </ion-card>\n                    </ion-col>\n                  </ion-row>\n                </ion-list>\n              </ion-col>\n            </ion-row>\n\n          </ion-slide>\n          <!-- for buying Archive -->\n          <ion-slide>\n            <ion-row>\n              <ion-col no-padding>\n                <ion-list mode=\"ios\">\n                  <ion-row class=\"for_eachrow\" *ngFor=\"let buy of buyItemListArchive\"> \n                    <ion-col no-padding>\n                      <ion-card mode=\"ios\" class=\"for_items\" >\n                        <ion-card-content no-padding>\n                          <ion-row>\n                            <ion-col text-left size=\"5\">\n                              <div class=\"for_available\">Available</div>\n                              <ion-item lines=\"none\" class=\"for_itemms\" style=\"margin-bottom: -8px;\">\n                                <ion-avatar slot=\"start\">\n                                  <div *ngIf=\"!buy?.profile_photo\">\n                                    <img src=\"assets/greenthumb-images/userpic.png\">\n                                  </div>\n                                  <div *ngIf=\"buy?.profile_photo\">\n                                    <img [src] = \"buy.profile_photo\" >\n                                  </div> \n                                </ion-avatar>\n                                <ion-label class=\"for_name\">{{buy.username}}</ion-label>\n                              </ion-item>\n                              <img (click)=\"goProductview(buy.id)\" style=\"padding-bottom: 5px;\" [src] = \"buy.item_cover_photo\" class=\"for_itemmimg\">\n                            </ion-col>\n                            <ion-col text-left size=\"7\">\n                              <ion-row (click)=\"goProductview(buy.id)\">\n                                <ion-col no-padding text-left>\n                                  <div class=\"for_unitqty\">{{buy.quantity}} &nbsp;\n                                    <label *ngIf=\"buy.stocks_id=='1'\">\n                                      <label *ngIf=\"buy.quantity>1\">pieces</label>\n                                      <label *ngIf=\"buy.quantity<=1\">piece</label>\n                                    </label>\n                                    <label *ngIf=\"buy.stocks_id=='2'\">\n                                      <label *ngIf=\"buy.quantity>1\">pounds</label>\n                                      <label *ngIf=\"buy.quantity<=1\">pound</label>\n                                    </label>\n                                    <label *ngIf=\"buy.stocks_id=='3'\">\n                                      <label *ngIf=\"buy.quantity>1\">ounces</label>\n                                      <label *ngIf=\"buy.quantity<=1\">ounce</label>\n                                    </label>\n                                    <label *ngIf=\"buy.stocks_id=='4'\">\n                                      {{buy.others_stock}}\n                                    </label>\n                                  </div>\n                                 <ion-label color=\"secondary\"><h1 class=\"for_pprice\">${{buy.price}}</h1></ion-label>\n                                </ion-col>\n                                <ion-col no-padding text-right>\n                                  <div class=\"for_category\">{{buy.category}}</div>\n                                  <div style=\"margin-top: 5px;\">\n                                      <ionic4-star-rating #rating\n                                        activeIcon = \"ios-star\"\n                                        defaultIcon = \"ios-star-outline\"\n                                        activeColor = \"#ffce00\" \n                                        defaultColor = \"#ffce00\"\n                                        readonly=\"false\"\n                                        rating=\"{{buy.user_rating}}\"\n                                        fontSize = \"15px\">\n                                      </ionic4-star-rating>\n                                  </div>\n                                </ion-col>\n                              </ion-row>\n                              <ion-row style=\"margin-top: -2px;\" (click)=\"goProductview(buy.id)\">\n                                <ion-col no-padding text-left>\n                                  <ion-label color=\"dark\">\n                                    <h2 class=\"for_itemmname\">{{buy.title}}</h2></ion-label>\n                                </ion-col>\n                              </ion-row>\n                              <ion-label ><h2 class=\"for_itemmlocation\">\n                                <ion-icon class=\"for_pin\" mode=\"ios\" color=\"primary\" name=\"pin\"></ion-icon>&nbsp; {{buy.location}}\n                              </h2></ion-label>\n                              <ion-row>\n                                <ion-col no-padding text-left size=\"12\" class=\"for_borderitem\">\n                                  <ion-row>\n                                    <ion-col text-left no-padding size=\"6\">\n                                      <ion-item mode=\"ios\" lines=\"none\" class=\"for_itemsold\">\n                                        <ion-icon name=\"heart\" mode=\"ios\"\n                                         id=\"class{{buy.id}}\" [class]=\"(buy.save_status == '1') ? 'myHeartSave' : 'myHeartUnSave'\"></ion-icon>\n                                        <ion-label style=\"margin: 0;\" color=\"secondary\"><h2 class=\"for_itembtn\"> &nbsp;Save</h2></ion-label>\n                                      </ion-item>\n                                    </ion-col>\n                                    <ion-col text-left no-padding size=\"4\" (click)=\"goMessage(buy.user_id, buy.id)\">\n                                      <ion-item mode=\"ios\" lines=\"none\" class=\"for_itemsold\">\n                                        <ion-icon class=\"for_itembtnicon\" slot=\"start\" mode=\"ios\" color=\"secondary\" name=\"mail\"></ion-icon>\n                                        <ion-label style=\"margin: 0;\" color=\"secondary\"><h2 class=\"for_itembtn\">&nbsp; Message</h2></ion-label>\n                                      </ion-item>\n                                    </ion-col>\n                                  </ion-row>\n                                </ion-col>\n                              </ion-row>\n                            </ion-col>\n                          </ion-row>\n                        </ion-card-content>\n                      </ion-card>\n                    </ion-col>\n                  </ion-row>\n                </ion-list>\n              </ion-col>\n            </ion-row>\n          </ion-slide>\n\n        </ion-slides>\n        \n\n        \n\n\n      </ion-grid>\n    </ion-slide>\n  </ion-slides>\n\n</ion-content>\n");

/***/ }),

/***/ "./src/app/offers/offers-routing.module.ts":
/*!*************************************************!*\
  !*** ./src/app/offers/offers-routing.module.ts ***!
  \*************************************************/
/*! exports provided: OffersPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OffersPageRoutingModule", function() { return OffersPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _offers_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./offers.page */ "./src/app/offers/offers.page.ts");




const routes = [
    {
        path: '',
        component: _offers_page__WEBPACK_IMPORTED_MODULE_3__["OffersPage"]
    }
];
let OffersPageRoutingModule = class OffersPageRoutingModule {
};
OffersPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], OffersPageRoutingModule);



/***/ }),

/***/ "./src/app/offers/offers.module.ts":
/*!*****************************************!*\
  !*** ./src/app/offers/offers.module.ts ***!
  \*****************************************/
/*! exports provided: OffersPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OffersPageModule", function() { return OffersPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _offers_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./offers-routing.module */ "./src/app/offers/offers-routing.module.ts");
/* harmony import */ var ionic4_star_rating__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ionic4-star-rating */ "./node_modules/ionic4-star-rating/dist/index.js");
/* harmony import */ var _offers_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./offers.page */ "./src/app/offers/offers.page.ts");








let OffersPageModule = class OffersPageModule {
};
OffersPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            ionic4_star_rating__WEBPACK_IMPORTED_MODULE_6__["StarRatingModule"],
            _offers_routing_module__WEBPACK_IMPORTED_MODULE_5__["OffersPageRoutingModule"]
        ],
        declarations: [_offers_page__WEBPACK_IMPORTED_MODULE_7__["OffersPage"]]
    })
], OffersPageModule);



/***/ }),

/***/ "./src/app/offers/offers.page.scss":
/*!*****************************************!*\
  !*** ./src/app/offers/offers.page.scss ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".for_items {\n  margin-top: 0%;\n  box-shadow: 0 1px 10px #e2f0cb;\n  border-radius: 10px;\n  border: 1px solid #e2f0cb;\n  margin-bottom: 0;\n}\n\n.for_chip {\n  --background: transparent;\n  margin: 0;\n  padding-right: 0;\n  padding-left: 15px;\n}\n\n.for_itemimg {\n  margin: auto;\n  width: 90%;\n  padding: 7px;\n  padding-top: 0;\n}\n\n.for_price {\n  font-size: 20px;\n  font-weight: bolder;\n  padding-top: 5px;\n}\n\n.for_itemname {\n  font-size: 15px;\n  font-weight: bolder;\n}\n\n.for_itemcateg {\n  text-transform: uppercase;\n  font-size: 12px;\n  font-weight: lighter;\n}\n\n.for_status_item {\n  text-transform: uppercase;\n  font-size: 12px;\n  font-weight: lighter;\n}\n\n.for_itemlocation {\n  font-size: 13px;\n  font-weight: lighter;\n}\n\n.for_pin {\n  font-size: 15px;\n  margin-bottom: -1%;\n  margin-left: -1%;\n}\n\n.for_itembtn {\n  font-size: 11px;\n}\n\n.for_itembtnicon {\n  font-size: 20px;\n  margin: 0;\n}\n\n.for_itembtnsold {\n  margin: 0;\n  width: 15px;\n  height: auto;\n  margin-right: 5%;\n}\n\n.col1 {\n  margin-left: -30px;\n}\n\n.col2 {\n  margin-left: 25px;\n}\n\n.for_itemsold {\n  --padding-start: 0;\n  margin-top: -10px;\n  --background: transparent;\n  margin-right: -30px;\n  margin-bottom: -10px;\n}\n\n.for_cols {\n  padding-bottom: 5px;\n  padding-top: 5px;\n}\n\n.for_eachrow {\n  margin-top: 10px;\n}\n\n.for_itemimgbuying {\n  padding-left: 5px;\n  margin: auto;\n  width: 90%;\n  height: 70%;\n}\n\n.for_colbuying {\n  padding-top: 0;\n  margin-top: -8px;\n}\n\nion-thumbnail {\n  min-width: 5rem;\n  min-height: 5rem;\n}\n\nion-thumbnail img {\n  max-width: 5rem;\n  min-width: 5rem;\n}\n\n.for_itemms {\n  --padding-start: 0;\n  margin-top: -8px;\n  --inner-padding-end: 0;\n  --background: transparent;\n}\n\n.for_itemms ion-avatar {\n  width: 100% !important;\n  height: 100% !important;\n  max-width: 25px !important;\n  max-height: 25px !important;\n  margin-right: 5px;\n  margin-top: 0;\n}\n\n.for_itemms ion-avatar img {\n  border: 1px solid rgba(0, 0, 0, 0.12);\n  width: 100% !important;\n  height: 100% !important;\n  max-width: 25px !important;\n  max-height: 25px !important;\n  margin-right: 5px;\n  margin-top: 0;\n  border-radius: 50%;\n}\n\n.for_name {\n  font-size: 12px;\n  text-transform: capitalize;\n  margin-top: 0;\n  color: black;\n}\n\n.for_itemmimg {\n  margin: auto;\n  width: 90%;\n}\n\n.for_itemmimgbuying {\n  margin: auto;\n  width: 65%;\n  margin-top: -10%;\n  padding-bottom: 5px;\n}\n\n.for_pprice {\n  font-size: 23px;\n  font-weight: bolder;\n}\n\n.for_category {\n  background: #76c961;\n  padding: 3px;\n  border-top-right-radius: 10px;\n  margin-top: -5px;\n  margin-right: -5px;\n  border-bottom-left-radius: 10px;\n  text-transform: capitalize;\n  font-size: 12px;\n  font-weight: lighter;\n  color: white;\n  text-align: center;\n}\n\n.for_itemmname {\n  font-size: 15px;\n  font-weight: bolder;\n}\n\n.for_itemmlocation {\n  font-size: 12.5px;\n  font-weight: lighter;\n  color: #989aa2;\n  padding-right: 5px;\n  padding-bottom: 2.5%;\n}\n\n.for_available {\n  border-top-left-radius: 10px;\n  border-bottom-right-radius: 10px;\n  font-size: 10px;\n  color: #679733;\n  text-align: center;\n  font-weight: bold;\n  text-transform: uppercase;\n  background: #e2f0cb;\n  padding: 3px;\n  margin-top: -5px;\n  margin-left: -5px;\n  margin-bottom: 10px;\n}\n\n.for_borderitem {\n  border-top: 1px solid #e2f0cb;\n  padding-top: 2.5%;\n}\n\n.for_itemmbtn {\n  font-size: 11px;\n}\n\n.for_unitqty {\n  font-size: 11px;\n  text-align: left;\n  color: #989aa2;\n}\n\n.myHeartUnSave {\n  margin: 0;\n  color: #d3dbc9;\n  font-size: 20px;\n  margin-right: 3%;\n}\n\n.myHeartSave {\n  margin: 0;\n  color: red;\n  font-size: 20px;\n  margin-right: 3%;\n}\n\n.for_aligncenter {\n  display: flex !important;\n  align-content: center !important;\n  align-items: center !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZW5kcmljay9EZXNrdG9wL1RlbG1vIFNvbHV0aW9ucy9Gb3JQcm9kdWN0aW9uL21heTEwMjAyMS9sYXN0IHNldHVwL2dyZWVudGh1bWJhcHAgY29weS9zcmMvYXBwL29mZmVycy9vZmZlcnMucGFnZS5zY3NzIiwic3JjL2FwcC9vZmZlcnMvb2ZmZXJzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQTtFQUNJLGNBQUE7RUFDQSw4QkFBQTtFQUNBLG1CQUFBO0VBQ0EseUJBQUE7RUFDQSxnQkFBQTtBQ0FKOztBREVBO0VBQ0kseUJBQUE7RUFDQSxTQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtBQ0NKOztBRENBO0VBQ0ksWUFBQTtFQUNBLFVBQUE7RUFDQSxZQUFBO0VBQ0EsY0FBQTtBQ0VKOztBREFBO0VBQ0ksZUFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7QUNHSjs7QUREQTtFQUNJLGVBQUE7RUFDQSxtQkFBQTtBQ0lKOztBREZBO0VBQ0kseUJBQUE7RUFDQSxlQUFBO0VBQ0Esb0JBQUE7QUNLSjs7QURIQTtFQUNJLHlCQUFBO0VBQ0EsZUFBQTtFQUNBLG9CQUFBO0FDTUo7O0FESkE7RUFDSSxlQUFBO0VBQ0Esb0JBQUE7QUNPSjs7QURMQTtFQUNJLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0FDUUo7O0FETkE7RUFDSSxlQUFBO0FDU0o7O0FEUEE7RUFDSSxlQUFBO0VBQ0EsU0FBQTtBQ1VKOztBRFJBO0VBQ0ksU0FBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7QUNXSjs7QURUQTtFQUNJLGtCQUFBO0FDWUo7O0FEVkE7RUFDSSxpQkFBQTtBQ2FKOztBRFhBO0VBQ0ksa0JBQUE7RUFDQSxpQkFBQTtFQUNBLHlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxvQkFBQTtBQ2NKOztBRFpBO0VBQ0ksbUJBQUE7RUFDQSxnQkFBQTtBQ2VKOztBRGJBO0VBQ0ksZ0JBQUE7QUNnQko7O0FEZEE7RUFDSSxpQkFBQTtFQUNBLFlBQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtBQ2lCSjs7QURmQTtFQUNJLGNBQUE7RUFDQSxnQkFBQTtBQ2tCSjs7QURoQkE7RUFDSSxlQUFBO0VBQ0EsZ0JBQUE7QUNtQko7O0FEbEJJO0VBQ0ksZUFBQTtFQUNBLGVBQUE7QUNvQlI7O0FEYkE7RUFDSSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0Esc0JBQUE7RUFDQSx5QkFBQTtBQ2dCSjs7QURmSTtFQUNJLHNCQUFBO0VBQ0EsdUJBQUE7RUFDQSwwQkFBQTtFQUNBLDJCQUFBO0VBQ0EsaUJBQUE7RUFBa0IsYUFBQTtBQ2tCMUI7O0FEakJRO0VBQ0kscUNBQUE7RUFDQSxzQkFBQTtFQUNBLHVCQUFBO0VBQ0EsMEJBQUE7RUFDQSwyQkFBQTtFQUNBLGlCQUFBO0VBQWtCLGFBQUE7RUFDbEIsa0JBQUE7QUNvQlo7O0FEZkE7RUFDSSxlQUFBO0VBQ0EsMEJBQUE7RUFDQSxhQUFBO0VBQ0EsWUFBQTtBQ2tCSjs7QURoQkE7RUFDSSxZQUFBO0VBQ0EsVUFBQTtBQ21CSjs7QURqQkE7RUFDSSxZQUFBO0VBQ0EsVUFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7QUNvQko7O0FEbEJBO0VBQ0ksZUFBQTtFQUNBLG1CQUFBO0FDcUJKOztBRG5CQTtFQUNJLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLDZCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLCtCQUFBO0VBQ0EsMEJBQUE7RUFDQSxlQUFBO0VBQ0Esb0JBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7QUNzQko7O0FEcEJBO0VBQ0ksZUFBQTtFQUNBLG1CQUFBO0FDdUJKOztBRHJCQTtFQUNJLGlCQUFBO0VBQ0Esb0JBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7RUFDQSxvQkFBQTtBQ3dCSjs7QUR0QkE7RUFDSSw0QkFBQTtFQUNBLGdDQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EseUJBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7QUN5Qko7O0FEdkJBO0VBQ0ksNkJBQUE7RUFDQSxpQkFBQTtBQzBCSjs7QUR4QkE7RUFDSSxlQUFBO0FDMkJKOztBRHpCQTtFQUNJLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7QUM0Qko7O0FEMUJBO0VBQ0ksU0FBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUM2Qko7O0FEM0JBO0VBQ0ksU0FBQTtFQUNBLFVBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUM4Qko7O0FENUJBO0VBQ0ksd0JBQUE7RUFDQSxnQ0FBQTtFQUNBLDhCQUFBO0FDK0JKIiwiZmlsZSI6InNyYy9hcHAvb2ZmZXJzL29mZmVycy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcbi5mb3JfaXRlbXN7XG4gICAgbWFyZ2luLXRvcDogMCU7XG4gICAgYm94LXNoYWRvdzogMCAxcHggMTBweCAjZTJmMGNiO1xuICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gICAgYm9yZGVyOiAxcHggc29saWQgI2UyZjBjYjtcbiAgICBtYXJnaW4tYm90dG9tOiAwO1xufVxuLmZvcl9jaGlwe1xuICAgIC0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG4gICAgbWFyZ2luOiAwO1xuICAgIHBhZGRpbmctcmlnaHQ6IDA7XG4gICAgcGFkZGluZy1sZWZ0OiAxNXB4O1xufVxuLmZvcl9pdGVtaW1ne1xuICAgIG1hcmdpbjogYXV0bztcbiAgICB3aWR0aDogOTAlO1xuICAgIHBhZGRpbmc6IDdweDtcbiAgICBwYWRkaW5nLXRvcDogMDtcbn1cbi5mb3JfcHJpY2V7XG4gICAgZm9udC1zaXplOiAyMHB4O1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkZXI7XG4gICAgcGFkZGluZy10b3A6IDVweDtcbn1cbi5mb3JfaXRlbW5hbWV7XG4gICAgZm9udC1zaXplOiAxNXB4O1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkZXI7XG59XG4uZm9yX2l0ZW1jYXRlZ3tcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICBmb250LXdlaWdodDogbGlnaHRlcjtcbn1cbi5mb3Jfc3RhdHVzX2l0ZW17XG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgICBmb250LXNpemU6IDEycHg7XG4gICAgZm9udC13ZWlnaHQ6IGxpZ2h0ZXI7XG59XG4uZm9yX2l0ZW1sb2NhdGlvbntcbiAgICBmb250LXNpemU6IDEzcHg7XG4gICAgZm9udC13ZWlnaHQ6IGxpZ2h0ZXI7XG59XG4uZm9yX3BpbntcbiAgICBmb250LXNpemU6IDE1cHg7XG4gICAgbWFyZ2luLWJvdHRvbTogLTElO1xuICAgIG1hcmdpbi1sZWZ0OiAtMSU7XG59XG4uZm9yX2l0ZW1idG57XG4gICAgZm9udC1zaXplOiAxMXB4Oztcbn1cbi5mb3JfaXRlbWJ0bmljb257XG4gICAgZm9udC1zaXplOiAyMHB4O1xuICAgIG1hcmdpbjogMDtcbn1cbi5mb3JfaXRlbWJ0bnNvbGR7XG4gICAgbWFyZ2luOiAwO1xuICAgIHdpZHRoOiAxNXB4O1xuICAgIGhlaWdodDogYXV0bztcbiAgICBtYXJnaW4tcmlnaHQ6IDUlO1xufVxuLmNvbDF7XG4gICAgbWFyZ2luLWxlZnQ6IC0zMHB4O1xufVxuLmNvbDJ7XG4gICAgbWFyZ2luLWxlZnQ6IDI1cHg7XG59XG4uZm9yX2l0ZW1zb2xke1xuICAgIC0tcGFkZGluZy1zdGFydDogMDtcbiAgICBtYXJnaW4tdG9wOiAtMTBweDtcbiAgICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICAgIG1hcmdpbi1yaWdodDogLTMwcHg7XG4gICAgbWFyZ2luLWJvdHRvbTogLTEwcHg7XG59XG4uZm9yX2NvbHN7XG4gICAgcGFkZGluZy1ib3R0b206IDVweDtcbiAgICBwYWRkaW5nLXRvcDogNXB4O1xufVxuLmZvcl9lYWNocm93e1xuICAgIG1hcmdpbi10b3A6IDEwcHg7XG59XG4uZm9yX2l0ZW1pbWdidXlpbmd7XG4gICAgcGFkZGluZy1sZWZ0OiA1cHg7XG4gICAgbWFyZ2luOiBhdXRvO1xuICAgIHdpZHRoOiA5MCU7XG4gICAgaGVpZ2h0OiA3MCU7XG59XG4uZm9yX2NvbGJ1eWluZ3tcbiAgICBwYWRkaW5nLXRvcDogMDtcbiAgICBtYXJnaW4tdG9wOiAtOHB4O1xufVxuaW9uLXRodW1ibmFpbCB7XG4gICAgbWluLXdpZHRoOiA1cmVtOyAgICBcbiAgICBtaW4taGVpZ2h0OiA1cmVtO1xuICAgIGltZyB7XG4gICAgICAgIG1heC13aWR0aDogNXJlbTsgICAgXG4gICAgICAgIG1pbi13aWR0aDogNXJlbTtcbiAgICB9XG59XG5cblxuXG5cbi5mb3JfaXRlbW1ze1xuICAgIC0tcGFkZGluZy1zdGFydDogMDtcbiAgICBtYXJnaW4tdG9wOiAtOHB4O1xuICAgIC0taW5uZXItcGFkZGluZy1lbmQ6IDA7XG4gICAgLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgICBpb24tYXZhdGFyICB7ICAgICBcbiAgICAgICAgd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcbiAgICAgICAgaGVpZ2h0OiAxMDAlICFpbXBvcnRhbnQ7XG4gICAgICAgIG1heC13aWR0aDogMjVweCAhaW1wb3J0YW50O1xuICAgICAgICBtYXgtaGVpZ2h0OiAyNXB4ICFpbXBvcnRhbnQ7XG4gICAgICAgIG1hcmdpbi1yaWdodDogNXB4O21hcmdpbi10b3A6IDA7XG4gICAgICAgIGltZ3tcbiAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkIHJnYmEoMCwgMCwgMCwgMC4xMik7XG4gICAgICAgICAgICB3aWR0aDogMTAwJSAhaW1wb3J0YW50O1xuICAgICAgICAgICAgaGVpZ2h0OiAxMDAlICFpbXBvcnRhbnQ7XG4gICAgICAgICAgICBtYXgtd2lkdGg6IDI1cHggIWltcG9ydGFudDtcbiAgICAgICAgICAgIG1heC1oZWlnaHQ6IDI1cHggIWltcG9ydGFudDtcbiAgICAgICAgICAgIG1hcmdpbi1yaWdodDogNXB4O21hcmdpbi10b3A6IDA7XG4gICAgICAgICAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgICAgIH1cbiAgICB9XG4gIH1cbiAgXG4uZm9yX25hbWV7XG4gICAgZm9udC1zaXplOiAxMnB4O1xuICAgIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xuICAgIG1hcmdpbi10b3A6IDA7XG4gICAgY29sb3I6IGJsYWNrO1xufVxuLmZvcl9pdGVtbWltZ3tcbiAgICBtYXJnaW46IGF1dG87XG4gICAgd2lkdGg6IDkwJTtcbn1cbi5mb3JfaXRlbW1pbWdidXlpbmd7XG4gICAgbWFyZ2luOiBhdXRvO1xuICAgIHdpZHRoOiA2NSU7XG4gICAgbWFyZ2luLXRvcDogLTEwJTtcbiAgICBwYWRkaW5nLWJvdHRvbTogNXB4O1xufVxuLmZvcl9wcHJpY2V7XG4gICAgZm9udC1zaXplOiAyM3B4O1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkZXI7XG59XG4uZm9yX2NhdGVnb3J5e1xuICAgIGJhY2tncm91bmQ6ICM3NmM5NjE7XG4gICAgcGFkZGluZzogM3B4O1xuICAgIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiAxMHB4O1xuICAgIG1hcmdpbi10b3A6IC01cHg7XG4gICAgbWFyZ2luLXJpZ2h0OiAtNXB4O1xuICAgIGJvcmRlci1ib3R0b20tbGVmdC1yYWRpdXM6IDEwcHg7XG4gICAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG4gICAgZm9udC1zaXplOiAxMnB4O1xuICAgIGZvbnQtd2VpZ2h0OiBsaWdodGVyO1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4uZm9yX2l0ZW1tbmFtZXtcbiAgICBmb250LXNpemU6IDE1cHg7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbn1cbi5mb3JfaXRlbW1sb2NhdGlvbntcbiAgICBmb250LXNpemU6IDEyLjVweDtcbiAgICBmb250LXdlaWdodDogbGlnaHRlcjtcbiAgICBjb2xvcjogIzk4OWFhMjtcbiAgICBwYWRkaW5nLXJpZ2h0OiA1cHg7XG4gICAgcGFkZGluZy1ib3R0b206IDIuNSU7XG59XG4uZm9yX2F2YWlsYWJsZXtcbiAgICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiAxMHB4O1xuICAgIGJvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOiAxMHB4O1xuICAgIGZvbnQtc2l6ZTogMTBweDtcbiAgICBjb2xvcjogIzY3OTczMztcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgICBiYWNrZ3JvdW5kOiAjZTJmMGNiO1xuICAgIHBhZGRpbmc6IDNweDtcbiAgICBtYXJnaW4tdG9wOiAtNXB4O1xuICAgIG1hcmdpbi1sZWZ0OiAtNXB4O1xuICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XG59XG4uZm9yX2JvcmRlcml0ZW17XG4gICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICNlMmYwY2I7XG4gICAgcGFkZGluZy10b3A6IDIuNSU7XG59XG4uZm9yX2l0ZW1tYnRue1xuICAgIGZvbnQtc2l6ZTogMTFweDtcbn1cbi5mb3JfdW5pdHF0eXtcbiAgICBmb250LXNpemU6IDExcHg7XG4gICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICBjb2xvcjogIzk4OWFhMjtcbn1cbi5teUhlYXJ0VW5TYXZle1xuICAgIG1hcmdpbjogMDtcbiAgICBjb2xvcjogI2QzZGJjOTtcbiAgICBmb250LXNpemU6IDIwcHg7XG4gICAgbWFyZ2luLXJpZ2h0OiAzJTtcbn1cbi5teUhlYXJ0U2F2ZXtcbiAgICBtYXJnaW46IDA7XG4gICAgY29sb3I6IHJlZDtcbiAgICBmb250LXNpemU6IDIwcHg7XG4gICAgbWFyZ2luLXJpZ2h0OiAzJTtcbn1cbi5mb3JfYWxpZ25jZW50ZXJ7XG4gICAgZGlzcGxheTogZmxleCFpbXBvcnRhbnQ7XG4gICAgYWxpZ24tY29udGVudDogY2VudGVyIWltcG9ydGFudDtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyIWltcG9ydGFudDtcbn1cbiIsIi5mb3JfaXRlbXMge1xuICBtYXJnaW4tdG9wOiAwJTtcbiAgYm94LXNoYWRvdzogMCAxcHggMTBweCAjZTJmMGNiO1xuICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjZTJmMGNiO1xuICBtYXJnaW4tYm90dG9tOiAwO1xufVxuXG4uZm9yX2NoaXAge1xuICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICBtYXJnaW46IDA7XG4gIHBhZGRpbmctcmlnaHQ6IDA7XG4gIHBhZGRpbmctbGVmdDogMTVweDtcbn1cblxuLmZvcl9pdGVtaW1nIHtcbiAgbWFyZ2luOiBhdXRvO1xuICB3aWR0aDogOTAlO1xuICBwYWRkaW5nOiA3cHg7XG4gIHBhZGRpbmctdG9wOiAwO1xufVxuXG4uZm9yX3ByaWNlIHtcbiAgZm9udC1zaXplOiAyMHB4O1xuICBmb250LXdlaWdodDogYm9sZGVyO1xuICBwYWRkaW5nLXRvcDogNXB4O1xufVxuXG4uZm9yX2l0ZW1uYW1lIHtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBmb250LXdlaWdodDogYm9sZGVyO1xufVxuXG4uZm9yX2l0ZW1jYXRlZyB7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgZm9udC13ZWlnaHQ6IGxpZ2h0ZXI7XG59XG5cbi5mb3Jfc3RhdHVzX2l0ZW0ge1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBmb250LXNpemU6IDEycHg7XG4gIGZvbnQtd2VpZ2h0OiBsaWdodGVyO1xufVxuXG4uZm9yX2l0ZW1sb2NhdGlvbiB7XG4gIGZvbnQtc2l6ZTogMTNweDtcbiAgZm9udC13ZWlnaHQ6IGxpZ2h0ZXI7XG59XG5cbi5mb3JfcGluIHtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBtYXJnaW4tYm90dG9tOiAtMSU7XG4gIG1hcmdpbi1sZWZ0OiAtMSU7XG59XG5cbi5mb3JfaXRlbWJ0biB7XG4gIGZvbnQtc2l6ZTogMTFweDtcbn1cblxuLmZvcl9pdGVtYnRuaWNvbiB7XG4gIGZvbnQtc2l6ZTogMjBweDtcbiAgbWFyZ2luOiAwO1xufVxuXG4uZm9yX2l0ZW1idG5zb2xkIHtcbiAgbWFyZ2luOiAwO1xuICB3aWR0aDogMTVweDtcbiAgaGVpZ2h0OiBhdXRvO1xuICBtYXJnaW4tcmlnaHQ6IDUlO1xufVxuXG4uY29sMSB7XG4gIG1hcmdpbi1sZWZ0OiAtMzBweDtcbn1cblxuLmNvbDIge1xuICBtYXJnaW4tbGVmdDogMjVweDtcbn1cblxuLmZvcl9pdGVtc29sZCB7XG4gIC0tcGFkZGluZy1zdGFydDogMDtcbiAgbWFyZ2luLXRvcDogLTEwcHg7XG4gIC0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG4gIG1hcmdpbi1yaWdodDogLTMwcHg7XG4gIG1hcmdpbi1ib3R0b206IC0xMHB4O1xufVxuXG4uZm9yX2NvbHMge1xuICBwYWRkaW5nLWJvdHRvbTogNXB4O1xuICBwYWRkaW5nLXRvcDogNXB4O1xufVxuXG4uZm9yX2VhY2hyb3cge1xuICBtYXJnaW4tdG9wOiAxMHB4O1xufVxuXG4uZm9yX2l0ZW1pbWdidXlpbmcge1xuICBwYWRkaW5nLWxlZnQ6IDVweDtcbiAgbWFyZ2luOiBhdXRvO1xuICB3aWR0aDogOTAlO1xuICBoZWlnaHQ6IDcwJTtcbn1cblxuLmZvcl9jb2xidXlpbmcge1xuICBwYWRkaW5nLXRvcDogMDtcbiAgbWFyZ2luLXRvcDogLThweDtcbn1cblxuaW9uLXRodW1ibmFpbCB7XG4gIG1pbi13aWR0aDogNXJlbTtcbiAgbWluLWhlaWdodDogNXJlbTtcbn1cbmlvbi10aHVtYm5haWwgaW1nIHtcbiAgbWF4LXdpZHRoOiA1cmVtO1xuICBtaW4td2lkdGg6IDVyZW07XG59XG5cbi5mb3JfaXRlbW1zIHtcbiAgLS1wYWRkaW5nLXN0YXJ0OiAwO1xuICBtYXJnaW4tdG9wOiAtOHB4O1xuICAtLWlubmVyLXBhZGRpbmctZW5kOiAwO1xuICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xufVxuLmZvcl9pdGVtbXMgaW9uLWF2YXRhciB7XG4gIHdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XG4gIGhlaWdodDogMTAwJSAhaW1wb3J0YW50O1xuICBtYXgtd2lkdGg6IDI1cHggIWltcG9ydGFudDtcbiAgbWF4LWhlaWdodDogMjVweCAhaW1wb3J0YW50O1xuICBtYXJnaW4tcmlnaHQ6IDVweDtcbiAgbWFyZ2luLXRvcDogMDtcbn1cbi5mb3JfaXRlbW1zIGlvbi1hdmF0YXIgaW1nIHtcbiAgYm9yZGVyOiAxcHggc29saWQgcmdiYSgwLCAwLCAwLCAwLjEyKTtcbiAgd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcbiAgaGVpZ2h0OiAxMDAlICFpbXBvcnRhbnQ7XG4gIG1heC13aWR0aDogMjVweCAhaW1wb3J0YW50O1xuICBtYXgtaGVpZ2h0OiAyNXB4ICFpbXBvcnRhbnQ7XG4gIG1hcmdpbi1yaWdodDogNXB4O1xuICBtYXJnaW4tdG9wOiAwO1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG59XG5cbi5mb3JfbmFtZSB7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG4gIG1hcmdpbi10b3A6IDA7XG4gIGNvbG9yOiBibGFjaztcbn1cblxuLmZvcl9pdGVtbWltZyB7XG4gIG1hcmdpbjogYXV0bztcbiAgd2lkdGg6IDkwJTtcbn1cblxuLmZvcl9pdGVtbWltZ2J1eWluZyB7XG4gIG1hcmdpbjogYXV0bztcbiAgd2lkdGg6IDY1JTtcbiAgbWFyZ2luLXRvcDogLTEwJTtcbiAgcGFkZGluZy1ib3R0b206IDVweDtcbn1cblxuLmZvcl9wcHJpY2Uge1xuICBmb250LXNpemU6IDIzcHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkZXI7XG59XG5cbi5mb3JfY2F0ZWdvcnkge1xuICBiYWNrZ3JvdW5kOiAjNzZjOTYxO1xuICBwYWRkaW5nOiAzcHg7XG4gIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiAxMHB4O1xuICBtYXJnaW4tdG9wOiAtNXB4O1xuICBtYXJnaW4tcmlnaHQ6IC01cHg7XG4gIGJvcmRlci1ib3R0b20tbGVmdC1yYWRpdXM6IDEwcHg7XG4gIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xuICBmb250LXNpemU6IDEycHg7XG4gIGZvbnQtd2VpZ2h0OiBsaWdodGVyO1xuICBjb2xvcjogd2hpdGU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmZvcl9pdGVtbW5hbWUge1xuICBmb250LXNpemU6IDE1cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkZXI7XG59XG5cbi5mb3JfaXRlbW1sb2NhdGlvbiB7XG4gIGZvbnQtc2l6ZTogMTIuNXB4O1xuICBmb250LXdlaWdodDogbGlnaHRlcjtcbiAgY29sb3I6ICM5ODlhYTI7XG4gIHBhZGRpbmctcmlnaHQ6IDVweDtcbiAgcGFkZGluZy1ib3R0b206IDIuNSU7XG59XG5cbi5mb3JfYXZhaWxhYmxlIHtcbiAgYm9yZGVyLXRvcC1sZWZ0LXJhZGl1czogMTBweDtcbiAgYm9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6IDEwcHg7XG4gIGZvbnQtc2l6ZTogMTBweDtcbiAgY29sb3I6ICM2Nzk3MzM7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIGJhY2tncm91bmQ6ICNlMmYwY2I7XG4gIHBhZGRpbmc6IDNweDtcbiAgbWFyZ2luLXRvcDogLTVweDtcbiAgbWFyZ2luLWxlZnQ6IC01cHg7XG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XG59XG5cbi5mb3JfYm9yZGVyaXRlbSB7XG4gIGJvcmRlci10b3A6IDFweCBzb2xpZCAjZTJmMGNiO1xuICBwYWRkaW5nLXRvcDogMi41JTtcbn1cblxuLmZvcl9pdGVtbWJ0biB7XG4gIGZvbnQtc2l6ZTogMTFweDtcbn1cblxuLmZvcl91bml0cXR5IHtcbiAgZm9udC1zaXplOiAxMXB4O1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICBjb2xvcjogIzk4OWFhMjtcbn1cblxuLm15SGVhcnRVblNhdmUge1xuICBtYXJnaW46IDA7XG4gIGNvbG9yOiAjZDNkYmM5O1xuICBmb250LXNpemU6IDIwcHg7XG4gIG1hcmdpbi1yaWdodDogMyU7XG59XG5cbi5teUhlYXJ0U2F2ZSB7XG4gIG1hcmdpbjogMDtcbiAgY29sb3I6IHJlZDtcbiAgZm9udC1zaXplOiAyMHB4O1xuICBtYXJnaW4tcmlnaHQ6IDMlO1xufVxuXG4uZm9yX2FsaWduY2VudGVyIHtcbiAgZGlzcGxheTogZmxleCAhaW1wb3J0YW50O1xuICBhbGlnbi1jb250ZW50OiBjZW50ZXIgIWltcG9ydGFudDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlciAhaW1wb3J0YW50O1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/offers/offers.page.ts":
/*!***************************************!*\
  !*** ./src/app/offers/offers.page.ts ***!
  \***************************************/
/*! exports provided: OffersPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OffersPage", function() { return OffersPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _shared_model_item_model__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../shared/model/item.model */ "./src/app/shared/model/item.model.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _providers_credential_provider__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../providers/credential-provider */ "./src/providers/credential-provider.ts");







let OffersPage = class OffersPage {
    constructor(router, toastController, postPvdr, storage, alertCtrl, route, navCtrl) {
        this.router = router;
        this.toastController = toastController;
        this.postPvdr = postPvdr;
        this.storage = storage;
        this.alertCtrl = alertCtrl;
        this.route = route;
        this.navCtrl = navCtrl;
        this.segmentSelling = 0;
        this.segmentBuying = 0;
        this.segment = 0;
        this.fromRate = false;
        this.itemList = [];
        this.itemListArchive = [];
        this.buyItemList = [];
        this.buyItemListArchive = [];
        this.login_user_id = "";
        this.route.queryParams.subscribe(params => {
            this.fromRate = params["fromRate"];
            // console.log("params in productview:"+JSON.stringify(params));
        });
    }
    segmentChanged() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            yield this.slider.slideTo(this.segment);
        });
    }
    slideChanged() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.segment = yield this.slider.getActiveIndex();
        });
    }
    ngOnInit() {
        //  this.plotData();
    }
    ionViewWillEnter() {
        this.plotData();
    }
    plotData() {
        if (this.fromRate) {
            this.segmentSelling = 1;
        }
        this.storage.get('greenthumb_user_id').then((user_id) => {
            this.login_user_id = user_id;
            let body2 = {
                action: 'getSellerPost',
                user_id: user_id,
            };
            console.log(JSON.stringify(body2));
            this.postPvdr.postData(body2, 'post_item.php').subscribe((data) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                if (data.success) {
                    const items = [];
                    //var pictureProfile: string = '';
                    var x = 0;
                    for (const key in data.result) {
                        items.push(new _shared_model_item_model__WEBPACK_IMPORTED_MODULE_4__["Item"](data.result[key].item_id, data.result[key].user_id, data.result[key].username, (data.result[key].profile_photo == '') ? '' :
                            this.postPvdr.myServer() + "/greenthumb/images/" + data.result[key].profile_photo, data.result[key].price, data.result[key].title, data.result[key].stocks_id, data.result[key].others_stock, parseInt(data.result[key].quantity), data.result[key].category, data.result[key].location, data.result[key].saveStatus, data.result[key].status, this.postPvdr.myServer() + "/greenthumb/images/posted_items/images/" + data.result[key].item_photo, data.result[key].user_rating, data.result[key].date, data.result[key].time));
                    }
                    this.itemList = items;
                }
            }));
            let body2Archive = {
                action: 'getSellerPostArchive',
                user_id: user_id,
            };
            //console.log(JSON.stringify(body2));
            this.postPvdr.postData(body2Archive, 'post_item.php').subscribe((data) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                if (data.success) {
                    const itemsArchive = [];
                    //var pictureProfile: string = '';
                    var x = 0;
                    for (const key in data.result) {
                        itemsArchive.push(new _shared_model_item_model__WEBPACK_IMPORTED_MODULE_4__["Item"](data.result[key].item_id, data.result[key].user_id, data.result[key].username, (data.result[key].profile_photo == '') ? '' :
                            this.postPvdr.myServer() + "/greenthumb/images/" + data.result[key].profile_photo, data.result[key].price, data.result[key].title, data.result[key].stocks_id, data.result[key].others_stock, parseInt(data.result[key].quantity), data.result[key].category, data.result[key].location, data.result[key].saveStatus, data.result[key].status, this.postPvdr.myServer() + "/greenthumb/images/posted_items/images/" + data.result[key].item_photo, data.result[key].user_rating, data.result[key].date, data.result[key].time));
                    }
                    this.itemListArchive = itemsArchive;
                }
            }));
            let body21 = {
                action: 'getBuying',
                user_id: user_id,
            };
            console.log(JSON.stringify(body21));
            this.postPvdr.postData(body21, 'post_item.php').subscribe((data) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                if (data.success) {
                    const buyitems = [];
                    //var pictureProfile: string = '';
                    var x = 0;
                    for (const key in data.result) {
                        buyitems.push(new _shared_model_item_model__WEBPACK_IMPORTED_MODULE_4__["Item"](data.result[key].item_id, data.result[key].user_id, data.result[key].username, (data.result[key].profile_photo == '') ? '' :
                            this.postPvdr.myServer() + "/greenthumb/images/" + data.result[key].profile_photo, data.result[key].price, data.result[key].title, data.result[key].stocks_id, data.result[key].others_stock, parseInt(data.result[key].quantity), data.result[key].category, data.result[key].location, data.result[key].saveStatus, data.result[key].status, this.postPvdr.myServer() + "/greenthumb/images/posted_items/images/" + data.result[key].item_photo, data.result[key].user_rating, data.result[key].date, data.result[key].time));
                    }
                    this.buyItemList = buyitems;
                }
            }));
            let body21Archive = {
                action: 'getBuyingArchive',
                user_id: user_id,
            };
            //console.log(JSON.stringify(body21));
            this.postPvdr.postData(body21Archive, 'post_item.php').subscribe((data) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                if (data.success) {
                    const buyitemsArchive = [];
                    //var pictureProfile: string = '';
                    var x = 0;
                    for (const key in data.result) {
                        buyitemsArchive.push(new _shared_model_item_model__WEBPACK_IMPORTED_MODULE_4__["Item"](data.result[key].item_id, data.result[key].user_id, data.result[key].username, (data.result[key].profile_photo == '') ? '' :
                            this.postPvdr.myServer() + "/greenthumb/images/" + data.result[key].profile_photo, data.result[key].price, data.result[key].title, data.result[key].stocks_id, data.result[key].others_stock, parseInt(data.result[key].quantity), data.result[key].category, data.result[key].location, data.result[key].saveStatus, data.result[key].status, this.postPvdr.myServer() + "/greenthumb/images/posted_items/images/" + data.result[key].item_photo, data.result[key].user_rating, data.result[key].date, data.result[key].time));
                    }
                    this.buyItemListArchive = buyitemsArchive;
                }
            }));
        });
    }
    segmentSellingChanged() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            yield this.sliderSelling.slideTo(this.segmentSelling);
        });
    }
    segmentBuyingChanged() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            yield this.sliderBuying.slideTo(this.segmentBuying);
        });
    }
    slideSellingChanged() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.segmentSelling = yield this.sliderSelling.getActiveIndex();
        });
    }
    slideBuyingChanged() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.segmentBuying = yield this.sliderBuying.getActiveIndex();
        });
    }
    deleteItem(item_id) {
        this.alertCtrl.create({
            header: "Are you sure to delete this item?",
            message: "",
            buttons: [
                {
                    text: "No",
                    handler: () => {
                        console.log("Disagree clicked");
                    }
                },
                {
                    text: "Yes",
                    handler: () => {
                        console.log("Agree clicked");
                        this.confirmDelete(item_id);
                    }
                }
            ]
        }).then(res => {
            res.present();
        });
    }
    confirmDelete(item_id) {
        let body = {
            action: 'deleteItem',
            item_id: item_id
        };
        this.postPvdr.postData(body, 'save_item.php').subscribe(data => {
            if (data.success) {
                this.toastSuccessDelete();
                this.plotData();
            }
        });
    }
    toastSuccessDelete() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: 'Item Successfully Deleted',
                duration: 3000
            });
            toast.present();
        });
    }
    markSold(item_id, item_status) {
        //console.log("gi saved lamang pud");
        let body = {
            action: 'markSoldItem',
            item_id: item_id,
            user_id: this.login_user_id,
            item_status: item_status
        };
        this.postPvdr.postData(body, 'save_item.php').subscribe(data => {
            if (data.success) {
                // if(data.saveStatus == '1'){
                //   console.log('1 ko');
                //   var element = document.getElementById("class"+item_id);
                //   element.classList.remove("myHeartUnSave");
                //   element.classList.add("myHeartSave");
                //   console.log("classList:"+element.classList);
                // }
                // else{
                //   console.log("0 ko");
                //   var element = document.getElementById("class"+item_id);
                //   element.classList.remove("myHeartSave");
                //   element.classList.add("myHeartUnSave");
                // }
            }
        });
    }
    goBack() {
        window.history.back();
    }
    goCategories() {
        this.router.navigate(['categories']);
    }
    goHelp() {
        this.router.navigate(['tabs/help']);
    }
    goSellingProductview() {
        this.router.navigate(['sellingproductview']);
    }
    goProductview(item_id) {
        // this.router.navigate(['productview']);
        let navigationExtras = {
            queryParams: { item_id }
        };
        this.navCtrl.navigateForward(['productview'], navigationExtras);
    }
    goMessage(item_user_id, item_id) {
        this.navCtrl.navigateRoot(['/message/' + item_user_id + "-separator-" + item_id]);
    }
    goEdit(item_id) {
        let navigationExtras = {
            queryParams: { item_id }
        };
        this.navCtrl.navigateRoot(['edititem'], navigationExtras);
    }
};
OffersPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"] },
    { type: _providers_credential_provider__WEBPACK_IMPORTED_MODULE_6__["PostProvider"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('slides', { static: true }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonSlides"])
], OffersPage.prototype, "slider", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('slidesSelling', { static: true }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonSlides"])
], OffersPage.prototype, "sliderSelling", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('slidesBuying', { static: true }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonSlides"])
], OffersPage.prototype, "sliderBuying", void 0);
OffersPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-offers',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./offers.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/offers/offers.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./offers.page.scss */ "./src/app/offers/offers.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"],
        _providers_credential_provider__WEBPACK_IMPORTED_MODULE_6__["PostProvider"],
        _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"],
        _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"]])
], OffersPage);



/***/ })

}]);
//# sourceMappingURL=offers-offers-module-es2015.js.map