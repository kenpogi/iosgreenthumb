(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["paymentrequest-paymentrequest-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/paymentrequest/paymentrequest.page.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/paymentrequest/paymentrequest.page.html ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header mode=\"ios\" >\n  <ion-toolbar mode=\"ios\" style=\"--background: transparent;\">\n    <ion-buttons slot=\"secondary\" (click)=\"goBack()\">\n      <ion-button>\n          <ion-icon name=\"arrow-back\"  mode=\"ios\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n      <ion-title style=\"font-size: 14px;\">Earnings & Payment Request</ion-title>\n\n      <ion-buttons slot=\"primary\">\n          <ion-button>\n              <img src=\"assets/icon/brixylogo.png\" style=\"width: 32px;\" mode=\"ios\">\n          </ion-button>\n        </ion-buttons>\n    </ion-toolbar>\n</ion-header>\n\n<ion-content mode=\"ios\" text-center>\n    \n    <!-- <ion-row *ngIf=\"cashout_status==0\">\n      <ion-col style=\"color: black\" mode=\"ios\" text-left>\n          <ion-label class=\"lvltext\"><b>You are unable to cashout as of this moment. Please reach 50k gold bars to cashout.</b>&nbsp;&nbsp;</ion-label>\n      </ion-col>\n    </ion-row> -->\n\n    <div class=\"overlaybackground\"><div class=\"wrapper\"></div></div>\n      <ion-toolbar mode=\"ios\" style=\"--background: transparent;padding-top: 3%;\">\n        <ion-text color=\"warning\"><p style=\"font-size: 15px;\"><b>Your Total Accumulated Gold Bars</b></p></ion-text>\n        <ion-text color=\"warning\"><h4 style=\"margin-top: 0%;\"><img class=\"lvlicon\" src=\"assets/icon/gold.png\">\n            <b class=\"lvlnum\">14,566{{gold_bar | number:'1.0':'en-US'}}</b></h4></ion-text>\n        <ion-text color=\"warning\"><p class=\"text_gold\"><b>\n            You need to collect a total number of 50,000.00 \n            GOLD BARS to unlock the cashout method.\n          </b></p></ion-text>\n        <div class=\"form_overlay\">\n          <ion-card mode=\"ios\" style=\"box-shadow: none;margin-top: 10%;\">\n            <img mode=\"ios\" src=\"assets/icon/brixylogo.png\" style=\"width: 40%;margin: auto;padding-top: 5%;\" />\n            <ion-list class=\"line-input\">\n              <ion-row>\n                <ion-col>\n                  <ion-label color=\"dark\" class=\"item_label\">Payoneer ID</ion-label>\n                  <ion-item>\n                    <ion-input text-center type=\"text\" [(ngModel)]=\"payoneerId\" mode=\"ios\" class=\"item_input\" size=\"small\"></ion-input>\n                  </ion-item>\n                </ion-col>\n              </ion-row>\n            </ion-list>\n            <ion-card-header mode=\"ios\" style=\"padding-bottom: 0;\">\n              <ion-card-title style=\"font-weight: bolder;text-transform: uppercase;\">Choose A Payout Option</ion-card-title>\n            </ion-card-header>\n            <ion-card-content mode=\"ios\" no-padding>\n              <ion-list class=\"line-input\">\n                <ion-row>\n                  <ion-col>\n                    <ion-button expand=\"block\" color=\"dark\" fill=\"outline\" shape=\"round\" mode=\"ios\" class=\"payoutbtn\">Payout 1</ion-button>\n                  </ion-col>\n                  <ion-col>\n                    <ion-button expand=\"block\" color=\"dark\" fill=\"outline\" shape=\"round\" mode=\"ios\" class=\"payoutbtn\">Payout 2</ion-button>\n                  </ion-col>\n                </ion-row>\n                <ion-row>\n                    <ion-col>\n                      <ion-button expand=\"block\" color=\"dark\" fill=\"outline\"  shape=\"round\" mode=\"ios\" class=\"payoutbtn\">Payout 3</ion-button>\n                    </ion-col>\n                    <ion-col>\n                      <ion-button expand=\"block\" color=\"dark\" fill=\"outline\" shape=\"round\" mode=\"ios\" class=\"payoutbtn\">Payout 4</ion-button>\n                    </ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col>\n                      <ion-button expand=\"block\" color=\"dark\" fill=\"outline\" shape=\"round\" mode=\"ios\" class=\"payoutbtn\">Payout 5</ion-button>\n                    </ion-col>\n                    <ion-col>\n                      <ion-button expand=\"block\" color=\"dark\" fill=\"outline\" shape=\"round\" mode=\"ios\" class=\"payoutbtn\">Payout 6</ion-button>\n                    </ion-col>\n                  </ion-row>\n              </ion-list>\n              <ion-list>\n                <ion-row>\n                  <ion-col>\n                    <ion-button expand=\"block\" mode=\"ios\">SEND PAYOUT REQUEST</ion-button>\n                  </ion-col>\n                </ion-row>\n              </ion-list>\n            </ion-card-content>\n          </ion-card>\n        </div>\n      </ion-toolbar>\n\n    \n    <!-- <ion-row>\n        <ion-col style=\"color: black\" mode=\"ios\" text-left>\n            <ion-label class=\"lvltext\"><b>GOLD BARS:</b>&nbsp;&nbsp;</ion-label> <br>\n            <img class=\"lvlicon\" src=\"assets/icon/gold.png\">\n            <ion-text class=\"lvlnum\">{{gold_bar | number:'1.0':'en-US'}}</ion-text>\n        </ion-col>\n        <ion-col style=\"color: black;margin-top: 1.2%;\" mode=\"ios\" text-right>\n            <ion-label class=\"lvltext\"><b>COINS:</b>&nbsp;&nbsp;</ion-label><br>\n            <img class=\"lvlicon2\" src=\"assets/icon/imgcoin.png\">\n            <ion-text class=\"lvlnum\">{{user_coins | number:'1.0':'en-US'}}</ion-text>\n        </ion-col>\n    </ion-row>\n\n    <ion-grid>\n      <ion-row  mode=\"ios\" text-center style=\"background: #1dc1e6;border-radius: 30px;width: 100%;\">\n          <ion-col style=\"color: white\" >\n              <ion-label>Payment Request&nbsp;&nbsp;</ion-label>\n          </ion-col>\n      </ion-row>\n      <ion-row>\n          <ion-col>\n              <ion-list  mode=\"ios\">\n                  <ion-item *ngFor=\"let x of paychartList;\" (click)=\"cashout_status==1 ? paymentRequest(x): false;\">\n                      <ion-label style=\"font-size: 12px\">\n                          <ion-icon name=\"logo-bitcoin\" style=\"zoom: 1.2\" color=\"warning\"></ion-icon>{{x.gold_bars}}</ion-label>\n                      <ion-note slot=\"end\" color=\"danger\" no-padding style=\"padding-top: 5px;\">\n                          <ion-button [disabled]=\"cashout_status==0\"  mode=\"ios\" size=\"small\" color=\"success\" fill=\"outline\" style=\"font-size:11px\">\n                                  USD {{x.usd_amount}}</ion-button>\n                      </ion-note>\n                  </ion-item>\n              </ion-list>\n          </ion-col>\n      </ion-row>\n  </ion-grid> -->\n\n   \n</ion-content>\n");

/***/ }),

/***/ "./src/app/paymentrequest/paymentrequest-routing.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/paymentrequest/paymentrequest-routing.module.ts ***!
  \*****************************************************************/
/*! exports provided: PaymentrequestPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaymentrequestPageRoutingModule", function() { return PaymentrequestPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _paymentrequest_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./paymentrequest.page */ "./src/app/paymentrequest/paymentrequest.page.ts");




const routes = [
    {
        path: '',
        component: _paymentrequest_page__WEBPACK_IMPORTED_MODULE_3__["PaymentrequestPage"]
    }
];
let PaymentrequestPageRoutingModule = class PaymentrequestPageRoutingModule {
};
PaymentrequestPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], PaymentrequestPageRoutingModule);



/***/ }),

/***/ "./src/app/paymentrequest/paymentrequest.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/paymentrequest/paymentrequest.module.ts ***!
  \*********************************************************/
/*! exports provided: PaymentrequestPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaymentrequestPageModule", function() { return PaymentrequestPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _paymentrequest_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./paymentrequest-routing.module */ "./src/app/paymentrequest/paymentrequest-routing.module.ts");
/* harmony import */ var _paymentrequest_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./paymentrequest.page */ "./src/app/paymentrequest/paymentrequest.page.ts");







let PaymentrequestPageModule = class PaymentrequestPageModule {
};
PaymentrequestPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _paymentrequest_routing_module__WEBPACK_IMPORTED_MODULE_5__["PaymentrequestPageRoutingModule"]
        ],
        declarations: [_paymentrequest_page__WEBPACK_IMPORTED_MODULE_6__["PaymentrequestPage"]]
    })
], PaymentrequestPageModule);



/***/ }),

/***/ "./src/app/paymentrequest/paymentrequest.page.scss":
/*!*********************************************************!*\
  !*** ./src/app/paymentrequest/paymentrequest.page.scss ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".lvlicon {\n  width: 25px !important;\n  height: 25px;\n  margin: auto;\n  margin-bottom: -1%;\n  margin-right: 3%;\n}\n\n.lvlnum {\n  margin: 0;\n  margin-top: 4%;\n  font-family: Verdana, Geneva, Tahoma, sans-serif;\n}\n\n.text_gold {\n  font-size: 11px;\n  padding-left: 18px;\n  padding-right: 18px;\n  margin-top: 5%;\n}\n\n.payoutbtn {\n  border-radius: 25px;\n}\n\n.overlaybackground {\n  width: 100%;\n  height: 230px;\n  position: absolute;\n  top: 0px;\n  left: 0px;\n  background: black;\n  background-image: url(\"/assets/icon/background1.jpg\");\n  background-position: center center;\n  background-repeat: no-repeat;\n  background-size: cover;\n}\n\n.wrapper {\n  width: 100%;\n  height: 230px;\n  position: absolute;\n  top: 0px;\n  left: 0px;\n  background-color: #4242429e;\n  -webkit-backdrop-filter: blur(10px);\n          backdrop-filter: blur(10px);\n}\n\n.form_overlay {\n  background: white;\n  background: white;\n  width: 95%;\n  margin: auto;\n  --background: var(--ion-item-background,transparent);\n  --color: var(--ion-color-step-600,#666);\n  border-radius: 8px;\n  transform: translateZ(0);\n  transition: transform 0.5s cubic-bezier(0.12, 0.72, 0.29, 1);\n  font-size: 14px;\n  box-shadow: 0 4px 16px rgba(0, 0, 0, 0.12);\n}\n\n.line-input {\n  margin-bottom: 0 !important;\n}\n\n.line-input ion-item {\n  --border-color: transparent!important;\n  --highlight-height: 0;\n  border: 1px solid #222428;\n  border-radius: 4px;\n  height: 40px;\n  margin-top: 4%;\n}\n\n.item_input {\n  font-size: 14px;\n  --padding-top: 0;\n  color: #424242 !important;\n}\n\n.item_label {\n  font-weight: 300;\n  font-size: 13px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZW5kcmljay9EZXNrdG9wL1RlbG1vIFNvbHV0aW9ucy9Gb3JQcm9kdWN0aW9uL21heTEwMjAyMS9sYXN0IHNldHVwL2dyZWVudGh1bWJhcHAgY29weS9zcmMvYXBwL3BheW1lbnRyZXF1ZXN0L3BheW1lbnRyZXF1ZXN0LnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGF5bWVudHJlcXVlc3QvcGF5bWVudHJlcXVlc3QucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksc0JBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7QUNDSjs7QURDRTtFQUNFLFNBQUE7RUFDQSxjQUFBO0VBQ0EsZ0RBQUE7QUNFSjs7QURBRTtFQUNFLGVBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsY0FBQTtBQ0dKOztBRERBO0VBSUksbUJBQUE7QUNDSjs7QURFRTtFQUNFLFdBQUE7RUFDRSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSxRQUFBO0VBQ0EsU0FBQTtFQUNBLGlCQUFBO0VBQ0EscURBQUE7RUFDRixrQ0FBQTtFQUNBLDRCQUFBO0VBQ0Esc0JBQUE7QUNDSjs7QURDSTtFQUNFLFdBQUE7RUFDRSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSxRQUFBO0VBQ0EsU0FBQTtFQUNBLDJCQUFBO0VBQ0EsbUNBQUE7VUFBQSwyQkFBQTtBQ0VSOztBREFJO0VBQ0ksaUJBQUE7RUFBa0IsaUJBQUE7RUFDbEIsVUFBQTtFQUNBLFlBQUE7RUFBaUIsb0RBQUE7RUFDakIsdUNBQUE7RUFDQSxrQkFBQTtFQUVBLHdCQUFBO0VBR0EsNERBQUE7RUFFQSxlQUFBO0VBRUEsMENBQUE7QUNLUjs7QURISTtFQUNJLDJCQUFBO0FDTVI7O0FETFE7RUFDSSxxQ0FBQTtFQUNBLHFCQUFBO0VBRUMseUJBQUE7RUFDRCxrQkFBQTtFQUNBLFlBQUE7RUFDQSxjQUFBO0FDTVo7O0FESEk7RUFDSSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSx5QkFBQTtBQ01SOztBREpJO0VBRUksZ0JBQUE7RUFDQSxlQUFBO0FDTVIiLCJmaWxlIjoic3JjL2FwcC9wYXltZW50cmVxdWVzdC9wYXltZW50cmVxdWVzdC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubHZsaWNvbntcbiAgICB3aWR0aDogMjVweCAhaW1wb3J0YW50OyBcbiAgICBoZWlnaHQ6IDI1cHg7IFxuICAgIG1hcmdpbjogYXV0bztcbiAgICBtYXJnaW4tYm90dG9tOiAtMSU7XG4gICAgbWFyZ2luLXJpZ2h0OiAzJTtcbiAgfSAgXG4gIC5sdmxudW17XG4gICAgbWFyZ2luOiAwO1xuICAgIG1hcmdpbi10b3A6IDQlO1xuICAgIGZvbnQtZmFtaWx5OiBWZXJkYW5hLCBHZW5ldmEsIFRhaG9tYSwgc2Fucy1zZXJpZjtcbiAgfVxuICAudGV4dF9nb2xke1xuICAgIGZvbnQtc2l6ZTogMTFweDtcbiAgICBwYWRkaW5nLWxlZnQ6IDE4cHg7XG4gICAgcGFkZGluZy1yaWdodDogMThweDtcbiAgICBtYXJnaW4tdG9wOiA1JTtcbiAgfVxuLnBheW91dGJ0bntcbiAgLy8gLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgLy8gICBjb2xvcjogIzk4OWFhMjtcbiAgLy8gICBib3JkZXI6IDFweCBzb2xpZCAjOTg5YWEyO1xuICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XG59XG5cbiAgLm92ZXJsYXliYWNrZ3JvdW5ke1xuICAgIHdpZHRoOiAxMDAlO1xuICAgICAgaGVpZ2h0OiAyMzBweDtcbiAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgIHRvcDogMHB4O1xuICAgICAgbGVmdDogMHB4O1xuICAgICAgYmFja2dyb3VuZDogYmxhY2s7XG4gICAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoJy9hc3NldHMvaWNvbi9iYWNrZ3JvdW5kMS5qcGcnKTtcbiAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXIgY2VudGVyO1xuICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICB9XG4gICAgLndyYXBwZXJ7XG4gICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgaGVpZ2h0OiAyMzBweDtcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICB0b3A6IDBweDtcbiAgICAgICAgbGVmdDogMHB4O1xuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNDI0MjQyOWU7XG4gICAgICAgIGJhY2tkcm9wLWZpbHRlcjogYmx1cigxMHB4KTtcbiAgICAgIH1cbiAgICAuZm9ybV9vdmVybGF5e1xuICAgICAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgICAgICAgd2lkdGg6IDk1JTtcbiAgICAgICAgbWFyZ2luOiBhdXRvOyAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1pdGVtLWJhY2tncm91bmQsdHJhbnNwYXJlbnQpO1xuICAgICAgICAtLWNvbG9yOiB2YXIoLS1pb24tY29sb3Itc3RlcC02MDAsIzY2Nik7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDhweDtcbiAgICAgICAgLXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZVooMCk7XG4gICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWigwKTtcbiAgICAgICAgLXdlYmtpdC10cmFuc2l0aW9uOiAtd2Via2l0LXRyYW5zZm9ybSAuNXMgY3ViaWMtYmV6aWVyKC4xMiwuNzIsLjI5LDEpO1xuICAgICAgICB0cmFuc2l0aW9uOiAtd2Via2l0LXRyYW5zZm9ybSAuNXMgY3ViaWMtYmV6aWVyKC4xMiwuNzIsLjI5LDEpO1xuICAgICAgICB0cmFuc2l0aW9uOiB0cmFuc2Zvcm0gLjVzIGN1YmljLWJlemllciguMTIsLjcyLC4yOSwxKTtcbiAgICAgICAgdHJhbnNpdGlvbjogdHJhbnNmb3JtIC41cyBjdWJpYy1iZXppZXIoLjEyLC43MiwuMjksMSksLXdlYmtpdC10cmFuc2Zvcm0gLjVzIGN1YmljLWJlemllciguMTIsLjcyLC4yOSwxKTtcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICAtd2Via2l0LWJveC1zaGFkb3c6IDAgNHB4IDE2cHggcmdiYSgwLDAsMCwuMTIpO1xuICAgICAgICBib3gtc2hhZG93OiAwIDRweCAxNnB4IHJnYmEoMCwwLDAsLjEyKTtcbiAgICB9XG4gICAgLmxpbmUtaW5wdXQge1xuICAgICAgICBtYXJnaW4tYm90dG9tOiAwIWltcG9ydGFudDtcbiAgICAgICAgaW9uLWl0ZW0ge1xuICAgICAgICAgICAgLS1ib3JkZXItY29sb3I6IHRyYW5zcGFyZW50IWltcG9ydGFudDtcbiAgICAgICAgICAgIC0taGlnaGxpZ2h0LWhlaWdodDogMDtcbiAgICAgICAgICAgIC8vIGJvcmRlcjogMXB4IHNvbGlkICNkZWRlZGU7XG4gICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgIzIyMjQyODtcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgICAgICAgICAgIGhlaWdodDogNDBweDtcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDQlO1xuICAgICAgICB9XG4gICAgfVxuICAgIC5pdGVtX2lucHV0e1xuICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgIC0tcGFkZGluZy10b3A6IDA7XG4gICAgICAgIGNvbG9yOiAjNDI0MjQyIWltcG9ydGFudDtcbiAgICB9XG4gICAgLml0ZW1fbGFiZWx7XG4gICAgICAgIC8vIGNvbG9yOiAjYjNhZWFlICFpbXBvcnRhbnQ7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiAzMDA7XG4gICAgICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgICB9IiwiLmx2bGljb24ge1xuICB3aWR0aDogMjVweCAhaW1wb3J0YW50O1xuICBoZWlnaHQ6IDI1cHg7XG4gIG1hcmdpbjogYXV0bztcbiAgbWFyZ2luLWJvdHRvbTogLTElO1xuICBtYXJnaW4tcmlnaHQ6IDMlO1xufVxuXG4ubHZsbnVtIHtcbiAgbWFyZ2luOiAwO1xuICBtYXJnaW4tdG9wOiA0JTtcbiAgZm9udC1mYW1pbHk6IFZlcmRhbmEsIEdlbmV2YSwgVGFob21hLCBzYW5zLXNlcmlmO1xufVxuXG4udGV4dF9nb2xkIHtcbiAgZm9udC1zaXplOiAxMXB4O1xuICBwYWRkaW5nLWxlZnQ6IDE4cHg7XG4gIHBhZGRpbmctcmlnaHQ6IDE4cHg7XG4gIG1hcmdpbi10b3A6IDUlO1xufVxuXG4ucGF5b3V0YnRuIHtcbiAgYm9yZGVyLXJhZGl1czogMjVweDtcbn1cblxuLm92ZXJsYXliYWNrZ3JvdW5kIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMjMwcHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAwcHg7XG4gIGxlZnQ6IDBweDtcbiAgYmFja2dyb3VuZDogYmxhY2s7XG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybChcIi9hc3NldHMvaWNvbi9iYWNrZ3JvdW5kMS5qcGdcIik7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlciBjZW50ZXI7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG59XG5cbi53cmFwcGVyIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMjMwcHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAwcHg7XG4gIGxlZnQ6IDBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzQyNDI0MjllO1xuICBiYWNrZHJvcC1maWx0ZXI6IGJsdXIoMTBweCk7XG59XG5cbi5mb3JtX292ZXJsYXkge1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIHdpZHRoOiA5NSU7XG4gIG1hcmdpbjogYXV0bztcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24taXRlbS1iYWNrZ3JvdW5kLHRyYW5zcGFyZW50KTtcbiAgLS1jb2xvcjogdmFyKC0taW9uLWNvbG9yLXN0ZXAtNjAwLCM2NjYpO1xuICBib3JkZXItcmFkaXVzOiA4cHg7XG4gIC13ZWJraXQtdHJhbnNmb3JtOiB0cmFuc2xhdGVaKDApO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVooMCk7XG4gIC13ZWJraXQtdHJhbnNpdGlvbjogLXdlYmtpdC10cmFuc2Zvcm0gMC41cyBjdWJpYy1iZXppZXIoMC4xMiwgMC43MiwgMC4yOSwgMSk7XG4gIHRyYW5zaXRpb246IC13ZWJraXQtdHJhbnNmb3JtIDAuNXMgY3ViaWMtYmV6aWVyKDAuMTIsIDAuNzIsIDAuMjksIDEpO1xuICB0cmFuc2l0aW9uOiB0cmFuc2Zvcm0gMC41cyBjdWJpYy1iZXppZXIoMC4xMiwgMC43MiwgMC4yOSwgMSk7XG4gIHRyYW5zaXRpb246IHRyYW5zZm9ybSAwLjVzIGN1YmljLWJlemllcigwLjEyLCAwLjcyLCAwLjI5LCAxKSwgLXdlYmtpdC10cmFuc2Zvcm0gMC41cyBjdWJpYy1iZXppZXIoMC4xMiwgMC43MiwgMC4yOSwgMSk7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiAwIDRweCAxNnB4IHJnYmEoMCwgMCwgMCwgMC4xMik7XG4gIGJveC1zaGFkb3c6IDAgNHB4IDE2cHggcmdiYSgwLCAwLCAwLCAwLjEyKTtcbn1cblxuLmxpbmUtaW5wdXQge1xuICBtYXJnaW4tYm90dG9tOiAwICFpbXBvcnRhbnQ7XG59XG4ubGluZS1pbnB1dCBpb24taXRlbSB7XG4gIC0tYm9yZGVyLWNvbG9yOiB0cmFuc3BhcmVudCFpbXBvcnRhbnQ7XG4gIC0taGlnaGxpZ2h0LWhlaWdodDogMDtcbiAgYm9yZGVyOiAxcHggc29saWQgIzIyMjQyODtcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xuICBoZWlnaHQ6IDQwcHg7XG4gIG1hcmdpbi10b3A6IDQlO1xufVxuXG4uaXRlbV9pbnB1dCB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgLS1wYWRkaW5nLXRvcDogMDtcbiAgY29sb3I6ICM0MjQyNDIgIWltcG9ydGFudDtcbn1cblxuLml0ZW1fbGFiZWwge1xuICBmb250LXdlaWdodDogMzAwO1xuICBmb250LXNpemU6IDEzcHg7XG59Il19 */");

/***/ }),

/***/ "./src/app/paymentrequest/paymentrequest.page.ts":
/*!*******************************************************!*\
  !*** ./src/app/paymentrequest/paymentrequest.page.ts ***!
  \*******************************************************/
/*! exports provided: PaymentrequestPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaymentrequestPage", function() { return PaymentrequestPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _shared_model_paychart_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../shared/model/paychart.model */ "./src/app/shared/model/paychart.model.ts");
/* harmony import */ var _providers_credential_provider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../providers/credential-provider */ "./src/providers/credential-provider.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _payreqconfirmation_payreqconfirmation_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../payreqconfirmation/payreqconfirmation.page */ "./src/app/payreqconfirmation/payreqconfirmation.page.ts");







let PaymentrequestPage = class PaymentrequestPage {
    constructor(storage, postPvdr, modalController, toastController) {
        this.storage = storage;
        this.postPvdr = postPvdr;
        this.modalController = modalController;
        this.toastController = toastController;
        this.paychartList = [];
        this.payoneerId = "";
    }
    goBack() {
        window.history.back();
    }
    ngOnInit() {
        this.plotData();
    }
    paymentRequest(paychart) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            if (this.gold_bar < paychart.gold_bars) {
                this.presentToast();
            }
            else {
                const modal = yield this.modalController.create({
                    component: _payreqconfirmation_payreqconfirmation_page__WEBPACK_IMPORTED_MODULE_6__["PayreqconfirmationPage"],
                    cssClass: 'sendgiftmodal',
                    componentProps: { paychart: paychart }
                });
                modal.onDidDismiss()
                    .then((data) => {
                    //const user = data['data']; // Here's your selected user!
                    //this.showCoinsandGold();
                });
                return yield modal.present();
            }
        });
    }
    presentToast() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: 'You have insufficient balance of gold bars. ',
                duration: 3000
            });
            toast.present();
        });
    }
    plotData() {
        this.storage.get("user_id").then((user_id) => {
            let body = {
                action: 'getuserdata',
                user_id: user_id
            };
            this.postPvdr.postData(body, 'credentials-api.php').subscribe((data) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                if (data.success) {
                    this.gold_bar = data.result.gold_bar;
                    this.user_coins = data.result.user_coins;
                    this.cashout_status = data.result.cashout_status;
                }
            }));
            let body3 = {
                action: 'getPayoneerId',
                user_id: user_id
            };
            console.log("what?:" + JSON.stringify(body3));
            this.postPvdr.postData(body3, 'payment.php').subscribe(data => {
                if (data.success) {
                    for (const key in data.result) {
                        this.payoneerId = data.result[key].payment_id;
                    }
                    console.log("mypayoneerId:" + this.payoneerId);
                }
            });
        });
        let body2 = {
            action: 'showPaychart'
        };
        this.postPvdr.postData(body2, 'payment.php').subscribe(data => {
            if (data.success) {
                const paychart = [];
                for (const key in data.result) {
                    paychart.push(new _shared_model_paychart_model__WEBPACK_IMPORTED_MODULE_3__["paychartModel"](data.result[key].id, data.result[key].gold_bars, data.result[key].usd_amount));
                }
                this.paychartList = paychart;
            }
        });
    }
};
PaymentrequestPage.ctorParameters = () => [
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_2__["Storage"] },
    { type: _providers_credential_provider__WEBPACK_IMPORTED_MODULE_4__["PostProvider"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ModalController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ToastController"] }
];
PaymentrequestPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-paymentrequest',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./paymentrequest.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/paymentrequest/paymentrequest.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./paymentrequest.page.scss */ "./src/app/paymentrequest/paymentrequest.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_storage__WEBPACK_IMPORTED_MODULE_2__["Storage"],
        _providers_credential_provider__WEBPACK_IMPORTED_MODULE_4__["PostProvider"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ModalController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ToastController"]])
], PaymentrequestPage);



/***/ }),

/***/ "./src/app/shared/model/paychart.model.ts":
/*!************************************************!*\
  !*** ./src/app/shared/model/paychart.model.ts ***!
  \************************************************/
/*! exports provided: paychartModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "paychartModel", function() { return paychartModel; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");

class paychartModel {
    constructor(id, gold_bars, usd_amount) {
        this.id = id;
        this.gold_bars = gold_bars;
        this.usd_amount = usd_amount;
    }
}


/***/ })

}]);
//# sourceMappingURL=paymentrequest-paymentrequest-module-es2015.js.map