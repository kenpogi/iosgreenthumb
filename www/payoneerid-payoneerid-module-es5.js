function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["payoneerid-payoneerid-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/payoneerid/payoneerid.page.html":
  /*!***************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/payoneerid/payoneerid.page.html ***!
    \***************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPayoneeridPayoneeridPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header mode=\"ios\" >\n  <ion-toolbar mode=\"ios\" style=\"--background: transparent;\">\n    <ion-buttons slot=\"secondary\" (click)=\"goBack()\">\n      <ion-button>\n          <ion-icon name=\"arrow-back\"  mode=\"ios\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n      <ion-title style=\"font-size: 14px;\">Payoneer ID</ion-title>\n\n      <ion-buttons slot=\"primary\">\n          <ion-button>\n              <img src=\"assets/icon/brixylogo.png\" style=\"width: 32px;\" mode=\"ios\">\n          </ion-button>\n        </ion-buttons>\n    </ion-toolbar>\n</ion-header>\n\n\n<ion-content mode=\"ios\" text-center>\n\n  <div class=\"overlaybackground\"><div class=\"wrapper\"></div></div>\n      <ion-toolbar mode=\"ios\" style=\"--background: transparent;padding-top: 3%;\">\n        <img src=\"assets/icon/brixylogo.png\" style=\"width: 40%;\" mode=\"ios\">\n        \n        <div class=\"form_overlay\">\n          <ion-card mode=\"ios\" style=\"box-shadow: none;margin-top: 3%;\">\n            <img mode=\"ios\" src=\"assets/icon/payoneer.png\" style=\"width: 50%;margin: auto;padding-top: 10%;\" />\n            <ion-card-content mode=\"ios\" no-padding>\n              <ion-list class=\"line-input\">\n                <ion-row>\n                  <ion-col>\n                    <ion-item>\n                      <ion-input placeholder=\"Payoneer ID\" [(ngModel)]=\"payoneerId\" mode=\"ios\" type=\"text\" class=\"item_input\" size=\"small\"></ion-input>\n                    </ion-item>\n                  </ion-col>\n                </ion-row>\n              </ion-list>\n              <ion-list>\n                <ion-row>\n                  <ion-col>\n                    <ion-button (click)=\"addPaymentId()\"  expand=\"block\" mode=\"ios\">SUBMIT/EDIT</ion-button>\n                  </ion-col>\n                </ion-row>\n              </ion-list>\n            </ion-card-content> <br>\n          </ion-card>\n        </div>\n      </ion-toolbar>\n\n      <ion-row text-center>\n        <ion-col>\n          <p text-center style=\"font-size: 13px;padding-left: 20px;padding-right: 20px;\">\n            Brixy Live uses Payooner as the payment system for cash out requests. \n            Please enter or double check if the Payooner ID you have entered is correct.\n          </p>\n        </ion-col>\n    </ion-row>\n    <ion-row text-center padding style=\"padding-top: 0;\">\n      <ion-col>\n        <ion-button mode=\"ios\" expand=\"block\" (click)=\"goPaymentreq()\" color=\"medium\">SKIP</ion-button>\n      </ion-col>\n  </ion-row>\n   \n\n\n</ion-content>\n";
    /***/
  },

  /***/
  "./src/app/payoneerid/payoneerid-routing.module.ts":
  /*!*********************************************************!*\
    !*** ./src/app/payoneerid/payoneerid-routing.module.ts ***!
    \*********************************************************/

  /*! exports provided: PayoneeridPageRoutingModule */

  /***/
  function srcAppPayoneeridPayoneeridRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PayoneeridPageRoutingModule", function () {
      return PayoneeridPageRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _payoneerid_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./payoneerid.page */
    "./src/app/payoneerid/payoneerid.page.ts");

    var routes = [{
      path: '',
      component: _payoneerid_page__WEBPACK_IMPORTED_MODULE_3__["PayoneeridPage"]
    }];

    var PayoneeridPageRoutingModule = function PayoneeridPageRoutingModule() {
      _classCallCheck(this, PayoneeridPageRoutingModule);
    };

    PayoneeridPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], PayoneeridPageRoutingModule);
    /***/
  },

  /***/
  "./src/app/payoneerid/payoneerid.module.ts":
  /*!*************************************************!*\
    !*** ./src/app/payoneerid/payoneerid.module.ts ***!
    \*************************************************/

  /*! exports provided: PayoneeridPageModule */

  /***/
  function srcAppPayoneeridPayoneeridModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PayoneeridPageModule", function () {
      return PayoneeridPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _payoneerid_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./payoneerid-routing.module */
    "./src/app/payoneerid/payoneerid-routing.module.ts");
    /* harmony import */


    var _payoneerid_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./payoneerid.page */
    "./src/app/payoneerid/payoneerid.page.ts");

    var PayoneeridPageModule = function PayoneeridPageModule() {
      _classCallCheck(this, PayoneeridPageModule);
    };

    PayoneeridPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _payoneerid_routing_module__WEBPACK_IMPORTED_MODULE_5__["PayoneeridPageRoutingModule"]],
      declarations: [_payoneerid_page__WEBPACK_IMPORTED_MODULE_6__["PayoneeridPage"]]
    })], PayoneeridPageModule);
    /***/
  },

  /***/
  "./src/app/payoneerid/payoneerid.page.scss":
  /*!*************************************************!*\
    !*** ./src/app/payoneerid/payoneerid.page.scss ***!
    \*************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPayoneeridPayoneeridPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".overlaybackground {\n  width: 100%;\n  height: 230px;\n  position: absolute;\n  top: 0px;\n  left: 0px;\n  background: black;\n  background-image: url(\"/assets/icon/background1.jpg\");\n  background-position: center center;\n  background-repeat: no-repeat;\n  background-size: cover;\n}\n\n.wrapper {\n  width: 100%;\n  height: 230px;\n  position: absolute;\n  top: 0px;\n  left: 0px;\n  background-color: #4242428c;\n  -webkit-backdrop-filter: blur(10px);\n          backdrop-filter: blur(10px);\n}\n\n.form_overlay {\n  background: white;\n  background: white;\n  width: 95%;\n  margin: auto;\n  --background: var(--ion-item-background,transparent);\n  --color: var(--ion-color-step-600,#666);\n  border-radius: 8px;\n  transform: translateZ(0);\n  transition: transform 0.5s cubic-bezier(0.12, 0.72, 0.29, 1);\n  font-size: 14px;\n  box-shadow: 0 4px 16px rgba(0, 0, 0, 0.12);\n}\n\n.line-input {\n  margin-bottom: 0 !important;\n}\n\n.line-input ion-item {\n  --border-color: transparent!important;\n  --highlight-height: 0;\n  border: 1px solid #dedede;\n  border-radius: 4px;\n  height: 40px;\n  margin-top: 4%;\n}\n\n.item_input {\n  font-size: 14px;\n  --padding-top: 0;\n  color: #424242 !important;\n}\n\n.item_label {\n  color: #b3aeae !important;\n  font-weight: 300;\n  font-size: 13px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZW5kcmljay9EZXNrdG9wL1RlbG1vIFNvbHV0aW9ucy9Gb3JQcm9kdWN0aW9uL21heTEwMjAyMS9sYXN0IHNldHVwL2dyZWVudGh1bWJhcHAgY29weS9zcmMvYXBwL3BheW9uZWVyaWQvcGF5b25lZXJpZC5wYWdlLnNjc3MiLCJzcmMvYXBwL3BheW9uZWVyaWQvcGF5b25lZXJpZC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0U7RUFDRSxXQUFBO0VBQ0UsYUFBQTtFQUNBLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLFNBQUE7RUFDQSxpQkFBQTtFQUNBLHFEQUFBO0VBQ0Ysa0NBQUE7RUFDQSw0QkFBQTtFQUNBLHNCQUFBO0FDQUo7O0FERUk7RUFDRSxXQUFBO0VBQ0UsYUFBQTtFQUNBLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLFNBQUE7RUFDQSwyQkFBQTtFQUNBLG1DQUFBO1VBQUEsMkJBQUE7QUNDUjs7QURDQTtFQUNJLGlCQUFBO0VBQWtCLGlCQUFBO0VBQ2xCLFVBQUE7RUFDQSxZQUFBO0VBQWlCLG9EQUFBO0VBQ2pCLHVDQUFBO0VBQ0Esa0JBQUE7RUFFQSx3QkFBQTtFQUdBLDREQUFBO0VBRUEsZUFBQTtFQUVBLDBDQUFBO0FDSUo7O0FERkE7RUFDSSwyQkFBQTtBQ0tKOztBREpJO0VBQ0kscUNBQUE7RUFDQSxxQkFBQTtFQUNBLHlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsY0FBQTtBQ01SOztBREhBO0VBQ0ksZUFBQTtFQUNBLGdCQUFBO0VBQ0EseUJBQUE7QUNNSjs7QURKQTtFQUNJLHlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0FDT0oiLCJmaWxlIjoic3JjL2FwcC9wYXlvbmVlcmlkL3BheW9uZWVyaWQucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXG4gIC5vdmVybGF5YmFja2dyb3VuZHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICAgIGhlaWdodDogMjMwcHg7XG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICB0b3A6IDBweDtcbiAgICAgIGxlZnQ6IDBweDtcbiAgICAgIGJhY2tncm91bmQ6IGJsYWNrO1xuICAgICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKCcvYXNzZXRzL2ljb24vYmFja2dyb3VuZDEuanBnJyk7XG4gICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyIGNlbnRlcjtcbiAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgfVxuICAgIC53cmFwcGVye1xuICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIGhlaWdodDogMjMwcHg7XG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgdG9wOiAwcHg7XG4gICAgICAgIGxlZnQ6IDBweDtcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzQyNDI0MjhjO1xuICAgICAgICBiYWNrZHJvcC1maWx0ZXI6IGJsdXIoMTBweCk7XG4gICAgICB9XG4uZm9ybV9vdmVybGF5e1xuICAgIGJhY2tncm91bmQ6IHdoaXRlO2JhY2tncm91bmQ6IHdoaXRlO1xuICAgIHdpZHRoOiA5NSU7XG4gICAgbWFyZ2luOiBhdXRvOyAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1pdGVtLWJhY2tncm91bmQsdHJhbnNwYXJlbnQpO1xuICAgIC0tY29sb3I6IHZhcigtLWlvbi1jb2xvci1zdGVwLTYwMCwjNjY2KTtcbiAgICBib3JkZXItcmFkaXVzOiA4cHg7XG4gICAgLXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZVooMCk7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVaKDApO1xuICAgIC13ZWJraXQtdHJhbnNpdGlvbjogLXdlYmtpdC10cmFuc2Zvcm0gLjVzIGN1YmljLWJlemllciguMTIsLjcyLC4yOSwxKTtcbiAgICB0cmFuc2l0aW9uOiAtd2Via2l0LXRyYW5zZm9ybSAuNXMgY3ViaWMtYmV6aWVyKC4xMiwuNzIsLjI5LDEpO1xuICAgIHRyYW5zaXRpb246IHRyYW5zZm9ybSAuNXMgY3ViaWMtYmV6aWVyKC4xMiwuNzIsLjI5LDEpO1xuICAgIHRyYW5zaXRpb246IHRyYW5zZm9ybSAuNXMgY3ViaWMtYmV6aWVyKC4xMiwuNzIsLjI5LDEpLC13ZWJraXQtdHJhbnNmb3JtIC41cyBjdWJpYy1iZXppZXIoLjEyLC43MiwuMjksMSk7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgIC13ZWJraXQtYm94LXNoYWRvdzogMCA0cHggMTZweCByZ2JhKDAsMCwwLC4xMik7XG4gICAgYm94LXNoYWRvdzogMCA0cHggMTZweCByZ2JhKDAsMCwwLC4xMik7XG59XG4ubGluZS1pbnB1dCB7XG4gICAgbWFyZ2luLWJvdHRvbTogMCFpbXBvcnRhbnQ7XG4gICAgaW9uLWl0ZW0ge1xuICAgICAgICAtLWJvcmRlci1jb2xvcjogdHJhbnNwYXJlbnQhaW1wb3J0YW50O1xuICAgICAgICAtLWhpZ2hsaWdodC1oZWlnaHQ6IDA7XG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICNkZWRlZGU7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgICAgICAgaGVpZ2h0OiA0MHB4O1xuICAgICAgICBtYXJnaW4tdG9wOiA0JTtcbiAgICB9XG59XG4uaXRlbV9pbnB1dHtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgLS1wYWRkaW5nLXRvcDogMDtcbiAgICBjb2xvcjogIzQyNDI0MiFpbXBvcnRhbnQ7XG59XG4uaXRlbV9sYWJlbHtcbiAgICBjb2xvcjogI2IzYWVhZSAhaW1wb3J0YW50O1xuICAgIGZvbnQtd2VpZ2h0OiAzMDA7XG4gICAgZm9udC1zaXplOiAxM3B4O1xufSIsIi5vdmVybGF5YmFja2dyb3VuZCB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDIzMHB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMHB4O1xuICBsZWZ0OiAwcHg7XG4gIGJhY2tncm91bmQ6IGJsYWNrO1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCIvYXNzZXRzL2ljb24vYmFja2dyb3VuZDEuanBnXCIpO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXIgY2VudGVyO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xufVxuXG4ud3JhcHBlciB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDIzMHB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMHB4O1xuICBsZWZ0OiAwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICM0MjQyNDI4YztcbiAgYmFja2Ryb3AtZmlsdGVyOiBibHVyKDEwcHgpO1xufVxuXG4uZm9ybV9vdmVybGF5IHtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICB3aWR0aDogOTUlO1xuICBtYXJnaW46IGF1dG87XG4gIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWl0ZW0tYmFja2dyb3VuZCx0cmFuc3BhcmVudCk7XG4gIC0tY29sb3I6IHZhcigtLWlvbi1jb2xvci1zdGVwLTYwMCwjNjY2KTtcbiAgYm9yZGVyLXJhZGl1czogOHB4O1xuICAtd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlWigwKTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGVaKDApO1xuICAtd2Via2l0LXRyYW5zaXRpb246IC13ZWJraXQtdHJhbnNmb3JtIDAuNXMgY3ViaWMtYmV6aWVyKDAuMTIsIDAuNzIsIDAuMjksIDEpO1xuICB0cmFuc2l0aW9uOiAtd2Via2l0LXRyYW5zZm9ybSAwLjVzIGN1YmljLWJlemllcigwLjEyLCAwLjcyLCAwLjI5LCAxKTtcbiAgdHJhbnNpdGlvbjogdHJhbnNmb3JtIDAuNXMgY3ViaWMtYmV6aWVyKDAuMTIsIDAuNzIsIDAuMjksIDEpO1xuICB0cmFuc2l0aW9uOiB0cmFuc2Zvcm0gMC41cyBjdWJpYy1iZXppZXIoMC4xMiwgMC43MiwgMC4yOSwgMSksIC13ZWJraXQtdHJhbnNmb3JtIDAuNXMgY3ViaWMtYmV6aWVyKDAuMTIsIDAuNzIsIDAuMjksIDEpO1xuICBmb250LXNpemU6IDE0cHg7XG4gIC13ZWJraXQtYm94LXNoYWRvdzogMCA0cHggMTZweCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xuICBib3gtc2hhZG93OiAwIDRweCAxNnB4IHJnYmEoMCwgMCwgMCwgMC4xMik7XG59XG5cbi5saW5lLWlucHV0IHtcbiAgbWFyZ2luLWJvdHRvbTogMCAhaW1wb3J0YW50O1xufVxuLmxpbmUtaW5wdXQgaW9uLWl0ZW0ge1xuICAtLWJvcmRlci1jb2xvcjogdHJhbnNwYXJlbnQhaW1wb3J0YW50O1xuICAtLWhpZ2hsaWdodC1oZWlnaHQ6IDA7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNkZWRlZGU7XG4gIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgaGVpZ2h0OiA0MHB4O1xuICBtYXJnaW4tdG9wOiA0JTtcbn1cblxuLml0ZW1faW5wdXQge1xuICBmb250LXNpemU6IDE0cHg7XG4gIC0tcGFkZGluZy10b3A6IDA7XG4gIGNvbG9yOiAjNDI0MjQyICFpbXBvcnRhbnQ7XG59XG5cbi5pdGVtX2xhYmVsIHtcbiAgY29sb3I6ICNiM2FlYWUgIWltcG9ydGFudDtcbiAgZm9udC13ZWlnaHQ6IDMwMDtcbiAgZm9udC1zaXplOiAxM3B4O1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/payoneerid/payoneerid.page.ts":
  /*!***********************************************!*\
    !*** ./src/app/payoneerid/payoneerid.page.ts ***!
    \***********************************************/

  /*! exports provided: PayoneeridPage */

  /***/
  function srcAppPayoneeridPayoneeridPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PayoneeridPage", function () {
      return PayoneeridPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var _providers_credential_provider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../providers/credential-provider */
    "./src/providers/credential-provider.ts");

    var PayoneeridPage = /*#__PURE__*/function () {
      function PayoneeridPage(router, storage, postPvdr) {
        _classCallCheck(this, PayoneeridPage);

        this.router = router;
        this.storage = storage;
        this.postPvdr = postPvdr;
        this.payoneerId = "";
      }

      _createClass(PayoneeridPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.plotData();
        }
      }, {
        key: "goBack",
        value: function goBack() {
          // this.router.navigate(['myaccount']);
          window.history.back();
        }
      }, {
        key: "goPaymentreq",
        value: function goPaymentreq() {
          this.router.navigate(['paymentrequest']);
        }
      }, {
        key: "addPaymentId",
        value: function addPaymentId() {
          var _this = this;

          this.storage.get("user_id").then(function (user_id) {
            var body3 = {
              action: 'addPayoneerId',
              user_id: user_id,
              payment_type_id: '1',
              payment_id: _this.payoneerId
            };
            console.log("get:" + JSON.stringify(body3));

            _this.postPvdr.postData(body3, 'payment.php').subscribe(function (data) {
              if (data.success) {
                _this.router.navigate(['paymentrequest']);
              }
            });
          });
        }
      }, {
        key: "plotData",
        value: function plotData() {
          var _this2 = this;

          this.storage.get("user_id").then(function (user_id) {
            var body3 = {
              action: 'getPayoneerId',
              user_id: user_id
            };

            _this2.postPvdr.postData(body3, 'payment.php').subscribe(function (data) {
              if (data.success) {
                for (var key in data.result) {
                  _this2.payoneerId = data.result[key].payment_id;
                }
              }
            });
          });
        }
      }]);

      return PayoneeridPage;
    }();

    PayoneeridPage.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }, {
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"]
      }, {
        type: _providers_credential_provider__WEBPACK_IMPORTED_MODULE_4__["PostProvider"]
      }];
    };

    PayoneeridPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-payoneerid',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./payoneerid.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/payoneerid/payoneerid.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./payoneerid.page.scss */
      "./src/app/payoneerid/payoneerid.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"], _providers_credential_provider__WEBPACK_IMPORTED_MODULE_4__["PostProvider"]])], PayoneeridPage);
    /***/
  }
}]);
//# sourceMappingURL=payoneerid-payoneerid-module-es5.js.map