function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["readytogolive-readytogolive-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/readytogolive/readytogolive.page.html":
  /*!*********************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/readytogolive/readytogolive.page.html ***!
    \*********************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppReadytogoliveReadytogolivePageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header no-border mode=\"ios\">\n  <ion-toolbar mode=\"ios\">\n      <ion-buttons slot=\"secondary\" (click)=\"goBack()\" >\n        <ion-button  mode=\"ios\">\n            <ion-icon name=\"arrow-back\"  mode=\"ios\"></ion-icon>\n        </ion-button>\n      </ion-buttons>\n      <ion-title  mode=\"ios\">\n        <img src=\"assets/icon/imggolive2.gif\" style=\"width: 80px;padding-top: 6px;\" mode=\"ios\">\n      </ion-title>\n      <ion-buttons slot=\"primary\"  mode=\"ios\"> \n        <ion-button  mode=\"ios\">\n            <img src=\"assets/icon/brixylogo.png\" style=\"width: 32px;\" mode=\"ios\">\n        </ion-button>\n      </ion-buttons>\n    </ion-toolbar>\n</ion-header>\n\n<ion-content mode=\"ios\" padding>\n  <ion-text mode=\"ios\" text-center>\n      <p mode=\"ios\" style=\"font-size: 17px;font-weight: bold;\">Ready to broadcast your moment?</p>\n  </ion-text>\n  <ion-grid padding style=\"padding-bottom: 25px!important\">\n    <ion-row>\n      <ion-col text-center>\n        <img src=\"assets/icon/imgmakeup.png\" mode=\"ios\" style=\"padding:10px;width: 90%\">\n        <ion-label style=\"font-size: 14px;\">Makeup</ion-label>  \n      </ion-col>\n      <ion-col  text-center>\n        <img src=\"assets/icon/imgcontent.png\" mode=\"ios\" style=\"padding:10px;width: 90%\">\n         <ion-label style=\"font-size: 14px;\">Content</ion-label>\n      </ion-col>\n    </ion-row> <br>\n     <ion-row>\n      <ion-col text-center>\n        <img src=\"assets/icon/imgbackground.png\" mode=\"ios\" style=\"padding:10px;width: 90%\">\n        <ion-label style=\"font-size: 14px;\">Background</ion-label>  \n      </ion-col>\n      <ion-col  text-center>\n        <img src=\"assets/icon/imglighting.png\" mode=\"ios\" style=\"padding:10px;width: 90%\">\n         <ion-label style=\"font-size: 14px;\">Lighting</ion-label>\n      </ion-col>\n    </ion-row>\n  </ion-grid> \n  <br>\n  <ion-radio-group [(ngModel)]=\"myBroadcast\" name=\"broadcastType\" mode=\"ios\">\n  <ion-row>\n    <ion-col size=\"1\"></ion-col>\n\n    \n      <ion-col size=\"5\">\n        <ion-item text-left lines=\"none\">\n          <ion-radio  style=\"font-size: 10px;\" value=\"1\" ></ion-radio>&nbsp;&nbsp;&nbsp;&nbsp;\n          <ion-label style=\"font-size: 14px;font-weight: 500;\" ><b> PUBLIC</b></ion-label>\n        </ion-item>\n      </ion-col>\n      <ion-col size=\"5\">\n        <ion-item text-left lines=\"none\">\n          <ion-radio style=\"font-size: 10px;\" value=\"2\" ></ion-radio>&nbsp;&nbsp;&nbsp;&nbsp;\n          <ion-label style=\"font-size: 14px;font-weight: 500;\" ><b> PRIVATE</b></ion-label>\n        </ion-item>\n      </ion-col>  \n     \n    <ion-col size=\"1\"></ion-col>\n  </ion-row>\n</ion-radio-group>\n  <ion-row>\n    <ion-col text-center>\n      <button (click)=\"goLive()\" class=\"round-button\">Go Live</button>\n    </ion-col>\n  </ion-row>\n  \n</ion-content>\n";
    /***/
  },

  /***/
  "./src/app/readytogolive/readytogolive.module.ts":
  /*!*******************************************************!*\
    !*** ./src/app/readytogolive/readytogolive.module.ts ***!
    \*******************************************************/

  /*! exports provided: ReadytogolivePageModule */

  /***/
  function srcAppReadytogoliveReadytogoliveModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ReadytogolivePageModule", function () {
      return ReadytogolivePageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _readytogolive_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./readytogolive.page */
    "./src/app/readytogolive/readytogolive.page.ts");

    var routes = [{
      path: '',
      component: _readytogolive_page__WEBPACK_IMPORTED_MODULE_6__["ReadytogolivePage"]
    }];

    var ReadytogolivePageModule = function ReadytogolivePageModule() {
      _classCallCheck(this, ReadytogolivePageModule);
    };

    ReadytogolivePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      declarations: [_readytogolive_page__WEBPACK_IMPORTED_MODULE_6__["ReadytogolivePage"]]
    })], ReadytogolivePageModule);
    /***/
  },

  /***/
  "./src/app/readytogolive/readytogolive.page.scss":
  /*!*******************************************************!*\
    !*** ./src/app/readytogolive/readytogolive.page.scss ***!
    \*******************************************************/

  /*! exports provided: default */

  /***/
  function srcAppReadytogoliveReadytogolivePageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".round-button {\n  line-height: 40px;\n  border: 2px solid #f5f5f5;\n  border-radius: 100%;\n  color: #f5f5f5;\n  /* text-align: center; */\n  /* text-decoration: none; */\n  background: #1dc1e6;\n  box-shadow: 0 0 3px grey;\n  font-size: 14px;\n  font-weight: bold;\n  padding: 15px;\n}\n\n.round-button:hover {\n  background: #777555;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZW5kcmljay9EZXNrdG9wL1RlbG1vIFNvbHV0aW9ucy9Gb3JQcm9kdWN0aW9uL21heTEwMjAyMS9sYXN0IHNldHVwL2dyZWVudGh1bWJhcHAgY29weS9zcmMvYXBwL3JlYWR5dG9nb2xpdmUvcmVhZHl0b2dvbGl2ZS5wYWdlLnNjc3MiLCJzcmMvYXBwL3JlYWR5dG9nb2xpdmUvcmVhZHl0b2dvbGl2ZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxpQkFBQTtFQUNBLHlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxjQUFBO0VBQ0Esd0JBQUE7RUFDQSwyQkFBQTtFQUNBLG1CQUFBO0VBQ0Esd0JBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxhQUFBO0FDQ0o7O0FERUE7RUFDSSxtQkFBQTtBQ0NKIiwiZmlsZSI6InNyYy9hcHAvcmVhZHl0b2dvbGl2ZS9yZWFkeXRvZ29saXZlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5yb3VuZC1idXR0b24ge1xuICAgIGxpbmUtaGVpZ2h0OiA0MHB4O1xuICAgIGJvcmRlcjogMnB4IHNvbGlkICNmNWY1ZjU7XG4gICAgYm9yZGVyLXJhZGl1czogMTAwJTtcbiAgICBjb2xvcjogI2Y1ZjVmNTtcbiAgICAvKiB0ZXh0LWFsaWduOiBjZW50ZXI7ICovXG4gICAgLyogdGV4dC1kZWNvcmF0aW9uOiBub25lOyAqL1xuICAgIGJhY2tncm91bmQ6ICMxZGMxZTY7XG4gICAgYm94LXNoYWRvdzogMCAwIDNweCBncmV5O1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICBwYWRkaW5nOiAxNXB4O1xuXG59XG4ucm91bmQtYnV0dG9uOmhvdmVyIHtcbiAgICBiYWNrZ3JvdW5kOiAjNzc3NTU1O1xufSIsIi5yb3VuZC1idXR0b24ge1xuICBsaW5lLWhlaWdodDogNDBweDtcbiAgYm9yZGVyOiAycHggc29saWQgI2Y1ZjVmNTtcbiAgYm9yZGVyLXJhZGl1czogMTAwJTtcbiAgY29sb3I6ICNmNWY1ZjU7XG4gIC8qIHRleHQtYWxpZ246IGNlbnRlcjsgKi9cbiAgLyogdGV4dC1kZWNvcmF0aW9uOiBub25lOyAqL1xuICBiYWNrZ3JvdW5kOiAjMWRjMWU2O1xuICBib3gtc2hhZG93OiAwIDAgM3B4IGdyZXk7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIHBhZGRpbmc6IDE1cHg7XG59XG5cbi5yb3VuZC1idXR0b246aG92ZXIge1xuICBiYWNrZ3JvdW5kOiAjNzc3NTU1O1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/readytogolive/readytogolive.page.ts":
  /*!*****************************************************!*\
    !*** ./src/app/readytogolive/readytogolive.page.ts ***!
    \*****************************************************/

  /*! exports provided: ReadytogolivePage */

  /***/
  function srcAppReadytogoliveReadytogolivePageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ReadytogolivePage", function () {
      return ReadytogolivePage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");

    var ReadytogolivePage = /*#__PURE__*/function () {
      function ReadytogolivePage(storage, router) {
        _classCallCheck(this, ReadytogolivePage);

        this.storage = storage;
        this.router = router;
      }

      _createClass(ReadytogolivePage, [{
        key: "goBack",
        value: function goBack() {
          this.router.navigate(['termsconditions']);
        }
      }, {
        key: "goLive",
        value: function goLive() {
          this.storage.set('broadcast_type', this.myBroadcast);
          this.router.navigate(['livestream']);
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {
          this.myBroadcast = '1';
        }
      }]);

      return ReadytogolivePage;
    }();

    ReadytogolivePage.ctorParameters = function () {
      return [{
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_2__["Storage"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
      }];
    };

    ReadytogolivePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-readytogolive',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./readytogolive.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/readytogolive/readytogolive.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./readytogolive.page.scss */
      "./src/app/readytogolive/readytogolive.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_storage__WEBPACK_IMPORTED_MODULE_2__["Storage"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])], ReadytogolivePage);
    /***/
  }
}]);
//# sourceMappingURL=readytogolive-readytogolive-module-es5.js.map