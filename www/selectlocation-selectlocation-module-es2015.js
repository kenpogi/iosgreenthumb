(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["selectlocation-selectlocation-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/selectlocation/selectlocation.page.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/selectlocation/selectlocation.page.html ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header mode=\"ios\" no-border>\n  <ion-toolbar mode=\"ios\">\n      <ion-buttons slot=\"secondary\" (click)=\"goBack()\">\n        <ion-button mode=\"ios\" style=\"font-size: 15px;\">\n            Back\n        </ion-button>\n      </ion-buttons>\n\n      <ion-title color=\"secondary\">Delivery</ion-title>\n\n      <ion-buttons slot=\"primary\" (click)=\"goHelp()\">\n        <ion-button mode=\"ios\" style=\"font-size: 15px;\">\n            Help\n        </ion-button>\n      </ion-buttons>\n    </ion-toolbar>\n</ion-header>\n<ion-content padding>\n  <!-- <ion-list mode=\"ios\" style=\"margin-left: -5%;margin-bottom: 5%;\">\n    <ion-radio-group value=\"{{delivery_type_id}}\" mode=\"ios\">\n      <ion-item (click)=\"Divshow()\" mode=\"ios\" style=\"--border-color: #e2e2e2\">\n        <ion-label class=\"item_label\" color=\"secondary\">LOCAL PICKUP</ion-label>\n        <ion-radio style=\"zoom: 0.8;\" slot=\"start\" color=\"primary\" value=\"1\"></ion-radio>\n      </ion-item>\n      <ion-item (click)=\"Divhide()\" mode=\"ios\" style=\"--border-color: #e2e2e2\">\n        <ion-label class=\"item_label\" color=\"secondary\">SHIPPING</ion-label>\n        <ion-radio style=\"zoom: 0.8;\" slot=\"start\" color=\"primary\" value=\"2\"></ion-radio>\n      </ion-item>\n    </ion-radio-group>\n  </ion-list> -->\n  <ion-list *ngIf=\"showDiv\" class=\"line-input\">\n    <ion-label class=\"item_label\" color=\"secondary\">LOCATION</ion-label>\n    <ion-radio-group mode=\"ios\" value=\"2\">\n       <ion-item (click)=\"hide()\" style=\"margin-top: 1%;--background:transparent\">\n        <ion-radio style=\"zoom: 0.8;\" value=\"1\" ></ion-radio>&nbsp;&nbsp;&nbsp;&nbsp;\n        <ion-label style=\"font-size: 14px;\">(Default) Location</ion-label>\n       </ion-item>\n       <ion-item (click)=\"show()\">\n        <ion-radio style=\"zoom: 0.8;\" value=\"2\" ></ion-radio>&nbsp;&nbsp;&nbsp;&nbsp;\n        <ion-label style=\"font-size: 14px;\">Custom Location</ion-label>\n       </ion-item>\n    </ion-radio-group>\n  </ion-list>\n  <ion-list *ngIf=\"showMe\" class=\"line-inputcateg\" style=\"margin-top: -2%;\">\n    <ion-label class=\"item_label2\" color=\"secondary\">Enter Custom Location</ion-label>\n    <ion-item>\n      <ion-input mode=\"ios\" type=\"text\" [(ngModel)]=\"city\" class=\"item_input\" placeholder=\"City\" size=\"small\"></ion-input>\n     </ion-item>\n     <!-- <ion-item>\n      <ion-input mode=\"ios\" type=\"text\" [(ngModel)]=\"district_area\" class=\"item_input\" placeholder=\"District / Area\" size=\"small\"></ion-input>\n     </ion-item> -->\n     <ion-item>\n      <ion-input mode=\"ios\" type=\"number\" [(ngModel)]=\"zip_code\" class=\"item_input\" placeholder=\"Zip code\" size=\"small\"></ion-input>\n     </ion-item>\n     \n  </ion-list>\n\n  <ion-list class=\"for_accountitems\" *ngIf=\"hideDiv\">\n    <ion-label class=\"item_label\" color=\"secondary\">DELIVERY METHOD</ion-label>\n    <ion-item style=\"margin-top: 5px;\" mode=\"ios\" id=\"for_eachitem\">\n      <ion-label>Tacloban City, Philippines</ion-label>\n      <ion-note mode=\"ios\" color=\"secondary\" style=\"font-size: 13px;\">Edit</ion-note>\n    </ion-item>\n    <ion-item mode=\"ios\" id=\"for_eachitem\">\n      <ion-label>Sell & ship nationwide\n        <ion-text color=\"medium\">\n          <h6 style=\"font-size: 13px;\">12.9% service fee applies</h6>\n        </ion-text>\n        <ion-text color=\"secondary\">\n          <h6 style=\"font-size: 13px;font-weight: bold;\">Shipping Policy</h6>\n        </ion-text>\n      </ion-label>\n      <ion-toggle mode=\"ios\" slot=\"end\"></ion-toggle>\n    </ion-item>\n    <ion-item mode=\"ios\" id=\"for_eachitem\" lines=\"none\">\n      <ion-label>What size of package will you use?\n        <ion-text color=\"secondary\">\n          <h6 style=\"font-size: 13px;font-weight: bold;\">Help me decide</h6>\n        </ion-text>\n      </ion-label>\n    </ion-item>\n    <ion-radio-group mode=\"ios\">\n      <ion-item mode=\"ios\" id=\"for_eachitem\">\n        <ion-radio slot=\"start\"></ion-radio>\n        <ion-label>X-small\n          <ion-text color=\"medium\">\n            <h6 style=\"font-size: 13px;\">8oz (1/2 lb))</h6>\n          </ion-text>\n        </ion-label>\n        <img src=\"assets/greenthumb-images/xsmall.PNG\">\n      </ion-item>\n      <ion-item mode=\"ios\" id=\"for_eachitem\">\n        <ion-radio slot=\"start\"></ion-radio>\n        <ion-label>Small\n          <ion-text color=\"medium\">\n            <h6 style=\"font-size: 13px;\">Approx: 9\" x 6\" x 3\"</h6>\n          </ion-text>\n        </ion-label>\n        <img src=\"assets/greenthumb-images/small.PNG\">\n      </ion-item>\n      <ion-item mode=\"ios\" id=\"for_eachitem\">\n        <ion-radio slot=\"start\"></ion-radio>\n        <ion-label>Medium\n          <ion-text color=\"medium\">\n            <h6 style=\"font-size: 13px;\">Approx: 12\" x 9\" x 6\"</h6>\n          </ion-text>\n        </ion-label>\n        <img src=\"assets/greenthumb-images/medium.PNG\">\n      </ion-item>\n      <ion-item mode=\"ios\" id=\"for_eachitem\">\n        <ion-radio slot=\"start\"></ion-radio>\n        <ion-label>Large\n          <ion-text color=\"medium\">\n            <h6 style=\"font-size: 13px;\">Approx: 14\" x 10\" x 6\"</h6>\n          </ion-text>\n        </ion-label>\n        <img src=\"assets/greenthumb-images/large.PNG\">\n      </ion-item>\n    </ion-radio-group>\n    <ion-item mode=\"ios\" id=\"for_eachitem\">\n      <ion-label>Enable Buy Now\n        <ion-text color=\"medium\">\n          <h6 style=\"font-size: 13px;\">Auto-accept first full-price shipping offer</h6>\n        </ion-text>\n      </ion-label>\n      <ion-toggle mode=\"ios\" slot=\"end\"></ion-toggle>\n    </ion-item>\n  </ion-list>\n \n</ion-content>\n<div class=\"postitem-overlay\" padding>\n  <ion-button expand=\"block\" [disabled]=\"disableButton\" (click)=\"goShareitem()\" mode=\"ios\" fill=\"outline\" color=\"primary\">\n    NEXT\n  </ion-button>\n  <div class=\"postitem_container\">\n    <ul class=\"postitem_progressbar\">\n      <li class=\"whenfocus active\"><span>Photo</span></li>\n      <li class=\"whenfocus active\"><span>Details</span></li>\n      <li class=\"whenfocus active\"><span>Price</span></li>\n      <li class=\"whenfocus\"><span>Delivery</span></li>\n      <li><span>Post</span></li>\n    </ul>\n  </div>\n</div>");

/***/ }),

/***/ "./src/app/selectlocation/selectlocation-routing.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/selectlocation/selectlocation-routing.module.ts ***!
  \*****************************************************************/
/*! exports provided: SelectlocationPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelectlocationPageRoutingModule", function() { return SelectlocationPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _selectlocation_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./selectlocation.page */ "./src/app/selectlocation/selectlocation.page.ts");




const routes = [
    {
        path: '',
        component: _selectlocation_page__WEBPACK_IMPORTED_MODULE_3__["SelectlocationPage"]
    }
];
let SelectlocationPageRoutingModule = class SelectlocationPageRoutingModule {
};
SelectlocationPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], SelectlocationPageRoutingModule);



/***/ }),

/***/ "./src/app/selectlocation/selectlocation.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/selectlocation/selectlocation.module.ts ***!
  \*********************************************************/
/*! exports provided: SelectlocationPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelectlocationPageModule", function() { return SelectlocationPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _selectlocation_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./selectlocation-routing.module */ "./src/app/selectlocation/selectlocation-routing.module.ts");
/* harmony import */ var _selectlocation_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./selectlocation.page */ "./src/app/selectlocation/selectlocation.page.ts");







let SelectlocationPageModule = class SelectlocationPageModule {
};
SelectlocationPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _selectlocation_routing_module__WEBPACK_IMPORTED_MODULE_5__["SelectlocationPageRoutingModule"]
        ],
        declarations: [_selectlocation_page__WEBPACK_IMPORTED_MODULE_6__["SelectlocationPage"]]
    })
], SelectlocationPageModule);



/***/ }),

/***/ "./src/app/selectlocation/selectlocation.page.scss":
/*!*********************************************************!*\
  !*** ./src/app/selectlocation/selectlocation.page.scss ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".line-input {\n  margin-bottom: 0 !important;\n  background: transparent;\n}\n.line-input ion-item {\n  --border-color: transparent!important;\n  --highlight-height: 0;\n  margin-top: -5%;\n  --background:transparent;\n  --padding-start: 0;\n}\n.line-inputcateg {\n  margin-bottom: 0 !important;\n  background: transparent;\n}\n.line-inputcateg ion-item {\n  --border-color: transparent!important;\n  --highlight-height: 0;\n  border: 1px solid #e2e2e2;\n  border-radius: 4px;\n  height: 35px;\n  margin-top: 4%;\n  --background: #ffffffb5;\n}\n.item_input {\n  font-size: 13px;\n  margin-top: -15px;\n  color: #424242 !important;\n}\n.item_label {\n  font-weight: bold;\n  font-size: 15px;\n}\n.item_label2 {\n  font-weight: bold;\n  font-size: 14px;\n}\n.for_accountitems {\n  margin-top: 8%;\n  padding: 0;\n}\n#for_eachitem {\n  font-size: 14px;\n  --inner-padding-end: 0px !important;\n  --border-color: #e2f0cb;\n  --padding-start: 0% !important;\n}\n.for_eacheader {\n  font-size: 15px;\n  padding-left: 0;\n  font-weight: bold;\n  color: #679733;\n}\n.for_eachimg {\n  width: 17px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZW5kcmljay9EZXNrdG9wL1RlbG1vIFNvbHV0aW9ucy9Gb3JQcm9kdWN0aW9uL21heTEwMjAyMS9sYXN0IHNldHVwL2dyZWVudGh1bWJhcHAgY29weS9zcmMvYXBwL3NlbGVjdGxvY2F0aW9uL3NlbGVjdGxvY2F0aW9uLnBhZ2Uuc2NzcyIsInNyYy9hcHAvc2VsZWN0bG9jYXRpb24vc2VsZWN0bG9jYXRpb24ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksMkJBQUE7RUFDQSx1QkFBQTtBQ0NKO0FEQUk7RUFDSSxxQ0FBQTtFQUNBLHFCQUFBO0VBQ0EsZUFBQTtFQUNELHdCQUFBO0VBQ0Msa0JBQUE7QUNFUjtBRENFO0VBQ0UsMkJBQUE7RUFDQSx1QkFBQTtBQ0VKO0FEREk7RUFDSSxxQ0FBQTtFQUNBLHFCQUFBO0VBQ0EseUJBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxjQUFBO0VBQ0EsdUJBQUE7QUNHUjtBREFFO0VBQ0UsZUFBQTtFQUNBLGlCQUFBO0VBQ0EseUJBQUE7QUNHSjtBRERFO0VBQ0UsaUJBQUE7RUFDQSxlQUFBO0FDSUo7QURGRTtFQUNFLGlCQUFBO0VBQ0EsZUFBQTtBQ0tKO0FESEk7RUFDRSxjQUFBO0VBQ0EsVUFBQTtBQ01OO0FESkk7RUFDRSxlQUFBO0VBQ0EsbUNBQUE7RUFDQSx1QkFBQTtFQUNBLDhCQUFBO0FDT047QURMSTtFQUNFLGVBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0FDUU47QUROSTtFQUNFLFdBQUE7QUNTTiIsImZpbGUiOiJzcmMvYXBwL3NlbGVjdGxvY2F0aW9uL3NlbGVjdGxvY2F0aW9uLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5saW5lLWlucHV0IHtcbiAgICBtYXJnaW4tYm90dG9tOiAwIWltcG9ydGFudDtcbiAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgICBpb24taXRlbSB7XG4gICAgICAgIC0tYm9yZGVyLWNvbG9yOiB0cmFuc3BhcmVudCFpbXBvcnRhbnQ7XG4gICAgICAgIC0taGlnaGxpZ2h0LWhlaWdodDogMDtcbiAgICAgICAgbWFyZ2luLXRvcDogLTUlO1xuICAgICAgIC0tYmFja2dyb3VuZDp0cmFuc3BhcmVudDtcbiAgICAgICAgLS1wYWRkaW5nLXN0YXJ0OiAwO1xuICAgIH1cbiAgfVxuICAubGluZS1pbnB1dGNhdGVnIHtcbiAgICBtYXJnaW4tYm90dG9tOiAwIWltcG9ydGFudDtcbiAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgICBpb24taXRlbSB7XG4gICAgICAgIC0tYm9yZGVyLWNvbG9yOiB0cmFuc3BhcmVudCFpbXBvcnRhbnQ7XG4gICAgICAgIC0taGlnaGxpZ2h0LWhlaWdodDogMDtcbiAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2UyZTJlMjtcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNHB4O1xuICAgICAgICBoZWlnaHQ6IDM1cHg7XG4gICAgICAgIG1hcmdpbi10b3A6IDQlO1xuICAgICAgICAtLWJhY2tncm91bmQ6ICNmZmZmZmZiNTtcbiAgICB9XG4gIH1cbiAgLml0ZW1faW5wdXR7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIG1hcmdpbi10b3A6IC0xNXB4O1xuICAgIGNvbG9yOiAjNDI0MjQyIWltcG9ydGFudDtcbiAgfVxuICAuaXRlbV9sYWJlbHtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICBmb250LXNpemU6IDE1cHg7XG4gIH1cbiAgLml0ZW1fbGFiZWwye1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgfVxuICAgIC5mb3JfYWNjb3VudGl0ZW1ze1xuICAgICAgbWFyZ2luLXRvcDogOCU7XG4gICAgICBwYWRkaW5nOiAwO1xuICAgIH1cbiAgICAjZm9yX2VhY2hpdGVte1xuICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgLS1pbm5lci1wYWRkaW5nLWVuZDogMHB4ICFpbXBvcnRhbnQ7XG4gICAgICAtLWJvcmRlci1jb2xvcjogI2UyZjBjYjtcbiAgICAgIC0tcGFkZGluZy1zdGFydDogMCUgIWltcG9ydGFudDtcbiAgICB9XG4gICAgLmZvcl9lYWNoZWFkZXJ7XG4gICAgICBmb250LXNpemU6IDE1cHg7XG4gICAgICBwYWRkaW5nLWxlZnQ6IDA7XG4gICAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICAgIGNvbG9yOiAjNjc5NzMzO1xuICAgIH1cbiAgICAuZm9yX2VhY2hpbWd7XG4gICAgICB3aWR0aDogMTdweDtcbiAgICB9XG4gICAgIiwiLmxpbmUtaW5wdXQge1xuICBtYXJnaW4tYm90dG9tOiAwICFpbXBvcnRhbnQ7XG4gIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xufVxuLmxpbmUtaW5wdXQgaW9uLWl0ZW0ge1xuICAtLWJvcmRlci1jb2xvcjogdHJhbnNwYXJlbnQhaW1wb3J0YW50O1xuICAtLWhpZ2hsaWdodC1oZWlnaHQ6IDA7XG4gIG1hcmdpbi10b3A6IC01JTtcbiAgLS1iYWNrZ3JvdW5kOnRyYW5zcGFyZW50O1xuICAtLXBhZGRpbmctc3RhcnQ6IDA7XG59XG5cbi5saW5lLWlucHV0Y2F0ZWcge1xuICBtYXJnaW4tYm90dG9tOiAwICFpbXBvcnRhbnQ7XG4gIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xufVxuLmxpbmUtaW5wdXRjYXRlZyBpb24taXRlbSB7XG4gIC0tYm9yZGVyLWNvbG9yOiB0cmFuc3BhcmVudCFpbXBvcnRhbnQ7XG4gIC0taGlnaGxpZ2h0LWhlaWdodDogMDtcbiAgYm9yZGVyOiAxcHggc29saWQgI2UyZTJlMjtcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xuICBoZWlnaHQ6IDM1cHg7XG4gIG1hcmdpbi10b3A6IDQlO1xuICAtLWJhY2tncm91bmQ6ICNmZmZmZmZiNTtcbn1cblxuLml0ZW1faW5wdXQge1xuICBmb250LXNpemU6IDEzcHg7XG4gIG1hcmdpbi10b3A6IC0xNXB4O1xuICBjb2xvcjogIzQyNDI0MiAhaW1wb3J0YW50O1xufVxuXG4uaXRlbV9sYWJlbCB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBmb250LXNpemU6IDE1cHg7XG59XG5cbi5pdGVtX2xhYmVsMiB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBmb250LXNpemU6IDE0cHg7XG59XG5cbi5mb3JfYWNjb3VudGl0ZW1zIHtcbiAgbWFyZ2luLXRvcDogOCU7XG4gIHBhZGRpbmc6IDA7XG59XG5cbiNmb3JfZWFjaGl0ZW0ge1xuICBmb250LXNpemU6IDE0cHg7XG4gIC0taW5uZXItcGFkZGluZy1lbmQ6IDBweCAhaW1wb3J0YW50O1xuICAtLWJvcmRlci1jb2xvcjogI2UyZjBjYjtcbiAgLS1wYWRkaW5nLXN0YXJ0OiAwJSAhaW1wb3J0YW50O1xufVxuXG4uZm9yX2VhY2hlYWRlciB7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgcGFkZGluZy1sZWZ0OiAwO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgY29sb3I6ICM2Nzk3MzM7XG59XG5cbi5mb3JfZWFjaGltZyB7XG4gIHdpZHRoOiAxN3B4O1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/selectlocation/selectlocation.page.ts":
/*!*******************************************************!*\
  !*** ./src/app/selectlocation/selectlocation.page.ts ***!
  \*******************************************************/
/*! exports provided: SelectlocationPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SelectlocationPage", function() { return SelectlocationPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/geolocation/ngx */ "./node_modules/@ionic-native/geolocation/ngx/index.js");
/* harmony import */ var _providers_credential_provider__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../providers/credential-provider */ "./src/providers/credential-provider.ts");
/* harmony import */ var _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/native-geocoder/ngx */ "./node_modules/@ionic-native/native-geocoder/ngx/index.js");








let SelectlocationPage = class SelectlocationPage {
    constructor(router, storage, toastController, nativeGeocoder, navCtrl, postPvdr, geo) {
        this.router = router;
        this.storage = storage;
        this.toastController = toastController;
        this.nativeGeocoder = nativeGeocoder;
        this.navCtrl = navCtrl;
        this.postPvdr = postPvdr;
        this.geo = geo;
        this.showMe = false;
        this.city = " ";
        this.zip_code = " ";
        this.district_area = "";
        this.delivery_type_id = 1;
        this.delivery_type = "";
        this.hideDiv = false;
        this.showDiv = false;
        this.myCurrentLocation = '';
        this.login_user_id = '';
        this.disableButton = false;
    }
    ngOnInit() {
        this.showMe = true;
        this.showDiv = true;
    }
    show() {
        this.showMe = true;
    }
    hide() {
        this.showMe = false;
        this.disableButton = true;
        this.getCurrentLocation();
    }
    Divshow() {
        this.hideDiv = false;
        this.showDiv = true;
        this.showMe = false;
        this.delivery_type_id = 1;
    }
    Divhide() {
        this.showDiv = false;
        this.hideDiv = true;
        this.showMe = false;
        this.delivery_type_id = 2;
    }
    getCurrentLocation() {
        this.storage.get("greenthumb_user_id").then((user_id) => {
            this.login_user_id = user_id;
            let body = {
                action: 'check_location',
                user_id: user_id
            };
            this.postPvdr.postData(body, 'user.php').subscribe((data) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                if (data.success) {
                    if (data.my_location != '') {
                        this.lat = data.latitude;
                        this.lng = data.longitude;
                        this.myCurrentLocation = data.my_location;
                        this.city = data.my_location;
                        this.disableButton = false;
                        //this.drawMap();
                    }
                    else {
                        this.getGeoLocation();
                    }
                }
            }));
        });
        //this.plotData();
    }
    getGeoLocation() {
        this.geo.getCurrentPosition({
            timeout: 10000,
            enableHighAccuracy: true
        }).then((res) => {
            this.lat = res.coords.latitude;
            this.lng = res.coords.longitude;
            let options = {
                useLocale: true,
                maxResults: 5
            };
            this.nativeGeocoder.reverseGeocode(this.lat, this.lng, options)
                .then((result) => {
                console.log(JSON.stringify(result[0]));
                // console.log("re:"+result[0].locality);
                // console.log("are:"+result[0].subAdministrativeArea);
                // console.log("th:"+result[0].subThoroughfare);
                this.myCurrentLocation = result[0].thoroughfare + ' '
                    + result[0].locality + ', ' + result[0].subAdministrativeArea + ' '
                    + result[0].countryName;
                this.city = result[0].thoroughfare + ' '
                    + result[0].locality;
                this.zip_code = "6500";
                this.disableButton = false;
            })
                .catch((error) => {
                console.log(error);
                this.getGeoLocation();
            });
            //this.drawMap();
        }).catch((er) => {
            this.getGeoLocation();
        });
    }
    goHelp() {
        //window.location.href="http://greenthumbtrade.com/help";
        this.navCtrl.navigateForward((['tabs/help']), { animated: false, });
    }
    goBack() {
        window.history.back();
        //this.navCtrl.navigateForward((['setprice']), { animated: false, });
    }
    goShareitem() {
        if (this.zip_code == "") {
            this.presentToast("Please Input Zip Code");
        }
        else if (this.city == "") {
            this.presentToast("Please Input Location");
        }
        else {
            if (this.delivery_type_id == 1) {
                this.delivery_type = "Local Pick-up";
            }
            else if (this.delivery_type_id == 2) {
                this.delivery_type = "Shipping";
            }
            let data = {
                location: this.city,
                zip_code: this.zip_code,
                district_area: this.district_area,
                delivery_type_id: this.delivery_type_id,
                delivery_type: this.delivery_type
            };
            this.storage.set("greenthumb_location", data);
            this.navCtrl.navigateForward((['shareitem']), { animated: false, });
        }
    }
    presentToast(x) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: x,
                duration: 3000
            });
            toast.present();
        });
    }
};
SelectlocationPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"] },
    { type: _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_7__["NativeGeocoder"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"] },
    { type: _providers_credential_provider__WEBPACK_IMPORTED_MODULE_6__["PostProvider"] },
    { type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_5__["Geolocation"] }
];
SelectlocationPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-selectlocation',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./selectlocation.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/selectlocation/selectlocation.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./selectlocation.page.scss */ "./src/app/selectlocation/selectlocation.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
        _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"],
        _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_7__["NativeGeocoder"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"],
        _providers_credential_provider__WEBPACK_IMPORTED_MODULE_6__["PostProvider"],
        _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_5__["Geolocation"]])
], SelectlocationPage);



/***/ })

}]);
//# sourceMappingURL=selectlocation-selectlocation-module-es2015.js.map