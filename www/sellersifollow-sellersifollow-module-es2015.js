(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["sellersifollow-sellersifollow-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/sellersifollow/sellersifollow.page.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/sellersifollow/sellersifollow.page.html ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header mode=\"ios\" no-border>\n  <ion-toolbar mode=\"ios\">\n      <ion-buttons slot=\"secondary\" (click)=\"goBack()\">\n        <ion-button mode=\"ios\" style=\"font-size: 15px;\">\n            Back\n        </ion-button>\n      </ion-buttons>\n\n      <ion-title color=\"secondary\">Seller Interaction</ion-title>\n\n      <ion-buttons slot=\"primary\" (click)=\"goHelp()\">\n        <ion-button mode=\"ios\" style=\"font-size: 15px;\">\n            Help\n        </ion-button>\n      </ion-buttons>\n    </ion-toolbar>\n</ion-header>\n<ion-segment style=\"padding: 5px\" color=\"primary\" mode=\"ios\" (ionChange)=\"segmentChanged()\" [(ngModel)]=\"segment\">\n  <ion-segment-button class=\"for_segment\" mode=\"ios\" value=\"0\">\n      <ion-label>Followers \n        <ion-badge class=\"for_segmentbadge\" mode=\"ios\" color=\"tertiary\">\n          {{totalFollower | number:'1.0':'en-US'}} \n      </ion-badge></ion-label>\n  </ion-segment-button>  \n  <ion-segment-button class=\"for_segment\" mode=\"ios\" value=\"1\">\n      <ion-label>Following \n        <ion-badge class=\"for_segmentbadge\" mode=\"ios\" color=\"tertiary\">\n          {{totalFollowing | number:'1.0':'en-US'}} \n      </ion-badge></ion-label>\n  </ion-segment-button>\n</ion-segment>\n<ion-content>\n  <!-- <ion-searchbar style=\"padding-right: 12px;margin-top: 3%;\" placeholder=\"Search...\" id=\"customsearchbar\" mode=\"ios\"></ion-searchbar> -->\n  <ion-slides #slides (ionSlideDidChange)=\"slideChanged()\" mode=\"ios\">\n    <!-- For followers -->\n        <ion-slide>\n          <ion-grid>\n            <ion-row style=\"padding: 5px;\">\n              <ion-col>\n                <ion-list mode=\"ios\">\n                  <ion-item *ngFor=\"let follower of myFollowers\"  mode=\"ios\" class=\"for_eachitem\" lines=\"none\" (click)=\"goSellerprofile(follower.id)\">\n                    <ion-avatar slot=\"start\" mode=\"ios\" id=\"for_avatar\">\n                      <div *ngIf=\"!follower?.profile_photo\">\n                        <img src=\"assets/greenthumb-images/userpic.png\">\n                      </div>\n                      <div *ngIf=\"follower?.profile_photo\">\n                        <img [src] = \"follower.profile_photo\" >\n                      </div> \n                    </ion-avatar>\n                    <ion-label mode=\"ios\" color=\"secondary\">\n                      <h2 class=\"for_username\" mode=\"ios\">{{follower.username}}</h2>\n                      <!-- <h3 class=\"for_name\" mode=\"ios\">{{follower.fname}} {{follower.lname}}</h3> -->\n                    </ion-label>\n                    <ion-button mode=\"ios\" size=\"small\" expand=\"block\" class=\"btn_follow\">Following</ion-button>\n                  </ion-item>\n                </ion-list>\n              </ion-col>\n            </ion-row>\n          </ion-grid>\n        </ion-slide>\n<!-- For following -->\n<ion-slide>\n  <ion-grid>\n    <ion-row style=\"padding: 5px;\">\n      <ion-col>\n        <ion-list mode=\"ios\">\n          <ion-item *ngFor=\"let ifollow of iFollowed\" mode=\"ios\" class=\"for_eachitem\" lines=\"none\" (click)=\"goSellerprofile(ifollow.id)\">\n            <ion-avatar slot=\"start\" mode=\"ios\" id=\"for_avatar\">\n              <div *ngIf=\"!ifollow?.profile_photo\">\n                <img src=\"assets/greenthumb-images/userpic.png\">\n              </div>\n              <div *ngIf=\"ifollow?.profile_photo\">\n                <img [src] = \"ifollow.profile_photo\" >\n              </div> \n            </ion-avatar>\n            <ion-label mode=\"ios\" color=\"secondary\">\n              <h2 class=\"for_username\" mode=\"ios\">{{ifollow.username}}</h2>\n              <!-- <h3 class=\"for_name\" mode=\"ios\">{{ifollow.fname}} {{ifollow.lname}}</h3> -->\n            </ion-label>\n            <ion-button mode=\"ios\" size=\"small\" slot=\"end\" fill=\"outline\" class=\"btn_follow\">Following</ion-button>\n          </ion-item>\n        </ion-list>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-slide>\n  </ion-slides>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/sellersifollow/sellersifollow-routing.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/sellersifollow/sellersifollow-routing.module.ts ***!
  \*****************************************************************/
/*! exports provided: SellersifollowPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SellersifollowPageRoutingModule", function() { return SellersifollowPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _sellersifollow_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./sellersifollow.page */ "./src/app/sellersifollow/sellersifollow.page.ts");




const routes = [
    {
        path: '',
        component: _sellersifollow_page__WEBPACK_IMPORTED_MODULE_3__["SellersifollowPage"]
    }
];
let SellersifollowPageRoutingModule = class SellersifollowPageRoutingModule {
};
SellersifollowPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], SellersifollowPageRoutingModule);



/***/ }),

/***/ "./src/app/sellersifollow/sellersifollow.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/sellersifollow/sellersifollow.module.ts ***!
  \*********************************************************/
/*! exports provided: SellersifollowPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SellersifollowPageModule", function() { return SellersifollowPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _sellersifollow_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./sellersifollow-routing.module */ "./src/app/sellersifollow/sellersifollow-routing.module.ts");
/* harmony import */ var _sellersifollow_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./sellersifollow.page */ "./src/app/sellersifollow/sellersifollow.page.ts");







let SellersifollowPageModule = class SellersifollowPageModule {
};
SellersifollowPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _sellersifollow_routing_module__WEBPACK_IMPORTED_MODULE_5__["SellersifollowPageRoutingModule"]
        ],
        declarations: [_sellersifollow_page__WEBPACK_IMPORTED_MODULE_6__["SellersifollowPage"]]
    })
], SellersifollowPageModule);



/***/ }),

/***/ "./src/app/sellersifollow/sellersifollow.page.scss":
/*!*********************************************************!*\
  !*** ./src/app/sellersifollow/sellersifollow.page.scss ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".btn_follow {\n  height: 25px;\n  width: 125px;\n}\n\n.for_eachitem {\n  --padding-start: 0;\n  --inner-padding-end: 0;\n}\n\n.for_username {\n  font-size: 13px;\n  color: black;\n}\n\n.for_name {\n  font-size: 12px;\n}\n\n.for_pin {\n  margin-left: -1%;\n  font-size: 13px;\n  margin-bottom: -1%;\n}\n\n.for_segment {\n  padding: 3px;\n  padding-bottom: 5px;\n  padding-top: 5px;\n  padding-right: 0;\n}\n\n.for_segmentbadge {\n  color: #e44545;\n  font-size: 11px;\n  padding-left: 5px;\n  padding-right: 5px;\n  display: inline;\n  padding-top: 1px;\n  padding-bottom: 1px;\n  margin-left: 5px;\n}\n\nion-avatar {\n  width: 100% !important;\n  height: 100% !important;\n  max-width: 45px !important;\n  max-height: 45px !important;\n}\n\nion-item {\n  --padding-start: 0% !important;\n  --inner-padding-end: 0;\n  --padding-bottom: 10px;\n}\n\n#for_avatar {\n  width: 100% !important;\n  height: 100% !important;\n  max-width: 45px !important;\n  max-height: 45px !important;\n}\n\n#for_avatar img {\n  border: 1px solid rgba(0, 0, 0, 0.12);\n  width: 100% !important;\n  height: 100% !important;\n  max-width: 45px !important;\n  max-height: 45px !important;\n  border-radius: 50%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZW5kcmljay9EZXNrdG9wL1RlbG1vIFNvbHV0aW9ucy9Gb3JQcm9kdWN0aW9uL21heTEwMjAyMS9sYXN0IHNldHVwL2dyZWVudGh1bWJhcHAgY29weS9zcmMvYXBwL3NlbGxlcnNpZm9sbG93L3NlbGxlcnNpZm9sbG93LnBhZ2Uuc2NzcyIsInNyYy9hcHAvc2VsbGVyc2lmb2xsb3cvc2VsbGVyc2lmb2xsb3cucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksWUFBQTtFQUNBLFlBQUE7QUNDSjs7QURDQTtFQUNJLGtCQUFBO0VBQ0Esc0JBQUE7QUNFSjs7QURBQTtFQUNJLGVBQUE7RUFDQSxZQUFBO0FDR0o7O0FEREE7RUFDSSxlQUFBO0FDSUo7O0FERkE7RUFDSSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtBQ0tKOztBREhBO0VBQ0ksWUFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtBQ01KOztBREpBO0VBQ0ksY0FBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtBQ09KOztBRExBO0VBQ0ksc0JBQUE7RUFDQSx1QkFBQTtFQUNBLDBCQUFBO0VBQ0EsMkJBQUE7QUNRSjs7QUROQTtFQUNJLDhCQUFBO0VBQ0Esc0JBQUE7RUFDQSxzQkFBQTtBQ1NKOztBRFBBO0VBQ0ksc0JBQUE7RUFDQSx1QkFBQTtFQUNBLDBCQUFBO0VBQ0EsMkJBQUE7QUNVSjs7QURUUTtFQUNJLHFDQUFBO0VBQ0Esc0JBQUE7RUFDQSx1QkFBQTtFQUNBLDBCQUFBO0VBQ0EsMkJBQUE7RUFDQSxrQkFBQTtBQ1daIiwiZmlsZSI6InNyYy9hcHAvc2VsbGVyc2lmb2xsb3cvc2VsbGVyc2lmb2xsb3cucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmJ0bl9mb2xsb3d7XG4gICAgaGVpZ2h0OiAyNXB4O1xuICAgIHdpZHRoOiAxMjVweDtcbn1cbi5mb3JfZWFjaGl0ZW17XG4gICAgLS1wYWRkaW5nLXN0YXJ0OiAwO1xuICAgIC0taW5uZXItcGFkZGluZy1lbmQ6IDA7XG59XG4uZm9yX3VzZXJuYW1le1xuICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgICBjb2xvcjpibGFjaztcbn1cbi5mb3JfbmFtZXtcbiAgICBmb250LXNpemU6IDEycHg7XG59XG4uZm9yX3BpbntcbiAgICBtYXJnaW4tbGVmdDogLTElO1xuICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgICBtYXJnaW4tYm90dG9tOiAtMSU7XG59XG4uZm9yX3NlZ21lbnR7XG4gICAgcGFkZGluZzogM3B4O1xuICAgIHBhZGRpbmctYm90dG9tOiA1cHg7XG4gICAgcGFkZGluZy10b3A6IDVweDtcbiAgICBwYWRkaW5nLXJpZ2h0OiAwO1xufVxuLmZvcl9zZWdtZW50YmFkZ2V7XG4gICAgY29sb3I6ICNlNDQ1NDU7XG4gICAgZm9udC1zaXplOiAxMXB4O1xuICAgIHBhZGRpbmctbGVmdDogNXB4O1xuICAgIHBhZGRpbmctcmlnaHQ6IDVweDtcbiAgICBkaXNwbGF5OiBpbmxpbmU7XG4gICAgcGFkZGluZy10b3A6IDFweDtcbiAgICBwYWRkaW5nLWJvdHRvbTogMXB4O1xuICAgIG1hcmdpbi1sZWZ0OiA1cHg7XG59XG5pb24tYXZhdGFye1xuICAgIHdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XG4gICAgaGVpZ2h0OiAxMDAlICFpbXBvcnRhbnQ7XG4gICAgbWF4LXdpZHRoOiA0NXB4ICFpbXBvcnRhbnQ7XG4gICAgbWF4LWhlaWdodDogNDVweCAhaW1wb3J0YW50O1xufVxuaW9uLWl0ZW0ge1xuICAgIC0tcGFkZGluZy1zdGFydDogMCUgIWltcG9ydGFudDtcbiAgICAtLWlubmVyLXBhZGRpbmctZW5kOiAwO1xuICAgIC0tcGFkZGluZy1ib3R0b206IDEwcHg7XG59XG4jZm9yX2F2YXRhcntcbiAgICB3aWR0aDogMTAwJSAhaW1wb3J0YW50O1xuICAgIGhlaWdodDogMTAwJSAhaW1wb3J0YW50O1xuICAgIG1heC13aWR0aDogNDVweCAhaW1wb3J0YW50O1xuICAgIG1heC1oZWlnaHQ6IDQ1cHggIWltcG9ydGFudDtcbiAgICAgICAgaW1ne1xuICAgICAgICAgICAgYm9yZGVyOiAxcHggc29saWQgcmdiYSgwLCAwLCAwLCAwLjEyKTtcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XG4gICAgICAgICAgICBoZWlnaHQ6IDEwMCUgIWltcG9ydGFudDtcbiAgICAgICAgICAgIG1heC13aWR0aDogNDVweCAhaW1wb3J0YW50O1xuICAgICAgICAgICAgbWF4LWhlaWdodDogNDVweCAhaW1wb3J0YW50O1xuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAgICAgICB9XG59IiwiLmJ0bl9mb2xsb3cge1xuICBoZWlnaHQ6IDI1cHg7XG4gIHdpZHRoOiAxMjVweDtcbn1cblxuLmZvcl9lYWNoaXRlbSB7XG4gIC0tcGFkZGluZy1zdGFydDogMDtcbiAgLS1pbm5lci1wYWRkaW5nLWVuZDogMDtcbn1cblxuLmZvcl91c2VybmFtZSB7XG4gIGZvbnQtc2l6ZTogMTNweDtcbiAgY29sb3I6IGJsYWNrO1xufVxuXG4uZm9yX25hbWUge1xuICBmb250LXNpemU6IDEycHg7XG59XG5cbi5mb3JfcGluIHtcbiAgbWFyZ2luLWxlZnQ6IC0xJTtcbiAgZm9udC1zaXplOiAxM3B4O1xuICBtYXJnaW4tYm90dG9tOiAtMSU7XG59XG5cbi5mb3Jfc2VnbWVudCB7XG4gIHBhZGRpbmc6IDNweDtcbiAgcGFkZGluZy1ib3R0b206IDVweDtcbiAgcGFkZGluZy10b3A6IDVweDtcbiAgcGFkZGluZy1yaWdodDogMDtcbn1cblxuLmZvcl9zZWdtZW50YmFkZ2Uge1xuICBjb2xvcjogI2U0NDU0NTtcbiAgZm9udC1zaXplOiAxMXB4O1xuICBwYWRkaW5nLWxlZnQ6IDVweDtcbiAgcGFkZGluZy1yaWdodDogNXB4O1xuICBkaXNwbGF5OiBpbmxpbmU7XG4gIHBhZGRpbmctdG9wOiAxcHg7XG4gIHBhZGRpbmctYm90dG9tOiAxcHg7XG4gIG1hcmdpbi1sZWZ0OiA1cHg7XG59XG5cbmlvbi1hdmF0YXIge1xuICB3aWR0aDogMTAwJSAhaW1wb3J0YW50O1xuICBoZWlnaHQ6IDEwMCUgIWltcG9ydGFudDtcbiAgbWF4LXdpZHRoOiA0NXB4ICFpbXBvcnRhbnQ7XG4gIG1heC1oZWlnaHQ6IDQ1cHggIWltcG9ydGFudDtcbn1cblxuaW9uLWl0ZW0ge1xuICAtLXBhZGRpbmctc3RhcnQ6IDAlICFpbXBvcnRhbnQ7XG4gIC0taW5uZXItcGFkZGluZy1lbmQ6IDA7XG4gIC0tcGFkZGluZy1ib3R0b206IDEwcHg7XG59XG5cbiNmb3JfYXZhdGFyIHtcbiAgd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcbiAgaGVpZ2h0OiAxMDAlICFpbXBvcnRhbnQ7XG4gIG1heC13aWR0aDogNDVweCAhaW1wb3J0YW50O1xuICBtYXgtaGVpZ2h0OiA0NXB4ICFpbXBvcnRhbnQ7XG59XG4jZm9yX2F2YXRhciBpbWcge1xuICBib3JkZXI6IDFweCBzb2xpZCByZ2JhKDAsIDAsIDAsIDAuMTIpO1xuICB3aWR0aDogMTAwJSAhaW1wb3J0YW50O1xuICBoZWlnaHQ6IDEwMCUgIWltcG9ydGFudDtcbiAgbWF4LXdpZHRoOiA0NXB4ICFpbXBvcnRhbnQ7XG4gIG1heC1oZWlnaHQ6IDQ1cHggIWltcG9ydGFudDtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/sellersifollow/sellersifollow.page.ts":
/*!*******************************************************!*\
  !*** ./src/app/sellersifollow/sellersifollow.page.ts ***!
  \*******************************************************/
/*! exports provided: SellersifollowPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SellersifollowPage", function() { return SellersifollowPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _providers_credential_provider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../providers/credential-provider */ "./src/providers/credential-provider.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _shared_model_follow_model__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../shared/model/follow.model */ "./src/app/shared/model/follow.model.ts");








let SellersifollowPage = class SellersifollowPage {
    constructor(storage, navCtrl, postPvdr, router) {
        this.storage = storage;
        this.navCtrl = navCtrl;
        this.postPvdr = postPvdr;
        this.router = router;
        this.segment = 0;
    }
    ngOnInit() {
    }
    goHelp() {
        //window.location.href="http://greenthumbtrade.com/help";
        this.router.navigate(['tabs/help']);
    }
    ionViewWillEnter() {
        this.plotData();
    }
    plotData() {
        console.log("folo");
        this.storage.get('greenthumb_user_id').then((user_id) => {
            // for followers
            let body = {
                action: 'showFollow',
                type: 1,
                user_id: user_id
            };
            this.postPvdr.postData(body, 'followers.php').subscribe(data => {
                // console.log(data);
                if (data.success) {
                    const followers = [];
                    var picture = '';
                    for (const key in data.result) {
                        // picture = (data.result[key].profile_photo == '') ? '' :
                        // this.postPvdr.myServer()+"/brixy-live/images/"+data.result[key].profile_photo;
                        followers.push(new _shared_model_follow_model__WEBPACK_IMPORTED_MODULE_6__["Follow"](data.result[key].user_id, data.result[key].fname, data.result[key].lname, data.result[key].city, data.result[key].country, data.result[key].username, (data.result[key].profile_photo == '') ? '' :
                            this.postPvdr.myServer() + "/greenthumb/images/" + data.result[key].profile_photo));
                    }
                    this.myFollowers = followers;
                }
            });
            let body2 = {
                action: 'getFollow',
                type: 1,
                user_id: user_id
            };
            this.postPvdr.postData(body2, 'followers.php').subscribe(data => {
                if (data.success) {
                    this.totalFollower = data.result;
                }
            });
            // for following
            let body3 = {
                action: 'showFollow',
                type: 2,
                user_id: user_id
            };
            this.postPvdr.postData(body3, 'followers.php').subscribe(data => {
                if (data.success) {
                    const followers1 = [];
                    var picture = '';
                    for (const key in data.result) {
                        // picture = (data.result[key].profile_photo == '') ? '' :
                        // this.postPvdr.myServer()+"/brixy-live/images/"+data.result[key].profile_photo;
                        followers1.push(new _shared_model_follow_model__WEBPACK_IMPORTED_MODULE_6__["Follow"](data.result[key].user_id, data.result[key].fname, data.result[key].lname, data.result[key].city, data.result[key].country, data.result[key].username, (data.result[key].profile_photo == '') ? '' :
                            this.postPvdr.myServer() + "/greenthumb/images/" + data.result[key].profile_photo));
                        //console.log(data.result[key].user_id);
                    }
                    this.iFollowed = followers1;
                }
            });
            let body4 = {
                action: 'getFollow',
                type: 2,
                user_id: user_id
            };
            this.postPvdr.postData(body4, 'followers.php').subscribe(data => {
                if (data.success) {
                    this.totalFollowing = data.result;
                }
            });
        });
    }
    goBack() {
        window.history.back();
    }
    goSellerprofile(user_profile_id) {
        // this.router.navigate((['viewitemsellerprofile']));
        let navigationExtras = {
            queryParams: { user_profile_id }
        };
        this.navCtrl.navigateForward(['viewitemsellerprofile'], navigationExtras);
    }
    segmentChanged() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            yield this.slider.slideTo(this.segment);
        });
    }
    slideChanged() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.segment = yield this.slider.getActiveIndex();
            this.storage.set("segment", this.segment);
        });
    }
};
SellersifollowPage.ctorParameters = () => [
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"] },
    { type: _providers_credential_provider__WEBPACK_IMPORTED_MODULE_4__["PostProvider"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('slides', { static: true }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonSlides"])
], SellersifollowPage.prototype, "slider", void 0);
SellersifollowPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-sellersifollow',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./sellersifollow.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/sellersifollow/sellersifollow.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./sellersifollow.page.scss */ "./src/app/sellersifollow/sellersifollow.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"],
        _providers_credential_provider__WEBPACK_IMPORTED_MODULE_4__["PostProvider"],
        _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
], SellersifollowPage);



/***/ })

}]);
//# sourceMappingURL=sellersifollow-sellersifollow-module-es2015.js.map