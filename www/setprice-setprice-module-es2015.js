(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["setprice-setprice-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/setprice/setprice.page.html":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/setprice/setprice.page.html ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header mode=\"ios\" no-border>\n  <ion-toolbar mode=\"ios\">\n      <ion-buttons slot=\"secondary\" (click)=\"goBack()\">\n        <ion-button mode=\"ios\" style=\"font-size: 15px;\">\n            Back\n        </ion-button>\n      </ion-buttons>\n\n      <ion-title color=\"secondary\">Set your price</ion-title>\n\n      <ion-buttons slot=\"primary\" (click)=\"goHelp()\">\n        <ion-button mode=\"ios\" style=\"font-size: 15px;\">\n            Help\n        </ion-button>\n      </ion-buttons>\n    </ion-toolbar>\n</ion-header>\n<ion-content padding class=\"for_post\">\n  <ion-row justify-content-center align-items-center style='height: 100%'>\n    <ion-col>\n      <ion-list class=\"line-input\">\n        <ion-item>\n          <ion-input mode=\"ios\" [(ngModel)]=\"price\" type=\"number\" class=\"item_input\" placeholder=\"$0\"></ion-input>\n         </ion-item>\n      </ion-list>\n    </ion-col>\n  </ion-row>\n</ion-content>\n<div class=\"postitem-overlay\" padding>\n  <ion-button expand=\"block\" (click)=\"goSelectlocation()\" mode=\"ios\" fill=\"outline\" color=\"primary\">\n    NEXT\n  </ion-button>\n  <div class=\"postitem_container\">\n    <ul class=\"postitem_progressbar\">\n      <li class=\"whenfocus active\"><span>Photo</span></li>\n      <li class=\"whenfocus active\"><span>Details</span></li>\n      <li class=\"whenfocus\"><span>Price</span></li>\n      <li><span>Delivery</span></li>\n      <li><span>Post</span></li>\n    </ul>\n  </div>\n</div>");

/***/ }),

/***/ "./src/app/setprice/setprice-routing.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/setprice/setprice-routing.module.ts ***!
  \*****************************************************/
/*! exports provided: SetpricePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SetpricePageRoutingModule", function() { return SetpricePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _setprice_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./setprice.page */ "./src/app/setprice/setprice.page.ts");




const routes = [
    {
        path: '',
        component: _setprice_page__WEBPACK_IMPORTED_MODULE_3__["SetpricePage"]
    }
];
let SetpricePageRoutingModule = class SetpricePageRoutingModule {
};
SetpricePageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], SetpricePageRoutingModule);



/***/ }),

/***/ "./src/app/setprice/setprice.module.ts":
/*!*********************************************!*\
  !*** ./src/app/setprice/setprice.module.ts ***!
  \*********************************************/
/*! exports provided: SetpricePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SetpricePageModule", function() { return SetpricePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _setprice_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./setprice-routing.module */ "./src/app/setprice/setprice-routing.module.ts");
/* harmony import */ var _setprice_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./setprice.page */ "./src/app/setprice/setprice.page.ts");







let SetpricePageModule = class SetpricePageModule {
};
SetpricePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _setprice_routing_module__WEBPACK_IMPORTED_MODULE_5__["SetpricePageRoutingModule"]
        ],
        declarations: [_setprice_page__WEBPACK_IMPORTED_MODULE_6__["SetpricePage"]]
    })
], SetpricePageModule);



/***/ }),

/***/ "./src/app/setprice/setprice.page.scss":
/*!*********************************************!*\
  !*** ./src/app/setprice/setprice.page.scss ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".for_post {\n  --background: none;\n  background-image: url(\"/assets/greenthumb-images/bunny1.png\");\n  background-repeat: no-repeat;\n  background-size: contain;\n}\n\n.for_post:before {\n  content: \"\";\n  position: absolute;\n  left: 0;\n  right: 0;\n  top: 0;\n  bottom: 5%;\n  background: rgba(255, 255, 255, 0.9);\n}\n\n.line-input {\n  margin-bottom: 0 !important;\n  background: transparent;\n}\n\n.line-input ion-item {\n  --border-color: transparent!important;\n  --highlight-height: 0;\n  border: 1px solid #e2e2e2;\n  border-radius: 4px;\n  height: 59px;\n  margin: auto;\n  --background: white;\n  width: 160px;\n  text-align: center !important;\n}\n\n.item_input {\n  font-size: 32px;\n  color: #679733 !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZW5kcmljay9EZXNrdG9wL1RlbG1vIFNvbHV0aW9ucy9Gb3JQcm9kdWN0aW9uL21heTEwMjAyMS9sYXN0IHNldHVwL2dyZWVudGh1bWJhcHAgY29weS9zcmMvYXBwL3NldHByaWNlL3NldHByaWNlLnBhZ2Uuc2NzcyIsInNyYy9hcHAvc2V0cHJpY2Uvc2V0cHJpY2UucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksa0JBQUE7RUFDQSw2REFBQTtFQUNBLDRCQUFBO0VBQ0Esd0JBQUE7QUNDSjs7QURDQTtFQUNJLFdBQUE7RUFDQSxrQkFBQTtFQUNBLE9BQUE7RUFBUyxRQUFBO0VBQ1QsTUFBQTtFQUFRLFVBQUE7RUFDUixvQ0FBQTtBQ0lKOztBREZBO0VBQ0ksMkJBQUE7RUFDQSx1QkFBQTtBQ0tKOztBREpJO0VBQ0kscUNBQUE7RUFDQSxxQkFBQTtFQUNBLHlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLDZCQUFBO0FDTVI7O0FESEE7RUFDSSxlQUFBO0VBQ0EseUJBQUE7QUNNSiIsImZpbGUiOiJzcmMvYXBwL3NldHByaWNlL3NldHByaWNlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5mb3JfcG9zdHtcbiAgICAtLWJhY2tncm91bmQ6IG5vbmU7XG4gICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKCcvYXNzZXRzL2dyZWVudGh1bWItaW1hZ2VzL2J1bm55MS5wbmcnKTtcbiAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcbn1cbi5mb3JfcG9zdDpiZWZvcmUge1xuICAgIGNvbnRlbnQ6IFwiXCI7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGxlZnQ6IDA7IHJpZ2h0OiAwO1xuICAgIHRvcDogMDsgYm90dG9tOiA1JTtcbiAgICBiYWNrZ3JvdW5kOiByZ2JhKDI1NSwyNTUsMjU1LDkwJSk7XG4gIH1cbi5saW5lLWlucHV0IHtcbiAgICBtYXJnaW4tYm90dG9tOiAwIWltcG9ydGFudDtcbiAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgICBpb24taXRlbSB7XG4gICAgICAgIC0tYm9yZGVyLWNvbG9yOiB0cmFuc3BhcmVudCFpbXBvcnRhbnQ7XG4gICAgICAgIC0taGlnaGxpZ2h0LWhlaWdodDogMDtcbiAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2UyZTJlMjtcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNHB4O1xuICAgICAgICBoZWlnaHQ6IDU5cHg7XG4gICAgICAgIG1hcmdpbjogYXV0bztcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgICAgICAgd2lkdGg6IDE2MHB4O1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXIgIWltcG9ydGFudDtcbiAgICB9XG4gIH1cbi5pdGVtX2lucHV0e1xuICAgIGZvbnQtc2l6ZTogMzJweDtcbiAgICBjb2xvcjogIzY3OTczMyAhaW1wb3J0YW50O1xufVxuXG5cblxuIiwiLmZvcl9wb3N0IHtcbiAgLS1iYWNrZ3JvdW5kOiBub25lO1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCIvYXNzZXRzL2dyZWVudGh1bWItaW1hZ2VzL2J1bm55MS5wbmdcIik7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcbn1cblxuLmZvcl9wb3N0OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXCI7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogMDtcbiAgcmlnaHQ6IDA7XG4gIHRvcDogMDtcbiAgYm90dG9tOiA1JTtcbiAgYmFja2dyb3VuZDogcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjkpO1xufVxuXG4ubGluZS1pbnB1dCB7XG4gIG1hcmdpbi1ib3R0b206IDAgIWltcG9ydGFudDtcbiAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG59XG4ubGluZS1pbnB1dCBpb24taXRlbSB7XG4gIC0tYm9yZGVyLWNvbG9yOiB0cmFuc3BhcmVudCFpbXBvcnRhbnQ7XG4gIC0taGlnaGxpZ2h0LWhlaWdodDogMDtcbiAgYm9yZGVyOiAxcHggc29saWQgI2UyZTJlMjtcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xuICBoZWlnaHQ6IDU5cHg7XG4gIG1hcmdpbjogYXV0bztcbiAgLS1iYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgd2lkdGg6IDE2MHB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXIgIWltcG9ydGFudDtcbn1cblxuLml0ZW1faW5wdXQge1xuICBmb250LXNpemU6IDMycHg7XG4gIGNvbG9yOiAjNjc5NzMzICFpbXBvcnRhbnQ7XG59Il19 */");

/***/ }),

/***/ "./src/app/setprice/setprice.page.ts":
/*!*******************************************!*\
  !*** ./src/app/setprice/setprice.page.ts ***!
  \*******************************************/
/*! exports provided: SetpricePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SetpricePage", function() { return SetpricePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");





let SetpricePage = class SetpricePage {
    constructor(router, navCtrl, toastController, storage) {
        this.router = router;
        this.navCtrl = navCtrl;
        this.toastController = toastController;
        this.storage = storage;
    }
    ngOnInit() {
    }
    goHelp() {
        //window.location.href="http://greenthumbtrade.com/help";
        this.navCtrl.navigateForward((['tabs/help']), { animated: false, });
    }
    goBack() {
        window.history.back();
        //this.navCtrl.navigateForward((['describeitem']), { animated: false, });
    }
    goSelectlocation() {
        if (this.price == null || this.price == '') {
            this.presentToast("Please Input Price of the Item");
        }
        else {
            this.storage.set("greenthumb_price", this.price);
            this.navCtrl.navigateForward((['selectlocation']), { animated: false, });
        }
    }
    presentToast(x) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: x,
                duration: 3000
            });
            toast.present();
        });
    }
};
SetpricePage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"] }
];
SetpricePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-setprice',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./setprice.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/setprice/setprice.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./setprice.page.scss */ "./src/app/setprice/setprice.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"],
        _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"]])
], SetpricePage);



/***/ })

}]);
//# sourceMappingURL=setprice-setprice-module-es2015.js.map