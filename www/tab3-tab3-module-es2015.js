(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["tab3-tab3-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/tab3/tab3.page.html":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/tab3/tab3.page.html ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header mode=\"ios\" no-border>\n  <ion-toolbar mode=\"ios\">\n      <ion-buttons slot=\"secondary\" (click)=\"goTabs()\">\n        <ion-button mode=\"ios\" style=\"font-size: 15px;\">\n            Cancel\n        </ion-button>\n      </ion-buttons>\n\n      <ion-title color=\"secondary\">Post an Item</ion-title>\n\n      <ion-buttons slot=\"primary\" (click)=\"goHelp()\">\n        <ion-button mode=\"ios\" style=\"font-size: 15px;\">\n            Help\n        </ion-button>\n      </ion-buttons>\n    </ion-toolbar>\n</ion-header>\n<ion-content padding class=\"for_post\">\n\n  <ion-row justify-content-center align-items-center style='height: 100%'>\n    <ion-col no-padding size=\"12\">\n      <ion-row>\n        <ion-col text-center>\n          <ion-button mode=\"ios\" (click)=\"takePhoto()\" class=\"post_btnpic\">\n              <img src=\"assets/greenthumb-images/post_uploadpic.png\">\n          </ion-button>\n          <ion-label><h5 class=\"post_label\">Take a photo</h5></ion-label>\n        </ion-col>\n        <ion-col text-center>\n          <ion-button mode=\"ios\" class=\"post_btnpic\" (click)=\"getImage()\">\n              <img src=\"assets/greenthumb-images/post_uploadpics.png\">\n          </ion-button>\n          <ion-label><h5 class=\"post_label\" >Select a photo</h5></ion-label>\n        </ion-col>\n      </ion-row>\n      <br>\n      <ion-row>\n        <ion-grid>\n          <ion-row *ngFor=\"let photo of photos; let i = index\">\n            <ion-col *ngIf=\"i % 2 == 0\">\n              <ion-card class=\"block\" mode=\"ios\">\n                <ion-icon name=\"trash\" class=\"deleteIcon\" (click)=\"deletePhoto(i)\"></ion-icon>\n                <img [src]=\"photo\" *ngIf=\"photo\" />\n              </ion-card>\n             </ion-col>\n          </ion-row>\n        </ion-grid>\n        \n        <ion-grid>\n          <ion-row *ngFor=\"let photo of photos; let i = index\">\n            <ion-col *ngIf=\"i % 2 != 0\">\n              <ion-card class=\"block\" mode=\"ios\">\n                <ion-icon name=\"trash\" class=\"deleteIcon\" (click)=\"deletePhoto(i)\"></ion-icon>\n                <img [src]=\"photo\" *ngIf=\"photo\" />\n              </ion-card>\n             </ion-col>\n          </ion-row>\n        </ion-grid>\n        \n\n      </ion-row>\n      <ion-row>\n        <ion-col>\n          <br><br>\n          <ion-list class=\"line-input\">\n            <ion-row>\n              <ion-col>\n                <ion-label class=\"item_label\">Product Name</ion-label>\n                <ion-item>\n                  <ion-input mode=\"ios\" type=\"text\" [(ngModel)]=\"greenthumb_title\" class=\"item_input\" placeholder=\"Name, brand, model, etc.\"></ion-input>\n                </ion-item>\n              </ion-col>\n            </ion-row>\n          </ion-list>\n          <br>\n        </ion-col>\n      </ion-row>\n    </ion-col>\n  </ion-row>\n\n \n  \n</ion-content>\n\n<div class=\"postitem-overlay\" padding>\n  <ion-button expand=\"block\" (click)=\"goDescribe()\" mode=\"ios\" fill=\"outline\" color=\"primary\">\n    NEXT\n  </ion-button>\n  <div class=\"postitem_container\">\n    <ul class=\"postitem_progressbar\">\n      <li class=\"whenfocus\"><span>Photo</span></li>\n      <li><span>Details</span></li>\n      <li><span>Price</span></li>\n      <li><span>Delivery</span></li>\n      <li><span>Post</span></li>\n    </ul>\n  </div>\n</div>");

/***/ }),

/***/ "./src/app/tab3/tab3.module.ts":
/*!*************************************!*\
  !*** ./src/app/tab3/tab3.module.ts ***!
  \*************************************/
/*! exports provided: Tab3PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab3PageModule", function() { return Tab3PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _tab3_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./tab3.page */ "./src/app/tab3/tab3.page.ts");







const routes = [
    {
        path: '',
        component: _tab3_page__WEBPACK_IMPORTED_MODULE_6__["Tab3Page"]
    }
];
let Tab3PageModule = class Tab3PageModule {
};
Tab3PageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_tab3_page__WEBPACK_IMPORTED_MODULE_6__["Tab3Page"]]
    })
], Tab3PageModule);



/***/ }),

/***/ "./src/app/tab3/tab3.page.scss":
/*!*************************************!*\
  !*** ./src/app/tab3/tab3.page.scss ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".for_post {\n  --background: none;\n  background-image: url(\"/assets/greenthumb-images/bunny2.png\");\n  background-position: center center;\n  background-repeat: no-repeat;\n  background-size: contain;\n}\n\n.for_post:before {\n  content: \"\";\n  position: absolute;\n  left: 0;\n  right: 0;\n  top: 0;\n  bottom: 0;\n  background: rgba(255, 255, 255, 0.9);\n}\n\n.post_btnpic {\n  height: 82px;\n  --background: white;\n  border: 1px solid #e2e2e2;\n  border-radius: 10px;\n  width: 58%;\n}\n\n.post_label {\n  font-size: 14px;\n  padding-top: 2%;\n}\n\n.line-input {\n  margin-bottom: 0 !important;\n  background: transparent;\n}\n\n.line-input ion-item {\n  --border-color: transparent!important;\n  --highlight-height: 0;\n  border: 1px solid #e2e2e2;\n  border-radius: 4px;\n  height: 40px;\n  margin-top: 3%;\n  --background: #ffffffb5;\n}\n\n.item_input {\n  font-size: 14px;\n  --padding-top: 0;\n  color: #424242 !important;\n}\n\n.item_label {\n  color: black !important;\n  font-weight: bold;\n  font-size: 15px;\n}\n\n.block {\n  position: relative;\n  box-shadow: none;\n  margin: 0;\n}\n\n.block .deleteIcon {\n  position: absolute !important;\n  color: #ffffff !important;\n  right: 5%;\n  top: 3%;\n  font-size: 20px;\n}\n\n.block .deleteIcon:before {\n  font-size: 30px !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZW5kcmljay9EZXNrdG9wL1RlbG1vIFNvbHV0aW9ucy9Gb3JQcm9kdWN0aW9uL21heTEwMjAyMS9sYXN0IHNldHVwL2dyZWVudGh1bWJhcHAgY29weS9zcmMvYXBwL3RhYjMvdGFiMy5wYWdlLnNjc3MiLCJzcmMvYXBwL3RhYjMvdGFiMy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxrQkFBQTtFQUNBLDZEQUFBO0VBQ0Esa0NBQUE7RUFDQSw0QkFBQTtFQUNBLHdCQUFBO0FDQ0o7O0FEQ0E7RUFDSSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxPQUFBO0VBQVMsUUFBQTtFQUNULE1BQUE7RUFBUSxTQUFBO0VBQ1Isb0NBQUE7QUNJSjs7QURGQTtFQUNJLFlBQUE7RUFDQSxtQkFBQTtFQUNBLHlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxVQUFBO0FDS0o7O0FESEE7RUFDSSxlQUFBO0VBQ0EsZUFBQTtBQ01KOztBREpBO0VBQ0UsMkJBQUE7RUFDQSx1QkFBQTtBQ09GOztBRE5FO0VBQ0kscUNBQUE7RUFDQSxxQkFBQTtFQUNBLHlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsY0FBQTtFQUNBLHVCQUFBO0FDUU47O0FETEE7RUFDRSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSx5QkFBQTtBQ1FGOztBRE5BO0VBQ0UsdUJBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7QUNTRjs7QUROQTtFQUNFLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxTQUFBO0FDU0Y7O0FEUkU7RUFDRSw2QkFBQTtFQUNBLHlCQUFBO0VBQ0EsU0FBQTtFQUNBLE9BQUE7RUFDQSxlQUFBO0FDVUo7O0FEUkU7RUFDRSwwQkFBQTtBQ1VKIiwiZmlsZSI6InNyYy9hcHAvdGFiMy90YWIzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5mb3JfcG9zdHtcbiAgICAtLWJhY2tncm91bmQ6IG5vbmU7XG4gICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKCcvYXNzZXRzL2dyZWVudGh1bWItaW1hZ2VzL2J1bm55Mi5wbmcnKTtcbiAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXIgY2VudGVyO1xuICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gICAgYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xufVxuLmZvcl9wb3N0OmJlZm9yZSB7XG4gICAgY29udGVudDogXCJcIjtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgbGVmdDogMDsgcmlnaHQ6IDA7XG4gICAgdG9wOiAwOyBib3R0b206IDA7XG4gICAgYmFja2dyb3VuZDogcmdiYSgyNTUsMjU1LDI1NSw5MCUpO1xuICB9XG4ucG9zdF9idG5waWN7XG4gICAgaGVpZ2h0OiA4MnB4O1xuICAgIC0tYmFja2dyb3VuZDogd2hpdGU7XG4gICAgYm9yZGVyOiAxcHggc29saWQgI2UyZTJlMjtcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgIHdpZHRoOiA1OCU7XG59XG4ucG9zdF9sYWJlbHtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgcGFkZGluZy10b3A6IDIlO1xufVxuLmxpbmUtaW5wdXQge1xuICBtYXJnaW4tYm90dG9tOiAwIWltcG9ydGFudDtcbiAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG4gIGlvbi1pdGVtIHtcbiAgICAgIC0tYm9yZGVyLWNvbG9yOiB0cmFuc3BhcmVudCFpbXBvcnRhbnQ7XG4gICAgICAtLWhpZ2hsaWdodC1oZWlnaHQ6IDA7XG4gICAgICBib3JkZXI6IDFweCBzb2xpZCAjZTJlMmUyO1xuICAgICAgYm9yZGVyLXJhZGl1czogNHB4O1xuICAgICAgaGVpZ2h0OiA0MHB4O1xuICAgICAgbWFyZ2luLXRvcDogMyU7XG4gICAgICAtLWJhY2tncm91bmQ6ICNmZmZmZmZiNTtcbiAgfVxufVxuLml0ZW1faW5wdXR7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgLS1wYWRkaW5nLXRvcDogMDtcbiAgY29sb3I6ICM0MjQyNDIhaW1wb3J0YW50O1xufVxuLml0ZW1fbGFiZWx7XG4gIGNvbG9yOiBibGFjayAhaW1wb3J0YW50O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgZm9udC1zaXplOiAxNXB4O1xufVxuXG4uYmxvY2sge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGJveC1zaGFkb3c6IG5vbmU7XG4gIG1hcmdpbjogMDtcbiAgLmRlbGV0ZUljb24ge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZSAhaW1wb3J0YW50O1xuICAgIGNvbG9yOiAjZmZmZmZmICFpbXBvcnRhbnQ7XG4gICAgcmlnaHQ6IDUlO1xuICAgIHRvcDogMyU7XG4gICAgZm9udC1zaXplOiAyMHB4O1xuICB9XG4gIC5kZWxldGVJY29uOmJlZm9yZSB7XG4gICAgZm9udC1zaXplOiAzMHB4ICFpbXBvcnRhbnQ7XG4gIH1cbiAgfVxuIiwiLmZvcl9wb3N0IHtcbiAgLS1iYWNrZ3JvdW5kOiBub25lO1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCIvYXNzZXRzL2dyZWVudGh1bWItaW1hZ2VzL2J1bm55Mi5wbmdcIik7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlciBjZW50ZXI7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcbn1cblxuLmZvcl9wb3N0OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXCI7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogMDtcbiAgcmlnaHQ6IDA7XG4gIHRvcDogMDtcbiAgYm90dG9tOiAwO1xuICBiYWNrZ3JvdW5kOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuOSk7XG59XG5cbi5wb3N0X2J0bnBpYyB7XG4gIGhlaWdodDogODJweDtcbiAgLS1iYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgYm9yZGVyOiAxcHggc29saWQgI2UyZTJlMjtcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgd2lkdGg6IDU4JTtcbn1cblxuLnBvc3RfbGFiZWwge1xuICBmb250LXNpemU6IDE0cHg7XG4gIHBhZGRpbmctdG9wOiAyJTtcbn1cblxuLmxpbmUtaW5wdXQge1xuICBtYXJnaW4tYm90dG9tOiAwICFpbXBvcnRhbnQ7XG4gIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xufVxuLmxpbmUtaW5wdXQgaW9uLWl0ZW0ge1xuICAtLWJvcmRlci1jb2xvcjogdHJhbnNwYXJlbnQhaW1wb3J0YW50O1xuICAtLWhpZ2hsaWdodC1oZWlnaHQ6IDA7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNlMmUyZTI7XG4gIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgaGVpZ2h0OiA0MHB4O1xuICBtYXJnaW4tdG9wOiAzJTtcbiAgLS1iYWNrZ3JvdW5kOiAjZmZmZmZmYjU7XG59XG5cbi5pdGVtX2lucHV0IHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICAtLXBhZGRpbmctdG9wOiAwO1xuICBjb2xvcjogIzQyNDI0MiAhaW1wb3J0YW50O1xufVxuXG4uaXRlbV9sYWJlbCB7XG4gIGNvbG9yOiBibGFjayAhaW1wb3J0YW50O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgZm9udC1zaXplOiAxNXB4O1xufVxuXG4uYmxvY2sge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGJveC1zaGFkb3c6IG5vbmU7XG4gIG1hcmdpbjogMDtcbn1cbi5ibG9jayAuZGVsZXRlSWNvbiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZSAhaW1wb3J0YW50O1xuICBjb2xvcjogI2ZmZmZmZiAhaW1wb3J0YW50O1xuICByaWdodDogNSU7XG4gIHRvcDogMyU7XG4gIGZvbnQtc2l6ZTogMjBweDtcbn1cbi5ibG9jayAuZGVsZXRlSWNvbjpiZWZvcmUge1xuICBmb250LXNpemU6IDMwcHggIWltcG9ydGFudDtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/tab3/tab3.page.ts":
/*!***********************************!*\
  !*** ./src/app/tab3/tab3.page.ts ***!
  \***********************************/
/*! exports provided: Tab3Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab3Page", function() { return Tab3Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/file-transfer/ngx */ "./node_modules/@ionic-native/file-transfer/ngx/index.js");
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "./node_modules/@ionic-native/camera/ngx/index.js");
/* harmony import */ var _providers_credential_provider__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../providers/credential-provider */ "./src/providers/credential-provider.ts");
/* harmony import */ var _shared_model_item_model__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../shared/model/item.model */ "./src/app/shared/model/item.model.ts");
/* harmony import */ var _ionic_native_crop_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic-native/crop/ngx */ "./node_modules/@ionic-native/crop/ngx/index.js");
/* harmony import */ var _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic-native/file/ngx */ "./node_modules/@ionic-native/file/ngx/index.js");











let Tab3Page = class Tab3Page {
    constructor(router, navCtrl, toastController, postPvdr, camera, crop, transfer, alertCtrl, storage, file) {
        this.router = router;
        this.navCtrl = navCtrl;
        this.toastController = toastController;
        this.postPvdr = postPvdr;
        this.camera = camera;
        this.crop = crop;
        this.transfer = transfer;
        this.alertCtrl = alertCtrl;
        this.storage = storage;
        this.file = file;
        this.greenthumb_title = "";
        this.user_id = "";
        this.fileName = "";
        this.croppedImagepath = "";
        this.isLoading = false;
        this.pictures = [];
        this.userData = { user_id: "", token: "", imageB64: "" };
        this.rand = Math.floor(Math.random() * 1000000) + 8;
    }
    goTabs() {
        this.router.navigate(['tabs']);
    }
    goHelp() {
        //window.location.href="http://greenthumbtrade.com/help";
        this.navCtrl.navigateForward((['tabs/help']), { animated: false, });
    }
    goDescribe() {
        this.storage.set("greenthumb_title", this.greenthumb_title);
        if (this.greenthumb_title == '') {
            this.presentToast();
        }
        else {
            this.navCtrl.navigateForward((['describeitem']), { animated: false, });
        }
    }
    presentToast() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: 'Please Provide Title of the Item.',
                duration: 3000
            });
            toast.present();
        });
    }
    ngOnInit() {
        this.photos = [];
    }
    ionViewWillEnter() {
        this.plotData();
    }
    plotData() {
        this.storage.get("greenthumb_user_id").then((u_id) => {
            this.user_id = u_id;
        });
    }
    deletePhoto(index) {
        this.alertCtrl.create({
            header: "Sure you want to delete this photo? There is NO undo!",
            message: "",
            buttons: [
                {
                    text: "No",
                    handler: () => {
                        console.log("Disagree clicked");
                    }
                },
                {
                    text: "Yes",
                    handler: () => {
                        console.log("Agree clicked");
                        this.photos.splice(index, 1);
                    }
                }
            ]
        }).then(res => {
            res.present();
        });
    }
    getImage() {
        console.log("getImage function");
        this.pickImage(this.camera.PictureSourceType.PHOTOLIBRARY);
        // const cameraOptions: CameraOptions = {
        //   quality: 70,
        //   destinationType: this.camera.DestinationType.DATA_URL,
        //   sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
        //   saveToPhotoAlbum: false,
        //   correctOrientation: true,
        //   allowEdit:true,
        //   targetHeight: 150,
        // }
        // this.camera.getPicture(cameraOptions).then((imageData) => {
        //   this.base64Image = 'data:image/jpeg;base64,' + imageData;
        //   this.photos.push(this.base64Image);
        //     this.photos.reverse();
        //     this.sendData(imageData);
        // }, (err) => {
        //   console.log(err);
        // });
    }
    takePhoto() {
        console.log("coming here");
        this.pickImage(this.camera.PictureSourceType.CAMERA);
        // const options: CameraOptions = {
        //   quality: 50,
        //   destinationType: this.camera.DestinationType.DATA_URL,
        //   encodingType: this.camera.EncodingType.JPEG,
        //   mediaType: this.camera.MediaType.PICTURE,
        //   correctOrientation: true,
        //   targetWidth: 450,
        //   targetHeight: 450,
        //   saveToPhotoAlbum: false
        // };
        // this.camera.getPicture(options).then(
        //   imageData => {
        //     this.base64Image = "data:image/jpeg;base64," + imageData;
        //     this.photos.push(this.base64Image);
        //     this.photos.reverse();
        //     this.sendData(imageData);
        //   },
        //   err => {
        //     console.log(err);
        //   }
        // );
    }
    pickImage(sourceType) {
        const options = {
            quality: 100,
            sourceType: sourceType,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE
        };
        this.camera.getPicture(options).then((imageData) => {
            // imageData is either a base64 encoded string or a file URI
            // If it's base64 (DATA_URL):
            // let base64Image = 'data:image/jpeg;base64,' + imageData;
            console.log("imageData before:" + imageData);
            this.cropImage(imageData);
        }, (err) => {
            // Handle error
        });
    }
    // async selectImage() {
    //   const actionSheet = await this.actionSheetController.create({
    //     header: "Select Image source",
    //     buttons: [{
    //       text: 'Load from Library',
    //       handler: () => {
    //         this.pickImage(this.camera.PictureSourceType.PHOTOLIBRARY);
    //       }
    //     },
    //     {
    //       text: 'Use Camera',
    //       handler: () => {
    //         this.pickImage(this.camera.PictureSourceType.CAMERA);
    //       }
    //     },
    //     {
    //       text: 'Cancel',
    //       role: 'cancel'
    //     }
    //     ]
    //   });
    //   await actionSheet.present();
    // }
    cropImage(fileUrl) {
        console.log("fileURL:" + fileUrl);
        this.crop.crop(fileUrl, { quality: 50 })
            .then(newPath => {
            this.showCroppedImage(newPath.split('?')[0]);
        }, error => {
            alert('Error cropping image' + error);
        });
    }
    showCroppedImage(ImagePath) {
        this.isLoading = true;
        var copyPath = ImagePath;
        var splitPath = copyPath.split('/');
        var imageName = splitPath[splitPath.length - 1];
        var filePath = ImagePath.split(imageName)[0];
        this.file.readAsDataURL(filePath, imageName).then(base64 => {
            this.croppedImagepath = base64;
            this.base64Image = base64;
            this.photos.push(this.base64Image);
            this.photos.reverse();
            this.sendData(base64);
            this.isLoading = false;
        }, error => {
            alert('Error in showing image' + error);
            this.isLoading = false;
        });
    }
    sendData(imageData) {
        const fileTransfer = this.transfer.create();
        this.rand = Math.floor(Math.random() * 1000000) + 8;
        this.fileName = "greenthumb_" + this.rand + "_" + this.user_id + ".jpg";
        this.userData.imageB64 = imageData;
        this.userData.user_id = "1";
        this.userData.token = "222";
        //console.log(this.userData);
        // console.log("image:"+this.base64Image);
        let options = {
            fileKey: 'photo',
            fileName: this.fileName,
            chunkedMode: false,
            httpMethod: 'post',
            mimeType: "image/jpeg",
            headers: {}
        };
        fileTransfer.upload(this.base64Image, this.postPvdr.myServer() + '/greenthumb/images/posted_items/images_upload.php', options)
            .then((data) => {
            this.pictures.push(new _shared_model_item_model__WEBPACK_IMPORTED_MODULE_8__["ItemPicture"](this.fileName));
            this.storage.set("greenthumb_itempost_pic", this.pictures);
        }, (err) => {
            console.log(err);
            alert("Please check your internet connection and try again.");
        });
        // this.authService.postData(this.userData, "userImage").then(
        //   result => {
        //     this.responseData = result;
        //   },
        //   err => {
        //   }
        // );
    }
};
Tab3Page.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"] },
    { type: _providers_credential_provider__WEBPACK_IMPORTED_MODULE_7__["PostProvider"] },
    { type: _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_6__["Camera"] },
    { type: _ionic_native_crop_ngx__WEBPACK_IMPORTED_MODULE_9__["Crop"] },
    { type: _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_5__["FileTransfer"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_2__["Storage"] },
    { type: _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_10__["File"] }
];
Tab3Page = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-tab3',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./tab3.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/tab3/tab3.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./tab3.page.scss */ "./src/app/tab3/tab3.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"],
        _providers_credential_provider__WEBPACK_IMPORTED_MODULE_7__["PostProvider"],
        _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_6__["Camera"],
        _ionic_native_crop_ngx__WEBPACK_IMPORTED_MODULE_9__["Crop"],
        _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_5__["FileTransfer"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"],
        _ionic_storage__WEBPACK_IMPORTED_MODULE_2__["Storage"],
        _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_10__["File"]])
], Tab3Page);



/***/ })

}]);
//# sourceMappingURL=tab3-tab3-module-es2015.js.map