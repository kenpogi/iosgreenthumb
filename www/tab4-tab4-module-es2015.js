(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["tab4-tab4-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/tab4/tab4.page.html":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/tab4/tab4.page.html ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header translucent mode=\"ios\">\n    <ion-toolbar mode=\"ios\">\n        <ion-buttons slot=\"secondary\" (click)=\"goBack()\">\n            <ion-button>\n              <ion-icon slot=\"icon-only\" name=\"arrow-back\" mode=\"ios\"></ion-icon>\n            </ion-button>\n          </ion-buttons>\n\n        <ion-title>Brixy Store</ion-title>\n\n        <ion-buttons slot=\"primary\" (click)=\"toAccount()\">\n          <ion-button>\n              <img src=\"assets/icon/profilee.png\" style=\"width: 32px;\" (click)=\"toAccount()\" mode=\"ios\">\n          </ion-button>\n        </ion-buttons>\n      </ion-toolbar>\n</ion-header>\n\n<ion-content mode=\"ios\" padding>\n             <ion-row style=\"padding-bottom: 5px;\">\n                <ion-col style=\"color: black\" mode=\"ios\" text-left>\n                    <ion-label class=\"lvltext\"><b>GOLD BARS:</b>&nbsp;&nbsp;</ion-label> <br>\n                    <img class=\"lvlicon\" src=\"assets/icon/gold.png\">\n                    <ion-text class=\"lvlnum\">{{gold_bar | number:'1.0':'en-US'}}</ion-text>\n                </ion-col>\n                <ion-col style=\"color: black;margin-top: 1.2%;\" mode=\"ios\" text-right>\n                    <ion-label class=\"lvltext\"><b>COINS:</b>&nbsp;&nbsp;</ion-label><br>\n                    <img class=\"lvlicon2\" src=\"assets/icon/imgcoin.png\">\n                    <ion-text class=\"lvlnum\">{{user_coins | number:'1.0':'en-US'}}</ion-text>\n                </ion-col>\n            </ion-row>\n\n\n            <ion-grid>\n                <ion-row  mode=\"ios\" text-center style=\"background: #1dc1e6;border-radius: 30px;width: 100%;\">\n                    <ion-col style=\"color: white\" >\n                        <ion-label>PURCHASE COINS&nbsp;&nbsp;</ion-label>\n                    </ion-col>\n\n                </ion-row>\n                <!-- <ion-row>\n                    <ion-col>\n                        <ion-list  mode=\"ios\">\n                            <ion-item *ngFor=\"let x of coinsList;\" (click)=\"purchaseCoins(x);\">\n                                <ion-label style=\"font-size: 12px\">\n                                    <ion-icon name=\"logo-bitcoin\" style=\"zoom: 1.2\" color=\"warning\"></ion-icon>{{x.coins}}</ion-label>\n                                <ion-note slot=\"end\" color=\"danger\" no-padding style=\"padding-top: 5px;\">\n                                    <ion-button  mode=\"ios\" size=\"small\" color=\"success\" fill=\"outline\" style=\"font-size:11px\">\n                                            USD {{x.amount}}</ion-button>\n                                </ion-note>\n                            </ion-item>\n                        </ion-list>\n                    </ion-col>\n                </ion-row> -->\n\n            <ion-row>\n                <ion-col>\n                    <ion-list  mode=\"ios\">\n                        <ion-item *ngFor=\"let x of coinsList;\" (click)=\"purchaseCoins(x);\">\n                            <ion-label style=\"font-size: 12px\">\n                                <img class=\"lvlicon2\" src=\"assets/icon/imgcoin.png\">{{x.coins}}</ion-label>\n                            <ion-note slot=\"end\" color=\"danger\" no-padding style=\"padding-top: 5px;\">\n                                <ion-button  mode=\"ios\" size=\"small\" color=\"success\" fill=\"outline\" style=\"font-size:11px\">\n                                        USD {{x.amount}}</ion-button>\n                            </ion-note>\n                        </ion-item>\n                     </ion-list>\n                </ion-col>\n            </ion-row>\n            </ion-grid>\n\n    <!-- <ion-slides #slides (ionSlideDidChange)=\"slideChanged()\" mode=\"ios\">\n        <ion-slide>\n            <ion-grid>\n           \n            <ion-row  mode=\"ios\" text-center style=\"background: #1dc1e6;border-radius: 30px;width: 100%;\">\n                    <ion-col style=\"color: white\" >\n                        <ion-label>PURCHASE GIFTS&nbsp;&nbsp;</ion-label>\n                    </ion-col>\n            </ion-row>\n            <ion-row>\n                <ion-col>\n                    <ion-list mode=\"ios\">\n                        <ion-item (click)=\"bronze()\">\n                            <ion-label style=\"font-size: 12px\">Bronze (Popular Gifts)</ion-label>\n                            <ion-note slot=\"end\" color=\"danger\">{{bronzeGiftCount}}</ion-note>\n                        </ion-item>\n                        <ion-item (click)=\"silver()\">\n                            <ion-label style=\"font-size: 12px\">Silver (Hot Gifts)</ion-label>\n                            <ion-note slot=\"end\" color=\"danger\">{{silverGiftCount}}</ion-note>\n                        </ion-item>\n                        <ion-item (click)=\"gold()\">\n                            <ion-label style=\"font-size: 12px\">Gold (Exclusive Gifts)</ion-label>\n                            <ion-note slot=\"end\" color=\"danger\">{{goldGiftCount}}</ion-note>\n                        </ion-item>\n                        <ion-item (click)=\"popular()\">\n                            <ion-label style=\"font-size: 12px\">Popular Pinoy Gifts</ion-label>\n                            <ion-note slot=\"end\" color=\"danger\" >{{popularGiftCount}}</ion-note>\n                        </ion-item>\n                     </ion-list>\n                </ion-col>\n            </ion-row>\n            <ion-row   mode=\"ios\" text-center style=\"background: #1dc1e6;border-radius: 30px;width: 100%;\">\n                    <ion-col style=\"color: white\" >\n                        <ion-label>PURCHASE GOLD&nbsp;&nbsp;</ion-label>\n                    </ion-col>\n            </ion-row>\n            <ion-row>\n                <ion-col>\n                    <ion-list  mode=\"ios\">\n                        <ion-item *ngFor=\"let x of coinsList;\" (click)=\"purchaseCoins(x);\">\n                            <ion-label style=\"font-size: 12px\">\n                                <ion-icon name=\"logo-bitcoin\" style=\"zoom: 1.2\" color=\"warning\"></ion-icon>{{x.coins}}</ion-label>\n                            <ion-note slot=\"end\" color=\"danger\" no-padding style=\"padding-top: 5px;\">\n                                <ion-button  mode=\"ios\" size=\"small\" color=\"success\" fill=\"outline\" style=\"font-size:11px\">\n                                        USD {{x.amount}}</ion-button>\n                            </ion-note>\n                        </ion-item>\n                        \n                     </ion-list>\n                </ion-col>\n            </ion-row>\n            </ion-grid>\n        </ion-slide>\n        <ion-slide>\n            <ion-grid>\n            <ion-row  mode=\"ios\" text-center style=\"background: #1dc1e6;border-radius: 30px;width: 100%;\">\n                    <ion-col style=\"color: white\" >\n                        <ion-label>GIFTS&nbsp;&nbsp;</ion-label>\n                    </ion-col>\n            </ion-row>\n            <ion-row>\n                <ion-col>\n                    <ion-list  mode=\"ios\">\n                        <ion-item (click)=\"bronze()\">\n                            <ion-label style=\"font-size: 12px\">Bronze (Popular Gifts)</ion-label>\n                            <ion-note slot=\"end\" color=\"danger\">99</ion-note>\n                        </ion-item>\n                        <ion-item (click)=\"silver()\">\n                            <ion-label style=\"font-size: 12px\">Silver (Hot Gifts)</ion-label>\n                            <ion-note slot=\"end\" color=\"danger\">99</ion-note>\n                        </ion-item>\n                        <ion-item (click)=\"gold()\">\n                            <ion-label style=\"font-size: 12px\">Gold (Exclusive Gifts)</ion-label>\n                            <ion-note slot=\"end\" color=\"danger\">99</ion-note>\n                        </ion-item>\n                        <ion-item (click)=\"popular()\">\n                            <ion-label style=\"font-size: 12px\">Popular Pinoy Gifts</ion-label>\n                            <ion-note slot=\"end\" color=\"danger\" >99</ion-note>\n                        </ion-item>\n                     </ion-list>\n                </ion-col>\n            </ion-row>\n            </ion-grid>\n        </ion-slide>\n        <ion-slide>\n            <ion-grid>\n                <ion-row  mode=\"ios\" text-center style=\"background: #1dc1e6;border-radius: 30px;width: 100%;\">\n                    <ion-col style=\"color: white\" >\n                        <ion-label>PURCHASE GOLD&nbsp;&nbsp;</ion-label>\n                    </ion-col>\n            </ion-row>\n            <ion-row>\n                <ion-col>\n                    <ion-list  mode=\"ios\">\n                        <ion-item *ngFor=\"let x of coinsList;\" (click)=\"purchaseCoins(x);\">\n                            <ion-label style=\"font-size: 12px\">\n                                <ion-icon name=\"logo-bitcoin\" style=\"zoom: 1.2\" color=\"warning\"></ion-icon>{{x.coins}}</ion-label>\n                            <ion-note slot=\"end\" color=\"danger\" no-padding style=\"padding-top: 5px;\">\n                                <ion-button  mode=\"ios\" size=\"small\" color=\"success\" fill=\"outline\" style=\"font-size:11px\">\n                                        USD {{x.amount}}</ion-button>\n                            </ion-note>\n                        </ion-item>\n                     </ion-list>\n                </ion-col>\n            </ion-row>\n            </ion-grid>\n        </ion-slide>\n    </ion-slides> -->\n</ion-content>\n");

/***/ }),

/***/ "./src/app/shared/model/coin.model.ts":
/*!********************************************!*\
  !*** ./src/app/shared/model/coin.model.ts ***!
  \********************************************/
/*! exports provided: coinModel, coinTransactionModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "coinModel", function() { return coinModel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "coinTransactionModel", function() { return coinTransactionModel; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");

class coinModel {
    constructor(id, coins, amount) {
        this.id = id;
        this.coins = coins;
        this.amount = amount;
    }
}
class coinTransactionModel {
    constructor(image, dateandtime, text) {
        this.image = image;
        this.dateandtime = dateandtime;
        this.text = text;
    }
}


/***/ }),

/***/ "./src/app/tab4/tab4.module.ts":
/*!*************************************!*\
  !*** ./src/app/tab4/tab4.module.ts ***!
  \*************************************/
/*! exports provided: Tab4PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab4PageModule", function() { return Tab4PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _tab4_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./tab4.page */ "./src/app/tab4/tab4.page.ts");







const routes = [
    {
        path: '',
        component: _tab4_page__WEBPACK_IMPORTED_MODULE_6__["Tab4Page"]
    }
];
let Tab4PageModule = class Tab4PageModule {
};
Tab4PageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_tab4_page__WEBPACK_IMPORTED_MODULE_6__["Tab4Page"]]
    })
], Tab4PageModule);



/***/ }),

/***/ "./src/app/tab4/tab4.page.scss":
/*!*************************************!*\
  !*** ./src/app/tab4/tab4.page.scss ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".justify {\n  text-align: justify;\n}\n\n.img-responsive {\n  width: 100%;\n  height: auto;\n}\n\n.btn-default {\n  --background: #1dc1e6;\n}\n\n.item-inner {\n  padding-right: 0px !important;\n}\n\n.lvlicon {\n  width: 25px !important;\n  height: 25px;\n  margin: auto;\n  margin-bottom: -2%;\n  margin-right: 3%;\n}\n\n.lvlicon2 {\n  width: 20px !important;\n  height: 18px;\n  margin: auto;\n  margin-bottom: -1%;\n  margin-right: 3%;\n}\n\n.lvltext {\n  text-transform: capitalize;\n  font-size: 12px;\n  font-family: arial;\n}\n\n.lvlnum {\n  margin: 0;\n  margin-top: 4%;\n  font-weight: bolder;\n  font-family: Verdana, Geneva, Tahoma, sans-serif;\n}\n\nion-item {\n  --padding-start: 0% !important;\n  --inner-padding-end: 0px !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZW5kcmljay9EZXNrdG9wL1RlbG1vIFNvbHV0aW9ucy9Gb3JQcm9kdWN0aW9uL21heTEwMjAyMS9sYXN0IHNldHVwL2dyZWVudGh1bWJhcHAgY29weS9zcmMvYXBwL3RhYjQvdGFiNC5wYWdlLnNjc3MiLCJzcmMvYXBwL3RhYjQvdGFiNC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxtQkFBQTtBQ0NKOztBRENBO0VBQ0ksV0FBQTtFQUNBLFlBQUE7QUNFSjs7QURBQTtFQUNJLHFCQUFBO0FDR0o7O0FEREE7RUFDSSw2QkFBQTtBQ0lKOztBREZDO0VBQ0csc0JBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7QUNLSjs7QURIRTtFQUNFLHNCQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0FDTUo7O0FESkU7RUFDRSwwQkFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtBQ09KOztBRExFO0VBQ0UsU0FBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtFQUNBLGdEQUFBO0FDUUo7O0FETkU7RUFDRSw4QkFBQTtFQUNBLG1DQUFBO0FDU0oiLCJmaWxlIjoic3JjL2FwcC90YWI0L3RhYjQucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmp1c3RpZnl7XG4gICAgdGV4dC1hbGlnbjoganVzdGlmeTtcbn1cbi5pbWctcmVzcG9uc2l2ZXtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IGF1dG87XG59XG4uYnRuLWRlZmF1bHR7XG4gICAgLS1iYWNrZ3JvdW5kOiAjMWRjMWU2O1xufVxuLml0ZW0taW5uZXIge1xuICAgIHBhZGRpbmctcmlnaHQ6IDBweCFpbXBvcnRhbnQ7XG4gfVxuIC5sdmxpY29ue1xuICAgIHdpZHRoOiAyNXB4ICFpbXBvcnRhbnQ7IFxuICAgIGhlaWdodDogMjVweDsgXG4gICAgbWFyZ2luOiBhdXRvO1xuICAgIG1hcmdpbi1ib3R0b206IC0yJTtcbiAgICBtYXJnaW4tcmlnaHQ6IDMlO1xuICB9XG4gIC5sdmxpY29uMntcbiAgICB3aWR0aDogMjBweCAhaW1wb3J0YW50O1xuICAgIGhlaWdodDogMThweDtcbiAgICBtYXJnaW46IGF1dG87XG4gICAgbWFyZ2luLWJvdHRvbTogLTElO1xuICAgIG1hcmdpbi1yaWdodDogMyU7XG4gIH1cbiAgLmx2bHRleHR7XG4gICAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG4gICAgZm9udC1zaXplOiAxMnB4O1xuICAgIGZvbnQtZmFtaWx5OiBhcmlhbDtcbiAgfVxuICAubHZsbnVte1xuICAgIG1hcmdpbjogMDtcbiAgICBtYXJnaW4tdG9wOiA0JTtcbiAgICBmb250LXdlaWdodDogYm9sZGVyO1xuICAgIGZvbnQtZmFtaWx5OiBWZXJkYW5hLCBHZW5ldmEsIFRhaG9tYSwgc2Fucy1zZXJpZjtcbiAgfVxuICBpb24taXRlbSB7XG4gICAgLS1wYWRkaW5nLXN0YXJ0OiAwJSAhaW1wb3J0YW50O1xuICAgIC0taW5uZXItcGFkZGluZy1lbmQ6IDBweCAhaW1wb3J0YW50O1xuICAgXG59XG4iLCIuanVzdGlmeSB7XG4gIHRleHQtYWxpZ246IGp1c3RpZnk7XG59XG5cbi5pbWctcmVzcG9uc2l2ZSB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IGF1dG87XG59XG5cbi5idG4tZGVmYXVsdCB7XG4gIC0tYmFja2dyb3VuZDogIzFkYzFlNjtcbn1cblxuLml0ZW0taW5uZXIge1xuICBwYWRkaW5nLXJpZ2h0OiAwcHggIWltcG9ydGFudDtcbn1cblxuLmx2bGljb24ge1xuICB3aWR0aDogMjVweCAhaW1wb3J0YW50O1xuICBoZWlnaHQ6IDI1cHg7XG4gIG1hcmdpbjogYXV0bztcbiAgbWFyZ2luLWJvdHRvbTogLTIlO1xuICBtYXJnaW4tcmlnaHQ6IDMlO1xufVxuXG4ubHZsaWNvbjIge1xuICB3aWR0aDogMjBweCAhaW1wb3J0YW50O1xuICBoZWlnaHQ6IDE4cHg7XG4gIG1hcmdpbjogYXV0bztcbiAgbWFyZ2luLWJvdHRvbTogLTElO1xuICBtYXJnaW4tcmlnaHQ6IDMlO1xufVxuXG4ubHZsdGV4dCB7XG4gIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xuICBmb250LXNpemU6IDEycHg7XG4gIGZvbnQtZmFtaWx5OiBhcmlhbDtcbn1cblxuLmx2bG51bSB7XG4gIG1hcmdpbjogMDtcbiAgbWFyZ2luLXRvcDogNCU7XG4gIGZvbnQtd2VpZ2h0OiBib2xkZXI7XG4gIGZvbnQtZmFtaWx5OiBWZXJkYW5hLCBHZW5ldmEsIFRhaG9tYSwgc2Fucy1zZXJpZjtcbn1cblxuaW9uLWl0ZW0ge1xuICAtLXBhZGRpbmctc3RhcnQ6IDAlICFpbXBvcnRhbnQ7XG4gIC0taW5uZXItcGFkZGluZy1lbmQ6IDBweCAhaW1wb3J0YW50O1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/tab4/tab4.page.ts":
/*!***********************************!*\
  !*** ./src/app/tab4/tab4.page.ts ***!
  \***********************************/
/*! exports provided: Tab4Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab4Page", function() { return Tab4Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _giftbronze_giftbronze_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../giftbronze/giftbronze.component */ "./src/app/giftbronze/giftbronze.component.ts");
/* harmony import */ var _giftgold_giftgold_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./../giftgold/giftgold.component */ "./src/app/giftgold/giftgold.component.ts");
/* harmony import */ var _giftsilver_giftsilver_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./../giftsilver/giftsilver.component */ "./src/app/giftsilver/giftsilver.component.ts");
/* harmony import */ var _giftpopular_giftpopular_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./../giftpopular/giftpopular.component */ "./src/app/giftpopular/giftpopular.component.ts");
/* harmony import */ var _providers_credential_provider__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../providers/credential-provider */ "./src/providers/credential-provider.ts");
/* harmony import */ var _shared_model_coin_model__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../shared/model/coin.model */ "./src/app/shared/model/coin.model.ts");
/* harmony import */ var _purchasecoins_purchasecoins_page__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../purchasecoins/purchasecoins.page */ "./src/app/purchasecoins/purchasecoins.page.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");













let Tab4Page = class Tab4Page {
    constructor(router, popoverCtrl, modalController, storage, postPvdr) {
        this.router = router;
        this.popoverCtrl = popoverCtrl;
        this.modalController = modalController;
        this.storage = storage;
        this.postPvdr = postPvdr;
        this.segment = 0;
        this.bronzeGiftCount = 0;
        this.silverGiftCount = 0;
        this.goldGiftCount = 0;
        this.popularGiftCount = 0;
        this.gold_bar = 0;
        this.user_coins = 0;
        this.coinsList = [];
    }
    goWallet() {
        this.router.navigate(['wallet']);
    }
    toAccount() {
        this.router.navigate(['myaccount']);
    }
    goBack() {
        window.history.back();
    }
    segmentChanged() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            yield this.slider.slideTo(this.segment);
        });
    }
    slideChanged() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.segment = yield this.slider.getActiveIndex();
        });
    }
    bronze(ev) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const popover = yield this.popoverCtrl.create({
                component: _giftbronze_giftbronze_component__WEBPACK_IMPORTED_MODULE_4__["GiftbronzeComponent"],
                event: ev,
                animated: true,
                showBackdrop: true
            });
            return yield popover.present();
        });
    }
    silver(ev) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const popover = yield this.popoverCtrl.create({
                component: _giftsilver_giftsilver_component__WEBPACK_IMPORTED_MODULE_6__["GiftsilverComponent"],
                event: ev,
                animated: true,
                showBackdrop: true
            });
            return yield popover.present();
        });
    }
    gold(ev) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const popover = yield this.popoverCtrl.create({
                component: _giftgold_giftgold_component__WEBPACK_IMPORTED_MODULE_5__["GiftgoldComponent"],
                event: ev,
                animated: true,
                showBackdrop: true
            });
            return yield popover.present();
        });
    }
    popular(ev) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const popover = yield this.popoverCtrl.create({
                component: _giftpopular_giftpopular_component__WEBPACK_IMPORTED_MODULE_7__["GiftpopularComponent"],
                event: ev,
                animated: true,
                showBackdrop: true
            });
            return yield popover.present();
        });
    }
    ngOnInit() {
        console.log("hello tab4");
        this.plotData();
    }
    purchaseCoins(coins) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            // console.log("coins:"+coins.id);
            // console.log("coins:"+coins.coins);
            // console.log("coins:"+coins.amount);
            const modal = yield this.modalController.create({
                component: _purchasecoins_purchasecoins_page__WEBPACK_IMPORTED_MODULE_10__["PurchasecoinsPage"],
                cssClass: 'purchasecoins',
                componentProps: { coin: coins }
            });
            modal.onDidDismiss()
                .then((data) => {
                //const user = data['data']; // Here's your selected user!
                this.showCoinsandGold();
            });
            return yield modal.present();
        });
    }
    plotData() {
        this.showCoinsandGold();
        let body2 = {
            action: 'showCoins'
        };
        this.postPvdr.postData(body2, 'brixy-store.php').subscribe(data => {
            if (data.success) {
                const coins = [];
                for (const key in data.result) {
                    coins.push(new _shared_model_coin_model__WEBPACK_IMPORTED_MODULE_9__["coinModel"](data.result[key].id, data.result[key].coins, data.result[key].amount));
                }
                this.coinsList = coins;
            }
        });
    }
    showCoinsandGold() {
        this.storage.get("user_id").then((user_id) => {
            let body = {
                action: 'getGold_bar',
                user_id: user_id
            };
            this.postPvdr.postData(body, 'brixy-store.php').subscribe(data => {
                if (data.success) {
                    for (const key in data.result) {
                        this.gold_bar = data.result[key].gold_bar;
                        this.user_coins = data.result[key].user_coins;
                    }
                }
            });
        });
    }
};
Tab4Page.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["PopoverController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_11__["Storage"] },
    { type: _providers_credential_provider__WEBPACK_IMPORTED_MODULE_8__["PostProvider"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('slides', { static: true }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonSlides"])
], Tab4Page.prototype, "slider", void 0);
Tab4Page = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-tab4',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./tab4.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/tab4/tab4.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./tab4.page.scss */ "./src/app/tab4/tab4.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["PopoverController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"],
        _ionic_storage__WEBPACK_IMPORTED_MODULE_11__["Storage"],
        _providers_credential_provider__WEBPACK_IMPORTED_MODULE_8__["PostProvider"]])
], Tab4Page);



/***/ })

}]);
//# sourceMappingURL=tab4-tab4-module-es2015.js.map