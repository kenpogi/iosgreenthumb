(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["tab5-tab5-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/tab5/tab5.page.html":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/tab5/tab5.page.html ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header no-border mode=\"ios\">\n    <ion-toolbar mode=\"ios\">\n      <ion-buttons slot=\"secondary\" (click)=\"goBack()\">\n        <ion-button mode=\"ios\" style=\"font-size: 15px;\" color=\"dark\" (click)=\"goBack()\">\n            Back\n        </ion-button>\n      </ion-buttons>\n        <ion-title color=\"secondary\">Messages</ion-title>\n        <ion-buttons slot=\"primary\" (click)=\"goHelp()\">\n          <ion-button mode=\"ios\" style=\"font-size: 15px;\" (click)=\"goHelp()\">\n              Help\n          </ion-button>\n        </ion-buttons>\n    </ion-toolbar>\n</ion-header>\n<ion-segment style=\"padding: 5px\" color=\"primary\" mode=\"ios\" (ionChange)=\"segmentChanged()\" [(ngModel)]=\"segment\">\n  <ion-segment-button class=\"for_segment\" mode=\"ios\" value=\"0\">\n      <ion-label>Messages \n        <ion-badge *ngIf=\"notif_message>0\" class=\"for_segmentbadge\" mode=\"ios\" color=\"tertiary\">{{notif_message}}\n      </ion-badge></ion-label>\n  </ion-segment-button>  \n  <ion-segment-button  class=\"for_segment\" mode=\"ios\" value=\"1\">\n      <ion-label>Notifications \n        <ion-badge *ngIf=\"notif_notif>0\" class=\"for_segmentbadge\" mode=\"ios\" color=\"tertiary\">{{notif_notif}}\n      </ion-badge></ion-label>\n  </ion-segment-button>\n  <ion-segment-button class=\"for_segment\" mode=\"ios\" value=\"2\">\n    <ion-label>Announcements \n      <ion-badge *ngIf=\"notif_announcement>0\" class=\"for_segmentbadge\" mode=\"ios\" color=\"tertiary\">{{notif_announcement}}\n    </ion-badge></ion-label>\n</ion-segment-button>\n</ion-segment>\n<ion-content>\n  <ion-slides #slides (ionSlideDidChange)=\"slideChanged()\" mode=\"ios\">\n    <!-- For Messages -->\n        <ion-slide>\n\n          <ion-grid style=\"text-align: center;\" *ngIf=\"loader\"> \n\n            <ion-spinner name=\"crescent\"></ion-spinner>\n        </ion-grid>\n\n          <ion-grid *ngIf=\"!loader\">\n            <ion-row style=\"padding: 5px;padding-top: 0;\">\n              <ion-col>\n                <ion-searchbar style=\"padding: 0;text-align: left;margin-bottom: 10px;\" placeholder=\"Search...\" id=\"customsearchbar\" mode=\"ios\"></ion-searchbar>\n                <ion-list mode=\"ios\">\n                  <ion-item mode=\"ios\" *ngFor=\"let conversation of myConversations;\">\n                    <ion-avatar slot=\"start\" mode=\"ios\" id=\"for_sellingavatar\">\n                      <div *ngIf=\"!conversation?.profile_photo\">\n                        <img src=\"assets/greenthumb-images/userpic.png\">\n                      </div>\n                      <div *ngIf=\"conversation?.profile_photo\">\n                        <img [src] = \"conversation.profile_photo\" >\n                      </div> \n                    </ion-avatar>\n                    <ion-label (click)=\"goMessage(conversation?.id, conversation.item_id)\">\n                      <h2 style=\"font-size: 14px;color: #679733;text-transform: capitalize;\">{{conversation?.nickname}}</h2>\n                      <h3 style=\"font-size: 13px;text-transform: capitalize;\">{{conversation.item_name}}</h3>\n                      <p style=\"font-size: 12px;padding: 0;\" [ngClass]=\"conversation?.user_sent != login_user_id ? (conversation?.status == '0' ? 'unread' : '') : ''\">\n                        {{conversation.message}}</p>\n                    </ion-label>\n                    <ion-label slot=\"end\" text-right color=\"secondary\" style=\"margin-left: -100px;align-self: flex-end;\">\n                      <h3 style=\"font-size: 10px;margin-bottom: -2px;\" mode=\"ios\">{{conversation?.time}} </h3>\n                      <h3 style=\"font-size: 10px;\" mode=\"ios\">{{conversation?.date}} </h3>\n                    </ion-label>\n                  </ion-item>\n                  <!-- <ion-item mode=\"ios\" *ngFor=\"let conversation of myConversations;\">\n                    <ion-avatar slot=\"start\" mode=\"ios\">\n                      <div *ngIf=\"!conversation?.profile_photo\">\n                        <img src=\"assets/greenthumb-images/userpic.png\">\n                      </div>\n                      <div *ngIf=\"conversation?.profile_photo\">\n                        <img [src] = \"conversation.profile_photo\" >\n                      </div> \n                    </ion-avatar>\n                    <ion-label (click)=\"goMessage(conversation?.id, conversation.item_id)\" mode=\"ios\">\n                      <h2 style=\"font-size: 14px;color: #679733;\" mode=\"ios\">{{conversation?.nickname}} - {{conversation.item_name}}</h2>\n                      <h3 style=\"font-size: 12px;\" mode=\"ios\" [ngClass]=\"conversation?.user_sent != login_user_id ? (conversation?.status == '0' ? 'unread' : '') : ''\">{{conversation.message}}</h3>\n                    </ion-label>\n                    <ion-label style=\"flex: 0 0 30px;\" text-right mode=\"ios\" slot=\"end\" color=\"secondary\">\n                      <h3 style=\"font-size: 12px;\" mode=\"ios\">{{conversation?.date}} {{conversation?.time}}</h3>\n                    </ion-label>\n                  </ion-item> -->\n                  \n                </ion-list>\n              </ion-col>\n            </ion-row>\n          </ion-grid>\n        </ion-slide>\n        <!-- For Notifications -->\n        <ion-slide>\n          <ion-grid>\n            <ion-row style=\"padding: 5px;\">\n              <ion-col>\n                <ion-list mode=\"ios\">\n                  <ion-item mode=\"ios\" *ngFor=\"let notif of systemNotif\">\n                    <ion-avatar slot=\"start\" mode=\"ios\">\n                      <div *ngIf=\"notif.with_rate=='0'\">\n                        <img style=\"border-radius: 0;\" src=\"assets/greenthumb-images/like_notif.png\">\n                      </div> \n                      <div *ngIf=\"notif.with_rate=='1' || notif.with_rate=='2'\">\n                        <img src=\"assets/greenthumb-images/dollar_notif.png\">\n                      </div> \n                    </ion-avatar>\n                    <ion-label mode=\"ios\" text-wrap>\n                      <h2 style=\"font-size: 13px;color: #679733;\" mode=\"ios\">\n                        <i>{{notif.notification}}</i></h2>\n                      <p *ngIf=\"notif.with_rate!='1'\" style=\"font-size: 12px;padding: 0;color: #989aa2;padding-top: 3px;\">\n                        {{notif.date}} {{notif.time}}\n                      </p>\n                    </ion-label>\n                    <ion-label style=\"flex: 0 0 20px;\" text-right mode=\"ios\" slot=\"end\">\n                      <ion-row id='notif{{notif.id}}' *ngIf=\"notif.with_rate=='1'\">\n                        <ion-col no-padding (click)=\"goRate(notif.rate_user_id, notif.id)\">\n                          <img src=\"assets/greenthumb-images/check_notif.png\">\n                        </ion-col>\n                      </ion-row>\n                    </ion-label>\n                  </ion-item>\n                  <!-- <ion-item mode=\"ios\">\n                    <ion-avatar slot=\"start\" mode=\"ios\">\n                      <div>\n                        <img src=\"assets/greenthumb-images/like_notif.png\">\n                      </div> \n                    </ion-avatar>\n                    <ion-label mode=\"ios\" text-wrap>\n                      <h2 style=\"font-size: 13px;color: #679733;\" mode=\"ios\">\n                        <i>Buyer \"Michel Fernandez\" has just mark your product \n                          \"Lorem item name\" as purchased and sold. Please verify.</i></h2>\n                    </ion-label>\n                    <ion-label style=\"flex: 0 0 30px;\" text-right mode=\"ios\" slot=\"end\">\n                      <ion-row>\n                        <ion-col (click)=\"goRate()\">\n                          <img src=\"assets/greenthumb-images/check_notif.png\">\n                        </ion-col>\n                      </ion-row>\n                      <ion-row>\n                        <ion-col style=\"padding-top: 0;\">\n                          <img src=\"assets/greenthumb-images/close_notif.png\">\n                        </ion-col>\n                      </ion-row>\n                    </ion-label>\n                  </ion-item> -->\n                </ion-list>\n              </ion-col>\n            </ion-row>\n          </ion-grid>\n        </ion-slide>\n        <!-- For Announcements -->\n        <ion-slide>\n          <ion-grid>\n            <ion-row style=\"padding: 5px;\">\n              <ion-col>\n                <ion-list mode=\"ios\">\n                  <ion-card mode=\"ios\" *ngFor=\"let announce of announcementAll\" no-margin text-left>\n                    <ion-card-header mode=\"ios\" class=\"card_header\">\n                      <ion-card-title mode=\"ios\" class=\"card_title\">{{announce.title}}</ion-card-title>\n                    </ion-card-header>\n                    <ion-card-content mode=\"ios\" class=\"card_content\">\n                      {{announce.announcement}}\n                      <br>\n                      <div text-right class=\"card_time\">{{announce.date}} {{announce.time}}</div>\n                    </ion-card-content>\n                  </ion-card>\n                </ion-list>\n              </ion-col>\n            </ion-row>\n          </ion-grid>\n        </ion-slide>\n    </ion-slides>\n</ion-content>\n\n\n");

/***/ }),

/***/ "./src/app/tab5/tab5.module.ts":
/*!*************************************!*\
  !*** ./src/app/tab5/tab5.module.ts ***!
  \*************************************/
/*! exports provided: Tab5PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab5PageModule", function() { return Tab5PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _tab5_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./tab5.page */ "./src/app/tab5/tab5.page.ts");
/* harmony import */ var _shared_header_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../shared/header.module */ "./src/app/shared/header.module.ts");








const routes = [
    {
        path: '',
        component: _tab5_page__WEBPACK_IMPORTED_MODULE_6__["Tab5Page"]
    }
];
let Tab5PageModule = class Tab5PageModule {
};
Tab5PageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
            _shared_header_module__WEBPACK_IMPORTED_MODULE_7__["HeaderModule"]
        ],
        declarations: [_tab5_page__WEBPACK_IMPORTED_MODULE_6__["Tab5Page"]]
    })
], Tab5PageModule);



/***/ }),

/***/ "./src/app/tab5/tab5.page.scss":
/*!*************************************!*\
  !*** ./src/app/tab5/tab5.page.scss ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".unread {\n  font-weight: bold;\n  color: black;\n}\n\n.for_segment {\n  padding: 3px;\n  padding-bottom: 5px;\n  padding-top: 5px;\n  padding-right: 0;\n}\n\n.for_segmentbadge {\n  color: #e44545;\n  font-size: 10px;\n  padding-left: 3px;\n  padding-right: 4px;\n  display: inline;\n  padding-top: 1px;\n  padding-bottom: 1px;\n}\n\nion-item {\n  --padding-start: 0% !important;\n  --inner-padding-end: 0;\n  --border-color: #f4f7f8;\n  --padding-bottom: 10px;\n}\n\nion-avatar {\n  width: 100% !important;\n  height: 100% !important;\n  max-width: 45px !important;\n  max-height: 45px !important;\n}\n\n#for_sellingavatar {\n  width: 100% !important;\n  height: 100% !important;\n  max-width: 55px !important;\n  max-height: 55px !important;\n}\n\n#for_sellingavatar img {\n  border: 1px solid rgba(0, 0, 0, 0.12);\n  width: 100% !important;\n  height: 100% !important;\n  max-width: 55px !important;\n  max-height: 55px !important;\n  border-radius: 50%;\n}\n\n.card_header {\n  padding-bottom: 0;\n}\n\n.card_title {\n  font-size: 15px;\n}\n\n.card_content {\n  font-size: 14px;\n  padding-top: 8px;\n  line-height: 1.1;\n}\n\n.card_time {\n  padding-top: 3%;\n  color: black;\n}\n\n.unread {\n  font-weight: bold;\n  color: black;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZW5kcmljay9EZXNrdG9wL1RlbG1vIFNvbHV0aW9ucy9Gb3JQcm9kdWN0aW9uL21heTEwMjAyMS9sYXN0IHNldHVwL2dyZWVudGh1bWJhcHAgY29weS9zcmMvYXBwL3RhYjUvdGFiNS5wYWdlLnNjc3MiLCJzcmMvYXBwL3RhYjUvdGFiNS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxpQkFBQTtFQUNBLFlBQUE7QUNDSjs7QURDQTtFQUNJLFlBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7QUNFSjs7QURBQTtFQUNJLGNBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0FDR0o7O0FEREE7RUFDSSw4QkFBQTtFQUNBLHNCQUFBO0VBQ0EsdUJBQUE7RUFDQSxzQkFBQTtBQ0lKOztBREZBO0VBQ0ksc0JBQUE7RUFDQSx1QkFBQTtFQUNBLDBCQUFBO0VBQ0EsMkJBQUE7QUNLSjs7QURIQTtFQUNJLHNCQUFBO0VBQ0EsdUJBQUE7RUFDQSwwQkFBQTtFQUNBLDJCQUFBO0FDTUo7O0FETFE7RUFDSSxxQ0FBQTtFQUNBLHNCQUFBO0VBQ0EsdUJBQUE7RUFDQSwwQkFBQTtFQUNBLDJCQUFBO0VBQ0Esa0JBQUE7QUNPWjs7QURIQTtFQUNJLGlCQUFBO0FDTUo7O0FESkE7RUFDSSxlQUFBO0FDT0o7O0FETEE7RUFDSSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtBQ1FKOztBRE5BO0VBQ0ksZUFBQTtFQUNBLFlBQUE7QUNTSjs7QUROQTtFQUNJLGlCQUFBO0VBQ0EsWUFBQTtBQ1NKIiwiZmlsZSI6InNyYy9hcHAvdGFiNS90YWI1LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi51bnJlYWR7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgY29sb3I6IGJsYWNrO1xufVxuLmZvcl9zZWdtZW50e1xuICAgIHBhZGRpbmc6IDNweDtcbiAgICBwYWRkaW5nLWJvdHRvbTogNXB4O1xuICAgIHBhZGRpbmctdG9wOiA1cHg7XG4gICAgcGFkZGluZy1yaWdodDogMDtcbn1cbi5mb3Jfc2VnbWVudGJhZGdle1xuICAgIGNvbG9yOiAjZTQ0NTQ1O1xuICAgIGZvbnQtc2l6ZTogMTBweDtcbiAgICBwYWRkaW5nLWxlZnQ6IDNweDtcbiAgICBwYWRkaW5nLXJpZ2h0OiA0cHg7XG4gICAgZGlzcGxheTogaW5saW5lO1xuICAgIHBhZGRpbmctdG9wOiAxcHg7XG4gICAgcGFkZGluZy1ib3R0b206IDFweDtcbn1cbmlvbi1pdGVtIHtcbiAgICAtLXBhZGRpbmctc3RhcnQ6IDAlICFpbXBvcnRhbnQ7XG4gICAgLS1pbm5lci1wYWRkaW5nLWVuZDogMDtcbiAgICAtLWJvcmRlci1jb2xvcjogI2Y0ZjdmODsgXG4gICAgLS1wYWRkaW5nLWJvdHRvbTogMTBweDtcbn1cbmlvbi1hdmF0YXJ7XG4gICAgd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcbiAgICBoZWlnaHQ6IDEwMCUgIWltcG9ydGFudDtcbiAgICBtYXgtd2lkdGg6IDQ1cHggIWltcG9ydGFudDtcbiAgICBtYXgtaGVpZ2h0OiA0NXB4ICFpbXBvcnRhbnQ7XG59XG4jZm9yX3NlbGxpbmdhdmF0YXJ7XG4gICAgd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcbiAgICBoZWlnaHQ6IDEwMCUgIWltcG9ydGFudDtcbiAgICBtYXgtd2lkdGg6IDU1cHggIWltcG9ydGFudDtcbiAgICBtYXgtaGVpZ2h0OiA1NXB4ICFpbXBvcnRhbnQ7XG4gICAgICAgIGltZ3tcbiAgICAgICAgICAgIGJvcmRlcjogMXB4IHNvbGlkIHJnYmEoMCwgMCwgMCwgMC4xMik7XG4gICAgICAgICAgICB3aWR0aDogMTAwJSAhaW1wb3J0YW50O1xuICAgICAgICAgICAgaGVpZ2h0OiAxMDAlICFpbXBvcnRhbnQ7XG4gICAgICAgICAgICBtYXgtd2lkdGg6IDU1cHggIWltcG9ydGFudDtcbiAgICAgICAgICAgIG1heC1oZWlnaHQ6IDU1cHggIWltcG9ydGFudDtcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICAgICAgfVxufVxuXG4uY2FyZF9oZWFkZXJ7XG4gICAgcGFkZGluZy1ib3R0b206IDA7XG59XG4uY2FyZF90aXRsZXtcbiAgICBmb250LXNpemU6IDE1cHg7XG59XG4uY2FyZF9jb250ZW50e1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICBwYWRkaW5nLXRvcDogOHB4O1xuICAgIGxpbmUtaGVpZ2h0OiAxLjE7XG59XG4uY2FyZF90aW1le1xuICAgIHBhZGRpbmctdG9wOiAzJTtcbiAgICBjb2xvcjpibGFja1xufVxuXG4udW5yZWFke1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIGNvbG9yOiBibGFjaztcbn1cbiIsIi51bnJlYWQge1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgY29sb3I6IGJsYWNrO1xufVxuXG4uZm9yX3NlZ21lbnQge1xuICBwYWRkaW5nOiAzcHg7XG4gIHBhZGRpbmctYm90dG9tOiA1cHg7XG4gIHBhZGRpbmctdG9wOiA1cHg7XG4gIHBhZGRpbmctcmlnaHQ6IDA7XG59XG5cbi5mb3Jfc2VnbWVudGJhZGdlIHtcbiAgY29sb3I6ICNlNDQ1NDU7XG4gIGZvbnQtc2l6ZTogMTBweDtcbiAgcGFkZGluZy1sZWZ0OiAzcHg7XG4gIHBhZGRpbmctcmlnaHQ6IDRweDtcbiAgZGlzcGxheTogaW5saW5lO1xuICBwYWRkaW5nLXRvcDogMXB4O1xuICBwYWRkaW5nLWJvdHRvbTogMXB4O1xufVxuXG5pb24taXRlbSB7XG4gIC0tcGFkZGluZy1zdGFydDogMCUgIWltcG9ydGFudDtcbiAgLS1pbm5lci1wYWRkaW5nLWVuZDogMDtcbiAgLS1ib3JkZXItY29sb3I6ICNmNGY3Zjg7XG4gIC0tcGFkZGluZy1ib3R0b206IDEwcHg7XG59XG5cbmlvbi1hdmF0YXIge1xuICB3aWR0aDogMTAwJSAhaW1wb3J0YW50O1xuICBoZWlnaHQ6IDEwMCUgIWltcG9ydGFudDtcbiAgbWF4LXdpZHRoOiA0NXB4ICFpbXBvcnRhbnQ7XG4gIG1heC1oZWlnaHQ6IDQ1cHggIWltcG9ydGFudDtcbn1cblxuI2Zvcl9zZWxsaW5nYXZhdGFyIHtcbiAgd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcbiAgaGVpZ2h0OiAxMDAlICFpbXBvcnRhbnQ7XG4gIG1heC13aWR0aDogNTVweCAhaW1wb3J0YW50O1xuICBtYXgtaGVpZ2h0OiA1NXB4ICFpbXBvcnRhbnQ7XG59XG4jZm9yX3NlbGxpbmdhdmF0YXIgaW1nIHtcbiAgYm9yZGVyOiAxcHggc29saWQgcmdiYSgwLCAwLCAwLCAwLjEyKTtcbiAgd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcbiAgaGVpZ2h0OiAxMDAlICFpbXBvcnRhbnQ7XG4gIG1heC13aWR0aDogNTVweCAhaW1wb3J0YW50O1xuICBtYXgtaGVpZ2h0OiA1NXB4ICFpbXBvcnRhbnQ7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbn1cblxuLmNhcmRfaGVhZGVyIHtcbiAgcGFkZGluZy1ib3R0b206IDA7XG59XG5cbi5jYXJkX3RpdGxlIHtcbiAgZm9udC1zaXplOiAxNXB4O1xufVxuXG4uY2FyZF9jb250ZW50IHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBwYWRkaW5nLXRvcDogOHB4O1xuICBsaW5lLWhlaWdodDogMS4xO1xufVxuXG4uY2FyZF90aW1lIHtcbiAgcGFkZGluZy10b3A6IDMlO1xuICBjb2xvcjogYmxhY2s7XG59XG5cbi51bnJlYWQge1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgY29sb3I6IGJsYWNrO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/tab5/tab5.page.ts":
/*!***********************************!*\
  !*** ./src/app/tab5/tab5.page.ts ***!
  \***********************************/
/*! exports provided: Tab5Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab5Page", function() { return Tab5Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _liveprofile_liveprofile_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../liveprofile/liveprofile.page */ "./src/app/liveprofile/liveprofile.page.ts");
/* harmony import */ var _rate_rate_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../rate/rate.page */ "./src/app/rate/rate.page.ts");
/* harmony import */ var _providers_credential_provider__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../providers/credential-provider */ "./src/providers/credential-provider.ts");
/* harmony import */ var _shared_model_chat_model__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../shared/model/chat.model */ "./src/app/shared/model/chat.model.ts");
/* harmony import */ var _shared_model_announcement_model__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../shared/model/announcement.model */ "./src/app/shared/model/announcement.model.ts");
/* harmony import */ var _shared_model_systemnotif_model__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../shared/model/systemnotif.model */ "./src/app/shared/model/systemnotif.model.ts");













let Tab5Page = class Tab5Page {
    constructor(router, modalController, storage, postPvdr, navCtrl) {
        this.router = router;
        this.modalController = modalController;
        this.storage = storage;
        this.postPvdr = postPvdr;
        this.navCtrl = navCtrl;
        this.segment = 0;
        this.searchData = [];
        this.systemNotif = [];
        this.myConversations = [];
        this.announcementAll = [];
        this.myClass = false;
        this.login_user_id = '';
        this.loader = true;
    }
    goRate(rate_user_id, notif_id) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _rate_rate_page__WEBPACK_IMPORTED_MODULE_6__["RatePage"],
                cssClass: 'ratingmodalstyle',
                componentProps: {
                    rate_user_id,
                    login_user_id: this.login_user_id,
                    notif_id
                }
            });
            modal.onDidDismiss()
                .then((data) => {
                console.log("data" + JSON.stringify(data));
                if (data['data']) {
                    console.log("true pag-rate");
                    //document.getElementById("notif"+notif_id).style.visibility = "hidden";
                    //this.router.navigate(['tabs/offers']);
                    let navigationExtras = {
                        queryParams: { fromRate: true }
                    };
                    this.navCtrl.navigateForward(['tabs/offers'], navigationExtras);
                }
                else {
                    console.log("wala mag-rate");
                }
            });
            return yield modal.present();
        });
    }
    goBack() {
        window.history.back();
    }
    goHelp() {
        this.router.navigate(['tabs/help']);
    }
    segmentChanged() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            yield this.slider.slideTo(this.segment).then(() => {
                console.log("slide to activated");
                this.content.scrollToTop();
                // this.scrollToTop();
            });
        });
    }
    slideChanged() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.segment = yield this.slider.getActiveIndex();
            this.storage.set("greenthumb_segment", this.segment);
            if (this.segment == 1) {
                let body21 = {
                    action: 'UpdateNotif',
                    type: 2,
                    user_id: this.login_user_id
                };
                this.postPvdr.postData(body21, 'system_notification.php').subscribe(data => {
                    if (data.success) {
                        this.plotNotif();
                    }
                });
            }
            else if (this.segment == 2) {
                let body21 = {
                    action: 'UpdateNotif',
                    type: 3,
                    user_id: this.login_user_id
                };
                this.postPvdr.postData(body21, 'system_notification.php').subscribe(data => {
                    if (data.success) {
                        this.plotNotif();
                    }
                });
            }
        });
    }
    toAccount() {
        this.router.navigate(['myaccount']);
    }
    goMessage(val, item_id) {
        // this.router.navigateByUrl('/message/'+val);
        this.navCtrl.navigateRoot(['/message/' + val + "-separator-" + item_id + "-separator-" + '0']);
    }
    ngOnInit() {
        // this.plotData();
        // this.initializeData();
    }
    ionViewWillEnter() {
        console.log("ionviewwillenter in tab5 conversation");
        this.plotData();
        this.storage.get("greenthumb_segment").then((seg) => {
            if (seg != null) {
                this.segment = seg;
            }
        });
    }
    ionViewDidEnter() {
        console.log("ionViewDidEnter entered");
    }
    filterData(ev) {
        var val = ev.target.value;
        if (val && val.trim().length > 0) {
            // this.searchData = this.searchData.filter((item) => {
            //   console.log("item:"+JSON.stringify(item));
            //   return (item.nickname.toLowerCase().indexOf(val.toLowerCase()) >-1);
            this.searchData = [];
            this.myClass = true;
            this.initializeData(val);
            // })
        }
        else if (val.length == 0) {
            this.searchData = [];
            this.myClass = false;
        }
        else {
            this.searchData = [];
            this.myClass = false;
        }
        console.log("val.trim():" + val.trim());
        console.log("val.length():" + val.length);
    }
    searchedConvo(val) {
        //this.router.navigateByUrl('/message/'+val);
        this.navCtrl.navigateRoot(['/message/' + val]);
    }
    searchProfile() {
        this.storage.get("user_id").then((user_id) => {
            let body = {
                action: 'getuserdata',
                user_id: user_id
            };
            this.postPvdr.postData(body, 'credentials-api.php').subscribe((data) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                if (data.success) {
                    // for(const key in data.result){
                    //   pictureProfile = (data.result[key].profile_photo == '') ? '' :
                    //   "http://"+this.postPvdr.myServer()+"/brixy-live/images/"+data.result[key].profile_photo;
                    //   livestream_users.push(new UserLive(
                    //     data.result[key].user_id,
                    //     data.result[key].stream_id,
                    //     data.result[key].fname,
                    //     data.result[key].lname,
                    //     data.result[key].city,
                    //     pictureProfile,
                    //     data.result[key].user_level,
                    //     data.result[key].badge_name,
                    //     data.result[key].viewers,
                    //     data.result[key].broadcast_topic
                    //     )
                    //     );
                    // }
                }
            }));
        });
    }
    viewProfile(user_id) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _liveprofile_liveprofile_page__WEBPACK_IMPORTED_MODULE_5__["LiveprofilePage"],
                cssClass: 'liveprofilemodalstyle',
                componentProps: {
                    liveStreamProfileId: user_id
                }
            });
            return yield modal.present();
        });
    }
    plotData() {
        this.loader = true;
        // for all announcement
        this.storage.get('greenthumb_user_id').then((user_id) => {
            this.login_user_id = user_id;
            let body322 = {
                action: 'getAllAnnouncement',
                user_id: user_id
            };
            //console.log("storyalang:"+JSON.stringify(body321));
            this.postPvdr.postData(body322, 'system_notification.php').subscribe(data => {
                if (data.success) {
                    const announcementsAllList = [];
                    for (const key in data.result) {
                        announcementsAllList.push(new _shared_model_announcement_model__WEBPACK_IMPORTED_MODULE_9__["Announcement"](data.result[key].id, data.result[key].title, data.result[key].announcment, data.result[key].date, data.result[key].time));
                    }
                    this.announcementAll = announcementsAllList;
                }
            });
            let body3 = {
                action: 'getMySystemNotifications',
                user_id: user_id
            };
            this.postPvdr.postData(body3, 'system_notification.php').subscribe(data => {
                if (data.success) {
                    const notificationList = [];
                    for (const key in data.result) {
                        notificationList.push(new _shared_model_systemnotif_model__WEBPACK_IMPORTED_MODULE_10__["SystemNotif"](data.result[key].id, data.result[key].notification_message, data.result[key].date, data.result[key].time, data.result[key].with_rate, data.result[key].rate_user_id));
                    }
                    this.systemNotif = notificationList;
                }
            });
            this.plotNotif();
        });
        this.myConversations = [];
        this.storage.get("greenthumb_user_id").then((user_id) => {
            this.login_user_id = user_id;
            let body = {
                action: 'getConversation',
                user_id: user_id
            };
            console.log("body of plotdata:" + JSON.stringify(body));
            this.postPvdr.postData(body, 'messages.php').subscribe((data) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                if (data.success) {
                    var myNickname = "";
                    for (const key in data.result) {
                        if (data.result[key].myNickname == "") {
                            if (data.result[key].login_type_id == "3") {
                                myNickname = data.result[key].mobile_num;
                            }
                            else if (data.result[key].login_type_id == "4") {
                                myNickname = data.result[key].email;
                            }
                        }
                        else {
                            myNickname = data.result[key].myNickname;
                        }
                        this.myConversations.push(new _shared_model_chat_model__WEBPACK_IMPORTED_MODULE_8__["Conversations"](data.result[key].user_id, data.result[key].id, myNickname, (data.result[key].profile_photo == '') ? '' :
                            this.postPvdr.myServer() + "/greenthumb/images/" + data.result[key].profile_photo, data.result[key].item_id, data.result[key].item_name, data.result[key].message, data.result[key].date_sent, data.result[key].time_sent, data.result[key].notif_status, data.result[key].user_sent));
                    }
                    this.loader = false;
                }
            }));
        });
    }
    plotNotif() {
        let body21 = {
            action: 'getNotifMessage',
            type: 1,
            user_id: this.login_user_id
        };
        this.postPvdr.postData(body21, 'messages.php').subscribe(data => {
            console.log(data);
            if (data.success) {
                this.notif_message = parseInt(data.notif_message);
                this.notif_notif = parseInt(data.notif_notif);
                this.notif_announcement = parseInt(data.notif_announcement);
                //console.log("notif_message:"+ data.notif_message +":"+data.notif_notif+':'+data.notif_announcement);
            }
        });
    }
    initializeData(val) {
        this.storage.get("user_id").then((user_id) => {
            let body = {
                action: 'getUsersToChat',
                nickname: val,
                user_id: user_id
            };
            this.postPvdr.postData(body, 'messages.php').subscribe((data) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                if (data.success) {
                    var myNickname = "";
                    for (const key in data.result) {
                        if (data.result[key].myNickname == "") {
                            if (data.result[key].login_type_id == "3") {
                                myNickname = data.result[key].mobile_num;
                            }
                            else if (data.result[key].login_type_id == "4") {
                                myNickname = data.result[key].email;
                            }
                        }
                        else {
                            myNickname = data.result[key].myNickname;
                        }
                        this.searchData.push(new _shared_model_chat_model__WEBPACK_IMPORTED_MODULE_8__["Users_search"](data.result[key].user_id, myNickname, (data.result[key].profile_photo == '') ? '' :
                            this.postPvdr.myServer() + "/brixy-live/images/" + data.result[key].profile_photo));
                    }
                }
            }));
        });
        // this.searchData = [
        //   {
        //     "name": "Argie",
        //     "code": "65"
        //   },
        //   {
        //     "name": "Argie1",
        //     "code": "234"
        //   },
        //   {
        //     "name": "Argie2",
        //     "code": "23"
        //   },
        //   {
        //     "name": "Diane",
        //     "code": "345"
        //   }
        //   ,
        //   {
        //     "name": "Diane2",
        //     "code": "4534"
        //   },
        //   {
        //     "name": "Diane3",
        //     "code": "fhf"
        //   },
        //   {
        //     "name": "Lyanna",
        //     "code": "as"
        //   }
        //   ,
        //   {
        //     "name": "Lyanna1",
        //     "code": "fdgfd"
        //   },
        //   {
        //     "name": "Lyanna2",
        //     "code": "mycsdfdsode"
        //   },
        //   {
        //     "name": "Lyanna3",
        //     "code": "sdfg"
        //   }
        // ]
    }
};
Tab5Page.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"] },
    { type: _providers_credential_provider__WEBPACK_IMPORTED_MODULE_7__["PostProvider"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('slides', { static: true }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonSlides"])
], Tab5Page.prototype, "slider", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonContent"], { static: false }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonContent"])
], Tab5Page.prototype, "content", void 0);
Tab5Page = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-tab5',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./tab5.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/tab5/tab5.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./tab5.page.scss */ "./src/app/tab5/tab5.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"],
        _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"],
        _providers_credential_provider__WEBPACK_IMPORTED_MODULE_7__["PostProvider"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"]])
], Tab5Page);



/***/ })

}]);
//# sourceMappingURL=tab5-tab5-module-es2015.js.map