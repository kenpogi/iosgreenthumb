(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["termsconditions-termsconditions-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/termsconditions/termsconditions.page.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/termsconditions/termsconditions.page.html ***!
  \*************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header translucent mode=\"ios\">\n  <ion-toolbar mode=\"ios\">\n      <ion-buttons slot=\"secondary\" (click)=\"goBack()\">\n        <ion-button>\n            <ion-icon name=\"arrow-back\" mode=\"ios\"></ion-icon>\n        </ion-button>\n      </ion-buttons>\n\n      <ion-title>Terms of Service</ion-title>\n\n      <ion-buttons slot=\"primary\">\n        <ion-button>\n            <img src=\"assets/icon/brixylogo.png\" style=\"width: 34px;\" mode=\"ios\">\n        </ion-button>\n      </ion-buttons>\n    </ion-toolbar>\n</ion-header>\n<ion-content mode=\"ios\">\n  <iframe src=\"https://brixylive.com/terms-of-use-conditions/\" style=\"height: 80vh; width: 100%;\"></iframe>\n<!-- <p class=\"justify\" padding  style=\"margin-bottom: 150px;\">\n    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce convallis pellentesque metus id lacinia. Nunc dapibus pulvinar auctor. Duis nec sem at orci commodo viverra id in ipsum. Fusce tellus nisl, vestibulum sed rhoncus at, pretium non libero. Cras vel lacus ut ipsum vehicula aliquam at quis urna.\n    <br><br>Nunc ac ornare ante. Fusce lobortis neque in diam vulputate quis semper sem elementum.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce convallis pellentesque metus id lacinia. Nunc dapibus pulvinar auctor. Duis nec sem at orci commodo viverra id in ipsum. Fusce tellus nisl, vestibulum sed rhoncus at, pretium non libero. Cras vel lacus ut ipsum vehicula aliquam at quis urna.\n    <br><br>Nunc ac ornare ante. Fusce lobortis neque in diam vulputate quis semper sem elementum.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce convallis pellentesque metus id lacinia. Cras vel lacus ut ipsum vehicula aliquam at quis urna. Nunc ac ornare ante. Fusce lobortis neque in diam vulputate quis semper sem elementum.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fu\n    <br><br>Nunc ac ornare ante. Fusce lobortis neque in diam vulputate quis semper sem elementum.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce convallis pellentesque metus id lacinia. Nunc dapibus pulvinar auctor. Duis nec sem at orci commodo viverra id in ipsum. Fusce tellus nisl, vestibulum sed rhoncus at, pretium non libero. Cras vel lacus ut ipsum vehicula aliquam at quis urna.\n    <br><br>Nunc ac ornare ante. Fusce lobortis neque in diam vulputate quis semper sem elementum.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce convallis pellentesque metus id lacinia. Cras vel lacus ut ipsum vehicula aliquam at quis urna. Nunc ac ornare ante. Fusce lobortis neque in diam vulputate quis semper sem elementum.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fu\n\n  </p> -->\n<!-- <div class=\"my-overlay\" padding [hidden]=\"overlayHidden\"> -->\n  <div class=\"my-overlay\" padding >\n    <ion-item style=\"text-align: center;\" lines=\"none\">\n        <ion-checkbox mode=\"ios\" (click)=\"agreeTermed()\" style=\"font-size: 10px;\" color=\"primary\"></ion-checkbox>\n        <ion-label style=\"font-size: 13px;font-weight: 500;color: #a5a5a5;\"><b> I agree to the Terms and Conditions of Brixy Live</b></ion-label>\n    </ion-item>\n     <ion-row>\n    <ion-col text-center>\n        <ion-button mode=\"ios\" style=\"width:100%\" [disabled]=\"!checked\" color=\"primary\" (click)=\"goLive()\">Continue</ion-button>\n      </ion-col>\n      <ion-col text-center>\n          <ion-button mode=\"ios\" style=\"width:100%\" color=\"light\" (click)=\"goBack()\">Cancel</ion-button>\n      </ion-col> \n    </ion-row>\n    \n</div>\n</ion-content>");

/***/ }),

/***/ "./src/app/termsconditions/termsconditions.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/termsconditions/termsconditions.module.ts ***!
  \***********************************************************/
/*! exports provided: TermsconditionsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TermsconditionsPageModule", function() { return TermsconditionsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _termsconditions_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./termsconditions.page */ "./src/app/termsconditions/termsconditions.page.ts");







const routes = [
    {
        path: '',
        component: _termsconditions_page__WEBPACK_IMPORTED_MODULE_6__["TermsconditionsPage"]
    }
];
let TermsconditionsPageModule = class TermsconditionsPageModule {
};
TermsconditionsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_termsconditions_page__WEBPACK_IMPORTED_MODULE_6__["TermsconditionsPage"]]
    })
], TermsconditionsPageModule);



/***/ }),

/***/ "./src/app/termsconditions/termsconditions.page.scss":
/*!***********************************************************!*\
  !*** ./src/app/termsconditions/termsconditions.page.scss ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".justify {\n  text-align: justify;\n}\n\n.img-responsive {\n  width: 100%;\n  height: auto;\n}\n\n.btn-default {\n  --background: #1dc1e6;\n}\n\n.my-overlay {\n  position: fixed;\n  width: 100%;\n  height: 20%;\n  background: white;\n  box-shadow: 5px 5px black;\n  z-index: 20;\n  bottom: 0%;\n  left: 0;\n  border-top: 1px solid #ebf1f0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZW5kcmljay9EZXNrdG9wL1RlbG1vIFNvbHV0aW9ucy9Gb3JQcm9kdWN0aW9uL21heTEwMjAyMS9sYXN0IHNldHVwL2dyZWVudGh1bWJhcHAgY29weS9zcmMvYXBwL3Rlcm1zY29uZGl0aW9ucy90ZXJtc2NvbmRpdGlvbnMucGFnZS5zY3NzIiwic3JjL2FwcC90ZXJtc2NvbmRpdGlvbnMvdGVybXNjb25kaXRpb25zLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLG1CQUFBO0FDQ0o7O0FEQ0E7RUFDSSxXQUFBO0VBQ0EsWUFBQTtBQ0VKOztBREFBO0VBQ0kscUJBQUE7QUNHSjs7QUREQTtFQUNJLGVBQUE7RUFDQSxXQUFBO0VBQ0EsV0FBQTtFQUNBLGlCQUFBO0VBQ0EseUJBQUE7RUFDQSxXQUFBO0VBQ0EsVUFBQTtFQUNBLE9BQUE7RUFDQSw2QkFBQTtBQ0lKIiwiZmlsZSI6InNyYy9hcHAvdGVybXNjb25kaXRpb25zL3Rlcm1zY29uZGl0aW9ucy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuanVzdGlmeXtcbiAgICB0ZXh0LWFsaWduOiBqdXN0aWZ5O1xufVxuLmltZy1yZXNwb25zaXZle1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogYXV0bztcbn1cbi5idG4tZGVmYXVsdHtcbiAgICAtLWJhY2tncm91bmQ6ICMxZGMxZTY7XG59XG4ubXktb3ZlcmxheSB7XG4gICAgcG9zaXRpb246IGZpeGVkO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogMjAlO1xuICAgIGJhY2tncm91bmQ6ICB3aGl0ZTtcbiAgICBib3gtc2hhZG93OiA1cHggNXB4IGJsYWNrO1xuICAgIHotaW5kZXg6IDIwO1xuICAgIGJvdHRvbTogMCU7XG4gICAgbGVmdDogMDtcbiAgICBib3JkZXItdG9wOiAxcHggc29saWQgI2ViZjFmMDtcbiAgfVxuIiwiLmp1c3RpZnkge1xuICB0ZXh0LWFsaWduOiBqdXN0aWZ5O1xufVxuXG4uaW1nLXJlc3BvbnNpdmUge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiBhdXRvO1xufVxuXG4uYnRuLWRlZmF1bHQge1xuICAtLWJhY2tncm91bmQ6ICMxZGMxZTY7XG59XG5cbi5teS1vdmVybGF5IHtcbiAgcG9zaXRpb246IGZpeGVkO1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAyMCU7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBib3gtc2hhZG93OiA1cHggNXB4IGJsYWNrO1xuICB6LWluZGV4OiAyMDtcbiAgYm90dG9tOiAwJTtcbiAgbGVmdDogMDtcbiAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICNlYmYxZjA7XG59Il19 */");

/***/ }),

/***/ "./src/app/termsconditions/termsconditions.page.ts":
/*!*********************************************************!*\
  !*** ./src/app/termsconditions/termsconditions.page.ts ***!
  \*********************************************************/
/*! exports provided: TermsconditionsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TermsconditionsPage", function() { return TermsconditionsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");



let TermsconditionsPage = class TermsconditionsPage {
    constructor(router) {
        this.router = router;
        this.checked = false;
    }
    goLive() {
        this.router.navigate(['readytogolive']);
    }
    goBack() {
        this.router.navigate(['tab3']);
    }
    agreeTermed() {
        this.checked = !this.checked;
        console.log("this.checked:" + this.checked);
    }
    ngOnInit() {
    }
};
TermsconditionsPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
TermsconditionsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-termsconditions',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./termsconditions.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/termsconditions/termsconditions.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./termsconditions.page.scss */ "./src/app/termsconditions/termsconditions.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
], TermsconditionsPage);



/***/ })

}]);
//# sourceMappingURL=termsconditions-termsconditions-module-es2015.js.map