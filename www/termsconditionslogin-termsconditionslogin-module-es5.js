function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["termsconditionslogin-termsconditionslogin-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/termsconditionslogin/termsconditionslogin.page.html":
  /*!***********************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/termsconditionslogin/termsconditionslogin.page.html ***!
    \***********************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppTermsconditionsloginTermsconditionsloginPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header mode=\"ios\" no-border>\n  <ion-toolbar mode=\"ios\">\n      <ion-title color=\"secondary\">Terms of Service</ion-title>\n    </ion-toolbar>\n</ion-header>\n<ion-content mode=\"ios\">\n  <iframe src=\"https://greenthumbtrade.com/terms-of-service\" style=\"height: 80vh; width: 100%;\"></iframe>\n<!-- <p style=\"margin-top: 0;margin-bottom: 50px;padding-top: 2%;\" padding mode=\"ios\" text-justify>\n    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce convallis pellentesque metus id lacinia. Nunc dapibus pulvinar auctor. Duis nec sem at orci commodo viverra id in ipsum. Fusce tellus nisl, vestibulum sed rhoncus at, pretium non libero. Cras vel lacus ut ipsum vehicula aliquam at quis urna.\n    <br><br>Nunc ac ornare ante. Fusce lobortis neque in diam vulputate quis semper sem elementum.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce convallis pellentesque metus id lacinia. Nunc dapibus pulvinar auctor. Duis nec sem at orci commodo viverra id in ipsum. Fusce tellus nisl, vestibulum sed rhoncus at, pretium non libero. Cras vel lacus ut ipsum vehicula aliquam at quis urna.\n    <br><br>Nunc ac ornare ante. Fusce lobortis neque in diam vulputate quis semper sem elementum.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce convallis pellentesque metus id lacinia. Cras vel lacus ut ipsum vehicula aliquam at quis urna. Nunc ac ornare ante. Fusce lobortis neque in diam vulputate quis semper sem elementum.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fu\n    <br><br>Nunc ac ornare ante. Fusce lobortis neque in diam vulputate quis semper sem elementum.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce convallis pellentesque metus id lacinia. Nunc dapibus pulvinar auctor. Duis nec sem at orci commodo viverra id in ipsum. Fusce tellus nisl, vestibulum sed rhoncus at, pretium non libero. Cras vel lacus ut ipsum vehicula aliquam at quis urna.\n    <br><br>Nunc ac ornare ante. Fusce lobortis neque in diam vulputate quis semper sem elementum.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce convallis pellentesque metus id lacinia. Cras vel lacus ut ipsum vehicula aliquam at quis urna. Nunc ac ornare ante. Fusce lobortis neque in diam vulputate quis semper sem elementum.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fu\n  </p> -->\n  <br> <br> <br> <br> <br> <br> \n  <div class=\"my-overlay\" padding>\n    \n    <ion-item lines=\"none\" style=\"--padding-start:0\" mode=\"ios\">\n      <ion-checkbox mode=\"ios\" (click)=\"agreeTermedChecked()\" style=\"font-size: 10px;margin: 0;margin-top: -10px;\" color=\"primary\" slot=\"start\"></ion-checkbox>\n      <h6 mode=\"ios\" text-center style=\"font-size: 13px;font-weight: 500;color: #a5a5a5;margin-bottom: 5%;margin-top: 2%;\">\n        By proceeding you also agree to the \n        <ion-text color=\"secondary\" (click)=\"terms_of_service()\">Terms of Service, </ion-text> \n        <ion-text color=\"secondary\" (click)=\"user_policy()\">User Policy,</ion-text> \n         and <ion-text color=\"secondary\" (click)=\"privacy_policy()\"> Privacy Policy.</ion-text> </h6>\n    </ion-item>\n    <ion-button [disabled]=\"!checked\" (click)=\"agreeTermed()\" expand=\"block\" mode=\"ios\" style=\"margin-bottom: 7%;\" color=\"primary\">\n      I AGREE\n    </ion-button>\n</div>\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/termsconditionslogin/termsconditionslogin.module.ts":
  /*!*********************************************************************!*\
    !*** ./src/app/termsconditionslogin/termsconditionslogin.module.ts ***!
    \*********************************************************************/

  /*! exports provided: TermsconditionsloginPageModule */

  /***/
  function srcAppTermsconditionsloginTermsconditionsloginModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "TermsconditionsloginPageModule", function () {
      return TermsconditionsloginPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _termsconditionslogin_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./termsconditionslogin.page */
    "./src/app/termsconditionslogin/termsconditionslogin.page.ts");

    var routes = [{
      path: '',
      component: _termsconditionslogin_page__WEBPACK_IMPORTED_MODULE_6__["TermsconditionsloginPage"]
    }];

    var TermsconditionsloginPageModule = function TermsconditionsloginPageModule() {
      _classCallCheck(this, TermsconditionsloginPageModule);
    };

    TermsconditionsloginPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      declarations: [_termsconditionslogin_page__WEBPACK_IMPORTED_MODULE_6__["TermsconditionsloginPage"]]
    })], TermsconditionsloginPageModule);
    /***/
  },

  /***/
  "./src/app/termsconditionslogin/termsconditionslogin.page.scss":
  /*!*********************************************************************!*\
    !*** ./src/app/termsconditionslogin/termsconditionslogin.page.scss ***!
    \*********************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppTermsconditionsloginTermsconditionsloginPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".my-overlay {\n  position: fixed;\n  width: 100%;\n  height: 20%;\n  background: white;\n  box-shadow: 5px 5px black;\n  z-index: 200000000000000000000000;\n  bottom: 0%;\n  left: 0;\n  border-top: 1px solid #e2e2e2;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZW5kcmljay9EZXNrdG9wL1RlbG1vIFNvbHV0aW9ucy9Gb3JQcm9kdWN0aW9uL21heTEwMjAyMS9sYXN0IHNldHVwL2dyZWVudGh1bWJhcHAgY29weS9zcmMvYXBwL3Rlcm1zY29uZGl0aW9uc2xvZ2luL3Rlcm1zY29uZGl0aW9uc2xvZ2luLnBhZ2Uuc2NzcyIsInNyYy9hcHAvdGVybXNjb25kaXRpb25zbG9naW4vdGVybXNjb25kaXRpb25zbG9naW4ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO0VBQ0UsZUFBQTtFQUNBLFdBQUE7RUFDQSxXQUFBO0VBQ0EsaUJBQUE7RUFDQSx5QkFBQTtFQUNBLGlDQUFBO0VBQ0EsVUFBQTtFQUNBLE9BQUE7RUFDQSw2QkFBQTtBQ0FGIiwiZmlsZSI6InNyYy9hcHAvdGVybXNjb25kaXRpb25zbG9naW4vdGVybXNjb25kaXRpb25zbG9naW4ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXG4ubXktb3ZlcmxheSB7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMjAlO1xuICBiYWNrZ3JvdW5kOiAgd2hpdGU7XG4gIGJveC1zaGFkb3c6IDVweCA1cHggYmxhY2s7XG4gIHotaW5kZXg6IDIwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDtcbiAgYm90dG9tOiAwJTtcbiAgbGVmdDogMDtcbiAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICNlMmUyZTI7XG59XG5cbi8vIC5ncmVlbnRodW1iX2xpbmtze1xuLy8gICBjb2xvcjogIzc2Yzk2MTsgXG4vLyAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbi8vIH0iLCIubXktb3ZlcmxheSB7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMjAlO1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgYm94LXNoYWRvdzogNXB4IDVweCBibGFjaztcbiAgei1pbmRleDogMjAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwO1xuICBib3R0b206IDAlO1xuICBsZWZ0OiAwO1xuICBib3JkZXItdG9wOiAxcHggc29saWQgI2UyZTJlMjtcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/termsconditionslogin/termsconditionslogin.page.ts":
  /*!*******************************************************************!*\
    !*** ./src/app/termsconditionslogin/termsconditionslogin.page.ts ***!
    \*******************************************************************/

  /*! exports provided: TermsconditionsloginPage */

  /***/
  function srcAppTermsconditionsloginTermsconditionsloginPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "TermsconditionsloginPage", function () {
      return TermsconditionsloginPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _userpolicy_userpolicy_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../userpolicy/userpolicy.page */
    "./src/app/userpolicy/userpolicy.page.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _privacypolicy_privacypolicy_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../privacypolicy/privacypolicy.page */
    "./src/app/privacypolicy/privacypolicy.page.ts");

    var TermsconditionsloginPage = /*#__PURE__*/function () {
      function TermsconditionsloginPage(router, modalController) {
        _classCallCheck(this, TermsconditionsloginPage);

        this.router = router;
        this.modalController = modalController;
        this.checked = false;
      }

      _createClass(TermsconditionsloginPage, [{
        key: "goBack",
        value: function goBack() {
          this.router.navigate(['ads']);
        }
      }, {
        key: "agreeTermed",
        value: function agreeTermed() {
          this.router.navigate(['choose']);
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "agreeTermedChecked",
        value: function agreeTermedChecked() {
          this.checked = !this.checked;
          console.log("this.checked:" + this.checked);
        }
      }, {
        key: "terms_of_service",
        value: function terms_of_service() {}
      }, {
        key: "user_policy",
        value: function user_policy() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
            var modal;
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    _context.next = 2;
                    return this.modalController.create({
                      component: _userpolicy_userpolicy_page__WEBPACK_IMPORTED_MODULE_3__["UserpolicyPage"],
                      cssClass: ''
                    });

                  case 2:
                    modal = _context.sent;
                    _context.next = 5;
                    return modal.present();

                  case 5:
                    return _context.abrupt("return", _context.sent);

                  case 6:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        }
      }, {
        key: "privacy_policy",
        value: function privacy_policy() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
            var modal;
            return regeneratorRuntime.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    _context2.next = 2;
                    return this.modalController.create({
                      component: _privacypolicy_privacypolicy_page__WEBPACK_IMPORTED_MODULE_5__["PrivacypolicyPage"],
                      cssClass: ''
                    });

                  case 2:
                    modal = _context2.sent;
                    _context2.next = 5;
                    return modal.present();

                  case 5:
                    return _context2.abrupt("return", _context2.sent);

                  case 6:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this);
          }));
        }
      }]);

      return TermsconditionsloginPage;
    }();

    TermsconditionsloginPage.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"]
      }];
    };

    TermsconditionsloginPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-termsconditionslogin',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./termsconditionslogin.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/termsconditionslogin/termsconditionslogin.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./termsconditionslogin.page.scss */
      "./src/app/termsconditionslogin/termsconditionslogin.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"]])], TermsconditionsloginPage);
    /***/
  }
}]);
//# sourceMappingURL=termsconditionslogin-termsconditionslogin-module-es5.js.map