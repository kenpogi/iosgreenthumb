(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["uploadphoto-uploadphoto-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/uploadphoto/uploadphoto.page.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/uploadphoto/uploadphoto.page.html ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header mode=\"ios\" no-border>\n  <ion-toolbar mode=\"ios\">\n    <ion-buttons slot=\"secondary\" (click)=\"goBack()\">\n      <ion-button mode=\"ios\" style=\"font-size: 15px;\">\n          Back\n      </ion-button>\n    </ion-buttons>\n\n      <ion-title color=\"secondary\">Upload Profile Picture</ion-title>\n\n    </ion-toolbar>\n</ion-header>\n\n<ion-content padding>\n  <ion-row>\n    <ion-col>\n      <ion-button fill=\"outline\" expand=\"block\"  (click)=\"getImage()\" mode=\"ios\">Choose a Photo</ion-button>\n      <ion-avatar *ngIf=\"image\">\n        <img [src]=\"image\" />\n      </ion-avatar>\n      <ion-button *ngIf=\"image\" expand=\"block\" mode=\"ios\"  (click)=\"uploadphoto()\">UPLOAD PHOTO</ion-button>\n    </ion-col>\n  </ion-row>\n \n</ion-content>\n");

/***/ }),

/***/ "./src/app/uploadphoto/uploadphoto.module.ts":
/*!***************************************************!*\
  !*** ./src/app/uploadphoto/uploadphoto.module.ts ***!
  \***************************************************/
/*! exports provided: UploadphotoPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UploadphotoPageModule", function() { return UploadphotoPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _uploadphoto_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./uploadphoto.page */ "./src/app/uploadphoto/uploadphoto.page.ts");







const routes = [
    {
        path: '',
        component: _uploadphoto_page__WEBPACK_IMPORTED_MODULE_6__["UploadphotoPage"]
    }
];
let UploadphotoPageModule = class UploadphotoPageModule {
};
UploadphotoPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_uploadphoto_page__WEBPACK_IMPORTED_MODULE_6__["UploadphotoPage"]]
    })
], UploadphotoPageModule);



/***/ }),

/***/ "./src/app/uploadphoto/uploadphoto.page.scss":
/*!***************************************************!*\
  !*** ./src/app/uploadphoto/uploadphoto.page.scss ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-avatar {\n  width: 100% !important;\n  height: 100% !important;\n  max-width: 250px !important;\n  max-height: 250px !important;\n  margin: auto;\n  margin-top: 20%;\n  margin-bottom: 10%;\n}\nion-avatar img {\n  border: 1px solid rgba(0, 0, 0, 0.12);\n  width: 100% !important;\n  height: 100% !important;\n  max-width: 250px !important;\n  max-height: 250px !important;\n  border-radius: 50%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZW5kcmljay9EZXNrdG9wL1RlbG1vIFNvbHV0aW9ucy9Gb3JQcm9kdWN0aW9uL21heTEwMjAyMS9sYXN0IHNldHVwL2dyZWVudGh1bWJhcHAgY29weS9zcmMvYXBwL3VwbG9hZHBob3RvL3VwbG9hZHBob3RvLnBhZ2Uuc2NzcyIsInNyYy9hcHAvdXBsb2FkcGhvdG8vdXBsb2FkcGhvdG8ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksc0JBQUE7RUFDQSx1QkFBQTtFQUNBLDJCQUFBO0VBQ0EsNEJBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0FDQ0o7QURBSTtFQUNJLHFDQUFBO0VBQ0Esc0JBQUE7RUFDQSx1QkFBQTtFQUNBLDJCQUFBO0VBQ0EsNEJBQUE7RUFDQSxrQkFBQTtBQ0VSIiwiZmlsZSI6InNyYy9hcHAvdXBsb2FkcGhvdG8vdXBsb2FkcGhvdG8ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWF2YXRhciAgeyAgICAgXG4gICAgd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcbiAgICBoZWlnaHQ6IDEwMCUgIWltcG9ydGFudDtcbiAgICBtYXgtd2lkdGg6IDI1MHB4ICFpbXBvcnRhbnQ7XG4gICAgbWF4LWhlaWdodDogMjUwcHggIWltcG9ydGFudDtcbiAgICBtYXJnaW46IGF1dG87XG4gICAgbWFyZ2luLXRvcDogMjAlO1xuICAgIG1hcmdpbi1ib3R0b206IDEwJTtcbiAgICBpbWd7XG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkIHJnYmEoMCwgMCwgMCwgMC4xMik7XG4gICAgICAgIHdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XG4gICAgICAgIGhlaWdodDogMTAwJSAhaW1wb3J0YW50O1xuICAgICAgICBtYXgtd2lkdGg6IDI1MHB4ICFpbXBvcnRhbnQ7XG4gICAgICAgIG1heC1oZWlnaHQ6IDI1MHB4ICFpbXBvcnRhbnQ7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICB9XG59IiwiaW9uLWF2YXRhciB7XG4gIHdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XG4gIGhlaWdodDogMTAwJSAhaW1wb3J0YW50O1xuICBtYXgtd2lkdGg6IDI1MHB4ICFpbXBvcnRhbnQ7XG4gIG1heC1oZWlnaHQ6IDI1MHB4ICFpbXBvcnRhbnQ7XG4gIG1hcmdpbjogYXV0bztcbiAgbWFyZ2luLXRvcDogMjAlO1xuICBtYXJnaW4tYm90dG9tOiAxMCU7XG59XG5pb24tYXZhdGFyIGltZyB7XG4gIGJvcmRlcjogMXB4IHNvbGlkIHJnYmEoMCwgMCwgMCwgMC4xMik7XG4gIHdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XG4gIGhlaWdodDogMTAwJSAhaW1wb3J0YW50O1xuICBtYXgtd2lkdGg6IDI1MHB4ICFpbXBvcnRhbnQ7XG4gIG1heC1oZWlnaHQ6IDI1MHB4ICFpbXBvcnRhbnQ7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/uploadphoto/uploadphoto.page.ts":
/*!*************************************************!*\
  !*** ./src/app/uploadphoto/uploadphoto.page.ts ***!
  \*************************************************/
/*! exports provided: UploadphotoPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UploadphotoPage", function() { return UploadphotoPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "./node_modules/@ionic-native/camera/ngx/index.js");
/* harmony import */ var _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/file-transfer/ngx */ "./node_modules/@ionic-native/file-transfer/ngx/index.js");
/* harmony import */ var _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/file/ngx */ "./node_modules/@ionic-native/file/ngx/index.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _providers_credential_provider__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../providers/credential-provider */ "./src/providers/credential-provider.ts");









let UploadphotoPage = class UploadphotoPage {
    constructor(router, loadingCtrl, camera, transfer, file, storage, toastController, postPvdr) {
        this.router = router;
        this.loadingCtrl = loadingCtrl;
        this.camera = camera;
        this.transfer = transfer;
        this.file = file;
        this.storage = storage;
        this.toastController = toastController;
        this.postPvdr = postPvdr;
        this.fileName = "";
        this.rand = Math.floor(Math.random() * 1000000) + 8;
    }
    goBack() {
        window.history.back();
    }
    ngOnInit() {
    }
    presentToast() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: 'Profile picture updated.',
                duration: 3000
            });
            toast.present();
        });
    }
    loading() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            //show loading
            this.loader = yield this.loadingCtrl.create({
                message: "Uploading..."
            });
            this.loader.present();
        });
    }
    getImage() {
        const cameraOptions = {
            quality: 70,
            destinationType: this.camera.DestinationType.DATA_URL,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            saveToPhotoAlbum: false,
            correctOrientation: true,
            allowEdit: true,
            targetHeight: 150,
        };
        this.camera.getPicture(cameraOptions).then((imageData) => {
            let base64Image = 'data:image/jpeg;base64,' + imageData;
            this.image = base64Image;
            console.log("image:" + this.image);
        }, (err) => {
            console.log(err);
            // Handle error
        });
    }
    // cropImage(){
    //   const options: CameraOptions = {
    //     quality: 70,
    //     destinationType: this.camera.DestinationType.DATA_URL,
    //     sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
    //     saveToPhotoAlbum: false,
    //     allowEdit: true,
    //     targetWidth: 250,
    //     targetHeight: 250
    //   }
    //   this.camera.getPicture(options).then((imageData) => {
    //     this.image = 'data:image/jpeg;base64,' + imageData;
    //   }, (err) => {
    //       //handle error
    //   });
    // }
    uploadphoto() {
        this.loading();
        const fileTransfer = this.transfer.create();
        this.storage.get("greenthumb_user_id").then((u_id) => {
            this.user_id = u_id;
            console.log("greenthumb_user_id:" + u_id);
            this.fileName = "greenthumb_" + this.rand + "_" + this.user_id + ".jpg";
            this.storage.get('picture').then((val) => {
                this.old_picture = val;
                console.log(this.old_picture + " is old pic");
                let options = {
                    fileKey: 'photo',
                    fileName: this.fileName,
                    chunkedMode: false,
                    httpMethod: 'post',
                    mimeType: "image/jpeg",
                    headers: {}
                };
                //console.log("options Upload:"+JSON.stringify(options));
                fileTransfer.upload(this.image, this.postPvdr.myServer() + '/greenthumb/images_upload.php', options)
                    .then((data) => {
                    let body = {
                        action: "update_picture",
                        picture: this.fileName,
                        old_picture: this.old_picture,
                        user_id: this.user_id
                    };
                    console.log("body update_picture:" + JSON.stringify(body));
                    this.postPvdr.postData(body, 'credentials-api.php').subscribe(data => {
                        if (data.success) {
                            this.loader.dismiss();
                            this.presentToast();
                            this.router.navigate(["myaccount"]);
                        }
                        else {
                            alert("Please check your internet connection and try again.");
                        }
                    });
                }, (err) => {
                    console.log(err);
                    this.loader.dismiss();
                    alert("Please check your internet connection and try again.");
                });
            });
        });
        // this.storage.get('email').then((email) => {
        //   if(email) {
        //     this.key_email = email;
        //       this.fileName = "brixy_"+this.rand+"_"+email+".jpg";
        //       console.log(this.fileName+" is current pic email");
        //       //this.fileName = "brixy_"+email+".jpg";
        //   } else {
        //     this.storage.get('mobile_num').then((mobile_num) => {
        //       if(mobile_num){
        //         this.key_mobile = mobile_num
        //         this.fileName = "brixy_"+this.rand+"_"+mobile_num+"mob.jpg";
        //         console.log(this.fileName+" is current pic mobile");
        //         //this.fileName = "brixy_"+mobile_num+".jpg";
        //       } else {
        //         alert("Unable to update, your session has expired. Please try logging in again.");
        //       }
        //     });
        //   }
        //   this.storage.get('picture').then((val) => {
        //     this.old_picture = val;
        //     console.log(this.old_picture+" is old pic");
        //   });
        //   this.storage.get('user_id').then((val) => {
        //     this.user_id = val;
        //     console.log(this.user_id+" is user_id");
        //   });
        //   let options: FileUploadOptions = {
        //     fileKey: 'photo',
        //     fileName: this.fileName,
        //     chunkedMode: false,
        //     httpMethod: 'post',
        //     mimeType: "image/jpeg",
        //     headers: {}
        //   }
        //   console.log("options Upload:"+JSON.stringify(options));
        //   fileTransfer.upload(this.image, 'http://192.168.1.2/brixy-live/images_upload.php', options)
        //   .then((data) => {
        //     console.log("upload photo data:"+JSON.stringify(data));
        //     //if(this.key_email){
        //         let body = {
        //           action : "update_picture",
        //           picture : this.fileName,
        //           old_picture : this.old_picture, //remove the old picture uploaded
        //           user_id : this.user_id
        //         };
        //         console.log("body update_picture:"+JSON.stringify(body));
        //         this.postPvdr.postData(body, 'credentials-api.php').subscribe(data => {
        //           if(data.success){
        //             this.loader.dismiss();
        //             this.presentToast();
        //             this.router.navigate(["tabs"]);
        //           } else {
        //             alert("Please check your internet connection and try again.");
        //           }
        //         });
        //     // } else if (this.key_mobile) {
        //     //     let body = {
        //     //       action : "update_picture_mobile",
        //     //       picture : this.fileName,
        //     //       old_picture : this.old_picture, //remove the old picture uploaded
        //     //       user_id : this.user_id
        //     //     };
        //     //     console.log("body update_picture mobile:"+JSON.stringify(body));
        //     //     this.postPvdr.postData(body, 'credentials-api.php').subscribe(data => {
        //     //       if(data.success){
        //     //         this.loader.dismiss();
        //     //         this.presentToast();
        //     //         this.router.navigate(["tabs"]);
        //     //       } else {
        //     //         alert("Please check your internet connection and try again.");
        //     //       }
        //     //     });
        //     // } 
        //     //   }else {
        //     //     alert("Unable to update, your session has expired. Please try logging in again.");
        //     // }
        //   }, (err) => {
        //     console.log(err);
        //     this.loader.dismiss();
        //     alert("Please check your internet connection and try again.");
        //   });
        // });
    }
};
UploadphotoPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"] },
    { type: _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_4__["Camera"] },
    { type: _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_5__["FileTransfer"] },
    { type: _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_6__["File"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_7__["Storage"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"] },
    { type: _providers_credential_provider__WEBPACK_IMPORTED_MODULE_8__["PostProvider"] }
];
UploadphotoPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-uploadphoto',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./uploadphoto.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/uploadphoto/uploadphoto.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./uploadphoto.page.scss */ "./src/app/uploadphoto/uploadphoto.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"],
        _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_4__["Camera"],
        _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_5__["FileTransfer"],
        _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_6__["File"],
        _ionic_storage__WEBPACK_IMPORTED_MODULE_7__["Storage"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"],
        _providers_credential_provider__WEBPACK_IMPORTED_MODULE_8__["PostProvider"]])
], UploadphotoPage);



/***/ })

}]);
//# sourceMappingURL=uploadphoto-uploadphoto-module-es2015.js.map