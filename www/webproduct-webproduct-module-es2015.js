(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["webproduct-webproduct-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/webproduct/webproduct.page.html":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/webproduct/webproduct.page.html ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header mode=\"ios\" no-border>\n  <ion-toolbar mode=\"ios\">\n      <ion-buttons slot=\"secondary\" (click)=\"goBack()\">\n        <ion-button mode=\"ios\" style=\"font-size: 15px;\">\n            Back\n        </ion-button>\n      </ion-buttons>\n\n      <!-- <ion-title color=\"secondary\">Profile</ion-title> -->\n    </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-grid>\n    <ion-row>\n      <ion-col size=\"12\" size-sm>\n        <ion-card mode=\"ios\" style=\"box-shadow: none;\">\n          <img class=\"for_itemimg\" src=\"assets/greenthumb-images/sampleproduct.jpg\" />\n          <ion-card-header text-center>\n            <ion-card-title class=\"for_title\" color=\"black\">Organic Green</ion-card-title>\n            <ion-card-subtitle class=\"for_titlecateg\" color=\"secondary\">category</ion-card-subtitle>\n            <div>\n              <ion-icon class=\"for_staricon\" name=\"star\" mode=\"ios\" size=\"medium\"></ion-icon>\n              <ion-icon class=\"for_staricon\" name=\"star\" mode=\"ios\" size=\"medium\"></ion-icon>\n              <ion-icon class=\"for_staricon\" name=\"star\" mode=\"ios\" size=\"medium\"></ion-icon>\n              <ion-icon class=\"for_staricon\" name=\"star\" mode=\"ios\" size=\"medium\"></ion-icon>\n              <ion-icon class=\"for_staricon\" name=\"star-half\" mode=\"ios\" size=\"medium\"></ion-icon>\n            </div>\n            <ion-card-subtitle class=\"for_titleprice\" color=\"secondary\">$214.22</ion-card-subtitle>\n          </ion-card-header>\n        </ion-card>  \n      </ion-col>\n      <ion-col size=\"12\" size-sm>\n        <ion-card mode=\"ios\" style=\"box-shadow: none;\">\n          <img class=\"for_itemimg\" src=\"assets/greenthumb-images/sampleproduct.jpg\" />\n          <ion-card-header text-center>\n            <ion-card-title class=\"for_title\" color=\"black\">Organic Green</ion-card-title>\n            <ion-card-subtitle class=\"for_titlecateg\" color=\"secondary\">category</ion-card-subtitle>\n            <div>\n              <ion-icon class=\"for_staricon\" name=\"star\" mode=\"ios\" size=\"medium\"></ion-icon>\n              <ion-icon class=\"for_staricon\" name=\"star\" mode=\"ios\" size=\"medium\"></ion-icon>\n              <ion-icon class=\"for_staricon\" name=\"star\" mode=\"ios\" size=\"medium\"></ion-icon>\n              <ion-icon class=\"for_staricon\" name=\"star\" mode=\"ios\" size=\"medium\"></ion-icon>\n              <ion-icon class=\"for_staricon\" name=\"star-half\" mode=\"ios\" size=\"medium\"></ion-icon>\n            </div>\n            <ion-card-subtitle class=\"for_titleprice\" color=\"secondary\">$214.22</ion-card-subtitle>\n          </ion-card-header>\n        </ion-card>  \n      </ion-col>\n      <ion-col size=\"12\" size-sm>\n        <ion-card mode=\"ios\" style=\"box-shadow: none;\">\n          <img class=\"for_itemimg\" src=\"assets/greenthumb-images/sampleproduct.jpg\" />\n          <ion-card-header text-center>\n            <ion-card-title class=\"for_title\" color=\"black\">Organic Green</ion-card-title>\n            <ion-card-subtitle class=\"for_titlecateg\" color=\"secondary\">category</ion-card-subtitle>\n            <div>\n              <ion-icon class=\"for_staricon\" name=\"star\" mode=\"ios\" size=\"medium\"></ion-icon>\n              <ion-icon class=\"for_staricon\" name=\"star\" mode=\"ios\" size=\"medium\"></ion-icon>\n              <ion-icon class=\"for_staricon\" name=\"star\" mode=\"ios\" size=\"medium\"></ion-icon>\n              <ion-icon class=\"for_staricon\" name=\"star\" mode=\"ios\" size=\"medium\"></ion-icon>\n              <ion-icon class=\"for_staricon\" name=\"star-half\" mode=\"ios\" size=\"medium\"></ion-icon>\n            </div>\n            <ion-card-subtitle class=\"for_titleprice\" color=\"secondary\">$214.22</ion-card-subtitle>\n          </ion-card-header>\n        </ion-card>  \n      </ion-col>\n      <ion-col size=\"12\" size-sm>\n        <ion-card mode=\"ios\" style=\"box-shadow: none;\">\n          <img class=\"for_itemimg\" src=\"assets/greenthumb-images/sampleproduct.jpg\" />\n          <ion-card-header text-center>\n            <ion-card-title class=\"for_title\" color=\"black\">Organic Green</ion-card-title>\n            <ion-card-subtitle class=\"for_titlecateg\" color=\"secondary\">category</ion-card-subtitle>\n            <div>\n              <ion-icon class=\"for_staricon\" name=\"star\" mode=\"ios\" size=\"medium\"></ion-icon>\n              <ion-icon class=\"for_staricon\" name=\"star\" mode=\"ios\" size=\"medium\"></ion-icon>\n              <ion-icon class=\"for_staricon\" name=\"star\" mode=\"ios\" size=\"medium\"></ion-icon>\n              <ion-icon class=\"for_staricon\" name=\"star\" mode=\"ios\" size=\"medium\"></ion-icon>\n              <ion-icon class=\"for_staricon\" name=\"star-half\" mode=\"ios\" size=\"medium\"></ion-icon>\n            </div>\n            <ion-card-subtitle class=\"for_titleprice\" color=\"secondary\">$214.22</ion-card-subtitle>\n          </ion-card-header>\n        </ion-card>  \n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/webproduct/webproduct-routing.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/webproduct/webproduct-routing.module.ts ***!
  \*********************************************************/
/*! exports provided: WebproductPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WebproductPageRoutingModule", function() { return WebproductPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _webproduct_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./webproduct.page */ "./src/app/webproduct/webproduct.page.ts");




const routes = [
    {
        path: '',
        component: _webproduct_page__WEBPACK_IMPORTED_MODULE_3__["WebproductPage"]
    }
];
let WebproductPageRoutingModule = class WebproductPageRoutingModule {
};
WebproductPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], WebproductPageRoutingModule);



/***/ }),

/***/ "./src/app/webproduct/webproduct.module.ts":
/*!*************************************************!*\
  !*** ./src/app/webproduct/webproduct.module.ts ***!
  \*************************************************/
/*! exports provided: WebproductPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WebproductPageModule", function() { return WebproductPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _webproduct_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./webproduct-routing.module */ "./src/app/webproduct/webproduct-routing.module.ts");
/* harmony import */ var _webproduct_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./webproduct.page */ "./src/app/webproduct/webproduct.page.ts");







let WebproductPageModule = class WebproductPageModule {
};
WebproductPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _webproduct_routing_module__WEBPACK_IMPORTED_MODULE_5__["WebproductPageRoutingModule"]
        ],
        declarations: [_webproduct_page__WEBPACK_IMPORTED_MODULE_6__["WebproductPage"]]
    })
], WebproductPageModule);



/***/ }),

/***/ "./src/app/webproduct/webproduct.page.scss":
/*!*************************************************!*\
  !*** ./src/app/webproduct/webproduct.page.scss ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".for_title {\n  font-size: 17px;\n  margin-top: 3px;\n  font-weight: 400;\n  display: block;\n  text-transform: capitalize;\n  color: black;\n}\n\n.for_itemimg {\n  background: #f6f7f2;\n}\n\n.for_staricon {\n  color: #ffc107 !important;\n}\n\n.for_titleprice {\n  line-height: 100%;\n  display: inline-block;\n  font-weight: bold;\n  font-size: 16px;\n}\n\n.for_categitem {\n  text-align: center !important;\n  color: #fff;\n  background: red;\n  font-size: 13px;\n  text-transform: capitalize;\n  font-weight: 500;\n  font-family: Satisfy, cursive;\n  display: block;\n  line-height: 35px;\n  position: absolute;\n  letter-spacing: 1px;\n  top: 13px;\n  z-index: 1;\n  padding: 0 15px;\n  left: 10px;\n  border-radius: 55px/35px;\n}\n\n.for_titlecateg {\n  margin-top: 2px;\n  font-size: 13px;\n  font-weight: 400;\n  text-transform: uppercase;\n  margin-bottom: 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZW5kcmljay9EZXNrdG9wL1RlbG1vIFNvbHV0aW9ucy9Gb3JQcm9kdWN0aW9uL21heTEwMjAyMS9sYXN0IHNldHVwL2dyZWVudGh1bWJhcHAgY29weS9zcmMvYXBwL3dlYnByb2R1Y3Qvd2VicHJvZHVjdC5wYWdlLnNjc3MiLCJzcmMvYXBwL3dlYnByb2R1Y3Qvd2VicHJvZHVjdC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxlQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLDBCQUFBO0VBQ0EsWUFBQTtBQ0NKOztBRENBO0VBQ0ksbUJBQUE7QUNFSjs7QURBQTtFQUNJLHlCQUFBO0FDR0o7O0FEREE7RUFDSSxpQkFBQTtFQUNBLHFCQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0FDSUo7O0FERkE7RUFDSSw2QkFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0EsZUFBQTtFQUNBLDBCQUFBO0VBQ0EsZ0JBQUE7RUFDQSw2QkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxTQUFBO0VBQ0EsVUFBQTtFQUNBLGVBQUE7RUFDQSxVQUFBO0VBQ0Esd0JBQUE7QUNLSjs7QURIQTtFQUNJLGVBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSx5QkFBQTtFQUNBLG1CQUFBO0FDTUoiLCJmaWxlIjoic3JjL2FwcC93ZWJwcm9kdWN0L3dlYnByb2R1Y3QucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmZvcl90aXRsZXtcbiAgICBmb250LXNpemU6IDE3cHg7XG4gICAgbWFyZ2luLXRvcDogM3B4O1xuICAgIGZvbnQtd2VpZ2h0OiA0MDA7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG4gICAgY29sb3I6IGJsYWNrO1xufVxuLmZvcl9pdGVtaW1ne1xuICAgIGJhY2tncm91bmQ6ICNmNmY3ZjI7XG59XG4uZm9yX3N0YXJpY29ue1xuICAgIGNvbG9yOiAjZmZjMTA3ICFpbXBvcnRhbnQ7XG59XG4uZm9yX3RpdGxlcHJpY2V7XG4gICAgbGluZS1oZWlnaHQ6IDEwMCU7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIGZvbnQtc2l6ZTogMTZweDtcbn1cbi5mb3JfY2F0ZWdpdGVte1xuICAgIHRleHQtYWxpZ246IGNlbnRlciAhaW1wb3J0YW50O1xuICAgIGNvbG9yOiAjZmZmO1xuICAgIGJhY2tncm91bmQ6IHJlZDtcbiAgICBmb250LXNpemU6IDEzcHg7XG4gICAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgICBmb250LWZhbWlseTogU2F0aXNmeSxjdXJzaXZlO1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIGxpbmUtaGVpZ2h0OiAzNXB4O1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBsZXR0ZXItc3BhY2luZzogMXB4O1xuICAgIHRvcDogMTNweDtcbiAgICB6LWluZGV4OiAxO1xuICAgIHBhZGRpbmc6IDAgMTVweDtcbiAgICBsZWZ0OiAxMHB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDU1cHgvMzVweDtcbn1cbi5mb3JfdGl0bGVjYXRlZ3tcbiAgICBtYXJnaW4tdG9wOiAycHg7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIGZvbnQtd2VpZ2h0OiA0MDA7XG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xufVxuIiwiLmZvcl90aXRsZSB7XG4gIGZvbnQtc2l6ZTogMTdweDtcbiAgbWFyZ2luLXRvcDogM3B4O1xuICBmb250LXdlaWdodDogNDAwO1xuICBkaXNwbGF5OiBibG9jaztcbiAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG4gIGNvbG9yOiBibGFjaztcbn1cblxuLmZvcl9pdGVtaW1nIHtcbiAgYmFja2dyb3VuZDogI2Y2ZjdmMjtcbn1cblxuLmZvcl9zdGFyaWNvbiB7XG4gIGNvbG9yOiAjZmZjMTA3ICFpbXBvcnRhbnQ7XG59XG5cbi5mb3JfdGl0bGVwcmljZSB7XG4gIGxpbmUtaGVpZ2h0OiAxMDAlO1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBmb250LXNpemU6IDE2cHg7XG59XG5cbi5mb3JfY2F0ZWdpdGVtIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyICFpbXBvcnRhbnQ7XG4gIGNvbG9yOiAjZmZmO1xuICBiYWNrZ3JvdW5kOiByZWQ7XG4gIGZvbnQtc2l6ZTogMTNweDtcbiAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIGZvbnQtZmFtaWx5OiBTYXRpc2Z5LCBjdXJzaXZlO1xuICBkaXNwbGF5OiBibG9jaztcbiAgbGluZS1oZWlnaHQ6IDM1cHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGV0dGVyLXNwYWNpbmc6IDFweDtcbiAgdG9wOiAxM3B4O1xuICB6LWluZGV4OiAxO1xuICBwYWRkaW5nOiAwIDE1cHg7XG4gIGxlZnQ6IDEwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDU1cHgvMzVweDtcbn1cblxuLmZvcl90aXRsZWNhdGVnIHtcbiAgbWFyZ2luLXRvcDogMnB4O1xuICBmb250LXNpemU6IDEzcHg7XG4gIGZvbnQtd2VpZ2h0OiA0MDA7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XG59Il19 */");

/***/ }),

/***/ "./src/app/webproduct/webproduct.page.ts":
/*!***********************************************!*\
  !*** ./src/app/webproduct/webproduct.page.ts ***!
  \***********************************************/
/*! exports provided: WebproductPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WebproductPage", function() { return WebproductPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let WebproductPage = class WebproductPage {
    constructor() { }
    ngOnInit() {
    }
    goBack() {
        window.history.back();
    }
};
WebproductPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-webproduct',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./webproduct.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/webproduct/webproduct.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./webproduct.page.scss */ "./src/app/webproduct/webproduct.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], WebproductPage);



/***/ })

}]);
//# sourceMappingURL=webproduct-webproduct-module-es2015.js.map