(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["webprofile-webprofile-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/webprofile/webprofile.page.html":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/webprofile/webprofile.page.html ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header mode=\"ios\" no-border>\n  <ion-toolbar mode=\"ios\">\n      <ion-buttons slot=\"secondary\" (click)=\"goBack()\">\n        <ion-button mode=\"ios\" style=\"font-size: 15px;\">\n            Back\n        </ion-button>\n      </ion-buttons>\n\n      <!-- <ion-title color=\"secondary\">Profile</ion-title> -->\n    </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-grid>\n    <ion-row>\n      <ion-col size=\"12\" size-sm>\n        <ion-card mode=\"ios\" class=\"for_cardmargin\">\n          <img class=\"for_img\" src=\"assets/greenthumb-images/sampleimg.jpg\" />\n          <ion-card-content class=\"for_backroundoverlay\">\n            <h2 class=\"for_h2title\">Macerity</h2>\n            <div>\n              <ion-icon class=\"for_staricon\" name=\"star\" mode=\"ios\" size=\"medium\"></ion-icon>\n              <ion-icon class=\"for_staricon\" name=\"star\" mode=\"ios\" size=\"medium\"></ion-icon>\n              <ion-icon class=\"for_staricon\" name=\"star\" mode=\"ios\" size=\"medium\"></ion-icon>\n              <ion-icon class=\"for_staricon\" name=\"star\" mode=\"ios\" size=\"medium\"></ion-icon>\n              <ion-icon class=\"for_staricon\" name=\"star-half\" mode=\"ios\" size=\"medium\"></ion-icon>\n            </div>\n            <p class=\"for_paddress\">California, NY, 12845,</p>\n            <p class=\"for_paddress\" style=\"line-height: 0;\">\n              <ion-icon class=\"for_callicon\" name=\"call\" mode=\"ios\"></ion-icon>&nbsp;09907852578</p>\n          </ion-card-content>\n          <ion-card-header class=\"for_headerbackground\">\n            <ion-button class=\"for_iconarrow\" mode=\"ios\" fill=\"clear\">\n              <ion-icon mode=\"ios\" name=\"arrow-forward\" color=\"light\"></ion-icon>\n            </ion-button>\n          </ion-card-header>\n        </ion-card>  \n      </ion-col>\n      <ion-col size=\"12\" size-sm>\n        <ion-card mode=\"ios\" class=\"for_cardmargin\">\n          <img class=\"for_img\" src=\"assets/greenthumb-images/sampleimg.jpg\" />\n          <ion-card-content class=\"for_backroundoverlay\">\n            <h2 class=\"for_h2title\">Macerity</h2>\n            <div>\n              <ion-icon class=\"for_staricon\" name=\"star\" mode=\"ios\" size=\"medium\"></ion-icon>\n              <ion-icon class=\"for_staricon\" name=\"star\" mode=\"ios\" size=\"medium\"></ion-icon>\n              <ion-icon class=\"for_staricon\" name=\"star\" mode=\"ios\" size=\"medium\"></ion-icon>\n              <ion-icon class=\"for_staricon\" name=\"star\" mode=\"ios\" size=\"medium\"></ion-icon>\n              <ion-icon class=\"for_staricon\" name=\"star-half\" mode=\"ios\" size=\"medium\"></ion-icon>\n            </div>\n            <p class=\"for_paddress\">California, NY, 12845,</p>\n            <p class=\"for_paddress\" style=\"line-height: 0;\">\n              <ion-icon class=\"for_callicon\" name=\"call\" mode=\"ios\"></ion-icon>&nbsp;09907852578</p>\n          </ion-card-content>\n          <ion-card-header class=\"for_headerbackground\">\n            <ion-button class=\"for_iconarrow\" mode=\"ios\" fill=\"clear\">\n              <ion-icon mode=\"ios\" name=\"arrow-forward\" color=\"light\"></ion-icon>\n            </ion-button>\n          </ion-card-header>\n        </ion-card>\n      </ion-col>\n      <ion-col size=\"12\" size-sm>\n        <ion-card mode=\"ios\" class=\"for_cardmargin\">\n          <img class=\"for_img\" src=\"assets/greenthumb-images/sampleimg.jpg\" />\n          <ion-card-content class=\"for_backroundoverlay\">\n            <h2 class=\"for_h2title\">Macerity</h2>\n            <div>\n              <ion-icon class=\"for_staricon\" name=\"star\" mode=\"ios\" size=\"medium\"></ion-icon>\n              <ion-icon class=\"for_staricon\" name=\"star\" mode=\"ios\" size=\"medium\"></ion-icon>\n              <ion-icon class=\"for_staricon\" name=\"star\" mode=\"ios\" size=\"medium\"></ion-icon>\n              <ion-icon class=\"for_staricon\" name=\"star\" mode=\"ios\" size=\"medium\"></ion-icon>\n              <ion-icon class=\"for_staricon\" name=\"star-half\" mode=\"ios\" size=\"medium\"></ion-icon>\n            </div>\n            <p class=\"for_paddress\">California, NY, 12845,</p>\n            <p class=\"for_paddress\" style=\"line-height: 0;\">\n              <ion-icon class=\"for_callicon\" name=\"call\" mode=\"ios\"></ion-icon>&nbsp;09907852578</p>\n          </ion-card-content>\n          <ion-card-header class=\"for_headerbackground\">\n            <ion-button class=\"for_iconarrow\" mode=\"ios\" fill=\"clear\">\n              <ion-icon mode=\"ios\" name=\"arrow-forward\" color=\"light\"></ion-icon>\n            </ion-button>\n          </ion-card-header>\n        </ion-card>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>\n");

/***/ }),

/***/ "./src/app/webprofile/webprofile-routing.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/webprofile/webprofile-routing.module.ts ***!
  \*********************************************************/
/*! exports provided: WebprofilePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WebprofilePageRoutingModule", function() { return WebprofilePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _webprofile_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./webprofile.page */ "./src/app/webprofile/webprofile.page.ts");




const routes = [
    {
        path: '',
        component: _webprofile_page__WEBPACK_IMPORTED_MODULE_3__["WebprofilePage"]
    }
];
let WebprofilePageRoutingModule = class WebprofilePageRoutingModule {
};
WebprofilePageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], WebprofilePageRoutingModule);



/***/ }),

/***/ "./src/app/webprofile/webprofile.module.ts":
/*!*************************************************!*\
  !*** ./src/app/webprofile/webprofile.module.ts ***!
  \*************************************************/
/*! exports provided: WebprofilePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WebprofilePageModule", function() { return WebprofilePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _webprofile_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./webprofile-routing.module */ "./src/app/webprofile/webprofile-routing.module.ts");
/* harmony import */ var _webprofile_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./webprofile.page */ "./src/app/webprofile/webprofile.page.ts");







let WebprofilePageModule = class WebprofilePageModule {
};
WebprofilePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _webprofile_routing_module__WEBPACK_IMPORTED_MODULE_5__["WebprofilePageRoutingModule"]
        ],
        declarations: [_webprofile_page__WEBPACK_IMPORTED_MODULE_6__["WebprofilePage"]]
    })
], WebprofilePageModule);



/***/ }),

/***/ "./src/app/webprofile/webprofile.page.scss":
/*!*************************************************!*\
  !*** ./src/app/webprofile/webprofile.page.scss ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".for_backroundoverlay {\n  position: absolute;\n  top: 0;\n  left: 0;\n  z-index: 9;\n  width: 100%;\n  height: 100%;\n  background: rgba(0, 0, 0, 0.45);\n  color: #fff;\n}\n\n.for_img {\n  min-height: 220px;\n  -o-object-fit: cover;\n     object-fit: cover;\n}\n\n.for_h2title {\n  margin-top: 1.5rem;\n  margin-bottom: 0.5rem;\n  font-size: 20px;\n}\n\n.for_staricon {\n  color: #fa9a00 !important;\n}\n\n.for_paddress {\n  margin: 0 0 5px;\n  line-height: 23px;\n  padding: 0;\n}\n\n.for_callicon {\n  font-size: 16px;\n  margin-bottom: -3px;\n}\n\n.for_headerbackground {\n  background: white;\n  z-index: 999;\n  padding: 15px 20px;\n}\n\n.for_iconarrow {\n  background: #34ad54;\n}\n\n/*##Device = Desktops*/\n\n@media (min-width: 1281px) {\n  .for_cardmargin {\n    margin: 16px;\n  }\n}\n\n/* ##Device = Laptops, Desktops*/\n\n@media (min-width: 1025px) and (max-width: 1280px) {\n  .for_cardmargin {\n    margin: 16px;\n  }\n}\n\n/* ##Device = Tablets, Ipads (portrait)*/\n\n@media (min-width: 768px) and (max-width: 1024px) {\n  .for_cardmargin {\n    margin: 0px;\n  }\n}\n\n/* ##Device = Tablets, Ipads (landscape)*/\n\n@media (min-width: 768px) and (max-width: 1024px) and (orientation: landscape) {\n  .for_cardmargin {\n    margin: 0px;\n  }\n}\n\n/* ##Device = Low Resolution Tablets, Mobiles (Landscape)*/\n\n@media (min-width: 481px) and (max-width: 767px) {\n  .for_cardmargin {\n    margin: 0px;\n  }\n}\n\n/* ##Device = Most of the Smartphones Mobiles (Portrait)*/\n\n@media (min-width: 320px) and (max-width: 480px) {\n  .for_cardmargin {\n    margin: 0px;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9rZW5kcmljay9EZXNrdG9wL1RlbG1vIFNvbHV0aW9ucy9Gb3JQcm9kdWN0aW9uL21heTEwMjAyMS9sYXN0IHNldHVwL2dyZWVudGh1bWJhcHAgY29weS9zcmMvYXBwL3dlYnByb2ZpbGUvd2VicHJvZmlsZS5wYWdlLnNjc3MiLCJzcmMvYXBwL3dlYnByb2ZpbGUvd2VicHJvZmlsZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxrQkFBQTtFQUNBLE1BQUE7RUFDQSxPQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsK0JBQUE7RUFDQSxXQUFBO0FDQ0o7O0FEQ0E7RUFDSSxpQkFBQTtFQUNBLG9CQUFBO0tBQUEsaUJBQUE7QUNFSjs7QURBQTtFQUNJLGtCQUFBO0VBQ0EscUJBQUE7RUFDQSxlQUFBO0FDR0o7O0FEREE7RUFDSSx5QkFBQTtBQ0lKOztBREZBO0VBQ0ksZUFBQTtFQUNBLGlCQUFBO0VBQ0EsVUFBQTtBQ0tKOztBREhBO0VBQ0ksZUFBQTtFQUNBLG1CQUFBO0FDTUo7O0FESkE7RUFDSSxpQkFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtBQ09KOztBRExBO0VBQ0ksbUJBQUE7QUNRSjs7QUROQSxzQkFBQTs7QUFDQTtFQUNJO0lBQ0ksWUFBQTtFQ1NOO0FBQ0Y7O0FEUEEsZ0NBQUE7O0FBQ0U7RUFDRTtJQUNJLFlBQUE7RUNTTjtBQUNGOztBRE5BLHdDQUFBOztBQUNBO0VBQ0k7SUFDSSxXQUFBO0VDUU47QUFDRjs7QURMQSx5Q0FBQTs7QUFDQTtFQUNJO0lBQ0ksV0FBQTtFQ09OO0FBQ0Y7O0FETEEsMERBQUE7O0FBQ0E7RUFDSTtJQUNJLFdBQUE7RUNPTjtBQUNGOztBRExBLHlEQUFBOztBQUNFO0VBQ0U7SUFDSSxXQUFBO0VDT047QUFDRiIsImZpbGUiOiJzcmMvYXBwL3dlYnByb2ZpbGUvd2VicHJvZmlsZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZm9yX2JhY2tyb3VuZG92ZXJsYXl7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogMDtcbiAgICBsZWZ0OiAwO1xuICAgIHotaW5kZXg6IDk7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIGJhY2tncm91bmQ6IHJnYmEoMCwwLDAsLjQ1KTtcbiAgICBjb2xvcjogI2ZmZjtcbn1cbi5mb3JfaW1ne1xuICAgIG1pbi1oZWlnaHQ6IDIyMHB4O1xuICAgIG9iamVjdC1maXQ6IGNvdmVyO1xufVxuLmZvcl9oMnRpdGxle1xuICAgIG1hcmdpbi10b3A6IDEuNXJlbTtcbiAgICBtYXJnaW4tYm90dG9tOiAuNXJlbTtcbiAgICBmb250LXNpemU6IDIwcHg7XG59XG4uZm9yX3N0YXJpY29ue1xuICAgIGNvbG9yOiAjZmE5YTAwICFpbXBvcnRhbnQ7XG59XG4uZm9yX3BhZGRyZXNze1xuICAgIG1hcmdpbjogMCAwIDVweDtcbiAgICBsaW5lLWhlaWdodDogMjNweDtcbiAgICBwYWRkaW5nOiAwO1xufVxuLmZvcl9jYWxsaWNvbntcbiAgICBmb250LXNpemU6IDE2cHg7XG4gICAgbWFyZ2luLWJvdHRvbTogLTNweDtcbn1cbi5mb3JfaGVhZGVyYmFja2dyb3VuZHtcbiAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgICB6LWluZGV4OiA5OTk7XG4gICAgcGFkZGluZzogMTVweCAyMHB4O1xufVxuLmZvcl9pY29uYXJyb3d7XG4gICAgYmFja2dyb3VuZDogIzM0YWQ1NDtcbn1cbi8qIyNEZXZpY2UgPSBEZXNrdG9wcyovXG5AbWVkaWEgKG1pbi13aWR0aDogMTI4MXB4KSB7XG4gICAgLmZvcl9jYXJkbWFyZ2lue1xuICAgICAgICBtYXJnaW46IDE2cHg7XG4gICAgfSBcbn1cbi8qICMjRGV2aWNlID0gTGFwdG9wcywgRGVza3RvcHMqL1xuICBAbWVkaWEgKG1pbi13aWR0aDogMTAyNXB4KSBhbmQgKG1heC13aWR0aDogMTI4MHB4KSB7XG4gICAgLmZvcl9jYXJkbWFyZ2lue1xuICAgICAgICBtYXJnaW46IDE2cHg7XG4gICAgfSBcbiAgfVxuICBcbi8qICMjRGV2aWNlID0gVGFibGV0cywgSXBhZHMgKHBvcnRyYWl0KSovXG5AbWVkaWEgKG1pbi13aWR0aDogNzY4cHgpIGFuZCAobWF4LXdpZHRoOiAxMDI0cHgpIHtcbiAgICAuZm9yX2NhcmRtYXJnaW57XG4gICAgICAgIG1hcmdpbjogMHB4O1xuICAgIH0gXG4gIH1cbiAgXG4vKiAjI0RldmljZSA9IFRhYmxldHMsIElwYWRzIChsYW5kc2NhcGUpKi9cbkBtZWRpYSAobWluLXdpZHRoOiA3NjhweCkgYW5kIChtYXgtd2lkdGg6IDEwMjRweCkgYW5kIChvcmllbnRhdGlvbjogbGFuZHNjYXBlKSB7XG4gICAgLmZvcl9jYXJkbWFyZ2lue1xuICAgICAgICBtYXJnaW46IDBweDtcbiAgICB9IFxuICB9XG4vKiAjI0RldmljZSA9IExvdyBSZXNvbHV0aW9uIFRhYmxldHMsIE1vYmlsZXMgKExhbmRzY2FwZSkqL1xuQG1lZGlhIChtaW4td2lkdGg6IDQ4MXB4KSBhbmQgKG1heC13aWR0aDogNzY3cHgpIHtcbiAgICAuZm9yX2NhcmRtYXJnaW57XG4gICAgICAgIG1hcmdpbjogMHB4O1xuICAgIH0gXG59XG4vKiAjI0RldmljZSA9IE1vc3Qgb2YgdGhlIFNtYXJ0cGhvbmVzIE1vYmlsZXMgKFBvcnRyYWl0KSovXG4gIEBtZWRpYSAobWluLXdpZHRoOiAzMjBweCkgYW5kIChtYXgtd2lkdGg6IDQ4MHB4KSB7XG4gICAgLmZvcl9jYXJkbWFyZ2lue1xuICAgICAgICBtYXJnaW46IDBweDtcbiAgICB9IFxuICB9IiwiLmZvcl9iYWNrcm91bmRvdmVybGF5IHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDA7XG4gIGxlZnQ6IDA7XG4gIHotaW5kZXg6IDk7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG4gIGJhY2tncm91bmQ6IHJnYmEoMCwgMCwgMCwgMC40NSk7XG4gIGNvbG9yOiAjZmZmO1xufVxuXG4uZm9yX2ltZyB7XG4gIG1pbi1oZWlnaHQ6IDIyMHB4O1xuICBvYmplY3QtZml0OiBjb3Zlcjtcbn1cblxuLmZvcl9oMnRpdGxlIHtcbiAgbWFyZ2luLXRvcDogMS41cmVtO1xuICBtYXJnaW4tYm90dG9tOiAwLjVyZW07XG4gIGZvbnQtc2l6ZTogMjBweDtcbn1cblxuLmZvcl9zdGFyaWNvbiB7XG4gIGNvbG9yOiAjZmE5YTAwICFpbXBvcnRhbnQ7XG59XG5cbi5mb3JfcGFkZHJlc3Mge1xuICBtYXJnaW46IDAgMCA1cHg7XG4gIGxpbmUtaGVpZ2h0OiAyM3B4O1xuICBwYWRkaW5nOiAwO1xufVxuXG4uZm9yX2NhbGxpY29uIHtcbiAgZm9udC1zaXplOiAxNnB4O1xuICBtYXJnaW4tYm90dG9tOiAtM3B4O1xufVxuXG4uZm9yX2hlYWRlcmJhY2tncm91bmQge1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgei1pbmRleDogOTk5O1xuICBwYWRkaW5nOiAxNXB4IDIwcHg7XG59XG5cbi5mb3JfaWNvbmFycm93IHtcbiAgYmFja2dyb3VuZDogIzM0YWQ1NDtcbn1cblxuLyojI0RldmljZSA9IERlc2t0b3BzKi9cbkBtZWRpYSAobWluLXdpZHRoOiAxMjgxcHgpIHtcbiAgLmZvcl9jYXJkbWFyZ2luIHtcbiAgICBtYXJnaW46IDE2cHg7XG4gIH1cbn1cbi8qICMjRGV2aWNlID0gTGFwdG9wcywgRGVza3RvcHMqL1xuQG1lZGlhIChtaW4td2lkdGg6IDEwMjVweCkgYW5kIChtYXgtd2lkdGg6IDEyODBweCkge1xuICAuZm9yX2NhcmRtYXJnaW4ge1xuICAgIG1hcmdpbjogMTZweDtcbiAgfVxufVxuLyogIyNEZXZpY2UgPSBUYWJsZXRzLCBJcGFkcyAocG9ydHJhaXQpKi9cbkBtZWRpYSAobWluLXdpZHRoOiA3NjhweCkgYW5kIChtYXgtd2lkdGg6IDEwMjRweCkge1xuICAuZm9yX2NhcmRtYXJnaW4ge1xuICAgIG1hcmdpbjogMHB4O1xuICB9XG59XG4vKiAjI0RldmljZSA9IFRhYmxldHMsIElwYWRzIChsYW5kc2NhcGUpKi9cbkBtZWRpYSAobWluLXdpZHRoOiA3NjhweCkgYW5kIChtYXgtd2lkdGg6IDEwMjRweCkgYW5kIChvcmllbnRhdGlvbjogbGFuZHNjYXBlKSB7XG4gIC5mb3JfY2FyZG1hcmdpbiB7XG4gICAgbWFyZ2luOiAwcHg7XG4gIH1cbn1cbi8qICMjRGV2aWNlID0gTG93IFJlc29sdXRpb24gVGFibGV0cywgTW9iaWxlcyAoTGFuZHNjYXBlKSovXG5AbWVkaWEgKG1pbi13aWR0aDogNDgxcHgpIGFuZCAobWF4LXdpZHRoOiA3NjdweCkge1xuICAuZm9yX2NhcmRtYXJnaW4ge1xuICAgIG1hcmdpbjogMHB4O1xuICB9XG59XG4vKiAjI0RldmljZSA9IE1vc3Qgb2YgdGhlIFNtYXJ0cGhvbmVzIE1vYmlsZXMgKFBvcnRyYWl0KSovXG5AbWVkaWEgKG1pbi13aWR0aDogMzIwcHgpIGFuZCAobWF4LXdpZHRoOiA0ODBweCkge1xuICAuZm9yX2NhcmRtYXJnaW4ge1xuICAgIG1hcmdpbjogMHB4O1xuICB9XG59Il19 */");

/***/ }),

/***/ "./src/app/webprofile/webprofile.page.ts":
/*!***********************************************!*\
  !*** ./src/app/webprofile/webprofile.page.ts ***!
  \***********************************************/
/*! exports provided: WebprofilePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WebprofilePage", function() { return WebprofilePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let WebprofilePage = class WebprofilePage {
    constructor() { }
    ngOnInit() {
    }
    goBack() {
        window.history.back();
    }
};
WebprofilePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-webprofile',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./webprofile.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/webprofile/webprofile.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./webprofile.page.scss */ "./src/app/webprofile/webprofile.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], WebprofilePage);



/***/ })

}]);
//# sourceMappingURL=webprofile-webprofile-module-es2015.js.map